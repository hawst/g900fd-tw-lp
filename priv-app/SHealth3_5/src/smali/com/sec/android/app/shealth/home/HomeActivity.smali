.class public Lcom/sec/android/app/shealth/home/HomeActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;
.source "HomeActivity.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IContentInitializatorGetter;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;
.implements Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;
.implements Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$IRestoreCompleteListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/home/HomeActivity$SystemCloseDialogReceiver;
    }
.end annotation


# static fields
.field private static final ALL_DATA:Ljava/lang/String; = "ALL_DATA"

.field private static final BP_BG_DATA:Ljava/lang/String; = "BP_BG_DATA"

.field private static final BURNT_CALORIES:Ljava/lang/String; = "burnt_calories"

.field private static final CANCEL_PIN_REQUEST_CODE:I = 0x457

.field public static final CROP_FROM_ALBUM:I = 0x4

.field public static final CROP_FROM_CAMERA:I = 0x5

.field private static final DATA_CONNECTION_DIALOG:Ljava/lang/String; = "data_connection_dialog"

.field private static final HR_STRESS_DATA:Ljava/lang/String; = "HR_STRESS_DATA"

.field private static final MEDIA_DOCUMENTS_URI:Ljava/lang/String; = "content://com.android.providers.media.documents/"

.field public static final MORE_EDIT_FAVORITES:I = 0x0

.field public static final MORE_HELP:I = 0x3

.field public static final MORE_SETTINGS:I = 0x2

.field public static final MORE_SET_WALLPAPER:I = 0x1

.field private static final NOTICE_EXPORTABLE_FEATURE:Ljava/lang/String; = "notice_exportable_feature"

.field private static final PIN_CODE_SERVICE:Ljava/lang/String; = "com.sec.android.app.shealth.common.custom.base.PincodeResetService"

.field private static final POPUP_MOBILE:I = 0x6

.field private static final POPUP_WIFI:I = 0x8

.field public static final REQUEST_FROM_ALBUM:I = 0x3

.field public static final REQUEST_TAKE_PICTURE:I = 0x2

.field private static final SEND_SYNC_WEARABLE_MGR:I = 0x65

.field public static final SET_WALLPAPER:I = 0x1

.field private static final SYSTEM_DIALOG_REASON_HOME_KEY:Ljava/lang/String; = "homekey"

.field private static final SYSTEM_DIALOG_REASON_KEY:Ljava/lang/String; = "reason"

.field private static final TAG:Ljava/lang/String;

.field private static exportableData:Z

.field public static isConncectDevicePopupShown:Z

.field public static isDeleteDownloadAppPopupShown:Z

.field public static isThirdPartyAppLaunched:Z

.field private static mDrawableList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static sAllDataPresent:Z

.field private static sBPBGData:Z

.field private static sHRStressData:Z


# instance fields
.field private final TRANSPARENT_DRAWABLE:Landroid/graphics/drawable/ColorDrawable;

.field backPressListenernew:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;

.field private burntCaloriesPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

.field private connectionBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

.field private connectionWifiBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

.field private doNotShowCheckBox:Landroid/widget/CheckBox;

.field private homeFlag:Z

.field private homeMenu:Landroid/view/Menu;

.field private isCheckSeviceConnection:Z

.field private isMenuLoaded:Z

.field private isSettingsItem:Z

.field private mActivityContext:Landroid/content/Context;

.field private mBurntCalories:[I

.field private mDeviceFinderServiceListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

.field private mDeviceItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;",
            ">;"
        }
    .end annotation
.end field

.field private mDoNotShowAgain:Landroid/widget/CheckBox;

.field private mExportableFeatureDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mExportableFeatureDialogBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

.field private mFeatureList:Landroid/widget/TextView;

.field mHandler:Landroid/os/Handler;

.field private mHomeWearableManager:Lcom/sec/android/app/shealth/home/HomeWearableManager;

.field private mIsHelpFlag:Z

.field private mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

.field private mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

.field public mSummayFrag:Lcom/sec/android/app/shealth/home/HomeFragment;

.field private mSyncButton:Landroid/widget/LinearLayout;

.field private mSyncButtonView:Landroid/view/View;

.field private mSyncCounter:I

.field private mSyncProgressImage:Landroid/view/View;

.field private mSyncTimeoutRunnable:Ljava/lang/Runnable;

.field private mSyncing:Z

.field private mSystemConfigurationChanged:Z

.field private mWearableConnectedObserver:Landroid/database/ContentObserver;

.field private networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

.field private popupMode:I

.field private preOff:F

.field private restoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

.field private simplePopupController:Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;

.field private systemCloseDialogReceiver:Lcom/sec/android/app/shealth/home/HomeActivity$SystemCloseDialogReceiver;

.field public unsupportedFeatureList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;",
            ">;"
        }
    .end annotation
.end field

.field private updateCheck:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

.field private widgetBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private widgetIntent:Landroid/content/Intent;

.field private widgetLaunchAfterTCPPAcceptedIntent:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 163
    const-class v0, Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    .line 213
    sput-boolean v1, Lcom/sec/android/app/shealth/home/HomeActivity;->isConncectDevicePopupShown:Z

    .line 214
    sput-boolean v1, Lcom/sec/android/app/shealth/home/HomeActivity;->isDeleteDownloadAppPopupShown:Z

    .line 215
    sput-boolean v1, Lcom/sec/android/app/shealth/home/HomeActivity;->isThirdPartyAppLaunched:Z

    .line 221
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/home/HomeActivity;->mDrawableList:Ljava/util/ArrayList;

    .line 234
    sput-boolean v1, Lcom/sec/android/app/shealth/home/HomeActivity;->exportableData:Z

    .line 235
    sput-boolean v1, Lcom/sec/android/app/shealth/home/HomeActivity;->sAllDataPresent:Z

    .line 236
    sput-boolean v1, Lcom/sec/android/app/shealth/home/HomeActivity;->sHRStressData:Z

    .line 237
    sput-boolean v1, Lcom/sec/android/app/shealth/home/HomeActivity;->sBPBGData:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 162
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;-><init>()V

    .line 164
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const-string v1, "#00000000"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->TRANSPARENT_DRAWABLE:Landroid/graphics/drawable/ColorDrawable;

    .line 175
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncing:Z

    .line 184
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->isSettingsItem:Z

    .line 186
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->isMenuLoaded:Z

    .line 192
    iput-object v3, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mActivityContext:Landroid/content/Context;

    .line 195
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mIsHelpFlag:Z

    .line 197
    const v0, 0x3f7d70a4    # 0.99f

    iput v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->preOff:F

    .line 200
    iput-object v3, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->burntCaloriesPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 205
    iput-object v3, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mExportableFeatureDialogBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 206
    iput-object v3, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mExportableFeatureDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 209
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->unsupportedFeatureList:Ljava/util/ArrayList;

    .line 218
    iput-object v3, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->homeMenu:Landroid/view/Menu;

    .line 223
    iput-object v3, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->widgetLaunchAfterTCPPAcceptedIntent:Landroid/content/Intent;

    .line 226
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSystemConfigurationChanged:Z

    .line 227
    iput-object v3, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mHomeWearableManager:Lcom/sec/android/app/shealth/home/HomeWearableManager;

    .line 228
    iput v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncCounter:I

    .line 230
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->isCheckSeviceConnection:Z

    .line 242
    new-instance v0, Lcom/sec/android/app/shealth/home/HomeActivity$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/home/HomeActivity$1;-><init>(Lcom/sec/android/app/shealth/home/HomeActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mWearableConnectedObserver:Landroid/database/ContentObserver;

    .line 264
    new-instance v0, Lcom/sec/android/app/shealth/home/HomeActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/home/HomeActivity$2;-><init>(Lcom/sec/android/app/shealth/home/HomeActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mDeviceFinderServiceListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    .line 861
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mHandler:Landroid/os/Handler;

    .line 863
    new-instance v0, Lcom/sec/android/app/shealth/home/HomeActivity$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/home/HomeActivity$5;-><init>(Lcom/sec/android/app/shealth/home/HomeActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncTimeoutRunnable:Ljava/lang/Runnable;

    .line 1194
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->simplePopupController:Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;

    .line 1551
    new-instance v0, Lcom/sec/android/app/shealth/home/HomeActivity$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/home/HomeActivity$11;-><init>(Lcom/sec/android/app/shealth/home/HomeActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->widgetBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 1714
    iput-object v3, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mDeviceItemList:Ljava/util/ArrayList;

    .line 1901
    iput v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->popupMode:I

    .line 1912
    new-instance v0, Lcom/sec/android/app/shealth/home/HomeActivity$16;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/home/HomeActivity$16;-><init>(Lcom/sec/android/app/shealth/home/HomeActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->backPressListenernew:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;

    .line 2000
    return-void
.end method

.method private CheckForVersion()V
    .locals 4

    .prologue
    .line 663
    new-instance v0, Lcom/sec/android/app/shealth/CheckVersionCode;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/CheckVersionCode;-><init>(Landroid/content/Context;)V

    .line 665
    .local v0, "checkCode":Lcom/sec/android/app/shealth/CheckVersionCode;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/CheckVersionCode;->readAssertFile()V

    .line 667
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/CheckVersionCode;->isTermsOfUseUpdate()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 668
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 669
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "termOfUseVersion"

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/CheckVersionCode;->getTermsOfUseUpdateCode()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 671
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/CheckVersionCode;->isPrivacyUpdate()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 672
    const-string/jumbo v2, "privacyVersion"

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/CheckVersionCode;->getPrivacyUpdateCode()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 675
    :cond_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/home/HomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 676
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->finish()V

    .line 684
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_1
    :goto_0
    return-void

    .line 678
    :cond_2
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/CheckVersionCode;->isPrivacyUpdate()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 679
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/shealth/home/UpdatePrivacyPolicy;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 680
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string/jumbo v2, "privacyVersion"

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/CheckVersionCode;->getPrivacyUpdateCode()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 681
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/home/HomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 682
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->finish()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/home/HomeActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;

    .prologue
    .line 162
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->setSyncActionBarButton()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/home/HomeActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->refreshFocusables()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/home/HomeActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 162
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/home/HomeActivity;->setSyncProgressbarStatus(Z)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/home/HomeActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->simplePopupController:Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/home/HomeActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->doNotShowCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/shealth/home/HomeActivity;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;
    .param p1, "x1"    # Landroid/widget/CheckBox;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->doNotShowCheckBox:Landroid/widget/CheckBox;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/home/HomeActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;

    .prologue
    .line 162
    iget v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->popupMode:I

    return v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/shealth/home/HomeActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;
    .param p1, "x1"    # I

    .prologue
    .line 162
    iput p1, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->popupMode:I

    return p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/home/HomeActivity;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mBurntCalories:[I

    return-object v0
.end method

.method static synthetic access$1402(Lcom/sec/android/app/shealth/home/HomeActivity;[I)[I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;
    .param p1, "x1"    # [I

    .prologue
    .line 162
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mBurntCalories:[I

    return-object p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/home/HomeActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/home/HomeActivity;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 162
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/home/HomeActivity;->addToScreenViewsList(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/home/HomeActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mContent:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/home/HomeActivity;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 162
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/home/HomeActivity;->addToScreenViewsList(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1900(Lcom/sec/android/app/shealth/home/HomeActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->refreshFocusables()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/home/HomeActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;

    .prologue
    .line 162
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->syncWearableDevice()V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/android/app/shealth/home/HomeActivity;)Lcom/sec/android/app/shealth/home/HomeWearableManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mHomeWearableManager:Lcom/sec/android/app/shealth/home/HomeWearableManager;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/shealth/home/HomeActivity;[I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;
    .param p1, "x1"    # [I

    .prologue
    .line 162
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/home/HomeActivity;->createBurntCaloriesPopup([I)V

    return-void
.end method

.method static synthetic access$2200(Lcom/sec/android/app/shealth/home/HomeActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 162
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/home/HomeActivity;->startUpdateActivity(Z)V

    return-void
.end method

.method static synthetic access$2300(Lcom/sec/android/app/shealth/home/HomeActivity;)Lcom/sec/android/app/shealth/settings/SHealthUpdation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->updateCheck:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/shealth/home/HomeActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mDoNotShowAgain:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/sec/android/app/shealth/home/HomeActivity;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;
    .param p1, "x1"    # Landroid/widget/CheckBox;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mDoNotShowAgain:Landroid/widget/CheckBox;

    return-object p1
.end method

.method static synthetic access$2500(Lcom/sec/android/app/shealth/home/HomeActivity;)Lcom/sec/android/app/shealth/common/utils/NetworkUtils;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/shealth/home/HomeActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->connectionWifiBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/android/app/shealth/home/HomeActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;

    .prologue
    .line 162
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->checkForUpdates()V

    return-void
.end method

.method static synthetic access$2800(Lcom/sec/android/app/shealth/home/HomeActivity;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->widgetIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/android/app/shealth/home/HomeActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->connectionBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/home/HomeActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mActivityContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/home/HomeActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mFeatureList:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/shealth/home/HomeActivity;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mFeatureList:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/home/HomeActivity;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 162
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/home/HomeActivity;->launchTargetWidgetOrNotification(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$600()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    sget-object v0, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/home/HomeActivity;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncTimeoutRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/home/HomeActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;

    .prologue
    .line 162
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncing:Z

    return v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/shealth/home/HomeActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 162
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncing:Z

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/home/HomeActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;

    .prologue
    .line 162
    iget v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncCounter:I

    return v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/shealth/home/HomeActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;
    .param p1, "x1"    # I

    .prologue
    .line 162
    iput p1, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncCounter:I

    return p1
.end method

.method static synthetic access$908(Lcom/sec/android/app/shealth/home/HomeActivity;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity;

    .prologue
    .line 162
    iget v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncCounter:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncCounter:I

    return v0
.end method

.method private checkDeleteDownloadApp()V
    .locals 11

    .prologue
    .line 1624
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string v7, "com.sec.android.app.shealth.plugins.sleepmonitor"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 1626
    .local v2, "info":Landroid/content/pm/PackageInfo;
    sget-object v6, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v7, "SleepMonitor installed"

    invoke-static {v6, v7}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1628
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string/jumbo v8, "remove_downloadable"

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v4

    .line 1629
    .local v4, "result":Landroid/os/Bundle;
    sget-object v6, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v7, "Send Delete SleepMonitor"

    invoke-static {v6, v7}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1631
    const-string v6, "is_success"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1633
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->showDeleteSleepPopup()Z

    move-result v6

    sput-boolean v6, Lcom/sec/android/app/shealth/home/HomeActivity;->isDeleteDownloadAppPopupShown:Z

    .line 1635
    invoke-static {p0}, Lcom/sec/android/app/shealth/home/favorite/AppRegistryUtil;->getPreloadedPluginsList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v3

    .line 1636
    .local v3, "preloadAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    .line 1637
    .local v5, "temp":Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;
    iget-object v6, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f09017e

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1638
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->packagename:Ljava/lang/String;

    const/4 v8, 0x1

    iget v9, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->pluginId:I

    invoke-static {v6, v7, v8, v9}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->updateAppFavoriteStatus(Landroid/content/Context;Ljava/lang/String;II)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1646
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "info":Landroid/content/pm/PackageInfo;
    .end local v3    # "preloadAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    .end local v4    # "result":Landroid/os/Bundle;
    .end local v5    # "temp":Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;
    :catch_0
    move-exception v0

    .line 1647
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 1648
    sget-object v6, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v7, "SleepMonitor didn\'t install"

    invoke-static {v6, v7}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1650
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    :goto_1
    return-void

    .line 1643
    .restart local v2    # "info":Landroid/content/pm/PackageInfo;
    .restart local v4    # "result":Landroid/os/Bundle;
    :cond_2
    :try_start_1
    sget-object v6, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Delete error : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "detail_message"

    invoke-virtual {v4, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method private checkForUpdates()V
    .locals 3

    .prologue
    .line 1596
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isLaterButtonClickedOnAppUpdatePopup(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1597
    new-instance v1, Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->updateCheck:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    .line 1599
    new-instance v0, Lcom/sec/android/app/shealth/home/HomeActivity$12;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/home/HomeActivity$12;-><init>(Lcom/sec/android/app/shealth/home/HomeActivity;)V

    .line 1611
    .local v0, "onAppUpdateListener":Lcom/sec/android/app/shealth/settings/SHealthUpdation$OnAppUpdateListener;
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->updateCheck:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->registerAppUpdateListener(Lcom/sec/android/app/shealth/settings/SHealthUpdation$OnAppUpdateListener;)V

    .line 1613
    .end local v0    # "onAppUpdateListener":Lcom/sec/android/app/shealth/settings/SHealthUpdation$OnAppUpdateListener;
    :cond_0
    return-void
.end method

.method private connectedWearableDevice()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1718
    sget-object v0, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "prepareListDialogItem"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1719
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->getLoadUserDeviceName()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mDeviceItemList:Ljava/util/ArrayList;

    .line 1721
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mDeviceItemList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1722
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mDeviceItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 1723
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mDeviceItemList:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;->deviceName:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->showDevicePopup(Ljava/lang/String;)V

    .line 1727
    :cond_0
    :goto_0
    return-void

    .line 1724
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mDeviceItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v2, :cond_0

    .line 1725
    const v0, 0x7f090bbf

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->showDevicePopup(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static copyFile(Ljava/io/File;Ljava/io/File;)Z
    .locals 4
    .param p0, "srcFile"    # Ljava/io/File;
    .param p1, "destFile"    # Ljava/io/File;

    .prologue
    .line 1810
    const/4 v2, 0x0

    .line 1812
    .local v2, "result":Z
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1814
    .local v1, "in":Ljava/io/InputStream;
    :try_start_1
    invoke-static {v1, p1}, Lcom/sec/android/app/shealth/home/HomeActivity;->copyToFile(Ljava/io/InputStream;Ljava/io/File;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    .line 1816
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 1822
    .end local v1    # "in":Ljava/io/InputStream;
    :goto_0
    return v2

    .line 1816
    .restart local v1    # "in":Ljava/io/InputStream;
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v3
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1818
    .end local v1    # "in":Ljava/io/InputStream;
    :catch_0
    move-exception v0

    .line 1819
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 1820
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static copyToFile(Ljava/io/InputStream;Ljava/io/File;)Z
    .locals 6
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .param p1, "destFile"    # Ljava/io/File;

    .prologue
    const/4 v4, 0x0

    .line 1826
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1828
    .local v3, "out":Ljava/io/OutputStream;
    const/16 v5, 0x1000

    :try_start_1
    new-array v0, v5, [B

    .line 1830
    .local v0, "buffer":[B
    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .local v1, "bytesRead":I
    if-ltz v1, :cond_0

    .line 1831
    const/4 v5, 0x0

    invoke-virtual {v3, v0, v5, v1}, Ljava/io/OutputStream;->write([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1834
    .end local v0    # "buffer":[B
    .end local v1    # "bytesRead":I
    :catchall_0
    move-exception v5

    :try_start_2
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V

    throw v5
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1837
    .end local v3    # "out":Ljava/io/OutputStream;
    :catch_0
    move-exception v2

    .line 1838
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 1839
    .end local v2    # "e":Ljava/io/IOException;
    :goto_1
    return v4

    .line 1834
    .restart local v0    # "buffer":[B
    .restart local v1    # "bytesRead":I
    .restart local v3    # "out":Ljava/io/OutputStream;
    :cond_0
    :try_start_3
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 1836
    const/4 v4, 0x1

    goto :goto_1
.end method

.method private createBurntCaloriesPopup([I)V
    .locals 3
    .param p1, "cal"    # [I

    .prologue
    .line 1868
    if-nez p1, :cond_0

    .line 1881
    :goto_0
    return-void

    .line 1871
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f03012b

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090a19

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/home/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/home/HomeActivity$15;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/home/HomeActivity$15;-><init>(Lcom/sec/android/app/shealth/home/HomeActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->burntCaloriesPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1880
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->burntCaloriesPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "burnt_calories"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private createDiaglog()V
    .locals 5

    .prologue
    const v4, 0x7f090047

    const v3, 0x7f0300a3

    const/4 v2, 0x2

    .line 1906
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f0907a1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->backPressListenernew:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->connectionBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1908
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f0907a2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->backPressListenernew:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->connectionWifiBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1910
    return-void
.end method

.method private deleteDownloadAppIntent(Ljava/util/ArrayList;)Landroid/content/Intent;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 1653
    .local p1, "arr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 1654
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1655
    .local v2, "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v3, "com.sec.android.app.shealth.plugins.sleepmonitor"

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1656
    move-object p1, v2

    .line 1658
    .end local v2    # "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    move-object v1, v3

    check-cast v1, [Ljava/lang/String;

    .line 1660
    .local v1, "package_name":[Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.sec.shealth.action.DELETE_DOWNLOAD_APPS"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1661
    .local v0, "intent":Landroid/content/Intent;
    const-string v3, "PACKAGE_NAME"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1663
    return-object v0
.end method

.method private enablePedometerStatus(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "pluginlist":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 2050
    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_3

    .line 2051
    const/4 v0, 0x0

    .line 2052
    .local v0, "counter":I
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v6, v4, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->packagename:Ljava/lang/String;

    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget v4, v4, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->pluginId:I

    invoke-static {v5, v6, v7, v4}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->updateEnabledStatus(Landroid/content/Context;Ljava/lang/String;ZI)I

    .line 2053
    invoke-static {}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->clear()V

    .line 2055
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v7, v4}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->getAllAppRegistryDatas(ZLandroid/content/Context;)Ljava/util/List;

    move-result-object v3

    .line 2056
    .local v3, "plugins":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    if-eqz v3, :cond_2

    .line 2057
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    .line 2058
    .local v2, "plugin":Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;
    iget v4, v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->isFavorite:I

    if-ne v4, v7, :cond_0

    .line 2059
    iget-object v4, v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->actions:Ljava/lang/String;

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->isUseCigna()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->actions:Ljava/lang/String;

    const-string v5, "com.sec.shealth.action.COACH"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2061
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->pluginId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->packagename:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->setOrder(Ljava/lang/String;I)V

    .line 2062
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2065
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "plugin":Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;
    :cond_2
    invoke-static {v7}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->setIsInitialised(Z)V

    .line 2066
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->setTotalFavoriteCount(I)V

    .line 2067
    invoke-static {}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->logPrefFile()V

    .line 2070
    .end local v0    # "counter":I
    .end local v3    # "plugins":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    :cond_3
    return-void
.end method

.method public static exportDataExist()Z
    .locals 1

    .prologue
    .line 2136
    sget-boolean v0, Lcom/sec/android/app/shealth/home/HomeActivity;->exportableData:Z

    return v0
.end method

.method private getAndSaveBackgroundImage(Landroid/graphics/Bitmap;II)V
    .locals 1
    .param p1, "source"    # Landroid/graphics/Bitmap;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 1510
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/GalleryUtils;->getDefaultHealthBoardBackgroundPortraitFilePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->saveImageToCacheInSdCard(Landroid/graphics/Bitmap;Ljava/lang/String;)Z

    .line 1511
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/GalleryUtils;->getDefaultHealthBoardBackgroundFilePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/FolderUtils;->deleteFile(Ljava/lang/String;)Z

    .line 1512
    return-void
.end method

.method private getDataCount(Ljava/lang/String;Landroid/net/Uri;)I
    .locals 8
    .param p1, "COLUMNS_ID"    # Ljava/lang/String;
    .param p2, "CONTENT_URI"    # Landroid/net/Uri;

    .prologue
    .line 2079
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v2, v0

    .line 2080
    .local v2, "projection":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 2081
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 2083
    .local v7, "ret":I
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2084
    if-eqz v6, :cond_0

    .line 2085
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 2088
    :cond_0
    if-eqz v6, :cond_1

    .line 2089
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2092
    :cond_1
    return v7

    .line 2088
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 2089
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private getImageFile(Landroid/net/Uri;)Ljava/io/File;
    .locals 15
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1730
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_data"

    aput-object v1, v2, v0

    .line 1731
    .local v2, "projection":[Ljava/lang/String;
    if-nez p1, :cond_0

    .line 1732
    sget-object p1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 1735
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "content://com.google.android.apps.photos.content"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1736
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/shealth/home/HomeActivity;->getGooglePhotosURL(Landroid/net/Uri;)V

    .line 1737
    const/4 v0, 0x0

    .line 1775
    :goto_0
    return-object v0

    .line 1740
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "content://com.android.providers.media.documents/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1741
    invoke-static/range {p1 .. p1}, Landroid/provider/DocumentsContract;->getDocumentId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v14

    .line 1742
    .local v14, "wholeID":Ljava/lang/String;
    const-string v11, ""

    .line 1743
    .local v11, "id":Ljava/lang/String;
    if-eqz v14, :cond_2

    .line 1745
    const-string v0, ":"

    invoke-virtual {v14, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v11, v0, v1

    .line 1747
    :cond_2
    const-string v3, "_id=?"

    .line 1749
    .local v3, "selection":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v11, v4, v5

    const-string v5, "date_modified desc"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 1755
    .end local v3    # "selection":Ljava/lang/String;
    .end local v11    # "id":Ljava/lang/String;
    .end local v14    # "wholeID":Ljava/lang/String;
    .local v12, "mCursor":Landroid/database/Cursor;
    :goto_1
    if-nez v12, :cond_4

    .line 1756
    new-instance v0, Ljava/io/File;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 1753
    .end local v12    # "mCursor":Landroid/database/Cursor;
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "date_modified desc"

    move-object/from16 v5, p1

    move-object v6, v2

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .restart local v12    # "mCursor":Landroid/database/Cursor;
    goto :goto_1

    .line 1757
    :cond_4
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_5

    .line 1758
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 1759
    const/4 v12, 0x0

    .line 1760
    const/4 v0, 0x0

    goto :goto_0

    .line 1762
    :cond_5
    const-string v0, "_data"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    .line 1763
    .local v10, "column_index":I
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1765
    invoke-interface {v12, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 1767
    .local v13, "path":Ljava/lang/String;
    if-eqz v12, :cond_6

    .line 1768
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 1769
    const/4 v12, 0x0

    .line 1771
    :cond_6
    invoke-static {v13}, Lcom/sec/android/app/shealth/common/utils/Utils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1772
    sget-object v0, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v1, "File path is empty"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1773
    const/4 v0, 0x0

    goto :goto_0

    .line 1775
    :cond_7
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private goToSettingActivity()V
    .locals 2

    .prologue
    .line 1496
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1497
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.shealth"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1498
    const-string v1, "android.shealth.action.LAUNCH_SETTINGS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1499
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 1500
    return-void
.end method

.method private isGearDevice()Z
    .locals 1

    .prologue
    .line 1395
    const/4 v0, 0x0

    .line 1396
    .local v0, "checkForGear":Z
    return v0
.end method

.method private isServiceRunning(Ljava/lang/String;)Z
    .locals 5
    .param p1, "serviceClassName"    # Ljava/lang/String;

    .prologue
    .line 1851
    const-string v4, "activity"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/home/HomeActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 1852
    .local v0, "activityManager":Landroid/app/ActivityManager;
    const v4, 0x7fffffff

    invoke-virtual {v0, v4}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v3

    .line 1854
    .local v3, "services":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    if-eqz v3, :cond_1

    .line 1856
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 1858
    .local v2, "runningServiceInfo":Landroid/app/ActivityManager$RunningServiceInfo;
    iget-object v4, v2, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1860
    const/4 v4, 0x1

    .line 1864
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "runningServiceInfo":Landroid/app/ActivityManager$RunningServiceInfo;
    :goto_0
    return v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private killProcessAndRestart()V
    .locals 3

    .prologue
    .line 762
    sget-object v1, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v2, "killProcessAndRestart"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 764
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 765
    .local v0, "intent_splash":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 766
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 767
    const-string v1, "com.sec.android.app.shealth"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 768
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 769
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 771
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->finishAffinity()V

    .line 773
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-static {v1}, Landroid/os/Process;->killProcess(I)V

    .line 774
    return-void
.end method

.method private launchPasswordWithDelay(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 778
    const/4 v0, 0x0

    .line 780
    .local v0, "delayTime":I
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    const-string/jumbo v3, "security_pin_enabled"

    invoke-virtual {v2, p0, v3}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 782
    const/16 v0, 0x96

    .line 787
    :cond_0
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    .line 788
    .local v1, "notifyHandler":Landroid/os/Handler;
    new-instance v2, Lcom/sec/android/app/shealth/home/HomeActivity$4;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/shealth/home/HomeActivity$4;-><init>(Lcom/sec/android/app/shealth/home/HomeActivity;Landroid/content/Intent;)V

    int-to-long v3, v0

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 798
    return-void
.end method

.method private launchTargetWidgetOrNotification(Landroid/content/Intent;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    .line 805
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    if-nez v2, :cond_0

    .line 806
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    .line 807
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 808
    .local v1, "launcherintent":Landroid/content/Intent;
    const-string/jumbo v2, "widgetActivityAction"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 811
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 813
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 819
    :goto_0
    const-string/jumbo v2, "widgetActivityPackage"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 820
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 821
    sget-object v2, Lcom/sec/android/app/shealth/home/HomeActivity;->PLUG_IN_WIDGET_BORADCAST:Ljava/lang/String;

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 822
    const-string/jumbo v2, "widget"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 823
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 824
    const-string v2, "WidgetOrNotification"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "WidgetOrNotification received  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 826
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    const-string/jumbo v3, "security_pin_enabled"

    invoke-virtual {v2, p0, v3}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 828
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    const-string/jumbo v3, "request_password"

    invoke-virtual {v2, p0, v3, v5}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->saveBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 829
    sget-object v2, Lcom/sec/android/app/shealth/home/HomeActivity;->WIDGET_LAUNCH_PASSWORD:Ljava/lang/String;

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 832
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isChinaModel()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 833
    new-instance v2, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    .line 834
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->createDiaglog()V

    .line 835
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->widgetIntent:Landroid/content/Intent;

    .line 836
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isWifiEnabled()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isLaunchWifiPopupWasShown(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 837
    const/16 v2, 0x8

    iput v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->popupMode:I

    .line 838
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->connectionWifiBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "data_connection_dialog"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 848
    :goto_1
    return-void

    .line 817
    :cond_2
    const-string v2, "com.sec.shealth.action.STEALTH_MODE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 839
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isWifiEnabled()Z

    move-result v2

    if-nez v2, :cond_4

    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isLaunchMobilePopupWasShown(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 840
    const/4 v2, 0x6

    iput v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->popupMode:I

    .line 841
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->connectionBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "data_connection_dialog"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1

    .line 843
    :cond_4
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/home/HomeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 846
    :cond_5
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/home/HomeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method private registerSystemCloseDialogReceiver()V
    .locals 3

    .prologue
    .line 2025
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->systemCloseDialogReceiver:Lcom/sec/android/app/shealth/home/HomeActivity$SystemCloseDialogReceiver;

    if-nez v1, :cond_0

    .line 2027
    new-instance v1, Lcom/sec/android/app/shealth/home/HomeActivity$SystemCloseDialogReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/home/HomeActivity$SystemCloseDialogReceiver;-><init>(Lcom/sec/android/app/shealth/home/HomeActivity$1;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->systemCloseDialogReceiver:Lcom/sec/android/app/shealth/home/HomeActivity$SystemCloseDialogReceiver;

    .line 2028
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 2029
    .local v0, "dialogFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2030
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->systemCloseDialogReceiver:Lcom/sec/android/app/shealth/home/HomeActivity$SystemCloseDialogReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2033
    .end local v0    # "dialogFilter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private setActivityBackground()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 1108
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 1109
    .local v0, "display":Landroid/view/Display;
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 1110
    .local v2, "size":Landroid/graphics/Point;
    invoke-virtual {v0, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 1112
    sget-object v4, Lcom/sec/android/app/shealth/home/HomeActivity;->mDrawableList:Ljava/util/ArrayList;

    if-nez v4, :cond_0

    .line 1113
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->setDrawableList()V

    .line 1116
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/GalleryUtils;->getDefaultHealthBoardBackgroundPortraitFilePath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/common/utils/FolderUtils;->isFileExist(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1118
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 1119
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    new-instance v5, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/GalleryUtils;->getDefaultHealthBoardBackgroundPortraitFilePath()Ljava/lang/String;

    move-result-object v7

    iget v8, v2, Landroid/graphics/Point;->x:I

    iget v9, v2, Landroid/graphics/Point;->y:I

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->getBitmapByPath(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v4, v5}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1133
    :cond_1
    :goto_0
    return-void

    .line 1121
    :catch_0
    move-exception v1

    .line 1122
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 1123
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget-object v4, Lcom/sec/android/app/shealth/home/HomeActivity;->mDrawableList:Ljava/util/ArrayList;

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 1126
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :cond_2
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getHomeWallpaperId(Landroid/content/Context;)I

    move-result v3

    .line 1127
    .local v3, "wallpaperId":I
    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    .line 1128
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget-object v4, Lcom/sec/android/app/shealth/home/HomeActivity;->mDrawableList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 1130
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget-object v4, Lcom/sec/android/app/shealth/home/HomeActivity;->mDrawableList:Ljava/util/ArrayList;

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private setBackgroundDialog(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;)V
    .locals 3
    .param p1, "builder"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .prologue
    .line 1503
    const v0, 0x7f0302b5

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1504
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1505
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setNeedRedraw(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1506
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "set_wallpaper_popup"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1507
    return-void
.end method

.method private setDrawableList()V
    .locals 2

    .prologue
    .line 1136
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/home/HomeActivity;->mDrawableList:Ljava/util/ArrayList;

    .line 1137
    sget-object v0, Lcom/sec/android/app/shealth/home/HomeActivity;->mDrawableList:Ljava/util/ArrayList;

    const v1, 0x7f0204a9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1138
    sget-object v0, Lcom/sec/android/app/shealth/home/HomeActivity;->mDrawableList:Ljava/util/ArrayList;

    const v1, 0x7f0204aa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1139
    sget-object v0, Lcom/sec/android/app/shealth/home/HomeActivity;->mDrawableList:Ljava/util/ArrayList;

    const v1, 0x7f0204ab

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1140
    return-void
.end method

.method private setHealthBoardWallpaper(I)V
    .locals 3
    .param p1, "wallpaperResId"    # I

    .prologue
    .line 1143
    sget-object v0, Lcom/sec/android/app/shealth/home/HomeActivity;->mDrawableList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1144
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->setDrawableList()V

    .line 1146
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget-object v0, Lcom/sec/android/app/shealth/home/HomeActivity;->mDrawableList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1147
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveHomeWallpaperId(Landroid/content/Context;I)V

    .line 1148
    return-void
.end method

.method private setSyncActionBarButton()V
    .locals 12

    .prologue
    const v11, 0x7f080303

    const/4 v10, -0x1

    const v9, 0x7f0900a5

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1196
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030135

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncButtonView:Landroid/view/View;

    .line 1197
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncButtonView:Landroid/view/View;

    const v3, 0x7f080521

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncButton:Landroid/widget/LinearLayout;

    .line 1198
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncButton:Landroid/widget/LinearLayout;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09020a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1199
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncButton:Landroid/widget/LinearLayout;

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09020a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 1201
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncButton:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    .line 1202
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncButtonView:Landroid/view/View;

    const v3, 0x7f080522

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncProgressImage:Landroid/view/View;

    .line 1203
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncButton:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v8}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 1205
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mHomeWearableManager:Lcom/sec/android/app/shealth/home/HomeWearableManager;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/HomeWearableManager;->checkConnectedWearable()I

    move-result v2

    if-ne v2, v8, :cond_1

    .line 1206
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v10, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 1207
    .local v1, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1208
    .local v0, "actionTitleLayout":Landroid/widget/LinearLayout;
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1209
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeCustomView()V

    .line 1210
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncButtonView:Landroid/view/View;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addCustomView(Landroid/view/View;)V

    .line 1211
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 1212
    iput-boolean v7, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->isCheckSeviceConnection:Z

    .line 1213
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncing:Z

    if-eqz v2, :cond_0

    .line 1214
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncButton:Landroid/widget/LinearLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1215
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncProgressImage:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1235
    .end local v0    # "actionTitleLayout":Landroid/widget/LinearLayout;
    .end local v1    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncButton:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/sec/android/app/shealth/home/HomeActivity$6;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/home/HomeActivity$6;-><init>(Lcom/sec/android/app/shealth/home/HomeActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1243
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncButton:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/sec/android/app/shealth/home/HomeActivity$7;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/home/HomeActivity$7;-><init>(Lcom/sec/android/app/shealth/home/HomeActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1252
    return-void

    .line 1217
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mHomeWearableManager:Lcom/sec/android/app/shealth/home/HomeWearableManager;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/HomeWearableManager;->checkConnectedWearable()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 1218
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v1, v7, v10, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 1219
    .restart local v1    # "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1220
    .restart local v0    # "actionTitleLayout":Landroid/widget/LinearLayout;
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1221
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeCustomView()V

    .line 1222
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 1223
    iput-boolean v7, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->isCheckSeviceConnection:Z

    goto :goto_0

    .line 1226
    .end local v0    # "actionTitleLayout":Landroid/widget/LinearLayout;
    .end local v1    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :cond_2
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->isCheckSeviceConnection:Z

    if-nez v2, :cond_3

    .line 1227
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mHomeWearableManager:Lcom/sec/android/app/shealth/home/HomeWearableManager;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/HomeWearableManager;->destroy()V

    .line 1228
    new-instance v2, Lcom/sec/android/app/shealth/home/HomeWearableManager;

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mActivityContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mDeviceFinderServiceListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/home/HomeWearableManager;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mHomeWearableManager:Lcom/sec/android/app/shealth/home/HomeWearableManager;

    .line 1229
    iput-boolean v8, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->isCheckSeviceConnection:Z

    goto :goto_0

    .line 1231
    :cond_3
    iput-boolean v7, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->isCheckSeviceConnection:Z

    goto :goto_0
.end method

.method private setSyncProgressbarStatus(Z)V
    .locals 5
    .param p1, "isPlay"    # Z

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 1255
    sget-object v0, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setSyncProgressbarStatus isPlay : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1256
    if-eqz p1, :cond_0

    .line 1257
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1258
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncProgressImage:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1259
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncTimeoutRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1265
    :goto_0
    return-void

    .line 1261
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1262
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncButton:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 1263
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncProgressImage:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private showDeleteSleepPopup()Z
    .locals 3

    .prologue
    .line 1667
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f0907be

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090d6c

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "DELETE_SLEEP_POPUP"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1671
    const/4 v0, 0x1

    return v0
.end method

.method private showDevicePopup(Ljava/lang/String;)V
    .locals 5
    .param p1, "deviceName"    # Ljava/lang/String;

    .prologue
    const v4, 0x7f03012c

    .line 1675
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v2, 0x0

    const v3, 0x7f0900e3

    invoke-direct {v1, p0, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090047

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/home/HomeActivity$14;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/home/HomeActivity$14;-><init>(Lcom/sec/android/app/shealth/home/HomeActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/home/HomeActivity$13;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/shealth/home/HomeActivity$13;-><init>(Lcom/sec/android/app/shealth/home/HomeActivity;Ljava/lang/String;)V

    invoke-virtual {v1, v4, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    .line 1710
    .local v0, "b":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/app/shealth/home/HomeActivity;->isConncectDevicePopupShown:Z

    .line 1711
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "SHOW_WEARABLE_POPUP"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1712
    return-void
.end method

.method private showExportableFeatureDialog(Ljava/lang/String;)V
    .locals 3
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 602
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mExportableFeatureDialogBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 603
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mExportableFeatureDialogBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const v1, 0x7f0900e1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 604
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mExportableFeatureDialogBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const v1, 0x7f03018a

    new-instance v2, Lcom/sec/android/app/shealth/home/HomeActivity$3;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/shealth/home/HomeActivity$3;-><init>(Lcom/sec/android/app/shealth/home/HomeActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 658
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mExportableFeatureDialogBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mExportableFeatureDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 659
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mExportableFeatureDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "notice_exportable_feature"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 660
    return-void
.end method

.method private startUpdateActivity(Z)V
    .locals 2
    .param p1, "isForced"    # Z

    .prologue
    .line 1616
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/UpdateCheckActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1617
    .local v0, "appUpdate":Landroid/content/Intent;
    const-string v1, "FORCE_UPDATE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1618
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 1619
    return-void
.end method

.method private syncWearableDevice()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 851
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/home/HomeActivity;->setSyncProgressbarStatus(Z)V

    .line 853
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncing:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mHomeWearableManager:Lcom/sec/android/app/shealth/home/HomeWearableManager;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/HomeWearableManager;->checkConnectedWearable()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 854
    sget-object v0, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "syncDevice start in HomeActivity"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 855
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mHomeWearableManager:Lcom/sec/android/app/shealth/home/HomeWearableManager;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/HomeWearableManager;->sendSyncIntent()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncing:Z

    .line 858
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSummayFrag:Lcom/sec/android/app/shealth/home/HomeFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/HomeFragment;->getHomeLatestData()Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncing:Z

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setReadyForSync(Z)V

    .line 859
    return-void
.end method

.method private unregisterSystemCloseDialogReceiver()V
    .locals 1

    .prologue
    .line 2040
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->systemCloseDialogReceiver:Lcom/sec/android/app/shealth/home/HomeActivity$SystemCloseDialogReceiver;

    if-eqz v0, :cond_0

    .line 2042
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->systemCloseDialogReceiver:Lcom/sec/android/app/shealth/home/HomeActivity$SystemCloseDialogReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2043
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->systemCloseDialogReceiver:Lcom/sec/android/app/shealth/home/HomeActivity$SystemCloseDialogReceiver;

    .line 2045
    :cond_0
    return-void
.end method


# virtual methods
.method public callTermsAndConditions()V
    .locals 3

    .prologue
    .line 301
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isTermsAndConditionsOf355Accepted(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->getMigrationState(Landroid/content/Context;)I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->getUpgradeStatus(Landroid/content/Context;)Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    move-result-object v1

    sget-object v2, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_APP_UPGRADE_DONE:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    if-ne v1, v2, :cond_0

    .line 311
    sget-object v1, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v2, " [35XTO355] This is upgrade from 3.5.X"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 313
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "isUpgradeFrom35x"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 314
    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 315
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 317
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public checkExportableDataExist()Z
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2102
    const-string v6, "_id"

    sget-object v7, Lcom/samsung/android/sdk/health/content/ShealthContract$Stress;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/shealth/home/HomeActivity;->getDataCount(Ljava/lang/String;Landroid/net/Uri;)I

    move-result v6

    if-lez v6, :cond_7

    move v3, v4

    .line 2103
    .local v3, "stress":Z
    :goto_0
    const-string v6, "_id"

    sget-object v7, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRate;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/shealth/home/HomeActivity;->getDataCount(Ljava/lang/String;Landroid/net/Uri;)I

    move-result v6

    if-lez v6, :cond_8

    move v2, v4

    .line 2104
    .local v2, "heartRate":Z
    :goto_1
    const-string v6, "_id"

    sget-object v7, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodPressure;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/shealth/home/HomeActivity;->getDataCount(Ljava/lang/String;Landroid/net/Uri;)I

    move-result v6

    if-lez v6, :cond_9

    move v1, v4

    .line 2105
    .local v1, "bloodPressure":Z
    :goto_2
    const-string v6, "_id"

    sget-object v7, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodGlucose;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/shealth/home/HomeActivity;->getDataCount(Ljava/lang/String;Landroid/net/Uri;)I

    move-result v6

    if-lez v6, :cond_a

    move v0, v4

    .line 2107
    .local v0, "bloodGlucose":Z
    :goto_3
    sget-object v6, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->Stress:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v6}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v6

    sget-object v7, Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;

    if-eq v6, v7, :cond_0

    .line 2108
    const/4 v3, 0x0

    .line 2110
    :cond_0
    sget-object v6, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->HeartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v6}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v6

    sget-object v7, Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    if-eq v6, v7, :cond_1

    .line 2111
    const/4 v2, 0x0

    .line 2114
    :cond_1
    sget-object v6, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[EXPORT] stress : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2115
    sget-object v6, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[EXPORT] heartRate : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2116
    sget-object v6, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[EXPORT] bloodPressure : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2117
    sget-object v6, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[EXPORT] bloodGlucose : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2119
    if-nez v3, :cond_2

    if-nez v2, :cond_2

    if-nez v1, :cond_2

    if-eqz v0, :cond_b

    :cond_2
    move v6, v4

    :goto_4
    sput-boolean v6, Lcom/sec/android/app/shealth/home/HomeActivity;->exportableData:Z

    .line 2121
    if-nez v0, :cond_3

    if-eqz v1, :cond_c

    :cond_3
    move v6, v4

    :goto_5
    sput-boolean v6, Lcom/sec/android/app/shealth/home/HomeActivity;->sBPBGData:Z

    .line 2122
    if-nez v3, :cond_4

    if-eqz v2, :cond_d

    :cond_4
    move v6, v4

    :goto_6
    sput-boolean v6, Lcom/sec/android/app/shealth/home/HomeActivity;->sHRStressData:Z

    .line 2124
    sget-boolean v6, Lcom/sec/android/app/shealth/home/HomeActivity;->sBPBGData:Z

    if-eqz v6, :cond_e

    sget-boolean v6, Lcom/sec/android/app/shealth/home/HomeActivity;->sHRStressData:Z

    if-eqz v6, :cond_e

    move v6, v4

    :goto_7
    sput-boolean v6, Lcom/sec/android/app/shealth/home/HomeActivity;->sAllDataPresent:Z

    .line 2125
    sget-boolean v6, Lcom/sec/android/app/shealth/home/HomeActivity;->sBPBGData:Z

    if-nez v6, :cond_5

    sget-boolean v6, Lcom/sec/android/app/shealth/home/HomeActivity;->sHRStressData:Z

    if-eqz v6, :cond_6

    :cond_5
    move v5, v4

    :cond_6
    sput-boolean v5, Lcom/sec/android/app/shealth/home/HomeActivity;->exportableData:Z

    .line 2127
    sget-object v4, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[EXPORT] getDataCount(StressColumns._ID, Stress.CONTENT_URI) : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_id"

    sget-object v7, Lcom/samsung/android/sdk/health/content/ShealthContract$Stress;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/shealth/home/HomeActivity;->getDataCount(Ljava/lang/String;Landroid/net/Uri;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2128
    sget-object v4, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[EXPORT] getDataCount(BloodGlucoseColumns._ID, BloodGlucose.CONTENT_URI) : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_id"

    sget-object v7, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodGlucose;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/shealth/home/HomeActivity;->getDataCount(Ljava/lang/String;Landroid/net/Uri;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2129
    sget-object v4, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[EXPORT] getDataCount(BloodPressureColumns._ID, BloodPressure.CONTENT_URI) : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_id"

    sget-object v7, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodPressure;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/shealth/home/HomeActivity;->getDataCount(Ljava/lang/String;Landroid/net/Uri;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2130
    sget-object v4, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[EXPORT] getDataCount(HeartRateColumns._ID, HeartRate.CONTENT_URI) : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_id"

    sget-object v7, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRate;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/shealth/home/HomeActivity;->getDataCount(Ljava/lang/String;Landroid/net/Uri;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2131
    sget-object v4, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[EXPORT] exportableData : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-boolean v6, Lcom/sec/android/app/shealth/home/HomeActivity;->exportableData:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2132
    sget-boolean v4, Lcom/sec/android/app/shealth/home/HomeActivity;->exportableData:Z

    return v4

    .end local v0    # "bloodGlucose":Z
    .end local v1    # "bloodPressure":Z
    .end local v2    # "heartRate":Z
    .end local v3    # "stress":Z
    :cond_7
    move v3, v5

    .line 2102
    goto/16 :goto_0

    .restart local v3    # "stress":Z
    :cond_8
    move v2, v5

    .line 2103
    goto/16 :goto_1

    .restart local v2    # "heartRate":Z
    :cond_9
    move v1, v5

    .line 2104
    goto/16 :goto_2

    .restart local v1    # "bloodPressure":Z
    :cond_a
    move v0, v5

    .line 2105
    goto/16 :goto_3

    .restart local v0    # "bloodGlucose":Z
    :cond_b
    move v6, v5

    .line 2119
    goto/16 :goto_4

    :cond_c
    move v6, v5

    .line 2121
    goto/16 :goto_5

    :cond_d
    move v6, v5

    .line 2122
    goto/16 :goto_6

    :cond_e
    move v6, v5

    .line 2124
    goto/16 :goto_7
.end method

.method public closeScreen()V
    .locals 0

    .prologue
    .line 1175
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->onBackPressed()V

    .line 1176
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->closeScreen()V

    .line 1177
    return-void
.end method

.method protected customizeActionBar()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1181
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->customizeActionBar()V

    .line 1182
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->homeMenu:Landroid/view/Menu;

    if-eqz v0, :cond_0

    .line 1183
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->homeMenu:Landroid/view/Menu;

    invoke-interface {v0, v2, v1}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 1186
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 1187
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarListener(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$ActionBarListener;Z)V

    .line 1188
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f020852

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarUpButton(I)V

    .line 1189
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f090019

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 1190
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 1191
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->TRANSPARENT_DRAWABLE:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1192
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 702
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSummayFrag:Lcom/sec/android/app/shealth/home/HomeFragment;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSummayFrag:Lcom/sec/android/app/shealth/home/HomeFragment;

    instance-of v2, v2, Lcom/sec/android/app/shealth/home/HomeFragment;

    if-eqz v2, :cond_0

    .line 704
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSummayFrag:Lcom/sec/android/app/shealth/home/HomeFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/HomeFragment;->getPluginListCount()I

    move-result v1

    .line 705
    .local v1, "numberOFChild":I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    const/16 v3, 0x16

    if-ne v2, v3, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    .line 707
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 708
    .local v0, "currentView":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    add-int/lit8 v3, v1, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 710
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 711
    const/4 v2, 0x1

    .line 716
    .end local v0    # "currentView":Landroid/view/View;
    .end local v1    # "numberOFChild":I
    :goto_0
    return v2

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v2

    goto :goto_0
.end method

.method public getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    .locals 3
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 1269
    const-string/jumbo v2, "set_wallpaper_popup"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1270
    new-instance v1, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener;-><init>()V

    .line 1361
    :goto_0
    return-object v1

    .line 1271
    :cond_0
    const-string v2, "data_connection_dialog"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1272
    new-instance v1, Lcom/sec/android/app/shealth/home/HomeActivity$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/home/HomeActivity$8;-><init>(Lcom/sec/android/app/shealth/home/HomeActivity;)V

    .line 1311
    .local v1, "contentInitializationListenerforDataFail":Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    goto :goto_0

    .line 1312
    .end local v1    # "contentInitializationListenerforDataFail":Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    :cond_1
    const-string v2, "burnt_calories"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1313
    new-instance v0, Lcom/sec/android/app/shealth/home/HomeActivity$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/home/HomeActivity$9;-><init>(Lcom/sec/android/app/shealth/home/HomeActivity;)V

    .local v0, "contentInitializationListenerforBurntCalories":Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    move-object v1, v0

    .line 1359
    goto :goto_0

    .line 1361
    .end local v0    # "contentInitializationListenerforBurntCalories":Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    :cond_2
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

    move-result-object v1

    goto :goto_0
.end method

.method public getCroppedBitmap(Landroid/content/Context;Landroid/net/Uri;II)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uriFrom"    # Landroid/net/Uri;
    .param p3, "photoHeight"    # I
    .param p4, "photoWidth"    # I

    .prologue
    .line 1515
    const/4 v0, 0x0

    .line 1516
    .local v0, "bitmapToSave":Landroid/graphics/Bitmap;
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1517
    :cond_0
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p3, p4, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1539
    :goto_0
    return-object v0

    .line 1519
    :cond_1
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1520
    .local v3, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v4, 0x1

    iput v4, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 1521
    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v3}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 1522
    const/4 v2, 0x0

    .line 1524
    .local v2, "fileDescriptor":Landroid/content/res/AssetFileDescriptor;
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string/jumbo v5, "r"

    invoke-virtual {v4, p2, v5}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1529
    if-eqz v2, :cond_2

    .line 1530
    :try_start_1
    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v5, v3}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1531
    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1537
    :cond_2
    :goto_1
    invoke-static {v0, p3, p4}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->cropAndScaleBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 1533
    :catch_0
    move-exception v1

    .line 1534
    .local v1, "e":Ljava/io/IOException;
    sget-object v4, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    invoke-static {v4, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1525
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 1526
    .local v1, "e":Ljava/io/FileNotFoundException;
    :try_start_2
    sget-object v4, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    invoke-static {v4, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1529
    if-eqz v2, :cond_2

    .line 1530
    :try_start_3
    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v5, v3}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1531
    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 1533
    :catch_2
    move-exception v1

    .line 1534
    .local v1, "e":Ljava/io/IOException;
    sget-object v4, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    invoke-static {v4, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1528
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 1529
    if-eqz v2, :cond_3

    .line 1530
    :try_start_4
    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v5, v6, v3}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1531
    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 1535
    :cond_3
    :goto_2
    throw v4

    .line 1533
    :catch_3
    move-exception v1

    .line 1534
    .restart local v1    # "e":Ljava/io/IOException;
    sget-object v5, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    invoke-static {v5, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method public getCroppedBitmap(Landroid/content/Context;Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pathFrom"    # Ljava/lang/String;
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 1543
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1544
    .local v0, "fileFrom":Ljava/io/File;
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 1545
    .local v1, "uriFrom":Landroid/net/Uri;
    invoke-virtual {p0, p1, v1, p3, p4}, Lcom/sec/android/app/shealth/home/HomeActivity;->getCroppedBitmap(Landroid/content/Context;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v2

    return-object v2
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 4
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 1922
    sget-object v1, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dialogTag: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1924
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->restoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    if-eqz v1, :cond_0

    .line 1926
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->restoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    move-result-object v0

    .line 1927
    .local v0, "buttonController":Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    if-eqz v0, :cond_0

    .line 1974
    .end local v0    # "buttonController":Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    :goto_0
    return-object v0

    .line 1933
    :cond_0
    const-string v1, "data_connection_dialog"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1934
    new-instance v0, Lcom/sec/android/app/shealth/home/HomeActivity$17;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/home/HomeActivity$17;-><init>(Lcom/sec/android/app/shealth/home/HomeActivity;)V

    goto :goto_0

    .line 1974
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGooglePhotosURL(Landroid/net/Uri;)V
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 1781
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "/https"

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1782
    .local v4, "splitArray":[Ljava/lang/String;
    aget-object v6, v4, v9

    aget-object v7, v4, v9

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 1784
    .local v2, "isVideo":I
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "https://"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "http://"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1785
    :cond_0
    if-nez v2, :cond_3

    .line 1786
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v6

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 1787
    .local v0, "display":Landroid/view/Display;
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    .line 1788
    .local v3, "size":Landroid/graphics/Point;
    invoke-virtual {v0, v3}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 1789
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/GalleryUtils;->getDefaultHealthBoardBackgroundFilePath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/common/utils/GalleryUtils;->getTempUriFromString(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 1790
    .local v5, "tempImageUri":Landroid/net/Uri;
    if-nez v5, :cond_1

    .line 1791
    new-instance v6, Ljava/io/File;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/GalleryUtils;->getDefaultHealthBoardBackgroundFilePath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    .line 1794
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v6, "com.android.camera.action.CROP"

    invoke-direct {v1, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1795
    .local v1, "intent":Landroid/content/Intent;
    const-string v6, "image/*"

    invoke-virtual {v1, p1, v6}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 1796
    const-string/jumbo v6, "output"

    invoke-virtual {v1, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1797
    const-string v6, "aspectX"

    iget v7, v3, Landroid/graphics/Point;->x:I

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1798
    const-string v6, "aspectY"

    iget v7, v3, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1799
    const-string/jumbo v6, "scale"

    invoke-virtual {v1, v6, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1800
    const-string/jumbo v6, "noFaceDetection"

    invoke-virtual {v1, v6, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1801
    const/4 v6, 0x4

    invoke-virtual {p0, v1, v6}, Lcom/sec/android/app/shealth/home/HomeActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1802
    iget-object v6, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-virtual {v6, v8}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->putPinWallPaper(Z)V

    .line 1807
    .end local v0    # "display":Landroid/view/Display;
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v3    # "size":Landroid/graphics/Point;
    .end local v5    # "tempImageUri":Landroid/net/Uri;
    :cond_2
    :goto_0
    return-void

    .line 1804
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090990

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v9}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public hideDrawMenu()V
    .locals 2

    .prologue
    .line 1170
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->TRANSPARENT_DRAWABLE:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1171
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->hideDrawMenu()V

    .line 1172
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 16
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 989
    if-nez p2, :cond_1

    .line 1105
    :cond_0
    :goto_0
    return-void

    .line 993
    :cond_1
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 995
    :sswitch_0
    if-eqz p3, :cond_0

    .line 996
    const-string/jumbo v13, "wallpaper_resource_id"

    const/4 v14, -0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    .line 997
    .local v11, "wallpaperResId":I
    const/4 v13, -0x1

    if-eq v11, v13, :cond_0

    .line 998
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/sec/android/app/shealth/home/HomeActivity;->setHealthBoardWallpaper(I)V

    .line 1000
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/GalleryUtils;->getDefaultHealthBoardBackgroundPortraitFilePath()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/sec/android/app/shealth/common/utils/FolderUtils;->isFileExist(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 1001
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/GalleryUtils;->getDefaultHealthBoardBackgroundPortraitFilePath()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/sec/android/app/shealth/common/utils/FolderUtils;->deleteFile(Ljava/lang/String;)Z

    .line 1003
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    const-string v14, "com.sec.android.app.shealth"

    const-string v15, "H001"

    invoke-static {v13, v14, v15}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1010
    .end local v11    # "wallpaperResId":I
    :sswitch_1
    if-eqz p3, :cond_0

    .line 1012
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/app/shealth/home/HomeActivity;->getImageFile(Landroid/net/Uri;)Ljava/io/File;

    move-result-object v8

    .line 1013
    .local v8, "original_file":Ljava/io/File;
    if-eqz v8, :cond_0

    .line 1016
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v13

    invoke-interface {v13}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    .line 1017
    .local v4, "display":Landroid/view/Display;
    new-instance v9, Landroid/graphics/Point;

    invoke-direct {v9}, Landroid/graphics/Point;-><init>()V

    .line 1018
    .local v9, "size":Landroid/graphics/Point;
    invoke-virtual {v4, v9}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 1019
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/GalleryUtils;->getDefaultHealthBoardBackgroundFilePath()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/sec/android/app/shealth/common/utils/GalleryUtils;->getTempUriFromString(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    .line 1020
    .local v10, "tempImageUri":Landroid/net/Uri;
    if-nez v10, :cond_3

    .line 1021
    new-instance v13, Ljava/io/File;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/GalleryUtils;->getDefaultHealthBoardBackgroundFilePath()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v13}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v10

    .line 1023
    :cond_3
    new-instance v3, Ljava/io/File;

    invoke-virtual {v10}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v3, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1024
    .local v3, "copy_file":Ljava/io/File;
    invoke-static {v8, v3}, Lcom/sec/android/app/shealth/home/HomeActivity;->copyFile(Ljava/io/File;Ljava/io/File;)Z

    .line 1026
    new-instance v6, Landroid/content/Intent;

    const-string v13, "com.android.camera.action.CROP"

    invoke-direct {v6, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1027
    .local v6, "intent":Landroid/content/Intent;
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v13

    const-string v14, "image/*"

    invoke-virtual {v6, v13, v14}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 1028
    const-string/jumbo v13, "output"

    invoke-virtual {v6, v13, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1029
    const-string v13, "aspectX"

    iget v14, v9, Landroid/graphics/Point;->x:I

    invoke-virtual {v6, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1030
    const-string v13, "aspectY"

    iget v14, v9, Landroid/graphics/Point;->y:I

    invoke-virtual {v6, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1031
    const-string/jumbo v13, "scale"

    const/4 v14, 0x1

    invoke-virtual {v6, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1032
    const-string/jumbo v13, "set-as-image"

    const/4 v14, 0x1

    invoke-virtual {v6, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1033
    const-string/jumbo v13, "noFaceDetection"

    const/4 v14, 0x1

    invoke-virtual {v6, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1034
    const/4 v13, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v13}, Lcom/sec/android/app/shealth/home/HomeActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1039
    sget-boolean v13, Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService;->isPasswordServiceReceived:Z

    if-nez v13, :cond_0

    .line 1041
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->putPinWallPaper(Z)V

    goto/16 :goto_0

    .line 1048
    .end local v3    # "copy_file":Ljava/io/File;
    .end local v4    # "display":Landroid/view/Display;
    .end local v6    # "intent":Landroid/content/Intent;
    .end local v8    # "original_file":Ljava/io/File;
    .end local v9    # "size":Landroid/graphics/Point;
    .end local v10    # "tempImageUri":Landroid/net/Uri;
    :sswitch_2
    if-eqz p3, :cond_0

    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v13

    if-eqz v13, :cond_0

    .line 1052
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v13

    invoke-interface {v13}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    .line 1053
    .restart local v4    # "display":Landroid/view/Display;
    new-instance v9, Landroid/graphics/Point;

    invoke-direct {v9}, Landroid/graphics/Point;-><init>()V

    .line 1054
    .restart local v9    # "size":Landroid/graphics/Point;
    invoke-virtual {v4, v9}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 1056
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/GalleryUtils;->getDefaultHealthBoardBackgroundFilePath()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/sec/android/app/shealth/common/utils/GalleryUtils;->getTempUriFromString(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    .line 1057
    .restart local v10    # "tempImageUri":Landroid/net/Uri;
    if-nez v10, :cond_4

    .line 1058
    new-instance v13, Ljava/io/File;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/GalleryUtils;->getDefaultHealthBoardBackgroundFilePath()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v13}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v10

    .line 1061
    :cond_4
    new-instance v6, Landroid/content/Intent;

    const-string v13, "com.android.camera.action.CROP"

    invoke-direct {v6, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1062
    .restart local v6    # "intent":Landroid/content/Intent;
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v13

    const-string v14, "image/*"

    invoke-virtual {v6, v13, v14}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 1063
    const-string/jumbo v13, "output"

    invoke-virtual {v6, v13, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1064
    const-string v13, "aspectX"

    iget v14, v9, Landroid/graphics/Point;->x:I

    invoke-virtual {v6, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1065
    const-string v13, "aspectY"

    iget v14, v9, Landroid/graphics/Point;->y:I

    invoke-virtual {v6, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1066
    const-string/jumbo v13, "scale"

    const/4 v14, 0x1

    invoke-virtual {v6, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1067
    const-string/jumbo v13, "noFaceDetection"

    const/4 v14, 0x1

    invoke-virtual {v6, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1068
    const/4 v13, 0x5

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v13}, Lcom/sec/android/app/shealth/home/HomeActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1073
    sget-boolean v13, Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService;->isPasswordServiceReceived:Z

    if-nez v13, :cond_0

    .line 1075
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->putPinWallPaper(Z)V

    goto/16 :goto_0

    .line 1083
    .end local v4    # "display":Landroid/view/Display;
    .end local v6    # "intent":Landroid/content/Intent;
    .end local v9    # "size":Landroid/graphics/Point;
    .end local v10    # "tempImageUri":Landroid/net/Uri;
    :sswitch_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v13

    invoke-interface {v13}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    .line 1084
    .restart local v4    # "display":Landroid/view/Display;
    new-instance v9, Landroid/graphics/Point;

    invoke-direct {v9}, Landroid/graphics/Point;-><init>()V

    .line 1085
    .restart local v9    # "size":Landroid/graphics/Point;
    invoke-virtual {v4, v9}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 1086
    iget v13, v9, Landroid/graphics/Point;->x:I

    iget v14, v9, Landroid/graphics/Point;->y:I

    if-ge v13, v14, :cond_6

    iget v12, v9, Landroid/graphics/Point;->x:I

    .line 1087
    .local v12, "width":I
    :goto_1
    iget v13, v9, Landroid/graphics/Point;->x:I

    iget v14, v9, Landroid/graphics/Point;->y:I

    if-ge v13, v14, :cond_7

    iget v5, v9, Landroid/graphics/Point;->y:I

    .line 1089
    .local v5, "height":I
    :goto_2
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/GalleryUtils;->getDefaultHealthBoardBackgroundFilePath()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-virtual {v0, v1, v13, v12, v5}, Lcom/sec/android/app/shealth/home/HomeActivity;->getCroppedBitmap(Landroid/content/Context;Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 1090
    .local v7, "mPhoto":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v12, v5}, Lcom/sec/android/app/shealth/home/HomeActivity;->getAndSaveBackgroundImage(Landroid/graphics/Bitmap;II)V

    .line 1092
    if-eqz v7, :cond_5

    .line 1093
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    .line 1096
    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->setActivityBackground()V

    .line 1097
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    const-string v14, "com.sec.android.app.shealth"

    const-string v15, "H001"

    invoke-static {v13, v14, v15}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1086
    .end local v5    # "height":I
    .end local v7    # "mPhoto":Landroid/graphics/Bitmap;
    .end local v12    # "width":I
    :cond_6
    iget v12, v9, Landroid/graphics/Point;->y:I

    goto :goto_1

    .line 1087
    .restart local v12    # "width":I
    :cond_7
    iget v5, v9, Landroid/graphics/Point;->x:I

    goto :goto_2

    .line 1101
    .end local v4    # "display":Landroid/view/Display;
    .end local v9    # "size":Landroid/graphics/Point;
    .end local v12    # "width":I
    :sswitch_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/home/HomeActivity;->restoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    if-eqz v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/home/HomeActivity;->restoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    move/from16 v0, p1

    move/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v13, v0, v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->handleUserInteraction(IILandroid/content/Intent;)Z

    move-result v13

    if-eqz v13, :cond_0

    goto/16 :goto_0

    .line 993
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_1
        0x4 -> :sswitch_3
        0x5 -> :sswitch_3
        0x138a -> :sswitch_4
    .end sparse-switch
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 977
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "launchWidget"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 979
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "launchWidget"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 981
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "widgetActivityAction"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 983
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "widgetActivityAction"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 985
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->moveTaskToBack(Z)Z

    .line 986
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 18
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 321
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "[PERF] HomeActivity.onCreate() - START"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    const-string v13, "VerificationLog"

    const-string/jumbo v14, "onCreate"

    invoke-static {v13, v14}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    const v13, 0x7f0c001f

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/shealth/home/HomeActivity;->setTheme(I)V

    .line 324
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "[PERF] HomeActivity.onCreate() Themse Set- START"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getWindow()Landroid/view/Window;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 328
    sget-boolean v13, Lcom/sec/android/app/shealth/SHealthApplication;->isPedometerAvailabilityMismatch:Z

    if-eqz v13, :cond_0

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v13

    if-eqz v13, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->isMobilePedometerDisabled(Landroid/content/Context;)Z

    move-result v13

    if-nez v13, :cond_0

    .line 330
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "------enabling Pedometer"

    invoke-static {v13, v14}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f090020

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->getPluginRegistryDataFromName(Ljava/lang/String;Landroid/content/Context;)Ljava/util/List;

    move-result-object v8

    .line 332
    .local v8, "plugins":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/sec/android/app/shealth/home/HomeActivity;->enablePedometerStatus(Ljava/util/List;)V

    .line 333
    const/4 v13, 0x0

    sput-boolean v13, Lcom/sec/android/app/shealth/SHealthApplication;->isPedometerAvailabilityMismatch:Z

    .line 335
    .end local v8    # "plugins":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    :cond_0
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "[PERF] HomeActivity.onCreate() background null set - START"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    invoke-super/range {p0 .. p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 338
    if-eqz p1, :cond_1

    .line 340
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v13

    const-string v14, "CONFIRM_RESTORE_WITHOUT_CHECKING_SERVER"

    invoke-virtual {v13, v14}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v10

    check-cast v10, Landroid/support/v4/app/DialogFragment;

    .line 341
    .local v10, "restoreProgressDialog":Landroid/support/v4/app/DialogFragment;
    if-eqz v10, :cond_1

    .line 343
    invoke-virtual {v10}, Landroid/support/v4/app/DialogFragment;->dismissAllowingStateLoss()V

    .line 348
    .end local v10    # "restoreProgressDialog":Landroid/support/v4/app/DialogFragment;
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->callTermsAndConditions()V

    .line 353
    :try_start_0
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "[PERF] HomeActivity.onCreate() getContentResolver().call - START"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    sget-object v14, Lcom/samsung/android/sdk/health/cp/ThinCPConstants;->MIGRATION_AUTHORITY_URI:Landroid/net/Uri;

    const-string/jumbo v15, "rename_platform_db_if_exists"

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-virtual/range {v13 .. v17}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v11

    .line 355
    .local v11, "resultRename":Landroid/os/Bundle;
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "[PERF] HomeActivity.onCreate() getContentResolver().call - END"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    .line 361
    .end local v11    # "resultRename":Landroid/os/Bundle;
    :goto_0
    :try_start_1
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "[PERF] HomeActivity.onCreate() DataMigrationManager.getInstance().cleanUp - START"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    invoke-static {}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->getInstance()Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->cleanUp(Landroid/content/Context;)V

    .line 363
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "[PERF] HomeActivity.onCreate() DataMigrationManager.getInstance().cleanUp - END"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 367
    :goto_1
    :try_start_2
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "[PERF] HomeActivity.onCreate() KeyManager.getInstance().setInputPincodeBeforeOOBE - START"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lcom/sec/android/service/health/keyManager/KeyManager;->setInputPincodeBeforeOOBE(Z)V

    .line 369
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "[PERF] HomeActivity.onCreate() KeyManager.getInstance().setInputPincodeBeforeOOBE - END"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 372
    :goto_2
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "[PERF] HomeActivity.onCreate() KeyManager.getInstance().finishOOBE - START"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    invoke-static/range {p0 .. p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->createUpgradeStatus(Landroid/content/Context;)Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    .line 374
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/android/service/health/keyManager/KeyManager;->finishOOBE()V

    .line 375
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier;->unblockRestore()V

    .line 376
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "[PERF] HomeActivity.onCreate() KeyManager.getInstance().finishOOBE - END"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v13}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->setMigrationStatus(Landroid/content/Context;I)V

    .line 379
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "\t[PERF] HomeActivity.onCreate() Breakup 1- START"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isChinaModel()Z

    move-result v13

    if-eqz v13, :cond_d

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/shealth/home/HomeActivity;->popupMode:I

    if-eqz v13, :cond_d

    .line 382
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "China model and data popup showed - not run checkForUpdates"

    invoke-static {v13, v14}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    :goto_3
    if-eqz p1, :cond_2

    .line 389
    const-string/jumbo v13, "system_configuration_changed"

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSystemConfigurationChanged:Z

    .line 392
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->registerSystemCloseDialogReceiver()V

    .line 393
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "HomeActivity onCreate"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    .line 404
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/home/HomeActivity;->widgetLaunchAfterTCPPAcceptedIntent:Landroid/content/Intent;

    if-eqz v13, :cond_e

    .line 405
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "[35XTO355] calling widget intent "

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/home/HomeActivity;->widgetLaunchAfterTCPPAcceptedIntent:Landroid/content/Intent;

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/app/shealth/home/HomeActivity;->launchPasswordWithDelay(Landroid/content/Intent;)V

    .line 407
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/shealth/home/HomeActivity;->widgetLaunchAfterTCPPAcceptedIntent:Landroid/content/Intent;

    .line 446
    :cond_3
    :goto_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v13

    const-string v14, "burnt_calories"

    invoke-virtual {v13, v14}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v5

    check-cast v5, Landroid/support/v4/app/DialogFragment;

    .line 447
    .local v5, "dialogFragment":Landroid/support/v4/app/DialogFragment;
    if-eqz v5, :cond_4

    .line 449
    invoke-virtual {v5}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 452
    :cond_4
    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/home/HomeActivity;->setDrawerMenuListener(Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;)V

    .line 453
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "\t[PERF] HomeActivity.onCreate() Breakup 2 - START"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 455
    const-string v13, "com.sec.android.app.shealth.common.custom.base.PincodeResetService"

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/app/shealth/home/HomeActivity;->isServiceRunning(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_5

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    const-string/jumbo v14, "security_pin_enabled"

    move-object/from16 v0, p0

    invoke-virtual {v13, v0, v14}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 457
    new-instance v7, Landroid/content/Intent;

    const-class v13, Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService;

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 458
    .local v7, "pinCodeResetService":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/sec/android/app/shealth/home/HomeActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 461
    .end local v7    # "pinCodeResetService":Landroid/content/Intent;
    :cond_5
    new-instance v13, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    .line 462
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->setActivityBackground()V

    .line 463
    new-instance v13, Lcom/sec/android/app/shealth/home/HomeFragment;

    invoke-direct {v13}, Lcom/sec/android/app/shealth/home/HomeFragment;-><init>()V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSummayFrag:Lcom/sec/android/app/shealth/home/HomeFragment;

    .line 464
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSummayFrag:Lcom/sec/android/app/shealth/home/HomeFragment;

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/shealth/home/HomeActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 466
    sget-boolean v13, Lcom/sec/android/app/shealth/home/HomeActivity;->isDeleteDownloadAppPopupShown:Z

    if-nez v13, :cond_6

    .line 467
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->checkDeleteDownloadApp()V

    .line 469
    :cond_6
    move-object/from16 v0, p0

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/home/HomeActivity;->mActivityContext:Landroid/content/Context;

    .line 472
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->checkExportableDataExist()Z

    move-result v13

    if-eqz v13, :cond_1d

    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getUpgradeStatus(Landroid/content/Context;)I

    move-result v13

    if-eqz v13, :cond_1d

    .line 476
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "[EXPORT] There is data present in the DB so as to export"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    sget-boolean v13, Lcom/sec/android/app/shealth/home/HomeActivity;->sAllDataPresent:Z

    if-eqz v13, :cond_18

    .line 478
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "[EXPORT] All data present in the DB so as to export"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    const-string v13, "BP_BG_DATA"

    move-object/from16 v0, p0

    invoke-static {v0, v13}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isExportDialogFeatureShown(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_15

    const-string v13, "HR_STRESS_DATA"

    move-object/from16 v0, p0

    invoke-static {v0, v13}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isExportDialogFeatureShown(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_15

    .line 481
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "[EXPORT] BP BG popup already shown hence showing only hr stress popup"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    const-string v13, "HR_STRESS_DATA"

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/app/shealth/home/HomeActivity;->showExportableFeatureDialog(Ljava/lang/String;)V

    .line 483
    const-string v13, "HR_STRESS_DATA"

    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v13, v14}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setExportDialogFeatureShown(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 521
    :goto_5
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v13

    const-string v14, "IS_UPGRADE_DONE"

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 523
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/home/HomeActivity;->mActivityContext:Landroid/content/Context;

    const-string v14, "com.sec.android.app.shealth_preferences"

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v9

    .line 524
    .local v9, "pref":Landroid/content/SharedPreferences;
    if-eqz v9, :cond_7

    .line 525
    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v13

    const-string v14, "gender"

    invoke-interface {v13, v14}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 529
    :cond_7
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->checkProfileItems()V

    .line 530
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/GalleryUtils;->getDefaultHealthBoardBackgroundPortraitFilePath()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/sec/android/app/shealth/common/utils/FolderUtils;->isFileExist(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_8

    .line 531
    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/app/shealth/home/HomeActivity;->setHealthBoardWallpaper(I)V

    .line 535
    .end local v9    # "pref":Landroid/content/SharedPreferences;
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v13

    const-string v14, "IS_FIRST_RECYCLE"

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v13

    if-eqz v13, :cond_9

    .line 536
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->recycleBitmap()V

    .line 539
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v13

    const-string/jumbo v14, "showOnStart"

    invoke-virtual {v13, v14}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/shealth/home/HomeActivity;->homeFlag:Z

    .line 541
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/shealth/home/HomeActivity;->homeFlag:Z

    if-nez v13, :cond_a

    .line 543
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/home/HomeActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/home/HomeActivity;->mDrawerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v13, v14}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->closeDrawerQuick(Landroid/view/View;)V

    .line 544
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/home/HomeActivity;->TRANSPARENT_DRAWABLE:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v13, v14}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarBackground(Landroid/graphics/drawable/Drawable;)V

    .line 546
    :cond_a
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "\t[PERF] HomeActivity.onCreate() Breakup 3- START"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isRestoreAgainHomePopup(Landroid/content/Context;)Z

    move-result v13

    if-eqz v13, :cond_b

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSystemConfigurationChanged:Z

    if-nez v13, :cond_b

    .line 551
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_b

    .line 552
    invoke-static {}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->getInstance()Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->doNormalInitialization(Landroid/content/Context;)Z

    .line 553
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v13

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/sec/android/service/health/keyManager/KeyManager;->setContentProviderState(Z)V

    .line 554
    new-instance v13, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/shealth/home/HomeActivity;->restoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .line 555
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/home/HomeActivity;->restoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->startRestoreProcess()V

    .line 566
    :cond_b
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v13

    if-eqz v13, :cond_1e

    .line 567
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v13

    const-string v14, "com.sec.android.app.shealth.walkingmate"

    const-string v15, "W009"

    const/16 v16, 0x3e8

    invoke-static/range {v13 .. v16}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertStatusLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 573
    :goto_6
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "[PERF] HomeActivity.onCreate() BREAKUP 4- START"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 576
    :try_start_3
    new-instance v12, Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-direct {v12, v13}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;-><init>(Landroid/content/Context;)V

    .line 577
    .local v12, "scm":Lcom/samsung/android/sdk/health/content/ShealthContentManager;
    const/16 v13, 0x190

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->startBackup(ILcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 584
    .end local v12    # "scm":Lcom/samsung/android/sdk/health/content/ShealthContentManager;
    :goto_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/home/HomeActivity;->mHomeWearableManager:Lcom/sec/android/app/shealth/home/HomeWearableManager;

    if-nez v13, :cond_c

    .line 585
    new-instance v13, Lcom/sec/android/app/shealth/home/HomeWearableManager;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/home/HomeActivity;->mDeviceFinderServiceListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v14}, Lcom/sec/android/app/shealth/home/HomeWearableManager;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/shealth/home/HomeActivity;->mHomeWearableManager:Lcom/sec/android/app/shealth/home/HomeWearableManager;

    .line 587
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    const/16 v14, 0x235b

    invoke-static {v13, v14}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableUtil;->deleteNotification(Landroid/content/Context;I)Z

    .line 588
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    const/16 v14, 0x42c

    invoke-static {v13, v14}, Lcom/sec/android/service/health/sensor/handler/wearable/SWearableUtil;->deleteNotification(Landroid/content/Context;I)Z

    .line 590
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "[PERF] HomeActivity.onCreate() - END"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 591
    .end local v5    # "dialogFragment":Landroid/support/v4/app/DialogFragment;
    :goto_8
    return-void

    .line 384
    :cond_d
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->checkForUpdates()V

    goto/16 :goto_3

    .line 413
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v13

    const-string v14, "launchWidget"

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v13

    if-eqz v13, :cond_12

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/Intent;->getFlags()I

    move-result v13

    const/high16 v14, 0x100000

    and-int/2addr v13, v14

    if-nez v13, :cond_12

    .line 414
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v13

    const-string/jumbo v14, "widgetActivityAction"

    invoke-virtual {v13, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 415
    .local v2, "action":Ljava/lang/String;
    const-string v3, "com.sec.android.app.shealth.cignacoach.DESTINATION_MAIN"

    .line 416
    .local v3, "coachAction":Ljava/lang/String;
    const-string v4, "com.sec.shealth.action.COACH"

    .line 418
    .local v4, "coachDefualtAction":Ljava/lang/String;
    if-eqz v2, :cond_10

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_f

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_10

    :cond_f
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->isUseCigna()Z

    move-result v13

    if-nez v13, :cond_10

    .line 419
    const-string v13, "SHealth HomeActivity - OnCreate"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "FullVersionCountryTable.isUseCigna()  "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->isUseCigna()Z

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 420
    :cond_10
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    const-string/jumbo v14, "security_pin_enabled"

    move-object/from16 v0, p0

    invoke-virtual {v13, v0, v14}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_3

    .line 423
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isTermsAndConditionsOf355Accepted(Landroid/content/Context;)Z

    move-result v13

    const/4 v14, 0x1

    if-eq v13, v14, :cond_11

    .line 424
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "[35XTO355] storing  widget intent "

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/shealth/home/HomeActivity;->widgetLaunchAfterTCPPAcceptedIntent:Landroid/content/Intent;

    .line 426
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->callTermsAndConditions()V

    goto/16 :goto_4

    .line 428
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/app/shealth/home/HomeActivity;->launchPasswordWithDelay(Landroid/content/Intent;)V

    goto/16 :goto_4

    .line 432
    .end local v2    # "action":Ljava/lang/String;
    .end local v3    # "coachAction":Ljava/lang/String;
    .end local v4    # "coachDefualtAction":Ljava/lang/String;
    :cond_12
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v13

    const-string v14, "Restart_after_Restore"

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v13

    if-eqz v13, :cond_13

    .line 433
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->killProcessAndRestart()V

    goto/16 :goto_8

    .line 435
    :cond_13
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v13

    const-string v14, "Restart_after_update"

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v13

    if-eqz v13, :cond_14

    .line 436
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->finish()V

    .line 437
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v13

    invoke-static {v13}, Landroid/os/Process;->killProcess(I)V

    goto/16 :goto_8

    .line 440
    :cond_14
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isNeedDeviceSyncGuidePopup()Z

    move-result v13

    if-eqz v13, :cond_3

    sget-boolean v13, Lcom/sec/android/app/shealth/home/HomeActivity;->isConncectDevicePopupShown:Z

    if-nez v13, :cond_3

    .line 441
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->connectedWearableDevice()V

    goto/16 :goto_4

    .line 484
    .restart local v5    # "dialogFragment":Landroid/support/v4/app/DialogFragment;
    :cond_15
    const-string v13, "HR_STRESS_DATA"

    move-object/from16 v0, p0

    invoke-static {v0, v13}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isExportDialogFeatureShown(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_16

    const-string v13, "BP_BG_DATA"

    move-object/from16 v0, p0

    invoke-static {v0, v13}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isExportDialogFeatureShown(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_16

    .line 486
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "[EXPORT] hr stress popup already shown hence showing only BP BG popup"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 487
    const-string v13, "BP_BG_DATA"

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/app/shealth/home/HomeActivity;->showExportableFeatureDialog(Ljava/lang/String;)V

    .line 488
    const-string v13, "BP_BG_DATA"

    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v13, v14}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setExportDialogFeatureShown(Landroid/content/Context;Ljava/lang/String;Z)V

    goto/16 :goto_5

    .line 489
    :cond_16
    const-string v13, "BP_BG_DATA"

    move-object/from16 v0, p0

    invoke-static {v0, v13}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isExportDialogFeatureShown(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_17

    const-string v13, "HR_STRESS_DATA"

    move-object/from16 v0, p0

    invoke-static {v0, v13}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isExportDialogFeatureShown(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_17

    const-string v13, "ALL_DATA"

    move-object/from16 v0, p0

    invoke-static {v0, v13}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isExportDialogFeatureShown(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_17

    .line 492
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "[EXPORT] No popup already shown hence showing all popup"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    const-string v13, "ALL_DATA"

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/app/shealth/home/HomeActivity;->showExportableFeatureDialog(Ljava/lang/String;)V

    .line 494
    const-string v13, "ALL_DATA"

    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v13, v14}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setExportDialogFeatureShown(Landroid/content/Context;Ljava/lang/String;Z)V

    goto/16 :goto_5

    .line 496
    :cond_17
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "[EXPORT] [1] Dialog already shown"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 498
    :cond_18
    sget-boolean v13, Lcom/sec/android/app/shealth/home/HomeActivity;->sAllDataPresent:Z

    if-nez v13, :cond_1a

    sget-boolean v13, Lcom/sec/android/app/shealth/home/HomeActivity;->sBPBGData:Z

    if-eqz v13, :cond_1a

    .line 499
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "[EXPORT] BP or BG data present in the DB so as to export"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    const-string v13, "BP_BG_DATA"

    move-object/from16 v0, p0

    invoke-static {v0, v13}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isExportDialogFeatureShown(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_19

    .line 501
    const-string v13, "BP_BG_DATA"

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/app/shealth/home/HomeActivity;->showExportableFeatureDialog(Ljava/lang/String;)V

    .line 502
    const-string v13, "BP_BG_DATA"

    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v13, v14}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setExportDialogFeatureShown(Landroid/content/Context;Ljava/lang/String;Z)V

    goto/16 :goto_5

    .line 504
    :cond_19
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "[EXPORT] [2] Dialog already shown"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 506
    :cond_1a
    sget-boolean v13, Lcom/sec/android/app/shealth/home/HomeActivity;->sAllDataPresent:Z

    if-nez v13, :cond_1c

    sget-boolean v13, Lcom/sec/android/app/shealth/home/HomeActivity;->sHRStressData:Z

    if-eqz v13, :cond_1c

    .line 507
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "[EXPORT] HR or Stress data present in the DB so as to export"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    const-string v13, "HR_STRESS_DATA"

    move-object/from16 v0, p0

    invoke-static {v0, v13}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isExportDialogFeatureShown(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_1b

    .line 509
    const-string v13, "HR_STRESS_DATA"

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/app/shealth/home/HomeActivity;->showExportableFeatureDialog(Ljava/lang/String;)V

    .line 510
    const-string v13, "HR_STRESS_DATA"

    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v13, v14}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setExportDialogFeatureShown(Landroid/content/Context;Ljava/lang/String;Z)V

    goto/16 :goto_5

    .line 512
    :cond_1b
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "[EXPORT] [3] Dialog already shown"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 515
    :cond_1c
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v14, "[EXPORT] No data present in the DB so as to export"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 518
    :cond_1d
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "[EXPORT] SharedPreferencesHelper.getUpgradeStatus(this) : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getUpgradeStatus(Landroid/content/Context;)I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 570
    :cond_1e
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v13

    const-string v14, "com.sec.android.app.shealth.walkingmate"

    const-string v15, "W009"

    const/16 v16, 0x0

    invoke-static/range {v13 .. v16}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertStatusLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_6

    .line 578
    :catch_0
    move-exception v6

    .line 579
    .local v6, "e":Landroid/os/RemoteException;
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Backup profile data on server error : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    .line 580
    .end local v6    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v6

    .line 581
    .local v6, "e":Ljava/lang/Exception;
    sget-object v13, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Backup profile data on server error : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    .line 370
    .end local v5    # "dialogFragment":Landroid/support/v4/app/DialogFragment;
    .end local v6    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v13

    goto/16 :goto_2

    .line 364
    :catch_3
    move-exception v13

    goto/16 :goto_1

    .line 356
    :catch_4
    move-exception v13

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1401
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->homeMenu:Landroid/view/Menu;

    .line 1402
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f100014

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1404
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->isDrawerMenuShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1405
    const/4 v0, 0x0

    .line 1407
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 1373
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->unregisterSystemCloseDialogReceiver()V

    .line 1374
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/sec/android/app/shealth/receiver/LaunchWidgetReceiver;->prevWidgetClickTime:J

    .line 1375
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/home/HomeActivity;->isThirdPartyAppLaunched:Z

    .line 1376
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->unregisterWearableConnectedObserver()V

    .line 1377
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSummayFrag:Lcom/sec/android/app/shealth/home/HomeFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSummayFrag:Lcom/sec/android/app/shealth/home/HomeFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/HomeFragment;->getHomeLatestData()Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1378
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSummayFrag:Lcom/sec/android/app/shealth/home/HomeFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/HomeFragment;->getHomeLatestData()Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->recycleView()V

    .line 1380
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->updateCheck:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    if-eqz v0, :cond_1

    .line 1381
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->updateCheck:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->cancelCheckTask()V

    .line 1383
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mHomeWearableManager:Lcom/sec/android/app/shealth/home/HomeWearableManager;

    if-eqz v0, :cond_2

    .line 1384
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mHomeWearableManager:Lcom/sec/android/app/shealth/home/HomeWearableManager;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/HomeWearableManager;->destroy()V

    .line 1385
    :cond_2
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onDestroy()V

    .line 1386
    return-void
.end method

.method public onDrawerClosed()V
    .locals 2

    .prologue
    .line 1157
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->TRANSPARENT_DRAWABLE:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1158
    return-void
.end method

.method public onDrawerOpened()V
    .locals 3

    .prologue
    .line 1152
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0207a2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1153
    return-void
.end method

.method public onFinished(II)V
    .locals 0
    .param p1, "dataSyncType"    # I
    .param p2, "error"    # I

    .prologue
    .line 1993
    return-void
.end method

.method protected onLogSelected()V
    .locals 0

    .prologue
    .line 1367
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 721
    sget-object v0, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HomeActivity onNewIntent"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 723
    const-string v0, "Restart_after_Restore"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 724
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->killProcessAndRestart()V

    .line 759
    :cond_0
    :goto_0
    return-void

    .line 726
    :cond_1
    const-string v0, "Restart_after_update"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 727
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->finish()V

    .line 728
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    goto :goto_0

    .line 735
    :cond_2
    const-string v0, "exercisenotify"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "launchWidget"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 737
    :cond_3
    sget-object v0, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HomeActivity onNewIntent - exercisenotify or launchWidget"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 738
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 740
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    const-string/jumbo v1, "security_pin_enabled"

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 743
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isTermsAndConditionsOf355Accepted(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    .line 744
    sget-object v0, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v1, "[35XTO355] storing widget intent (onNewIntent)"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 745
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->widgetLaunchAfterTCPPAcceptedIntent:Landroid/content/Intent;

    .line 746
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->callTermsAndConditions()V

    goto :goto_0

    .line 748
    :cond_4
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/home/HomeActivity;->launchPasswordWithDelay(Landroid/content/Intent;)V

    goto :goto_0

    .line 754
    :cond_5
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 755
    const-string/jumbo v0, "showOnStart"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->homeFlag:Z

    .line 756
    const-string v0, "isSettingsItem"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->isSettingsItem:Z

    goto/16 :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 7
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 1458
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f080c8a

    if-ne v4, v5, :cond_0

    .line 1459
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "com.sec.android.app.shealth"

    const-string v6, "H002"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1460
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v4

    .line 1492
    :goto_0
    return v4

    .line 1462
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v4, 0x3

    const v5, 0x7f090908

    invoke-direct {v0, p0, v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    .line 1463
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const v5, 0x7f080091

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    .line 1464
    .local v1, "currentFragment":Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 1492
    :cond_1
    :goto_1
    const/4 v4, 0x0

    goto :goto_0

    .line 1466
    :pswitch_0
    instance-of v4, v1, Lcom/sec/android/app/shealth/home/HomeFragment;

    if-eqz v4, :cond_1

    .line 1468
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;

    invoke-direct {v2, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1469
    .local v2, "i":Landroid/content/Intent;
    const/4 v4, 0x1

    invoke-virtual {p0, v2, v4}, Lcom/sec/android/app/shealth/home/HomeActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    .line 1473
    .end local v2    # "i":Landroid/content/Intent;
    :pswitch_1
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->setBackgroundDialog(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;)V

    goto :goto_1

    .line 1476
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "com.sec.android.app.shealth"

    const-string v6, "H004"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1477
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mActivityContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_2

    .line 1478
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1479
    .local v3, "intent":Landroid/content/Intent;
    const v4, 0x8000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1480
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/HomeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 1482
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_2
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1483
    .restart local v3    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/HomeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 1488
    .end local v3    # "intent":Landroid/content/Intent;
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "com.sec.android.app.shealth"

    const-string v6, "H003"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1489
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->goToSettingActivity()V

    goto :goto_1

    .line 1464
    :pswitch_data_0
    .packed-switch 0x7f080c9d
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 959
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->widgetBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 960
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncing:Z

    .line 961
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "launchWidget"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 963
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 964
    sget-object v0, Lcom/sec/android/app/shealth/home/HomeActivity;->mDrawableList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 965
    sget-object v0, Lcom/sec/android/app/shealth/home/HomeActivity;->mDrawableList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 966
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/home/HomeActivity;->mDrawableList:Ljava/util/ArrayList;

    .line 968
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->unregisterWearableConnectedObserver()V

    .line 969
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSyncTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 970
    sget-object v0, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HomeActivity onPause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 971
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onPause()V

    .line 972
    return-void

    .line 962
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 688
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 689
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 690
    .local v0, "launcherintent":Landroid/content/Intent;
    const-string v1, "SHealth HomeActivity - isInitializationNeeded"

    invoke-virtual {v0}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 691
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 692
    const-wide/16 v1, 0x0

    sput-wide v1, Lcom/sec/android/app/shealth/receiver/LaunchWidgetReceiver;->prevWidgetClickTime:J

    .line 693
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->finish()V

    .line 695
    .end local v0    # "launcherintent":Landroid/content/Intent;
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 696
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v1, 0x7f080c9d

    const/4 v4, 0x1

    .line 1426
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->isGearDevice()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1427
    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1434
    :goto_0
    const v0, 0x7f080c8a

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1436
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->isMenuLoaded:Z

    if-nez v0, :cond_0

    .line 1438
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->isMenuLoaded:Z

    .line 1439
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->clearScreensViewsList()V

    .line 1440
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/home/HomeActivity$10;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/home/HomeActivity$10;-><init>(Lcom/sec/android/app/shealth/home/HomeActivity;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1451
    :cond_0
    return v4

    .line 1429
    :cond_1
    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onProgress(II)V
    .locals 0
    .param p1, "dataSyncType"    # I
    .param p2, "percent"    # I

    .prologue
    .line 1990
    return-void
.end method

.method protected onRestart()V
    .locals 0

    .prologue
    .line 1421
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onRestart()V

    .line 1422
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 885
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onResume()V

    .line 886
    const-string v2, "VerificationLog"

    const-string/jumbo v3, "onResume"

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 887
    sget-object v2, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v3, "[PERF] HomeActivity.onResume() - START"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 888
    sput-boolean v4, Lcom/sec/android/app/shealth/home/HomeActivity;->isThirdPartyAppLaunched:Z

    .line 889
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mDrawerMenuList:Landroid/widget/ListView;

    if-eqz v2, :cond_0

    .line 891
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mDrawerMenuList:Landroid/widget/ListView;

    invoke-virtual {v2, v4, v4}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 893
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->homeFlag:Z

    if-nez v2, :cond_1

    .line 895
    sget-object v2, Lcom/sec/android/app/shealth/home/HomeActivity;->onStopClassName:Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 897
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mDrawerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->closeDrawerQuick(Landroid/view/View;)V

    .line 898
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->TRANSPARENT_DRAWABLE:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarBackground(Landroid/graphics/drawable/Drawable;)V

    .line 909
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->restoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    if-eqz v2, :cond_2

    .line 910
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->restoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->restoreState()V

    .line 913
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->registerWearableConnectedObserver()V

    .line 915
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 916
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v2, "com.samsung.android.sdk.health.sensor.action.DATA_UPDATED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 917
    const-string v2, "com.samsung.android.shealth.HEALTH_SYNC_ERROR"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 918
    const-string v2, "com.sec.shealth.action.burnt_calories"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 920
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->widgetBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 922
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->setSyncActionBarButton()V

    .line 923
    sget-object v2, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "registerWearableConnectedObserver"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 925
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 926
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "launchWidget"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mHomeWearableManager:Lcom/sec/android/app/shealth/home/HomeWearableManager;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/HomeWearableManager;->checkConnectedWearable()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 927
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->syncWearableDevice()V

    .line 929
    :cond_3
    sget-object v2, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v3, "[PERF] HomeActivity.onResume() - END"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 930
    const-string v2, "VerificationLog"

    const-string v3, "Executed"

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 931
    return-void
.end method

.method protected onResumeFragments()V
    .locals 2

    .prologue
    .line 934
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onResumeFragments()V

    .line 935
    sget-object v0, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string v1, "[PERF] HomeActivity.onResumeFragments() - END"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 936
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 1413
    const-string/jumbo v0, "system_configuration_changed"

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSystemConfigurationChanged:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1414
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1415
    return-void
.end method

.method public onSlide(F)V
    .locals 0
    .param p1, "offset"    # F

    .prologue
    .line 1161
    return-void
.end method

.method public onStarted(IILjava/lang/String;)V
    .locals 0
    .param p1, "dataSyncType"    # I
    .param p2, "error"    # I
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    .line 1987
    return-void
.end method

.method protected onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 941
    sget-object v0, Lcom/sec/android/app/shealth/home/HomeActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HomeActivity onStop"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 942
    sget-boolean v0, Lcom/sec/android/app/shealth/home/HomeActivity;->isThirdPartyAppLaunched:Z

    if-eqz v0, :cond_0

    .line 944
    sput-boolean v3, Lcom/sec/android/app/shealth/home/HomeActivity;->isThirdPartyAppLaunched:Z

    .line 945
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/shealth/home/HomeActivity;->requestPassword:Z

    .line 948
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "launched_from_input_password"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 950
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "launched_from_input_password"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 952
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onStop()V

    .line 953
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->homeFlag:Z

    .line 954
    return-void
.end method

.method public registerWearableConnectedObserver()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1884
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/Settings$System;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mWearableConnectedObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1886
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.samsung.android.app.atracker"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Utils;->hasPackage(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1887
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants$SWearableConstants;->ACTIVITY_TRACKER_DEVICE_CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mWearableConnectedObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1888
    :cond_0
    return-void
.end method

.method public restoreCompleted(Z)V
    .locals 1
    .param p1, "restored"    # Z

    .prologue
    .line 1980
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isRestoreAgainHomePopup(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1982
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mSystemConfigurationChanged:Z

    .line 1984
    :cond_0
    return-void
.end method

.method public showDrawMenu()V
    .locals 3

    .prologue
    .line 1165
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0207a2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1166
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->showDrawMenu()V

    .line 1167
    return-void
.end method

.method public unregisterWearableConnectedObserver()V
    .locals 2

    .prologue
    .line 1891
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/HomeActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeActivity;->mWearableConnectedObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1892
    return-void
.end method
