.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$2;
.super Ljava/lang/Object;
.source "PhotoHorizontalScrollView.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 141
    iget v5, p1, Landroid/os/Message;->what:I

    if-nez v5, :cond_3

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    iget-boolean v5, v5, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->isEnable:Z

    if-eqz v5, :cond_3

    .line 142
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$Holder;

    .line 144
    .local v1, "holder":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$Holder;
    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;)Landroid/content/Context;

    move-result-object v5

    invoke-direct {v3, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;-><init>(Landroid/content/Context;)V

    .line 145
    .local v3, "img":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mParams:Landroid/widget/LinearLayout$LayoutParams;
    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 147
    sget-object v5, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v3, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 148
    iget-object v5, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$Holder;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 149
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    iget-object v5, v5, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mImageClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    invoke-virtual {v3, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->setTag(Ljava/lang/Object;)V

    .line 151
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0702ae

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;->setBackgroundColor(I)V

    .line 152
    const/4 v4, 0x0

    .line 153
    .local v4, "isAdd":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mItemLayout:Landroid/widget/LinearLayout;
    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;)Landroid/widget/LinearLayout;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v5

    if-ge v2, v5, :cond_1

    .line 154
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mItemLayout:Landroid/widget/LinearLayout;
    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;)Landroid/widget/LinearLayout;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$Holder;

    .line 155
    .local v0, "TAG":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$Holder;
    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$Holder;->filePath:Ljava/lang/String;

    iget-object v6, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$Holder;->filePath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 156
    const/4 v4, 0x1

    .line 153
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 159
    .end local v0    # "TAG":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$Holder;
    :cond_1
    if-nez v4, :cond_2

    .line 160
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mItemLayout:Landroid/widget/LinearLayout;
    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;)Landroid/widget/LinearLayout;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mParams:Landroid/widget/LinearLayout$LayoutParams;
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v6

    invoke-virtual {v5, v3, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 163
    :cond_2
    const/4 v5, 0x1

    .line 166
    .end local v1    # "holder":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$Holder;
    .end local v2    # "i":I
    .end local v3    # "img":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/FocusedImageView;
    .end local v4    # "isAdd":Z
    :goto_1
    return v5

    :cond_3
    const/4 v5, 0x0

    goto :goto_1
.end method
