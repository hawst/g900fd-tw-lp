.class public Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;
.super Ljava/lang/Object;
.source "StressDeviceConnector.java"


# static fields
.field private static final MEASURING_TIMEOUT:I = 0x2710

.field private static TAG:Ljava/lang/String;

.field private static mInstance:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;


# instance fields
.field private IsDeviceStarted:Z

.field isCountDownStarted:Z

.field public isRecord:Z

.field private mContext:Landroid/content/Context;

.field private mCountDownTimer:Landroid/os/CountDownTimer;

.field mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

.field private mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

.field mEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

.field private mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

.field mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

.field private mStressSensorListener:Lcom/sec/android/app/shealth/stress/StressSensorListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->IsDeviceStarted:Z

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mContext:Landroid/content/Context;

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->isRecord:Z

    .line 248
    new-instance v0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$1;-><init>(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    .line 306
    new-instance v0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$2;-><init>(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

    .line 344
    new-instance v0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$3;-><init>(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    .line 389
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->isCountDownStarted:Z

    .line 390
    new-instance v0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$4;

    const-wide/16 v2, 0x2710

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$4;-><init>(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;JJ)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mCountDownTimer:Landroid/os/CountDownTimer;

    .line 48
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->IsDeviceStarted:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->IsDeviceStarted:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->startDevice()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;)Lcom/sec/android/app/shealth/stress/StressSensorListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mStressSensorListener:Lcom/sec/android/app/shealth/stress/StressSensorListener;

    return-object v0
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mInstance:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mInstance:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    .line 43
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mInstance:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    return-object v0
.end method

.method private initSensor()V
    .locals 0

    .prologue
    .line 119
    return-void
.end method

.method private scanDevice()V
    .locals 10

    .prologue
    const/16 v9, 0x2718

    const/4 v8, 0x4

    .line 56
    sget-object v4, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "scanDevice"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    const/4 v0, 0x0

    .line 62
    .local v0, "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    if-eqz v4, :cond_0

    .line 63
    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    const/4 v5, 0x4

    const/16 v6, 0x2718

    const/4 v7, 0x4

    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->getConnectedDeviceList(III)Ljava/util/List;

    move-result-object v0

    .line 67
    :cond_0
    if-eqz v0, :cond_5

    .line 68
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 69
    sget-object v4, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;

    const-string v5, "deviceList == isEmpty"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    const/4 v5, 0x0

    const/16 v6, 0x103

    invoke-interface {v4, v5, v6}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;->onStopped(II)V

    .line 104
    :cond_1
    :goto_0
    return-void

    .line 72
    :cond_2
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 73
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 74
    .local v1, "dvc":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v4

    if-ne v4, v8, :cond_3

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v4

    if-ne v4, v9, :cond_3

    .line 76
    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 77
    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->join(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;)V

    .line 78
    sget-object v4, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "scanDevice() device: found- "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getProtocol()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_5

    goto :goto_1

    .line 91
    .end local v1    # "dvc":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 92
    .local v2, "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 80
    .end local v2    # "e":Landroid/os/RemoteException;
    .restart local v1    # "dvc":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_3
    :try_start_1
    sget-object v4, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "scanDevice() device: else - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getProtocol()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_1 .. :try_end_1} :catch_5

    goto/16 :goto_1

    .line 93
    .end local v1    # "dvc":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_1
    move-exception v2

    .line 94
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto/16 :goto_0

    .line 83
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_4
    :try_start_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-nez v4, :cond_1

    .line 84
    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    const/4 v5, 0x0

    const/16 v6, 0x103

    invoke-interface {v4, v5, v6}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;->onStopped(II)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_2 .. :try_end_2} :catch_5

    goto/16 :goto_0

    .line 95
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_2
    move-exception v2

    .line 96
    .local v2, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 88
    .end local v2    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :cond_5
    :try_start_3
    sget-object v4, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;

    const-string v5, "deviceList == null"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    const/4 v5, 0x0

    const/16 v6, 0x103

    invoke-interface {v4, v5, v6}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;->onStopped(II)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_3 .. :try_end_3} :catch_5

    goto/16 :goto_0

    .line 97
    :catch_3
    move-exception v2

    .line 98
    .local v2, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;->printStackTrace()V

    goto/16 :goto_0

    .line 99
    .end local v2    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    :catch_4
    move-exception v2

    .line 100
    .local v2, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto/16 :goto_0

    .line 101
    .end local v2    # "e":Ljava/lang/IllegalStateException;
    :catch_5
    move-exception v2

    .line 102
    .local v2, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private startDevice()V
    .locals 4

    .prologue
    .line 122
    sget-object v1, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startDevice"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    if-nez v1, :cond_0

    .line 125
    sget-object v1, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;

    const-string v2, "mDeviceFinder is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    .line 157
    :goto_0
    return-void

    .line 128
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-nez v1, :cond_1

    .line 129
    invoke-direct {p0}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->scanDevice()V

    goto :goto_0

    .line 132
    :cond_1
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->IsDeviceStarted:Z

    if-nez v1, :cond_2

    .line 133
    sget-object v1, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startReceivingData_1"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->startReceivingData(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;)V

    .line 135
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->isRecord:Z

    if-eqz v1, :cond_2

    .line 136
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->startRecord()V

    .line 139
    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->IsDeviceStarted:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_5

    goto :goto_0

    .line 140
    :catch_0
    move-exception v0

    .line 141
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 142
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 143
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 144
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 145
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 146
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 147
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 148
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_4
    move-exception v0

    .line 150
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;->printStackTrace()V

    goto :goto_0

    .line 151
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    :catch_5
    move-exception v0

    .line 153
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0
.end method

.method private stopDevice()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 192
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-eqz v2, :cond_0

    .line 194
    :try_start_0
    sget-object v2, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "stopReceivingData_1"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->stopReceivingData()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_4

    .line 210
    :goto_0
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->leave()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_8

    .line 221
    :goto_1
    iput-object v4, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 225
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    if-eqz v2, :cond_1

    .line 227
    :try_start_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->stopScan()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_2 .. :try_end_2} :catch_a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_b

    .line 237
    :goto_2
    :try_start_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->close()V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_c
    .catch Ljava/util/ConcurrentModificationException; {:try_start_3 .. :try_end_3} :catch_d

    .line 243
    :goto_3
    iput-object v4, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    .line 245
    :cond_1
    return-void

    .line 196
    :catch_0
    move-exception v1

    .line 197
    .local v1, "e1":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 198
    .end local v1    # "e1":Landroid/os/RemoteException;
    :catch_1
    move-exception v1

    .line 199
    .local v1, "e1":Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 200
    .end local v1    # "e1":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v1

    .line 201
    .local v1, "e1":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 202
    .end local v1    # "e1":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_3
    move-exception v0

    .line 203
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 204
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_4
    move-exception v0

    .line 206
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0

    .line 211
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :catch_5
    move-exception v0

    .line 212
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 213
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_6
    move-exception v0

    .line 214
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_1

    .line 215
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_7
    move-exception v0

    .line 216
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1

    .line 217
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_8
    move-exception v0

    .line 218
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 228
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_9
    move-exception v1

    .line 229
    .local v1, "e1":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 230
    .end local v1    # "e1":Landroid/os/RemoteException;
    :catch_a
    move-exception v1

    .line 231
    .local v1, "e1":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_2

    .line 232
    .end local v1    # "e1":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_b
    move-exception v0

    .line 233
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    .line 238
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_c
    move-exception v0

    .line 239
    .restart local v0    # "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_3

    .line 240
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_d
    move-exception v0

    .line 241
    .local v0, "e":Ljava/util/ConcurrentModificationException;
    invoke-virtual {v0}, Ljava/util/ConcurrentModificationException;->printStackTrace()V

    goto :goto_3
.end method


# virtual methods
.method public isStarted()Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->IsDeviceStarted:Z

    return v0
.end method

.method public startCountDownTimer()V
    .locals 2

    .prologue
    .line 406
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->isCountDownStarted:Z

    if-eqz v0, :cond_0

    .line 407
    sget-object v0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;

    const-string v1, "CountDownTimer restart!!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mCountDownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 409
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mCountDownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 415
    :goto_0
    return-void

    .line 411
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;

    const-string v1, "CountDownTimer start!!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 412
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->isCountDownStarted:Z

    .line 413
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mCountDownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    goto :goto_0
.end method

.method public startMeasuring(Landroid/content/Context;Lcom/sec/android/app/shealth/stress/StressSensorListener;Z)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/sec/android/app/shealth/stress/StressSensorListener;
    .param p3, "isRecordON"    # Z

    .prologue
    .line 108
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mContext:Landroid/content/Context;

    .line 109
    if-eqz p2, :cond_0

    .line 110
    iput-object p2, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mStressSensorListener:Lcom/sec/android/app/shealth/stress/StressSensorListener;

    .line 112
    :cond_0
    iput-boolean p3, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->isRecord:Z

    .line 113
    invoke-direct {p0}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->startDevice()V

    .line 114
    invoke-direct {p0}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->initSensor()V

    .line 115
    return-void
.end method

.method public startRecord()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 162
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->record()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_5

    .line 182
    :goto_0
    return-void

    .line 163
    :catch_0
    move-exception v0

    .line 165
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 166
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 168
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 169
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 171
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 172
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 174
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 175
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_4
    move-exception v0

    .line 177
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0

    .line 178
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :catch_5
    move-exception v0

    .line 180
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;->printStackTrace()V

    goto :goto_0
.end method

.method public stopCountDownTimer()V
    .locals 2

    .prologue
    .line 418
    sget-object v0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;

    const-string v1, "CountDownTimer stop!!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->isCountDownStarted:Z

    .line 420
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mCountDownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 421
    return-void
.end method

.method public stopMeasuring()V
    .locals 2

    .prologue
    .line 185
    sget-object v0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "stopMeasuring"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    invoke-direct {p0}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->stopDevice()V

    .line 188
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->IsDeviceStarted:Z

    .line 189
    return-void
.end method
