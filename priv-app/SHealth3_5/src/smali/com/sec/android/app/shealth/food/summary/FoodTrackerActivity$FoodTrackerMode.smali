.class final enum Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;
.super Ljava/lang/Enum;
.source "FoodTrackerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "FoodTrackerMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;

.field public static final enum BASE:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;

.field public static final enum GRAPH:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 127
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;

    const-string v1, "BASE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;->BASE:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;

    .line 128
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;

    const-string v1, "GRAPH"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;->GRAPH:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;

    .line 126
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;

    sget-object v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;->BASE:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;->GRAPH:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;->$VALUES:[Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 126
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 126
    const-class v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;
    .locals 1

    .prologue
    .line 126
    sget-object v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;->$VALUES:[Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$FoodTrackerMode;

    return-object v0
.end method
