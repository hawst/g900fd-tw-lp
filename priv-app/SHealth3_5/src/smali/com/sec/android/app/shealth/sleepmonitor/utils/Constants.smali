.class public final Lcom/sec/android/app/shealth/sleepmonitor/utils/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field public static final ACTIVITY_TRACKER:I = 0x2727

.field public static final LOGGING_CHART_VIEW:Ljava/lang/String; = "SL06"

.field public static final LOGGING_CHART_VIEW_SHARE_VIA:Ljava/lang/String; = "SL08"

.field public static final LOGGING_CONNECT_ACCESSORIES:Ljava/lang/String; = "SL07"

.field public static final LOGGING_HELP:Ljava/lang/String; = "SL04"

.field public static final LOGGING_LOG:Ljava/lang/String; = "SL05"

.field public static final LOGGING_LOG_DETAIL_SHARE_VIA:Ljava/lang/String; = "SL03"

.field public static final LOGGING_LOG_SHARE_VIA:Ljava/lang/String; = "SL02"

.field public static final LOGGING_SUMMARY_VIEW_SHARE_VIA:Ljava/lang/String; = "SL01"

.field public static final SLEEP_APP_ID:Ljava/lang/String; = "com.sec.android.app.shealth.sleepmonitor"

.field public static final STRING_ACTIVITY_TRACKER:Ljava/lang/String; = "Activity Tracker"

.field public static final STRING_ACTIVITY_TRACKER_CODE:Ljava/lang/String; = "Samsung EI-AN900A"

.field public static final STRING_GEAR:Ljava/lang/String; = "Galaxy Gear"

.field public static final STRING_GEAR2:Ljava/lang/String; = "Samsung Gear 2"

.field public static final STRING_TIZEN_GEAR:Ljava/lang/String; = "Samsung Gear"

.field public static final STRING_WING_TIP:Ljava/lang/String; = "Samsung Gear Fit"

.field public static final TYPE_GEAR:I = 0x2724

.field public static final TYPE_GEAR1_TIZEN:I = 0x2728

.field public static final TYPE_GEAR2:I = 0x2726

.field public static final TYPE_GEAR_FIT:I = 0x2723


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
