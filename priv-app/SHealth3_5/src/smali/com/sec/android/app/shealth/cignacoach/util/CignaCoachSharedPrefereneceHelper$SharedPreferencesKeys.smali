.class public interface abstract Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper$SharedPreferencesKeys;
.super Ljava/lang/Object;
.source "CignaCoachSharedPrefereneceHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SharedPreferencesKeys"
.end annotation


# static fields
.field public static final CIGNA_COACH_ASSESSMENT_HEIGHT_UNIT:Ljava/lang/String; = "CIGNA_COACH_ASSESSMENT_HEIGHT_UNIT"

.field public static final CIGNA_COACH_ASSESSMENT_HEIGHT_VALUE:Ljava/lang/String; = "CIGNA_COACH_ASSESSMENT_HEIGHT_VALUE"

.field public static final CIGNA_COACH_ASSESSMENT_WEIGHT_UNIT:Ljava/lang/String; = "CIGNA_COACH_ASSESSMENT_WEIGHT_UNIT"

.field public static final CIGNA_COACH_ASSESSMENT_WEIGHT_VALUE:Ljava/lang/String; = "CIGNA_COACH_ASSESSMENT_WEIGHT_VALUE"

.field public static final CIGNA_COACH_COMPLETE_ASSESSMENT:Ljava/lang/String; = "CIGNA_COACH_COMPLETE_ASSESSMENT"

.field public static final CIGNA_COACH_FIRST_LOADING_COMPLETE:Ljava/lang/String; = "CIGNA_COACH_FIRST_LOADING_COMPLETE"

.field public static final CIGNA_COACH_INTRO_MSG:Ljava/lang/String; = "CIGNA_COACH_INTRO_MSG"

.field public static final CIGNA_COACH_SCORE_VIEW_MESSAGE_MODE:Ljava/lang/String; = "CIGNA_COACH_SCORE_VIEW_MESSAGE_MODE"

.field public static final CIGNA_TERMS_OF_USE_VERSION_CODE:Ljava/lang/String; = "CIGNA_TERMS_OF_USE_VERSION_CODE"

.field public static final CINGA_AGREE_TERMS_OF_USE:Ljava/lang/String; = "CINGA_AGREE_TERMS_OF_USE"

.field public static final COACH_MSG_SPECIFIC_EXERCISE:Ljava/lang/String; = "COACH_MSG_SPECIFIC_EXERCISE"

.field public static final COACH_MSG_SPECIFIC_FOOD:Ljava/lang/String; = "COACH_MSG_SPECIFIC_FOOD"

.field public static final COACH_MSG_SPECIFIC_SLEEP:Ljava/lang/String; = "COACH_MSG_SPECIFIC_SLEEP"

.field public static final COACH_MSG_SPECIFIC_STRESS:Ljava/lang/String; = "COACH_MSG_SPECIFIC_STRESS"

.field public static final COACH_MSG_SPECIFIC_WEIGHT:Ljava/lang/String; = "COACH_MSG_SPECIFIC_WEIGHT"

.field public static final INITIALIZATION_CIGNA_COACH:Ljava/lang/String; = "INITIALIZATION_CIGNA_COACH"

.field public static final PREF_VERSION:Ljava/lang/String; = "PREF_VERSION"
