.class public Lcom/sec/android/app/shealth/walkingmate/utils/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field public static final ALARM_WALK_FOR_LIFE_CARD_OFF_BROADCAST_CODE:I = 0x556a592

.field public static final ALARM_WALK_FOR_LIFE_CARD_OFF_BROADCAST_HOUR:I = 0x11

.field public static final ALARM_WALK_FOR_LIFE_CARD_OFF_BROADCAST_MINUTE:I = 0x0

.field public static final ALARM_WALK_FOR_LIFE_CARD_OFF_BROADCAST_SECOND:I = 0x0

.field public static final ALARM_WALK_FOR_LIFE_CARD_OFF_DELAY_IN_HOURS:I = 0x1

.field public static final ALARM_WALK_FOR_LIFE_CARD_ON_BROADCAST_CODE:I = 0x1eb2cb9

.field public static final ALARM_WALK_FOR_LIFE_CARD_ON_BROADCAST_HOUR:I = 0xf

.field public static final ALARM_WALK_FOR_LIFE_CARD_ON_BROADCAST_MINUTE:I = 0x0

.field public static final ALARM_WALK_FOR_LIFE_CARD_ON_BROADCAST_SECOND:I = 0x0

.field public static final ANIMATION_NONE:I = 0x0

.field public static final ANIMATION_SCALE:I = 0x1

.field public static final ANIMATION_SLIDE:I = 0x2

.field public static final ASSESARY_SBAND:I = 0xa

.field public static final BLOOD_GLUCOSE:I = 0x10

.field public static final BLOOD_PRESSURE:I = 0x11

.field public static final CALENDAR:I = 0xe

.field public static final COMFORT_ZONE:I = 0x8

.field public static final COMFORT_ZONE_LEVEL:I = 0x9

.field public static final COM_SEC_ANDROID_APP_SHEALTH:Ljava/lang/String; = "com.sec.android.app.shealth"

.field public static final DEFAULT_INT_VALUE:I = 0x0

.field public static final DEFAULT_ROW_ID:I = -0x1

.field public static final DEFAULT_STRING_VALUE:Ljava/lang/String; = ""

.field public static final DEFAULT_WATCH_PERIOD_SYNC:I = 0x1

.field public static final DEFAULT_WEIGHT_VALUE:F = 60.0f

.field public static final EVENT_COMFORT_ZONE_STATE_CHANGED:I = 0x16442

.field public static final EXERCISE_MATE:I = 0x3

.field public static final EXERSICE_PICK:I = 0xd

.field public static final FOOD_TRACKER:I = 0x4

.field public static final HEALTHCARE_TYPE:I = 0x457

.field public static final HEALTH_BOARD:I = 0x1

.field public static final HEALTH_CARE:I = 0x5

.field public static final HEALTH_CARE_GLUCOSE:I = 0x35

.field public static final HEALTH_CARE_PRESSURE:I = 0x34

.field public static final HEALTH_CARE_WEIGHT:I = 0x33

.field public static final HEAVY_EXERCISE:B = 0x4t

.field public static final HOVER_ARROW_DIRECTION_CENTER:I = 0x0

.field public static final HOVER_ARROW_DIRECTION_LEFT:I = 0x1

.field public static final HOVER_ARROW_DIRECTION_RIGHT:I = 0x2

.field public static final INPUT_BLUETOOTH:I = 0x1

.field public static final INPUT_NORMAL:I = 0x0

.field public static final INPUT_USB:I = 0x2

.field public static final LIGHT_EXERCISE:B = 0x2t

.field public static final MAN:B = 0x0t

.field public static final MEAL_ACTIVITY:I = 0xc

.field public static final MODERATE_EXERCISE:B = 0x3t

.field public static final NO_EXERCISE:B = 0x1t

.field public static final PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.shealth"

.field public static final ROOT_ACTIVITY:I = 0xb

.field public static final RUNNINGPRO_APP_PNAME:Ljava/lang/String; = "com.rharham.RunningPro"

.field public static final RUNNINGPRO_CHINA_APP_PNAME:Ljava/lang/String; = "com.rharham.RunningPro.cn"

.field public static final RUNNING_PRO:I = 0x12

.field public static final SBAND_EMPTY_STATE:B = 0x3t

.field public static final SBAND_PEDOMETER_STATE:B = 0x2t

.field public static final SBAND_SLEEP_MONITOR_TIME_CORRECTION:J = 0x1499700L

.field public static final SBAND_SLEEP_STATE:B = 0x1t

.field public static final SBAND_WAKE_UP_STATE:B = 0x0t

.field public static final SETTINGS:I = 0x7

.field public static final SLEEP_MONITOR:I = 0x6

.field public static final TOTAL_REPORT_DATA_TYPE:I = -0x64

.field public static final VERY_HEAVY_EXERCISE:B = 0x5t

.field public static final WALKING_MATE:I = 0x2

.field public static final WALK_FOR_LIFE_OFF:I = 0x5fd0

.field public static final WALK_FOR_LIFE_ON:I = 0x47b5

.field public static final WATCH_PERIOD_12_HOURS:I = 0x3

.field public static final WATCH_PERIOD_1_DAY:I = 0x4

.field public static final WATCH_PERIOD_3_DAYS:I = 0x5

.field public static final WATCH_PERIOD_3_HOURS:I = 0x1

.field public static final WATCH_PERIOD_6_HOURS:I = 0x2

.field public static final WATCH_PERIOD_OFF:I = 0x0

.field public static final WEEK_STARTS_FROM_MONDAY:B = 0x0t

.field public static final WEEK_STARTS_FROM_SUNDAY:B = 0x1t

.field public static final WEIGHT:I = 0xf

.field public static final WOMAN:B = 0x1t


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
