.class public Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;
.source "SpO2InputActivity.java"


# instance fields
.field private mComment:Ljava/lang/String;

.field private mHighStatus:Landroid/widget/TextView;

.field private mId:J

.field private mInputSp02:Landroid/widget/TextView;

.field private mLowStatus:Landroid/widget/TextView;

.field private mStateBar:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getContentView()Landroid/view/View;
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 66
    const v1, 0x7f0a05f0

    invoke-static {p0, v1}, Lcom/sec/android/app/shealth/heartrate/utils/SupportUtils;->setScrollContainerHeight(Landroid/app/Activity;I)V

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "SPO2_ID_KEY"

    const-wide/16 v3, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->mId:J

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "MODE_KEY"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->mMode:Ljava/lang/String;

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "COMMENT_KEY"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->mComment:Ljava/lang/String;

    .line 72
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03021d

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 73
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f080993

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->mStateBar:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    .line 74
    const v1, 0x7f080992

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->mInputSp02:Landroid/widget/TextView;

    .line 75
    const v1, 0x7f080991

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "TIME_DATE_KEY"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->mInputSp02:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "SPO2_KEY"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    const v1, 0x7f080994

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->mLowStatus:Landroid/widget/TextView;

    .line 78
    const v1, 0x7f080995

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->mHighStatus:Landroid/widget/TextView;

    .line 80
    sget-object v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->SPO2:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {p0, v1}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->isMedical(Landroid/content/Context;Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 81
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->mStateBar:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->setVisibility(I)V

    .line 82
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->mInputSp02:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 83
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->mLowStatus:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 84
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->mHighStatus:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 93
    :goto_0
    const v1, 0x7f080996

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "HEART_RATE_KEY"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    return-object v0

    .line 86
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->mStateBar:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->setVisibility(I)V

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->mStateBar:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "SPO2_KEY"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v5

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    int-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->moveToPolygon(D)V

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->mInputSp02:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->mLowStatus:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 90
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->mHighStatus:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected isInputChanged()Z
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->mComment:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 41
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->onCreate(Landroid/os/Bundle;)V

    .line 42
    const v0, 0x7f0803e8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->mComment:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setText(Ljava/lang/CharSequence;)V

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->mComment:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setSelection(I)V

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    new-instance v1, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity$1;-><init>(Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->mMemoHeader:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09007c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901fd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 62
    return-void
.end method

.method protected onSaveButtonSelect(Z)V
    .locals 4
    .param p1, "isAdd"    # Z

    .prologue
    .line 99
    invoke-static {p0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->mId:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->updateCommentById(Ljava/lang/String;Ljava/lang/String;)Z

    .line 100
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->isInputChanged()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 101
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090835

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 103
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 104
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 105
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;->startActivity(Landroid/content/Intent;)V

    .line 106
    return-void
.end method
