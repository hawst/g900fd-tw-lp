.class public Lcom/sec/android/app/shealth/framework/repository/common/DBTables$BloodPressureView;
.super Ljava/lang/Object;
.source "DBTables.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/repository/common/DBTables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BloodPressureView"
.end annotation


# static fields
.field public static final KEY_AVG_DIASTOLIC:Ljava/lang/String; = "avgDiastolic"

.field public static final KEY_AVG_PULSE:Ljava/lang/String; = "avgPulse"

.field public static final KEY_AVG_SYSTOLIC:Ljava/lang/String; = "avgSystolic"

.field public static final KEY_COUNT:Ljava/lang/String; = "count"

.field public static final KEY_DAY_START:Ljava/lang/String; = "day_st"

.field public static final KEY_MAX_DIASTOLIC:Ljava/lang/String; = "maxDiastolic"

.field public static final KEY_MAX_PULSE:Ljava/lang/String; = "maxPulse"

.field public static final KEY_MAX_SYSTOLIC:Ljava/lang/String; = "maxSystolic"

.field public static final KEY_MIN_DIASTOLIC:Ljava/lang/String; = "maxDiastolic"

.field public static final KEY_MIN_PULSE:Ljava/lang/String; = "maxPulse"

.field public static final KEY_MIN_SYSTOLIC:Ljava/lang/String; = "maxSystolic"

.field public static final VIEW_NAME:Ljava/lang/String; = "blood_presure_view"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 442
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
