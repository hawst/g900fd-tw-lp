.class public Lcom/sec/android/app/shealth/CheckInitialize;
.super Landroid/app/Activity;
.source "CheckInitialize.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 30
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 31
    const-string v0, "CheckInitialize"

    const-string/jumbo v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/CheckInitialize;->setResult(I)V

    .line 37
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/CheckInitialize;->finish()V

    .line 38
    return-void

    .line 35
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/CheckInitialize;->setResult(I)V

    goto :goto_0
.end method
