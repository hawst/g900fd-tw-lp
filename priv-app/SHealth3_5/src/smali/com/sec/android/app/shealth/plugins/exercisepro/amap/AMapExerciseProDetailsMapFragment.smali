.class public Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
.source "AMapExerciseProDetailsMapFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ISaveChosenItemListenerGetter;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ValidFragment"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$AMapLoadedListener;
    }
.end annotation


# static fields
.field private static final BASE_SUM_LINE:I = 0x12c

.field private static final MIN_LOCATION_VALUE:F = 1.0E-5f

.field private static final POLYLINE_DEFAULT_COLOR:I

.field private static POLYLINE_DEFAULT_WIDTH:I = 0x0

.field private static final TAG:Ljava/lang/String; = "ExerciseProDetailsMapFragment"

.field private static mBackupDataType:I

.field private static mBackupIntent:Landroid/content/Intent;

.field private static mBackupRowId:J

.field public static mMapMode:I


# instance fields
.field private final FIVE_THOUSAND_METERS:I

.field private SPACE:Ljava/lang/String;

.field private final TEN_THOUSAND_METERS:I

.field private boundsBuilder:Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

.field bpm:I

.field private builder:Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

.field private currentZoomLevel:F

.field private delay:Z

.field hrBpmText:Landroid/widget/TextView;

.field private hrBpmTextNonMedical:Landroid/widget/TextView;

.field hrGuideText:Landroid/widget/TextView;

.field hrmPopUpLayout:Landroid/widget/RelativeLayout;

.field hrmText:Landroid/widget/TextView;

.field private layoutMedical:Landroid/widget/LinearLayout;

.field private layoutNonMedical:Landroid/widget/LinearLayout;

.field private mActivityType:I

.field private mAlertBuild_HRM_Result:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mAvgHrm:F

.field private mAvgSpeed:F

.field private mBundle:Landroid/os/Bundle;

.field private mCalories:F

.field private mContentView:Landroid/view/View;

.field private mDataType:I

.field private mDistance:F

.field private mDuration:F

.field private mElevation:F

.field private mEndMode:Z

.field private mExerciseId:J

.field private mFastSpeed:Landroid/widget/TextView;

.field private mIntent:Landroid/content/Intent;

.field private mIsMiniMode:Z

.field public mMap:Lcom/amap/api/maps2d/AMap;

.field private mMapClickListener:Lcom/amap/api/maps2d/AMap$OnMapClickListener;

.field private mMapHRMList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;",
            ">;"
        }
    .end annotation
.end field

.field private mMapMarkerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amap/api/maps2d/model/Marker;",
            ">;"
        }
    .end annotation
.end field

.field mMapPhotoMarker:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList;

.field private mMapView:Lcom/amap/api/maps2d/MapView;

.field mMarkListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;

.field private mMaxHrm:F

.field private mMaxSpeed:F

.field private mMinSpeed:F

.field private mNoDataNotiText:Landroid/widget/TextView;

.field private mPhotoDatas:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;"
        }
    .end annotation
.end field

.field mPhotoHorizontalScrollClickListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$OnTumbnailClickListener;

.field mPhotoScrollView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

.field private mPlaceMode:I

.field mPolygonHeight:I

.field mPolygonWidth:I

.field private mRowId:J

.field private mSlowSpeed:Landroid/widget/TextView;

.field private mStateBar:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;

.field private mSync_time:J

.field private mTryit_map:Z

.field private mUiSettings:Lcom/amap/api/maps2d/UiSettings;

.field maxBpmText:Landroid/widget/TextView;

.field maxHRzone:I

.field minBpmText:Landroid/widget/TextView;

.field minHRzone:I

.field rangeText:Landroid/widget/TextView;

.field private viewModeHandler:Landroid/os/Handler;

.field private viewModeRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 112
    const/16 v0, 0x9

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->POLYLINE_DEFAULT_WIDTH:I

    .line 113
    const/16 v0, 0xff

    const/4 v1, 0x0

    const/16 v2, 0xb7

    const/16 v3, 0xc7

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->POLYLINE_DEFAULT_COLOR:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 184
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 116
    const-string v0, " "

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->SPACE:Ljava/lang/String;

    .line 119
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapMarkerList:Ljava/util/ArrayList;

    .line 127
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mEndMode:Z

    .line 128
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mIsMiniMode:Z

    .line 141
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->currentZoomLevel:F

    .line 147
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMinSpeed:F

    .line 155
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->delay:Z

    .line 156
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->viewModeHandler:Landroid/os/Handler;

    .line 157
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mTryit_map:Z

    .line 163
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mPhotoDatas:Ljava/util/ArrayList;

    .line 167
    const/16 v0, 0x2710

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->TEN_THOUSAND_METERS:I

    .line 168
    const/16 v0, 0x1388

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->FIVE_THOUSAND_METERS:I

    .line 176
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->viewModeRunnable:Ljava/lang/Runnable;

    .line 738
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$6;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMarkListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;

    .line 755
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$7;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mPhotoHorizontalScrollClickListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$OnTumbnailClickListener;

    .line 1382
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->builder:Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    .line 1383
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->boundsBuilder:Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    .line 186
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mBackupDataType:I

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mDataType:I

    .line 187
    sget-wide v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mBackupRowId:J

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mRowId:J

    .line 188
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mBackupIntent:Landroid/content/Intent;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    .line 189
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapHRMList:Ljava/util/ArrayList;

    .line 190
    return-void
.end method

.method public constructor <init>(IJLandroid/content/Intent;)V
    .locals 3
    .param p1, "dataType"    # I
    .param p2, "rowId"    # J
    .param p4, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 193
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 116
    const-string v0, " "

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->SPACE:Ljava/lang/String;

    .line 119
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapMarkerList:Ljava/util/ArrayList;

    .line 127
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mEndMode:Z

    .line 128
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mIsMiniMode:Z

    .line 141
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->currentZoomLevel:F

    .line 147
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMinSpeed:F

    .line 155
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->delay:Z

    .line 156
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->viewModeHandler:Landroid/os/Handler;

    .line 157
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mTryit_map:Z

    .line 163
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mPhotoDatas:Ljava/util/ArrayList;

    .line 167
    const/16 v0, 0x2710

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->TEN_THOUSAND_METERS:I

    .line 168
    const/16 v0, 0x1388

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->FIVE_THOUSAND_METERS:I

    .line 176
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->viewModeRunnable:Ljava/lang/Runnable;

    .line 738
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$6;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMarkListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;

    .line 755
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$7;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mPhotoHorizontalScrollClickListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$OnTumbnailClickListener;

    .line 1382
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->builder:Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    .line 1383
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->boundsBuilder:Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    .line 194
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mDataType:I

    .line 195
    iput-wide p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mRowId:J

    .line 196
    iput-object p4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    .line 198
    sput p1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mBackupDataType:I

    .line 199
    sput-wide p2, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mBackupRowId:J

    .line 200
    sput-object p4, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mBackupIntent:Landroid/content/Intent;

    .line 201
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapHRMList:Ljava/util/ArrayList;

    .line 202
    return-void
.end method

.method public constructor <init>(ZJLandroid/content/Intent;)V
    .locals 3
    .param p1, "isMiniMode"    # Z
    .param p2, "rowId"    # J
    .param p4, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 205
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 116
    const-string v0, " "

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->SPACE:Ljava/lang/String;

    .line 119
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapMarkerList:Ljava/util/ArrayList;

    .line 127
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mEndMode:Z

    .line 128
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mIsMiniMode:Z

    .line 141
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->currentZoomLevel:F

    .line 147
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMinSpeed:F

    .line 155
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->delay:Z

    .line 156
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->viewModeHandler:Landroid/os/Handler;

    .line 157
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mTryit_map:Z

    .line 163
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mPhotoDatas:Ljava/util/ArrayList;

    .line 167
    const/16 v0, 0x2710

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->TEN_THOUSAND_METERS:I

    .line 168
    const/16 v0, 0x1388

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->FIVE_THOUSAND_METERS:I

    .line 176
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->viewModeRunnable:Ljava/lang/Runnable;

    .line 738
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$6;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMarkListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;

    .line 755
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$7;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mPhotoHorizontalScrollClickListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$OnTumbnailClickListener;

    .line 1382
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->builder:Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    .line 1383
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->boundsBuilder:Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    .line 206
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mIsMiniMode:Z

    .line 207
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mDataType:I

    .line 208
    iput-wide p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mRowId:J

    .line 209
    iput-object p4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    .line 211
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mDataType:I

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mBackupDataType:I

    .line 212
    sput-wide p2, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mBackupRowId:J

    .line 213
    sput-object p4, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mBackupIntent:Landroid/content/Intent;

    .line 214
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapHRMList:Ljava/util/ArrayList;

    .line 215
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->delay:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 104
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->delay:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)Lcom/amap/api/maps2d/AMap$OnMapClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapClickListener:Lcom/amap/api/maps2d/AMap$OnMapClickListener;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mStateBar:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mStateBar:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;
    .param p1, "x1"    # I

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->setHRMStateBar(I)V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mAlertBuild_HRM_Result:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mAlertBuild_HRM_Result:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)Lcom/amap/api/maps2d/model/LatLngBounds$Builder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->builder:Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)Lcom/amap/api/maps2d/model/LatLngBounds$Builder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->boundsBuilder:Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    .prologue
    .line 104
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->currentZoomLevel:F

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;
    .param p1, "x1"    # F

    .prologue
    .line 104
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->currentZoomLevel:F

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->viewModeRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->viewModeHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapMarkerList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->layoutMedical:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;
    .param p1, "x1"    # Landroid/widget/LinearLayout;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->layoutMedical:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->layoutNonMedical:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;
    .param p1, "x1"    # Landroid/widget/LinearLayout;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->layoutNonMedical:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->hrBpmTextNonMedical:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->hrBpmTextNonMedical:Landroid/widget/TextView;

    return-object p1
.end method

.method private addListenerViewModeButton()V
    .locals 3

    .prologue
    .line 687
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mContentView:Landroid/view/View;

    const v2, 0x7f080717

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 688
    .local v0, "mapModeSetting":Landroid/widget/ImageButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09021d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 689
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$4;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 700
    return-void
.end method

.method private convertDptoPx(I)I
    .locals 5
    .param p1, "dp"    # I

    .prologue
    .line 1331
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1332
    .local v2, "res":Landroid/content/res/Resources;
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 1333
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    int-to-float v3, p1

    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float v1, v3, v4

    .line 1334
    .local v1, "px":F
    float-to-int v3, v1

    return v3
.end method

.method private drawLine(IIILjava/util/List;Ljava/util/List;)V
    .locals 12
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "step"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 595
    .local p4, "locationItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;>;"
    .local p5, "speedItems":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    move v1, p1

    .local v1, "i":I
    :goto_0
    if-ge v1, p2, :cond_2

    .line 596
    mul-int v2, v1, p3

    .line 597
    .local v2, "index":I
    move-object/from16 v0, p5

    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getColor(ILjava/util/List;)I

    move-result v4

    .line 598
    .local v4, "lineColor":I
    new-instance v5, Lcom/amap/api/maps2d/model/PolylineOptions;

    invoke-direct {v5}, Lcom/amap/api/maps2d/model/PolylineOptions;-><init>()V

    .line 599
    .local v5, "polyLineOption":Lcom/amap/api/maps2d/model/PolylineOptions;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    if-gt v3, p3, :cond_1

    .line 600
    add-int v6, v2, v3

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v7

    if-ge v6, v7, :cond_0

    .line 601
    new-instance v7, Lcom/amap/api/maps2d/model/LatLng;

    add-int v6, v2, v3

    move-object/from16 v0, p4

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLatitude()D

    move-result-wide v8

    add-int v6, v2, v3

    move-object/from16 v0, p4

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLongitude()D

    move-result-wide v10

    invoke-direct {v7, v8, v9, v10, v11}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    invoke-virtual {v5, v7}, Lcom/amap/api/maps2d/model/PolylineOptions;->add(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/PolylineOptions;

    .line 599
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 605
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    sget v7, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->POLYLINE_DEFAULT_WIDTH:I

    int-to-float v7, v7

    invoke-virtual {v5, v7}, Lcom/amap/api/maps2d/model/PolylineOptions;->width(F)Lcom/amap/api/maps2d/model/PolylineOptions;

    move-result-object v7

    invoke-virtual {v7, v4}, Lcom/amap/api/maps2d/model/PolylineOptions;->color(I)Lcom/amap/api/maps2d/model/PolylineOptions;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/amap/api/maps2d/AMap;->addPolyline(Lcom/amap/api/maps2d/model/PolylineOptions;)Lcom/amap/api/maps2d/model/Polyline;

    .line 595
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 607
    .end local v2    # "index":I
    .end local v3    # "j":I
    .end local v4    # "lineColor":I
    .end local v5    # "polyLineOption":Lcom/amap/api/maps2d/model/PolylineOptions;
    :cond_2
    return-void
.end method

.method private drawMapRoute(Ljava/util/List;Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "locationItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;>;"
    .local p2, "speedItems":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    const/4 v1, 0x0

    .line 610
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x2

    if-ge v0, v3, :cond_0

    .line 621
    :goto_0
    return-void

    .line 612
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/16 v3, 0x12c

    if-le v0, v3, :cond_1

    .line 613
    const/16 v9, 0x12c

    .line 614
    .local v9, "baseLength":I
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    rem-int v2, v0, v9

    .line 615
    .local v2, "mod":I
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    div-int v10, v0, v9

    .line 616
    .local v10, "step":I
    add-int/lit8 v3, v10, 0x1

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->drawLine(IIILjava/util/List;Ljava/util/List;)V

    move-object v1, p0

    move v3, v9

    move v4, v10

    move-object v5, p1

    move-object v6, p2

    .line 617
    invoke-direct/range {v1 .. v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->drawLine(IIILjava/util/List;Ljava/util/List;)V

    goto :goto_0

    .line 619
    .end local v2    # "mod":I
    .end local v9    # "baseLength":I
    .end local v10    # "step":I
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x1

    move-object v3, p0

    move v4, v1

    move-object v7, p1

    move-object v8, p2

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->drawLine(IIILjava/util/List;Ljava/util/List;)V

    goto :goto_0
.end method

.method private drawNoSpeedDBMapRoute(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 625
    .local p1, "locationItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;>;"
    sget v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->POLYLINE_DEFAULT_COLOR:I

    .line 626
    .local v1, "lineColor":I
    sget v3, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->POLYLINE_DEFAULT_WIDTH:I

    .line 627
    .local v3, "lineWidth":I
    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMaxSpeed:F

    iput v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMinSpeed:F

    .line 629
    const-string v4, "ExerciseProDetailsMapFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ">>> locationItems="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",  <<<"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 630
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x2

    if-ge v4, v5, :cond_0

    .line 640
    :goto_0
    return-void

    .line 633
    :cond_0
    new-instance v2, Lcom/amap/api/maps2d/model/PolylineOptions;

    invoke-direct {v2}, Lcom/amap/api/maps2d/model/PolylineOptions;-><init>()V

    .line 634
    .local v2, "lineOpt":Lcom/amap/api/maps2d/model/PolylineOptions;
    int-to-float v4, v3

    invoke-virtual {v2, v4}, Lcom/amap/api/maps2d/model/PolylineOptions;->width(F)Lcom/amap/api/maps2d/model/PolylineOptions;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/amap/api/maps2d/model/PolylineOptions;->color(I)Lcom/amap/api/maps2d/model/PolylineOptions;

    .line 636
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 637
    new-instance v5, Lcom/amap/api/maps2d/model/LatLng;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLatitude()D

    move-result-wide v6

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLongitude()D

    move-result-wide v8

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    invoke-virtual {v2, v5}, Lcom/amap/api/maps2d/model/PolylineOptions;->add(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/PolylineOptions;

    .line 636
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 639
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    invoke-virtual {v4, v2}, Lcom/amap/api/maps2d/AMap;->addPolyline(Lcom/amap/api/maps2d/model/PolylineOptions;)Lcom/amap/api/maps2d/model/Polyline;

    goto :goto_0
.end method

.method private drawRoute(Ljava/util/ArrayList;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 567
    .local p1, "speedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;>;"
    .local p2, "historyLocation":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;>;"
    .local p3, "historySpeed":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 568
    :cond_0
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->drawNoSpeedDBMapRoute(Ljava/util/List;)V

    .line 572
    :goto_0
    return-void

    .line 570
    :cond_1
    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->drawMapRoute(Ljava/util/List;Ljava/util/List;)V

    goto :goto_0
.end method

.method private getColor(ILjava/util/List;)I
    .locals 4
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 577
    .local p2, "speedItems":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    .line 578
    .local v1, "speedItemsSize":I
    const/4 v0, 0x0

    .line 580
    .local v0, "speed":F
    if-lez v1, :cond_1

    .line 581
    add-int/lit8 v2, v1, -0x1

    if-gt v2, p1, :cond_0

    .line 582
    add-int/lit8 v2, v1, -0x1

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 590
    :goto_0
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMaxSpeed:F

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMinSpeed:F

    invoke-static {v0, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getLineColor(FFF)I

    move-result v2

    return v2

    .line 584
    :cond_0
    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_0

    .line 587
    :cond_1
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMaxSpeed:F

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMinSpeed:F

    add-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float v0, v2, v3

    goto :goto_0
.end method

.method private getDeviceGroupType(Ljava/lang/String;)J
    .locals 12
    .param p1, "userDeviceId"    # Ljava/lang/String;

    .prologue
    const-wide/16 v10, 0x0

    .line 971
    if-nez p1, :cond_1

    move-wide v0, v10

    .line 993
    :cond_0
    :goto_0
    return-wide v0

    .line 973
    :cond_1
    const-string v0, "_"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 974
    .local v9, "tempDeviceIDArray":[Ljava/lang/String;
    move-object v8, p1

    .line 975
    .local v8, "tempDeviceID":Ljava/lang/String;
    const/4 v0, 0x0

    aget-object v0, v9, v0

    const-string v1, ""

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 976
    .local v6, "deviceID":Ljava/lang/String;
    const-string v0, "_"

    invoke-virtual {v6, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 978
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "select device_group_type, device_id from user_device where device_id == \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 980
    .local v3, "tempSelection":Ljava/lang/String;
    const/4 v7, 0x0

    .line 982
    .local v7, "tempCursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 983
    if-eqz v7, :cond_3

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 985
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 986
    const-string v0, "device_group_type"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 990
    if-eqz v7, :cond_0

    .line 991
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 990
    :cond_3
    if-eqz v7, :cond_4

    .line 991
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_4
    move-wide v0, v10

    .line 993
    goto :goto_0

    .line 990
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_5

    .line 991
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
.end method

.method private getExtras()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    if-nez v0, :cond_0

    .line 273
    :goto_0
    return-void

    .line 259
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_workout_end_mode"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mEndMode:Z

    .line 260
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_during_try_it"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mTryit_map:Z

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_sync_mode"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mPlaceMode:I

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_sync_time"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mSync_time:J

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_sync_exercise_info_db_id"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mExerciseId:J

    .line 264
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_sync_activity_type"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mActivityType:I

    .line 265
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_sync_distance"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mDistance:F

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_sync_duration"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mDuration:F

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_sync_calories"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mCalories:F

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_sync_max_hrm"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMaxHrm:F

    .line 269
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_sync_max_speed"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMaxSpeed:F

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_sync_avg_hrm"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mAvgHrm:F

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_sync_avg_speed"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mAvgSpeed:F

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_sync_elevation"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mElevation:F

    goto/16 :goto_0
.end method

.method private getHeartRateData()V
    .locals 15

    .prologue
    const/4 v14, 0x1

    const/4 v13, 0x0

    .line 895
    const/4 v12, 0x0

    .line 896
    .local v12, "hrmValue":I
    const-wide/16 v9, 0x0

    .local v9, "heartrateSampleTime":J
    const-wide/16 v7, 0x0

    .line 897
    .local v7, "deviceGroupType":J
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v13

    const-string v0, "heart_rate_per_min"

    aput-object v0, v2, v14

    const/4 v0, 0x2

    const-string/jumbo v1, "sample_time"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string/jumbo v1, "user_device__id"

    aput-object v1, v2, v0

    .line 901
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "exercise__id =?  AND user_device__id LIKE \'10008%\'"

    .line 903
    .local v3, "selectionClause":Ljava/lang/String;
    new-array v4, v14, [Ljava/lang/String;

    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mRowId:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v13

    .line 906
    .local v4, "selectionArg":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 907
    .local v5, "sortOrder":Ljava/lang/String;
    const/4 v6, 0x0

    .line 910
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$RealTimeData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 912
    if-eqz v6, :cond_2

    .line 913
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 914
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 915
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_2

    .line 916
    const-string v0, "heart_rate_per_min"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 917
    const-string/jumbo v0, "sample_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 918
    const-string/jumbo v0, "user_device__id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getDeviceGroupType(Ljava/lang/String;)J

    move-result-wide v7

    .line 919
    const-wide/32 v0, 0x57e41

    cmp-long v0, v7, v0

    if-nez v0, :cond_0

    .line 920
    new-instance v11, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;

    invoke-direct {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;-><init>()V

    .line 921
    .local v11, "hrmData":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    int-to-float v0, v12

    invoke-virtual {v11, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->setHeartRateVal(F)V

    .line 922
    invoke-virtual {v11, v9, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->setSampleTime(J)V

    .line 923
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapHRMList:Ljava/util/ArrayList;

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 925
    .end local v11    # "hrmData":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 932
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    .line 933
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 932
    :cond_2
    if-eqz v6, :cond_3

    .line 933
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 936
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapHRMList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 937
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->setLocationtoHRMData()V

    .line 938
    :cond_4
    return-void
.end method

.method private getKmFromMeter(J)Ljava/lang/String;
    .locals 3
    .param p1, "metervalue"    # J

    .prologue
    .line 675
    const-wide/16 v1, 0x64

    div-long v1, p1, v1

    long-to-int v1, v1

    mul-int/lit8 v1, v1, 0x64

    int-to-float v0, v1

    .line 676
    .local v0, "maxSpeed":F
    float-to-long v1, v0

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/UnitUtils$UnitConverter;->getKmFromMeter(J)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getMaxHR(I)I
    .locals 5
    .param p1, "age"    # I

    .prologue
    const/16 v4, 0x32

    const/16 v3, 0x2d

    const/16 v2, 0x28

    const/16 v1, 0x23

    const/16 v0, 0x1e

    .line 1358
    if-lez p1, :cond_0

    if-ge p1, v0, :cond_0

    .line 1359
    const/16 v0, 0xc8

    .line 1378
    :goto_0
    return v0

    .line 1360
    :cond_0
    if-lt p1, v0, :cond_1

    if-ge p1, v1, :cond_1

    .line 1361
    const/16 v0, 0xbe

    goto :goto_0

    .line 1362
    :cond_1
    if-lt p1, v1, :cond_2

    if-ge p1, v2, :cond_2

    .line 1363
    const/16 v0, 0xb9

    goto :goto_0

    .line 1364
    :cond_2
    if-lt p1, v2, :cond_3

    if-ge p1, v3, :cond_3

    .line 1365
    const/16 v0, 0xb4

    goto :goto_0

    .line 1366
    :cond_3
    if-lt p1, v3, :cond_4

    if-ge p1, v4, :cond_4

    .line 1367
    const/16 v0, 0xaf

    goto :goto_0

    .line 1368
    :cond_4
    if-lt p1, v4, :cond_5

    const/16 v0, 0x37

    if-ge p1, v0, :cond_5

    .line 1369
    const/16 v0, 0xaa

    goto :goto_0

    .line 1370
    :cond_5
    const/16 v0, 0x37

    if-lt p1, v0, :cond_6

    const/16 v0, 0x3c

    if-ge p1, v0, :cond_6

    .line 1371
    const/16 v0, 0xa5

    goto :goto_0

    .line 1372
    :cond_6
    const/16 v0, 0x3c

    if-lt p1, v0, :cond_7

    const/16 v0, 0x41

    if-ge p1, v0, :cond_7

    .line 1373
    const/16 v0, 0xa0

    goto :goto_0

    .line 1374
    :cond_7
    const/16 v0, 0x41

    if-lt p1, v0, :cond_8

    const/16 v0, 0x46

    if-ge p1, v0, :cond_8

    .line 1375
    const/16 v0, 0x9b

    goto :goto_0

    .line 1376
    :cond_8
    const/16 v0, 0x46

    if-lt p1, v0, :cond_9

    .line 1377
    const/16 v0, 0x96

    goto :goto_0

    .line 1378
    :cond_9
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getMilesFromMeters(J)Ljava/lang/String;
    .locals 8
    .param p1, "metervalue"    # J

    .prologue
    const-wide/high16 v6, 0x4024000000000000L    # 10.0

    .line 681
    long-to-double v2, p1

    const-wide v4, 0x3f445c6dea1359a4L    # 6.2137E-4

    mul-double v0, v2, v4

    .line 682
    .local v0, "mi":D
    mul-double v2, v0, v6

    double-to-int v2, v2

    int-to-double v2, v2

    div-double v0, v2, v6

    .line 683
    new-instance v2, Ljava/text/DecimalFormat;

    const-string v3, "0.0"

    invoke-direct {v2, v3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private getSeedDataFormDB()V
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 835
    const-string v6, "AVG_SPEED"

    .line 836
    .local v6, "avgColumn":Ljava/lang/String;
    const-string v9, "MIN_SPEED"

    .line 837
    .local v9, "minColumn":Ljava/lang/String;
    const-string v8, "MAX_SPEED"

    .line 838
    .local v8, "maxColumn":Ljava/lang/String;
    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v11

    const-string/jumbo v0, "speed_per_hour"

    aput-object v0, v2, v12

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MAX(speed_per_hour) AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v13

    const/4 v0, 0x3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "MIN(speed_per_hour) AS "

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    const/4 v0, 0x4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "AVG(speed_per_hour) AS "

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    .line 844
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "exercise__id =?  AND update_time>? "

    .line 846
    .local v3, "selectionClause":Ljava/lang/String;
    new-array v4, v13, [Ljava/lang/String;

    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mRowId:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v11

    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v12

    .line 849
    .local v4, "selectionArg":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 850
    .local v5, "sortOrder":Ljava/lang/String;
    const/4 v7, 0x0

    .line 853
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$RealTimeData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 855
    if-eqz v7, :cond_0

    .line 856
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 857
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 858
    invoke-interface {v7, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMaxSpeed:F

    .line 859
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    long-to-float v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mAvgSpeed:F

    .line 860
    invoke-interface {v7, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMinSpeed:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 864
    :cond_0
    if-eqz v7, :cond_1

    .line 865
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 867
    :cond_1
    return-void

    .line 864
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_2

    .line 865
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private getUserAge()I
    .locals 9

    .prologue
    const/4 v8, 0x6

    const/4 v7, 0x1

    .line 1338
    new-instance v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v3, v5}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 1340
    .local v3, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    new-instance v1, Ljava/util/GregorianCalendar;

    invoke-direct {v1}, Ljava/util/GregorianCalendar;-><init>()V

    .line 1341
    .local v1, "birth":Ljava/util/Calendar;
    new-instance v4, Ljava/util/GregorianCalendar;

    invoke-direct {v4}, Ljava/util/GregorianCalendar;-><init>()V

    .line 1343
    .local v4, "today":Ljava/util/Calendar;
    new-instance v0, Ljava/util/Date;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDateStringToLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-direct {v0, v5, v6}, Ljava/util/Date;-><init>(J)V

    .line 1345
    .local v0, "bDate":Ljava/util/Date;
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1346
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1348
    const/4 v2, 0x0

    .line 1349
    .local v2, "factor":I
    invoke-virtual {v4, v8}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual {v1, v8}, Ljava/util/Calendar;->get(I)I

    move-result v6

    if-ge v5, v6, :cond_0

    .line 1350
    const/4 v2, -0x1

    .line 1353
    :cond_0
    invoke-virtual {v4, v7}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual {v1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v6

    sub-int/2addr v5, v6

    add-int/2addr v5, v2

    return v5
.end method

.method private isLogEntryFromWearable(I)Z
    .locals 1
    .param p1, "dataType"    # I

    .prologue
    .line 419
    const/16 v0, 0x2724

    if-eq p1, v0, :cond_0

    const/16 v0, 0x2728

    if-eq p1, v0, :cond_0

    const/16 v0, 0x2726

    if-eq p1, v0, :cond_0

    const/16 v0, 0x2723

    if-eq p1, v0, :cond_0

    const/16 v0, 0x272e

    if-eq p1, v0, :cond_0

    const/16 v0, 0x2727

    if-ne p1, v0, :cond_1

    .line 425
    :cond_0
    const/4 v0, 0x1

    .line 427
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private makeHRMarker(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;Ljava/util/ArrayList;)V
    .locals 6
    .param p1, "hrData"    # Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "markedHRData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;>;"
    const v5, 0x3727c5ac    # 1.0E-5f

    const/high16 v4, -0x40800000    # -1.0f

    .line 1015
    if-nez p1, :cond_1

    .line 1040
    :cond_0
    :goto_0
    return-void

    .line 1017
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getLatitude()F

    move-result v3

    cmpl-float v3, v3, v4

    if-nez v3, :cond_2

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getLongitude()F

    move-result v3

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_0

    .line 1021
    :cond_2
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_3

    .line 1022
    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1025
    :cond_3
    const/4 v2, 0x0

    .line 1026
    .local v2, "skip":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_4

    .line 1027
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;

    .line 1029
    .local v0, "data2":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getLatitude()F

    move-result v3

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getLatitude()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v3, v3, v5

    if-gez v3, :cond_5

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getLongitude()F

    move-result v3

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getLongitude()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v3, v3, v5

    if-gez v3, :cond_5

    .line 1031
    const-string v3, "ExerciseProDetailsMapFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Skipping: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getHeartRateVal()F

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1032
    const/4 v2, 0x1

    .line 1036
    .end local v0    # "data2":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    :cond_4
    if-nez v2, :cond_0

    .line 1037
    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1026
    .restart local v0    # "data2":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private makeHRMarker(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1006
    .local p1, "hrDatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 1007
    .local v2, "markedHRData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;

    .line 1008
    .local v0, "hrData":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->makeHRMarker(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 1010
    .end local v0    # "hrData":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapHRMList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 1011
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapHRMList:Ljava/util/ArrayList;

    .line 1012
    return-void
.end method

.method private playMapFromDB()V
    .locals 32

    .prologue
    .line 431
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 432
    .local v21, "locationItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;>;"
    new-instance v29, Ljava/util/ArrayList;

    invoke-direct/range {v29 .. v29}, Ljava/util/ArrayList;-><init>()V

    .line 434
    .local v29, "speedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;>;"
    const-string v6, "ExerciseProDetailsMapFragment"

    const-string/jumbo v7, "playMapFromDB()"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getLastLocation(Landroid/content/Context;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 436
    new-instance v6, Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    invoke-direct {v6}, Lcom/amap/api/maps2d/model/LatLngBounds$Builder;-><init>()V

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->builder:Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    .line 437
    new-instance v19, Lcom/amap/api/maps2d/model/LatLng;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getLastLocation(Landroid/content/Context;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v6

    iget-wide v6, v6, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getLastLocation(Landroid/content/Context;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v8

    iget-wide v8, v8, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    move-object/from16 v0, v19

    invoke-direct {v0, v6, v7, v8, v9}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    .line 438
    .local v19, "lat":Lcom/amap/api/maps2d/model/LatLng;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->builder:Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Lcom/amap/api/maps2d/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    .line 439
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->builder:Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    invoke-virtual {v7}, Lcom/amap/api/maps2d/model/LatLngBounds$Builder;->build()Lcom/amap/api/maps2d/model/LatLngBounds;

    move-result-object v7

    const/16 v8, 0x168

    const/16 v9, 0x220

    const/16 v30, 0x3

    move/from16 v0, v30

    invoke-static {v7, v8, v9, v0}, Lcom/amap/api/maps2d/CameraUpdateFactory;->newLatLngBounds(Lcom/amap/api/maps2d/model/LatLngBounds;III)Lcom/amap/api/maps2d/CameraUpdate;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/amap/api/maps2d/AMap;->moveCamera(Lcom/amap/api/maps2d/CameraUpdate;)V

    .line 442
    .end local v19    # "lat":Lcom/amap/api/maps2d/model/LatLng;
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mEndMode:Z

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mRowId:J

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getSelectionClause(ZJ)Ljava/lang/String;

    move-result-object v4

    .line 443
    .local v4, "selectionClause":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "data_type"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " = 300001"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 446
    .local v24, "selectionClause2":Ljava/lang/String;
    const-string v5, "create_time ASC"

    .line 448
    .local v5, "sortOder":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mEndMode:Z

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mRowId:J

    invoke-static/range {v4 .. v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getLocationItems(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentResolver;ZJ)Ljava/util/ArrayList;

    move-result-object v21

    .line 450
    const-string v4, ""

    .line 451
    const-string v6, "ExerciseProDetailsMapFragment"

    const-string v7, "Read cache db."

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 452
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mEndMode:Z

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mRowId:J

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getSelectionClause(ZJ)Ljava/lang/String;

    move-result-object v4

    .line 455
    const-string v5, "create_time ASC"

    .line 456
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    move-object/from16 v0, v24

    invoke-static {v0, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getSpeedItems(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentResolver;)Ljava/util/ArrayList;

    move-result-object v29

    .line 458
    if-nez v21, :cond_1

    .line 459
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mNoDataNotiText:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 564
    :goto_0
    return-void

    .line 461
    :cond_1
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_2

    .line 462
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mNoDataNotiText:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 466
    :cond_2
    sget v6, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapMode:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->setAMapType(ILcom/amap/api/maps2d/AMap;)Lcom/amap/api/maps2d/AMap;

    move-result-object v6

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    .line 468
    new-instance v6, Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    invoke-direct {v6}, Lcom/amap/api/maps2d/model/LatLngBounds$Builder;-><init>()V

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->boundsBuilder:Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    .line 470
    const/16 v23, 0x0

    .line 471
    .local v23, "maxSpeedIndex":I
    const/16 v22, 0x0

    .line 472
    .local v22, "mapPathIndex":I
    const/16 v28, 0x0

    .line 473
    .local v28, "speedIndex":I
    const/16 v26, 0x1

    .line 474
    .local v26, "spaceNumber":I
    const/4 v11, 0x0

    .line 475
    .local v11, "deltaSpeed":F
    const/16 v25, 0x0

    .line 476
    .local v25, "setSpeed":F
    if-eqz v29, :cond_3

    .line 477
    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v23, v6, -0x1

    .line 480
    :cond_3
    const-string v6, "ExerciseProDetailsMapFragment"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "No. of speeditems : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "  No. of locationItems: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 483
    .local v14, "historyLocation":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;>;"
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 484
    .local v15, "historySpeed":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mDataType:I

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->isLogEntryFromWearable(I)Z

    move-result v17

    .line 485
    .local v17, "isWearableLogEntry":Z
    const/16 v27, 0x1

    .line 487
    .local v27, "speedDistributed":Z
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_11

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    .line 488
    .local v20, "location":Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;
    move-object/from16 v0, v20

    invoke-interface {v14, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 489
    if-eqz v29, :cond_4

    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_7

    .line 490
    :cond_4
    const-string v6, "ExerciseProDetailsMapFragment"

    const-string v7, "Speed DB Empty"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 537
    :cond_5
    :goto_2
    new-instance v13, Lcom/amap/api/maps2d/model/LatLng;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLatitude()D

    move-result-wide v6

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLongitude()D

    move-result-wide v8

    invoke-direct {v13, v6, v7, v8, v9}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    .line 539
    .local v13, "historyLatLng":Lcom/amap/api/maps2d/model/LatLng;
    if-nez v22, :cond_10

    .line 541
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    new-instance v7, Lcom/amap/api/maps2d/model/MarkerOptions;

    invoke-direct {v7}, Lcom/amap/api/maps2d/model/MarkerOptions;-><init>()V

    invoke-virtual {v7, v13}, Lcom/amap/api/maps2d/model/MarkerOptions;->position(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/MarkerOptions;

    move-result-object v7

    const/high16 v8, 0x3f000000    # 0.5f

    const/high16 v9, 0x3f000000    # 0.5f

    invoke-virtual {v7, v8, v9}, Lcom/amap/api/maps2d/model/MarkerOptions;->anchor(FF)Lcom/amap/api/maps2d/model/MarkerOptions;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/amap/api/maps2d/AMap;->addMarker(Lcom/amap/api/maps2d/model/MarkerOptions;)Lcom/amap/api/maps2d/model/Marker;

    move-result-object v6

    const v7, 0x7f0203fc

    invoke-static {v7}, Lcom/amap/api/maps2d/model/BitmapDescriptorFactory;->fromResource(I)Lcom/amap/api/maps2d/model/BitmapDescriptor;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/amap/api/maps2d/model/Marker;->setIcon(Lcom/amap/api/maps2d/model/BitmapDescriptor;)V

    .line 543
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    const/high16 v7, 0x41880000    # 17.0f

    invoke-static {v13, v7}, Lcom/amap/api/maps2d/CameraUpdateFactory;->newLatLngZoom(Lcom/amap/api/maps2d/model/LatLng;F)Lcom/amap/api/maps2d/CameraUpdate;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/amap/api/maps2d/AMap;->moveCamera(Lcom/amap/api/maps2d/CameraUpdate;)V

    .line 553
    :cond_6
    :goto_3
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->boundsBuilder:Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    invoke-virtual {v6, v13}, Lcom/amap/api/maps2d/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    .line 554
    add-int/lit8 v22, v22, 0x1

    .line 555
    goto :goto_1

    .line 491
    .end local v13    # "historyLatLng":Lcom/amap/api/maps2d/model/LatLng;
    :cond_7
    if-eqz v17, :cond_a

    .line 492
    move-object/from16 v0, v29

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->getCreateTime()J

    move-result-wide v6

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getCreateTime()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-ltz v6, :cond_8

    .line 493
    add-int/lit8 v26, v26, 0x1

    goto :goto_2

    .line 495
    :cond_8
    move-object/from16 v0, v29

    move/from16 v1, v28

    move/from16 v2, v26

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getDeltaSpeed(Ljava/util/ArrayList;II)F

    move-result v11

    .line 497
    const/16 v18, 0x0

    .local v18, "j":I
    :goto_4
    move/from16 v0, v18

    move/from16 v1, v26

    if-ge v0, v1, :cond_9

    .line 498
    add-float v25, v25, v11

    .line 499
    invoke-static/range {v25 .. v25}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-interface {v15, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 497
    add-int/lit8 v18, v18, 0x1

    goto :goto_4

    .line 502
    :cond_9
    const/16 v26, 0x1

    .line 503
    add-int/lit8 v28, v28, 0x1

    .line 504
    move/from16 v0, v28

    move/from16 v1, v23

    if-le v0, v1, :cond_5

    .line 505
    move/from16 v28, v23

    goto/16 :goto_2

    .line 509
    .end local v18    # "j":I
    :cond_a
    if-eqz v27, :cond_c

    .line 510
    :goto_5
    add-int/lit8 v6, v23, -0x1

    move/from16 v0, v28

    if-gt v0, v6, :cond_b

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getCreateTime()J

    move-result-wide v7

    move-object/from16 v0, v29

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->getCreateTime()J

    move-result-wide v30

    cmp-long v6, v7, v30

    if-lez v6, :cond_b

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getCreateTime()J

    move-result-wide v7

    add-int/lit8 v6, v28, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->getCreateTime()J

    move-result-wide v30

    cmp-long v6, v7, v30

    if-lez v6, :cond_b

    .line 511
    add-int/lit8 v28, v28, 0x1

    goto :goto_5

    .line 513
    :cond_b
    const/16 v27, 0x0

    .line 516
    :cond_c
    move-object/from16 v0, v29

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->getCreateTime()J

    move-result-wide v6

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getCreateTime()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-ltz v6, :cond_d

    .line 517
    add-int/lit8 v26, v26, 0x1

    goto/16 :goto_2

    .line 519
    :cond_d
    move-object/from16 v0, v29

    move/from16 v1, v28

    move/from16 v2, v26

    move/from16 v3, v25

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getDeltaSpeed(Ljava/util/ArrayList;IIF)F

    move-result v11

    .line 520
    const/4 v6, 0x1

    move/from16 v0, v26

    if-le v0, v6, :cond_e

    .line 521
    const/16 v27, 0x1

    .line 524
    :cond_e
    const/16 v18, 0x0

    .restart local v18    # "j":I
    :goto_6
    move/from16 v0, v18

    move/from16 v1, v26

    if-ge v0, v1, :cond_f

    .line 525
    add-float v25, v25, v11

    .line 526
    invoke-static/range {v25 .. v25}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-interface {v15, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 524
    add-int/lit8 v18, v18, 0x1

    goto :goto_6

    .line 529
    :cond_f
    const/16 v26, 0x1

    .line 530
    add-int/lit8 v28, v28, 0x1

    .line 531
    move/from16 v0, v28

    move/from16 v1, v23

    if-le v0, v1, :cond_5

    .line 532
    move/from16 v28, v23

    goto/16 :goto_2

    .line 544
    .end local v18    # "j":I
    .restart local v13    # "historyLatLng":Lcom/amap/api/maps2d/model/LatLng;
    :cond_10
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    move/from16 v0, v22

    if-ne v0, v6, :cond_6

    .line 546
    const-string v6, "ExerciseProDetailsMapFragment"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "playMapFromDB() 7 mapPathIndex"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 548
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    new-instance v7, Lcom/amap/api/maps2d/model/MarkerOptions;

    invoke-direct {v7}, Lcom/amap/api/maps2d/model/MarkerOptions;-><init>()V

    invoke-virtual {v7, v13}, Lcom/amap/api/maps2d/model/MarkerOptions;->position(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/MarkerOptions;

    move-result-object v7

    const/high16 v8, 0x3f000000    # 0.5f

    const/high16 v9, 0x3f000000    # 0.5f

    invoke-virtual {v7, v8, v9}, Lcom/amap/api/maps2d/model/MarkerOptions;->anchor(FF)Lcom/amap/api/maps2d/model/MarkerOptions;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/amap/api/maps2d/AMap;->addMarker(Lcom/amap/api/maps2d/model/MarkerOptions;)Lcom/amap/api/maps2d/model/Marker;

    move-result-object v6

    const v7, 0x7f0203f1

    invoke-static {v7}, Lcom/amap/api/maps2d/model/BitmapDescriptorFactory;->fromResource(I)Lcom/amap/api/maps2d/model/BitmapDescriptor;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/amap/api/maps2d/model/Marker;->setIcon(Lcom/amap/api/maps2d/model/BitmapDescriptor;)V

    goto/16 :goto_3

    .line 558
    .end local v13    # "historyLatLng":Lcom/amap/api/maps2d/model/LatLng;
    .end local v20    # "location":Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;
    :cond_11
    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1, v14, v15}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->drawRoute(Ljava/util/ArrayList;Ljava/util/List;Ljava/util/List;)V

    .line 561
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->boundsBuilder:Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    invoke-virtual {v6}, Lcom/amap/api/maps2d/model/LatLngBounds$Builder;->build()Lcom/amap/api/maps2d/model/LatLngBounds;

    move-result-object v10

    .line 562
    .local v10, "bounds":Lcom/amap/api/maps2d/model/LatLngBounds;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v12

    .line 563
    .local v12, "display":Landroid/util/DisplayMetrics;
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mIsMiniMode:Z

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    invoke-static {v10, v12, v6, v7, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->panAllRouteOnMap(Ljava/lang/Object;Landroid/util/DisplayMetrics;ZLcom/google/android/gms/maps/GoogleMap;Lcom/amap/api/maps2d/AMap;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/amap/api/maps2d/AMap;

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    goto/16 :goto_0
.end method

.method private setHRMList(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 998
    .local p1, "hrDatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;>;"
    if-nez p1, :cond_1

    .line 1003
    :cond_0
    :goto_0
    return-void

    .line 1000
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1002
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->makeHRMarker(Ljava/util/List;)V

    goto :goto_0
.end method

.method private setHRMStateBar(I)V
    .locals 11
    .param p1, "bpm"    # I

    .prologue
    const/4 v8, 0x2

    const/4 v10, 0x0

    .line 1257
    new-array v0, v8, [I

    fill-array-data v0, :array_0

    .line 1258
    .local v0, "avgRange":[I
    new-array v6, v8, [I

    iget v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->minHRzone:I

    aput v8, v6, v10

    const/4 v8, 0x1

    iget v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->maxHRzone:I

    aput v9, v6, v8

    .line 1259
    .local v6, "percentile":[I
    const/16 v3, 0xc8

    .line 1260
    .local v3, "maxHR":I
    const/4 v1, 0x0

    .line 1261
    .local v1, "greenBarMargin":I
    const/4 v2, 0x0

    .line 1262
    .local v2, "greenBarWidth":I
    const/16 v8, 0x10c

    invoke-direct {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->convertDptoPx(I)I

    move-result v7

    .line 1263
    .local v7, "width":I
    const/4 v4, 0x1

    .line 1265
    .local v4, "n":I
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->minBpmText:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 1267
    .local v5, "number":I
    iget v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->maxHRzone:I

    iget v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->minHRzone:I

    sub-int/2addr v8, v9

    int-to-float v8, v8

    int-to-float v9, v3

    div-float/2addr v8, v9

    int-to-float v9, v7

    mul-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 1268
    iget v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->minHRzone:I

    int-to-float v8, v8

    int-to-float v9, v3

    div-float/2addr v8, v9

    int-to-float v9, v7

    mul-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 1270
    :goto_0
    div-int/lit8 v5, v5, 0xa

    if-eqz v5, :cond_0

    .line 1271
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1274
    :cond_0
    const/4 v8, 0x3

    if-lt v4, v8, :cond_1

    .line 1275
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->minBpmText:Landroid/widget/TextView;

    add-int/lit8 v9, v1, 0x10

    invoke-virtual {v8, v9, v10, v10, v10}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1278
    :goto_1
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->maxBpmText:Landroid/widget/TextView;

    add-int v9, v1, v2

    invoke-virtual {v8, v9, v10, v10, v10}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1280
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mStateBar:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;

    invoke-virtual {v8, p1, v0, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->moveToPolygon(I[I[I)V

    .line 1281
    return-void

    .line 1277
    :cond_1
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->minBpmText:Landroid/widget/TextView;

    add-int/lit8 v9, v1, 0x17

    invoke-virtual {v8, v9, v10, v10, v10}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_1

    .line 1257
    :array_0
    .array-data 4
        0x0
        0xc8
    .end array-data
.end method

.method private setHeartRateMarkerOnMap()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1044
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapHRMList:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 1045
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapHRMList:Ljava/util/ArrayList;

    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->setHRMList(Ljava/util/List;)V

    .line 1046
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapHRMList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 1047
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapHRMList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;

    .line 1048
    .local v1, "hrmData":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getHeartRateVal()F

    move-result v4

    cmpl-float v4, v4, v8

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getLatitude()F

    move-result v4

    cmpl-float v4, v4, v8

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getLongitude()F

    move-result v4

    cmpl-float v4, v4, v8

    if-eqz v4, :cond_0

    .line 1049
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f02031c

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getHeartRateVal()F

    move-result v7

    float-to-int v7, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v4, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->drawTextToBitmap(Landroid/content/Context;ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1051
    .local v0, "bm":Landroid/graphics/Bitmap;
    new-instance v3, Lcom/amap/api/maps2d/model/LatLng;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getLatitude()F

    move-result v4

    float-to-double v4, v4

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getLongitude()F

    move-result v6

    float-to-double v6, v6

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    .line 1052
    .local v3, "pos":Lcom/amap/api/maps2d/model/LatLng;
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    new-instance v5, Lcom/amap/api/maps2d/model/MarkerOptions;

    invoke-direct {v5}, Lcom/amap/api/maps2d/model/MarkerOptions;-><init>()V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "HRM_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getHeartRateVal()F

    move-result v7

    float-to-int v7, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/amap/api/maps2d/model/MarkerOptions;->snippet(Ljava/lang/String;)Lcom/amap/api/maps2d/model/MarkerOptions;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/amap/api/maps2d/model/MarkerOptions;->position(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/MarkerOptions;

    move-result-object v5

    const/high16 v6, 0x3f000000    # 0.5f

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v5, v6, v7}, Lcom/amap/api/maps2d/model/MarkerOptions;->anchor(FF)Lcom/amap/api/maps2d/model/MarkerOptions;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/amap/api/maps2d/AMap;->addMarker(Lcom/amap/api/maps2d/model/MarkerOptions;)Lcom/amap/api/maps2d/model/Marker;

    move-result-object v4

    invoke-static {v0}, Lcom/amap/api/maps2d/model/BitmapDescriptorFactory;->fromBitmap(Landroid/graphics/Bitmap;)Lcom/amap/api/maps2d/model/BitmapDescriptor;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/amap/api/maps2d/model/Marker;->setIcon(Lcom/amap/api/maps2d/model/BitmapDescriptor;)V

    .line 1054
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    new-instance v5, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$9;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$9;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)V

    invoke-virtual {v4, v5}, Lcom/amap/api/maps2d/AMap;->setOnMarkerClickListener(Lcom/amap/api/maps2d/AMap$OnMarkerClickListener;)V

    .line 1046
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    .end local v3    # "pos":Lcom/amap/api/maps2d/model/LatLng;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 1074
    .end local v1    # "hrmData":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    :cond_1
    return-void
.end method

.method private setSpeedValue()V
    .locals 9

    .prologue
    const v8, 0x7f090810

    const v7, 0x7f09080f

    .line 643
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getDistanceUnitFromSharedPreferences()Ljava/lang/String;

    move-result-object v3

    const-string v4, "km"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 648
    .local v0, "kmUnit":Z
    const-string v3, "ExerciseProDetailsMapFragment"

    const-string/jumbo v4, "setSpeedValue()"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 649
    if-eqz v0, :cond_0

    .line 650
    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMinSpeed:F

    float-to-long v3, v3

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getKmFromMeter(J)Ljava/lang/String;

    move-result-object v2

    .line 651
    .local v2, "value":Ljava/lang/String;
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 657
    .local v1, "unit":Ljava/lang/String;
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mSlowSpeed:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->SPACE:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 658
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mSlowSpeed:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->SPACE:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 660
    if-eqz v0, :cond_1

    .line 661
    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMaxSpeed:F

    float-to-long v3, v3

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getKmFromMeter(J)Ljava/lang/String;

    move-result-object v2

    .line 662
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 669
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mFastSpeed:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->SPACE:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 670
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mFastSpeed:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->SPACE:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 671
    return-void

    .line 653
    .end local v1    # "unit":Ljava/lang/String;
    .end local v2    # "value":Ljava/lang/String;
    :cond_0
    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMinSpeed:F

    float-to-long v3, v3

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getMilesFromMeters(J)Ljava/lang/String;

    move-result-object v2

    .line 654
    .restart local v2    # "value":Ljava/lang/String;
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "unit":Ljava/lang/String;
    goto/16 :goto_0

    .line 665
    :cond_1
    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMaxSpeed:F

    float-to-long v3, v3

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getMilesFromMeters(J)Ljava/lang/String;

    move-result-object v2

    .line 666
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method private setUpMap()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 383
    const-string v1, "ExerciseProDetailsMapFragment"

    const-string/jumbo v2, "setUpMap()"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    invoke-virtual {v1, v3}, Lcom/amap/api/maps2d/AMap;->setMyLocationEnabled(Z)V

    .line 385
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/amap/api/maps2d/AMap;->setMapType(I)V

    .line 386
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$AMapLoadedListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$AMapLoadedListener;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$1;)V

    .line 387
    .local v0, "mAMapLoadedListener":Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$AMapLoadedListener;
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    invoke-virtual {v1, v0}, Lcom/amap/api/maps2d/AMap;->setOnMapLoadedListener(Lcom/amap/api/maps2d/AMap$OnMapLoadedListener;)V

    .line 388
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    invoke-virtual {v1}, Lcom/amap/api/maps2d/AMap;->getMinZoomLevel()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->currentZoomLevel:F

    .line 389
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$3;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)V

    invoke-virtual {v1, v2}, Lcom/amap/api/maps2d/AMap;->setOnCameraChangeListener(Lcom/amap/api/maps2d/AMap$OnCameraChangeListener;)V

    .line 406
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    invoke-virtual {v1}, Lcom/amap/api/maps2d/AMap;->getUiSettings()Lcom/amap/api/maps2d/UiSettings;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mUiSettings:Lcom/amap/api/maps2d/UiSettings;

    .line 407
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mUiSettings:Lcom/amap/api/maps2d/UiSettings;

    invoke-virtual {v1, v3}, Lcom/amap/api/maps2d/UiSettings;->setMyLocationButtonEnabled(Z)V

    .line 408
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mUiSettings:Lcom/amap/api/maps2d/UiSettings;

    invoke-virtual {v1, v3}, Lcom/amap/api/maps2d/UiSettings;->setZoomControlsEnabled(Z)V

    .line 409
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mUiSettings:Lcom/amap/api/maps2d/UiSettings;

    invoke-virtual {v1, v3}, Lcom/amap/api/maps2d/UiSettings;->setCompassEnabled(Z)V

    .line 411
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->playMapFromDB()V

    .line 412
    return-void
.end method

.method private setUpMapIfNeeded(Landroid/view/View;)V
    .locals 4
    .param p1, "contentView"    # Landroid/view/View;

    .prologue
    .line 345
    const-string v1, "ExerciseProDetailsMapFragment"

    const-string/jumbo v2, "setUpMapIfNeeded()"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    .line 348
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    if-nez v1, :cond_0

    .line 349
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapView:Lcom/amap/api/maps2d/MapView;

    invoke-virtual {v1}, Lcom/amap/api/maps2d/MapView;->getMap()Lcom/amap/api/maps2d/AMap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    .line 352
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    if-eqz v1, :cond_2

    .line 353
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mIsMiniMode:Z

    if-eqz v1, :cond_1

    .line 354
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)V

    invoke-virtual {v1, v2}, Lcom/amap/api/maps2d/AMap;->setOnMapClickListener(Lcom/amap/api/maps2d/AMap$OnMapClickListener;)V

    .line 367
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    invoke-virtual {v2}, Lcom/amap/api/maps2d/AMap;->getMapType()I

    move-result v2

    const-string v3, "ExerciseProDetailsMapFragment"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getMapMode(Landroid/app/Activity;ILjava/lang/String;)I

    move-result v1

    sput v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapMode:I

    .line 369
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->setUpMap()V

    .line 370
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mIsMiniMode:Z

    if-nez v1, :cond_2

    .line 371
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->setUpMarkIcon()V

    .line 372
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->updatePhotoMarkIcon()V

    .line 373
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->setHeartRateMarkerOnMap()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 380
    :cond_2
    :goto_0
    return-void

    .line 376
    :catch_0
    move-exception v0

    .line 377
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ExerciseProDetailsMapFragment"

    const-string/jumbo v2, "setUpMap() exception handling"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setUpMarkIcon()V
    .locals 2

    .prologue
    .line 703
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapPhotoMarker:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList;

    .line 704
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapPhotoMarker:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMarkListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList;->setOnUpdatePhotoListListener(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;)V

    .line 705
    return-void
.end method

.method private updatePhotoMarkIcon()V
    .locals 4

    .prologue
    .line 708
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mEndMode:Z

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mRowId:J

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getPhotoDatas(ZLandroid/app/Activity;J)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mPhotoDatas:Ljava/util/ArrayList;

    .line 710
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mPhotoDatas:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 736
    :cond_0
    :goto_0
    return-void

    .line 713
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mPhotoDatas:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 716
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mPhotoDatas:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 717
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapPhotoMarker:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mPhotoDatas:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList;->SetPhotoList(Ljava/util/List;)V

    .line 718
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$5;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)V

    invoke-virtual {v0, v1}, Lcom/amap/api/maps2d/AMap;->setOnMarkerClickListener(Lcom/amap/api/maps2d/AMap$OnMarkerClickListener;)V

    goto :goto_0
.end method


# virtual methods
.method public drawTextToBitmap(Landroid/content/Context;ILjava/lang/String;)Landroid/graphics/Bitmap;
    .locals 14
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "resourceId"    # I
    .param p3, "mText"    # Ljava/lang/String;

    .prologue
    .line 1078
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 1079
    .local v6, "resources":Landroid/content/res/Resources;
    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v10

    iget v7, v10, Landroid/util/DisplayMetrics;->density:F

    .line 1080
    .local v7, "scale":F
    move/from16 v0, p2

    invoke-static {v6, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1082
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v2

    .line 1084
    .local v2, "bitmapConfig":Landroid/graphics/Bitmap$Config;
    if-nez v2, :cond_0

    .line 1085
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 1089
    :cond_0
    const/4 v10, 0x1

    invoke-virtual {v1, v2, v10}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1091
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1093
    .local v4, "canvas":Landroid/graphics/Canvas;
    new-instance v5, Landroid/graphics/Paint;

    const/4 v10, 0x1

    invoke-direct {v5, v10}, Landroid/graphics/Paint;-><init>(I)V

    .line 1095
    .local v5, "paint":Landroid/graphics/Paint;
    const/16 v10, 0x6e

    const/16 v11, 0x6e

    const/16 v12, 0x6e

    invoke-static {v10, v11, v12}, Landroid/graphics/Color;->rgb(III)I

    move-result v10

    invoke-virtual {v5, v10}, Landroid/graphics/Paint;->setColor(I)V

    .line 1097
    const/high16 v10, 0x41400000    # 12.0f

    mul-float/2addr v10, v7

    float-to-int v10, v10

    int-to-float v10, v10

    invoke-virtual {v5, v10}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1099
    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v11, 0x0

    const/high16 v12, 0x3f800000    # 1.0f

    const v13, -0xbbbbbc

    invoke-virtual {v5, v10, v11, v12, v13}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 1102
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 1103
    .local v3, "bounds":Landroid/graphics/Rect;
    const/4 v10, 0x0

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v11

    move-object/from16 v0, p3

    invoke-virtual {v5, v0, v10, v11, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 1106
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v11

    sub-int/2addr v10, v11

    div-int/lit8 v8, v10, 0x2

    .line 1107
    .local v8, "x":I
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v11

    add-int/2addr v10, v11

    div-int/lit8 v9, v10, 0x2

    .line 1108
    .local v9, "y":I
    int-to-float v10, v8

    int-to-float v11, v9

    move-object/from16 v0, p3

    invoke-virtual {v4, v0, v10, v11, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1116
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "bitmapConfig":Landroid/graphics/Bitmap$Config;
    .end local v3    # "bounds":Landroid/graphics/Rect;
    .end local v4    # "canvas":Landroid/graphics/Canvas;
    .end local v5    # "paint":Landroid/graphics/Paint;
    .end local v6    # "resources":Landroid/content/res/Resources;
    .end local v7    # "scale":F
    .end local v8    # "x":I
    .end local v9    # "y":I
    :goto_0
    return-object v1

    .line 1111
    :catch_0
    move-exception v10

    .line 1116
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 4

    .prologue
    .line 888
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/sdk/health/sensor/utils/HealthServiceUtil;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 889
    .local v0, "android_id":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "10008_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 890
    .local v1, "deviceId":Ljava/lang/String;
    return-object v1
.end method

.method public getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 806
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$8;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)V

    return-object v0
.end method

.method public isAbleFinished()Z
    .locals 1

    .prologue
    .line 796
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mPhotoScrollView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mPhotoScrollView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 797
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mPhotoScrollView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->onBackPress()V

    .line 798
    const/4 v0, 0x1

    .line 801
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 788
    const/16 v0, 0x99

    if-ne p1, v0, :cond_0

    .line 789
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mEndMode:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mContentView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 790
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mContentView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->setUpMapIfNeeded(Landroid/view/View;)V

    .line 793
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 219
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 220
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mBundle:Landroid/os/Bundle;

    .line 221
    if-eqz p1, :cond_0

    .line 222
    const-string/jumbo v0, "realtime_workout_end_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mEndMode:Z

    .line 223
    const-string/jumbo v0, "realtime_during_try_it"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mTryit_map:Z

    .line 224
    const-string/jumbo v0, "realtime_sync_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mPlaceMode:I

    .line 225
    const-string/jumbo v0, "realtime_sync_time"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mSync_time:J

    .line 226
    const-string/jumbo v0, "realtime_sync_exercise_info_db_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mExerciseId:J

    .line 227
    const-string/jumbo v0, "realtime_sync_activity_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mActivityType:I

    .line 228
    const-string/jumbo v0, "realtime_sync_distance"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mDistance:F

    .line 229
    const-string/jumbo v0, "realtime_sync_duration"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mDuration:F

    .line 230
    const-string/jumbo v0, "realtime_sync_calories"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mCalories:F

    .line 231
    const-string/jumbo v0, "realtime_sync_max_hrm"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMaxHrm:F

    .line 232
    const-string/jumbo v0, "realtime_sync_max_speed"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMaxSpeed:F

    .line 233
    const-string/jumbo v0, "realtime_sync_avg_hrm"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mAvgHrm:F

    .line 234
    const-string/jumbo v0, "realtime_sync_avg_speed"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mAvgSpeed:F

    .line 235
    const-string/jumbo v0, "realtime_sync_elevation"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mElevation:F

    .line 239
    :goto_0
    return-void

    .line 237
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getExtras()V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v6, 0x7f080700

    const/16 v5, 0x8

    .line 299
    const v2, 0x7f030199

    const/4 v3, 0x0

    invoke-virtual {p1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 301
    .local v0, "contentView":Landroid/view/View;
    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mContentView:Landroid/view/View;

    .line 302
    if-eqz p3, :cond_0

    .line 303
    const-string v2, "mEndMode"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mEndMode:Z

    .line 305
    :cond_0
    const v2, 0x7f08070f

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mNoDataNotiText:Landroid/widget/TextView;

    .line 306
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mNoDataNotiText:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 308
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getSeedDataFormDB()V

    .line 309
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getHeartRateData()V

    .line 311
    const-string v2, "ExerciseProDetailsMapFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onCreateView() mTryit_map : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mTryit_map:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mTryit_map:Z

    if-nez v2, :cond_2

    .line 315
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/amap/api/maps2d/MapsInitializer;->initialize(Landroid/content/Context;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 319
    :goto_0
    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/amap/api/maps2d/MapView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapView:Lcom/amap/api/maps2d/MapView;

    .line 320
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapView:Lcom/amap/api/maps2d/MapView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mBundle:Landroid/os/Bundle;

    invoke-virtual {v2, v3}, Lcom/amap/api/maps2d/MapView;->onCreate(Landroid/os/Bundle;)V

    .line 321
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->setUpMapIfNeeded(Landroid/view/View;)V

    .line 327
    :goto_1
    const v2, 0x7f080719

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mPhotoScrollView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    .line 328
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mPhotoScrollView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mPhotoHorizontalScrollClickListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$OnTumbnailClickListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->setOnTumbnailClickListener(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$OnTumbnailClickListener;)V

    .line 329
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mPhotoScrollView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    invoke-virtual {v2, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->setVisibility(I)V

    .line 331
    const v2, 0x7f080716

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mSlowSpeed:Landroid/widget/TextView;

    .line 332
    const v2, 0x7f080715

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mFastSpeed:Landroid/widget/TextView;

    .line 333
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->setSpeedValue()V

    .line 335
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mIsMiniMode:Z

    if-eqz v2, :cond_1

    .line 336
    const v2, 0x7f080711

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 337
    const v2, 0x7f080717

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 340
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->addListenerViewModeButton()V

    .line 341
    return-object v0

    .line 316
    :catch_0
    move-exception v1

    .line 317
    .local v1, "e":Landroid/os/RemoteException;
    const-string v2, "ExerciseProDetailsMapFragment"

    const-string v3, "AMapServicesNotAvailable Exception in onCreateView()"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 324
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_2
    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 325
    const v2, 0x7f0806f5

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v3, 0x7f020798

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 284
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mTryit_map:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapView:Lcom/amap/api/maps2d/MapView;

    if-eqz v0, :cond_0

    .line 285
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapView:Lcom/amap/api/maps2d/MapView;

    invoke-virtual {v0}, Lcom/amap/api/maps2d/MapView;->onDestroy()V

    .line 287
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onDestroy()V

    .line 288
    return-void
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 292
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onLowMemory()V

    .line 293
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapView:Lcom/amap/api/maps2d/MapView;

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapView:Lcom/amap/api/maps2d/MapView;

    invoke-virtual {v0}, Lcom/amap/api/maps2d/MapView;->onLowMemory()V

    .line 295
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapView:Lcom/amap/api/maps2d/MapView;

    if-eqz v0, :cond_0

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapView:Lcom/amap/api/maps2d/MapView;

    invoke-virtual {v0}, Lcom/amap/api/maps2d/MapView;->onPause()V

    .line 279
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onPause()V

    .line 280
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 782
    if-eqz p1, :cond_0

    .line 783
    const-string v0, "mEndMode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mEndMode:Z

    .line 785
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 243
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onResume()V

    .line 244
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mTryit_map:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapView:Lcom/amap/api/maps2d/MapView;

    if-eqz v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapView:Lcom/amap/api/maps2d/MapView;

    invoke-virtual {v0}, Lcom/amap/api/maps2d/MapView;->onResume()V

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    if-eqz v0, :cond_1

    .line 249
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    invoke-virtual {v1}, Lcom/amap/api/maps2d/AMap;->getMapType()I

    move-result v1

    const-string v2, "ExerciseProDetailsMapFragment"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getMapMode(Landroid/app/Activity;ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapMode:I

    .line 250
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapMode:I

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->setAMapType(ILcom/amap/api/maps2d/AMap;)Lcom/amap/api/maps2d/AMap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMap:Lcom/amap/api/maps2d/AMap;

    .line 252
    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 764
    const-string/jumbo v0, "realtime_workout_end_mode"

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mEndMode:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 765
    const-string/jumbo v0, "realtime_during_try_it"

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mTryit_map:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 766
    const-string/jumbo v0, "realtime_sync_mode"

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mPlaceMode:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 767
    const-string/jumbo v0, "realtime_sync_time"

    iget-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mSync_time:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 768
    const-string/jumbo v0, "realtime_sync_exercise_info_db_id"

    iget-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mExerciseId:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 769
    const-string/jumbo v0, "realtime_sync_activity_type"

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mActivityType:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 770
    const-string/jumbo v0, "realtime_sync_distance"

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mDistance:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 771
    const-string/jumbo v0, "realtime_sync_duration"

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mDuration:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 772
    const-string/jumbo v0, "realtime_sync_calories"

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mCalories:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 773
    const-string/jumbo v0, "realtime_sync_max_hrm"

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMaxHrm:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 774
    const-string/jumbo v0, "realtime_sync_max_speed"

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMaxSpeed:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 775
    const-string/jumbo v0, "realtime_sync_avg_hrm"

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mAvgHrm:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 776
    const-string/jumbo v0, "realtime_sync_avg_speed"

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mAvgSpeed:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 777
    const-string/jumbo v0, "realtime_sync_elevation"

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mElevation:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 778
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapView:Lcom/amap/api/maps2d/MapView;

    invoke-virtual {v0, p1}, Lcom/amap/api/maps2d/MapView;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 779
    return-void
.end method

.method public setLocationtoHRMData()V
    .locals 11

    .prologue
    .line 945
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapHRMList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v8, v0, :cond_3

    .line 946
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapHRMList:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;

    .line 947
    .local v7, "hrmData":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "select location_data.[latitude],location_data.[longitude] from location_data where location_data.[exercise__id]= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mRowId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " and location_data.[sample_time]<= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getSampleTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " order by location_data.[sample_time] desc limit 1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 948
    .local v3, "selectionClause":Ljava/lang/String;
    const/4 v6, 0x0

    .line 951
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 952
    if-eqz v6, :cond_0

    .line 953
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 954
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 955
    const-string v0, "latitude"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v9

    .line 956
    .local v9, "latitude":F
    const-string v0, "longitude"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v10

    .line 957
    .local v10, "longitude":F
    invoke-virtual {v7, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->setLatitude(F)V

    .line 958
    invoke-virtual {v7, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->setLongitude(F)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 963
    .end local v9    # "latitude":F
    .end local v10    # "longitude":F
    :cond_0
    if-eqz v6, :cond_1

    .line 964
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 945
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 963
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 964
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 968
    .end local v3    # "selectionClause":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "hrmData":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    :cond_3
    return-void
.end method

.method public setOnMapClickListener(Lcom/amap/api/maps2d/AMap$OnMapClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/amap/api/maps2d/AMap$OnMapClickListener;

    .prologue
    .line 415
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapClickListener:Lcom/amap/api/maps2d/AMap$OnMapClickListener;

    .line 416
    return-void
.end method

.method public showHRdialog(I)V
    .locals 5
    .param p1, "hrValue"    # I

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x42c80000    # 100.0f

    .line 1121
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->bpm:I

    .line 1122
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getUserAge()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getMaxHR(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    const/high16 v2, 0x42480000    # 50.0f

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->minHRzone:I

    .line 1123
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getUserAge()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getMaxHR(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    const/high16 v2, 0x42aa0000    # 85.0f

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->maxHRzone:I

    .line 1125
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x80

    invoke-virtual {v1, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 1127
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 1130
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1131
    const v1, 0x7f090a4c

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1132
    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1134
    const v1, 0x7f0301a8

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$10;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$10;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1211
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$11;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$11;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1226
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$12;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$12;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1235
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$13;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$13;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1247
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1248
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mAlertBuild_HRM_Result:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-nez v1, :cond_0

    .line 1249
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mAlertBuild_HRM_Result:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 1251
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mAlertBuild_HRM_Result:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "error"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1254
    :cond_1
    return-void
.end method
