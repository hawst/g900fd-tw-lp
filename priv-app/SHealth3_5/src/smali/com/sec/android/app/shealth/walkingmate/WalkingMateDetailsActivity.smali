.class public Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "WalkingMateDetailsActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$ItemHolder;,
        Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$DeleterTask;
    }
.end annotation


# static fields
.field private static final FIELD_CALORIES:I = 0x4

.field private static final FIELD_CLIMBING_STEPS:I = 0x7

.field private static final FIELD_DATE:I = 0x0

.field private static final FIELD_DISTANCE:I = 0x3

.field private static final FIELD_DURATION:I = 0x2

.field private static final FIELD_HEALTHY_STEPS:I = 0x5

.field private static final FIELD_RUNNING_STEPS:I = 0x6

.field private static final FIELD_STEPS:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field private static final TOTAL_FIELDS:I = 0x8

.field private static final mContentDesc:[Ljava/lang/String;


# instance fields
.field private final Details:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[I>;"
        }
    .end annotation
.end field

.field private detailsContainer:Landroid/widget/LinearLayout;

.field private distanceUnit:Ljava/lang/String;

.field private format:Ljava/text/DateFormat;

.field private mCal:I

.field private mCustomName:Ljava/lang/String;

.field private mDate:Ljava/util/Date;

.field private mDeviceType:I

.field private mDistance:F

.field private mDuration:J

.field private mHealthySteps:J

.field private mRunningSteps:I

.field private mTag:Ljava/lang/String;

.field private mTotalStep:I

.field private pedometorItems:[I

.field private unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->TAG:Ljava/lang/String;

    .line 59
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mContentDesc:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 79
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MMMM dd yyyy"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->format:Ljava/text/DateFormat;

    .line 80
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->Details:Ljava/util/List;

    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->pedometorItems:[I

    .line 312
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->prepareShareView()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mTag:Ljava/lang/String;

    return-object v0
.end method

.method private getContents()Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$ItemHolder;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 327
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->pedometorItems:[I

    .line 328
    .local v8, "itemArray":[I
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 330
    .local v6, "holder":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$ItemHolder;>;"
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    array-length v0, v8

    if-ge v7, v0, :cond_2

    .line 331
    aget v0, v8, v7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getValue(I)[Ljava/lang/String;

    move-result-object v2

    .line 333
    .local v2, "value":[Ljava/lang/String;
    if-eqz v2, :cond_1

    array-length v0, v2

    if-lez v0, :cond_1

    aget-object v0, v2, v10

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->Details:Ljava/util/List;

    aget v4, v8, v7

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    aget v3, v0, v10

    .line 335
    .local v3, "iconResId":I
    const-string v1, ""

    .line 336
    .local v1, "itemName":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->Details:Ljava/util/List;

    aget v4, v8, v7

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    const/4 v4, 0x1

    aget v9, v0, v4

    .line 337
    .local v9, "resId":I
    const/4 v0, -0x1

    if-eq v9, v0, :cond_0

    .line 338
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 340
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$ItemHolder;

    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mContentDesc:[Ljava/lang/String;

    aget v5, v8, v7

    aget-object v4, v4, v5

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$ItemHolder;-><init>(Ljava/lang/String;[Ljava/lang/String;ILjava/lang/String;Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$1;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 330
    .end local v1    # "itemName":Ljava/lang/String;
    .end local v3    # "iconResId":I
    .end local v9    # "resId":I
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 343
    .end local v2    # "value":[Ljava/lang/String;
    :cond_2
    return-object v6
.end method

.method private getView(Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$ItemHolder;)Landroid/view/View;
    .locals 8
    .param p1, "item"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$ItemHolder;

    .prologue
    const v7, 0x7f080bd3

    const/4 v6, 0x1

    const v5, 0x7f080bd1

    const/4 v4, 0x0

    .line 295
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03029f

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 296
    .local v0, "contentView":Landroid/view/View;
    const v1, 0x7f0807f8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget v2, p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$ItemHolder;->itemIconResId:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 297
    const v1, 0x7f080bcf

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$ItemHolder;->itemName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 298
    iget-object v1, p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$ItemHolder;->itemValue:[Ljava/lang/String;

    array-length v1, v1

    if-le v1, v6, :cond_0

    .line 299
    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 300
    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$ItemHolder;->itemValue:[Ljava/lang/String;

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 301
    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$ItemHolder;->itemValue:[Ljava/lang/String;

    aget-object v2, v2, v6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 307
    :goto_0
    iget-object v1, p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$ItemHolder;->contentDesc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 309
    return-object v0

    .line 303
    :cond_0
    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 304
    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$ItemHolder;->itemValue:[Ljava/lang/String;

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private initInputTypeLayout()V
    .locals 11

    .prologue
    const v10, 0x7f090bd4

    const/16 v9, 0x2719

    const v8, 0x7f090a8c

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 449
    const v4, 0x7f08073b

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 450
    .local v2, "inputTypeTextView":Landroid/widget/TextView;
    const v4, 0x7f08073a

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 452
    .local v1, "inputTypeLayout":Landroid/widget/LinearLayout;
    const/4 v3, 0x0

    .line 453
    .local v3, "text":Ljava/lang/String;
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mDeviceType:I

    .line 455
    .local v0, "inputType":I
    if-ne v0, v9, :cond_1

    .line 456
    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 461
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mCustomName:Ljava/lang/String;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mCustomName:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 462
    if-ne v0, v9, :cond_2

    .line 463
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 504
    :goto_1
    if-eqz v3, :cond_0

    .line 505
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 507
    :cond_0
    return-void

    .line 458
    :cond_1
    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 466
    :cond_2
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mCustomName:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 469
    :cond_3
    sparse-switch v0, :sswitch_data_0

    .line 499
    const/4 v3, 0x0

    goto :goto_1

    .line 471
    :sswitch_0
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 472
    goto :goto_1

    .line 475
    :sswitch_1
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    const v6, 0x7f090bc2

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 476
    goto :goto_1

    .line 480
    :sswitch_2
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    const v6, 0x7f090190

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 481
    goto :goto_1

    .line 484
    :sswitch_3
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    const v6, 0x7f090bc3

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 485
    goto :goto_1

    .line 488
    :sswitch_4
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    const v6, 0x7f090bc0

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 489
    goto :goto_1

    .line 492
    :sswitch_5
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    const v6, 0x7f090bc1

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 493
    goto/16 :goto_1

    .line 496
    :sswitch_6
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    const v6, 0x7f090bbd

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 497
    goto/16 :goto_1

    .line 469
    nop

    :sswitch_data_0
    .sparse-switch
        0x2719 -> :sswitch_0
        0x2723 -> :sswitch_1
        0x2724 -> :sswitch_2
        0x2726 -> :sswitch_3
        0x2727 -> :sswitch_6
        0x2728 -> :sswitch_2
        0x272e -> :sswitch_4
        0x2730 -> :sswitch_5
    .end sparse-switch
.end method

.method private updatePedometerDetails()V
    .locals 5

    .prologue
    .line 285
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->detailsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    if-lez v3, :cond_0

    .line 286
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->detailsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 288
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getContents()Ljava/util/List;

    move-result-object v0

    .line 289
    .local v0, "holderList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$ItemHolder;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$ItemHolder;

    .line 290
    .local v2, "item":Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$ItemHolder;
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->detailsContainer:Landroid/widget/LinearLayout;

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getView(Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$ItemHolder;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 291
    .end local v2    # "item":Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$ItemHolder;
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->initInputTypeLayout()V

    .line 292
    return-void
.end method


# virtual methods
.method public arabNumberChangeLocale(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "stepStr"    # Ljava/lang/String;

    .prologue
    const v3, 0x7f090b8d

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 438
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 439
    .local v1, "language":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 440
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 445
    .local v0, "concatStep":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 442
    .end local v0    # "concatStep":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "concatStep":Ljava/lang/String;
    goto :goto_0
.end method

.method public distanceConverter(FLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "distance"    # F
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "unit"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    const/high16 v6, 0x42c80000    # 100.0f

    .line 510
    new-instance v2, Ljava/text/DecimalFormat;

    const-string v4, "0.00"

    invoke-direct {v2, v4}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 512
    .local v2, "twoDForm":Ljava/text/DecimalFormat;
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->distanceUnit:Ljava/lang/String;

    const-string/jumbo v5, "mi"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 513
    const v4, 0x3a22e36f

    mul-float/2addr v4, p1

    mul-float/2addr v4, v6

    float-to-int v4, v4

    int-to-float v4, v4

    div-float v0, v4, v6

    .line 514
    .local v0, "convert_distance":F
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget-object v4, v4, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 515
    .local v1, "language":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 516
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "%.2f"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 520
    :goto_0
    const v4, 0x7f0900cc

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 521
    const v4, 0x7f0900cb

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 533
    .local v3, "unitContentDesc":Ljava/lang/String;
    :goto_1
    sget-object v5, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mContentDesc:[Ljava/lang/String;

    const/4 v6, 0x3

    float-to-int v4, p1

    if-eqz v4, :cond_3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_2
    aput-object v4, v5, v6

    .line 534
    float-to-int v4, p1

    if-eqz v4, :cond_4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_3
    return-object v4

    .line 518
    .end local v3    # "unitContentDesc":Ljava/lang/String;
    :cond_0
    float-to-double v4, v0

    invoke-virtual {v2, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 523
    .end local v0    # "convert_distance":F
    .end local v1    # "language":Ljava/lang/String;
    :cond_1
    const/high16 v4, 0x41200000    # 10.0f

    div-float v4, p1, v4

    float-to-int v4, v4

    int-to-float v4, v4

    div-float v0, v4, v6

    .line 524
    .restart local v0    # "convert_distance":F
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget-object v4, v4, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 525
    .restart local v1    # "language":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 526
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "%.2f"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 530
    :goto_4
    const v4, 0x7f0900c7

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 531
    const v4, 0x7f0901c3

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "unitContentDesc":Ljava/lang/String;
    goto :goto_1

    .line 528
    .end local v3    # "unitContentDesc":Ljava/lang/String;
    :cond_2
    float-to-double v4, v0

    invoke-virtual {v2, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object p2

    goto :goto_4

    .line 533
    .restart local v3    # "unitContentDesc":Ljava/lang/String;
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "0 "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 534
    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "0 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_3
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 212
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$3;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;)V

    return-object v0
.end method

.method public getValue(I)[Ljava/lang/String;
    .locals 13
    .param p1, "index"    # I

    .prologue
    .line 347
    const-string v8, ""

    .line 348
    .local v8, "value":Ljava/lang/String;
    const-string v7, ""

    .line 351
    .local v7, "unit":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 432
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v8, v9, v10

    :goto_0
    return-object v9

    .line 353
    :pswitch_0
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mDate:Ljava/util/Date;

    invoke-virtual {v9}, Ljava/util/Date;->getTime()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    const-string v10, "/"

    invoke-static {p0, v9, v10}, Lcom/sec/android/app/shealth/common/utils/DateFormatHelper;->getSystemDateFormat(Landroid/content/Context;Ljava/lang/Long;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 354
    sget-object v9, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mContentDesc:[Ljava/lang/String;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->format:Ljava/text/DateFormat;

    iget-object v12, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mDate:Ljava/util/Date;

    invoke-virtual {v11, v12}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    .line 355
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v8, v9, v10

    goto :goto_0

    .line 358
    :pswitch_1
    const v9, 0x7f0900b9

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 359
    const-string v9, "Cal"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 360
    sget-object v9, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mContentDesc:[Ljava/lang/String;

    const/4 v10, 0x4

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget v12, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mCal:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const v12, 0x7f0900bb

    invoke-virtual {p0, v12}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    .line 367
    :goto_1
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget v12, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mCal:I

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    goto :goto_0

    .line 364
    :cond_0
    sget-object v9, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mContentDesc:[Ljava/lang/String;

    const/4 v10, 0x4

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget v12, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mCal:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const v12, 0x7f0901cb

    invoke-virtual {p0, v12}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    goto :goto_1

    .line 370
    :pswitch_2
    new-instance v9, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    iput-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    .line 371
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getDistanceUnit()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->distanceUnit:Ljava/lang/String;

    .line 372
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->distanceUnit:Ljava/lang/String;

    if-nez v9, :cond_1

    .line 373
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0900c7

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->distanceUnit:Ljava/lang/String;

    .line 375
    :cond_1
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    iget v11, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mDistance:F

    invoke-virtual {p0, v11, v8, v7}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->distanceConverter(FLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    goto/16 :goto_0

    .line 379
    :pswitch_3
    iget v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mTotalStep:I

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    .line 380
    .local v6, "stepStr":Ljava/lang/String;
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->arabNumberChangeLocale(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 382
    .local v4, "concatStep":Ljava/lang/String;
    if-eqz v6, :cond_2

    move-object v5, v4

    .line 383
    .local v5, "ret":Ljava/lang/String;
    :goto_2
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v5, v9, v10

    goto/16 :goto_0

    .end local v5    # "ret":Ljava/lang/String;
    :cond_2
    move-object v5, v8

    .line 382
    goto :goto_2

    .line 386
    .end local v4    # "concatStep":Ljava/lang/String;
    .end local v6    # "stepStr":Ljava/lang/String;
    :pswitch_4
    const v9, 0x7f0900eb

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 387
    iget-wide v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mDuration:J

    const-wide/16 v11, -0x1

    cmp-long v9, v9, v11

    if-nez v9, :cond_3

    .line 388
    const-string v9, "PEDOCHECK"

    const-string v10, "mDuration is -1"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    const-wide/16 v9, 0x0

    iput-wide v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mDuration:J

    .line 391
    :cond_3
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v10, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mDuration:J

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 392
    const-string v9, "1 "

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 393
    const v9, 0x7f0900ea

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 395
    :cond_4
    sget-object v9, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mContentDesc:[Ljava/lang/String;

    const/4 v10, 0x2

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const v12, 0x7f0900ec

    invoke-virtual {p0, v12}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    .line 396
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    goto/16 :goto_0

    .line 400
    :pswitch_5
    const v9, 0x7f0907e5

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 402
    iget-wide v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mHealthySteps:J

    const-wide/16 v11, -0x1

    cmp-long v9, v9, v11

    if-nez v9, :cond_5

    .line 403
    const-string v9, "PEDOCHECK"

    const-string v10, "healthy step is -1"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    const-wide/16 v9, 0x0

    iput-wide v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mHealthySteps:J

    .line 407
    :cond_5
    iget-wide v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mHealthySteps:J

    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 408
    .local v0, "HealthystepStr":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->arabNumberChangeLocale(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 410
    .local v2, "concatHealthyStep":Ljava/lang/String;
    if-eqz v0, :cond_6

    move-object v5, v2

    .line 411
    .restart local v5    # "ret":Ljava/lang/String;
    :goto_3
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v5, v9, v10

    goto/16 :goto_0

    .end local v5    # "ret":Ljava/lang/String;
    :cond_6
    move-object v5, v8

    .line 410
    goto :goto_3

    .line 415
    .end local v0    # "HealthystepStr":Ljava/lang/String;
    .end local v2    # "concatHealthyStep":Ljava/lang/String;
    :pswitch_6
    iget v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mRunningSteps:I

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 416
    .local v1, "RunstepStr":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->arabNumberChangeLocale(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 418
    .local v3, "concatRunStep":Ljava/lang/String;
    if-eqz v1, :cond_7

    move-object v5, v3

    .line 419
    .restart local v5    # "ret":Ljava/lang/String;
    :goto_4
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v5, v9, v10

    goto/16 :goto_0

    .end local v5    # "ret":Ljava/lang/String;
    :cond_7
    move-object v5, v8

    .line 418
    goto :goto_4

    .line 351
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_1
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v10, 0x2719

    const/4 v7, 0x0

    const/4 v9, 0x0

    .line 99
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 100
    .local v2, "intent":Landroid/content/Intent;
    const-string v6, "data_type"

    invoke-virtual {v2, v6, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mDeviceType:I

    .line 102
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 104
    const v6, 0x7f030286

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->setContentView(I)V

    .line 106
    const v6, 0x7f080738

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->detailsContainer:Landroid/widget/LinearLayout;

    .line 109
    const-string v6, "distance"

    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v6

    iput v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mDistance:F

    .line 110
    const-string/jumbo v6, "pick_calories"

    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v6

    float-to-int v6, v6

    iput v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mCal:I

    .line 111
    const-string/jumbo v6, "totalstep"

    invoke-virtual {v2, v6, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mTotalStep:I

    .line 112
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mCustomName:Ljava/lang/String;

    .line 114
    const-string/jumbo v6, "tag"

    invoke-virtual {v2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mTag:Ljava/lang/String;

    .line 115
    sget-object v6, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "tag : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mTag:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    const/4 v1, 0x0

    .line 118
    .local v1, "gearDeviceName":Ljava/lang/String;
    iget v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mDeviceType:I

    const/16 v7, 0x272f

    if-ne v6, v7, :cond_0

    .line 119
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mTag:Ljava/lang/String;

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x2

    aget-object v1, v6, v7

    .line 120
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getCustomeNameFromDeviceName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mCustomName:Ljava/lang/String;

    .line 123
    :cond_0
    const-string/jumbo v6, "runningsteps"

    invoke-virtual {v2, v6, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mRunningSteps:I

    .line 125
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    const v7, 0x7f09005b

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    .line 126
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    invoke-virtual {v6, v9}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 128
    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v0

    .line 129
    .local v0, "activeMonitor":Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;
    const-string/jumbo v6, "starttime"

    const-wide/16 v7, 0x0

    invoke-virtual {v2, v6, v7, v8}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 130
    .local v4, "mStartTime":J
    new-instance v6, Ljava/util/Date;

    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mDate:Ljava/util/Date;

    .line 131
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mDate:Ljava/util/Date;

    invoke-virtual {v6, v4, v5}, Ljava/util/Date;->setTime(J)V

    .line 132
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mDate:Ljava/util/Date;

    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getSContextData(Ljava/util/Date;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$SContextActiveData;

    move-result-object v3

    .line 133
    .local v3, "lData":Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$SContextActiveData;
    iget-wide v6, v3, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$SContextActiveData;->active_time:J

    iput-wide v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mDuration:J

    .line 135
    sget-object v6, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "start time = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    sget-object v6, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "duration 2= "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, v3, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$SContextActiveData;->active_time:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    sget-object v6, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "healthysteps 2= "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, v3, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$SContextActiveData;->healthy_steps:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    iget v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mDeviceType:I

    if-ne v6, v10, :cond_1

    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->isSupportDurationAndHealthPace()Z

    move-result v6

    if-nez v6, :cond_2

    :cond_1
    if-eqz v1, :cond_3

    const v6, 0x7f090bc0

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 142
    :cond_2
    const/4 v6, 0x7

    new-array v6, v6, [I

    fill-array-data v6, :array_0

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->pedometorItems:[I

    .line 144
    iget-wide v6, v3, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$SContextActiveData;->healthy_steps:J

    iput-wide v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mHealthySteps:J

    .line 146
    iget-wide v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mDuration:J

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    const-wide/16 v8, 0x3c

    div-long/2addr v6, v8

    long-to-int v6, v6

    int-to-long v6, v6

    iput-wide v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mDuration:J

    .line 148
    sget-object v6, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "duration 1= "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mDuration:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    sget-object v6, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "healthysteps 1= "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mHealthySteps:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    :goto_0
    return-void

    .line 152
    :cond_3
    const/4 v6, 0x5

    new-array v6, v6, [I

    fill-array-data v6, :array_1

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->pedometorItems:[I

    goto :goto_0

    .line 142
    nop

    :array_0
    .array-data 4
        0x0
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
    .end array-data

    .line 152
    :array_1
    .array-data 4
        0x0
        0x1
        0x3
        0x4
        0x6
    .end array-data
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v6, 0x1

    .line 162
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    .line 163
    .local v2, "inflater":Landroid/view/MenuInflater;
    const v3, 0x7f10002e

    invoke-virtual {v2, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 166
    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mDeviceType:I

    const/16 v4, 0x2719

    if-eq v3, v4, :cond_0

    .line 167
    const v3, 0x7f080c8d

    invoke-interface {p1, v3}, Landroid/view/Menu;->removeItem(I)V

    .line 168
    const v3, 0x7f080c98

    invoke-interface {p1, v3}, Landroid/view/Menu;->removeItem(I)V

    .line 169
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getTotalActionBarButtonCount()I

    move-result v3

    if-ge v3, v6, :cond_0

    .line 170
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v3, 0x7f0208e3

    new-instance v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$2;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;)V

    invoke-direct {v1, v3, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    .line 180
    .local v1, "actionBarShareviaButtonBuilder":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    const v3, 0x7f090033

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 181
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v0

    .line 182
    .local v0, "actionBarShareviaButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    new-array v4, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 185
    .end local v0    # "actionBarShareviaButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    .end local v1    # "actionBarShareviaButtonBuilder":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    :cond_0
    return v6
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const v2, 0x7f090035

    .line 191
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 207
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 194
    :sswitch_0
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09011c

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090048

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "ok"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 203
    :sswitch_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.walkingmate"

    const-string v2, "W021"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 191
    :sswitch_data_0
    .sparse-switch
        0x7f080c8d -> :sswitch_0
        0x7f080c98 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 280
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 281
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->updatePedometerDetails()V

    .line 282
    return-void
.end method
