.class public Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;
.super Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;
.source "ExerciseProDetailsChartFragment.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ValidFragment"
    }
.end annotation


# static fields
.field private static MARKING_COUNT:I

.field private static MARKING_INTERVAL:I

.field private static final TAG:Ljava/lang/String;

.field private static TIME_DEPTH_LEVEL:I


# instance fields
.field public final CHART_ID_ELEVATION:I

.field public final CHART_ID_EXT_HR:I

.field public final CHART_ID_SPEED:I

.field private bpmUnit:Ljava/lang/String;

.field density:F

.field private elevationUnit:Ljava/lang/String;

.field public final elvChart:Z

.field exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

.field private handlerListener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

.field public final hrmChart:Z

.field public isValidDeviceHrChart:Z

.field public isValidElevChart:Z

.field public isValidExtHrChart:Z

.field kmUnit:Z

.field private mContext:Landroid/app/Activity;

.field private mGraphBackground:Landroid/graphics/Bitmap;

.field private mGraphHandler:Landroid/graphics/Bitmap;

.field private mGraphLine:Landroid/graphics/Bitmap;

.field private mHandleOverBitmapBlack:Landroid/graphics/Bitmap;

.field private mHandleOverBitmapRed:Landroid/graphics/Bitmap;

.field private mHandleOverBitmapYellowGreen:Landroid/graphics/Bitmap;

.field private mLegendView:Landroid/view/View;

.field private mNormalBitmapBlack:Landroid/graphics/Bitmap;

.field private mNormalBitmapRed:Landroid/graphics/Bitmap;

.field private mNormalBitmapYellowGreen:Landroid/graphics/Bitmap;

.field private mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

.field private mRootView:Landroid/view/View;

.field private mRowID:J

.field private mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

.field private mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

.field private mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

.field private oneDForm:Ljava/text/DecimalFormat;

.field private oneDForm2:Ljava/text/DecimalFormat;

.field private oneDForm3:Ljava/text/DecimalFormat;

.field private paceUnit:Ljava/lang/String;

.field scaledDensity:F

.field public final spdChart:Z

.field private speedUnit:Ljava/lang/String;

.field private timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 72
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->TAG:Ljava/lang/String;

    .line 83
    sput v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->TIME_DEPTH_LEVEL:I

    .line 84
    sput v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->MARKING_INTERVAL:I

    .line 86
    const/4 v0, 0x6

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->MARKING_COUNT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 139
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;-><init>()V

    .line 74
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 75
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 77
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 90
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mRowID:J

    .line 92
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->kmUnit:Z

    .line 93
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->spdChart:Z

    .line 94
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->hrmChart:Z

    .line 95
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->elvChart:Z

    .line 98
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->isValidExtHrChart:Z

    .line 99
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->isValidDeviceHrChart:Z

    .line 100
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->isValidElevChart:Z

    .line 103
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->CHART_ID_SPEED:I

    .line 104
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->CHART_ID_EXT_HR:I

    .line 106
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->CHART_ID_ELEVATION:I

    .line 121
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#.#"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->oneDForm:Ljava/text/DecimalFormat;

    .line 122
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#.#"

    new-instance v2, Ljava/text/DecimalFormatSymbols;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-direct {v0, v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->oneDForm2:Ljava/text/DecimalFormat;

    .line 124
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->oneDForm3:Ljava/text/DecimalFormat;

    .line 810
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->handlerListener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    .line 140
    return-void
.end method

.method public constructor <init>(ZJ)V
    .locals 4
    .param p1, "isPaceMode"    # Z
    .param p2, "rowID"    # J

    .prologue
    const/4 v2, 0x0

    .line 152
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;-><init>(Z)V

    .line 74
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 75
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 77
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 90
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mRowID:J

    .line 92
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->kmUnit:Z

    .line 93
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->spdChart:Z

    .line 94
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->hrmChart:Z

    .line 95
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->elvChart:Z

    .line 98
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->isValidExtHrChart:Z

    .line 99
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->isValidDeviceHrChart:Z

    .line 100
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->isValidElevChart:Z

    .line 103
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->CHART_ID_SPEED:I

    .line 104
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->CHART_ID_EXT_HR:I

    .line 106
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->CHART_ID_ELEVATION:I

    .line 121
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#.#"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->oneDForm:Ljava/text/DecimalFormat;

    .line 122
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#.#"

    new-instance v2, Ljava/text/DecimalFormatSymbols;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-direct {v0, v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->oneDForm2:Ljava/text/DecimalFormat;

    .line 124
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->oneDForm3:Ljava/text/DecimalFormat;

    .line 810
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->handlerListener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    .line 153
    iput-wide p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mRowID:J

    .line 154
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mLegendView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Ljava/text/DecimalFormat;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->oneDForm3:Ljava/text/DecimalFormat;

    return-object v0
.end method

.method static synthetic access$1100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getPaceBubbleAreaUnit()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mContext:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Ljava/text/DecimalFormat;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->oneDForm2:Ljava/text/DecimalFormat;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;D)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;
    .param p1, "x1"    # D

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getPaceValue(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Ljava/text/DecimalFormat;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->oneDForm:Ljava/text/DecimalFormat;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getSpeedBubbleAreaUnit()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getBPMBubbleAreaUnit()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getBPMBubbleAreaUnit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1371
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->bpmUnit:Ljava/lang/String;

    .line 1372
    .local v0, "bpmUnit":Ljava/lang/String;
    return-object v0
.end method

.method private getDeviceGroupType(Ljava/lang/String;)J
    .locals 12
    .param p1, "userDeviceId"    # Ljava/lang/String;

    .prologue
    const-wide/16 v10, 0x0

    .line 276
    if-nez p1, :cond_1

    move-wide v0, v10

    .line 298
    :cond_0
    :goto_0
    return-wide v0

    .line 278
    :cond_1
    const-string v0, "_"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 279
    .local v9, "tempDeviceIDArray":[Ljava/lang/String;
    move-object v8, p1

    .line 280
    .local v8, "tempDeviceID":Ljava/lang/String;
    const/4 v0, 0x0

    aget-object v0, v9, v0

    const-string v1, ""

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 281
    .local v6, "deviceID":Ljava/lang/String;
    const-string v0, "_"

    invoke-virtual {v6, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 283
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "select device_group_type, device_id from user_device where device_id == \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 285
    .local v3, "tempSelection":Ljava/lang/String;
    const/4 v7, 0x0

    .line 287
    .local v7, "tempCursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 288
    if-eqz v7, :cond_3

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 290
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 291
    const-string v0, "device_group_type"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 295
    if-eqz v7, :cond_0

    .line 296
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 295
    :cond_3
    if-eqz v7, :cond_4

    .line 296
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_4
    move-wide v0, v10

    .line 298
    goto :goto_0

    .line 295
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_5

    .line 296
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
.end method

.method private getEndTime()J
    .locals 10

    .prologue
    .line 759
    const-wide/high16 v0, -0x8000000000000000L

    .line 760
    .local v0, "endTime":J
    const-wide/high16 v2, -0x8000000000000000L

    .line 761
    .local v2, "endTime2":J
    const-wide/high16 v4, -0x8000000000000000L

    .line 763
    .local v4, "endTime3":J
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    .line 764
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v0

    .line 767
    :cond_0
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_1

    .line 768
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v2

    .line 771
    :cond_1
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_2

    .line 772
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v4

    .line 775
    :cond_2
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    .line 777
    .local v6, "tmp":J
    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    return-wide v8
.end method

.method private getEndTimeForChartRange(J)J
    .locals 9
    .param p1, "inputEndTime"    # J

    .prologue
    .line 787
    const v3, 0x36ee80

    .line 788
    .local v3, "hour":I
    const v4, 0x927c0

    .line 790
    .local v4, "minute_10":I
    const-wide/16 v5, 0x0

    .line 792
    .local v5, "retTime":J
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V

    .line 794
    .local v1, "eDate":Ljava/util/Date;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 795
    .local v0, "c":Ljava/util/Calendar;
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 797
    const/16 v7, 0xc

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 799
    .local v2, "eMinute":I
    const v7, 0x36ee80

    sub-int/2addr v7, v2

    const v8, 0x927c0

    if-ge v7, v8, :cond_0

    .line 800
    const-wide/32 v7, 0x927c0

    add-long v5, p1, v7

    .line 806
    :goto_0
    return-wide v5

    .line 803
    :cond_0
    move-wide v5, p1

    goto :goto_0
.end method

.method private getMilesFromMeters(J)Ljava/lang/String;
    .locals 6
    .param p1, "distance"    # J

    .prologue
    .line 1422
    long-to-double v2, p1

    const-wide v4, 0x3f445c6dea1359a4L    # 6.2137E-4

    mul-double v0, v2, v4

    .line 1423
    .local v0, "mi":D
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->oneDForm2:Ljava/text/DecimalFormat;

    invoke-virtual {v2, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private getPaceBubbleAreaUnit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1376
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->paceUnit:Ljava/lang/String;

    .line 1377
    .local v0, "paceUnit":Ljava/lang/String;
    return-object v0
.end method

.method private getPaceValue(D)Ljava/lang/String;
    .locals 14
    .param p1, "time"    # D

    .prologue
    .line 1408
    const-string v5, "0\'00\""

    .line 1409
    .local v5, "value":Ljava/lang/String;
    const-wide/16 v7, 0x0

    cmpg-double v7, p1, v7

    if-gez v7, :cond_0

    move-object v6, v5

    .line 1418
    .end local v5    # "value":Ljava/lang/String;
    .local v6, "value":Ljava/lang/String;
    :goto_0
    return-object v6

    .line 1412
    .end local v6    # "value":Ljava/lang/String;
    .restart local v5    # "value":Ljava/lang/String;
    :cond_0
    const-wide/16 v0, 0x3c

    .line 1413
    .local v0, "HOUR":J
    long-to-double v7, v0

    div-double v7, p1, v7

    double-to-int v2, v7

    .line 1414
    .local v2, "hour":I
    long-to-double v7, v0

    rem-double/2addr p1, v7

    .line 1415
    const-string v7, "%d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    double-to-int v10, p1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1416
    .local v3, "minuteText":Ljava/lang/String;
    const-string v7, "%02d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    double-to-int v10, p1

    int-to-double v10, v10

    sub-double v10, p1, v10

    const-wide/high16 v12, 0x404e000000000000L    # 60.0

    mul-double/2addr v10, v12

    double-to-int v10, v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1417
    .local v4, "secondText":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    if-lez v2, :cond_1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, ":"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    :goto_1
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v6, v5

    .line 1418
    .end local v5    # "value":Ljava/lang/String;
    .restart local v6    # "value":Ljava/lang/String;
    goto :goto_0

    .line 1417
    .end local v6    # "value":Ljava/lang/String;
    .restart local v5    # "value":Ljava/lang/String;
    :cond_1
    const-string v7, ""

    goto :goto_1
.end method

.method private getSpeedBubbleAreaUnit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1366
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->speedUnit:Ljava/lang/String;

    .line 1367
    .local v0, "speedUnit":Ljava/lang/String;
    return-object v0
.end method

.method private getStartTime()J
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 736
    const-wide v0, 0x7fffffffffffffffL

    .line 737
    .local v0, "startTime":J
    const-wide v2, 0x7fffffffffffffffL

    .line 738
    .local v2, "startTime2":J
    const-wide v4, 0x7fffffffffffffffL

    .line 741
    .local v4, "startTime3":J
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    .line 742
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v0

    .line 745
    :cond_0
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_1

    .line 746
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v2

    .line 749
    :cond_1
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_2

    .line 750
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v4

    .line 753
    :cond_2
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    .line 755
    .local v6, "tmp":J
    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    return-wide v8
.end method

.method private getStartTimeForChartRange(J)J
    .locals 3
    .param p1, "inputStartTime"    # J

    .prologue
    .line 781
    const v0, 0x927c0

    .line 783
    .local v0, "minute_10":I
    const-wide/32 v1, 0x927c0

    sub-long v1, p1, v1

    return-wide v1
.end method

.method private initChartStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 26
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .param p2, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 1085
    new-instance v17, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct/range {v17 .. v17}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 1086
    .local v17, "xTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v21, 0x41700000    # 15.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->scaledDensity:F

    move/from16 v22, v0

    mul-float v21, v21, v22

    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 1087
    const/16 v21, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 1088
    const/16 v21, 0xff

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    move/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 1089
    const/16 v21, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 1091
    new-instance v18, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct/range {v18 .. v18}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 1092
    .local v18, "yLeftTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v21, 0x41700000    # 15.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->scaledDensity:F

    move/from16 v22, v0

    mul-float v21, v21, v22

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 1093
    const-string/jumbo v21, "sans-serif"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 1094
    const/16 v21, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 1095
    const/16 v21, 0xff

    const/16 v22, 0x8d

    const/16 v23, 0xd9

    const/16 v24, 0x31

    move-object/from16 v0, v18

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    move/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 1096
    const/16 v21, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 1098
    new-instance v19, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct/range {v19 .. v19}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 1099
    .local v19, "yRightTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v21, 0x41700000    # 15.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->scaledDensity:F

    move/from16 v22, v0

    mul-float v21, v21, v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 1100
    const-string/jumbo v21, "sans-serif"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 1101
    const/16 v21, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 1102
    const/16 v21, 0xff

    const/16 v22, 0xe5

    const/16 v23, 0x47

    const/16 v24, 0x47

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    move/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 1103
    const/16 v21, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 1105
    new-instance v20, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct/range {v20 .. v20}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 1106
    .local v20, "yRightTextStyle1":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v21, 0x41700000    # 15.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->scaledDensity:F

    move/from16 v22, v0

    mul-float v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 1107
    const-string/jumbo v21, "sans-serif"

    invoke-virtual/range {v20 .. v21}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 1108
    const/16 v21, 0x1

    invoke-virtual/range {v20 .. v21}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 1109
    const/16 v21, 0xff

    const/16 v22, 0xa4

    const/16 v23, 0xa4

    const/16 v24, 0xa4

    invoke-virtual/range {v20 .. v24}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 1110
    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 1112
    new-instance v10, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v10}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 1113
    .local v10, "handlerItemTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v21, 0x41900000    # 18.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->scaledDensity:F

    move/from16 v22, v0

    mul-float v21, v21, v22

    move/from16 v0, v21

    invoke-virtual {v10, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 1114
    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v10, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 1115
    const/16 v21, 0xff

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v10, v0, v1, v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 1116
    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v10, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 1118
    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/high16 v24, 0x41800000    # 16.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->density:F

    move/from16 v25, v0

    mul-float v24, v24, v25

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    move/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPadding(FFFF)V

    .line 1123
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f0203b2

    invoke-static/range {v21 .. v22}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mGraphBackground:Landroid/graphics/Bitmap;

    .line 1125
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mGraphBackground:Landroid/graphics/Bitmap;

    move-object/from16 v21, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setGraphBackgroundImage(Landroid/graphics/Bitmap;)V

    .line 1126
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f0700d8

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getColor(I)I

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setChartBackgroundColor(I)V

    .line 1128
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setSeparatorDateFormat(Ljava/lang/String;)V

    .line 1131
    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisMarkingVisible(ZI)V

    .line 1132
    const v21, -0xcbb1ec

    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisColor(II)V

    .line 1133
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 1134
    const/high16 v21, 0x41f00000    # 30.0f

    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTextSpace(FI)V

    .line 1136
    const/16 v21, 0x0

    const/16 v22, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTextVisible(ZI)V

    .line 1137
    const/16 v21, 0x0

    const/16 v22, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisMarkingVisible(ZI)V

    .line 1139
    const-string v21, "HH:mm"

    const/16 v22, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisDateFormat(Ljava/lang/String;I)V

    .line 1142
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setSeparatorVisible(Z)V

    .line 1145
    const/16 v21, 0x0

    const/16 v22, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisVisible(ZI)V

    .line 1146
    const/16 v21, 0x0

    const/16 v22, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisVisible(ZI)V

    .line 1150
    const/16 v21, 0x1

    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisVisible(ZI)V

    .line 1151
    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisMarkingVisible(ZI)V

    .line 1152
    const/16 v21, 0x1

    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextVisible(ZI)V

    .line 1153
    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLineVisible(ZI)V

    .line 1154
    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisDirection(II)V

    .line 1155
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 1156
    const/16 v21, 0x1

    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelAlign(II)V

    .line 1157
    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v5, v0, [I

    const/16 v21, 0x0

    const/16 v22, 0x0

    aput v22, v5, v21

    .line 1160
    .local v5, "associatedSeriesID":[I
    const/16 v21, 0x1

    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v5, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisAssociatedSeriesID(I[II)V

    .line 1162
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getPaceMode()Z

    move-result v21

    if-eqz v21, :cond_6

    .line 1163
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->kmUnit:Z

    move/from16 v21, v0

    if-eqz v21, :cond_5

    .line 1164
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "("

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const v22, 0x7f09080e

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ")"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitle(Ljava/lang/String;I)V

    .line 1173
    :goto_0
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitleTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 1178
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getSize()I

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_1

    .line 1179
    const/4 v11, 0x0

    .local v11, "index":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getSize()I

    move-result v21

    move/from16 v0, v21

    if-ge v11, v0, :cond_0

    .line 1180
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v11}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getY(I)D

    move-result-wide v21

    const-wide/16 v23, 0x0

    cmpl-double v21, v21, v23

    if-lez v21, :cond_8

    .line 1181
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->isValidExtHrChart:Z

    .line 1185
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->isValidExtHrChart:Z

    move/from16 v21, v0

    if-eqz v21, :cond_1

    .line 1186
    const/16 v21, 0x1

    const/16 v22, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisVisible(ZI)V

    .line 1187
    const/16 v21, 0x0

    const/16 v22, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisMarkingVisible(ZI)V

    .line 1188
    const/16 v21, 0x1

    const/16 v22, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextVisible(ZI)V

    .line 1189
    const/16 v21, 0x0

    const/16 v22, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLineVisible(ZI)V

    .line 1190
    const/16 v21, 0x1

    const/16 v22, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisDirection(II)V

    .line 1191
    const/16 v21, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 1192
    const/16 v21, 0x0

    const/16 v22, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelAlign(II)V

    .line 1193
    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v6, v0, [I

    const/16 v21, 0x0

    const/16 v22, 0x1

    aput v22, v6, v21

    .line 1196
    .local v6, "associatedSeriesID1":[I
    const/16 v21, 0x1

    const/16 v22, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v6, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisAssociatedSeriesID(I[II)V

    .line 1197
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v12, v0, [F

    fill-array-data v12, :array_0

    .line 1201
    .local v12, "labelOffset1":[F
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v13, v0, [F

    fill-array-data v13, :array_1

    .line 1205
    .local v13, "labelOffset2":[F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getSize()I

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_9

    .line 1206
    const/16 v21, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v12, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelOffsets([FI)V

    .line 1210
    :goto_2
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "("

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const v22, 0x7f0900d2

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ")"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitle(Ljava/lang/String;I)V

    .line 1211
    const/16 v21, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitleTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 1213
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getSize()I

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_1

    .line 1214
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v14

    .line 1215
    .local v14, "metrics":Landroid/util/DisplayMetrics;
    const/16 v21, 0x1

    const/high16 v22, 0x41700000    # 15.0f

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-static {v0, v1, v14}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v15

    .line 1216
    .local v15, "px":F
    const/16 v21, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v15, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitleOffset(FI)V

    .line 1256
    .end local v6    # "associatedSeriesID1":[I
    .end local v11    # "index":I
    .end local v12    # "labelOffset1":[F
    .end local v13    # "labelOffset2":[F
    .end local v14    # "metrics":Landroid/util/DisplayMetrics;
    .end local v15    # "px":F
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getSize()I

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_4

    .line 1257
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->isValidElevChart:Z

    .line 1258
    const/16 v21, 0x1

    const/16 v22, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisVisible(ZI)V

    .line 1259
    const/16 v21, 0x0

    const/16 v22, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisMarkingVisible(ZI)V

    .line 1260
    const/16 v21, 0x1

    const/16 v22, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextVisible(ZI)V

    .line 1261
    const/16 v21, 0x0

    const/16 v22, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLineVisible(ZI)V

    .line 1262
    const/16 v21, 0x1

    const/16 v22, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisDirection(II)V

    .line 1263
    const/16 v21, 0x2

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 1264
    const/16 v21, 0x0

    const/16 v22, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelAlign(II)V

    .line 1265
    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v7, v0, [I

    const/16 v21, 0x0

    const/16 v22, 0x2

    aput v22, v7, v21

    .line 1268
    .local v7, "associatedSeriesID2":[I
    const/16 v21, 0x1

    const/16 v22, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v7, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisAssociatedSeriesID(I[II)V

    .line 1270
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v12, v0, [F

    fill-array-data v12, :array_2

    .line 1273
    .restart local v12    # "labelOffset1":[F
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v13, v0, [F

    fill-array-data v13, :array_3

    .line 1276
    .restart local v13    # "labelOffset2":[F
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->isValidExtHrChart:Z

    move/from16 v21, v0

    if-nez v21, :cond_2

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->isValidDeviceHrChart:Z

    move/from16 v21, v0

    if-eqz v21, :cond_a

    .line 1277
    :cond_2
    const/16 v21, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v13, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelOffsets([FI)V

    .line 1280
    :goto_3
    sget-object v21, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoDistanceUnit:Ljava/lang/String;

    const-string v22, "km"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_b

    const/16 v16, 0x1

    .line 1281
    .local v16, "unitMile":Z
    :goto_4
    if-eqz v16, :cond_c

    .line 1282
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "("

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v22

    const v23, 0x7f090a1e

    invoke-virtual/range {v22 .. v23}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ")"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x2

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitle(Ljava/lang/String;I)V

    .line 1285
    :goto_5
    const/16 v21, 0x2

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitleTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 1286
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->isValidExtHrChart:Z

    move/from16 v21, v0

    if-nez v21, :cond_3

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->isValidDeviceHrChart:Z

    move/from16 v21, v0

    if-eqz v21, :cond_4

    .line 1287
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v14

    .line 1288
    .restart local v14    # "metrics":Landroid/util/DisplayMetrics;
    const/16 v21, 0x1

    const/high16 v22, 0x41980000    # 19.0f

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-static {v0, v1, v14}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v15

    .line 1289
    .restart local v15    # "px":F
    const/16 v21, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v15, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitleOffset(FI)V

    .line 1298
    .end local v7    # "associatedSeriesID2":[I
    .end local v12    # "labelOffset1":[F
    .end local v13    # "labelOffset2":[F
    .end local v14    # "metrics":Landroid/util/DisplayMetrics;
    .end local v15    # "px":F
    .end local v16    # "unitMile":Z
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f0203c3

    invoke-static/range {v21 .. v22}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mGraphHandler:Landroid/graphics/Bitmap;

    .line 1301
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mGraphHandler:Landroid/graphics/Bitmap;

    move-object/from16 v22, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    move-object/from16 v2, v22

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/logutils/graph/GraphFragmentUtils;->swellHandler(Landroid/content/Context;Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Landroid/graphics/Bitmap;)V

    .line 1302
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemStrokeWidth(F)V

    .line 1303
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 1304
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->handlerListener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    move-object/from16 v21, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerListener(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;)V

    .line 1305
    const/high16 v21, 0x426c0000    # 59.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->density:F

    move/from16 v22, v0

    mul-float v21, v21, v22

    const/high16 v22, 0x40000000    # 2.0f

    mul-float v21, v21, v22

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 1307
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f0a0057

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    const/high16 v22, 0x3f000000    # 0.5f

    mul-float v21, v21, v22

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0a0058

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    add-float v21, v21, v22

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemOffset(F)V

    .line 1309
    const/high16 v21, 0x41a00000    # 20.0f

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextOffset(F)V

    .line 1310
    const-wide/16 v21, 0x7d0

    move-object/from16 v0, p1

    move-wide/from16 v1, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerTimeOutDelay(J)V

    .line 1312
    const-string v21, "HH:mm"

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemDateFormat(Ljava/lang/String;)V

    .line 1315
    const/16 v21, 0x12c

    const/16 v22, 0x12c

    sget-object v23, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static/range {v21 .. v23}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mGraphLine:Landroid/graphics/Bitmap;

    .line 1316
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f0203c4

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    .line 1317
    .local v9, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v8, Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mGraphLine:Landroid/graphics/Bitmap;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-direct {v8, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1318
    .local v8, "canvas":Landroid/graphics/Canvas;
    const/16 v21, 0x0

    const/16 v22, 0x0

    invoke-virtual {v8}, Landroid/graphics/Canvas;->getWidth()I

    move-result v23

    invoke-virtual {v8}, Landroid/graphics/Canvas;->getHeight()I

    move-result v24

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v9, v0, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1319
    invoke-virtual {v9, v8}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1320
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mGraphLine:Landroid/graphics/Bitmap;

    move-object/from16 v21, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerLineImage(Landroid/graphics/Bitmap;)V

    .line 1322
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPopupEnable(Z)V

    .line 1323
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setMarkingLineBase(I)V

    .line 1324
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setLegendVisible(Z)V

    .line 1325
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->density:F

    move/from16 v21, v0

    const/high16 v22, 0x42080000    # 34.0f

    mul-float v21, v21, v22

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setSeparatorTextSpacingTop(I)V

    .line 1326
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerTooltipEnable(Z)V

    .line 1327
    return-void

    .line 1166
    .end local v8    # "canvas":Landroid/graphics/Canvas;
    .end local v9    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_5
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "("

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const v22, 0x7f090811

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ")"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitle(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 1168
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->kmUnit:Z

    move/from16 v21, v0

    if-eqz v21, :cond_7

    .line 1169
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "("

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const v22, 0x7f09080f

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ")"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitle(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 1171
    :cond_7
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "("

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const v22, 0x7f090810

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ")"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitle(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 1179
    .restart local v11    # "index":I
    :cond_8
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    .line 1208
    .restart local v6    # "associatedSeriesID1":[I
    .restart local v12    # "labelOffset1":[F
    .restart local v13    # "labelOffset2":[F
    :cond_9
    const/16 v21, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v13, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelOffsets([FI)V

    goto/16 :goto_2

    .line 1279
    .end local v6    # "associatedSeriesID1":[I
    .end local v11    # "index":I
    .restart local v7    # "associatedSeriesID2":[I
    :cond_a
    const/16 v21, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v12, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelOffsets([FI)V

    goto/16 :goto_3

    .line 1280
    :cond_b
    const/16 v16, 0x0

    goto/16 :goto_4

    .line 1284
    .restart local v16    # "unitMile":Z
    :cond_c
    const-string v21, "(m)"

    const/16 v22, 0x2

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitle(Ljava/lang/String;I)V

    goto/16 :goto_5

    .line 1197
    nop

    :array_0
    .array-data 4
        -0x3db80000    # -50.0f
        0x0
        0x0
    .end array-data

    .line 1201
    :array_1
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data

    .line 1270
    :array_2
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data

    .line 1273
    :array_3
    .array-data 4
        0x0
        0x42480000    # 50.0f
        0x42480000    # 50.0f
    .end array-data
.end method

.method private initSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V
    .locals 14
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    .prologue
    .line 966
    new-instance v5, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;

    invoke-direct {v5}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;-><init>()V

    .line 967
    .local v5, "seriesStyle":Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;
    new-instance v6, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;

    invoke-direct {v6}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;-><init>()V

    .line 969
    .local v6, "seriesStyle1":Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;
    new-instance v7, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;

    invoke-direct {v7}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;-><init>()V

    .line 971
    .local v7, "seriesStyle2":Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;
    const-string/jumbo v10, "speed"

    invoke-virtual {v5, v10}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setSeriesName(Ljava/lang/String;)V

    .line 972
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getPaceMode()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 973
    iget-boolean v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->kmUnit:Z

    if-eqz v10, :cond_1

    .line 974
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const v11, 0x7f09080e

    invoke-virtual {p0, v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ")"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitle(Ljava/lang/String;I)V

    .line 983
    :goto_0
    const-string v10, "heart rate"

    invoke-virtual {v6, v10}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setSeriesName(Ljava/lang/String;)V

    .line 987
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    const/4 v11, 0x0

    iget-wide v12, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mRowID:J

    invoke-static {v10, v11, v12, v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getWorkoutStatics(Landroid/content/Context;ZJ)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;

    move-result-object v8

    .line 988
    .local v8, "stastics":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;
    iget-wide v0, v8, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->maxAltitude:D

    .line 989
    .local v0, "mMaxElevation":D
    iget-wide v2, v8, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->minAltitude:D

    .line 991
    .local v2, "mMinElevation":D
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->loadUnitsFromSharedPreferences()V

    .line 992
    sget-object v10, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoDistanceUnit:Ljava/lang/String;

    const-string v11, "km"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_4

    const/4 v9, 0x1

    .line 993
    .local v9, "unitMile":Z
    :goto_1
    if-eqz v9, :cond_5

    .line 995
    const-wide v10, 0x3ff199999999999aL    # 1.1

    mul-double/2addr v10, v0

    const-wide/high16 v12, 0x4059000000000000L    # 100.0

    div-double/2addr v10, v12

    const-wide/high16 v12, 0x4059000000000000L    # 100.0

    mul-double/2addr v10, v12

    const-wide v12, 0x400a3f290abb44e5L    # 3.28084

    mul-double v0, v10, v12

    .line 996
    const-wide v10, 0x400a3f290abb44e5L    # 3.28084

    mul-double/2addr v2, v10

    .line 1009
    :goto_2
    const/4 v10, 0x1

    const/4 v11, 0x1

    double-to-float v12, v2

    double-to-float v13, v0

    invoke-virtual {v7, v10, v11, v12, v13}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->setFixedMinMaxY(ZZFF)V

    .line 1011
    const-string v10, "elevation"

    invoke-virtual {v7, v10}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->setSeriesName(Ljava/lang/String;)V

    .line 1015
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    invoke-virtual {v10}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0203c0

    invoke-static {v10, v11}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mNormalBitmapYellowGreen:Landroid/graphics/Bitmap;

    .line 1019
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    invoke-virtual {v10}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0203c1

    invoke-static {v10, v11}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mHandleOverBitmapYellowGreen:Landroid/graphics/Bitmap;

    .line 1021
    const/4 v10, 0x1

    invoke-virtual {v5, v10}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setValueMarkingVisible(Z)V

    .line 1022
    const/4 v10, 0x2

    invoke-virtual {v5, v10}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setValueMarkingVisibleOption(I)V

    .line 1024
    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mNormalBitmapYellowGreen:Landroid/graphics/Bitmap;

    invoke-virtual {v5, v10}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setValueMarkingNormalImage(Landroid/graphics/Bitmap;)V

    .line 1026
    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mHandleOverBitmapYellowGreen:Landroid/graphics/Bitmap;

    invoke-virtual {v5, v10}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setValueMarkingHandlerOverOutRangeImage(Landroid/graphics/Bitmap;)V

    .line 1027
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v10

    iget v4, v10, Landroid/util/DisplayMetrics;->density:F

    .line 1028
    .local v4, "scale":F
    const/high16 v10, 0x40a00000    # 5.0f

    mul-float/2addr v10, v4

    invoke-virtual {v5, v10}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setValueMarkingSize(F)V

    .line 1032
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    invoke-virtual {v10}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0203be

    invoke-static {v10, v11}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mNormalBitmapRed:Landroid/graphics/Bitmap;

    .line 1036
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    invoke-virtual {v10}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0203bf

    invoke-static {v10, v11}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mHandleOverBitmapRed:Landroid/graphics/Bitmap;

    .line 1038
    const/4 v10, 0x1

    invoke-virtual {v6, v10}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setValueMarkingVisible(Z)V

    .line 1039
    const/4 v10, 0x2

    invoke-virtual {v6, v10}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setValueMarkingVisibleOption(I)V

    .line 1040
    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mNormalBitmapRed:Landroid/graphics/Bitmap;

    invoke-virtual {v6, v10}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setValueMarkingNormalImage(Landroid/graphics/Bitmap;)V

    .line 1041
    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mHandleOverBitmapRed:Landroid/graphics/Bitmap;

    invoke-virtual {v6, v10}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setValueMarkingHandlerOverOutRangeImage(Landroid/graphics/Bitmap;)V

    .line 1042
    const/high16 v10, 0x40a00000    # 5.0f

    mul-float/2addr v10, v4

    invoke-virtual {v6, v10}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setValueMarkingSize(F)V

    .line 1052
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    invoke-virtual {v10}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0203b5

    invoke-static {v10, v11}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mNormalBitmapBlack:Landroid/graphics/Bitmap;

    .line 1056
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    invoke-virtual {v10}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0203b6

    invoke-static {v10, v11}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mHandleOverBitmapBlack:Landroid/graphics/Bitmap;

    .line 1058
    const/4 v10, 0x1

    invoke-virtual {v7, v10}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->setValueMarkingVisible(Z)V

    .line 1059
    const/4 v10, 0x2

    invoke-virtual {v7, v10}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->setValueMarkingVisibleOption(I)V

    .line 1060
    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mNormalBitmapBlack:Landroid/graphics/Bitmap;

    invoke-virtual {v7, v10}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->setValueMarkingNormalImage(Landroid/graphics/Bitmap;)V

    .line 1061
    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mHandleOverBitmapBlack:Landroid/graphics/Bitmap;

    invoke-virtual {v7, v10}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->setValueMarkingHandlerOverOutRangeImage(Landroid/graphics/Bitmap;)V

    .line 1062
    const/high16 v10, 0x40a00000    # 5.0f

    mul-float/2addr v10, v4

    invoke-virtual {v7, v10}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->setValueMarkingSize(F)V

    .line 1064
    const v10, -0x7226b7

    invoke-virtual {v5, v10}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setLineColor(I)V

    .line 1065
    const/high16 v10, 0x41200000    # 10.0f

    invoke-virtual {v5, v10}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setLineThickness(F)V

    .line 1067
    const v10, -0x1a8e8f

    invoke-virtual {v6, v10}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setLineColor(I)V

    .line 1068
    const/high16 v10, 0x41200000    # 10.0f

    invoke-virtual {v6, v10}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setLineThickness(F)V

    .line 1073
    const v10, 0x77a4a4a4

    invoke-virtual {v7, v10}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->setAreaColor(I)V

    .line 1074
    const v10, 0x77a4a4a4

    invoke-virtual {v7, v10}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->setLineColor(I)V

    .line 1076
    invoke-virtual {p1, v5}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->addSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;)V

    .line 1077
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isSupportHRM()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1078
    invoke-virtual {p1, v6}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->addSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;)V

    .line 1081
    :cond_0
    invoke-virtual {p1, v7}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->addSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;)V

    .line 1082
    return-void

    .line 976
    .end local v0    # "mMaxElevation":D
    .end local v2    # "mMinElevation":D
    .end local v4    # "scale":F
    .end local v8    # "stastics":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;
    .end local v9    # "unitMile":Z
    :cond_1
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const v11, 0x7f090811

    invoke-virtual {p0, v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ")"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitle(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 978
    :cond_2
    iget-boolean v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->kmUnit:Z

    if-eqz v10, :cond_3

    .line 979
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const v11, 0x7f09080f

    invoke-virtual {p0, v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ")"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitle(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 981
    :cond_3
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const v11, 0x7f090810

    invoke-virtual {p0, v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ")"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitle(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 992
    .restart local v0    # "mMaxElevation":D
    .restart local v2    # "mMinElevation":D
    .restart local v8    # "stastics":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;
    :cond_4
    const/4 v9, 0x0

    goto/16 :goto_1

    .line 1000
    .restart local v9    # "unitMile":Z
    :cond_5
    const-wide v10, 0x3ff199999999999aL    # 1.1

    mul-double/2addr v10, v0

    const-wide/high16 v12, 0x4059000000000000L    # 100.0

    div-double/2addr v10, v12

    const-wide/high16 v12, 0x4059000000000000L    # 100.0

    mul-double v0, v10, v12

    goto/16 :goto_2
.end method

.method private refreshChartView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 23
    .param p1, "periodStart"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 659
    new-instance v10, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    const/4 v4, 0x1

    const/4 v5, 0x3

    invoke-direct {v10, v4, v5}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;-><init>(II)V

    .line 661
    .local v10, "chartStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->initSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V

    .line 662
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v10, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->initChartStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 664
    new-instance v11, Lcom/samsung/android/sdk/chart/series/SchartDataSet;

    invoke-direct {v11}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;-><init>()V

    .line 666
    .local v11, "dataSet":Lcom/samsung/android/sdk/chart/series/SchartDataSet;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v11, v4}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->addSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)V

    .line 667
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v11, v4}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->addSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)V

    .line 669
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v11, v4}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->addSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)V

    .line 670
    new-instance v4, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-direct {v4, v5, v10, v11}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/samsung/android/sdk/chart/series/SchartDataSet;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    .line 672
    new-instance v16, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;

    invoke-direct/range {v16 .. v16}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;-><init>()V

    .line 674
    .local v16, "interaction":Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;
    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setScaleZoomInteractionEnabled(Z)V

    .line 675
    const/4 v4, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInteractionEnabled(Z)V

    .line 676
    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setPatialZoomInteractionEnabled(Z)V

    .line 677
    const/high16 v4, 0x40000000    # 2.0f

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomOutRate(F)V

    .line 678
    const/high16 v4, 0x40000000    # 2.0f

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInRate(F)V

    .line 680
    const/4 v4, 0x5

    const/4 v5, 0x5

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomIntervalStep(II)V

    .line 686
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setChartInteraction(Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;)V

    .line 703
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v14

    .line 704
    .local v14, "epochLastData":J
    const-wide/32 v4, 0x927c0

    sub-long v4, v14, v4

    const-wide/32 v6, 0x927c0

    rem-long v6, v14, v6

    sub-long v14, v4, v6

    .line 705
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    const/4 v5, 0x0

    long-to-double v6, v14

    const/16 v8, 0xa

    const/4 v9, 0x3

    invoke-virtual/range {v4 .. v9}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setStartVisual(IDII)V

    .line 709
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getStartTime()J

    move-result-wide v21

    .line 710
    .local v21, "startTime":J
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getEndTime()J

    move-result-wide v12

    .line 711
    .local v12, "endTime":J
    const-wide/16 v19, 0x0

    .line 712
    .local v19, "realStartTime":J
    const-wide/16 v17, 0x0

    .line 714
    .local v17, "realEndTime":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v21

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getStartTimeForChartRange(J)J

    move-result-wide v19

    .line 715
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getEndTimeForChartRange(J)J

    move-result-wide v17

    .line 718
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setScrollRangeType(I)V

    .line 719
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-wide/from16 v0, v19

    move-wide/from16 v2, v17

    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setUserScrollRange(JJ)V

    .line 721
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    const/4 v5, 0x0

    const/4 v6, 0x2

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setGraphType(II)V

    .line 722
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    const/4 v5, 0x1

    const/4 v6, 0x2

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setGraphType(II)V

    .line 724
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    const/4 v5, 0x2

    const/16 v6, 0xb

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setGraphType(II)V

    .line 726
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    const/4 v5, 0x0

    const/16 v6, 0x3e8

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->startGraphRevealAnimation(II)Z

    .line 727
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    const/4 v5, 0x1

    const/16 v6, 0x3e8

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->startGraphRevealAnimation(II)Z

    .line 729
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    const/4 v5, 0x2

    const/16 v6, 0x3e8

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->startGraphRevealAnimation(II)Z

    .line 733
    return-void
.end method

.method private setUpDataSet()V
    .locals 63

    .prologue
    .line 302
    new-instance v5, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v5}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 303
    new-instance v5, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v5}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 305
    new-instance v5, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v5}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 307
    const/16 v51, 0x0

    .line 314
    .local v51, "projection":[Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "select sample_time,user_device__id,create_time,speed_per_hour, heart_rate_per_min from (select user_device__id,sample_time,create_time,max(realtime_data.[speed_per_hour]) as speed_per_hour,realtime_data.[heart_rate_per_min] from realtime_data where exercise__id=="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mRowID:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " group by (sample_time-(select min(sample_time) from realtime_data where exercise__id == "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mRowID:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "))/60000 having min(ifnull(sample_time, create_time))"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " union "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "select user_device__id,sample_time,create_time,realtime_data.[speed_per_hour], max(realtime_data.[heart_rate_per_min]) as heart_rate_per_min from realtime_data where exercise__id=="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mRowID:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " group by (sample_time-(select min(sample_time) from realtime_data where exercise__id == "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mRowID:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "))/60000 having min(ifnull(sample_time, create_time)))"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " group by (ifnull(sample_time, create_time) - (select min(ifnull(sample_time, create_time)) from realtime_data where exercise__id == "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mRowID:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "))"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " order by ifnull(sample_time, create_time) asc  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 322
    .local v8, "selectionClause":Ljava/lang/String;
    const/16 v51, 0x0

    .line 324
    const/16 v19, 0x0

    .line 326
    .local v19, "cursor":Landroid/database/Cursor;
    const/4 v11, 0x0

    .line 327
    .local v11, "projection2":[Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "exercise__id = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mRowID:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 328
    .local v12, "selectionClause2":Ljava/lang/String;
    const/4 v11, 0x0

    .line 329
    const/16 v20, 0x0

    .line 332
    .local v20, "cursor2":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 334
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    sget-object v10, Lcom/samsung/android/sdk/health/content/ShealthContract$LocationData;->CONTENT_URI:Landroid/net/Uri;

    const/4 v13, 0x0

    const-string v14, "ifnull(sample_time, create_time) ASC"

    invoke-virtual/range {v9 .. v14}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v20

    .line 343
    const-wide/16 v37, 0x0

    .line 344
    .local v37, "heartRatePerMinSave":D
    const/4 v15, 0x0

    .line 345
    .local v15, "avg_speed_save":F
    const-wide/16 v46, 0x0

    .line 346
    .local v46, "lastSampleTimeSpeed":J
    const-wide/16 v44, 0x0

    .line 347
    .local v44, "lastSampleTimeHeartrate":J
    const-wide/16 v54, 0x0

    .line 351
    .local v54, "sampleTimeTemp":J
    if-eqz v19, :cond_1e

    if-eqz v20, :cond_1e

    .line 352
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-lez v5, :cond_15

    .line 353
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    .line 355
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v58

    .line 356
    .local v58, "start_c":Ljava/util/Calendar;
    const-string/jumbo v5, "sample_time"

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 357
    const-string v5, "create_time"

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v56

    .line 361
    .local v56, "startTime":J
    :goto_0
    move-object/from16 v0, v58

    move-wide/from16 v1, v56

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 364
    :goto_1
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v5

    if-nez v5, :cond_12

    .line 366
    const-string/jumbo v5, "sample_time"

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 367
    const-string v5, "create_time"

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v52

    .line 372
    .local v52, "sampleTime":J
    :goto_2
    const-wide/16 v25, 0x0

    .line 373
    .local v25, "deviceGroupType":J
    const-string/jumbo v5, "user_device__id"

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getDeviceGroupType(Ljava/lang/String;)J

    move-result-wide v25

    .line 387
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mRowID:J

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;->getExerciseData(Landroid/content/Context;J)Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->getmillisecond()J

    move-result-wide v5

    const-wide/32 v9, 0xea60

    div-long/2addr v5, v9

    long-to-int v5, v5

    add-int/lit8 v24, v5, 0x1

    .line 388
    .local v24, "dataPoints":I
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->getCount()I

    move-result v5

    move/from16 v0, v24

    if-le v0, v5, :cond_1

    .line 389
    sub-long v52, v52, v56

    .line 390
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->getCount()I

    move-result v5

    const/4 v6, 0x1

    if-le v5, v6, :cond_0

    .line 391
    add-int/lit8 v5, v24, -0x1

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->getCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    div-int/2addr v5, v6

    int-to-long v5, v5

    mul-long v52, v52, v5

    .line 392
    :cond_0
    add-long v52, v52, v56

    .line 394
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v18

    .line 395
    .local v18, "curr_c":Ljava/util/Calendar;
    move-object/from16 v0, v18

    move-wide/from16 v1, v52

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 396
    const/16 v5, 0xb

    const/16 v6, 0xb

    move-object/from16 v0, v58

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    neg-int v6, v6

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->add(II)V

    .line 397
    const/16 v5, 0xc

    const/16 v6, 0xc

    move-object/from16 v0, v58

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    neg-int v6, v6

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->add(II)V

    .line 398
    const/16 v5, 0xd

    const/16 v6, 0xd

    move-object/from16 v0, v58

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    neg-int v6, v6

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->add(II)V

    .line 399
    const/16 v5, 0xe

    const/16 v6, 0xe

    move-object/from16 v0, v58

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    neg-int v6, v6

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->add(II)V

    .line 400
    invoke-virtual/range {v18 .. v18}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v52

    .line 402
    const-string/jumbo v5, "speed_per_hour"

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getFloat(I)F

    move-result v5

    const/high16 v6, 0x447a0000    # 1000.0f

    div-float v34, v5, v6

    .line 403
    .local v34, "float_avg_speed":F
    const-string v5, "heart_rate_per_min"

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v35

    .line 405
    .local v35, "heartRatePerMin":D
    const/4 v5, 0x0

    cmpl-float v5, v34, v5

    if-nez v5, :cond_8

    .line 406
    move/from16 v15, v34

    .line 407
    const-wide/16 v5, 0x0

    cmpl-double v5, v35, v5

    if-nez v5, :cond_6

    .line 408
    const-wide/16 v44, 0x0

    .line 409
    const-wide/16 v37, 0x0

    .line 418
    :goto_3
    const-wide/16 v46, 0x0

    .line 437
    :goto_4
    new-instance v21, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct/range {v21 .. v21}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 440
    .local v21, "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getPaceMode()Z

    move-result v5

    if-eqz v5, :cond_10

    .line 441
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->kmUnit:Z

    if-eqz v5, :cond_e

    .line 443
    const/high16 v5, 0x40000000    # 2.0f

    cmpl-float v5, v15, v5

    if-ltz v5, :cond_b

    .line 444
    const/high16 v5, 0x45610000    # 3600.0f

    div-float/2addr v5, v15

    const/high16 v6, 0x42700000    # 60.0f

    div-float/2addr v5, v6

    float-to-double v0, v5

    move-wide/from16 v59, v0

    .line 445
    .local v59, "tempPace":D
    move-object/from16 v0, v21

    move-wide/from16 v1, v52

    move-wide/from16 v3, v59

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    .line 466
    .end local v59    # "tempPace":D
    :goto_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    .line 468
    const-wide/32 v5, 0x57e42

    cmp-long v5, v25, v5

    if-eqz v5, :cond_2

    const-wide/32 v5, 0x57e43

    cmp-long v5, v25, v5

    if-nez v5, :cond_3

    .line 469
    :cond_2
    new-instance v22, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct/range {v22 .. v22}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 470
    .local v22, "data2":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    const-wide/16 v5, 0x0

    cmpl-double v5, v35, v5

    if-eqz v5, :cond_3

    .line 471
    move-object/from16 v0, v22

    move-wide/from16 v1, v52

    move-wide/from16 v3, v35

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    .line 472
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    .line 483
    .end local v22    # "data2":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    :cond_3
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToNext()Z

    .line 484
    move-wide/from16 v54, v52

    .line 485
    goto/16 :goto_1

    .line 359
    .end local v18    # "curr_c":Ljava/util/Calendar;
    .end local v21    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .end local v24    # "dataPoints":I
    .end local v25    # "deviceGroupType":J
    .end local v34    # "float_avg_speed":F
    .end local v35    # "heartRatePerMin":D
    .end local v52    # "sampleTime":J
    .end local v56    # "startTime":J
    :cond_4
    const-string/jumbo v5, "sample_time"

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v56

    .restart local v56    # "startTime":J
    goto/16 :goto_0

    .line 369
    :cond_5
    const-string/jumbo v5, "sample_time"

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v52

    .restart local v52    # "sampleTime":J
    goto/16 :goto_2

    .line 411
    .restart local v18    # "curr_c":Ljava/util/Calendar;
    .restart local v24    # "dataPoints":I
    .restart local v25    # "deviceGroupType":J
    .restart local v34    # "float_avg_speed":F
    .restart local v35    # "heartRatePerMin":D
    :cond_6
    sub-long v5, v52, v46

    const-wide/16 v9, 0x2710

    cmp-long v5, v5, v9

    if-gez v5, :cond_7

    .line 412
    move/from16 v34, v15

    goto/16 :goto_3

    .line 414
    :cond_7
    move-wide/from16 v37, v35

    .line 415
    move-wide/from16 v44, v52

    goto/16 :goto_3

    .line 421
    :cond_8
    const-wide/16 v5, 0x0

    cmpl-double v5, v35, v5

    if-nez v5, :cond_a

    .line 422
    sub-long v5, v52, v44

    const-wide/16 v9, 0x2710

    cmp-long v5, v5, v9

    if-gez v5, :cond_9

    .line 423
    move-wide/from16 v35, v37

    .line 433
    :goto_6
    move-wide/from16 v46, v52

    .line 434
    move/from16 v15, v34

    goto/16 :goto_4

    .line 425
    :cond_9
    const-wide/16 v44, 0x0

    .line 426
    const-wide/16 v37, 0x0

    goto :goto_6

    .line 429
    :cond_a
    move-wide/from16 v37, v35

    .line 430
    move-wide/from16 v44, v52

    goto :goto_6

    .line 447
    .restart local v21    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    :cond_b
    const-wide/high16 v5, 0x403e000000000000L    # 30.0

    move-object/from16 v0, v21

    move-wide/from16 v1, v52

    invoke-virtual {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_5

    .line 639
    .end local v15    # "avg_speed_save":F
    .end local v18    # "curr_c":Ljava/util/Calendar;
    .end local v21    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .end local v24    # "dataPoints":I
    .end local v25    # "deviceGroupType":J
    .end local v34    # "float_avg_speed":F
    .end local v35    # "heartRatePerMin":D
    .end local v37    # "heartRatePerMinSave":D
    .end local v44    # "lastSampleTimeHeartrate":J
    .end local v46    # "lastSampleTimeSpeed":J
    .end local v52    # "sampleTime":J
    .end local v54    # "sampleTimeTemp":J
    .end local v56    # "startTime":J
    .end local v58    # "start_c":Ljava/util/Calendar;
    :catchall_0
    move-exception v5

    if-eqz v19, :cond_c

    .line 640
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 641
    :cond_c
    if-eqz v20, :cond_d

    .line 642
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    :cond_d
    throw v5

    .line 450
    .restart local v15    # "avg_speed_save":F
    .restart local v18    # "curr_c":Ljava/util/Calendar;
    .restart local v21    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .restart local v24    # "dataPoints":I
    .restart local v25    # "deviceGroupType":J
    .restart local v34    # "float_avg_speed":F
    .restart local v35    # "heartRatePerMin":D
    .restart local v37    # "heartRatePerMinSave":D
    .restart local v44    # "lastSampleTimeHeartrate":J
    .restart local v46    # "lastSampleTimeSpeed":J
    .restart local v52    # "sampleTime":J
    .restart local v54    # "sampleTimeTemp":J
    .restart local v56    # "startTime":J
    .restart local v58    # "start_c":Ljava/util/Calendar;
    :cond_e
    const v5, 0x404dfefc

    cmpl-float v5, v15, v5

    if-ltz v5, :cond_f

    .line 451
    const/high16 v5, 0x45610000    # 3600.0f

    const/high16 v6, 0x447a0000    # 1000.0f

    mul-float/2addr v6, v15

    const v7, 0x3a22e36f

    mul-float/2addr v6, v7

    div-float/2addr v5, v6

    const/high16 v6, 0x42700000    # 60.0f

    div-float/2addr v5, v6

    float-to-double v0, v5

    move-wide/from16 v59, v0

    .line 452
    .restart local v59    # "tempPace":D
    :try_start_1
    move-object/from16 v0, v21

    move-wide/from16 v1, v52

    move-wide/from16 v3, v59

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    goto/16 :goto_5

    .line 454
    .end local v59    # "tempPace":D
    :cond_f
    const-wide/high16 v5, 0x403e000000000000L    # 30.0

    move-object/from16 v0, v21

    move-wide/from16 v1, v52

    invoke-virtual {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    goto/16 :goto_5

    .line 459
    :cond_10
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->kmUnit:Z

    if-eqz v5, :cond_11

    .line 460
    float-to-double v5, v15

    move-object/from16 v0, v21

    move-wide/from16 v1, v52

    invoke-virtual {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    goto/16 :goto_5

    .line 462
    :cond_11
    float-to-long v5, v15

    const-wide/16 v9, 0x3e8

    mul-long/2addr v5, v9

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getMilesFromMeters(J)Ljava/lang/String;

    move-result-object v50

    .line 463
    .local v50, "mi_speed":Ljava/lang/String;
    invoke-static/range {v50 .. v50}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    move-object/from16 v0, v21

    move-wide/from16 v1, v52

    invoke-virtual {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    goto/16 :goto_5

    .line 486
    .end local v18    # "curr_c":Ljava/util/Calendar;
    .end local v21    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .end local v24    # "dataPoints":I
    .end local v25    # "deviceGroupType":J
    .end local v34    # "float_avg_speed":F
    .end local v35    # "heartRatePerMin":D
    .end local v50    # "mi_speed":Ljava/lang/String;
    .end local v52    # "sampleTime":J
    :cond_12
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 505
    .end local v56    # "startTime":J
    .end local v58    # "start_c":Ljava/util/Calendar;
    :goto_7
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 506
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->getCount()I

    move-result v5

    const/4 v6, 0x2

    if-le v5, v6, :cond_1a

    .line 507
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToFirst()Z

    .line 509
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v58

    .line 510
    .restart local v58    # "start_c":Ljava/util/Calendar;
    const-string/jumbo v5, "sample_time"

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v5

    if-eqz v5, :cond_16

    .line 511
    const-string v5, "create_time"

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v56

    .line 515
    .restart local v56    # "startTime":J
    :goto_8
    move-object/from16 v0, v58

    move-wide/from16 v1, v56

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 516
    const-wide/16 v42, 0x0

    .line 517
    .local v42, "lastElevation":D
    const-wide/16 v48, 0x0

    .line 518
    .local v48, "lastTime":J
    :cond_13
    :goto_9
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v5

    if-nez v5, :cond_1b

    .line 520
    const-string/jumbo v5, "sample_time"

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v5

    if-eqz v5, :cond_17

    .line 521
    const-string v5, "create_time"

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v52

    .line 533
    .restart local v52    # "sampleTime":J
    :goto_a
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v18

    .line 534
    .restart local v18    # "curr_c":Ljava/util/Calendar;
    move-object/from16 v0, v18

    move-wide/from16 v1, v52

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 535
    const/16 v5, 0xb

    const/16 v6, 0xb

    move-object/from16 v0, v58

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    neg-int v6, v6

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->add(II)V

    .line 536
    const/16 v5, 0xc

    const/16 v6, 0xc

    move-object/from16 v0, v58

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    neg-int v6, v6

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->add(II)V

    .line 537
    const/16 v5, 0xd

    const/16 v6, 0xd

    move-object/from16 v0, v58

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    neg-int v6, v6

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->add(II)V

    .line 538
    const/16 v5, 0xe

    const/16 v6, 0xe

    move-object/from16 v0, v58

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    neg-int v6, v6

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->add(II)V

    .line 539
    invoke-virtual/range {v18 .. v18}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v52

    .line 557
    const-string v5, "altitude"

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v32

    .line 559
    .local v32, "elevation":D
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->loadUnitsFromSharedPreferences()V

    .line 560
    sget-object v5, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoDistanceUnit:Ljava/lang/String;

    const-string v6, "km"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_18

    const/16 v62, 0x1

    .line 561
    .local v62, "unitMile":Z
    :goto_b
    if-eqz v62, :cond_14

    .line 562
    const-wide v5, 0x400a3f290abb44e5L    # 3.28084

    mul-double v32, v32, v5

    .line 567
    :cond_14
    new-instance v23, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct/range {v23 .. v23}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 569
    .local v23, "data3":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    move-object/from16 v0, v23

    move-wide/from16 v1, v52

    move-wide/from16 v3, v32

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    .line 570
    sget-object v5, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "DataSet Add SampleTime: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static/range {v52 .. v53}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " Elevation: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static/range {v32 .. v33}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 573
    sub-long v29, v52, v48

    .line 574
    .local v29, "diffMilisec":J
    sub-double v27, v32, v42

    .line 575
    .local v27, "diffElevation":D
    const-wide/32 v5, 0xea60

    div-long v5, v29, v5

    long-to-int v0, v5

    move/from16 v31, v0

    .line 577
    .local v31, "diffMin":I
    const-wide/16 v5, 0x0

    cmp-long v5, v48, v5

    if-eqz v5, :cond_19

    .line 578
    const/16 v61, 0x0

    .local v61, "times":I
    :goto_c
    move/from16 v0, v61

    move/from16 v1, v31

    if-ge v0, v1, :cond_19

    .line 579
    new-instance v39, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct/range {v39 .. v39}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 580
    .local v39, "interpolatedData":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    div-int v5, v61, v31

    int-to-double v5, v5

    mul-double v5, v5, v27

    add-double v40, v32, v5

    .line 581
    .local v40, "interpolatedElevation":D
    const v5, 0xea60

    mul-int v5, v5, v61

    int-to-long v5, v5

    add-long v5, v5, v48

    move-object/from16 v0, v39

    move-wide/from16 v1, v40

    invoke-virtual {v0, v5, v6, v1, v2}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    .line 582
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v0, v39

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    .line 578
    add-int/lit8 v61, v61, 0x1

    goto :goto_c

    .line 488
    .end local v18    # "curr_c":Ljava/util/Calendar;
    .end local v23    # "data3":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .end local v27    # "diffElevation":D
    .end local v29    # "diffMilisec":J
    .end local v31    # "diffMin":I
    .end local v32    # "elevation":D
    .end local v39    # "interpolatedData":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .end local v40    # "interpolatedElevation":D
    .end local v42    # "lastElevation":D
    .end local v48    # "lastTime":J
    .end local v52    # "sampleTime":J
    .end local v56    # "startTime":J
    .end local v58    # "start_c":Ljava/util/Calendar;
    .end local v61    # "times":I
    .end local v62    # "unitMile":Z
    :cond_15
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v18

    .line 490
    .restart local v18    # "curr_c":Ljava/util/Calendar;
    const/16 v5, 0xb

    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 491
    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 492
    const/16 v5, 0xd

    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 493
    const/16 v5, 0xe

    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 495
    invoke-virtual/range {v18 .. v18}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v16

    .line 496
    .local v16, "c":J
    new-instance v21, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct/range {v21 .. v21}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 497
    .restart local v21    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    new-instance v22, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct/range {v22 .. v22}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 499
    .restart local v22    # "data2":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    const-wide/16 v5, 0x0

    move-object/from16 v0, v21

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    .line 500
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    .line 502
    const-wide/16 v5, 0x0

    move-object/from16 v0, v22

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    .line 503
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    goto/16 :goto_7

    .line 513
    .end local v16    # "c":J
    .end local v18    # "curr_c":Ljava/util/Calendar;
    .end local v21    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .end local v22    # "data2":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .restart local v58    # "start_c":Ljava/util/Calendar;
    :cond_16
    const-string/jumbo v5, "sample_time"

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v56

    .restart local v56    # "startTime":J
    goto/16 :goto_8

    .line 523
    .restart local v42    # "lastElevation":D
    .restart local v48    # "lastTime":J
    :cond_17
    const-string/jumbo v5, "sample_time"

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v52

    .restart local v52    # "sampleTime":J
    goto/16 :goto_a

    .line 560
    .restart local v18    # "curr_c":Ljava/util/Calendar;
    .restart local v32    # "elevation":D
    :cond_18
    const/16 v62, 0x0

    goto/16 :goto_b

    .line 588
    .restart local v23    # "data3":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .restart local v27    # "diffElevation":D
    .restart local v29    # "diffMilisec":J
    .restart local v31    # "diffMin":I
    .restart local v62    # "unitMile":Z
    :cond_19
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    .line 589
    move-wide/from16 v42, v32

    .line 590
    move-wide/from16 v48, v52

    .line 591
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToNext()Z

    .line 592
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v5

    if-eqz v5, :cond_13

    cmp-long v5, v54, v52

    if-lez v5, :cond_13

    .line 593
    new-instance v21, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct/range {v21 .. v21}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 594
    .restart local v21    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    move-object/from16 v0, v21

    move-wide/from16 v1, v54

    move-wide/from16 v3, v32

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    .line 595
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    goto/16 :goto_9

    .line 599
    .end local v18    # "curr_c":Ljava/util/Calendar;
    .end local v21    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .end local v23    # "data3":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .end local v27    # "diffElevation":D
    .end local v29    # "diffMilisec":J
    .end local v31    # "diffMin":I
    .end local v32    # "elevation":D
    .end local v42    # "lastElevation":D
    .end local v48    # "lastTime":J
    .end local v52    # "sampleTime":J
    .end local v56    # "startTime":J
    .end local v58    # "start_c":Ljava/util/Calendar;
    .end local v62    # "unitMile":Z
    :cond_1a
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v18

    .line 601
    .restart local v18    # "curr_c":Ljava/util/Calendar;
    const/16 v5, 0xb

    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 602
    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 603
    const/16 v5, 0xd

    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 604
    const/16 v5, 0xe

    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 606
    invoke-virtual/range {v18 .. v18}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v16

    .line 607
    .restart local v16    # "c":J
    new-instance v23, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct/range {v23 .. v23}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 608
    .restart local v23    # "data3":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    const-wide/16 v5, 0x0

    move-object/from16 v0, v23

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    .line 609
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    .line 611
    .end local v16    # "c":J
    .end local v18    # "curr_c":Ljava/util/Calendar;
    .end local v23    # "data3":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    :cond_1b
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 639
    :goto_d
    if-eqz v19, :cond_1c

    .line 640
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 641
    :cond_1c
    if-eqz v20, :cond_1d

    .line 642
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    .line 644
    :cond_1d
    return-void

    .line 613
    :cond_1e
    :try_start_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v18

    .line 615
    .restart local v18    # "curr_c":Ljava/util/Calendar;
    const/16 v5, 0xb

    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 616
    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 617
    const/16 v5, 0xd

    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 618
    const/16 v5, 0xe

    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 620
    invoke-virtual/range {v18 .. v18}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v16

    .line 622
    .restart local v16    # "c":J
    new-instance v21, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct/range {v21 .. v21}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 623
    .restart local v21    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    const-wide/16 v5, 0x0

    move-object/from16 v0, v21

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    .line 624
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    .line 626
    new-instance v22, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct/range {v22 .. v22}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 627
    .restart local v22    # "data2":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    const-wide/16 v5, 0x0

    move-object/from16 v0, v22

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    .line 628
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    .line 634
    new-instance v23, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct/range {v23 .. v23}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 635
    .restart local v23    # "data3":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    const-wide/16 v5, 0x0

    move-object/from16 v0, v23

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    .line 636
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_d
.end method


# virtual methods
.method public getChartView()Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    .locals 1

    .prologue
    .line 1427
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    return-object v0
.end method

.method public getLegendView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1431
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mLegendView:Landroid/view/View;

    return-object v0
.end method

.method protected initGraphLegendArea()Landroid/view/View;
    .locals 6

    .prologue
    const v5, 0x7f080528

    const/16 v4, 0x8

    .line 1382
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 1383
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f0301bf

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mLegendView:Landroid/view/View;

    .line 1385
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mLegendView:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1386
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mLegendView:Landroid/view/View;

    const v3, 0x7f0807d9

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1387
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mLegendView:Landroid/view/View;

    const v3, 0x7f080529

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1388
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mLegendView:Landroid/view/View;

    const v3, 0x7f0807da

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1389
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mLegendView:Landroid/view/View;

    const v3, 0x7f0807dc

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1390
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mLegendView:Landroid/view/View;

    const v3, 0x7f0807db

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1391
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mLegendView:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1392
    .local v0, "first_text":Landroid/widget/TextView;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getPaceMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1393
    const v2, 0x7f090a4d

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1404
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mLegendView:Landroid/view/View;

    return-object v2

    .line 1395
    :cond_0
    const v2, 0x7f090a49

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method protected initGraphView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
    .locals 1
    .param p1, "periodStart"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 264
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 265
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries2:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 267
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mSeries3:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 268
    iput-object p3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 270
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->setUpDataSet()V

    .line 271
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->refreshChartView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    return-object v0
.end method

.method protected initInformationArea(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
    .locals 5
    .param p1, "periodStart"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1331
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    .line 1332
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    if-eqz v1, :cond_1

    .line 1333
    const/4 v0, 0x0

    .line 1335
    .local v0, "viewCount":I
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setFirstInformationVisible(Z)V

    .line 1338
    add-int/lit8 v0, v0, 0x1

    .line 1341
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->isValidExtHrChart:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->isValidDeviceHrChart:Z

    if-eqz v1, :cond_2

    .line 1342
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setSecondInformationVisible(Z)V

    .line 1343
    add-int/lit8 v0, v0, 0x1

    .line 1349
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->isValidElevChart:Z

    if-eqz v1, :cond_3

    .line 1350
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setThirdInformationVisible(Z)V

    .line 1351
    add-int/lit8 v0, v0, 0x1

    .line 1356
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setInformationWeight(I)V

    .line 1362
    .end local v0    # "viewCount":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    return-object v1

    .line 1345
    .restart local v0    # "viewCount":I
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setSecondInformationVisible(Z)V

    goto :goto_0

    .line 1353
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setThirdInformationVisible(Z)V

    goto :goto_1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v3, 0x7f090810

    const v2, 0x7f09080f

    .line 218
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->density:F

    .line 219
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->scaledDensity:F

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->scaledDensity:F

    .line 220
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 221
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->hideTabLayout()V

    .line 226
    :goto_0
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getDistanceUnitFromSharedPreferences()Ljava/lang/String;

    move-result-object v0

    const-string v1, "km"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->kmUnit:Z

    .line 227
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->kmUnit:Z

    if-eqz v0, :cond_5

    .line 228
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->speedUnit:Ljava/lang/String;

    .line 233
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->speedUnit:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 234
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->speedUnit:Ljava/lang/String;

    .line 236
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->bpmUnit:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 237
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->bpmUnit:Ljava/lang/String;

    .line 239
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->paceUnit:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 240
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getPaceMode()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 241
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->kmUnit:Z

    if-eqz v0, :cond_6

    .line 242
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900c7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->paceUnit:Ljava/lang/String;

    .line 252
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->elevationUnit:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 253
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090a57

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->elevationUnit:Ljava/lang/String;

    .line 256
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mContext:Landroid/app/Activity;

    .line 258
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mRootView:Landroid/view/View;

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mRootView:Landroid/view/View;

    return-object v0

    .line 223
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->showTabLayout()V

    goto/16 :goto_0

    .line 230
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->speedUnit:Ljava/lang/String;

    goto/16 :goto_1

    .line 244
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900cc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->paceUnit:Ljava/lang/String;

    goto :goto_2

    .line 246
    :cond_7
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->kmUnit:Z

    if-eqz v0, :cond_8

    .line 247
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->paceUnit:Ljava/lang/String;

    goto :goto_2

    .line 249
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->paceUnit:Ljava/lang/String;

    goto/16 :goto_2
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 164
    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mNormalBitmapYellowGreen:Landroid/graphics/Bitmap;

    .line 169
    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mHandleOverBitmapYellowGreen:Landroid/graphics/Bitmap;

    .line 174
    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mNormalBitmapRed:Landroid/graphics/Bitmap;

    .line 179
    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mHandleOverBitmapRed:Landroid/graphics/Bitmap;

    .line 184
    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mNormalBitmapBlack:Landroid/graphics/Bitmap;

    .line 188
    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mHandleOverBitmapBlack:Landroid/graphics/Bitmap;

    .line 193
    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mGraphBackground:Landroid/graphics/Bitmap;

    .line 198
    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mGraphHandler:Landroid/graphics/Bitmap;

    .line 203
    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mGraphLine:Landroid/graphics/Bitmap;

    .line 205
    invoke-super {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->onDestroyView()V

    .line 206
    return-void
.end method

.method public setIsPaceMode(Z)V
    .locals 0
    .param p1, "isPaceMode"    # Z

    .prologue
    .line 156
    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->setPaceMode(Z)V

    .line 157
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->updateGraphFragment()V

    .line 158
    return-void
.end method

.method public updateGraphFragment()V
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mRootView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 212
    invoke-super {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->updateGraphFragment()V

    .line 214
    :cond_0
    return-void
.end method
