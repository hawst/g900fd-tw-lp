.class Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$9;
.super Ljava/lang/Object;
.source "FoodTrackerMainContainer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->initImageByPaths(Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;Landroid/widget/ImageView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

.field final synthetic val$dayMealDataHolder:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;)V
    .locals 0

    .prologue
    .line 546
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$9;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    iput-object p2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$9;->val$dayMealDataHolder:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 552
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$9;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->access$200(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ENTER_PHOTO_BY_ICON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 554
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$9;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    # invokes: Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getFoodTrackerProvider()Lcom/sec/android/app/shealth/food/summary/FoodTrackerProvider;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->access$300(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;)Lcom/sec/android/app/shealth/food/summary/FoodTrackerProvider;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$9;->val$dayMealDataHolder:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->getMealType()Lcom/sec/android/app/shealth/food/constants/MealType;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerProvider;->startCameraActivity(Lcom/sec/android/app/shealth/food/constants/MealType;)V

    .line 556
    return-void
.end method
