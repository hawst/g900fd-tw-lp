.class public Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;
.super Landroid/os/AsyncTask;
.source "MaskTools.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/mask/MaskTools;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CollectingBitmapTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private mBitmapProvider:Lcom/sec/android/app/shealth/food/mask/BitmapProvider;

.field private mImagePaths:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mResultListener:Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;

.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mask/MaskTools;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mask/MaskTools;Ljava/util/List;Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;Lcom/sec/android/app/shealth/food/mask/BitmapProvider;)V
    .locals 0
    .param p3, "resultListener"    # Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;
    .param p4, "bitmapProvider"    # Lcom/sec/android/app/shealth/food/mask/BitmapProvider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;",
            "Lcom/sec/android/app/shealth/food/mask/BitmapProvider;",
            ")V"
        }
    .end annotation

    .prologue
    .line 225
    .local p2, "imagePaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;->this$0:Lcom/sec/android/app/shealth/food/mask/MaskTools;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 226
    iput-object p3, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;->mResultListener:Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;

    .line 227
    iput-object p4, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;->mBitmapProvider:Lcom/sec/android/app/shealth/food/mask/BitmapProvider;

    .line 228
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;->mImagePaths:Ljava/util/List;

    .line 229
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "voids"    # [Ljava/lang/Void;

    .prologue
    .line 233
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;->this$0:Lcom/sec/android/app/shealth/food/mask/MaskTools;

    # getter for: Lcom/sec/android/app/shealth/food/mask/MaskTools;->mWidth:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/mask/MaskTools;->access$000(Lcom/sec/android/app/shealth/food/mask/MaskTools;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;->this$0:Lcom/sec/android/app/shealth/food/mask/MaskTools;

    # getter for: Lcom/sec/android/app/shealth/food/mask/MaskTools;->mHeight:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/mask/MaskTools;->access$100(Lcom/sec/android/app/shealth/food/mask/MaskTools;)I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 234
    .local v1, "resultBitmap":Landroid/graphics/Bitmap;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;->mImagePaths:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;->this$0:Lcom/sec/android/app/shealth/food/mask/MaskTools;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/mask/MaskTools;->getAvailableSquaresCount()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 235
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 240
    :cond_0
    return-object v1

    .line 238
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;->this$0:Lcom/sec/android/app/shealth/food/mask/MaskTools;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;->mImagePaths:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;->mBitmapProvider:Lcom/sec/android/app/shealth/food/mask/BitmapProvider;

    # invokes: Lcom/sec/android/app/shealth/food/mask/MaskTools;->drawBitmap(ILjava/lang/String;Landroid/graphics/Bitmap;Lcom/sec/android/app/shealth/food/mask/BitmapProvider;)V
    invoke-static {v3, v0, v2, v1, v4}, Lcom/sec/android/app/shealth/food/mask/MaskTools;->access$200(Lcom/sec/android/app/shealth/food/mask/MaskTools;ILjava/lang/String;Landroid/graphics/Bitmap;Lcom/sec/android/app/shealth/food/mask/BitmapProvider;)V

    .line 234
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 220
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;->doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "result"    # Landroid/graphics/Bitmap;

    .prologue
    .line 246
    if-eqz p1, :cond_0

    .line 248
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 250
    :cond_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onCancelled(Ljava/lang/Object;)V

    .line 251
    return-void
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 220
    check-cast p1, Landroid/graphics/Bitmap;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;->onCancelled(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 254
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;->mResultListener:Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;->mResultListener:Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;

    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;->onDrawingResult(Landroid/graphics/Bitmap;)V

    .line 257
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 220
    check-cast p1, Landroid/graphics/Bitmap;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/mask/MaskTools$CollectingBitmapTask;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
