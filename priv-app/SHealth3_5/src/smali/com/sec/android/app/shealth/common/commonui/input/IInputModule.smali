.class public interface abstract Lcom/sec/android/app/shealth/common/commonui/input/IInputModule;
.super Ljava/lang/Object;
.source "IInputModule.java"


# virtual methods
.method public abstract getValue()Ljava/lang/Float;
.end method

.method public abstract hideDecBtn(Z)V
.end method

.method public abstract hideIncBtn(Z)V
.end method

.method public abstract hideUnit()V
.end method

.method public abstract isEmpty()Z
.end method

.method public abstract isOutOfRange()Z
.end method

.method public abstract refreshView()V
.end method

.method public abstract setAdditionalDrawingType(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;)V
.end method

.method public abstract setEnabled(Z)V
.end method

.method public abstract setInputRange(FF)V
.end method

.method public abstract setMoveDistance(F)V
.end method

.method public abstract setNormalRange(FF)V
.end method

.method public abstract setUnit(Ljava/lang/CharSequence;)V
.end method

.method public abstract setValue(F)V
.end method

.method public abstract showUnit()V
.end method

.method public abstract stopInertia()V
.end method
