.class Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$21;
.super Lcom/sec/android/app/shealth/food/foodpick/loader/ExtraFoodInfoRequestTask;
.source "SearchFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->addFoodItem(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

.field final synthetic val$foodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Landroid/content/Context;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .prologue
    .line 1192
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$21;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    iput-object p4, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$21;->val$foodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/shealth/food/foodpick/loader/ExtraFoodInfoRequestTask;-><init>(Landroid/content/Context;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    return-void
.end method


# virtual methods
.method public onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;)V
    .locals 4
    .param p1, "extraFoodInfoResult"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;

    .prologue
    .line 1196
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/loader/ExtraFoodInfoRequestTask;->onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;)V

    .line 1197
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$21;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    new-instance v1, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$21;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$21;->val$foodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v1

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->showPortionSizePopup(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$3000(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    .line 1198
    return-void
.end method

.method public bridge synthetic onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;

    .prologue
    .line 1192
    check-cast p1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;

    .end local p1    # "x0":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$21;->onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;)V

    return-void
.end method
