.class public final enum Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;
.super Ljava/lang/Enum;
.source "HeartrateTimerType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

.field public static final enum MEASURING:Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

.field public static final enum MEASURING_ERROR:Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

.field public static final enum READY:Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;


# instance fields
.field private timerType:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 22
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    const-string v1, "READY"

    invoke-direct {v0, v1, v3, v3}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;->READY:Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    .line 23
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    const-string v1, "MEASURING"

    invoke-direct {v0, v1, v4, v4}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;->MEASURING:Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    .line 24
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    const-string v1, "MEASURING_ERROR"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;->MEASURING_ERROR:Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    .line 21
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    sget-object v1, Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;->READY:Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;->MEASURING:Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;->MEASURING_ERROR:Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;->$VALUES:[Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "timerType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 29
    iput p3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;->timerType:I

    .line 30
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 21
    const-class v0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;->$VALUES:[Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    return-object v0
.end method
