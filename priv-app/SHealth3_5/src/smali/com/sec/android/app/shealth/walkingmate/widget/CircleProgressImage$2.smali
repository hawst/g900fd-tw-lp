.class Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$2;
.super Ljava/lang/Object;
.source "CircleProgressImage.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->initProgress(F)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 94
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mQueue:Ljava/util/concurrent/BlockingQueue;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->access$100(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)Ljava/util/concurrent/BlockingQueue;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mExecutor:Ljava/util/concurrent/ExecutorService;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->access$200(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->isShutdown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 103
    :goto_0
    return-void

    .line 98
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mQueue:Ljava/util/concurrent/BlockingQueue;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->access$100(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)Ljava/util/concurrent/BlockingQueue;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    const-wide/16 v2, 0x28

    invoke-virtual {v1, p0, v2, v3}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 99
    :catch_0
    move-exception v0

    .line 100
    .local v0, "e":Ljava/lang/InterruptedException;
    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->access$300()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
