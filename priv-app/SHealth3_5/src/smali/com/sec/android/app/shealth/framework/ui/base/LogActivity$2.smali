.class Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$2;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/ExpandableListView$OnChildClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChildClick(Landroid/widget/ExpandableListView;Landroid/view/View;IIJ)Z
    .locals 8

    const/4 v0, 0x3

    new-array v2, v0, [J

    const/4 v0, 0x2

    const-wide/16 v3, 0x3

    aput-wide v3, v2, v0

    const/4 v0, 0x0

    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    aget-wide v3, v2, v1

    long-to-int v1, v3

    if-gtz v1, :cond_0

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/4 v3, 0x0

    int-to-long v0, p3

    const/16 v4, 0x20

    shl-long/2addr v0, v4

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    aget-wide v0, v2, v3

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-eqz v6, :cond_1

    const-wide v6, 0x288b167155470c50L    # 2.1999015332233293E-113

    xor-long/2addr v0, v6

    :cond_1
    const/16 v6, 0x20

    ushr-long/2addr v0, v6

    const/16 v6, 0x20

    shl-long/2addr v0, v6

    xor-long/2addr v0, v4

    const-wide v4, 0x288b167155470c50L    # 2.1999015332233293E-113

    xor-long/2addr v0, v4

    aput-wide v0, v2, v3

    const/4 v0, 0x1

    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    aget-wide v3, v2, v1

    long-to-int v1, v3

    if-lt v0, v1, :cond_2

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    const/4 v3, 0x0

    int-to-long v0, p4

    const/16 v4, 0x20

    shl-long v4, v0, v4

    aget-wide v0, v2, v3

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-eqz v6, :cond_3

    const-wide v6, 0x288b167155470c50L    # 2.1999015332233293E-113

    xor-long/2addr v0, v6

    :cond_3
    const/16 v6, 0x20

    shl-long/2addr v0, v6

    const/16 v6, 0x20

    ushr-long/2addr v0, v6

    xor-long/2addr v0, v4

    const-wide v4, 0x288b167155470c50L    # 2.1999015332233293E-113

    xor-long/2addr v0, v4

    aput-wide v0, v2, v3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isDeleteMode:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isSelectMode:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$200(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isItemClicked:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$300(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isItemClicked:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$302(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Z)Z

    :cond_5
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isDeleteMode:Z
    invoke-static {v3}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isSelectMode:Z
    invoke-static {v3}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$200(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->showDetailScreen(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->showDetailScreen(Ljava/lang/Object;)V

    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->refreshFocusables()V

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$400(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->isCheckAll()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$400(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->setCheckAll(Z)V

    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$400(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$400(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->isLogSelected(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v3, v1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->setLogSelected(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$400(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mCursor:Landroid/database/Cursor;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->changeCursor(Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$400(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$400(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getSelectedLogCount()I

    move-result v1

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->updateSelectedCount(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$500(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->toggleSelectAllCheckBox()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$600(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$400(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getSelectedLogCount()I

    move-result v0

    if-nez v0, :cond_9

    const/4 v0, 0x1

    :goto_3
    const/4 v1, 0x2

    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    aget-wide v3, v2, v3

    long-to-int v3, v3

    if-lt v1, v3, :cond_a

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_2

    :cond_9
    const/4 v0, 0x0

    goto :goto_3

    :cond_a
    const/4 v3, 0x1

    int-to-long v0, v0

    const/16 v4, 0x20

    shl-long/2addr v0, v4

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    aget-wide v0, v2, v3

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-eqz v6, :cond_b

    const-wide v6, 0x288b167155470c50L    # 2.1999015332233293E-113

    xor-long/2addr v0, v6

    :cond_b
    const/16 v6, 0x20

    ushr-long/2addr v0, v6

    const/16 v6, 0x20

    shl-long/2addr v0, v6

    xor-long/2addr v0, v4

    const-wide v4, 0x288b167155470c50L    # 2.1999015332233293E-113

    xor-long/2addr v0, v4

    aput-wide v0, v2, v3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isSelectMode:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$200(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Z

    move-result v0

    if-eqz v0, :cond_11

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    const/4 v1, 0x2

    if-gt v0, v1, :cond_e

    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, -0x6

    aput v2, v1, v0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/16 v4, -0x38

    aput v4, v0, v2

    const/4 v2, 0x0

    :goto_4
    array-length v4, v0

    if-lt v2, v4, :cond_c

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_5
    array-length v4, v0

    if-lt v2, v4, :cond_d

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_c
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_d
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_e
    const/4 v0, 0x1

    aget-wide v0, v2, v0

    const-wide/16 v4, 0x0

    cmp-long v2, v0, v4

    if-eqz v2, :cond_f

    const-wide v4, 0x288b167155470c50L    # 2.1999015332233293E-113

    xor-long/2addr v0, v4

    :cond_f
    const/16 v2, 0x20

    shl-long/2addr v0, v2

    const/16 v2, 0x20

    shr-long/2addr v0, v2

    long-to-int v0, v0

    if-nez v0, :cond_10

    const/4 v0, 0x1

    :goto_6
    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->toggleActionButtonsVisibilityforSelectMode(Z)V
    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$700(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->toggleShareViewButton()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$800(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V

    goto/16 :goto_1

    :cond_10
    const/4 v0, 0x0

    goto :goto_6

    :cond_11
    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    const/4 v1, 0x2

    if-gt v0, v1, :cond_14

    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/16 v2, -0x75

    aput v2, v1, v0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/16 v4, -0x47

    aput v4, v0, v2

    const/4 v2, 0x0

    :goto_7
    array-length v4, v0

    if-lt v2, v4, :cond_12

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_8
    array-length v4, v0

    if-lt v2, v4, :cond_13

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_12
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    :cond_13
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    :cond_14
    const/4 v0, 0x1

    aget-wide v0, v2, v0

    const-wide/16 v4, 0x0

    cmp-long v2, v0, v4

    if-eqz v2, :cond_15

    const-wide v4, 0x288b167155470c50L    # 2.1999015332233293E-113

    xor-long/2addr v0, v4

    :cond_15
    const/16 v2, 0x20

    shl-long/2addr v0, v2

    const/16 v2, 0x20

    shr-long/2addr v0, v2

    long-to-int v0, v0

    if-nez v0, :cond_16

    const/4 v0, 0x1

    :goto_9
    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->enableActionBarButtons(Z)V
    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$900(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Z)V

    goto/16 :goto_1

    :cond_16
    const/4 v0, 0x0

    goto :goto_9
.end method
