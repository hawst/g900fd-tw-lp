.class Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;
.super Ljava/lang/Object;
.source "RealtimeHealthService.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V
    .locals 0

    .prologue
    .line 2634
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceived(ILcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;Landroid/os/Bundle;)V
    .locals 14
    .param p1, "dataType"    # I
    .param p2, "data"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
    .param p3, "extra"    # Landroid/os/Bundle;

    .prologue
    .line 2638
    const-string v11, "mFEDataListener"

    const-string v12, "mFEDataListener"

    invoke-static {v11, v12}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 2641
    const/16 v11, 0x8

    if-ne p1, v11, :cond_c

    move-object/from16 v5, p2

    .line 2643
    check-cast v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;

    .line 2645
    .local v5, "feData":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;
    iget v10, v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->equipmentType:I

    .line 2646
    .local v10, "type":I
    packed-switch v10, :pswitch_data_0

    .line 2673
    :goto_0
    :pswitch_0
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Ljava/util/ArrayList;

    move-result-object v12

    monitor-enter v12

    .line 2674
    :try_start_0
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2676
    .local v8, "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :try_start_1
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->activity_type:I

    invoke-interface {v8, v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onSensorInfoReceived(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 2678
    :catch_0
    move-exception v4

    .line 2679
    .local v4, "e":Ljava/lang/Exception;
    :try_start_2
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v11

    const-string v13, "mFEDataListener - onSensorInfoReceived->Exception"

    invoke-static {v11, v13}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2682
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v8    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :catchall_0
    move-exception v11

    monitor-exit v12
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v11

    .line 2649
    :pswitch_1
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    const-wide/16 v12, 0x1

    iput-wide v12, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->activity_exID:J

    goto :goto_0

    .line 2652
    :pswitch_2
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    const-wide/16 v12, 0x3

    iput-wide v12, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->activity_exID:J

    goto :goto_0

    .line 2655
    :pswitch_3
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    const-wide/16 v12, 0x2d

    iput-wide v12, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->activity_exID:J

    goto :goto_0

    .line 2658
    :pswitch_4
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    const-wide/16 v12, 0x2e

    iput-wide v12, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->activity_exID:J

    goto :goto_0

    .line 2661
    :pswitch_5
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    const-wide/16 v12, 0x2f

    iput-wide v12, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->activity_exID:J

    goto :goto_0

    .line 2664
    :pswitch_6
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    const-wide/16 v12, 0x15

    iput-wide v12, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->activity_exID:J

    goto :goto_0

    .line 2667
    :pswitch_7
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    const-wide/16 v12, 0x1c

    iput-wide v12, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->activity_exID:J

    goto :goto_0

    .line 2682
    .restart local v7    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_3
    monitor-exit v12
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2684
    iget-wide v2, v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->duration:J

    .line 2685
    .local v2, "duration":J
    const-wide v11, 0x7fffffffffffffffL

    cmp-long v11, v2, v11

    if-eqz v11, :cond_3

    .line 2686
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->duration:F

    long-to-float v12, v2

    cmpl-float v11, v11, v12

    if-eqz v11, :cond_2

    .line 2687
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    .line 2688
    const/4 v11, 0x1

    long-to-int v12, v2

    int-to-float v12, v12

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->workoutProgress(IF)V

    .line 2689
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Ljava/util/ArrayList;

    move-result-object v12

    monitor-enter v12

    .line 2690
    :try_start_4
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2692
    .restart local v8    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :try_start_5
    invoke-interface {v8, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateLapClock(J)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_2

    .line 2694
    :catch_1
    move-exception v4

    .line 2695
    .restart local v4    # "e":Ljava/lang/Exception;
    :try_start_6
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v11

    const-string v13, "mFEDataListener - duration onUpdateLapClock->Exception"

    invoke-static {v11, v13}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 2698
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v8    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :catchall_1
    move-exception v11

    monitor-exit v12
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v11

    :cond_1
    :try_start_7
    monitor-exit v12
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 2700
    :cond_2
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    long-to-float v12, v2

    iput v12, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->duration:F

    .line 2702
    :cond_3
    iget v0, v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->cumulativeCalories:F

    .line 2703
    .local v0, "calories":F
    const v11, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v11, v0, v11

    if-eqz v11, :cond_6

    .line 2704
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_calories_sum:F

    cmpl-float v11, v11, v0

    if-eqz v11, :cond_5

    .line 2705
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    .line 2706
    const/4 v11, 0x4

    float-to-int v12, v0

    int-to-float v12, v12

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->workoutProgress(IF)V

    .line 2707
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Ljava/util/ArrayList;

    move-result-object v12

    monitor-enter v12

    .line 2708
    :try_start_8
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 2710
    .restart local v8    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    const/4 v11, 0x4

    const/4 v13, 0x0

    :try_start_9
    invoke-interface {v8, v11, v0, v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateVaule(IFI)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto :goto_3

    .line 2712
    :catch_2
    move-exception v4

    .line 2713
    .restart local v4    # "e":Ljava/lang/Exception;
    :try_start_a
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v11

    const-string v13, "mFEDataListener - calories onUpdateVaule->Exception"

    invoke-static {v11, v13}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 2716
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v8    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :catchall_2
    move-exception v11

    monitor-exit v12
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    throw v11

    :cond_4
    :try_start_b
    monitor-exit v12
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 2718
    :cond_5
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iput v0, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_distance:F

    .line 2720
    :cond_6
    iget v1, v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->cumulativeDistance:F

    .line 2721
    .local v1, "distance":F
    const v11, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v11, v1, v11

    if-eqz v11, :cond_9

    .line 2722
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_distance:F

    cmpl-float v11, v11, v1

    if-eqz v11, :cond_8

    .line 2723
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    .line 2724
    const/4 v11, 0x2

    float-to-int v12, v1

    int-to-float v12, v12

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->workoutProgress(IF)V

    .line 2725
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Ljava/util/ArrayList;

    move-result-object v12

    monitor-enter v12

    .line 2726
    :try_start_c
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    .line 2728
    .restart local v8    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    const/4 v11, 0x2

    const/4 v13, 0x0

    :try_start_d
    invoke-interface {v8, v11, v1, v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateVaule(IFI)V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_3
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    goto :goto_4

    .line 2730
    :catch_3
    move-exception v4

    .line 2731
    .restart local v4    # "e":Ljava/lang/Exception;
    :try_start_e
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v11

    const-string v13, "mFEDataListener - distance onUpdateVaule->Exception"

    invoke-static {v11, v13}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 2734
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v8    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :catchall_3
    move-exception v11

    monitor-exit v12
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    throw v11

    :cond_7
    :try_start_f
    monitor-exit v12
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    .line 2736
    :cond_8
    const-string v11, "mFEDataListener"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v13, v13, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v13, v13, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_distance_sum:F

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 2737
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iput v1, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_distance_sum:F

    .line 2739
    :cond_9
    iget v9, v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->instantaneousSpeed:F

    .line 2740
    .local v9, "speed":F
    const v11, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v11, v9, v11

    if-eqz v11, :cond_b

    .line 2741
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_speed:F

    cmpl-float v11, v11, v9

    if-eqz v11, :cond_d

    .line 2742
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    .line 2743
    const/4 v11, 0x3

    float-to-int v12, v9

    int-to-float v12, v12

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->workoutProgress(IF)V

    .line 2744
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Ljava/util/ArrayList;

    move-result-object v12

    monitor-enter v12

    .line 2745
    :try_start_10
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_5
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_a

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_4

    .line 2747
    .restart local v8    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    const/4 v11, 0x3

    const/4 v13, 0x0

    :try_start_11
    invoke-interface {v8, v11, v9, v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onUpdateVaule(IFI)V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_4
    .catchall {:try_start_11 .. :try_end_11} :catchall_4

    goto :goto_5

    .line 2749
    :catch_4
    move-exception v4

    .line 2750
    .restart local v4    # "e":Ljava/lang/Exception;
    :try_start_12
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v11

    const-string v13, "mFEDataListener - speed onUpdateVaule->Exception"

    invoke-static {v11, v13}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 2753
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v8    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :catchall_4
    move-exception v11

    monitor-exit v12
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_4

    throw v11

    :cond_a
    :try_start_13
    monitor-exit v12
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_4

    .line 2758
    :goto_6
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    invoke-virtual {v11, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->setSpeed(F)V

    .line 2760
    :cond_b
    iget v6, v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->instantaneousHeartRate:I

    .line 2761
    .local v6, "hr":I
    const v11, 0x7fffffff

    if-eq v6, v11, :cond_c

    .line 2762
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v11, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setHRWorkoutResult(I)V

    .line 2765
    .end local v0    # "calories":F
    .end local v1    # "distance":F
    .end local v2    # "duration":J
    .end local v5    # "feData":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;
    .end local v6    # "hr":I
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v9    # "speed":F
    .end local v10    # "type":I
    :cond_c
    return-void

    .line 2755
    .restart local v0    # "calories":F
    .restart local v1    # "distance":F
    .restart local v2    # "duration":J
    .restart local v5    # "feData":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;
    .restart local v7    # "i$":Ljava/util/Iterator;
    .restart local v9    # "speed":F
    .restart local v10    # "type":I
    :cond_d
    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;

    .line 2756
    const/4 v11, 0x3

    float-to-int v12, v9

    int-to-float v12, v12

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->workoutProgress(IF)V

    goto :goto_6

    .line 2646
    nop

    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public onReceived(I[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;[Landroid/os/Bundle;)V
    .locals 0
    .param p1, "dataType"    # I
    .param p2, "data"    # [Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
    .param p3, "extra"    # [Landroid/os/Bundle;

    .prologue
    .line 2792
    return-void
.end method

.method public onStarted(II)V
    .locals 4
    .param p1, "dataType"    # I
    .param p2, "error"    # I

    .prologue
    .line 2770
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mFEDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$2200(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$18;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->record(J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_5

    .line 2784
    :goto_0
    return-void

    .line 2771
    :catch_0
    move-exception v0

    .line 2772
    .local v0, "e":Landroid/os/RemoteException;
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mFEDataListener - onStarted->RemoteException"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2773
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 2774
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mFEDataListener - onStarted->IllegalArgumentException"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2775
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 2776
    .local v0, "e":Ljava/lang/IllegalStateException;
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mFEDataListener - onStarted->IllegalStateException"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2777
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 2778
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mFEDataListener - onStarted->ShealthSensorServiceNotBoundException"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2779
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_4
    move-exception v0

    .line 2780
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mFEDataListener - onStarted->ShealthSensorNotSupportedException"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2781
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    :catch_5
    move-exception v0

    .line 2782
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mFEDataListener - onStarted->SHealthSensorInternalErrorException"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onStopped(II)V
    .locals 0
    .param p1, "dataType"    # I
    .param p2, "error"    # I

    .prologue
    .line 2788
    return-void
.end method
