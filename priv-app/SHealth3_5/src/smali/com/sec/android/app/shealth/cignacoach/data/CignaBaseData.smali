.class public Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
.super Ljava/lang/Object;
.source "CignaBaseData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

.field private mChecked:Z

.field private mCompleteDate:Ljava/util/Calendar;

.field private mExtraString:Ljava/lang/String;

.field private mIconDrawable:Landroid/graphics/drawable/Drawable;

.field private mId:I

.field private mMinFrequency:I

.field private mMissionToComplete:I

.field private mTitleString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 188
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mChecked:Z

    .line 28
    return-void
.end method

.method public constructor <init>(ILandroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "iconDrawable"    # Landroid/graphics/drawable/Drawable;
    .param p3, "titleString"    # Ljava/lang/String;
    .param p4, "extraString"    # Ljava/lang/String;
    .param p5, "categoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mChecked:Z

    .line 31
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mId:I

    .line 32
    iput-object p2, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mIconDrawable:Landroid/graphics/drawable/Drawable;

    .line 33
    iput-object p3, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mTitleString:Ljava/lang/String;

    .line 34
    iput-object p4, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mExtraString:Ljava/lang/String;

    .line 35
    iput-object p5, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mChecked:Z

    .line 39
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->readFromParcel(Landroid/os/Parcel;)V

    .line 40
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 168
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 118
    if-nez p1, :cond_1

    .line 163
    :cond_0
    :goto_0
    return v1

    .line 122
    :cond_1
    if-ne p0, p1, :cond_2

    move v1, v2

    .line 123
    goto :goto_0

    .line 125
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-ne v3, v4, :cond_0

    move-object v0, p1

    .line 126
    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;

    .line 128
    .local v0, "equalsObj":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mIconDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->getIconDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mIconDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v3, :cond_4

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->getIconDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-nez v3, :cond_0

    .line 132
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mIconDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_5

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->getIconDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 133
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mIconDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 134
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mIconDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->getIconDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 140
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mTitleString:Ljava/lang/String;

    if-eqz v3, :cond_6

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->getTitleString()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mTitleString:Ljava/lang/String;

    if-nez v3, :cond_7

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->getTitleString()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    .line 144
    :cond_7
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mTitleString:Ljava/lang/String;

    if-eqz v3, :cond_8

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->getTitleString()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 145
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mTitleString:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->getTitleString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 150
    :cond_8
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mExtraString:Ljava/lang/String;

    if-eqz v3, :cond_9

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->getExtraString()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_9
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mExtraString:Ljava/lang/String;

    if-nez v3, :cond_a

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->getExtraString()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    .line 154
    :cond_a
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mExtraString:Ljava/lang/String;

    if-eqz v3, :cond_b

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->getExtraString()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_b

    .line 155
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mExtraString:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->getExtraString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_b
    move v1, v2

    .line 160
    goto/16 :goto_0
.end method

.method public getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    return-object v0
.end method

.method public getCompleteDate()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mCompleteDate:Ljava/util/Calendar;

    return-object v0
.end method

.method public getExtraString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mExtraString:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 63
    const-string v0, ""

    .line 65
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mExtraString:Ljava/lang/String;

    goto :goto_0
.end method

.method public getIconDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mIconDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mId:I

    return v0
.end method

.method public getMinFrequency()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mMinFrequency:I

    return v0
.end method

.method public getMissionToComplete()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mMissionToComplete:I

    return v0
.end method

.method public getTitleString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mTitleString:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 52
    const-string v0, ""

    .line 54
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mTitleString:Ljava/lang/String;

    goto :goto_0
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mChecked:Z

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 181
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mId:I

    .line 182
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mTitleString:Ljava/lang/String;

    .line 183
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mExtraString:Ljava/lang/String;

    .line 184
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mCompleteDate:Ljava/util/Calendar;

    .line 185
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 186
    return-void
.end method

.method public setChecked(Z)V
    .locals 0
    .param p1, "checked"    # Z

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mChecked:Z

    .line 94
    return-void
.end method

.method public setCompleteDate(Ljava/util/Calendar;)V
    .locals 0
    .param p1, "completeDate"    # Ljava/util/Calendar;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mCompleteDate:Ljava/util/Calendar;

    .line 86
    return-void
.end method

.method public setExtraString(Ljava/lang/String;)V
    .locals 0
    .param p1, "extraString"    # Ljava/lang/String;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mExtraString:Ljava/lang/String;

    .line 70
    return-void
.end method

.method public setIconDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "iconDrawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mIconDrawable:Landroid/graphics/drawable/Drawable;

    .line 48
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 73
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mId:I

    .line 74
    return-void
.end method

.method public setMinFrequency(I)V
    .locals 0
    .param p1, "minFrequency"    # I

    .prologue
    .line 113
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mMinFrequency:I

    .line 114
    return-void
.end method

.method public setMissionToComplete(I)V
    .locals 0
    .param p1, "missionToComplete"    # I

    .prologue
    .line 105
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mMissionToComplete:I

    .line 106
    return-void
.end method

.method public setTitleString(Ljava/lang/String;)V
    .locals 0
    .param p1, "titleString"    # Ljava/lang/String;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mTitleString:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 173
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mTitleString:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mExtraString:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mCompleteDate:Ljava/util/Calendar;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 178
    return-void
.end method
