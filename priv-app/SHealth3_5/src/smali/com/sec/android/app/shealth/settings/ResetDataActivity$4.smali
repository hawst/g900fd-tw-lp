.class Lcom/sec/android/app/shealth/settings/ResetDataActivity$4;
.super Ljava/lang/Object;
.source "ResetDataActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/ResetDataActivity;->resetPreloadedPluginData(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)V
    .locals 0

    .prologue
    .line 243
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 246
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isRecording()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 247
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getInstance()Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getRealtimeService()Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v0

    .line 248
    .local v0, "realtimeService":Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->resetWorkout()V

    .line 250
    .end local v0    # "realtimeService":Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->clearExerciseData(Landroid/content/Context;)V

    .line 251
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->InitexerciseGoalData(Landroid/content/Context;)V

    .line 252
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->ClearRealtimeAllGoalType()V

    .line 253
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->ClearRealtimeAllGoalValue()V

    .line 254
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->clearRealTimeManualyMaxHR()V

    .line 255
    invoke-static {v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setNeedMaxHeartrateConfirm(Z)V

    .line 256
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->clearDisplayDataStatus()V

    .line 257
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setRealTimeAudioGuideIndex(I)V

    .line 258
    const/16 v1, 0x4653

    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveRealtimeActivityType(I)V

    .line 259
    invoke-static {v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setIsRealTimeMaxHRAutoUpdate(Z)V

    .line 260
    return-void
.end method
