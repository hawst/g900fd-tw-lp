.class Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$MultiCheckBoxDialogAdapter;
.super Landroid/widget/BaseAdapter;
.source "SHealthAlertDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MultiCheckBoxDialogAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)V
    .locals 0

    .prologue
    .line 1359
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$MultiCheckBoxDialogAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$1;

    .prologue
    .line 1359
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$MultiCheckBoxDialogAdapter;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 1363
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$MultiCheckBoxDialogAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->itemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->access$700(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1364
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$MultiCheckBoxDialogAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->itemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->access$700(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1366
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 1372
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 1378
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x1

    .line 1385
    if-nez p2, :cond_1

    .line 1387
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$MultiCheckBoxDialogAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1389
    .local v0, "inflater":Landroid/view/LayoutInflater;
    sget v2, Lcom/sec/android/app/shealth/common/commonui/R$layout;->multi_checkbox_dialog_item:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .end local p2    # "convertView":Landroid/view/View;
    check-cast p2, Landroid/widget/LinearLayout;

    .line 1392
    .restart local p2    # "convertView":Landroid/view/View;
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxViewHolder;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxViewHolder;-><init>()V

    .line 1394
    .local v1, "vh":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxViewHolder;
    sget v2, Lcom/sec/android/app/shealth/common/commonui/R$id;->multi_item_text:I

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxViewHolder;->textView:Landroid/widget/TextView;

    .line 1396
    sget v2, Lcom/sec/android/app/shealth/common/commonui/R$id;->multi_item_checkbox:I

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxViewHolder;->cb:Landroid/widget/CheckBox;

    .line 1398
    iget-object v2, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxViewHolder;->cb:Landroid/widget/CheckBox;

    sget v3, Lcom/sec/android/app/shealth/common/commonui/R$drawable;->checkbox_selector:I

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setBackgroundResource(I)V

    .line 1399
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1404
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$MultiCheckBoxDialogAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->itemList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->access$700(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxAlertItem;

    iget v2, v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxAlertItem;->checked:I

    if-ne v2, v4, :cond_2

    .line 1405
    iget-object v2, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxViewHolder;->cb:Landroid/widget/CheckBox;

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1409
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$MultiCheckBoxDialogAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->itemList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->access$700(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxAlertItem;

    iget-object v2, v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxAlertItem;->itemName:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$MultiCheckBoxDialogAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->itemList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->access$700(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxAlertItem;

    iget-object v2, v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxAlertItem;->itemName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 1411
    iget-object v3, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxViewHolder;->textView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$MultiCheckBoxDialogAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->itemList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->access$700(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxAlertItem;

    iget-object v2, v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxAlertItem;->itemName:Ljava/lang/String;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1412
    iget-object v3, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxViewHolder;->cb:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$MultiCheckBoxDialogAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->itemList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->access$700(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxAlertItem;

    iget-object v2, v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxAlertItem;->itemName:Ljava/lang/String;

    invoke-virtual {v3, v2}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 1416
    :cond_0
    iget-object v2, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxViewHolder;->cb:Landroid/widget/CheckBox;

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$MultiCheckBoxDialogAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v3, v3, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->checkedChangedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1418
    return-object p2

    .line 1401
    .end local v1    # "vh":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxViewHolder;
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxViewHolder;

    .restart local v1    # "vh":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxViewHolder;
    goto :goto_0

    .line 1407
    :cond_2
    iget-object v2, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxViewHolder;->cb:Landroid/widget/CheckBox;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1
.end method
