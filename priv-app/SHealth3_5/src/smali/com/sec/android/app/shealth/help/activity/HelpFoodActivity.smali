.class public Lcom/sec/android/app/shealth/help/activity/HelpFoodActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "HelpFoodActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 18
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 19
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/help/activity/HelpFoodActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f090fca

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 21
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 11
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 12
    const v0, 0x7f0301ff

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/help/activity/HelpFoodActivity;->setContentView(I)V

    .line 13
    return-void
.end method

.method protected onResume()V
    .locals 12

    .prologue
    const/16 v11, 0x8

    const/4 v10, 0x4

    const/4 v9, 0x0

    .line 26
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 27
    const v7, 0x7f0808f9

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/help/activity/HelpFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 28
    .local v1, "hideWithRTL1":Landroid/view/View;
    const v7, 0x7f0808fd

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/help/activity/HelpFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 29
    .local v2, "hideWithRTL2":Landroid/view/View;
    const v7, 0x7f080901

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/help/activity/HelpFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 30
    .local v3, "hideWithRTL3":Landroid/view/View;
    const v7, 0x7f0808fb

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/help/activity/HelpFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 31
    .local v4, "showWithRTL1":Landroid/view/View;
    const v7, 0x7f0808ff

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/help/activity/HelpFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 32
    .local v5, "showWithRTL2":Landroid/view/View;
    const v7, 0x7f080903

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/help/activity/HelpFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 34
    .local v6, "showWithRTL3":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/help/activity/HelpFoodActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 35
    .local v0, "config":Landroid/content/res/Configuration;
    invoke-virtual {v0}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_0

    .line 37
    invoke-virtual {v1, v10}, Landroid/view/View;->setVisibility(I)V

    .line 38
    invoke-virtual {v2, v10}, Landroid/view/View;->setVisibility(I)V

    .line 39
    invoke-virtual {v3, v10}, Landroid/view/View;->setVisibility(I)V

    .line 40
    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    .line 41
    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    .line 42
    invoke-virtual {v6, v9}, Landroid/view/View;->setVisibility(I)V

    .line 55
    :goto_0
    return-void

    .line 46
    :cond_0
    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    .line 47
    invoke-virtual {v2, v9}, Landroid/view/View;->setVisibility(I)V

    .line 48
    invoke-virtual {v3, v9}, Landroid/view/View;->setVisibility(I)V

    .line 49
    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    .line 50
    invoke-virtual {v5, v11}, Landroid/view/View;->setVisibility(I)V

    .line 51
    invoke-virtual {v6, v11}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
