.class Lcom/sec/android/app/shealth/common/commonui/input/InputModule$5;
.super Ljava/lang/Object;
.source "InputModule.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnValueChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->registerListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)V
    .locals 0

    .prologue
    .line 353
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$5;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onValueChanged(F)V
    .locals 4
    .param p1, "value"    # F

    .prologue
    .line 357
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$5;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mNeedToUpdateOtherView:Z

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$5;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mNeedToUpdateOtherView:Z

    .line 359
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$5;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setFloatValue(F)V

    .line 360
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$5;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mNeedToUpdateOtherView:Z

    .line 361
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$5;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mGradationView:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->access$700(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$5;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/common/commonui/R$string;->prompt_seek_control:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$5;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    iget-object v2, v2, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getFloatValue()Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$5;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/common/commonui/R$string;->prompt_drag_hold:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 365
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$5;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    # setter for: Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mCurrentValue:F
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->access$102(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;F)F

    .line 366
    return-void
.end method
