.class Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;
.super Ljava/lang/Object;
.source "AMapExerciseProMapActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field lastMeasuredTime:J

.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)V
    .locals 2

    .prologue
    .line 625
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 626
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;->lastMeasuredTime:J

    return-void
.end method


# virtual methods
.method public onActivityTypeChanged(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 733
    return-void
.end method

.method public onChangedWorkoutStatus(I)V
    .locals 2
    .param p1, "status"    # I

    .prologue
    .line 721
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10$6;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 728
    return-void
.end method

.method public onChannelStateChanged(ILjava/lang/String;)V
    .locals 2
    .param p1, "state"    # I
    .param p2, "uId"    # Ljava/lang/String;

    .prologue
    .line 644
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 650
    return-void
.end method

.method public onMaxDurationReached(J)V
    .locals 2
    .param p1, "second"    # J

    .prologue
    .line 708
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10$5;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 717
    return-void
.end method

.method public onNewLocation(Landroid/location/Location;)V
    .locals 14
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 654
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->gpsSignalLostBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$2000(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 655
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->gpsSignalLostDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$2100(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->gpsSignalLostDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$2100(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 656
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->gpsSignalLostDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$2100(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    .line 658
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->gpsSignalLostBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$2002(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 660
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->isInCenter:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->animationToCenterInProgress:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 661
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    iget-object v6, v6, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->mMapCenter:Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v4, v6, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    iget-object v6, v6, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->mMapCenter:Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v6, v6, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    invoke-static/range {v0 .. v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;->getDistanceFromLatLngInMeter(DDDD)D

    move-result-wide v2

    .line 662
    .local v2, "distanceFromCenter":D
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->mMap:Lcom/amap/api/maps2d/AMap;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Lcom/amap/api/maps2d/AMap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/maps2d/AMap;->getCameraPosition()Lcom/amap/api/maps2d/model/CameraPosition;

    move-result-object v0

    iget v0, v0, Lcom/amap/api/maps2d/model/CameraPosition;->zoom:F

    float-to-double v10, v0

    .line 664
    .local v10, "zoom":D
    const-wide/high16 v0, 0x3ffc000000000000L    # 1.75

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    const-wide/high16 v12, 0x4035000000000000L    # 21.0

    sub-double/2addr v12, v10

    invoke-static {v6, v7, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    mul-double v4, v0, v6

    .line 666
    .local v4, "thresholdDistance":D
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10$3;

    move-object v1, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10$3;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;DDLandroid/location/Location;)V

    invoke-virtual {v7, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 679
    .end local v2    # "distanceFromCenter":D
    .end local v4    # "thresholdDistance":D
    .end local v10    # "zoom":D
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10$4;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10$4;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;Landroid/location/Location;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 692
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 693
    .local v8, "currentTime":J
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;->lastMeasuredTime:J

    sub-long v0, v8, v0

    const-wide/16 v6, 0x7d0

    cmp-long v0, v0, v6

    if-ltz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$1500(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v0

    const/16 v1, 0x7d1

    if-ne v0, v1, :cond_4

    .line 694
    iput-wide v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;->lastMeasuredTime:J

    .line 696
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->mAMapListener:Lcom/amap/api/maps2d/LocationSource$OnLocationChangedListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$2400(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Lcom/amap/api/maps2d/LocationSource$OnLocationChangedListener;

    move-result-object v0

    if-eqz v0, :cond_3

    if-eqz p1, :cond_3

    .line 697
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->mAMapListener:Lcom/amap/api/maps2d/LocationSource$OnLocationChangedListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$2400(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Lcom/amap/api/maps2d/LocationSource$OnLocationChangedListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/maps2d/LocationSource$OnLocationChangedListener;->onLocationChanged(Landroid/location/Location;)V

    .line 700
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->playMapFromCacheDB(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$2200(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;Z)V

    .line 704
    :cond_4
    return-void
.end method

.method public onScreenLockMode(Z)V
    .locals 2
    .param p1, "locked"    # Z

    .prologue
    .line 751
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10$8;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10$8;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;Z)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 758
    return-void
.end method

.method public onShownUnlockProgress(Z)V
    .locals 0
    .param p1, "isShown"    # Z

    .prologue
    .line 768
    return-void
.end method

.method public onStartWorkOut()V
    .locals 0

    .prologue
    .line 763
    return-void
.end method

.method public onUpdateGoalFromWorkoutController(II)V
    .locals 0
    .param p1, "goalType"    # I
    .param p2, "value"    # I

    .prologue
    .line 773
    return-void
.end method

.method public onUpdateLapClock(J)V
    .locals 1
    .param p1, "second"    # J

    .prologue
    .line 639
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->lapClockDispUpdate(J)V
    invoke-static {v0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$1800(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;J)V

    .line 640
    return-void
.end method

.method public onUpdateVaule(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 629
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 635
    return-void
.end method

.method public onVisualGuideShow(Z)V
    .locals 2
    .param p1, "isShow"    # Z

    .prologue
    .line 737
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->isVisualGuideLayoutVisible:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$2700(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 738
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->isVisualGuideLayoutVisible:Z
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$2702(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;Z)Z

    .line 740
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10$7;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$10;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 747
    :cond_0
    return-void
.end method
