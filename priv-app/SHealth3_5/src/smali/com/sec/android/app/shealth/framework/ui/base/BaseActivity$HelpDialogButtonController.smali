.class Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$HelpDialogButtonController;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HelpDialogButtonController"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$HelpDialogButtonController;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$HelpDialogButtonController;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 42

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$4;->$SwitchMap$com$sec$android$app$shealth$common$commonui$dialog$DialogButtonType:[I

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    const/16 v1, 0x1f

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x5327

    aput v34, v2, v33

    const/16 v33, -0x33dd

    aput v33, v2, v32

    const/16 v32, -0x44

    aput v32, v2, v31

    const/16 v31, -0x8d6

    aput v31, v2, v30

    const/16 v30, -0x70

    aput v30, v2, v29

    const/16 v29, -0x14f1

    aput v29, v2, v28

    const/16 v28, -0x62

    aput v28, v2, v27

    const/16 v27, -0x29

    aput v27, v2, v26

    const/16 v26, 0x1703

    aput v26, v2, v25

    const/16 v25, 0x1876

    aput v25, v2, v24

    const/16 v24, 0x4e6b

    aput v24, v2, v23

    const/16 v23, -0x76a0

    aput v23, v2, v22

    const/16 v22, -0x7

    aput v22, v2, v21

    const/16 v21, -0x70

    aput v21, v2, v20

    const/16 v20, 0x1167

    aput v20, v2, v19

    const/16 v19, 0x263f

    aput v19, v2, v18

    const/16 v18, -0x29be

    aput v18, v2, v17

    const/16 v17, -0x41

    aput v17, v2, v16

    const/16 v16, -0x5f

    aput v16, v2, v15

    const/4 v15, -0x1

    aput v15, v2, v14

    const/16 v14, -0x49

    aput v14, v2, v13

    const/16 v13, -0x53

    aput v13, v2, v12

    const/16 v12, -0x11

    aput v12, v2, v11

    const/16 v11, -0x31

    aput v11, v2, v10

    const/16 v10, 0x628

    aput v10, v2, v9

    const/16 v9, -0x429d

    aput v9, v2, v8

    const/16 v8, -0x32

    aput v8, v2, v7

    const/16 v7, -0x4ba9

    aput v7, v2, v6

    const/16 v6, -0x27

    aput v6, v2, v5

    const/16 v5, -0x4097

    aput v5, v2, v3

    const/16 v3, -0x24

    aput v3, v2, v1

    const/16 v1, 0x1f

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x5354

    aput v35, v1, v34

    const/16 v34, -0x33ad

    aput v34, v1, v33

    const/16 v33, -0x34

    aput v33, v1, v32

    const/16 v32, -0x8b5

    aput v32, v1, v31

    const/16 v31, -0x9

    aput v31, v1, v30

    const/16 v30, -0x149f

    aput v30, v1, v29

    const/16 v29, -0x15

    aput v29, v1, v28

    const/16 v28, -0x5c

    aput v28, v1, v27

    const/16 v27, 0x176e

    aput v27, v1, v26

    const/16 v26, 0x1817

    aput v26, v1, v25

    const/16 v25, 0x4e18

    aput v25, v1, v24

    const/16 v24, -0x76b2

    aput v24, v1, v23

    const/16 v23, -0x77

    aput v23, v1, v22

    const/16 v22, -0x20

    aput v22, v1, v21

    const/16 v21, 0x1106

    aput v21, v1, v20

    const/16 v20, 0x2611

    aput v20, v1, v19

    const/16 v19, -0x29da

    aput v19, v1, v18

    const/16 v18, -0x2a

    aput v18, v1, v17

    const/16 v17, -0x32

    aput v17, v1, v16

    const/16 v16, -0x73

    aput v16, v1, v15

    const/16 v15, -0x2d

    aput v15, v1, v14

    const/16 v14, -0x3d

    aput v14, v1, v13

    const/16 v13, -0x72

    aput v13, v1, v12

    const/16 v12, -0x1f

    aput v12, v1, v11

    const/16 v11, 0x64b

    aput v11, v1, v10

    const/16 v10, -0x42fa

    aput v10, v1, v9

    const/16 v9, -0x43

    aput v9, v1, v8

    const/16 v8, -0x4b87

    aput v8, v1, v7

    const/16 v7, -0x4c

    aput v7, v1, v6

    const/16 v6, -0x40fa

    aput v6, v1, v5

    const/16 v5, -0x41

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    const/16 v1, 0x24

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, -0x58

    aput v40, v2, v39

    const/16 v39, 0x335f

    aput v39, v2, v38

    const/16 v38, -0x45ae

    aput v38, v2, v37

    const/16 v37, -0x9

    aput v37, v2, v36

    const/16 v36, -0x1d

    aput v36, v2, v35

    const/16 v35, -0x5f

    aput v35, v2, v34

    const/16 v34, -0x2e90

    aput v34, v2, v33

    const/16 v33, -0x5f

    aput v33, v2, v32

    const/16 v32, 0x7702

    aput v32, v2, v31

    const/16 v31, 0xc10

    aput v31, v2, v30

    const/16 v30, 0x2262

    aput v30, v2, v29

    const/16 v29, -0x4ba9

    aput v29, v2, v28

    const/16 v28, -0x39

    aput v28, v2, v27

    const/16 v27, -0x73

    aput v27, v2, v26

    const/16 v26, -0x22

    aput v26, v2, v25

    const/16 v25, -0x49

    aput v25, v2, v24

    const/16 v24, -0x5af4

    aput v24, v2, v23

    const/16 v23, -0x2b

    aput v23, v2, v22

    const/16 v22, -0x55

    aput v22, v2, v21

    const/16 v21, 0x4850

    aput v21, v2, v20

    const/16 v20, 0x5766

    aput v20, v2, v19

    const/16 v19, -0x49cd

    aput v19, v2, v18

    const/16 v18, -0x21

    aput v18, v2, v17

    const/16 v17, -0x25

    aput v17, v2, v16

    const/16 v16, -0x52

    aput v16, v2, v15

    const/16 v15, -0xfd1

    aput v15, v2, v14

    const/16 v14, -0x62

    aput v14, v2, v13

    const/16 v13, -0x22

    aput v13, v2, v12

    const/16 v12, 0x385a

    aput v12, v2, v11

    const/16 v11, -0x2ba5

    aput v11, v2, v10

    const/16 v10, -0x4f

    aput v10, v2, v9

    const/16 v9, -0x19

    aput v9, v2, v8

    const/16 v8, -0x57

    aput v8, v2, v7

    const/16 v7, -0x4392

    aput v7, v2, v6

    const/16 v6, -0x2d

    aput v6, v2, v3

    const/16 v3, 0x5c7d

    aput v3, v2, v1

    const/16 v1, 0x24

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, 0x1a

    const/16 v32, 0x1b

    const/16 v33, 0x1c

    const/16 v34, 0x1d

    const/16 v35, 0x1e

    const/16 v36, 0x1f

    const/16 v37, 0x20

    const/16 v38, 0x21

    const/16 v39, 0x22

    const/16 v40, 0x23

    const/16 v41, -0x3a

    aput v41, v1, v40

    const/16 v40, 0x3336

    aput v40, v1, v39

    const/16 v39, -0x45cd

    aput v39, v1, v38

    const/16 v38, -0x46

    aput v38, v1, v37

    const/16 v37, -0x33

    aput v37, v1, v36

    const/16 v36, -0x2e

    aput v36, v1, v35

    const/16 v35, -0x2f00

    aput v35, v1, v34

    const/16 v34, -0x2f

    aput v34, v1, v33

    const/16 v33, 0x7763

    aput v33, v1, v32

    const/16 v32, 0xc77

    aput v32, v1, v31

    const/16 v31, 0x220c

    aput v31, v1, v30

    const/16 v30, -0x4bde

    aput v30, v1, v29

    const/16 v29, -0x4c

    aput v29, v1, v28

    const/16 v28, -0x20

    aput v28, v1, v27

    const/16 v27, -0x41

    aput v27, v1, v26

    const/16 v26, -0x3c

    aput v26, v1, v25

    const/16 v25, -0x5ade

    aput v25, v1, v24

    const/16 v24, -0x5b

    aput v24, v1, v23

    const/16 v23, -0x25

    aput v23, v1, v22

    const/16 v22, 0x4831

    aput v22, v1, v21

    const/16 v21, 0x5748

    aput v21, v1, v20

    const/16 v20, -0x49a9

    aput v20, v1, v19

    const/16 v19, -0x4a

    aput v19, v1, v18

    const/16 v18, -0x4c

    aput v18, v1, v17

    const/16 v17, -0x24

    aput v17, v1, v16

    const/16 v16, -0xfb5

    aput v16, v1, v15

    const/16 v15, -0x10

    aput v15, v1, v14

    const/16 v14, -0x41

    aput v14, v1, v13

    const/16 v13, 0x3874

    aput v13, v1, v12

    const/16 v12, -0x2bc8

    aput v12, v1, v11

    const/16 v11, -0x2c

    aput v11, v1, v10

    const/16 v10, -0x6c

    aput v10, v1, v9

    const/16 v9, -0x79

    aput v9, v1, v8

    const/16 v8, -0x43fd

    aput v8, v1, v7

    const/16 v7, -0x44

    aput v7, v1, v6

    const/16 v6, 0x5c1e

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_3
    array-length v6, v1

    if-lt v3, v6, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_4
    array-length v6, v1

    if-lt v3, v6, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v5, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0x7910

    aput v13, v2, v12

    const/16 v12, 0x4315

    aput v12, v2, v11

    const/16 v11, 0x7022

    aput v11, v2, v10

    const/16 v10, 0x6713

    aput v10, v2, v9

    const/16 v9, 0x4b13

    aput v9, v2, v8

    const/16 v8, -0x3ed8

    aput v8, v2, v7

    const/16 v7, -0x5c

    aput v7, v2, v6

    const/16 v6, 0x451f

    aput v6, v2, v5

    const/16 v5, 0x312c

    aput v5, v2, v3

    const/16 v3, 0x6955

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0x797c

    aput v14, v1, v13

    const/16 v13, 0x4379

    aput v13, v1, v12

    const/16 v12, 0x7043

    aput v12, v1, v11

    const/16 v11, 0x6770

    aput v11, v1, v10

    const/16 v10, 0x4b67

    aput v10, v1, v9

    const/16 v9, -0x3eb5

    aput v9, v1, v8

    const/16 v8, -0x3f

    aput v8, v1, v7

    const/16 v7, 0x456d

    aput v7, v1, v6

    const/16 v6, 0x3145

    aput v6, v1, v5

    const/16 v5, 0x6931

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_6
    array-length v5, v1

    if-lt v3, v5, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0x6644

    aput v13, v2, v12

    const/16 v12, 0x2416

    aput v12, v2, v11

    const/16 v11, 0x555d

    aput v11, v2, v10

    const/16 v10, -0x71ff

    aput v10, v2, v9

    const/4 v9, -0x4

    aput v9, v2, v8

    const/16 v8, -0x2eb9

    aput v8, v2, v7

    const/16 v7, -0x43

    aput v7, v2, v6

    const/16 v6, 0x3233

    aput v6, v2, v5

    const/16 v5, -0x7cad

    aput v5, v2, v3

    const/16 v3, -0x20

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0x6621

    aput v14, v1, v13

    const/16 v13, 0x2466

    aput v13, v1, v12

    const/16 v12, 0x5524

    aput v12, v1, v11

    const/16 v11, -0x71ab

    aput v11, v1, v10

    const/16 v10, -0x72

    aput v10, v1, v9

    const/16 v9, -0x2ede

    aput v9, v1, v8

    const/16 v8, -0x2f

    aput v8, v1, v7

    const/16 v7, 0x325f

    aput v7, v1, v6

    const/16 v6, -0x7cce

    aput v6, v1, v5

    const/16 v5, -0x7d

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_8
    array-length v5, v1

    if-lt v3, v5, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v1, 0x4

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/16 v7, -0x2a

    aput v7, v2, v6

    const/16 v6, -0x20f7

    aput v6, v2, v5

    const/16 v5, -0x76

    aput v5, v2, v3

    const/16 v3, -0x3d

    aput v3, v2, v1

    const/4 v1, 0x4

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/16 v8, -0x6e

    aput v8, v1, v7

    const/16 v7, -0x20c0

    aput v7, v1, v6

    const/16 v6, -0x21

    aput v6, v1, v5

    const/16 v5, -0x7c

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_9
    array-length v5, v1

    if-lt v3, v5, :cond_8

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_a
    array-length v5, v1

    if-lt v3, v5, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    const/16 v1, 0x16

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x4a2c

    aput v26, v2, v25

    const/16 v25, 0x1423

    aput v25, v2, v24

    const/16 v24, 0x5073

    aput v24, v2, v23

    const/16 v23, -0x56db

    aput v23, v2, v22

    const/16 v22, -0x3b

    aput v22, v2, v21

    const/16 v21, -0x40

    aput v21, v2, v20

    const/16 v20, -0x1f

    aput v20, v2, v19

    const/16 v19, 0x2649

    aput v19, v2, v18

    const/16 v18, -0x6ebd

    aput v18, v2, v17

    const/16 v17, -0x7

    aput v17, v2, v16

    const/16 v16, 0x4f73

    aput v16, v2, v15

    const/16 v15, -0x12d8

    aput v15, v2, v14

    const/16 v14, -0x7d

    aput v14, v2, v13

    const/16 v13, -0x1a

    aput v13, v2, v12

    const/16 v12, -0xca8

    aput v12, v2, v11

    const/16 v11, -0x62

    aput v11, v2, v10

    const/16 v10, -0x48

    aput v10, v2, v9

    const/16 v9, -0xa

    aput v9, v2, v8

    const/16 v8, -0x52

    aput v8, v2, v7

    const/16 v7, 0x3b13

    aput v7, v2, v6

    const/16 v6, 0x1554

    aput v6, v2, v3

    const/16 v3, 0x5a76

    aput v3, v2, v1

    const/16 v1, 0x16

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x4a42

    aput v27, v1, v26

    const/16 v26, 0x144a

    aput v26, v1, v25

    const/16 v25, 0x5014

    aput v25, v1, v24

    const/16 v24, -0x56b0

    aput v24, v1, v23

    const/16 v23, -0x57

    aput v23, v1, v22

    const/16 v22, -0x50

    aput v22, v1, v21

    const/16 v21, -0x6f

    aput v21, v1, v20

    const/16 v20, 0x2625

    aput v20, v1, v19

    const/16 v19, -0x6eda

    aput v19, v1, v18

    const/16 v18, -0x6f

    aput v18, v1, v17

    const/16 v17, 0x4f5d

    aput v17, v1, v16

    const/16 v16, -0x12b1

    aput v16, v1, v15

    const/16 v15, -0x13

    aput v15, v1, v14

    const/16 v14, -0x6d

    aput v14, v1, v13

    const/16 v13, -0xcd5

    aput v13, v1, v12

    const/16 v12, -0xd

    aput v12, v1, v11

    const/16 v11, -0x27

    aput v11, v1, v10

    const/16 v10, -0x7b

    aput v10, v1, v9

    const/16 v9, -0x80

    aput v9, v1, v8

    const/16 v8, 0x3b7e

    aput v8, v1, v7

    const/16 v7, 0x153b

    aput v7, v1, v6

    const/16 v6, 0x5a15

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_b
    array-length v6, v1

    if-lt v3, v6, :cond_a

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_c
    array-length v6, v1

    if-lt v3, v6, :cond_b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const v1, 0x14000020

    invoke-virtual {v4, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$HelpDialogButtonController;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_0
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_1
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_2
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_3
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_4
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_5
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_6
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :cond_7
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_8

    :cond_8
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_9

    :cond_9
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_a

    :cond_a
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_b

    :cond_b
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_c

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
