.class Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$6;
.super Ljava/lang/Object;
.source "RestoreActivity.java"

# interfaces
.implements Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->cignaBackupRestore(Ljava/lang/String;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)V
    .locals 0

    .prologue
    .line 345
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFinished(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;ZLcom/cigna/coach/interfaces/IBackupAndRestore$BRError;Ljava/lang/String;)V
    .locals 6
    .param p1, "requestType"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;
    .param p2, "isSuccess"    # Z
    .param p3, "taskError"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;
    .param p4, "errorMessage"    # Ljava/lang/String;

    .prologue
    const-wide/16 v4, 0x64

    const/16 v3, 0x64

    .line 371
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "cignaBackupRestore restore on Finished"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    sget-object v0, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NONE:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    if-ne p3, v0, :cond_3

    .line 373
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "cignaBackupRestore Cigna Restore successfully completed"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setCignaRestoreTriggerPopup(Landroid/content/Context;Z)V

    .line 380
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$700(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$700(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 382
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/ProgressBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 383
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 386
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 387
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    const v3, 0x7f090d15

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 390
    :cond_1
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$6$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$6$1;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$6;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 402
    :cond_2
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$6$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$6$2;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$6;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 413
    return-void

    .line 376
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cignaBackupRestore Cigna Restore failed with error code : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setCignaRestoreTriggerPopup(Landroid/content/Context;Z)V

    goto/16 :goto_0
.end method

.method public onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V
    .locals 5
    .param p1, "requestType"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;
    .param p2, "fractionCompleted"    # D

    .prologue
    .line 357
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "cignaBackupRestore restore on progress"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/ProgressBar;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/TextView;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 360
    const-wide/high16 v1, 0x4014000000000000L    # 5.0

    mul-double/2addr v1, p2

    double-to-int v1, v1

    add-int/lit8 v0, v1, 0x5f

    .line 361
    .local v0, "percentage":I
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/ProgressBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 362
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    const v4, 0x7f090d15

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 365
    .end local v0    # "percentage":I
    :cond_0
    return-void
.end method

.method public onStarted(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;)V
    .locals 2
    .param p1, "requestType"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    .prologue
    .line 350
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "cignaBackupRestore restore started"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    return-void
.end method
