.class Lcom/sec/android/app/shealth/common/commonui/ListPopup$PopupListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ListPopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commonui/ListPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PopupListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/ListPopup;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/common/commonui/ListPopup;Landroid/content/Context;Ljava/util/List;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 236
    .local p3, "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    .line 237
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->getLayoutId()I

    move-result v0

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->getTextViewId()I

    move-result v1

    invoke-direct {p0, p2, v0, v1, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    .line 238
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v0, 0x1

    const/4 v8, 0x0

    .line 242
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 243
    .local v7, "view":Landroid/view/View;
    iget-object v9, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->getTextViewId()I

    move-result v9

    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 244
    .local v5, "textView":Landroid/widget/TextView;
    iget-object v9, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/ListPopup;->listener:Landroid/view/View$OnClickListener;
    invoke-static {v9}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->access$100(Lcom/sec/android/app/shealth/common/commonui/ListPopup;)Landroid/view/View$OnClickListener;

    move-result-object v9

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 245
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 246
    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 247
    invoke-virtual {v5}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout$LayoutParams;

    .line 248
    .local v6, "textViewLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v9, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->getListPopupItemHeightWithDivider()I

    move-result v9

    iget-object v10, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-virtual {v10}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->getDividerHeight()I

    move-result v10

    sub-int/2addr v9, v10

    iput v9, v6, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 249
    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 250
    iget-object v9, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/ListPopup;->selectedItemIndex:I
    invoke-static {v9}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->access$200(Lcom/sec/android/app/shealth/common/commonui/ListPopup;)I

    move-result v9

    if-ne p1, v9, :cond_1

    .line 251
    .local v0, "isCurrentItemSelected":Z
    :goto_0
    iget-object v9, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    iget-object v9, v9, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->context:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    if-eqz v0, :cond_2

    iget-object v9, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->getSelectedItemColorId()I

    move-result v9

    :goto_1
    invoke-virtual {v10, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 253
    iget-object v9, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mTextSelection:Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->access$300(Lcom/sec/android/app/shealth/common/commonui/ListPopup;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    iget-object v9, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mTextSelection:Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->access$300(Lcom/sec/android/app/shealth/common/commonui/ListPopup;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "\\s+"

    invoke-virtual {v9, v10}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 254
    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-interface {v9}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 255
    .local v1, "s":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mTextSelection:Ljava/lang/String;
    invoke-static {v10}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->access$300(Lcom/sec/android/app/shealth/common/commonui/ListPopup;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 256
    .local v3, "selectionStart":I
    iget-object v9, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mTextSelection:Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->access$300(Lcom/sec/android/app/shealth/common/commonui/ListPopup;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int v2, v3, v9

    .line 257
    .local v2, "selectionEnd":I
    if-ltz v3, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v9

    if-gt v2, v9, :cond_0

    .line 258
    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 259
    .local v4, "spannable":Landroid/text/SpannableStringBuilder;
    new-instance v9, Landroid/text/style/ForegroundColorSpan;

    iget-object v10, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    iget-object v10, v10, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->context:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mTextSelectionColorId:I
    invoke-static {v11}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->access$400(Lcom/sec/android/app/shealth/common/commonui/ListPopup;)I

    move-result v11

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    invoke-direct {v9, v10}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v10, 0x21

    invoke-virtual {v4, v9, v3, v2, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 261
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 264
    .end local v1    # "s":Ljava/lang/String;
    .end local v2    # "selectionEnd":I
    .end local v3    # "selectionStart":I
    .end local v4    # "spannable":Landroid/text/SpannableStringBuilder;
    :cond_0
    iget-object v9, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    iget-object v9, v9, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->context:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 268
    if-eqz v0, :cond_3

    .line 270
    invoke-virtual {v5}, Landroid/widget/TextView;->requestFocus()Z

    .line 271
    const-string/jumbo v9, "sans-serif"

    invoke-static {v9, v8}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 278
    :goto_2
    return-object v7

    .end local v0    # "isCurrentItemSelected":Z
    :cond_1
    move v0, v8

    .line 250
    goto/16 :goto_0

    .line 251
    .restart local v0    # "isCurrentItemSelected":Z
    :cond_2
    iget-object v9, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->getDefaultItemColorId()I

    move-result v9

    goto/16 :goto_1

    .line 276
    :cond_3
    const-string/jumbo v9, "sec-roboto-light"

    invoke-static {v9, v8}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_2
.end method
