.class public final enum Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;
.super Ljava/lang/Enum;
.source "ChartUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Operation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;

.field public static final enum AVG:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;

.field public static final enum CUMULATIVE:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;

.field public static final enum DEFAULT:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;

.field public static final enum SUM:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;->DEFAULT:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;

    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;

    const-string v1, "SUM"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;->SUM:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;

    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;

    const-string v1, "AVG"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;->AVG:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;

    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;

    const-string v1, "CUMULATIVE"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;->CUMULATIVE:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;

    .line 11
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;->DEFAULT:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;->SUM:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;->AVG:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;->CUMULATIVE:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;->$VALUES:[Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 11
    const-class v0, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;->$VALUES:[Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;

    return-object v0
.end method
