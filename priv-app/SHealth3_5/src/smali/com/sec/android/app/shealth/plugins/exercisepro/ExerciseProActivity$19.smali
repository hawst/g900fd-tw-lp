.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$19;
.super Ljava/lang/Object;
.source "ExerciseProActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->showInfomationDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V
    .locals 0

    .prologue
    .line 1680
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$19;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 5
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    .line 1684
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$19;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    const/4 v3, 0x0

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoDialog:Landroid/app/Dialog;
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->access$1602(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;Landroid/app/Dialog;)Landroid/app/Dialog;

    .line 1685
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1686
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "heartrate_warning_checked"

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$19;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mShowAgain:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->access$1700(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1687
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1688
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 1689
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x1

    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 1690
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$19;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$19;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    move-result-object v3

    const-string v4, "ExerciseProHRMFragment"

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->addTransparentFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;Ljava/lang/String;)V
    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->access$900(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;Ljava/lang/String;)V

    .line 1691
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$19;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mFragmentHandler:Landroid/os/Handler;

    const-wide/16 v3, 0x0

    invoke-virtual {v2, v1, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1692
    return-void
.end method
