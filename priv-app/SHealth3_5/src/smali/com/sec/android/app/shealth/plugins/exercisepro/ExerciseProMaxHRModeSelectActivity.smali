.class public Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "ExerciseProMaxHRModeSelectActivity.java"


# instance fields
.field private isAutoupdateMode:Z

.field private mAutomaticLayout:Landroid/widget/RelativeLayout;

.field mClickListener:Landroid/view/View$OnClickListener;

.field private mInputLayout:Landroid/view/View;

.field private mManuallyLayout:Landroid/widget/RelativeLayout;

.field private mMaxEditText:Landroid/widget/EditText;

.field private mMaxValue:I

.field resetPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private resetPopupShown:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 38
    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxValue:I

    .line 39
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->resetPopupShown:Z

    .line 248
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$5;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->checkChangesAndExit()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->checkDataAndSave()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxValue:I

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;
    .param p1, "x1"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxValue:I

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->showMaxHrWarningPopup()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;ZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->setModeLayout(ZZ)V

    return-void
.end method

.method static synthetic access$602(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->resetPopupShown:Z

    return p1
.end method

.method private checkChangesAndExit()V
    .locals 3

    .prologue
    .line 287
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 288
    .local v0, "value":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 289
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxValue:I

    .line 292
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->isAutoupdateMode:Z

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isRealTimeMaxHRAutoUpdate()Z

    move-result v2

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxValue:I

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealTimeManualyMaxHR()I

    move-result v2

    if-eq v1, v2, :cond_2

    .line 294
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->showResetPopup()V

    .line 299
    :goto_1
    return-void

    .line 291
    :cond_1
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxValue:I

    goto :goto_0

    .line 298
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->finish()V

    goto :goto_1
.end method

.method private checkDataAndSave()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 302
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 303
    .local v0, "value":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 304
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxValue:I

    .line 307
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->isAutoupdateMode:Z

    if-nez v1, :cond_3

    .line 308
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxValue:I

    const/16 v2, 0xf0

    if-gt v1, v2, :cond_0

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxValue:I

    const/16 v2, 0x64

    if-ge v1, v2, :cond_2

    .line 309
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->showMaxHrWarningPopup()V

    .line 320
    :goto_1
    return-void

    .line 306
    :cond_1
    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxValue:I

    goto :goto_0

    .line 312
    :cond_2
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxValue:I

    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setRealTimeManualyMaxHR(I)V

    .line 314
    :cond_3
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->isAutoupdateMode:Z

    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setIsRealTimeMaxHRAutoUpdate(Z)V

    .line 315
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isNeedMaxHeartrateConfirm()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 316
    invoke-static {v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setNeedMaxHeartrateConfirm(Z)V

    .line 318
    :cond_4
    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->setResult(I)V

    .line 319
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->finish()V

    goto :goto_1
.end method

.method private initLayout()V
    .locals 6

    .prologue
    const v5, 0x7f080779

    const v3, 0x7f0802dc

    const/4 v4, 0x1

    .line 142
    const v1, 0x7f080804

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mAutomaticLayout:Landroid/widget/RelativeLayout;

    .line 143
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mAutomaticLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f090adc

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 144
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mAutomaticLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f090ade

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 146
    const v1, 0x7f080805

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mManuallyLayout:Landroid/widget/RelativeLayout;

    .line 147
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mManuallyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f090685

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 148
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mManuallyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f090add

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 150
    const v1, 0x7f080806

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mInputLayout:Landroid/view/View;

    .line 151
    const v1, 0x7f080807

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxEditText:Landroid/widget/EditText;

    .line 152
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxEditText:Landroid/widget/EditText;

    const-string v2, "inputType=YearDateTime_edittext"

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 153
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxEditText:Landroid/widget/EditText;

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 177
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxEditText:Landroid/widget/EditText;

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$3;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 203
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxEditText:Landroid/widget/EditText;

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$4;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 225
    new-array v0, v4, [Landroid/text/InputFilter;

    .line 226
    .local v0, "fArray":[Landroid/text/InputFilter;
    const/4 v1, 0x0

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v0, v1

    .line 227
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxEditText:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 228
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxEditText:Landroid/widget/EditText;

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxValue:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 243
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mAutomaticLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 244
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mManuallyLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 245
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->isAutoupdateMode:Z

    invoke-direct {p0, v1, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->setModeLayout(ZZ)V

    .line 246
    return-void
.end method

.method private setModeLayout(ZZ)V
    .locals 6
    .param p1, "isAutoMode"    # Z
    .param p2, "isInit"    # Z

    .prologue
    const v5, 0x7f08077a

    const v4, 0x7f080778

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 265
    if-nez p2, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->isAutoupdateMode:Z

    if-ne v1, p1, :cond_0

    .line 284
    :goto_0
    return-void

    .line 267
    :cond_0
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->isAutoupdateMode:Z

    .line 268
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->isAutoupdateMode:Z

    if-eqz v1, :cond_2

    .line 269
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mAutomaticLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 270
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mManuallyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 271
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mInputLayout:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 272
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxEditText:Landroid/widget/EditText;

    invoke-static {p0, v1}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->hideKeyboard(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 283
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->refreshFocusables()V

    goto :goto_0

    .line 274
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mAutomaticLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 275
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mManuallyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 276
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mInputLayout:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 277
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 278
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 279
    .local v0, "config":Landroid/content/res/Configuration;
    iget v1, v0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-eq v1, v3, :cond_1

    .line 280
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxEditText:Landroid/widget/EditText;

    invoke-static {p0, v1}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->showKeyboard(Landroid/content/Context;Landroid/widget/EditText;)V

    goto :goto_1
.end method

.method private showMaxHrWarningPopup()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 346
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v1, p0, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 348
    .local v1, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f090ae3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f090f56

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/16 v5, 0x64

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    const/4 v5, 0x1

    const/16 v6, 0xf0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 349
    .local v0, "alertText":Ljava/lang/String;
    const v2, 0x7f090ae2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v2

    const/high16 v3, 0x20000

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->addFlags(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$8;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$8;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 367
    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$9;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$9;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 375
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "MaxHr"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 376
    return-void
.end method

.method private showResetPopup()V
    .locals 4

    .prologue
    .line 323
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->resetPopupShown:Z

    .line 324
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 326
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const v1, 0x7f090f4d

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090081

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090f4e

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$7;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$7;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$6;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$6;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x20000

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->addFlags(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 341
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->resetPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 342
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->resetPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "check"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 343
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 62
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 64
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    const v4, 0x7f090a23

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 67
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)V

    .line 83
    .local v0, "actionBarButtonListener":Landroid/view/View$OnClickListener;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setHomeButtonVisible(Z)V

    .line 85
    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v3, 0x7f090048

    invoke-direct {v2, v5, v3, v0, v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;Z)V

    .line 86
    .local v2, "actionButton3":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    new-array v4, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v2, v4, v5

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 88
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v3, 0x7f090044

    invoke-direct {v1, v5, v3, v0, v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;Z)V

    .line 89
    .local v1, "actionButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    new-array v4, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v1, v4, v5

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 90
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->refreshFocusables()V

    .line 91
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 120
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onBackPressed()V

    .line 121
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 44
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    const v0, 0x7f0301d0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->setContentView(I)V

    .line 46
    if-eqz p1, :cond_1

    .line 47
    const-string v0, "key_auto_mode"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->isAutoupdateMode:Z

    .line 48
    const-string v0, "key_max_value"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxValue:I

    .line 49
    const-string/jumbo v0, "resetPopupShown"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->resetPopupShown:Z

    .line 54
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->initLayout()V

    .line 55
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->resetPopupShown:Z

    if-eqz v0, :cond_0

    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->showResetPopup()V

    .line 58
    :cond_0
    return-void

    .line 51
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isRealTimeMaxHRAutoUpdate()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->isAutoupdateMode:Z

    .line 52
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealTimeManualyMaxHR()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxValue:I

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 114
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    .line 115
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 96
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPause()V

    .line 98
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 138
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 139
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 103
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 107
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->resetPopupShown:Z

    if-eqz v0, :cond_0

    .line 108
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->showResetPopup()V

    .line 111
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 126
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->resetPopupShown:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->resetPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->resetPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    .line 129
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 130
    const-string v0, "key_auto_mode"

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->isAutoupdateMode:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 131
    const-string v0, "key_max_value"

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxValue:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 132
    const-string/jumbo v0, "resetPopupShown"

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->resetPopupShown:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 133
    return-void
.end method
