.class Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11$1;
.super Ljava/lang/Object;
.source "DataMigrationHelper.java"

# interfaces
.implements Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11;)V
    .locals 0

    .prologue
    .line 547
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancelled()V
    .locals 2

    .prologue
    .line 566
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "migrateLocalDataFrom2xto25 - onCancelled - It should not be occurred"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 567
    return-void
.end method

.method public onFinished(Ljava/lang/Integer;)V
    .locals 4
    .param p1, "error"    # Ljava/lang/Integer;

    .prologue
    .line 550
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "migrateLocalDataFrom2xto25 - onFinished - error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 551
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 552
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mCurrentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$402(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;)Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 562
    :goto_0
    return-void

    .line 555
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mPopupHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$800(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11$1$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11$1$1;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11$1;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public onProgressUpdate(Ljava/lang/Integer;)V
    .locals 3
    .param p1, "progress"    # Ljava/lang/Integer;

    .prologue
    .line 571
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "migrateLocalDataFrom2xto25 - onProgressUpdate: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 572
    return-void
.end method
