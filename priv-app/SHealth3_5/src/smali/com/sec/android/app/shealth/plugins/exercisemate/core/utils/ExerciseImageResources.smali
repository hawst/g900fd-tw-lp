.class public Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;
.super Ljava/lang/Object;
.source "ExerciseImageResources.java"


# static fields
.field public static final RESOURCES:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final RESOURCES_XL:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const v7, 0x7f02035e

    const v6, 0x7f020336

    const v5, 0x7f020373

    const v4, 0x7f020375

    const v3, 0x7f020364

    .line 14
    new-instance v0, Ljava/util/TreeMap;

    sget-object v1, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    .line 16
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Pedometer"

    const v2, 0x7f020359

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Walking"

    const v2, 0x7f02037c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Slow Walking"

    const v2, 0x7f02036c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Normal Walking"

    const v2, 0x7f02037c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Fast Walking"

    const v2, 0x7f020342

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Walking upstairs"

    const v2, 0x7f02037e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Walking downstairs"

    const v2, 0x7f02037d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Running"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Running, General"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Running, 4 mph"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Running, 5 mph"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Running, 6 mph"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Running, 7 mph"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Running, 8 mph"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Running, 9 mph"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Running, 10 mph"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Running, 11 mph"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Running, 12 mph"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Running, 13 mph"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Running, 14 mph"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "General"

    const v2, 0x7f02037c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Hiking"

    const v2, 0x7f020348

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Ballet"

    const v2, 0x7f02032c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Dancing"

    const v2, 0x7f02033d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Inline skating, slow"

    const v2, 0x7f02034f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Inline skating, normal"

    const v2, 0x7f02034f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Inline skating, fast"

    const v2, 0x7f02034f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Inline skating"

    const v2, 0x7f02034f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Stretching, mild"

    const v2, 0x7f02037a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Pilates"

    const v2, 0x7f02035a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Yoga"

    const v2, 0x7f020381

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Aerobic, Low impact"

    const v2, 0x7f020328

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Aerobic, HighImpact"

    const v2, 0x7f020328

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Skipping (120~160/min)"

    const v2, 0x7f020351

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Skipping (100~120/min)"

    const v2, 0x7f020351

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Skipping (~100/min)"

    const v2, 0x7f020351

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Hula-hooping"

    const v2, 0x7f02034a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Rock climbing, High Difficulty"

    const v2, 0x7f020360

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Rock climbing, Low to Moderate Difficulty"

    const v2, 0x7f020360

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Tai chi, General"

    const v2, 0x7f020378

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Tai chi, Light Effort"

    const v2, 0x7f020378

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Hangliding"

    const v2, 0x7f020347

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Pistol shooting"

    const v2, 0x7f02035b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Archery, non-hunting "

    const v2, 0x7f02032a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Field hockey "

    const v2, 0x7f020343

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Ice hockey, general"

    const v2, 0x7f02034c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Hockey, ice, competitive"

    const v2, 0x7f02034c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Rugby, union, team, competitive"

    const v2, 0x7f020363

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Rugby, touch, non-competitive"

    const v2, 0x7f020363

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Golf"

    const v2, 0x7f020345

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Basketball, general"

    const v2, 0x7f020334

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Basketball, game"

    const v2, 0x7f020334

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Basketball"

    const v2, 0x7f020334

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Squash, general"

    const v2, 0x7f020371

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Ballroom dance, fast"

    const v2, 0x7f02032d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Ballroom dance, slow"

    const v2, 0x7f02032d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Baseball"

    const v2, 0x7f020333

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Soccer, competitive"

    const v2, 0x7f02036f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Soccer, casual"

    const v2, 0x7f02036f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Football"

    const v2, 0x7f02036f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Tennis, general"

    const v2, 0x7f020379

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Tennis, singles"

    const v2, 0x7f020379

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Tennis, doubles"

    const v2, 0x7f020379

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Tennis"

    const v2, 0x7f020379

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Badminton, competitive"

    const v2, 0x7f02032b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Badminton, social singles and doubles"

    const v2, 0x7f02032b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Badminton"

    const v2, 0x7f02032b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Billiard"

    const v2, 0x7f020337

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Boxing"

    const v2, 0x7f020339

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Boxing, in ring"

    const v2, 0x7f020339

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Boxing, punching bag"

    const v2, 0x7f020339

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Boxing, sparring"

    const v2, 0x7f020339

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Handball, general"

    const v2, 0x7f020346

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Handball, team"

    const v2, 0x7f020346

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Handball"

    const v2, 0x7f020346

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Horseback riding "

    const v2, 0x7f020349

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Martial arts, moderate pace"

    const v2, 0x7f020377

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Martial arts, slower pace"

    const v2, 0x7f020377

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Softball, general "

    const v2, 0x7f020370

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Softball, practice\u00a0"

    const v2, 0x7f020370

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Softball, pitching"

    const v2, 0x7f020370

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Table tennis"

    const v2, 0x7f020376

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Volleyball, competitive"

    const v2, 0x7f02037b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Volleyball, general"

    const v2, 0x7f02037b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Step machine"

    const v2, 0x7f020374

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Weight machine"

    const v2, 0x7f020380

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Bowling"

    const v2, 0x7f020338

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "WaterActivities"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Water Activities"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Water-Skiing"

    const v2, 0x7f02037f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Water Skiing"

    const v2, 0x7f02037f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Water-skiing"

    const v2, 0x7f02037f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Free-style Swimming, Fast"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Free-style Swimming, Slow"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Backstroke, Training"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Backstroke, Recreational"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Breaststroke, Training"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Breaststroke, Recreational"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Butterfly Stroke Swimming"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Aquarobics"

    const v2, 0x7f020329

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Canoeing, rowing"

    const v2, 0x7f02033a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Sailing, yachting, for leisure"

    const v2, 0x7f020365

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Skindiving, fast"

    const v2, 0x7f02036b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Skindiving, moderate"

    const v2, 0x7f02036b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Skindiving, scuba diving, general, rowing"

    const v2, 0x7f02036b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Snorkeling, for leisure"

    const v2, 0x7f02036b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Cycling"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Bicycling"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Bicycling, Slow"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Bicycling, Moderate"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Bicycling, Fast"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Bicycling, Very Fast"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Stationary bicycle, very light to light effort (30-50 watts)"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Stationary bicycle, moderate to vigorous effort (90-100 watts)"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Stationary bicycle, vigorous effort (101-160 watts)"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Stationary bicycle, vigorous effort"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Stationary bicycle, 201-270 watts, very vigorous effort"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Stationary bicycle, 51-89 watts, light-to-moderate effort"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Winter Activities"

    const v2, 0x7f02036a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Skiing/snowboarding, light"

    const v2, 0x7f02036a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Skiing/snowboarding, moderate"

    const v2, 0x7f02036a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Skiing/snowboarding, vigorous"

    const v2, 0x7f02036a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Skiing"

    const v2, 0x7f02036a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Ice dancing"

    const v2, 0x7f02034b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Ice skating"

    const v2, 0x7f020369

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Ice skating, slow"

    const v2, 0x7f020369

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Ice skating, general"

    const v2, 0x7f020369

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Ice skating, rapidly"

    const v2, 0x7f020369

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Speed skating, competitive"

    const v2, 0x7f020369

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Strength Workout"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Strength"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Strength Workouts"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Push-up"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Pull-up"

    const v2, 0x7f02033b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Sit-up"

    const v2, 0x7f020368

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Dumbbell benchpress"

    const v2, 0x7f02033e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Barbell benchpress"

    const v2, 0x7f02032e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Flat bench dumbbell flye"

    const v2, 0x7f020344

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Incline barbell press"

    const v2, 0x7f02034d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Dumbbell lateral raise"

    const v2, 0x7f020340

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Smith machine upright row"

    const v2, 0x7f02036d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Dumbbell overhead press"

    const v2, 0x7f020341

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Barbell bent-over row"

    const v2, 0x7f02032f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Barbell upright row"

    const v2, 0x7f020332

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "One-arm dumbbell row"

    const v2, 0x7f020357

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Lat pulldown"

    const v2, 0x7f020352

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Barbell curl"

    const v2, 0x7f020330

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Preacher curl machine"

    const v2, 0x7f02035d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Incline dumbbell curl"

    const v2, 0x7f02034e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Lying barbell extension"

    const v2, 0x7f020355

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Rope pressdown"

    const v2, 0x7f020362

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Dumbbell kickback"

    const v2, 0x7f02033f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Leg extension"

    const v2, 0x7f020353

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Barbell squat"

    const v2, 0x7f020331

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Leg press"

    const v2, 0x7f020354

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Seated leg curl"

    const v2, 0x7f020367

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Romanian deadlift"

    const v2, 0x7f020361

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Lying leg curl"

    const v2, 0x7f020356

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Seated calf raise"

    const v2, 0x7f020366

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Standing calf raise"

    const v2, 0x7f020372

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Crunch"

    const v2, 0x7f02033c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Reverse crunch"

    const v2, 0x7f02035f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Aerobics"

    const v2, 0x7f020328

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Aerobic"

    const v2, 0x7f020328

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Snowboarding"

    const v2, 0x7f02036e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Others"

    const v2, 0x7f020358

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Pull-up ( Chin-ups )"

    const v2, 0x7f02033b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Horseback riding"

    const v2, 0x7f020349

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Ballroom Dance, Slow"

    const v2, 0x7f02033d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Dance sports"

    const v2, 0x7f02033d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Martial arts, Moderate pace"

    const v2, 0x7f020350

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Sit-up ( Sit-ups )"

    const v2, 0x7f020368

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Push-up ( Press-ups )"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Skipping Rope"

    const v2, 0x7f020351

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Power walking"

    const v2, 0x7f02035c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Walking Downstairs"

    const v2, 0x7f02037d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Martial arts, Slower pace"

    const v2, 0x7f020377

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "T\'ai chi"

    const v2, 0x7f020378

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Tai chi"

    const v2, 0x7f020378

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Walking Upstairs"

    const v2, 0x7f02037e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Squash"

    const v2, 0x7f020371

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Billiards"

    const v2, 0x7f020337

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Hang gliding"

    const v2, 0x7f020347

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Exercise bike"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Swimming"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Rock climbing"

    const v2, 0x7f020360

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Skin/scuba diving"

    const v2, 0x7f02036b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Softball"

    const v2, 0x7f020370

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Volleyball"

    const v2, 0x7f02037b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    new-instance v0, Ljava/util/TreeMap;

    sget-object v1, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    .line 224
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Aerobics"

    const v2, 0x7f0202e0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Aerobic"

    const v2, 0x7f0202e0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Aquarobics"

    const v2, 0x7f0202e1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Badminton"

    const v2, 0x7f0202e2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Baseball"

    const v2, 0x7f0202e3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Basketball"

    const v2, 0x7f0202e4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Cycling"

    const v2, 0x7f0202e5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Billiards"

    const v2, 0x7f0202e6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Bowling"

    const v2, 0x7f0202e7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Boxing"

    const v2, 0x7f0202e8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Chin-ups"

    const v2, 0x7f0202e9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Dancing"

    const v2, 0x7f0202ea

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Fast walking"

    const v2, 0x7f0202ec

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Golf"

    const v2, 0x7f0202ed

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Handball"

    const v2, 0x7f0202ee

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Hang gliding"

    const v2, 0x7f0202ef

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Hiking"

    const v2, 0x7f0202f0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Horse riding"

    const v2, 0x7f0202f1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Hula-hooping"

    const v2, 0x7f0202f2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Inline skating"

    const v2, 0x7f0202f3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Judo"

    const v2, 0x7f0202f4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Skipping (with rope)"

    const v2, 0x7f0202f5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Power walking"

    const v2, 0x7f0202f6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Press-ups"

    const v2, 0x7f0202f7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Rock climbing"

    const v2, 0x7f0202f8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Running"

    const v2, 0x7f0202f9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Sit-ups"

    const v2, 0x7f0202fa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Ice skating"

    const v2, 0x7f0202fb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Skiing"

    const v2, 0x7f0202fc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Skin/scuba diving"

    const v2, 0x7f0202fd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Football"

    const v2, 0x7f0202fe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Softball"

    const v2, 0x7f0202ff

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Sports dance"

    const v2, 0x7f020300

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Squash"

    const v2, 0x7f020301

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Exercise bike"

    const v2, 0x7f020302

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Step machine"

    const v2, 0x7f020303

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Swimming"

    const v2, 0x7f020304

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "T\'ai chi"

    const v2, 0x7f020307

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 262
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Table tennis"

    const v2, 0x7f020305

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Taekwondo"

    const v2, 0x7f020306

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 264
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Tennis"

    const v2, 0x7f020308

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Volleyball"

    const v2, 0x7f020309

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Walking"

    const v2, 0x7f02030a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Walking downstairs"

    const v2, 0x7f02030b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Walking upstairs"

    const v2, 0x7f02030c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Water-skiing"

    const v2, 0x7f02030d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Weight machine"

    const v2, 0x7f02030e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Yoga"

    const v2, 0x7f02030f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Default"

    const v2, 0x7f0202eb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "General"

    const v2, 0x7f0202eb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Treadmill"

    const v2, 0x7f0203f0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Elliptical"

    const v2, 0x7f0203ed

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Bike"

    const v2, 0x7f0203eb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Rower"

    const v2, 0x7f0203ef

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Climber"

    const v2, 0x7f0203ec

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES_XL:Ljava/util/TreeMap;

    const-string v1, "Nordic skier"

    const v2, 0x7f0203ee

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
