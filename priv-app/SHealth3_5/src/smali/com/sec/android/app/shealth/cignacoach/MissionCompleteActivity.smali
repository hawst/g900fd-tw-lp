.class public Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;
.super Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;
.source "MissionCompleteActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity$3;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCompleteHeaderView:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;

.field private mCompleteInfoData:Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;

.field private mFinishDelayHandler:Landroid/os/Handler;

.field private mGoalId:I

.field private mLeftBtn:Landroid/widget/Button;

.field private mMissionFrequency:I

.field private mMissionId:I

.field private mMissionProgressFrequency:I

.field private mMissionProgressStatus:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

.field private mMissionTitle:Ljava/lang/String;

.field private mOkBtn:Landroid/widget/Button;

.field private mOneBtnArea:Landroid/widget/RelativeLayout;

.field private mRightBtn:Landroid/widget/Button;

.field private mRunnable:Ljava/lang/Runnable;

.field private mTwoBtnArea:Landroid/widget/RelativeLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;-><init>()V

    .line 42
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mFinishDelayHandler:Landroid/os/Handler;

    .line 211
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->prepareShareView()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mFinishDelayHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private initLayout()V
    .locals 1

    .prologue
    .line 82
    const v0, 0x7f08020f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mCompleteHeaderView:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;

    .line 84
    const v0, 0x7f080210

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mOneBtnArea:Landroid/widget/RelativeLayout;

    .line 85
    const v0, 0x7f080211

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mOkBtn:Landroid/widget/Button;

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mOkBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    const v0, 0x7f080212

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mTwoBtnArea:Landroid/widget/RelativeLayout;

    .line 90
    const v0, 0x7f080213

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mLeftBtn:Landroid/widget/Button;

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mLeftBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    const v0, 0x7f080214

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mRightBtn:Landroid/widget/Button;

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mRightBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    return-void
.end method

.method private updateButton()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mMissionProgressStatus:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    sget-object v1, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->PROGRESSED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    if-ne v0, v1, :cond_0

    .line 101
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->TAG:Ljava/lang/String;

    const-string v1, "PROGRESSED"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mOneBtnArea:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mTwoBtnArea:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 111
    :goto_0
    return-void

    .line 105
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->TAG:Ljava/lang/String;

    const-string v1, "COMPLETE"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mOneBtnArea:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mTwoBtnArea:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mLeftBtn:Landroid/widget/Button;

    const v1, 0x7f090c7b

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mRightBtn:Landroid/widget/Button;

    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto :goto_0
.end method

.method private updateCompleteHeaderView()V
    .locals 7

    .prologue
    .line 114
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mCompleteHeaderView:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;

    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;->MISSION:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->setCompleteType(Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;)V

    .line 116
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity$3;->$SwitchMap$com$cigna$coach$interfaces$IGoalsMissions$MissionProgressStatus:[I

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mMissionProgressStatus:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    invoke-virtual {v2}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 135
    :goto_0
    return-void

    .line 118
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    const v2, 0x7f090c79

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 119
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mCompleteHeaderView:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09032a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->setTitle(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mCompleteHeaderView:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mMissionTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->setSubTitle(Ljava/lang/CharSequence;)V

    .line 121
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mCompleteHeaderView:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0902f1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->setExclamations(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 125
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    const v2, 0x7f090c78

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 126
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mCompleteHeaderView:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09033c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->setTitle(Ljava/lang/CharSequence;)V

    .line 127
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mCompleteHeaderView:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mMissionTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->setSubTitle(Ljava/lang/CharSequence;)V

    .line 128
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090305

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mMissionProgressFrequency:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mMissionFrequency:I

    iget v6, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mMissionProgressFrequency:I

    sub-int/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 129
    .local v0, "progressMessage":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mCompleteHeaderView:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->setProgressMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 116
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 0

    .prologue
    .line 177
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->customizeActionBar()V

    .line 179
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 195
    const/4 v0, 0x0

    .line 196
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 208
    :goto_0
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->finish()V

    .line 209
    return-void

    .line 198
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    const-class v1, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 199
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "EXTRA_NAME_GOAL_ID"

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mGoalId:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 200
    const-string v1, "EXTRA_NAME_MISSION_FROM_WHERE"

    const/16 v2, 0x16

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 201
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 205
    :pswitch_2
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090835

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 196
    nop

    :pswitch_data_0
    .packed-switch 0x7f080211
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 47
    const v2, 0x7f03006f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->setContentView(I)V

    .line 49
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 51
    .local v1, "intent":Landroid/content/Intent;
    :try_start_0
    const-string v2, "intent_complete_info_data"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mCompleteInfoData:Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 58
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mCompleteInfoData:Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;

    if-nez v2, :cond_0

    .line 59
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->TAG:Ljava/lang/String;

    const-string v3, "Because complete info data is null, finish ~"

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->finish()V

    .line 79
    :goto_1
    return-void

    .line 52
    :catch_0
    move-exception v0

    .line 53
    .local v0, "e":Ljava/lang/ClassCastException;
    invoke-virtual {v0}, Ljava/lang/ClassCastException;->printStackTrace()V

    goto :goto_0

    .line 54
    .end local v0    # "e":Ljava/lang/ClassCastException;
    :catch_1
    move-exception v0

    .line 55
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 64
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const-string v2, "intent_complete_info_data"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mCompleteInfoData:Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;

    .line 66
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mCompleteInfoData:Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->getGoalId()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mGoalId:I

    .line 67
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mCompleteInfoData:Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->getMissionId()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mMissionId:I

    .line 68
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mCompleteInfoData:Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->getMissionProgressStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mMissionProgressStatus:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    .line 69
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mCompleteInfoData:Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->getMissionTitle()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mMissionTitle:Ljava/lang/String;

    .line 70
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mCompleteInfoData:Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->getMissionFrequency()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mMissionFrequency:I

    .line 71
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mCompleteInfoData:Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->getMissionProgressFrequency()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mMissionProgressFrequency:I

    .line 74
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->initLayout()V

    .line 75
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->updateButton()V

    .line 76
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->updateCompleteHeaderView()V

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->setCustomActionBar()V

    .line 78
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mFinishDelayHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0xfa0

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 183
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onDestroy()V

    .line 186
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mFinishDelayHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 190
    :goto_0
    return-void

    .line 187
    :catch_0
    move-exception v0

    .line 188
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setCustomActionBar()V
    .locals 6

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    .line 140
    .local v2, "actoiniBarHelper":Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    if-nez v2, :cond_1

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 142
    :cond_1
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 144
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getTotalActionBarButtonCount()I

    move-result v3

    if-nez v3, :cond_0

    .line 145
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;)V

    .line 165
    .local v0, "actionBarButtonListener":Landroid/view/View$OnClickListener;
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v3, 0x7f0207cf

    invoke-direct {v1, v3, v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    .line 166
    .local v1, "actionBarButtonMoreBuilder":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    const v3, 0x7f090033

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 167
    const/4 v3, 0x1

    new-array v3, v3, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v4, 0x0

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    goto :goto_0
.end method
