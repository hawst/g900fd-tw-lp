.class Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$3;
.super Ljava/lang/Object;
.source "BadgeAchieveActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)V
    .locals 0

    .prologue
    .line 385
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$3;->this$0:Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 389
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;->getRemainBadgeCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 390
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$3;->this$0:Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mFinishDelayHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 394
    :cond_0
    :goto_0
    return-void

    .line 391
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;->getRemainBadgeCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 392
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$3;->this$0:Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mFinishDelayHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
