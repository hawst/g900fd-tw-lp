.class Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity$1;
.super Ljava/lang/Object;
.source "AwardTodayActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity$1;->this$0:Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/high16 v3, 0x10000000

    .line 140
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 172
    :goto_0
    :pswitch_0
    return-void

    .line 142
    :pswitch_1
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v1

    const/16 v2, 0x2719

    if-ne v1, v2, :cond_0

    .line 145
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity$1;->this$0:Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;

    # getter for: Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->access$100(Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 146
    .local v0, "launcherIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity$1;->this$0:Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;

    # getter for: Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->access$100(Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 143
    .end local v0    # "launcherIntent":Landroid/content/Intent;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity$1;->this$0:Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;

    # invokes: Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->connectedWearableDevice()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->access$000(Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;)V

    goto :goto_0

    .line 152
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity$1;->this$0:Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;

    # getter for: Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->access$100(Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 153
    .restart local v0    # "launcherIntent":Landroid/content/Intent;
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 154
    iget-object v1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity$1;->this$0:Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;

    # getter for: Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->access$100(Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 159
    .end local v0    # "launcherIntent":Landroid/content/Intent;
    :pswitch_3
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity$1;->this$0:Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;

    # getter for: Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->access$100(Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 160
    .restart local v0    # "launcherIntent":Landroid/content/Intent;
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 161
    iget-object v1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity$1;->this$0:Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;

    # getter for: Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->access$100(Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 165
    .end local v0    # "launcherIntent":Landroid/content/Intent;
    :pswitch_4
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity$1;->this$0:Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;

    # getter for: Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->access$100(Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 166
    .restart local v0    # "launcherIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity$1;->this$0:Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;

    # getter for: Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->access$100(Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 140
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
