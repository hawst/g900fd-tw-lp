.class Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$5;
.super Ljava/lang/Object;
.source "UvSkinTypeDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)V
    .locals 0

    .prologue
    .line 284
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$5;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x1

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$5;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    # invokes: Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->clearSelection()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->access$300(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)V

    .line 290
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 330
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$5;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    # invokes: Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->updateButton()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->access$500(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)V

    .line 331
    return-void

    .line 294
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$5;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->rb_skintype_1:Landroid/widget/RadioButton;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->access$600(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)Landroid/widget/RadioButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 295
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$5;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->SKIN_EFFECT_1:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->setSelectedSkinOption(Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;)V

    goto :goto_0

    .line 300
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$5;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->rb_skintype_2:Landroid/widget/RadioButton;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->access$700(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)Landroid/widget/RadioButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 301
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$5;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->SKIN_EFFECT_2:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->setSelectedSkinOption(Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;)V

    goto :goto_0

    .line 306
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$5;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->rb_skintype_3:Landroid/widget/RadioButton;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->access$800(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)Landroid/widget/RadioButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$5;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->SKIN_EFFECT_3:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->setSelectedSkinOption(Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;)V

    goto :goto_0

    .line 312
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$5;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->rb_skintype_4:Landroid/widget/RadioButton;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->access$900(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)Landroid/widget/RadioButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$5;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->SKIN_EFFECT_4:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->setSelectedSkinOption(Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;)V

    goto :goto_0

    .line 318
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$5;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->rb_skintype_5:Landroid/widget/RadioButton;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->access$1000(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)Landroid/widget/RadioButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 319
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$5;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->SKIN_EFFECT_5:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->setSelectedSkinOption(Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;)V

    goto :goto_0

    .line 324
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$5;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->rb_skintype_6:Landroid/widget/RadioButton;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->access$1100(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)Landroid/widget/RadioButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 325
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$5;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->SKIN_EFFECT_6:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->setSelectedSkinOption(Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;)V

    goto :goto_0

    .line 290
    nop

    :pswitch_data_0
    .packed-switch 0x7f080ad2
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method
