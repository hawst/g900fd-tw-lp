.class Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;
.super Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;
.source "ChildFoodListItem.java"


# instance fields
.field private mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

.field private mMealItemData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V
    .locals 0
    .param p1, "mMealItemData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    .param p2, "mFoodInfoData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;->mMealItemData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .line 36
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 37
    return-void
.end method


# virtual methods
.method public addToFavorites()V
    .locals 0

    .prologue
    .line 60
    return-void
.end method

.method public createSelectedPanelHolder()Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;
    .locals 3

    .prologue
    .line 74
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;->mMealItemData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getAmount()F

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;F)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 99
    instance-of v2, p1, Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;

    if-nez v2, :cond_1

    .line 103
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 102
    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;

    .line 103
    .local v0, "that":Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;->mMealItemData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;->mMealItemData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    iget-object v3, v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;->mMealItemData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    iget-object v2, v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;->mMealItemData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    if-nez v2, :cond_0

    goto :goto_1
.end method

.method protected getDescription(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getDescription(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getListItemId()J
    .locals 2

    .prologue
    .line 84
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getListItemType()Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;->MEAL_ITEM:Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

    return-object v0
.end method

.method getMealItemData()Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;->mMealItemData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getView(Landroid/view/LayoutInflater;Landroid/view/View;Landroid/view/ViewGroup;Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;)Landroid/view/View;
    .locals 1
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .param p4, "outState"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-virtual {p4, v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;->setFavoriteStarsVisibility(Z)V

    .line 43
    invoke-virtual {p4, v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;->setAreItemsExpandable(Z)V

    .line 44
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->getView(Landroid/view/LayoutInflater;Landroid/view/View;Landroid/view/ViewGroup;Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;->mMealItemData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;->mMealItemData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFavorite()Z
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    return v0
.end method

.method public removeFromFavorites()V
    .locals 0

    .prologue
    .line 65
    return-void
.end method

.method public removeFromMyCategory()V
    .locals 0

    .prologue
    .line 80
    return-void
.end method
