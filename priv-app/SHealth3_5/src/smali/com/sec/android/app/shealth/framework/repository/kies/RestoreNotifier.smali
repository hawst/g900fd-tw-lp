.class public Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier;
.super Ljava/lang/Object;
.source "RestoreNotifier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$OnRestoreListener;,
        Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreStatus;,
        Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreType;
    }
.end annotation


# static fields
.field private static isBlocked:Z

.field private static listeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$OnRestoreListener;",
            ">;"
        }
    .end annotation
.end field

.field private static wl:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier;->listeners:Ljava/util/ArrayList;

    .line 29
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier;->isBlocked:Z

    .line 30
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier;->wl:Landroid/os/PowerManager$WakeLock;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    return-void
.end method

.method public static blockRestore()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier;->isBlocked:Z

    .line 56
    return-void
.end method

.method public static isRestoreBlocked()Z
    .locals 1

    .prologue
    .line 65
    sget-boolean v0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier;->isBlocked:Z

    return v0
.end method

.method public static restoreStarted(Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreType;)V
    .locals 3
    .param p0, "type"    # Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreType;

    .prologue
    .line 39
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$OnRestoreListener;

    .line 41
    .local v1, "listner":Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$OnRestoreListener;
    invoke-interface {v1, p0}, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$OnRestoreListener;->onRestoreStarted(Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreType;)V

    goto :goto_0

    .line 43
    .end local v1    # "listner":Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$OnRestoreListener;
    :cond_0
    return-void
.end method

.method public static restoreStopped(Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreType;)V
    .locals 3
    .param p0, "type"    # Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreType;

    .prologue
    .line 47
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$OnRestoreListener;

    .line 49
    .local v1, "listner":Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$OnRestoreListener;
    invoke-interface {v1, p0}, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$OnRestoreListener;->onRestoreStopped(Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreType;)V

    goto :goto_0

    .line 51
    .end local v1    # "listner":Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$OnRestoreListener;
    :cond_0
    return-void
.end method

.method public static setOnListener(Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$OnRestoreListener;)V
    .locals 1
    .param p0, "listener"    # Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$OnRestoreListener;

    .prologue
    .line 34
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 35
    return-void
.end method

.method public static unblockRestore()V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier;->isBlocked:Z

    .line 61
    return-void
.end method
