.class Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$2;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v3, 0x0

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    const/4 v0, 0x6

    new-array v1, v0, [I

    const/4 v0, 0x5

    const/16 v2, -0x1e

    aput v2, v1, v0

    const/16 v0, -0x3a

    aput v0, v1, v11

    const/16 v0, -0x3e

    aput v0, v1, v10

    const/16 v0, -0x42

    aput v0, v1, v9

    const/16 v0, 0x7866

    aput v0, v1, v8

    const/16 v0, 0x2039

    aput v0, v1, v3

    const/4 v0, 0x6

    new-array v0, v0, [I

    const/4 v2, 0x5

    const/16 v5, -0x74

    aput v5, v0, v2

    const/16 v2, -0x57

    aput v2, v0, v11

    const/16 v2, -0x55

    aput v2, v0, v10

    const/16 v2, -0x36

    aput v2, v0, v9

    const/16 v2, 0x7805

    aput v2, v0, v8

    const/16 v2, 0x2078

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v5, v0

    if-lt v2, v5, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    move v2, v3

    :goto_1
    array-length v5, v0

    if-lt v2, v5, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    const/4 v0, 0x6

    new-array v1, v0, [I

    const/4 v0, 0x5

    const/16 v2, -0x6dd7

    aput v2, v1, v0

    const/4 v0, -0x5

    aput v0, v1, v11

    const/16 v0, -0x13a9

    aput v0, v1, v10

    const/16 v0, -0x44

    aput v0, v1, v9

    const/16 v0, -0x50

    aput v0, v1, v8

    const/16 v0, -0x64

    aput v0, v1, v3

    const/4 v0, 0x6

    new-array v0, v0, [I

    const/4 v2, 0x5

    const/16 v6, -0x6da5

    aput v6, v0, v2

    const/16 v2, -0x6e

    aput v2, v0, v11

    const/16 v2, -0x13ca

    aput v2, v0, v10

    const/16 v2, -0x14

    aput v2, v0, v9

    const/16 v2, -0x22

    aput v2, v0, v8

    const/16 v2, -0x37

    aput v2, v0, v3

    move v2, v3

    :goto_2
    array-length v6, v0

    if-lt v2, v6, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    move v2, v3

    :goto_3
    array-length v6, v0

    if-lt v2, v6, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v0, 0x8

    new-array v1, v0, [I

    const/4 v0, 0x5

    const/4 v2, 0x6

    const/4 v5, 0x7

    const/16 v6, -0x7ff

    aput v6, v1, v5

    const/16 v5, -0x4f

    aput v5, v1, v2

    const/16 v2, -0xc

    aput v2, v1, v0

    const/16 v0, -0x5c

    aput v0, v1, v11

    const/16 v0, -0x66

    aput v0, v1, v10

    const/16 v0, -0x4d

    aput v0, v1, v9

    const/16 v0, -0x4b

    aput v0, v1, v8

    const/16 v0, 0x2c65

    aput v0, v1, v3

    const/16 v0, 0x8

    new-array v0, v0, [I

    const/4 v2, 0x5

    const/4 v5, 0x6

    const/4 v6, 0x7

    const/16 v7, -0x7bb

    aput v7, v0, v6

    const/4 v6, -0x8

    aput v6, v0, v5

    const/16 v5, -0x6f

    aput v5, v0, v2

    const/16 v2, -0x39

    aput v2, v0, v11

    const/16 v2, -0xd

    aput v2, v0, v10

    const/16 v2, -0x3b

    aput v2, v0, v9

    const/16 v2, -0x30

    aput v2, v0, v8

    const/16 v2, 0x2c21

    aput v2, v0, v3

    move v2, v3

    :goto_4
    array-length v5, v0

    if-lt v2, v5, :cond_4

    array-length v0, v1

    new-array v0, v0, [C

    :goto_5
    array-length v2, v0

    if-lt v3, v2, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mDeviceID:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->access$000(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)Landroid/app/Activity;

    move-result-object v0

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1, v4}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->finish()V

    return-void

    :cond_0
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_1
    aget v5, v1, v2

    int-to-char v5, v5

    aput-char v5, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    :cond_2
    aget v6, v0, v2

    aget v7, v1, v2

    xor-int/2addr v6, v7

    aput v6, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    :cond_3
    aget v6, v1, v2

    int-to-char v6, v6

    aput-char v6, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_3

    :cond_4
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_5
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_5
.end method
