.class public Lcom/sec/android/app/shealth/common/commonui/EditTextWithLengthLimit;
.super Landroid/widget/EditText;
.source "EditTextWithLengthLimit.java"


# static fields
.field private static final DEFAULT_VALUE:I = -0x1


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/common/commonui/EditTextWithLengthLimit;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/EditTextWithLengthLimit;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/EditTextWithLengthLimit;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 68
    return-void
.end method

.method private getMaxLengthValue(Landroid/content/Context;Landroid/util/AttributeSet;)I
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v4, -0x1

    .line 114
    sget-object v2, Lcom/android/internal/R$styleable;->TextView:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 122
    .local v1, "typedArray":Landroid/content/res/TypedArray;
    const/16 v2, 0x23

    const/4 v3, -0x1

    :try_start_0
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 127
    .local v0, "maxLength":I
    if-gt v0, v4, :cond_0

    .line 129
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Max length value not found"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 135
    .end local v0    # "maxLength":I
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v2

    .restart local v0    # "maxLength":I
    :cond_0
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 139
    return v0
.end method

.method private getToastTextId(Landroid/content/Context;Landroid/util/AttributeSet;)I
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v4, -0x1

    .line 84
    sget-object v2, Lcom/sec/android/app/shealth/common/commonui/R$styleable;->EditTextWithLengthLimit:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 92
    .local v1, "typedArray":Landroid/content/res/TypedArray;
    const/4 v2, 0x0

    const/4 v3, -0x1

    :try_start_0
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 96
    .local v0, "textId":I
    if-gt v0, v4, :cond_0

    .line 98
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Toast text id not found"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    .end local v0    # "textId":I
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v2

    .restart local v0    # "textId":I
    :cond_0
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 108
    return v0
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 72
    if-nez p2, :cond_0

    .line 74
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "EditText should have attributes"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 77
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelperThatShowsToast;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/EditTextWithLengthLimit;->getToastTextId(Landroid/content/Context;Landroid/util/AttributeSet;)I

    move-result v1

    invoke-direct {v0, p1, v1}, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelperThatShowsToast;-><init>(Landroid/content/Context;I)V

    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/EditTextWithLengthLimit;->getMaxLengthValue(Landroid/content/Context;Landroid/util/AttributeSet;)I

    move-result v1

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelperThatShowsToast;->addLimit(ILandroid/widget/EditText;)V

    .line 80
    return-void
.end method
