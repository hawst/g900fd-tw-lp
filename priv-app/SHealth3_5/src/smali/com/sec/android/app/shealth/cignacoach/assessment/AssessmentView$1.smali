.class Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$1;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "AssessmentView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->initGesture()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;)V
    .locals 0

    .prologue
    .line 580
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    const/high16 v0, -0x40800000    # -1.0f

    .line 607
    # setter for: Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mMoveLastXAxis:F
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->access$002(F)F

    .line 608
    # setter for: Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mMoveFisrtXAxis:F
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->access$202(F)F

    .line 609
    # setter for: Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->gestureOn:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->access$102(Z)Z

    .line 610
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    # setter for: Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mMoveFisrtXAxis:F
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->access$202(F)F

    .line 612
    return v1
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 619
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    .line 583
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    # setter for: Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mMoveLastXAxis:F
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->access$002(F)F

    .line 585
    # getter for: Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->gestureOn:Z
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->access$100()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 587
    const/4 v0, 0x0

    # setter for: Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->gestureOn:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->access$102(Z)Z

    .line 589
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->isKeyboardShow()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 602
    :cond_0
    :goto_0
    return v2

    .line 593
    :cond_1
    # getter for: Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mMoveLastXAxis:F
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->access$000()F

    move-result v0

    # getter for: Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mMoveFisrtXAxis:F
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->access$200()F

    move-result v1

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mTouchXSlop:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->access$300(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;)I

    move-result v1

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 594
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->hideKeyboard()V

    .line 595
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->activity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->access$400(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->goBackScreenForSwipe()V

    goto :goto_0

    .line 596
    :cond_2
    # getter for: Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mMoveFisrtXAxis:F
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->access$200()F

    move-result v0

    # getter for: Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mMoveLastXAxis:F
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->access$000()F

    move-result v1

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mTouchXSlop:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->access$300(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;)I

    move-result v1

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 597
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->hideKeyboard()V

    .line 598
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->activity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->access$400(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->goNextScreenForSwipe()V

    goto :goto_0
.end method
