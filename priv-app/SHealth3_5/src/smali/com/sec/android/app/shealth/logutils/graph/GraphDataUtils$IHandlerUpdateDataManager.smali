.class public interface abstract Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;
.super Ljava/lang/Object;
.source "GraphDataUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IHandlerUpdateDataManager"
.end annotation


# virtual methods
.method public abstract addLatestMeasureTimeInHour(J)V
.end method

.method public abstract getHandlerTime()J
.end method

.method public abstract getLatestMeasureTimeInHour(J)J
.end method

.method public abstract getMeasureCountForPeriod(IJ)I
.end method

.method public abstract setHandlerTime(J)V
.end method

.method public abstract setMeasureCountForPeriod(IJI)V
.end method
