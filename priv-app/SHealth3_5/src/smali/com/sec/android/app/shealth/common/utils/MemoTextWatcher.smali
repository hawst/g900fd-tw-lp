.class public abstract Lcom/sec/android/app/shealth/common/utils/MemoTextWatcher;
.super Ljava/lang/Object;
.source "MemoTextWatcher.java"

# interfaces
.implements Landroid/text/TextWatcher;


# static fields
.field public static final MAX_CHARACTER_COUNT:I = 0x32


# instance fields
.field private max_character:I

.field oldText:Ljava/lang/String;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/MemoTextWatcher;->oldText:Ljava/lang/String;

    .line 18
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/MemoTextWatcher;->max_character:I

    .line 19
    return-void
.end method

.method protected constructor <init>(I)V
    .locals 1
    .param p1, "maxSize"    # I

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/MemoTextWatcher;->oldText:Ljava/lang/String;

    .line 26
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/MemoTextWatcher;->max_character:I

    .line 27
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 6
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/4 v1, 0x0

    .line 51
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    iget v2, p0, Lcom/sec/android/app/shealth/common/utils/MemoTextWatcher;->max_character:I

    if-le v0, v2, :cond_0

    .line 52
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/utils/MemoTextWatcher;->oldText:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/MemoTextWatcher;->oldText:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    move-object v0, p1

    move v4, v1

    invoke-interface/range {v0 .. v5}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;II)Landroid/text/Editable;

    .line 53
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/MemoTextWatcher;->showMaxAlert()V

    .line 57
    :goto_0
    return-void

    .line 55
    :cond_0
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/utils/MemoTextWatcher;->inputtedTextLengthChanged(I)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 39
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/shealth/common/utils/MemoTextWatcher;->max_character:I

    if-gt v0, v1, :cond_0

    .line 40
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/MemoTextWatcher;->oldText:Ljava/lang/String;

    .line 41
    :cond_0
    return-void
.end method

.method protected inputtedTextLengthChanged(I)V
    .locals 0
    .param p1, "length"    # I

    .prologue
    .line 65
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 46
    return-void
.end method

.method protected abstract showMaxAlert()V
.end method
