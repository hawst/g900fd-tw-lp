.class public abstract Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
.super Landroid/widget/CursorTreeAdapter;


# static fields
.field public static final TAG_TIME_FORMAT:Ljava/lang/String; = "dd/MM/yyyy"


# instance fields
.field private groupExpandMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mCheckAll:Z

.field private mCheckedItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDeleteMode:Z

.field private mMenuDeleteMode:Z

.field protected onSelectCheckboxListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/CursorTreeAdapter;-><init>(Landroid/database/Cursor;Landroid/content/Context;Z)V

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->mDeleteMode:Z

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->mMenuDeleteMode:Z

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->mCheckAll:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->mCheckedItems:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->onSelectCheckboxListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->groupExpandMap:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public clearAllSelection()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->mCheckedItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public clearCheckedItems()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->mCheckedItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->mCheckAll:Z

    return-void
.end method

.method public getCheckedItems()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->mCheckedItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected abstract getColumnNameForCreateTime()Ljava/lang/String;
.end method

.method protected abstract getColumnNameForID()Ljava/lang/String;
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/CursorTreeAdapter;->getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->isGroupCollapsed(I)Z

    move-result v1

    if-nez v1, :cond_0

    instance-of v1, p4, Landroid/widget/ExpandableListView;

    if-eqz v1, :cond_0

    check-cast p4, Landroid/widget/ExpandableListView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getGroupCount()I

    move-result v1

    if-le v1, p1, :cond_0

    invoke-virtual {p4, p1}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    :cond_0
    return-object v0
.end method

.method protected getSelectedLogCount()I
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->mCheckedItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method protected abstract getTotalChildCount()I
.end method

.method public isCheckAll()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->mCheckAll:Z

    return v0
.end method

.method public isDeleteMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->mDeleteMode:Z

    return v0
.end method

.method protected isGroupCollapsed(I)Z
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->groupExpandMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->groupExpandMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isLogSelected(Ljava/lang/Object;)Z
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->mCheckedItems:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isMenuDeleteMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->mMenuDeleteMode:Z

    return v0
.end method

.method public setCheckAll(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->mCheckAll:Z

    return-void
.end method

.method public setClickedGroupPosition(IZ)V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->groupExpandMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->groupExpandMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->groupExpandMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setDeleteMode(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->mDeleteMode:Z

    return-void
.end method

.method protected setLogSelected(Ljava/lang/String;Z)V
    .locals 1

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->mCheckedItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->mCheckedItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public setMenuDeleteMode(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->mMenuDeleteMode:Z

    return-void
.end method
