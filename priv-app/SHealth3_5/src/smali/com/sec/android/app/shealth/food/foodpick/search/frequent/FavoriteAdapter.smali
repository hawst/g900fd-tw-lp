.class public Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;
.super Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;
.source "FavoriteAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$OnFavoriteClickListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;",
        ":",
        "Ljava/lang/Comparable;",
        ">",
        "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private mOnFavoriteClickListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$OnFavoriteClickListener",
            "<TE;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter<TE;>;"
    .local p2, "allItemsList":Ljava/util/List;, "Ljava/util/List<TE;>;"
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->itemsList:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->mOnFavoriteClickListeners:Ljava/util/List;

    .line 41
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->mOnFavoriteClickListeners:Ljava/util/List;

    return-object v0
.end method

.method private varargs makeFormattedToast(I[Ljava/lang/String;)V
    .locals 3
    .param p1, "stringResId"    # I
    .param p2, "params"    # [Ljava/lang/String;

    .prologue
    .line 100
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter<TE;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->context:Landroid/content/Context;

    invoke-virtual {v1, p1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 101
    return-void
.end method


# virtual methods
.method public addListItems(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 45
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter<TE;>;"
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<TE;>;"
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;->addListItems(Ljava/util/List;)V

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->itemsList:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 47
    return-void
.end method

.method public addOnFavoriteClickListener(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$OnFavoriteClickListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$OnFavoriteClickListener",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 110
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter<TE;>;"
    .local p1, "onFavoriteClickListener":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$OnFavoriteClickListener;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$OnFavoriteClickListener<TE;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->mOnFavoriteClickListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 51
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter<TE;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->itemsList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->getViewFromListItem(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected getViewFromListItem(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "Landroid/view/View;",
            "Landroid/view/ViewGroup;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 55
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter<TE;>;"
    .local p1, "listItem":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;, "TE;"
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->inflater:Landroid/view/LayoutInflater;

    invoke-interface {p1, v1, p2, p3}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;->getView(Landroid/view/LayoutInflater;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 56
    .local v0, "listItemView":Landroid/view/View;
    invoke-interface {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;->getFavoriteWrapper()Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$1;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$1;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    const v1, 0x7f0d0043

    invoke-virtual {v0, v1, p1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 83
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    return-object v0
.end method

.method protected performClickOnFavoriteButton(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter<TE;>;"
    .local p1, "listItem":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;, "TE;"
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 88
    invoke-interface {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;->isFavorite()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    invoke-interface {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;->removeFromFavorites()V

    .line 90
    const v0, 0x7f0909b9

    new-array v1, v1, [Ljava/lang/String;

    check-cast p1, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;

    .end local p1    # "listItem":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;, "TE;"
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->makeFormattedToast(I[Ljava/lang/String;)V

    .line 96
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->itemsList:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 97
    return-void

    .line 93
    .restart local p1    # "listItem":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;, "TE;"
    :cond_0
    invoke-interface {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;->addToFavorites()V

    .line 94
    const v0, 0x7f0909ba

    new-array v1, v1, [Ljava/lang/String;

    check-cast p1, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;

    .end local p1    # "listItem":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;, "TE;"
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->makeFormattedToast(I[Ljava/lang/String;)V

    goto :goto_0
.end method

.method public removeOnFavoriteClickListener(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$OnFavoriteClickListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$OnFavoriteClickListener",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 119
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter<TE;>;"
    .local p1, "onFavoriteClickListener":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$OnFavoriteClickListener;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$OnFavoriteClickListener<TE;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->mOnFavoriteClickListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 120
    return-void
.end method
