.class Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$2;
.super Ljava/lang/Object;
.source "HomeWallpaperChooser.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$2;->this$0:Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v2, 0x0

    .line 115
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$2;->this$0:Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;

    # getter for: Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->isKeyboardEvent:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->access$300(Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 117
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$2;->this$0:Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;

    invoke-virtual {v1, p2}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->setGalleryBackgroundFocus(Landroid/view/View;)V

    .line 119
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$2;->this$0:Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;

    # setter for: Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->currentWallpaper:I
    invoke-static {v1, p3}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->access$202(Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;I)I

    .line 120
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$2;->this$0:Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;

    # getter for: Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->backgrounds:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->access$000(Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 121
    invoke-virtual {p1, v0}, Landroid/widget/AdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 122
    invoke-virtual {p1, v0}, Landroid/widget/AdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 123
    invoke-virtual {p1, v0}, Landroid/widget/AdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2, v2, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 120
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 128
    .end local v0    # "i":I
    :cond_1
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 134
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
