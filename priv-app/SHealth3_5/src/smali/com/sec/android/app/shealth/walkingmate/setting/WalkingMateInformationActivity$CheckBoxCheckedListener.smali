.class Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;
.super Ljava/lang/Object;
.source "WalkingMateInformationActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckBoxCheckedListener"
.end annotation


# static fields
.field private static final mAnimationDuration:I = 0x1f4


# instance fields
.field private mAnimatedContainerView:Landroid/view/View;

.field private final mCheckBoxId:I

.field private mCollapseAnimation:Lcom/sec/android/app/shealth/walkingmate/utils/DropDownAnime;

.field private mContainer:Landroid/widget/LinearLayout;

.field mDetailTextView:Landroid/widget/TextView;

.field private mExpandAnimation:Lcom/sec/android/app/shealth/walkingmate/utils/DropDownAnime;

.field private mTitle:Landroid/widget/TextView;

.field private mUpperContainer:Landroid/view/View;

.field private mWithoutAnimation:Z

.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;Landroid/view/View;I)V
    .locals 2
    .param p2, "container"    # Landroid/view/View;
    .param p3, "checkBoxId"    # I

    .prologue
    .line 146
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147
    iput-object p2, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mUpperContainer:Landroid/view/View;

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mUpperContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mUpperContainer:Landroid/view/View;

    const v1, 0x7f0807c3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mDetailTextView:Landroid/widget/TextView;

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mUpperContainer:Landroid/view/View;

    const v1, 0x7f080baf

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mContainer:Landroid/widget/LinearLayout;

    .line 151
    iput p3, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mCheckBoxId:I

    .line 152
    const v0, 0x7f080632

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mAnimatedContainerView:Landroid/view/View;

    .line 153
    const v0, 0x7f08004d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mTitle:Landroid/widget/TextView;

    .line 154
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;Landroid/view/View;IZ)V
    .locals 0
    .param p2, "container"    # Landroid/view/View;
    .param p3, "checkBoxId"    # I
    .param p4, "isWithoutAnimation"    # Z

    .prologue
    .line 157
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;-><init>(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;Landroid/view/View;I)V

    .line 158
    iput-boolean p4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mWithoutAnimation:Z

    .line 159
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mContainer:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method private initializeDropDownAnimation()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x1f4

    .line 204
    const/4 v0, 0x0

    .line 206
    .local v0, "animatedViewHeight":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0ae9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 207
    .local v1, "image_height":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0aea

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    mul-int/lit8 v2, v3, 0x2

    .line 209
    .local v2, "padding":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mDetailTextView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v3

    invoke-virtual {v3}, Landroid/text/Layout;->getHeight()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0926

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v3, v1

    add-int v0, v3, v2

    .line 210
    new-instance v3, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener$1;

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mAnimatedContainerView:Landroid/view/View;

    const/4 v5, 0x0

    invoke-direct {v3, p0, v4, v0, v5}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;Landroid/view/View;IZ)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mCollapseAnimation:Lcom/sec/android/app/shealth/walkingmate/utils/DropDownAnime;

    .line 220
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mCollapseAnimation:Lcom/sec/android/app/shealth/walkingmate/utils/DropDownAnime;

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/app/shealth/walkingmate/utils/DropDownAnime;->setDuration(J)V

    .line 221
    new-instance v3, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener$2;

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mAnimatedContainerView:Landroid/view/View;

    const/4 v5, 0x1

    invoke-direct {v3, p0, v4, v0, v5}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener$2;-><init>(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;Landroid/view/View;IZ)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mExpandAnimation:Lcom/sec/android/app/shealth/walkingmate/utils/DropDownAnime;

    .line 238
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mExpandAnimation:Lcom/sec/android/app/shealth/walkingmate/utils/DropDownAnime;

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/app/shealth/walkingmate/utils/DropDownAnime;->setDuration(J)V

    .line 239
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v8, 0x8

    const/4 v6, 0x0

    .line 164
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v5

    if-nez v5, :cond_1

    const/4 v5, 0x1

    :goto_0
    invoke-virtual {p1, v5}, Landroid/view/View;->setSelected(Z)V

    .line 165
    iget-boolean v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mWithoutAnimation:Z

    if-eqz v5, :cond_2

    .line 166
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mAnimatedContainerView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    const/4 v7, -0x2

    iput v7, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 167
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mAnimatedContainerView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->requestLayout()V

    .line 168
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mWithoutAnimation:Z

    .line 195
    :cond_0
    return-void

    :cond_1
    move v5, v6

    .line 164
    goto :goto_0

    .line 172
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mCollapseAnimation:Lcom/sec/android/app/shealth/walkingmate/utils/DropDownAnime;

    if-nez v5, :cond_3

    .line 173
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->initializeDropDownAnimation()V

    .line 174
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mGuideList:Ljava/util/List;
    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->access$000(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;)Ljava/util/List;

    move-result-object v5

    iget v7, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mCheckBoxId:I

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v7

    if-eqz v7, :cond_6

    iget-object v7, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mExpandAnimation:Lcom/sec/android/app/shealth/walkingmate/utils/DropDownAnime;

    :goto_1
    invoke-virtual {v5, v7}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 177
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mGuideList:Ljava/util/List;
    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->access$000(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 179
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mGuideList:Ljava/util/List;
    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->access$000(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;)Ljava/util/List;

    move-result-object v5

    iget v7, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mCheckBoxId:I

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    const v7, 0x7f0804e6

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 180
    .local v4, "title_divider":Landroid/view/View;
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mGuideList:Ljava/util/List;
    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->access$000(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;)Ljava/util/List;

    move-result-object v5

    iget v7, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mCheckBoxId:I

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    const v7, 0x7f080bae

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 182
    .local v0, "content_divider":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v2

    .line 183
    .local v2, "mGuideListCurrentVisibility":I
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v3

    .line 185
    .local v3, "mGuideListCurrentVisibilityDivider":I
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 186
    iget v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mCheckBoxId:I

    if-ne v1, v5, :cond_4

    move v2, v8

    .end local v2    # "mGuideListCurrentVisibility":I
    :cond_4
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 188
    iget v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mCheckBoxId:I

    if-ne v1, v5, :cond_5

    move v3, v6

    .end local v3    # "mGuideListCurrentVisibilityDivider":I
    :cond_5
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 177
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 174
    .end local v0    # "content_divider":Landroid/view/View;
    .end local v1    # "i":I
    .end local v4    # "title_divider":Landroid/view/View;
    :cond_6
    iget-object v7, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mCollapseAnimation:Lcom/sec/android/app/shealth/walkingmate/utils/DropDownAnime;

    goto :goto_1

    .line 190
    .restart local v0    # "content_divider":Landroid/view/View;
    .restart local v1    # "i":I
    .restart local v2    # "mGuideListCurrentVisibility":I
    .restart local v3    # "mGuideListCurrentVisibilityDivider":I
    .restart local v4    # "title_divider":Landroid/view/View;
    :cond_7
    iget v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mCheckBoxId:I

    if-ne v1, v5, :cond_8

    move v2, v6

    .end local v2    # "mGuideListCurrentVisibility":I
    :cond_8
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 192
    iget v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mCheckBoxId:I

    if-ne v1, v5, :cond_9

    move v3, v8

    .end local v3    # "mGuideListCurrentVisibilityDivider":I
    :cond_9
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3
.end method
