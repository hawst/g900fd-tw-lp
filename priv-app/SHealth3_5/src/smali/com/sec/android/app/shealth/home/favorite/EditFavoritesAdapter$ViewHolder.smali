.class public Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter$ViewHolder;
.super Ljava/lang/Object;
.source "EditFavoritesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ViewHolder"
.end annotation


# instance fields
.field public checkIcon:Landroid/widget/CheckBox;

.field public iconImage:Landroid/widget/ImageView;

.field public iconText:Landroid/widget/TextView;

.field public imageContainer:Landroid/widget/RelativeLayout;

.field public thirdIcon:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object v0, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter$ViewHolder;->iconImage:Landroid/widget/ImageView;

    .line 46
    iput-object v0, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter$ViewHolder;->iconText:Landroid/widget/TextView;

    .line 47
    iput-object v0, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter$ViewHolder;->thirdIcon:Landroid/widget/ImageView;

    .line 48
    iput-object v0, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter$ViewHolder;->imageContainer:Landroid/widget/RelativeLayout;

    .line 49
    iput-object v0, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter$ViewHolder;->checkIcon:Landroid/widget/CheckBox;

    return-void
.end method
