.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ListPopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PopupListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;Landroid/content/Context;[Ljava/lang/String;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "items"    # [Ljava/lang/String;

    .prologue
    .line 291
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    .line 292
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getLayoutId()I

    move-result v0

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getTextViewId()I

    move-result v1

    invoke-direct {p0, p2, v0, v1, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    .line 293
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 16
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 297
    invoke-super/range {p0 .. p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v12

    .line 298
    .local v12, "view":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getTextViewId()I

    move-result v13

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 299
    .local v10, "text":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getBelowTextViewId()I

    move-result v13

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 300
    .local v1, "belowTextView":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->textPaddings:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$TextPaddings;
    invoke-static {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$TextPaddings;

    move-result-object v13

    if-eqz v13, :cond_0

    .line 301
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->applyPaddinsToTextView(Landroid/widget/TextView;)V
    invoke-static {v13, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;Landroid/widget/TextView;)V

    .line 302
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->applyPaddinsToTextView(Landroid/widget/TextView;)V
    invoke-static {v13, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;Landroid/widget/TextView;)V

    .line 304
    :cond_0
    if-nez p1, :cond_9

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->isAdditionalTextVisible:Z
    invoke-static {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)Z

    move-result v13

    if-nez v13, :cond_1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->tittleText:Ljava/lang/String;
    invoke-static {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_9

    .line 305
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getRecetTextViewId()I

    move-result v13

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 306
    .local v11, "titleTextView":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->textPaddings:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$TextPaddings;
    invoke-static {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$TextPaddings;

    move-result-object v13

    if-eqz v13, :cond_2

    .line 307
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->applyPaddinsToTextView(Landroid/widget/TextView;)V
    invoke-static {v13, v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;Landroid/widget/TextView;)V

    .line 309
    :cond_2
    const/4 v13, 0x0

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 310
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->tittleText:Ljava/lang/String;
    invoke-static {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_3

    .line 311
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->tittleText:Ljava/lang/String;
    invoke-static {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 317
    .end local v11    # "titleTextView":Landroid/widget/TextView;
    :cond_3
    :goto_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getClickableViewId()I

    move-result v13

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 318
    .local v2, "clickableView":Landroid/view/View;
    if-eqz v2, :cond_4

    .line 319
    if-nez p1, :cond_a

    .line 320
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getTopItemDrawableId()I

    move-result v13

    invoke-virtual {v2, v13}, Landroid/view/View;->setBackgroundResource(I)V

    .line 326
    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->listeners:[Landroid/view/View$OnClickListener;
    invoke-static {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)[Landroid/view/View$OnClickListener;

    move-result-object v13

    aget-object v13, v13, p1

    invoke-virtual {v2, v13}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 327
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 328
    .local v3, "clickableViewLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getListPopupItemHeight()I

    move-result v13

    iput v13, v3, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 329
    invoke-virtual {v2, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 331
    .end local v3    # "clickableViewLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->isAdditionalTextVisible:Z
    invoke-static {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 332
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->additionalItemsPosition:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$AdditionalItemsPosition;
    invoke-static {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$AdditionalItemsPosition;

    move-result-object v13

    sget-object v14, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$AdditionalItemsPosition;->BELOW:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$AdditionalItemsPosition;

    if-ne v13, v14, :cond_c

    .line 333
    const/4 v13, 0x0

    invoke-virtual {v1, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 334
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->additionalItems:[Ljava/lang/String;
    invoke-static {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->access$900(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)[Ljava/lang/String;

    move-result-object v13

    aget-object v13, v13, p1

    invoke-virtual {v1, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 341
    :cond_5
    :goto_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->hasContentDescription:Z
    invoke-static {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 342
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->descriptions:[Ljava/lang/String;
    invoke-static {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)[Ljava/lang/String;

    move-result-object v13

    aget-object v13, v13, p1

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 344
    :cond_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->selectedItemIndex:I
    invoke-static {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->access$1200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)I

    move-result v13

    move/from16 v0, p1

    if-ne v0, v13, :cond_d

    const/4 v4, 0x1

    .line 345
    .local v4, "isCurrentItemSelected":Z
    :goto_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    iget-object v13, v13, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->context:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    if-eqz v4, :cond_e

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getSelectedItemColorId()I

    move-result v13

    :goto_4
    invoke-virtual {v14, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setTextColor(I)V

    .line 347
    const-string/jumbo v13, "sec-roboto-light"

    const/4 v14, 0x0

    invoke-static {v13, v14}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v13

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 348
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->textSelection:Ljava/lang/String;
    invoke-static {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_7

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->textSelection:Ljava/lang/String;
    invoke-static {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "\\s+"

    invoke-virtual {v13, v14}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_7

    .line 349
    invoke-virtual {v10}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v13

    invoke-interface {v13}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 350
    .local v6, "s":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->textSelection:Ljava/lang/String;
    invoke-static {v14}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    .line 351
    .local v8, "selectionStart":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->textSelection:Ljava/lang/String;
    invoke-static {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int v7, v8, v13

    .line 352
    .local v7, "selectionEnd":I
    if-ltz v8, :cond_7

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v13

    if-gt v7, v13, :cond_7

    .line 353
    new-instance v9, Landroid/text/SpannableStringBuilder;

    invoke-direct {v9, v6}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 354
    .local v9, "spannable":Landroid/text/SpannableStringBuilder;
    new-instance v13, Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    iget-object v14, v14, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->context:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f07027a

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getColor(I)I

    move-result v14

    invoke-direct {v13, v14}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v14, 0x21

    invoke-virtual {v9, v13, v8, v7, v14}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 356
    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 359
    .end local v6    # "s":Ljava/lang/String;
    .end local v7    # "selectionEnd":I
    .end local v8    # "selectionStart":I
    .end local v9    # "spannable":Landroid/text/SpannableStringBuilder;
    :cond_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    iget-object v13, v13, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->context:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    if-eqz v4, :cond_f

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getSelectedItemColorId()I

    move-result v13

    :goto_5
    invoke-virtual {v14, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    invoke-virtual {v1, v13}, Landroid/widget/TextView;->setTextColor(I)V

    .line 362
    if-nez p1, :cond_8

    invoke-virtual {v12}, Landroid/view/View;->hasFocus()Z

    move-result v13

    if-nez v13, :cond_8

    .line 363
    invoke-virtual {v12}, Landroid/view/View;->requestFocus()Z

    .line 364
    :cond_8
    invoke-static {v12}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->makeSingleViewSingleTouchable(Landroid/view/View;)V

    .line 365
    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Landroid/view/View;->setFocusable(Z)V

    .line 366
    return-object v12

    .line 314
    .end local v2    # "clickableView":Landroid/view/View;
    .end local v4    # "isCurrentItemSelected":Z
    :cond_9
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getRecetTextViewId()I

    move-result v13

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/view/View;->setVisibility(I)V

    .line 315
    const/16 v13, 0x8

    invoke-virtual {v1, v13}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 321
    .restart local v2    # "clickableView":Landroid/view/View;
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->getCount()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    move/from16 v0, p1

    if-ne v0, v13, :cond_b

    .line 322
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getBottomItemDrawableId()I

    move-result v13

    invoke-virtual {v2, v13}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_1

    .line 324
    :cond_b
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getMiddleItemDrawableId()I

    move-result v13

    invoke-virtual {v2, v13}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_1

    .line 336
    :cond_c
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getAdditionalUnitViewId()I

    move-result v13

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 337
    .local v5, "rightText":Landroid/widget/TextView;
    const/4 v13, 0x0

    invoke-virtual {v5, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 338
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->additionalItems:[Ljava/lang/String;
    invoke-static {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->access$900(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)[Ljava/lang/String;

    move-result-object v13

    aget-object v13, v13, p1

    invoke-virtual {v5, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 344
    .end local v5    # "rightText":Landroid/widget/TextView;
    :cond_d
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 345
    .restart local v4    # "isCurrentItemSelected":Z
    :cond_e
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getDefaultItemColorId()I

    move-result v13

    goto/16 :goto_4

    .line 359
    :cond_f
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getGreyItemColorId()I

    move-result v13

    goto/16 :goto_5
.end method
