.class public Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;
.super Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;
.source "GoalDaoImplDb.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb",
        "<",
        "Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;",
        ">;",
        "Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;"
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    const-string/jumbo v3, "set_time"

    const-string v4, "_id"

    const-string/jumbo v5, "value"

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;-><init>(Landroid/net/Uri;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    return-void
.end method


# virtual methods
.method public deleteAllByGoalType(I)I
    .locals 4
    .param p1, "goalType"    # I

    .prologue
    .line 64
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string v1, "goal_type"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "="

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;->deleteAll(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)I

    move-result v0

    return v0
.end method

.method public getActiveGoal(IJ)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    .locals 6
    .param p1, "goalType"    # I
    .param p2, "beforTime"    # J

    .prologue
    .line 54
    new-instance v1, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    new-instance v2, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string/jumbo v3, "update_time"

    invoke-static {p2, p3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const-string v5, "<="

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;-><init>(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)V

    new-instance v2, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string v3, "goal_type"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const-string v5, "="

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->AND(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v0

    .line 59
    .local v0, "filter":Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;->getTimeColumnName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;->DESC:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;->getLastDataWithFilter(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Ljava/lang/String;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    return-object v1
.end method

.method protected bridge synthetic getDataContentValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)Landroid/content/ContentValues;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    .prologue
    .line 35
    check-cast p1, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    .end local p1    # "x0":Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;->getDataContentValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;)Landroid/content/ContentValues;

    move-result-object v0

    return-object v0
.end method

.method protected getDataContentValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;)Landroid/content/ContentValues;
    .locals 4
    .param p1, "item"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    .prologue
    .line 69
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)V

    .line 71
    .local v0, "values":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;
    const-string/jumbo v1, "value"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 72
    const-string v1, "goal_type"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getGoalType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 73
    const-string v1, "goal_subtype"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getGoalSubType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 74
    const-string/jumbo v1, "period"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getPeriod()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 75
    const-string/jumbo v1, "set_time"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getSetTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 77
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->getContentValues()Landroid/content/ContentValues;

    move-result-object v1

    return-object v1
.end method

.method protected bridge synthetic getItemFromRow(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    .locals 1
    .param p1, "x0"    # Landroid/database/Cursor;

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;->getItemFromRow(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v0

    return-object v0
.end method

.method protected getItemFromRow(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    .locals 18
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 82
    new-instance v17, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;-><init>(Landroid/database/Cursor;)V

    .line 83
    .local v17, "getter":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;
    new-instance v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    const-string/jumbo v3, "value"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getFloat(Ljava/lang/String;)F

    move-result v3

    const-string v4, "goal_type"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getInt(Ljava/lang/String;)I

    move-result v4

    const-string v5, "goal_subtype"

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getInt(Ljava/lang/String;)I

    move-result v5

    const-string/jumbo v6, "period"

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getInt(Ljava/lang/String;)I

    move-result v6

    const-string/jumbo v7, "set_time"

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    const-string v9, "_id"

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v9

    const-string v11, "create_time"

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v11

    const-string/jumbo v13, "update_time"

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v13

    const-string/jumbo v15, "time_zone"

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getInt(Ljava/lang/String;)I

    move-result v15

    const-string v16, "daylight_saving"

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getInt(Ljava/lang/String;)I

    move-result v16

    invoke-direct/range {v2 .. v16}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;-><init>(FIIIJJJJII)V

    return-object v2
.end method

.method public getLatestGoal(I)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    .locals 5
    .param p1, "goalType"    # I

    .prologue
    .line 47
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    new-instance v1, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string v2, "goal_type"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "="

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;-><init>(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)V

    .line 49
    .local v0, "filter":Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;->getTimeColumnName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;->DESC:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;->getLastDataWithFilter(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Ljava/lang/String;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    return-object v1
.end method
