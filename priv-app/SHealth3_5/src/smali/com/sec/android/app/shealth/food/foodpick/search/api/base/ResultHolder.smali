.class public final Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
.super Ljava/lang/Object;
.source "ResultHolder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private mErrorMessage:Ljava/lang/String;

.field private mSearchResult:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private mTaskResult:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 28
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder<TE;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    return-void
.end method

.method public static createErrorResult(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .locals 3
    .param p0, "errorTaskResult"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;
    .param p1, "errorMessage"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;",
            ">(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;",
            "Ljava/lang/String;",
            ")",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 60
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 61
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "errorMessage shouldn\'t be empty!"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 63
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;->isCompleted:Z

    if-eqz v1, :cond_1

    .line 64
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "errorTaskResult shouldn\'t be success result"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 66
    :cond_1
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;-><init>()V

    .line 67
    .local v0, "holder":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder<TE;>;"
    iput-object p1, v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;->mErrorMessage:Ljava/lang/String;

    .line 68
    iput-object p0, v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;->mTaskResult:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    .line 69
    return-object v0
.end method

.method public static createSuccessfulResult(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;",
            ">(TE;)",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 41
    .local p0, "searchResult":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;, "TE;"
    if-nez p0, :cond_0

    .line 42
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "searchResult shouldn\'t be empty!"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 44
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;-><init>()V

    .line 45
    .local v0, "holder":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder<TE;>;"
    iput-object p0, v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;->mSearchResult:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;

    .line 46
    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;->SUCCESS:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    iput-object v1, v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;->mTaskResult:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    .line 47
    return-object v0
.end method


# virtual methods
.method public getErrorMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder<TE;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;->mErrorMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getResultContents()Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 98
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder<TE;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;->mSearchResult:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;

    return-object v0
.end method

.method public getTaskResult()Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;
    .locals 1

    .prologue
    .line 77
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder<TE;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;->mTaskResult:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    return-object v0
.end method

.method public isSuccessful()Z
    .locals 2

    .prologue
    .line 84
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder<TE;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;->mTaskResult:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;->SUCCESS:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
