.class public interface abstract Lcom/sec/android/app/shealth/framework/ui/common/BundleKeys;
.super Ljava/lang/Object;


# static fields
.field public static final ABOUT_MODE:Ljava/lang/String; = "about_mode"

.field public static final ACTIVITY_FROM_ROTATE:Ljava/lang/String; = "activity_from_rotate"

.field public static final ACTIVITY_TO_START:Ljava/lang/String; = "activity_to_start"

.field public static final ACTIVITY_TO_START_FLAG:Ljava/lang/String; = "activity_to_start_flag"

.field public static final ANIMATION_TYPE:Ljava/lang/String; = "need_to_animate_appearance"

.field public static final ANT_DEVICE_DIALOG:Ljava/lang/String; = "ant_device_dialog"

.field public static final ANT_DEVICE_ISCONNECTED:Ljava/lang/String; = "ant_device_isconnected"

.field public static final ANT_DEVICE_NUMBER:Ljava/lang/String; = "ant_device_number"

.field public static final ANT_DEVICE_TYPE:Ljava/lang/String; = "ant_device_type"

.field public static final ANT_DEVICE_USER_NAME:Ljava/lang/String; = "ant_device_user_name"

.field public static final ANT_DEVICE_UUID:Ljava/lang/String; = "ant_device_uuid"

.field public static final BASIC_BIRTHDAY_DAY:Ljava/lang/String; = "basic_birthday_day"

.field public static final BASIC_BIRTHDAY_MONTH:Ljava/lang/String; = "basic_birthday_month"

.field public static final BASIC_BIRTHDAY_YEAR:Ljava/lang/String; = "basic_birthday_year"

.field public static final BASIC_GENDER:Ljava/lang/String; = "basic_gender"

.field public static final BASIC_HEIGHT:Ljava/lang/String; = "basic_height"

.field public static final BASIC_NAME:Ljava/lang/String; = "basic_name"

.field public static final BASIC_WEIGHT:Ljava/lang/String; = "basic_weight"

.field public static final BLUETOOTH_DEVICE_INIT:Ljava/lang/String; = "init"

.field public static final BLUETOOTH_DEVICE_UUID:Ljava/lang/String; = "ant_device_uuid"

.field public static final BT_DEVICE_ID:Ljava/lang/String; = "bt_device_id"

.field public static final BT_DEVICE_NAME:Ljava/lang/String; = "bt_device_name"

.field public static final BT_DEVICE_TYPE:Ljava/lang/String; = "bt_device_type"

.field public static final BT_DEVICE_USER_NAME:Ljava/lang/String; = "bt_device_user_name"

.field public static final BT_IS_HRM:Ljava/lang/String; = "bt_is_hrm"

.field public static final BT_RECEIVED_MSG_TYPE:Ljava/lang/String; = "bt_received_msg_type"

.field public static final BT_REGISTER_RECEIVER:Ljava/lang/String; = "bt_register_receiver"

.field public static final BUNDLE_KEY_FROM_POPUP:Ljava/lang/String; = "from_popup"

.field public static final BUNDLE_KEY_HUMIDITY_HIGH:Ljava/lang/String; = "HUMIDITY_HIGH"

.field public static final BUNDLE_KEY_HUMIDITY_LOW:Ljava/lang/String; = "HUMIDITY_LOW"

.field public static final BUNDLE_KEY_TEMP_HIGH:Ljava/lang/String; = "TEMP_HIGH"

.field public static final BUNDLE_KEY_TEMP_LOW:Ljava/lang/String; = "TEMP_LOW"

.field public static final BURNED_CALORIES_DIALOG_CHOICE:Ljava/lang/String; = "burned_calories_choice"

.field public static final CALENDAR_RESULT:Ljava/lang/String; = "calendar_result"

.field public static final CAME_FROM_NEXT:Ljava/lang/String; = "came_from_next"

.field public static final CHANGE_PIN:Ljava/lang/String; = "change_pin"

.field public static final CHECKED_ITEMS:Ljava/lang/String; = "checked_items"

.field public static final CURRENT_FRAGMENT_INDEX_KEY:Ljava/lang/String; = "current_fragment_index_key"

.field public static final CURRENT_TIME:Ljava/lang/String; = "current_time"

.field public static final CURRENT_WEIGHT_VALUE:Ljava/lang/String; = "current_weight_value"

.field public static final DATA_TYPE_KEY:Ljava/lang/String; = "data_type"

.field public static final DEC_BUTTON_STATE:Ljava/lang/String; = "dec_button_state"

.field public static final DEVICE_TYPE:Ljava/lang/String; = "device_type"

.field public static final EDIT_MODE:Ljava/lang/String; = "edit"

.field public static final EXERCISEPRO_RECODING_MODE:Ljava/lang/String; = "exercisepro_recoding_mode"

.field public static final EXTENDED_FOOD_INFO:Ljava/lang/String; = "extended_food_info"

.field public static final EXTENDED_FOOD_INFO_ID:Ljava/lang/String; = "extended_food_info_id"

.field public static final FASTING_ITEMS:Ljava/lang/String; = "fasting_items"

.field public static final FLIPPER_DATA_TYPE:Ljava/lang/String; = "flipper_data_type"

.field public static final FRAGMENT_TYPE_KEY:Ljava/lang/String; = "fragment_type"

.field public static final FRAGMENT_TYPE_MODE:Ljava/lang/String; = "fragment_type"

.field public static final FROM_EXERCISE_OR_FOOD:Ljava/lang/String; = "from_exercise_or_food"

.field public static final FROM_NOTIFICATION:Ljava/lang/String; = "from_notification"

.field public static final FROM_WEIGHT:Ljava/lang/String; = "from_weight"

.field public static final GOAL_ACHIEVE:Ljava/lang/String; = "goal_achieve"

.field public static final GOAL_EDIT_MODE:Ljava/lang/String; = "goal_edit_moe"

.field public static final GOAL_EXERCISE:Ljava/lang/String; = "goal_exercise"

.field public static final GOAL_FOOD:Ljava/lang/String; = "goal_food"

.field public static final GOAL_WEIGHT:Ljava/lang/String; = "goal_weight"

.field public static final HOUR_VALUE:Ljava/lang/String; = "hour_value"

.field public static final HOUR_VALUE_FOCUSED:Ljava/lang/String; = "hour_value_focused"

.field public static final ID_KEY:Ljava/lang/String; = "id_mode"

.field public static final INC_BUTTON_STATE:Ljava/lang/String; = "inc_button_state"

.field public static final INPUT_MEASURE_SELECTED_DATE_IN_LONG_KEY:Ljava/lang/String; = "SelectedDateTimeInLong"

.field public static final IS_FROM_SELECT_BUTTON:Ljava/lang/String; = "is_from_select_button"

.field public static final LAUNCHED_FROM_APPWIDGET:Ljava/lang/String; = "launched_from_appwidget"

.field public static final LIST_ITEM:Ljava/lang/String; = "list_item"

.field public static final LIST_ITEMS:Ljava/lang/String; = "list_items"

.field public static final MEAL_ID:Ljava/lang/String; = "meal"

.field public static final MEAL_ITEMS_KEY:Ljava/lang/String; = "meal_items"

.field public static final MEAL_KEY:Ljava/lang/String; = "meal"

.field public static final MEAL_TYPE:Ljava/lang/String; = "food_add_food_id"

.field public static final MEMO:Ljava/lang/String; = "memo"

.field public static final MINUTE_VALUE:Ljava/lang/String; = "minutes_value"

.field public static final MINUTE_VALUE_FOCUSED:Ljava/lang/String; = "minutes_value_focused"

.field public static final MODE_KEY:Ljava/lang/String; = "mode"

.field public static final NAVIGATION_BAR_ENABLED:Ljava/lang/String; = "navigation_bar_enabled"

.field public static final NAVIGATION_BAR_HIDE_ON_START:Ljava/lang/String; = "navigation_bar_hide_on_start"

.field public static final NAVIGATION_BAR_SCROLL:Ljava/lang/String; = "navigation_bar_scroll"

.field public static final NAVIGATION_BAR_SELECTED_ID:Ljava/lang/String; = "navigation_bar_selected_id"

.field public static final NAVIGATION_BAR_SELECTED_ID_EXTERNAL_ACCESS:Ljava/lang/String; = "navigation_bar_selected_id_external_access"

.field public static final NAVIGATION_BAR_SHOWN:Ljava/lang/String; = "navigation_bar_shown"

.field public static final PATH_TO_LOADED_URL:Ljava/lang/String; = "path_to_loaded_url"

.field public static final PEDOMETER_SENSOR_IS_AVAILABLE:Ljava/lang/String; = "pedometer_sensor_available"

.field public static final PERIOD_START_KEY:Ljava/lang/String; = "period_start"

.field public static final PERIOD_TYPE_KEY:Ljava/lang/String; = "period_type"

.field public static final PHOTO_DIALOG:Ljava/lang/String; = "Photo_dialog"

.field public static final PICK_CALORIES:Ljava/lang/String; = "pick_calories"

.field public static final PICK_QUANTITY:Ljava/lang/String; = "pick_quantity"

.field public static final PICK_RESULT:Ljava/lang/String; = "pick_result"

.field public static final PICK_TYPE:Ljava/lang/String; = "pick_type"

.field public static final PICK_UNIT:Ljava/lang/String; = "pick_unit"

.field public static final PULSE_CHECKBOX:Ljava/lang/String; = "pulse_checkbox"

.field public static final REALTIME_DURING_TRY_IT:Ljava/lang/String; = "realtime_during_try_it"

.field public static final REALTIME_SYNC_ACTIVITY_TYPE:Ljava/lang/String; = "realtime_sync_activity_type"

.field public static final REALTIME_SYNC_AVG_HRM:Ljava/lang/String; = "realtime_sync_avg_hrm"

.field public static final REALTIME_SYNC_AVG_SPEED:Ljava/lang/String; = "realtime_sync_avg_speed"

.field public static final REALTIME_SYNC_CALORIES:Ljava/lang/String; = "realtime_sync_calories"

.field public static final REALTIME_SYNC_DISTANCE:Ljava/lang/String; = "realtime_sync_distance"

.field public static final REALTIME_SYNC_DURATION:Ljava/lang/String; = "realtime_sync_duration"

.field public static final REALTIME_SYNC_ELEVATION:Ljava/lang/String; = "realtime_sync_elevation"

.field public static final REALTIME_SYNC_EXERCISEINFO_DB_ID:Ljava/lang/String; = "realtime_sync_exercise_info_db_id"

.field public static final REALTIME_SYNC_MAX_ELEVATION:Ljava/lang/String; = "realtime_sync_max_elevation"

.field public static final REALTIME_SYNC_MAX_HRM:Ljava/lang/String; = "realtime_sync_max_hrm"

.field public static final REALTIME_SYNC_MAX_SPEED:Ljava/lang/String; = "realtime_sync_max_speed"

.field public static final REALTIME_SYNC_MIN_ELEVATION:Ljava/lang/String; = "realtime_sync_min_elevation"

.field public static final REALTIME_SYNC_PLACE_MODE:Ljava/lang/String; = "realtime_sync_mode"

.field public static final REALTIME_SYNC_TIME:Ljava/lang/String; = "realtime_sync_time"

.field public static final REALTIME_WORKOUT_END_MODE:Ljava/lang/String; = "realtime_workout_end_mode"

.field public static final SET_PIN:Ljava/lang/String; = "set_pin"

.field public static final START_EDIT_MODE:Ljava/lang/String; = "start edit mode"

.field public static final START_TIME_FOR_HELPER:Ljava/lang/String; = "start_time_for_helper"

.field public static final S_BAND_PAIRING_FOR_PEDOMETER_SYNC:Ljava/lang/String; = "s_band_pairing_for_pedometer_sync"

.field public static final S_BAND_PAIRING_FOR_SLEEP_MONITOR_SYNC:Ljava/lang/String; = "s_band_pairing_for_sleep_monitor_sync"

.field public static final S_BAND_SYNC_DATA_TAG:Ljava/lang/String; = "s_band_sync_data"

.field public static final S_HRM_SEARCH_TIME_MY_ACCESSORIES:Ljava/lang/String; = "s_hrm_search_time_my_accessories"

.field public static final S_HRM_SEARCH_TIME_ONLY:Ljava/lang/String; = "s_hrm_search_time_only"

.field public static final S_HRM_SEARCH_TIME_REALTIME:Ljava/lang/String; = "s_hrm_search_time_realtime"

.field public static final TIME_COLUMN_NAME_KEY:Ljava/lang/String; = "column_name"

.field public static final TIME_PERIOD:Ljava/lang/String; = "time_period"

.field public static final TIP_ARTICLE_METADATA:Ljava/lang/String; = "article_metadata"

.field public static final TIP_DETAILS_LIST:Ljava/lang/String; = "tip_details_list"

.field public static final WALKINGMATE_RECODING_MODE:Ljava/lang/String; = "walkingmate_recoding_mode"

.field public static final WALK_FOR_LIFE_GOAL_TAG:Ljava/lang/String; = "walk_for_life_goal"

.field public static final WEIGHT_LOSS_GOAL:Ljava/lang/String; = "weight_loss_goal"
