.class Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$4;
.super Ljava/lang/Thread;
.source "WalkingMateDayStepService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->onStartCommand(Landroid/content/Intent;II)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V
    .locals 0

    .prologue
    .line 380
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 382
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v0

    if-nez v0, :cond_2

    .line 383
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 384
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->prepareWidgetInCaseOfPasswordLocked()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$500(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V

    .line 388
    :goto_0
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->notifiedContentProviderAccessible()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isInitializationNeeded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 390
    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setNotifiedContentProviderAccessible(Z)V

    .line 391
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "security_pin_enabled"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 392
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->sendNotification()V

    .line 401
    :cond_0
    :goto_1
    return-void

    .line 386
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->prepareWidgetInCaseOfNoProfile(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$600(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;Z)V

    goto :goto_0

    .line 398
    :cond_2
    const-string v0, "WalkingMateDayStepService"

    const-string v1, "CP is accessible"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->initializeService()I
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$700(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)I

    .line 400
    const-string v0, "WalkingMateDayStepService"

    const-string v1, "initializeService is completed"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
