.class Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;
.super Ljava/lang/Object;
.source "SpO2SummaryFragmentNew.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->measureFinishAnimationMedical()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V
    .locals 0

    .prologue
    .line 1010
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const v2, 0x3f28f5c3    # 0.66f

    const/high16 v6, 0x3f000000    # 0.5f

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    .line 1013
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;
    invoke-static {v3}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$3300(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1014
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;
    invoke-static {v3}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$3300(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    move-result-object v3

    invoke-virtual {v3, v11}, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->stopAnimation(Z)V

    .line 1016
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondIcon:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$1100(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/ImageView;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1017
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$1000(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/ImageView;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1018
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$1200(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/RelativeLayout;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1019
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    move v3, v1

    move v4, v2

    move v7, v5

    move v8, v6

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 1021
    .local v0, "scaleAnimationForMedical":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x104

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1022
    new-instance v10, Landroid/view/animation/AlphaAnimation;

    const/4 v2, 0x0

    invoke-direct {v10, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1023
    .local v10, "opacityAnimation":Landroid/view/animation/Animation;
    const-wide/16 v1, 0x104

    invoke-virtual {v10, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1024
    new-instance v9, Landroid/view/animation/AnimationSet;

    invoke-direct {v9, v5}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 1025
    .local v9, "animationSet":Landroid/view/animation/AnimationSet;
    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1026
    invoke-virtual {v9, v10}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1027
    invoke-virtual {v9, v11}, Landroid/view/animation/AnimationSet;->setFillAfter(Z)V

    .line 1028
    invoke-virtual {v9, v5}, Landroid/view/animation/AnimationSet;->setFillBefore(Z)V

    .line 1029
    new-instance v1, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12$1;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;)V

    invoke-virtual {v9, v1}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1052
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBig:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$3400(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/ImageView;

    move-result-object v1

    const v2, 0x7f0201d3

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1054
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$1200(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/RelativeLayout;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1055
    return-void
.end method
