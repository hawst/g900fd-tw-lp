.class public Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "WalkingMateReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver$WalkingMateEventProcessor;
    }
.end annotation


# static fields
.field public static final CONTENT_PROVIDER_ACCESSIBLE:Ljava/lang/String; = "com.sec.android.service.health.ContentProviderAccessible"

.field public static final SYNC_AND_START:Ljava/lang/String; = "com.sec.android.intent.action.WEARABLE_DEVICE_SYNC_AND_START"

.field public static final SYNC_CHECK:Ljava/lang/String; = "android.intent.action.WEARABLE_DEVICE_SYNC_CHECK"

.field public static final SYNC_START:Ljava/lang/String; = "android.intent.action.WEARABLE_DEVICE_SYNC"

.field private static final TAG:Ljava/lang/String;

.field private static alrmTimeinMilisec:J

.field private static final sLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    const-class v0, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->TAG:Ljava/lang/String;

    .line 36
    const-wide/16 v0, 0x7530

    sput-wide v0, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->alrmTimeinMilisec:J

    .line 44
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->sLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 47
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->sLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$200(Landroid/content/Context;Landroid/content/ComponentName;Z)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Landroid/content/ComponentName;
    .param p2, "x2"    # Z

    .prologue
    .line 32
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->enableWidgets(Landroid/content/Context;Landroid/content/ComponentName;Z)V

    return-void
.end method

.method static synthetic access$300()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 32
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->getHomeScreenWidgetPendingIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static enableWidgets(Landroid/content/Context;Landroid/content/ComponentName;Z)V
    .locals 4
    .param p0, "appContext"    # Landroid/content/Context;
    .param p1, "widgetName"    # Landroid/content/ComponentName;
    .param p2, "shouldBeEnabled"    # Z

    .prologue
    const/4 v0, 0x1

    .line 214
    if-eqz p2, :cond_0

    .line 217
    .local v0, "componentFutureState":I
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, p1, v0, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 222
    :goto_1
    return-void

    .line 214
    .end local v0    # "componentFutureState":I
    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    .line 219
    .restart local v0    # "componentFutureState":I
    :catch_0
    move-exception v1

    .line 220
    .local v1, "iae":Ljava/lang/IllegalArgumentException;
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private static getHomeScreenWidgetPendingIntent()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 225
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 226
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.shealth.broadcast.LAUNCH_APP_WIDGET_ACTIVITY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 227
    const-string/jumbo v1, "widgetActivityAction"

    const-string v2, "com.sec.shealth.action.PEDOMETER"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 228
    const-string/jumbo v1, "widgetActivityPackage"

    const-string v2, "com.sec.android.app.shealth"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 229
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth.walkingmate"

    const-string v3, "W030"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    return-object v0
.end method

.method public static final isSHealthMainAppinstalled()Z
    .locals 7

    .prologue
    .line 200
    const/4 v3, 0x0

    .line 201
    .local v3, "rtn":Z
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v2

    .line 202
    .local v2, "packages":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    .line 203
    .local v0, "a":Landroid/content/pm/PackageInfo;
    iget-object v4, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const-string v5, "com.sec.android.app.shealth"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 204
    const/4 v3, 0x1

    .line 209
    .end local v0    # "a":Landroid/content/pm/PackageInfo;
    :cond_1
    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isSHealthMainAppinstalled() : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    return v3
.end method

.method public static startAlarm(Landroid/content/Context;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 154
    const-string v1, "alarm"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 155
    .local v0, "am":Landroid/app/AlarmManager;
    if-eqz v0, :cond_0

    .line 156
    new-instance v7, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;

    invoke-direct {v7, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 157
    .local v7, "i":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.shealth.walkingmate.service.START_ALARM"

    invoke-virtual {v7, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 158
    invoke-static {p0, v2, v7, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 159
    .local v6, "pi":Landroid/app/PendingIntent;
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Inside startAlarm before starting Alarm with time:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-wide v3, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->alrmTimeinMilisec:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    const/4 v1, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sget-wide v4, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->alrmTimeinMilisec:J

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    .line 162
    .end local v6    # "pi":Landroid/app/PendingIntent;
    .end local v7    # "i":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public static stopAlarm(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 165
    const-string v3, "alarm"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 166
    .local v0, "am":Landroid/app/AlarmManager;
    if-eqz v0, :cond_0

    .line 167
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 168
    .local v1, "i":Landroid/content/Intent;
    const-string v3, "com.sec.android.app.shealth.walkingmate.service.START_ALARM"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 169
    invoke-static {p0, v4, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 170
    .local v2, "pi":Landroid/app/PendingIntent;
    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->TAG:Ljava/lang/String;

    const-string v4, "Inside stopAlarm before cancelling Alarm"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 173
    .end local v1    # "i":Landroid/content/Intent;
    .end local v2    # "pi":Landroid/app/PendingIntent;
    :cond_0
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "arg1"    # Landroid/content/Intent;

    .prologue
    .line 178
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onReceive : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    invoke-static {p1}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeHelper;->isSHealthUpgradeNeeded(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 183
    const-string v1, "WalkingMateReceiver"

    const-string v2, "isUpgradeNeeded = true"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    :goto_0
    return-void

    .line 187
    :cond_0
    invoke-static {p1}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeHelper;->isHealthServiceUpgradeNeeded(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 189
    const-string v1, "WalkingMateReceiver"

    const-string v2, "isHealthServiceOld = true"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 193
    :cond_1
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onReceive() - Start"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver$WalkingMateEventProcessor;

    invoke-direct {v0, p2}, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver$WalkingMateEventProcessor;-><init>(Landroid/content/Intent;)V

    .line 195
    .local v0, "bcThread":Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver$WalkingMateEventProcessor;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver$WalkingMateEventProcessor;->start()V

    .line 196
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onReceive() - End"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
