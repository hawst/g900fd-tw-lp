.class public Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "PrivacyPolicy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$ProfileFinishedReceiver;
    }
.end annotation


# static fields
.field private static FROM_SETTINGS:Ljava/lang/String;

.field private static isFromSettings:Z


# instance fields
.field private isAgreeCheck:Z

.field private mAgreeCheck:Landroid/widget/CheckBox;

.field private mCheckLayout:Landroid/widget/RelativeLayout;

.field private mNextImage:Landroid/widget/ImageView;

.field private mNextText:Landroid/widget/TextView;

.field private mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$ProfileFinishedReceiver;

.field private mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

.field private mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

.field private mSipIntentFilter:Landroid/content/IntentFilter;

.field private nextButtonLayout:Landroid/widget/RelativeLayout;

.field private ppDeletion2:Landroid/widget/TextView;

.field private ppDeletion2open:Landroid/widget/TextView;

.field private ppDeletion3:Landroid/widget/TextView;

.field private ppDeletion3open:Landroid/widget/TextView;

.field private ppDeletion4:Landroid/widget/TextView;

.field private ppDeletion4open:Landroid/widget/TextView;

.field private ppKorSub1Text2:Landroid/widget/TextView;

.field private ppKorSub2Text3:Landroid/widget/TextView;

.field private ppParagraph1:Landroid/widget/TextView;

.field private ppParagraph1open:Landroid/widget/TextView;

.field private ppParagraph2:Landroid/widget/TextView;

.field private ppParagraph2open:Landroid/widget/TextView;

.field private ppParagraph3:Landroid/widget/TextView;

.field private ppParagraph3_2:Landroid/widget/TextView;

.field private ppParagraph3open:Landroid/widget/TextView;

.field private ppParagraph3open2:Landroid/widget/TextView;

.field private ppParagraph4:Landroid/widget/TextView;

.field private ppParagraph4open:Landroid/widget/TextView;

.field private ppParagraph5:Landroid/widget/TextView;

.field private ppParagraph5open:Landroid/widget/TextView;

.field private ppParagraph6:Landroid/widget/TextView;

.field private ppParagraph6open:Landroid/widget/TextView;

.field private ppSpecialText:Landroid/widget/TextView;

.field private ppSpecialTextOpen:Landroid/widget/TextView;

.field private ppSpecialTitle:Landroid/widget/TextView;

.field private ppSpecialTitleOpen:Landroid/widget/TextView;

.field private titleEraseForInitial:Landroid/widget/TextView;

.field private titleEraseForSettings:Landroid/widget/TextView;

.field private titlePPForInitial:Landroid/widget/TextView;

.field private titlePPForSettings:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const-string v0, "fromSettings"

    sput-object v0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->FROM_SETTINGS:Ljava/lang/String;

    .line 69
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->isFromSettings:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 47
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mCheckLayout:Landroid/widget/RelativeLayout;

    .line 48
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mAgreeCheck:Landroid/widget/CheckBox;

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->isAgreeCheck:Z

    .line 58
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppKorSub2Text3:Landroid/widget/TextView;

    .line 62
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mNextText:Landroid/widget/TextView;

    .line 63
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mNextImage:Landroid/widget/ImageView;

    .line 900
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mAgreeCheck:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->isAgreeCheck:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->isAgreeCheck:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->enableNextBtn(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;)Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;)Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    return-object v0
.end method

.method private applyReviewedText()V
    .locals 12

    .prologue
    const v11, 0x7f090763

    const v10, 0x7f090762

    const v9, 0x7f090e52

    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 324
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget-object v3, v4, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 325
    .local v3, "systemLocale":Ljava/util/Locale;
    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 326
    .local v2, "strLanguage":Ljava/lang/String;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    .line 328
    .local v1, "countryName":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 330
    .local v0, "builder":Ljava/lang/StringBuilder;
    if-eqz v2, :cond_0

    const-string v4, "kk"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 331
    const v4, 0x7f090e6e

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 332
    const-string v4, "\n\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 334
    :cond_0
    const v4, 0x7f090e6f

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 335
    const-string v4, "\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 336
    const v4, 0x7f090e70

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337
    const-string v4, "\n\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 339
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ZA"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_17

    if-eqz v2, :cond_17

    const-string v4, "en"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 341
    const v4, 0x7f090ee2

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 367
    :goto_0
    const v4, 0x7f0806ae

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 368
    const v4, 0x7f080694

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 370
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph2:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->makeParagraph2Text()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 371
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph2open:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->makeParagraph2Text()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 373
    const v4, 0x7f0806b0

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f090e74

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e75

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e76

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e77

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e78

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e79

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e7a

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e7b

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 383
    const v4, 0x7f080696

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f090e74

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e75

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e76

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e77

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e78

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e79

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e7a

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e7b

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 397
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ES"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 399
    if-eqz v2, :cond_1

    const-string v4, "gl"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "ca"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "eu"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    :cond_1
    const-string v4, "es"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "ES"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 402
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTitle:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 403
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialText:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 404
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTitleOpen:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 405
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTextOpen:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 410
    :cond_3
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "CA"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 412
    if-eqz v2, :cond_1d

    const-string v4, "fr"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1d

    const-string v4, "CA"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1d

    .line 414
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTitle:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 415
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialText:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 416
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTitleOpen:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 417
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTextOpen:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 470
    :cond_4
    :goto_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "DE"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 472
    if-eqz v2, :cond_5

    const-string v4, "de"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 474
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph1:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090e4e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 475
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph3:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090e50

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 476
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph3_2:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090e51

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 477
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph4:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 478
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph5:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090e53

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 480
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph1open:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090e4e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 481
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph3open:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090e50

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 482
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph3open2:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090e51

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 483
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph4open:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 484
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph5open:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090e53

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 486
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph6:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 487
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph6open:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 492
    :cond_5
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "CH"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 494
    if-eqz v2, :cond_6

    const-string v4, "de"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 496
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph4:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 497
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph4open:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 499
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph6:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 500
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph6open:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 505
    :cond_6
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "AT"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 507
    if-eqz v2, :cond_7

    const-string v4, "de"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 509
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph1:Landroid/widget/TextView;

    const v5, 0x7f090ece

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 510
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph1open:Landroid/widget/TextView;

    const v5, 0x7f090ece

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 515
    :cond_7
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "AU"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 517
    if-eqz v2, :cond_8

    const-string v4, "en"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 519
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->titlePPForInitial:Landroid/widget/TextView;

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setText(I)V

    .line 520
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph1:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v11}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090764

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090765

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090766

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090767

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "</b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->makeParagraph2Text()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09076a

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "</b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09076b

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09076c

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "</b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09076d

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09076e

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09076f

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "</b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090770

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090771

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "</b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090772

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090773

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090774

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090775

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090776

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "</b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090777

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090778

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090779

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09077a

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09077b

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09077c

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09077d

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09077e

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09077f

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "</b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090780

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090781

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090782

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090783

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 556
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph2:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 557
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph3:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 558
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph3_2:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 559
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->titleEraseForInitial:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 560
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppDeletion2:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 561
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppDeletion3:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 562
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppDeletion4:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 563
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph6:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 564
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph4:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 565
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph5:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 567
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->titlePPForSettings:Landroid/widget/TextView;

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setText(I)V

    .line 568
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph1open:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v11}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090764

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090765

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090766

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090767

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "</b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->makeParagraph2Text()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09076a

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "</b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09076b

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09076c

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "</b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09076d

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09076e

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09076f

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "</b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090770

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090771

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "</b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090772

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090773

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090774

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090775

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090776

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "</b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090777

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090778

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090779

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09077a

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09077b

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09077c

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09077d

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09077e

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09077f

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "</b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090780

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090781

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090782

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090783

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<br/><br/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 604
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph2open:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 605
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph3open:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 606
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph3open2:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 607
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->titleEraseForSettings:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 608
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppDeletion2open:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 609
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppDeletion3open:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 610
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppDeletion4open:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 611
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph6open:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 612
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph4open:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 613
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph5open:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 618
    :cond_8
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "MA"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 620
    if-eqz v2, :cond_9

    const-string v4, "fr"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 622
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph1:Landroid/widget/TextView;

    const v5, 0x7f090ebb

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 623
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph1open:Landroid/widget/TextView;

    const v5, 0x7f090ebb

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 624
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph3:Landroid/widget/TextView;

    const v5, 0x7f090ebc

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 625
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph3open:Landroid/widget/TextView;

    const v5, 0x7f090ebc

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 630
    :cond_9
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "AL"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 632
    if-eqz v2, :cond_a

    const-string/jumbo v4, "sq"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 634
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph1:Landroid/widget/TextView;

    const v5, 0x7f090ed0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 635
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph1open:Landroid/widget/TextView;

    const v5, 0x7f090ed0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 640
    :cond_a
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ME"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 642
    if-eqz v2, :cond_b

    const-string/jumbo v4, "sr"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 644
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph1:Landroid/widget/TextView;

    const v5, 0x7f090ed7

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 645
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph1open:Landroid/widget/TextView;

    const v5, 0x7f090ed7

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 646
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph3:Landroid/widget/TextView;

    const v5, 0x7f090ed8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 647
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph3open:Landroid/widget/TextView;

    const v5, 0x7f090ed8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 652
    :cond_b
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "MX"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 654
    if-eqz v2, :cond_c

    const-string v4, "es"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    const-string v4, "US"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 656
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph1:Landroid/widget/TextView;

    const v5, 0x7f090eaa

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 657
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph1open:Landroid/widget/TextView;

    const v5, 0x7f090eaa

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 658
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTitle:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 659
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialText:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 660
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTitleOpen:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 661
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTextOpen:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 666
    :cond_c
    if-eqz v2, :cond_d

    const-string/jumbo v4, "ur"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_e

    :cond_d
    const-string v4, "cs"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 668
    :cond_e
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTitle:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 669
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialText:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 670
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTitleOpen:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 671
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTextOpen:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 675
    :cond_f
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "RU"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 677
    if-eqz v2, :cond_10

    const-string v4, "kk"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 679
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTitle:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 680
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialText:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 681
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTitleOpen:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 682
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTextOpen:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 684
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialText:Landroid/widget/TextView;

    const v5, 0x7f090eb7

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 685
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTextOpen:Landroid/widget/TextView;

    const v5, 0x7f090eb7

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 690
    :cond_10
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "KZ"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 692
    if-eqz v2, :cond_11

    const-string v4, "kk"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 694
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTitle:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 695
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialText:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 696
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTitleOpen:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 697
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTextOpen:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 702
    :cond_11
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "FR"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 704
    if-eqz v2, :cond_12

    const-string v4, "fr"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 706
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph1:Landroid/widget/TextView;

    const v5, 0x7f090eda

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 707
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph1open:Landroid/widget/TextView;

    const v5, 0x7f090eda

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 708
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph3:Landroid/widget/TextView;

    const v5, 0x7f090edb

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 709
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph3open:Landroid/widget/TextView;

    const v5, 0x7f090edb

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 711
    const-string v4, "CA"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 713
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTitle:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 714
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialText:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 715
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTitleOpen:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 716
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTextOpen:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 718
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialText:Landroid/widget/TextView;

    const v5, 0x7f090edd

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 719
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTextOpen:Landroid/widget/TextView;

    const v5, 0x7f090edd

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 725
    :cond_12
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "CD"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 727
    if-eqz v2, :cond_13

    const-string v4, "fr"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 729
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph1:Landroid/widget/TextView;

    const v5, 0x7f090ee4

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 730
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph1open:Landroid/widget/TextView;

    const v5, 0x7f090ee4

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 735
    :cond_13
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "PT"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 737
    if-eqz v2, :cond_14

    const-string/jumbo v4, "pt"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 739
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph1:Landroid/widget/TextView;

    const v5, 0x7f090ee7

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 740
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph1open:Landroid/widget/TextView;

    const v5, 0x7f090ee7

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 745
    :cond_14
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ZA"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 747
    if-eqz v2, :cond_15

    const-string v4, "en"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 749
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph1:Landroid/widget/TextView;

    const v5, 0x7f090edf

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 750
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph1open:Landroid/widget/TextView;

    const v5, 0x7f090edf

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 752
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTitle:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 753
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialText:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 754
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTitleOpen:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 755
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTextOpen:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 756
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialText:Landroid/widget/TextView;

    const v5, 0x7f090ee0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 757
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTextOpen:Landroid/widget/TextView;

    const v5, 0x7f090ee0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 762
    :cond_15
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "BA"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_16

    .line 764
    if-eqz v2, :cond_16

    const-string v4, "hr"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_16

    .line 766
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph1:Landroid/widget/TextView;

    const v5, 0x7f090ed3

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 767
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph1open:Landroid/widget/TextView;

    const v5, 0x7f090ed3

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 768
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph3:Landroid/widget/TextView;

    const v5, 0x7f090ed4

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 769
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph3open:Landroid/widget/TextView;

    const v5, 0x7f090ed4

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 772
    :cond_16
    return-void

    .line 343
    :cond_17
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "AL"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_18

    if-eqz v2, :cond_18

    const-string/jumbo v4, "sq"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 345
    const v4, 0x7f090ed1

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 347
    :cond_18
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "BA"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_19

    if-eqz v2, :cond_19

    const-string v4, "hr"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_19

    .line 349
    const v4, 0x7f090ed5

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 351
    :cond_19
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "FR"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1a

    if-eqz v2, :cond_1a

    const-string v4, "fr"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1a

    .line 353
    const v4, 0x7f090edc

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 355
    :cond_1a
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ME"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1b

    if-eqz v2, :cond_1b

    const-string/jumbo v4, "sr"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1b

    .line 357
    const v4, 0x7f090ed9

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 359
    :cond_1b
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ES"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1c

    if-eqz v2, :cond_1c

    const-string v4, "es"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 361
    const v4, 0x7f090eac

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 364
    :cond_1c
    const v4, 0x7f090e72

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 419
    :cond_1d
    if-eqz v2, :cond_4

    const-string v4, "en"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 421
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph1:Landroid/widget/TextView;

    const v5, 0x7f090ef5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 422
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph3:Landroid/widget/TextView;

    const v5, 0x7f090ef7

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 423
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph3_2:Landroid/widget/TextView;

    const v5, 0x7f090ef8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 424
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppDeletion2:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f090ef9

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090efa

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090efb

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090efc

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 430
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppDeletion4:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f090efd

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090efe

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090eff

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090f00

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090f01

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090f02

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090f03

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090f04

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 440
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph6:Landroid/widget/TextView;

    const v5, 0x7f090f05

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 441
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph4:Landroid/widget/TextView;

    const v5, 0x7f090f06

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 442
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph5:Landroid/widget/TextView;

    const v5, 0x7f090f07

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 444
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph1open:Landroid/widget/TextView;

    const v5, 0x7f090ef5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 445
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph3open:Landroid/widget/TextView;

    const v5, 0x7f090ef7

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 446
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph3open2:Landroid/widget/TextView;

    const v5, 0x7f090ef8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 447
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppDeletion2open:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f090ef9

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090efa

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090efb

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090efc

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 453
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppDeletion4open:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f090efd

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090efe

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090eff

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090f00

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090f01

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090f02

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090f03

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090f04

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 463
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph6open:Landroid/widget/TextView;

    const v5, 0x7f090f05

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 464
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph4open:Landroid/widget/TextView;

    const v5, 0x7f090f06

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 465
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph5open:Landroid/widget/TextView;

    const v5, 0x7f090f07

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1
.end method

.method private enableNextBtn(Z)V
    .locals 4
    .param p1, "isEnable"    # Z

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const v2, 0x3ecccccd    # 0.4f

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mNextText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mNextImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 317
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mNextText:Landroid/widget/TextView;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setAlpha(F)V

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mNextImage:Landroid/widget/ImageView;

    if-eqz p1, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 320
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 317
    goto :goto_0

    :cond_2
    move v1, v2

    .line 318
    goto :goto_1
.end method

.method private initLayout()V
    .locals 11

    .prologue
    const v10, 0x7f08069c

    const v9, 0x7f08068d

    const v8, 0x7f08068c

    const/4 v7, 0x0

    const/16 v6, 0x8

    .line 101
    const v3, 0x7f08068b

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 103
    .local v0, "base":Landroid/widget/LinearLayout;
    const v3, 0x7f0806b7

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mCheckLayout:Landroid/widget/RelativeLayout;

    .line 104
    const v3, 0x7f0806b8

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mAgreeCheck:Landroid/widget/CheckBox;

    .line 105
    const v3, 0x7f080613

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->nextButtonLayout:Landroid/widget/RelativeLayout;

    .line 107
    const v3, 0x7f08039d

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mNextText:Landroid/widget/TextView;

    .line 108
    const v3, 0x7f080614

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mNextImage:Landroid/widget/ImageView;

    .line 109
    invoke-direct {p0, v7}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->enableNextBtn(Z)V

    .line 110
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mAgreeCheck:Landroid/widget/CheckBox;

    invoke-virtual {v3, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 112
    const v3, 0x7f08068e

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->titlePPForSettings:Landroid/widget/TextView;

    .line 113
    const v3, 0x7f0806a8

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->titlePPForInitial:Landroid/widget/TextView;

    .line 114
    const v3, 0x7f080693

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->titleEraseForSettings:Landroid/widget/TextView;

    .line 115
    const v3, 0x7f0806ad

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->titleEraseForInitial:Landroid/widget/TextView;

    .line 117
    const v3, 0x7f0806a9

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph1:Landroid/widget/TextView;

    .line 118
    const v3, 0x7f0806aa

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph2:Landroid/widget/TextView;

    .line 119
    const v3, 0x7f0806ab

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph3:Landroid/widget/TextView;

    .line 120
    const v3, 0x7f0806ac

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph3_2:Landroid/widget/TextView;

    .line 121
    const v3, 0x7f0806b2

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph4:Landroid/widget/TextView;

    .line 122
    const v3, 0x7f0806b5

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph5:Landroid/widget/TextView;

    .line 123
    const v3, 0x7f0806b1

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph6:Landroid/widget/TextView;

    .line 124
    const v3, 0x7f0806ae

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppDeletion2:Landroid/widget/TextView;

    .line 125
    const v3, 0x7f0806af

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppDeletion3:Landroid/widget/TextView;

    .line 126
    const v3, 0x7f0806b0

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppDeletion4:Landroid/widget/TextView;

    .line 128
    const v3, 0x7f08068f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph1open:Landroid/widget/TextView;

    .line 129
    const v3, 0x7f080690

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph2open:Landroid/widget/TextView;

    .line 130
    const v3, 0x7f080691

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph3open:Landroid/widget/TextView;

    .line 131
    const v3, 0x7f080692

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph3open2:Landroid/widget/TextView;

    .line 132
    const v3, 0x7f080698

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph4open:Landroid/widget/TextView;

    .line 133
    const v3, 0x7f08069b

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph5open:Landroid/widget/TextView;

    .line 134
    const v3, 0x7f080697

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph6open:Landroid/widget/TextView;

    .line 135
    const v3, 0x7f080694

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppDeletion2open:Landroid/widget/TextView;

    .line 136
    const v3, 0x7f080695

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppDeletion3open:Landroid/widget/TextView;

    .line 137
    const v3, 0x7f080696

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppDeletion4open:Landroid/widget/TextView;

    .line 139
    const v3, 0x7f0806b3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTitle:Landroid/widget/TextView;

    .line 140
    const v3, 0x7f0806b4

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialText:Landroid/widget/TextView;

    .line 141
    const v3, 0x7f080699

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTitleOpen:Landroid/widget/TextView;

    .line 142
    const v3, 0x7f08069a

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppSpecialTextOpen:Landroid/widget/TextView;

    .line 144
    const v3, 0x7f08069f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppKorSub1Text2:Landroid/widget/TextView;

    .line 145
    const v3, 0x7f0806a3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppKorSub2Text3:Landroid/widget/TextView;

    .line 147
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isRtlLanguage()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 148
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->titlePPForSettings:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\u200f"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e44

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->titlePPForInitial:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\u200f"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e44

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->titleEraseForSettings:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\u200f"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e6d

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->titleEraseForInitial:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\u200f"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e6d

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 154
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->isPPCheckMode()Z

    move-result v3

    if-nez v3, :cond_2

    .line 156
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mCheckLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 157
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mAgreeCheck:Landroid/widget/CheckBox;

    invoke-virtual {v3, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 158
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->nextButtonLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 159
    const v3, 0x7f0806b9

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 160
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 257
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v2, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 258
    .local v2, "systemLocale":Ljava/util/Locale;
    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 262
    .local v1, "strLanguage":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isKoreaModel()Z

    move-result v3

    if-eqz v3, :cond_3

    if-eqz v1, :cond_3

    const-string v3, "ko"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 263
    const v3, 0x7f0806b6

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 264
    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 266
    const v3, 0x7f0806a7

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 267
    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 269
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    const v4, 0x7f0905c4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 271
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    .line 273
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppKorSub1Text2:Landroid/widget/TextView;

    const v4, 0x7f0905cf

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 274
    const v3, 0x7f0806a2

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 275
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppKorSub2Text3:Landroid/widget/TextView;

    const v4, 0x7f0905d0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 276
    const v3, 0x7f0806a4

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 277
    const v3, 0x7f0806a5

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 278
    const v3, 0x7f0806a6

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 308
    :cond_1
    :goto_1
    return-void

    .line 164
    .end local v1    # "strLanguage":Ljava/lang/String;
    .end local v2    # "systemLocale":Ljava/util/Locale;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-virtual {v3, v7}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->setUserCheckPrivacyPolicy(Z)V

    .line 166
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mCheckLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 167
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mAgreeCheck:Landroid/widget/CheckBox;

    invoke-virtual {v3, v7}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 168
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->nextButtonLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 170
    const v3, 0x7f0806b9

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 171
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 173
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mCheckLayout:Landroid/widget/RelativeLayout;

    new-instance v4, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$1;-><init>(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;)V

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 195
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mCheckLayout:Landroid/widget/RelativeLayout;

    new-instance v4, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$2;-><init>(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;)V

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mAgreeCheck:Landroid/widget/CheckBox;

    new-instance v4, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$3;-><init>(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;)V

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 220
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->nextButtonLayout:Landroid/widget/RelativeLayout;

    new-instance v4, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$4;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$4;-><init>(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;)V

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 283
    .restart local v1    # "strLanguage":Ljava/lang/String;
    .restart local v2    # "systemLocale":Ljava/util/Locale;
    :cond_3
    const v3, 0x7f0806b6

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 284
    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 286
    const v3, 0x7f0806a7

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 287
    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 289
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    const v4, 0x7f090e43

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 291
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_4

    .line 293
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph2open:Landroid/widget/TextView;

    const v4, 0x7f090e80

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 294
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph3open:Landroid/widget/TextView;

    const v4, 0x7f090e81

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 295
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph3open2:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 296
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->titleEraseForSettings:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 297
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppDeletion2open:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 298
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppDeletion3open:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 299
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppDeletion4open:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 300
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph6open:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 301
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->ppParagraph4open:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 305
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->applyReviewedText()V

    goto/16 :goto_1
.end method

.method private isPPCheckMode()Z
    .locals 1

    .prologue
    .line 896
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->isInitialSettingOngoing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private makeParagraph2Text()Ljava/lang/String;
    .locals 8

    .prologue
    .line 824
    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->getAllAppRegistryDatas(ZLandroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 826
    .local v0, "enabledPlugIns":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget-object v4, v5, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 827
    .local v4, "systemLocale":Ljava/util/Locale;
    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 829
    .local v2, "strLanguage":Ljava/lang/String;
    const v5, 0x7f090e59

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 831
    .local v3, "strParagraph2":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e5b

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 832
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e5d

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 834
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_c

    .line 836
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090020

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-boolean v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->enabled:Z

    if-eqz v5, :cond_1

    .line 837
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e5c

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 834
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 839
    :cond_1
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090028

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-boolean v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->enabled:Z

    if-eqz v5, :cond_2

    .line 840
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e5f

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 842
    :cond_2
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090021

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-boolean v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->enabled:Z

    if-eqz v5, :cond_3

    .line 843
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e5e

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 844
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e5a

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 846
    :cond_3
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090022

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-boolean v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->enabled:Z

    if-eqz v5, :cond_4

    .line 848
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e60

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 850
    :cond_4
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09017e

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-boolean v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->enabled:Z

    if-eqz v5, :cond_5

    .line 852
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e61

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 854
    :cond_5
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090026

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-boolean v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->enabled:Z

    if-eqz v5, :cond_6

    .line 856
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e62

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 858
    :cond_6
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09002c

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-boolean v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->enabled:Z

    if-eqz v5, :cond_7

    .line 860
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e63

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 862
    :cond_7
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090240

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-boolean v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->enabled:Z

    if-eqz v5, :cond_8

    .line 864
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e64

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 866
    :cond_8
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09002f

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-boolean v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->enabled:Z

    if-eqz v5, :cond_9

    .line 868
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e65

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 870
    :cond_9
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901ae

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_a

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-boolean v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->enabled:Z

    if-eqz v5, :cond_a

    .line 872
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e66

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 874
    :cond_a
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901b4

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_b

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-boolean v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->enabled:Z

    if-eqz v5, :cond_b

    .line 876
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e67

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 878
    :cond_b
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090cf5

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-boolean v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->enabled:Z

    if-eqz v5, :cond_0

    .line 880
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e68

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 881
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e69

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 884
    :cond_c
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "AU"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    if-eqz v2, :cond_d

    const-string v5, "en"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 886
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090769

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 892
    :goto_2
    return-object v3

    .line 890
    :cond_d
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e6a

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_2
.end method

.method private registerProfileFinishedReceiver()V
    .locals 2

    .prologue
    .line 917
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$ProfileFinishedReceiver;

    if-nez v0, :cond_0

    .line 919
    new-instance v0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$ProfileFinishedReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$ProfileFinishedReceiver;-><init>(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$ProfileFinishedReceiver;

    .line 920
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mSipIntentFilter:Landroid/content/IntentFilter;

    .line 921
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mSipIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.shealth.action.FINISHED_PROFILE_SETUP"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 922
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$ProfileFinishedReceiver;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mSipIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 925
    :cond_0
    return-void
.end method

.method private unregisterProfileFinishedReceiver()V
    .locals 1

    .prologue
    .line 932
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$ProfileFinishedReceiver;

    if-eqz v0, :cond_0

    .line 934
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$ProfileFinishedReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 935
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$ProfileFinishedReceiver;

    .line 937
    :cond_0
    return-void
.end method


# virtual methods
.method public closeScreen()V
    .locals 2

    .prologue
    .line 805
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->isPPCheckMode()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->isFromSettings:Z

    if-nez v1, :cond_0

    .line 807
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 808
    .local v0, "startMain":Landroid/content/Intent;
    const-string v1, "android.intent.category.HOME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 809
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->startActivity(Landroid/content/Intent;)V

    .line 813
    .end local v0    # "startMain":Landroid/content/Intent;
    :goto_0
    return-void

    .line 811
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->closeScreen()V

    goto :goto_0
.end method

.method protected customizeActionBar()V
    .locals 0

    .prologue
    .line 818
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 820
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 788
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->isPPCheckMode()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->isFromSettings:Z

    if-nez v1, :cond_0

    .line 793
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 794
    .local v0, "splashIntent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->startActivity(Landroid/content/Intent;)V

    .line 795
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->moveTaskToBack(Z)Z

    .line 801
    .end local v0    # "splashIntent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 798
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 74
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 76
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    .line 77
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    .line 79
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getIntent()Landroid/content/Intent;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->FROM_SETTINGS:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->isFromSettings:Z

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->isInitialSettingOngoing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 84
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setRestoreAgainHomePopup(Landroid/content/Context;Z)V

    .line 87
    :cond_1
    const v0, 0x7f03018d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->setContentView(I)V

    .line 89
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->initLayout()V

    .line 90
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->registerProfileFinishedReceiver()V

    .line 91
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->unregisterProfileFinishedReceiver()V

    .line 96
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    .line 97
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 776
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 778
    sget-boolean v1, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->isFromSettings:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->isDeviceSignInSamsungAccount(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 779
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 780
    .local v0, "intent":Landroid/content/Intent;
    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 781
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->startActivity(Landroid/content/Intent;)V

    .line 782
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->finish()V

    .line 784
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
