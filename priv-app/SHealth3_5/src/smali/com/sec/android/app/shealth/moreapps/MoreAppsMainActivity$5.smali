.class Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$5;
.super Ljava/lang/Object;
.source "MoreAppsMainActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->showLoadingPopUp(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;

.field final synthetic val$cancellable:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;Z)V
    .locals 0

    .prologue
    .line 298
    iput-object p1, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$5;->this$0:Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;

    iput-boolean p2, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$5;->val$cancellable:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    .line 304
    const/4 v0, 0x4

    if-ne p2, v0, :cond_1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$5;->val$cancellable:Z

    if-eqz v0, :cond_1

    .line 305
    iget-object v0, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$5;->this$0:Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;

    # getter for: Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mLoadingPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->access$000(Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 306
    iget-object v0, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$5;->this$0:Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;

    # getter for: Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mLoadingPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->access$000(Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->dismiss()V

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$5;->this$0:Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mLoadingPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->access$002(Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    .line 309
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$5;->this$0:Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->onBackPressed()V

    .line 311
    :cond_1
    const/4 v0, 0x1

    return v0
.end method
