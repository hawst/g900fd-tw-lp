.class public Lcom/sec/android/app/shealth/uv/UvActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;
.source "UvActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/logutils/graph/FragmentSwitchable;


# static fields
.field private static final ACCESSORIES_DUPLICATE_DIALOG:Ljava/lang/String; = "accessories_duplicate_dialog"

.field public static final ACTIONBAR_LOG:I = 0x0

.field private static final LAUNCH_MODE:Ljava/lang/String; = "launch_mode"

.field private static final SENSOR:Ljava/lang/String; = "sensor"

.field private static final WARNING_STRING:Ljava/lang/String; = "is_warning_checked"

.field public static isDialogDissmised:Z


# instance fields
.field private actionbarClickListener:Landroid/view/View$OnClickListener;

.field againOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field animationFadeIn:Landroid/view/animation/AnimationSet;

.field animationFadeOut:Landroid/view/animation/AnimationSet;

.field private backFinish:Z

.field private backpressed:Z

.field private doNotShowCheckBox:Landroid/widget/CheckBox;

.field fadeIn:Landroid/view/animation/Animation;

.field fadeOut:Landroid/view/animation/Animation;

.field private fromDashboard:Z

.field private hasNoUVSensor:Z

.field private hestiaImages:[I

.field private images:[I

.field private informationHestiaImagesArray:[Ljava/lang/String;

.field private informationImagesArray:[Ljava/lang/String;

.field isImage1Found:Z

.field isImage2Found:Z

.field private isMeasureCompleted:Z

.field private isShownPopup:Z

.field listener:Landroid/view/View$OnClickListener;

.field private mActionBarAccButtonBuilder:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

.field private mActionBarShareButtonBuilder:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

.field private mAnimationViewInfo0:Landroid/widget/ImageView;

.field private mAnimationViewInfo1:Landroid/widget/ImageView;

.field mCheckboxLayoutClickListener:Landroid/view/View$OnClickListener;

.field private mData:Lcom/sec/android/app/shealth/uv/data/SkinData;

.field private mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mDeviceTypes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mDialogHandler:Landroid/os/Handler;

.field private mDisclaimerShowAgain:Landroid/widget/CheckBox;

.field private mGraphFragment:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

.field private mInfoView:Landroid/view/View;

.field private mInformationText1:Landroid/widget/TextView;

.field private mInformationText2:Landroid/widget/TextView;

.field private mIsOnConfigChanged:Z

.field private mShowAgain:Landroid/widget/CheckBox;

.field private mShowAgainCheckLayout:Landroid/widget/RelativeLayout;

.field private mShowAgainNoSensorCheckLayout:Landroid/widget/RelativeLayout;

.field private mSkinTypeDialog:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

.field private mSkinTypeDialogDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

.field private mUvInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mUvNoSensorInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mUvSkinDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;

.field shareListener:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/uv/UvActivity;->isDialogDissmised:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 73
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;-><init>()V

    .line 89
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->isMeasureCompleted:Z

    .line 91
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->backFinish:Z

    .line 96
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->fromDashboard:Z

    .line 109
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->isImage1Found:Z

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->isImage2Found:Z

    .line 111
    new-array v0, v4, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->images:[I

    .line 115
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "uv_guide_t_1"

    aput-object v1, v0, v3

    const-string/jumbo v1, "uv_guide_t_2"

    aput-object v1, v0, v5

    const-string/jumbo v1, "uv_guide_t_3"

    aput-object v1, v0, v6

    const-string/jumbo v1, "uv_guide_t_4"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string/jumbo v2, "uv_guide_t_5"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "uv_guide_t_6"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "uv_guide_t_5"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "uv_guide_t_6"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->informationImagesArray:[Ljava/lang/String;

    .line 118
    new-array v0, v4, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->hestiaImages:[I

    .line 127
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "uv_guide_hestia_1"

    aput-object v1, v0, v3

    const-string/jumbo v1, "uv_guide_hestia_2"

    aput-object v1, v0, v5

    const-string/jumbo v1, "uv_guide_hestia_3"

    aput-object v1, v0, v6

    const-string/jumbo v1, "uv_guide_hestia_4"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string/jumbo v2, "uv_guide_hestia_5"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "uv_guide_hestia_6"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "uv_guide_hestia_5"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "uv_guide_hestia_6"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->informationHestiaImagesArray:[Ljava/lang/String;

    .line 130
    new-instance v0, Lcom/sec/android/app/shealth/uv/UvActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/uv/UvActivity$1;-><init>(Lcom/sec/android/app/shealth/uv/UvActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->actionbarClickListener:Landroid/view/View$OnClickListener;

    .line 210
    new-instance v0, Lcom/sec/android/app/shealth/uv/UvActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/uv/UvActivity$2;-><init>(Lcom/sec/android/app/shealth/uv/UvActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mDialogHandler:Landroid/os/Handler;

    .line 577
    new-instance v0, Lcom/sec/android/app/shealth/uv/UvActivity$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/uv/UvActivity$10;-><init>(Lcom/sec/android/app/shealth/uv/UvActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->listener:Landroid/view/View$OnClickListener;

    .line 586
    new-instance v0, Lcom/sec/android/app/shealth/uv/UvActivity$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/uv/UvActivity$11;-><init>(Lcom/sec/android/app/shealth/uv/UvActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->shareListener:Landroid/view/View$OnClickListener;

    .line 701
    new-instance v0, Lcom/sec/android/app/shealth/uv/UvActivity$12;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/uv/UvActivity$12;-><init>(Lcom/sec/android/app/shealth/uv/UvActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mCheckboxLayoutClickListener:Landroid/view/View$OnClickListener;

    .line 870
    new-instance v0, Lcom/sec/android/app/shealth/uv/UvActivity$20;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/uv/UvActivity$20;-><init>(Lcom/sec/android/app/shealth/uv/UvActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->againOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 918
    new-instance v0, Lcom/sec/android/app/shealth/uv/UvActivity$21;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/uv/UvActivity$21;-><init>(Lcom/sec/android/app/shealth/uv/UvActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSkinTypeDialogDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    return-void

    .line 111
    nop

    :array_0
    .array-data 4
        0x7f0208bd
        0x7f0208be
        0x7f0208bf
        0x7f0208c0
        0x7f0208c1
        0x7f0208c2
        0x7f0208c1
        0x7f0208c2
    .end array-data

    .line 118
    :array_1
    .array-data 4
        0x7f0208b7
        0x7f0208b8
        0x7f0208b9
        0x7f0208ba
        0x7f0208bb
        0x7f0208bc
        0x7f0208bb
        0x7f0208bc
    .end array-data
.end method

.method private OnConnectivityActivity()V
    .locals 4

    .prologue
    .line 652
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth.uv"

    const-string v3, "UV11"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mDeviceTypes:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 655
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mDeviceTypes:Ljava/util/ArrayList;

    .line 657
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mDeviceTypes:Ljava/util/ArrayList;

    const/16 v2, 0x272e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 658
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 659
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->DATA_TYPE_KEY:Ljava/lang/String;

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 660
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->CONNECTIVITY_TYPE_KEY:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 661
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->HEADER_TEXT_ID:Ljava/lang/String;

    const v2, 0x7f090d9d

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 662
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->SCANNING_TYPE_INFO_TEXT_RES_ID_KEY:Ljava/lang/String;

    const v2, 0x7f090d9e

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 663
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->NO_DEVICES_TEXT_RES_ID_KEY:Ljava/lang/String;

    const v2, 0x7f090092

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 664
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->DEVICE_TYPES_KEY:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mDeviceTypes:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 665
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->startActivity(Landroid/content/Intent;)V

    .line 666
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/uv/UvActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->hasNoUVSensor:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/uv/UvActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->showInfomationDialog()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/uv/UvActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->prepareShareView()V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mShowAgain:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/shealth/uv/UvActivity;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;
    .param p1, "x1"    # Landroid/widget/CheckBox;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mShowAgain:Landroid/widget/CheckBox;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->doNotShowCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/shealth/uv/UvActivity;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;
    .param p1, "x1"    # Landroid/widget/CheckBox;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->doNotShowCheckBox:Landroid/widget/CheckBox;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mInformationText1:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/shealth/uv/UvActivity;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mInformationText1:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mInformationText2:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/sec/android/app/shealth/uv/UvActivity;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mInformationText2:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mShowAgainCheckLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/shealth/uv/UvActivity;Landroid/widget/RelativeLayout;)Landroid/widget/RelativeLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;
    .param p1, "x1"    # Landroid/widget/RelativeLayout;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mShowAgainCheckLayout:Landroid/widget/RelativeLayout;

    return-object p1
.end method

.method static synthetic access$1602(Lcom/sec/android/app/shealth/uv/UvActivity;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mInfoView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/uv/UvActivity;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/uv/UvActivity;->onInitWarningVideo(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/uv/UvActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mUvInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mDrawerLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/uv/UvActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->showNoSensorInformationDialog()V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/android/app/shealth/uv/UvActivity;)Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mDialogHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/shealth/uv/UvActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->fromDashboard:Z

    return v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2302(Lcom/sec/android/app/shealth/uv/UvActivity;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$2400(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/sec/android/app/shealth/uv/UvActivity;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$2500(Lcom/sec/android/app/shealth/uv/UvActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->backpressed:Z

    return v0
.end method

.method static synthetic access$2502(Lcom/sec/android/app/shealth/uv/UvActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->backpressed:Z

    return p1
.end method

.method static synthetic access$2600(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mDrawerLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/android/app/shealth/uv/UvActivity;)Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mShowAgainNoSensorCheckLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$2802(Lcom/sec/android/app/shealth/uv/UvActivity;Landroid/widget/RelativeLayout;)Landroid/widget/RelativeLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;
    .param p1, "x1"    # Landroid/widget/RelativeLayout;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mShowAgainNoSensorCheckLayout:Landroid/widget/RelativeLayout;

    return-object p1
.end method

.method static synthetic access$2900(Lcom/sec/android/app/shealth/uv/UvActivity;)Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSkinTypeDialog:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/uv/UvActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->showSkinTypeDialog()V

    return-void
.end method

.method static synthetic access$3000(Lcom/sec/android/app/shealth/uv/UvActivity;Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;
    .param p1, "x1"    # Landroid/widget/ImageView;
    .param p2, "x2"    # Landroid/widget/ImageView;
    .param p3, "x3"    # [I
    .param p4, "x4"    # [Ljava/lang/String;
    .param p5, "x5"    # I
    .param p6, "x6"    # Z

    .prologue
    .line 73
    invoke-direct/range {p0 .. p6}, Lcom/sec/android/app/shealth/uv/UvActivity;->animate(Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/uv/UvActivity;)Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/uv/UvActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->backFinish:Z

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/uv/UvActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->isMeasureCompleted:Z

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/shealth/uv/UvActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->isShownPopup:Z

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/uv/UvActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->OnConnectivityActivity()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/uv/UvActivity;)Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/UvActivity;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mGraphFragment:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    return-object v0
.end method

.method private animate(Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V
    .locals 12
    .param p1, "imageView1"    # Landroid/widget/ImageView;
    .param p2, "imageView2"    # Landroid/widget/ImageView;
    .param p3, "images"    # [I
    .param p4, "ImageArr"    # [Ljava/lang/String;
    .param p5, "imageIndex"    # I
    .param p6, "forever"    # Z

    .prologue
    .line 991
    sget-boolean v1, Lcom/sec/android/app/shealth/uv/UvActivity;->isDialogDissmised:Z

    if-eqz v1, :cond_0

    .line 1077
    :goto_0
    return-void

    .line 993
    :cond_0
    move/from16 v0, p5

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->initFadingAnimationEffect(I)V

    .line 994
    invoke-virtual {p1}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 995
    .local v9, "ImgTag1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 996
    .local v10, "ImgTag2":Ljava/lang/String;
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->isImage1Found:Z

    .line 997
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->isImage2Found:Z

    .line 998
    aget-object v1, p4, p5

    invoke-virtual {v9, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 999
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->isImage1Found:Z

    .line 1000
    invoke-virtual {p1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1001
    invoke-virtual {p2}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1002
    array-length v1, p3

    add-int/lit8 v1, v1, -0x1

    move/from16 v0, p5

    if-ne v1, v0, :cond_3

    .line 1003
    const/4 v1, 0x0

    aget v1, p3, v1

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1004
    const/4 v1, 0x0

    aget-object v1, p4, v1

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 1009
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->animationFadeOut:Landroid/view/animation/AnimationSet;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1010
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->animationFadeIn:Landroid/view/animation/AnimationSet;

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1012
    :cond_1
    aget-object v1, p4, p5

    invoke-virtual {v10, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1013
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->isImage2Found:Z

    .line 1014
    invoke-virtual {p1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1015
    invoke-virtual {p2}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1016
    array-length v1, p3

    add-int/lit8 v1, v1, -0x1

    move/from16 v0, p5

    if-ne v0, v1, :cond_4

    .line 1018
    const/4 v1, 0x0

    aget v1, p3, v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1019
    const/4 v1, 0x0

    aget-object v1, p4, v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 1024
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->animationFadeOut:Landroid/view/animation/AnimationSet;

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1025
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->animationFadeIn:Landroid/view/animation/AnimationSet;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1028
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->animationFadeIn:Landroid/view/animation/AnimationSet;

    new-instance v2, Lcom/sec/android/app/shealth/uv/UvActivity$22;

    invoke-direct {v2, p0, p2, p1}, Lcom/sec/android/app/shealth/uv/UvActivity$22;-><init>(Lcom/sec/android/app/shealth/uv/UvActivity;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1048
    iget-object v11, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->animationFadeOut:Landroid/view/animation/AnimationSet;

    new-instance v1, Lcom/sec/android/app/shealth/uv/UvActivity$23;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move/from16 v6, p5

    move-object/from16 v7, p4

    move/from16 v8, p6

    invoke-direct/range {v1 .. v8}, Lcom/sec/android/app/shealth/uv/UvActivity$23;-><init>(Lcom/sec/android/app/shealth/uv/UvActivity;Landroid/widget/ImageView;Landroid/widget/ImageView;[II[Ljava/lang/String;Z)V

    invoke-virtual {v11, v1}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    goto/16 :goto_0

    .line 1006
    :cond_3
    add-int/lit8 v1, p5, 0x1

    aget v1, p3, v1

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1007
    add-int/lit8 v1, p5, 0x1

    aget-object v1, p4, v1

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    goto :goto_1

    .line 1021
    :cond_4
    add-int/lit8 v1, p5, 0x1

    aget v1, p3, v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1022
    add-int/lit8 v1, p5, 0x1

    aget-object v1, p4, v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    goto :goto_2
.end method

.method private dismissVisibleDialogs()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mUvInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mUvInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mUvInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    .line 279
    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mUvInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 280
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->displayInformationDialog()V

    .line 284
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mUvNoSensorInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mUvNoSensorInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 285
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mUvNoSensorInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    .line 286
    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mUvNoSensorInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 287
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->displayNonSensorInformationDialog()V

    .line 290
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvLocationAlertDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvLocationAlertDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvLocationAlertDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iput-object v1, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvLocationAlertDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 293
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iput-boolean v2, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->needsLocationPopup:Z

    .line 296
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvRecommendedDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvRecommendedDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 297
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvRecommendedDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    .line 298
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iput-object v1, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvRecommendedDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 299
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iput-boolean v2, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->isShownRecomendedDialog:Z

    .line 302
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvSPFDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvSPFDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvSPFDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iput-object v1, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvSPFDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 305
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iput-boolean v2, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->isSPFDialogDisplayed:Z

    .line 308
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvFailureAlertDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvFailureAlertDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 309
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvFailureAlertDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    .line 310
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iput-object v1, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mUvFailureAlertDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 311
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iput-boolean v2, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->isFailurePopupVisible:Z

    .line 313
    :cond_5
    return-void
.end method

.method private displayInformationDialog()V
    .locals 4

    .prologue
    .line 316
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/uv/UvActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/uv/UvActivity$4;-><init>(Lcom/sec/android/app/shealth/uv/UvActivity;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 322
    return-void
.end method

.method private displayNonSensorInformationDialog()V
    .locals 4

    .prologue
    .line 325
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/uv/UvActivity$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/uv/UvActivity$5;-><init>(Lcom/sec/android/app/shealth/uv/UvActivity;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 331
    return-void
.end method

.method private initFadingAnimationEffect(I)V
    .locals 7
    .param p1, "imageIndex"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 962
    const/16 v0, 0x1f4

    .line 963
    .local v0, "fadeInDuration":I
    const/16 v2, 0x5dc

    .line 964
    .local v2, "timeBetween":I
    const/16 v1, 0x1f4

    .line 966
    .local v1, "fadeOutDuration":I
    const/4 v3, 0x4

    if-eq p1, v3, :cond_0

    const/4 v3, 0x5

    if-eq p1, v3, :cond_0

    const/4 v3, 0x6

    if-ne p1, v3, :cond_1

    .line 967
    :cond_0
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v5, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->fadeIn:Landroid/view/animation/Animation;

    .line 968
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v4, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->fadeOut:Landroid/view/animation/Animation;

    .line 974
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->fadeIn:Landroid/view/animation/Animation;

    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 975
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->fadeIn:Landroid/view/animation/Animation;

    int-to-long v4, v0

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 976
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->fadeIn:Landroid/view/animation/Animation;

    const/16 v4, 0x514

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 978
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->fadeOut:Landroid/view/animation/Animation;

    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 979
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->fadeOut:Landroid/view/animation/Animation;

    int-to-long v4, v2

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 980
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->fadeOut:Landroid/view/animation/Animation;

    int-to-long v4, v1

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 981
    new-instance v3, Landroid/view/animation/AnimationSet;

    invoke-direct {v3, v6}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->animationFadeIn:Landroid/view/animation/AnimationSet;

    .line 982
    new-instance v3, Landroid/view/animation/AnimationSet;

    invoke-direct {v3, v6}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->animationFadeOut:Landroid/view/animation/AnimationSet;

    .line 983
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->animationFadeIn:Landroid/view/animation/AnimationSet;

    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->fadeIn:Landroid/view/animation/Animation;

    invoke-virtual {v3, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 984
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->animationFadeOut:Landroid/view/animation/AnimationSet;

    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->fadeOut:Landroid/view/animation/Animation;

    invoke-virtual {v3, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 985
    return-void

    .line 970
    :cond_1
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v5, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->fadeIn:Landroid/view/animation/Animation;

    .line 971
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v4, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->fadeOut:Landroid/view/animation/Animation;

    goto :goto_0
.end method

.method private initUVSensorScenario(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1100
    const/4 v1, 0x0

    .line 1101
    .local v1, "mUvSensor":Landroid/hardware/Sensor;
    const-string/jumbo v2, "sensor"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    .line 1102
    .local v0, "mSensorManager":Landroid/hardware/SensorManager;
    const v2, 0x1001d

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    .line 1103
    if-nez v1, :cond_0

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->hasNoUVSensor:Z

    .line 1104
    return-void

    .line 1103
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private onInitWarningVideo(Landroid/view/View;)V
    .locals 4
    .param p1, "mInfoView"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 879
    if-eqz p1, :cond_0

    .line 880
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 881
    const v0, 0x7f080af2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    .line 882
    const v0, 0x7f080af3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    .line 883
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/Utils;->isHestiaModel()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 884
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->hestiaImages:[I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 885
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->hestiaImages:[I

    aget v1, v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 895
    :cond_0
    :goto_0
    return-void

    .line 888
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->images:[I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 889
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->images:[I

    aget v1, v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private showDeleteDialog()V
    .locals 4

    .prologue
    .line 423
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->terminateCurrentState()V

    .line 424
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 425
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 426
    const v1, 0x7f090062

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 427
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090032

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 428
    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 429
    new-instance v1, Lcom/sec/android/app/shealth/uv/UvActivity$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/uv/UvActivity$7;-><init>(Lcom/sec/android/app/shealth/uv/UvActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 441
    new-instance v1, Lcom/sec/android/app/shealth/uv/UvActivity$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/uv/UvActivity$8;-><init>(Lcom/sec/android/app/shealth/uv/UvActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 451
    new-instance v1, Lcom/sec/android/app/shealth/uv/UvActivity$9;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/uv/UvActivity$9;-><init>(Lcom/sec/android/app/shealth/uv/UvActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 461
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 462
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 463
    return-void
.end method

.method private showInfomationDialog()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 714
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 715
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 716
    const v1, 0x7f09002f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 717
    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 718
    const v1, 0x7f030267

    new-instance v2, Lcom/sec/android/app/shealth/uv/UvActivity$13;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/uv/UvActivity$13;-><init>(Lcom/sec/android/app/shealth/uv/UvActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 752
    new-instance v1, Lcom/sec/android/app/shealth/uv/UvActivity$14;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/uv/UvActivity$14;-><init>(Lcom/sec/android/app/shealth/uv/UvActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 765
    new-instance v1, Lcom/sec/android/app/shealth/uv/UvActivity$15;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/uv/UvActivity$15;-><init>(Lcom/sec/android/app/shealth/uv/UvActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 786
    if-eqz p0, :cond_1

    .line 788
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mUvInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-nez v1, :cond_0

    .line 789
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mUvInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 791
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->startInformationVideo()V

    .line 792
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mUvInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string/jumbo v3, "uv_information_dialog"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 794
    :cond_1
    return-void
.end method

.method private showNoSensorInformationDialog()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 799
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 800
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 801
    const v1, 0x7f09002f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 802
    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 804
    new-instance v1, Lcom/sec/android/app/shealth/uv/UvActivity$16;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/uv/UvActivity$16;-><init>(Lcom/sec/android/app/shealth/uv/UvActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 813
    new-instance v1, Lcom/sec/android/app/shealth/uv/UvActivity$17;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/uv/UvActivity$17;-><init>(Lcom/sec/android/app/shealth/uv/UvActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 835
    new-instance v1, Lcom/sec/android/app/shealth/uv/UvActivity$18;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/uv/UvActivity$18;-><init>(Lcom/sec/android/app/shealth/uv/UvActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 843
    const v1, 0x7f030268

    new-instance v2, Lcom/sec/android/app/shealth/uv/UvActivity$19;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/uv/UvActivity$19;-><init>(Lcom/sec/android/app/shealth/uv/UvActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 861
    if-eqz p0, :cond_1

    .line 863
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mUvNoSensorInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-nez v1, :cond_0

    .line 864
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mUvNoSensorInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 866
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mUvNoSensorInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "accessories_duplicate_dialog"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 868
    :cond_1
    return-void
.end method

.method private showSkinTypeDialog()V
    .locals 2

    .prologue
    .line 901
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSkinTypeDialog:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    if-nez v0, :cond_0

    .line 902
    new-instance v0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSkinTypeDialog:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    .line 903
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSkinTypeDialog:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSkinTypeDialogDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 905
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSkinTypeDialog:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->show()V

    .line 906
    return-void
.end method

.method private showSkinTypeDialog(Lcom/sec/android/app/shealth/uv/data/SkinData;)V
    .locals 2
    .param p1, "skinData"    # Lcom/sec/android/app/shealth/uv/data/SkinData;

    .prologue
    .line 910
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSkinTypeDialog:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    if-nez v0, :cond_0

    .line 911
    new-instance v0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSkinTypeDialog:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    .line 912
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSkinTypeDialog:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->updateContent(Lcom/sec/android/app/shealth/uv/data/SkinData;)V

    .line 913
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSkinTypeDialog:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSkinTypeDialogDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 915
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSkinTypeDialog:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->show()V

    .line 916
    return-void
.end method


# virtual methods
.method public callShowInformationDialog()V
    .locals 4

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mDialogHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 150
    return-void
.end method

.method protected customizeActionBar()V
    .locals 11

    .prologue
    const v10, 0x7f0907b4

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 601
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->customizeActionBar()V

    .line 602
    const/4 v0, 0x0

    .line 603
    .local v0, "emptyActionItemTextId":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 604
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    const v3, 0x7f09002f

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 605
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    new-array v3, v9, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    new-instance v4, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v5, 0x7f0207c0

    const v6, 0x7f09005a

    iget-object v7, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->actionbarClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {v4, v5, v0, v6, v7}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    aput-object v4, v3, v8

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 606
    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v3, 0x7f0207bc

    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mActionBarAccButtonBuilder:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 607
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mActionBarAccButtonBuilder:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    invoke-virtual {v2, v10}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 608
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mActionBarAccButtonBuilder:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    invoke-virtual {v2, v10}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setContentDescription(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 609
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    new-array v3, v9, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mActionBarAccButtonBuilder:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 610
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09020e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarUpButtonDescription(Ljava/lang/String;)V

    .line 611
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getGraphFragment()Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    .line 612
    .local v1, "uvgraph":Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getDataCount()I

    move-result v2

    if-eqz v2, :cond_0

    .line 613
    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v3, 0x7f0207cf

    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->shareListener:Landroid/view/View$OnClickListener;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mActionBarShareButtonBuilder:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 614
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mActionBarShareButtonBuilder:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v3, 0x7f090033

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 615
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    new-array v3, v9, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mActionBarShareButtonBuilder:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 616
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 623
    :goto_0
    return-void

    .line 617
    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p0}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getDataCount()I

    move-result v2

    if-nez v2, :cond_1

    .line 618
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    goto :goto_0

    .line 620
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2, v9}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 630
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x16

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 632
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 633
    .local v0, "currentView":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f080304

    if-ne v1, v2, :cond_0

    .line 635
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f08030e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    check-cast v1, Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    .line 636
    const/4 v1, 0x1

    .line 641
    .end local v0    # "currentView":Landroid/view/View;
    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method public getAppContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 695
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public getCallFromHome()Z
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->fromDashboard:Z

    return v0
.end method

.method public getGraphFragment()Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    .locals 1

    .prologue
    .line 673
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mGraphFragment:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    return-object v0
.end method

.method protected getHelpItem()Ljava/lang/String;
    .locals 1

    .prologue
    .line 691
    const-string v0, "com.sec.shealth.help.action.UV"

    return-object v0
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    .line 355
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mGraphFragment:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 356
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->fromDashboard:Z

    if-eqz v0, :cond_0

    .line 358
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->finish()V

    .line 386
    :goto_0
    return-void

    .line 360
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    goto :goto_0

    .line 362
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 364
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->backFinish:Z

    if-eqz v0, :cond_2

    .line 365
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->terminateCurrentState()V

    .line 366
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->finish()V

    .line 375
    :goto_1
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/uv/UvActivity$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/uv/UvActivity$6;-><init>(Lcom/sec/android/app/shealth/uv/UvActivity;)V

    const-wide/16 v2, 0x9c4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 368
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090d99

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 373
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->backFinish:Z

    goto :goto_1

    .line 384
    :cond_3
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 406
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->clearAndRefreshDrawerMenu()V

    .line 417
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 418
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mIsOnConfigChanged:Z

    .line 419
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const-wide/16 v5, 0xfa

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 154
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 156
    invoke-direct {p0, p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->initUVSensorScenario(Landroid/content/Context;)V

    .line 159
    new-instance v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .line 160
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->hasNoUVSensor:Z

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->initUvSensorAvaialability(Z)V

    .line 161
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->initUvState()V

    .line 163
    new-instance v1, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mGraphFragment:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    .line 164
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mGraphFragment:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 165
    invoke-static {p0}, Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mUvSkinDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;

    .line 166
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "show_graph_fragment"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 167
    .local v0, "isShowGraphFrag":Z
    if-eqz p1, :cond_0

    .line 168
    const-string v1, "chart_shown"

    invoke-virtual {p1, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 169
    const-string v1, "chart_shown"

    invoke-virtual {p1, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 170
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    const-string/jumbo v2, "state_finished"

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->ismFinishedState:Z

    .line 174
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->hasNoUVSensor:Z

    if-eqz v1, :cond_3

    .line 175
    if-eqz v0, :cond_1

    .line 176
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->fromDashboard:Z

    .line 177
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mGraphFragment:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/uv/UvActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 208
    :goto_0
    return-void

    .line 179
    :cond_1
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "uv_warning_checked"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_2

    .line 180
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mDrawerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->closeDrawer(Landroid/view/View;)V

    .line 181
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->callShowInformationDialog()V

    goto :goto_0

    .line 183
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iput-boolean v3, v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->needsLocationPopup:Z

    .line 184
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/uv/UvActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    goto :goto_0

    .line 190
    :cond_3
    if-eqz v0, :cond_4

    .line 191
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->fromDashboard:Z

    .line 192
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mGraphFragment:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/uv/UvActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    goto :goto_0

    .line 195
    :cond_4
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "uv_warning_checked"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_5

    .line 196
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mDrawerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->closeDrawer(Landroid/view/View;)V

    .line 197
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mDialogHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 198
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mUvSkinDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;->getDataCount()I

    move-result v1

    if-nez v1, :cond_6

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "skin_type_checked"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_6

    .line 200
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mDrawerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->closeDrawer(Landroid/view/View;)V

    .line 201
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mDialogHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 203
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iput-boolean v4, v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->needsLocationPopup:Z

    .line 204
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/uv/UvActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 467
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f10002c

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 469
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->isDrawerMenuShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 470
    const/4 v0, 0x0

    .line 473
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 949
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mUvSkinDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;->clearData()Z

    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mUvSkinDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;

    .line 950
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mUvInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mUvInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 951
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    if-eqz v0, :cond_1

    .line 952
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->clearDeviceConnector()V

    .line 954
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    if-eqz v0, :cond_2

    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .line 955
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mGraphFragment:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    if-eqz v0, :cond_3

    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mGraphFragment:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    .line 956
    :cond_3
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onDestroy()V

    .line 957
    return-void
.end method

.method protected onLogSelected()V
    .locals 3

    .prologue
    .line 646
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.uv"

    const-string v2, "UV03"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 647
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->startActivity(Landroid/content/Intent;)V

    .line 648
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 517
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->isMeasureCompleted:Z

    if-nez v2, :cond_0

    .line 520
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 541
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    if-eqz v2, :cond_1

    .line 542
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 544
    .local v0, "intent":Landroid/content/Intent;
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 574
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    :goto_1
    const/4 v2, 0x0

    :goto_2
    return v2

    .line 525
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->prepareShareView()V

    .line 526
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "com.sec.android.app.shealth.uv"

    const-string v4, "UV07"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 529
    :sswitch_1
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 530
    .local v1, "intentSetting":Landroid/content/Intent;
    const-string v2, "com.sec.android.app.shealth"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 531
    const-string v2, "android.shealth.action.LAUNCH_SETTINGS"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 532
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/uv/UvActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 536
    .end local v1    # "intentSetting":Landroid/content/Intent;
    :sswitch_2
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 537
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "com.sec.android.app.shealth.uv"

    const-string v4, "UV06"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    const/4 v2, 0x1

    goto :goto_2

    .line 556
    .restart local v0    # "intent":Landroid/content/Intent;
    :pswitch_0
    const-string v2, "launch_mode"

    const-string/jumbo v3, "sensor"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 557
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->finish()V

    .line 558
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 562
    :pswitch_1
    invoke-static {p0}, Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;->getData()Lcom/sec/android/app/shealth/uv/data/SkinData;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mData:Lcom/sec/android/app/shealth/uv/data/SkinData;

    .line 563
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSkinTypeDialog:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    .line 564
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mData:Lcom/sec/android/app/shealth/uv/data/SkinData;

    if-eqz v2, :cond_2

    .line 565
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mData:Lcom/sec/android/app/shealth/uv/data/SkinData;

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->showSkinTypeDialog(Lcom/sec/android/app/shealth/uv/data/SkinData;)V

    goto :goto_1

    .line 567
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->showSkinTypeDialog()V

    goto :goto_1

    .line 520
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f080c8a -> :sswitch_2
        0x7f080c90 -> :sswitch_0
        0x7f080ca4 -> :sswitch_1
    .end sparse-switch

    .line 544
    :pswitch_data_0
    .packed-switch 0x7f080caf
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->isShownPopup:Z

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    .line 339
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 343
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mUvInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mUvInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 344
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 345
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 347
    :cond_2
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onPause()V

    .line 348
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 6
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 479
    if-eqz p1, :cond_1

    .line 480
    :try_start_0
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 481
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v4

    const v5, 0x7f10002c

    invoke-virtual {v4, v5, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 482
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->isDrawerMenuShown()Z

    move-result v4

    if-nez v4, :cond_0

    .line 483
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->customizeActionBar()V

    .line 484
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getGraphFragment()Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    .line 485
    .local v1, "graphFragment":Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->isVisible()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 487
    const v2, 0x7f080ca4

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 488
    const v2, 0x7f080c8a

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 489
    const v2, 0x7f080c90

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 490
    const v2, 0x7f080caf

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 491
    const v2, 0x7f080cb0

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 511
    .end local v1    # "graphFragment":Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;
    :cond_1
    :goto_0
    return v3

    .line 495
    .restart local v1    # "graphFragment":Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;
    :cond_2
    const v4, 0x7f080ca4

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 496
    const v4, 0x7f080caf

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-static {}, Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->getInstance()Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->isStarted()Z

    move-result v4

    if-ne v4, v3, :cond_4

    move v4, v2

    :goto_1
    invoke-interface {v5, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 497
    const v4, 0x7f080c8a

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 498
    const v4, 0x7f080c90

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->getInstance()Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/uv/common/UvDeviceConnector;->isStarted()Z

    move-result v5

    if-ne v5, v3, :cond_7

    :cond_3
    :goto_2
    invoke-interface {v4, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 499
    const v2, 0x7f080cb0

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 500
    const v2, 0x7f080caf

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 507
    .end local v1    # "graphFragment":Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;
    :catch_0
    move-exception v0

    .line 508
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 496
    .end local v0    # "e":Ljava/lang/NullPointerException;
    .restart local v1    # "graphFragment":Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;
    :cond_4
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v4, v4, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    instance-of v4, v4, Lcom/sec/android/app/shealth/uv/state/UvStateReady;

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v4, v4, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    instance-of v4, v4, Lcom/sec/android/app/shealth/uv/state/UvStateFinished;

    if-eqz v4, :cond_6

    :cond_5
    move v4, v3

    goto :goto_1

    :cond_6
    move v4, v2

    goto :goto_1

    .line 498
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v5, v5, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    instance-of v5, v5, Lcom/sec/android/app/shealth/uv/state/UvStateReady;

    if-nez v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v5, v5, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    instance-of v5, v5, Lcom/sec/android/app/shealth/uv/state/UvStateFinished;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v5, :cond_3

    move v2, v3

    goto :goto_2
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 400
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 401
    const-string v0, "delete_popup"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->isShownPopup:Z

    .line 402
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 232
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onResume()V

    .line 234
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mIsOnConfigChanged:Z

    if-eqz v0, :cond_1

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSkinTypeDialog:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSkinTypeDialog:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSkinTypeDialog:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->updateText()V

    .line 238
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->dismissVisibleDialogs()V

    .line 240
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/uv/UvActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/uv/UvActivity$3;-><init>(Lcom/sec/android/app/shealth/uv/UvActivity;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 248
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mIsOnConfigChanged:Z

    .line 251
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mUvInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mUvInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 252
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mInfoView:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 253
    iput-object v4, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    .line 254
    iput-object v4, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mInfoView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->onInitWarningVideo(Landroid/view/View;)V

    .line 256
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->startInformationVideo()V

    .line 260
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->isShownPopup:Z

    if-eqz v0, :cond_3

    .line 261
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->showDeleteDialog()V

    .line 263
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->isDrawerMenuShown()Z

    move-result v0

    if-nez v0, :cond_4

    .line 264
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->customizeActionBar()V

    .line 269
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->ismFinishedState:Z

    if-eqz v0, :cond_5

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getStateFinished()Lcom/sec/android/app/shealth/uv/state/UvState;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->resetViews()V

    .line 274
    :cond_5
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 390
    const-string v0, "chart_shown"

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mGraphFragment:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->isVisible()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 391
    const-string v0, "delete_popup"

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->isShownPopup:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 392
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->ismFinishedState:Z

    if-eqz v0, :cond_0

    .line 393
    const-string/jumbo v0, "state_finished"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 395
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 396
    return-void
.end method

.method public startInformationVideo()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1080
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 1081
    sput-boolean v5, Lcom/sec/android/app/shealth/uv/UvActivity;->isDialogDissmised:Z

    .line 1082
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/Utils;->isHestiaModel()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1083
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->hestiaImages:[I

    aget v1, v1, v5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1084
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->informationHestiaImagesArray:[Ljava/lang/String;

    aget-object v1, v1, v5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 1085
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->hestiaImages:[I

    aget v1, v1, v6

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1086
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->informationHestiaImagesArray:[Ljava/lang/String;

    aget-object v1, v1, v6

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 1087
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->hestiaImages:[I

    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->informationHestiaImagesArray:[Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/uv/UvActivity;->animate(Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V

    .line 1097
    :cond_0
    :goto_0
    return-void

    .line 1089
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->images:[I

    aget v1, v1, v5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1090
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->informationImagesArray:[Ljava/lang/String;

    aget-object v1, v1, v5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 1091
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->images:[I

    aget v1, v1, v6

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1092
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->informationImagesArray:[Ljava/lang/String;

    aget-object v1, v1, v6

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 1093
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->images:[I

    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->informationImagesArray:[Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/uv/UvActivity;->animate(Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method public switchFragmentToGraph()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 684
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->fromDashboard:Z

    .line 685
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.uv"

    const-string v2, "UV04"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 686
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mGraphFragment:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    invoke-virtual {p0, v0, v3, v3}, Lcom/sec/android/app/shealth/uv/UvActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;ZZ)V

    .line 687
    return-void
.end method

.method public switchFragmentToSummary()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 678
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->hasNoUVSensor:Z

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->initUvSensorAvaialability(Z)V

    .line 679
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {p0, v0, v2, v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;ZZ)V

    .line 680
    return-void
.end method
