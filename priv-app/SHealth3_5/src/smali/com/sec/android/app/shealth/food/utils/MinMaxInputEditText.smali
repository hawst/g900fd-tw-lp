.class public Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;
.super Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;
.source "MinMaxInputEditText.java"


# instance fields
.field private mMinValue:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;-><init>(Landroid/content/Context;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    return-void
.end method


# virtual methods
.method public onEditorAction(I)V
    .locals 4
    .param p1, "actionCode"    # I

    .prologue
    .line 70
    const/4 v1, 0x0

    .line 73
    .local v1, "isInvalid":Z
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-nez v3, :cond_2

    .line 75
    const/4 v1, 0x1

    .line 91
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->onEditorAction(I)V

    .line 92
    const/4 v3, 0x6

    if-ne p1, v3, :cond_1

    .line 94
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->requestFocus()Z

    .line 95
    if-nez v1, :cond_1

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->setSelection(I)V

    .line 101
    :cond_1
    return-void

    .line 77
    :cond_2
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 79
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    .line 80
    .local v2, "value":F
    iget v3, p0, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->mMinValue:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    int-to-float v3, v3

    cmpg-float v3, v2, v3

    if-gez v3, :cond_0

    const/4 v3, 0x0

    cmpl-float v3, v2, v3

    if-lez v3, :cond_0

    .line 82
    const/4 v1, 0x1

    goto :goto_0

    .line 86
    .end local v2    # "value":F
    :catch_0
    move-exception v0

    .line 88
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v3, "MinMaxInputEditText"

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 46
    const/4 v2, 0x4

    if-ne p1, v2, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 50
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    .line 52
    .local v1, "value":F
    iget v2, p0, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->mMinValue:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    int-to-float v2, v2

    cmpl-float v2, v1, v2

    if-ltz v2, :cond_0

    .line 53
    const/4 v2, 0x0

    .line 62
    .end local v1    # "value":F
    :goto_0
    return v2

    .line 55
    :catch_0
    move-exception v0

    .line 56
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v2, "MinMaxInputEditText"

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 62
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v2

    goto :goto_0
.end method

.method public setMinValue(I)V
    .locals 0
    .param p1, "minValue"    # I

    .prologue
    .line 105
    iput p1, p0, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->mMinValue:I

    .line 106
    return-void
.end method
