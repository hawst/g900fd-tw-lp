.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;
.super Ljava/lang/Object;
.source "ExerciseProMapActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field lastMeasuredTime:J

.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V
    .locals 2

    .prologue
    .line 604
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 605
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->lastMeasuredTime:J

    return-void
.end method


# virtual methods
.method public onActivityTypeChanged(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 705
    return-void
.end method

.method public onChangedWorkoutStatus(I)V
    .locals 2
    .param p1, "status"    # I

    .prologue
    .line 693
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$6;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 700
    return-void
.end method

.method public onChannelStateChanged(ILjava/lang/String;)V
    .locals 2
    .param p1, "state"    # I
    .param p2, "uId"    # Ljava/lang/String;

    .prologue
    .line 623
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 629
    return-void
.end method

.method public onMaxDurationReached(J)V
    .locals 2
    .param p1, "second"    # J

    .prologue
    .line 681
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$5;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 689
    return-void
.end method

.method public onNewLocation(Landroid/location/Location;)V
    .locals 14
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 633
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->gpsSignalLostBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$2000(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 634
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->gpsSignalLostDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$2100(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 635
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->gpsSignalLostDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$2100(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    .line 637
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->gpsSignalLostBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$2002(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 639
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isInCenter:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->animationToCenterInProgress:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 640
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    iget-object v6, v6, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapCenter:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v4, v6, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    iget-object v6, v6, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapCenter:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v6, v6, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-static/range {v0 .. v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;->getDistanceFromLatLngInMeter(DDDD)D

    move-result-wide v2

    .line 641
    .local v2, "distanceFromCenter":D
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/google/android/gms/maps/GoogleMap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getCameraPosition()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    iget v0, v0, Lcom/google/android/gms/maps/model/CameraPosition;->zoom:F

    float-to-double v10, v0

    .line 643
    .local v10, "zoom":D
    const-wide/high16 v0, 0x3ffc000000000000L    # 1.75

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    const-wide/high16 v12, 0x4035000000000000L    # 21.0

    sub-double/2addr v12, v10

    invoke-static {v6, v7, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    mul-double v4, v0, v6

    .line 645
    .local v4, "thresholdDistance":D
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$3;

    move-object v1, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$3;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;DDLandroid/location/Location;)V

    invoke-virtual {v7, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 658
    .end local v2    # "distanceFromCenter":D
    .end local v4    # "thresholdDistance":D
    .end local v10    # "zoom":D
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$4;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$4;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;Landroid/location/Location;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 671
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 672
    .local v8, "currentTime":J
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->lastMeasuredTime:J

    sub-long v0, v8, v0

    const-wide/16 v6, 0x7d0

    cmp-long v0, v0, v6

    if-ltz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$1500(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v0

    const/16 v1, 0x7d1

    if-ne v0, v1, :cond_3

    .line 673
    iput-wide v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->lastMeasuredTime:J

    .line 674
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->playMapFromCacheDB(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$2200(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Z)V

    .line 677
    :cond_3
    return-void
.end method

.method public onScreenLockMode(Z)V
    .locals 2
    .param p1, "locked"    # Z

    .prologue
    .line 723
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$8;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$8;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;Z)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 730
    return-void
.end method

.method public onShownUnlockProgress(Z)V
    .locals 0
    .param p1, "isShown"    # Z

    .prologue
    .line 740
    return-void
.end method

.method public onStartWorkOut()V
    .locals 0

    .prologue
    .line 735
    return-void
.end method

.method public onUpdateGoalFromWorkoutController(II)V
    .locals 0
    .param p1, "goalType"    # I
    .param p2, "value"    # I

    .prologue
    .line 745
    return-void
.end method

.method public onUpdateLapClock(J)V
    .locals 1
    .param p1, "second"    # J

    .prologue
    .line 618
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->lapClockDispUpdate(J)V
    invoke-static {v0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$1800(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;J)V

    .line 619
    return-void
.end method

.method public onUpdateVaule(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 608
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 614
    return-void
.end method

.method public onVisualGuideShow(Z)V
    .locals 2
    .param p1, "isShow"    # Z

    .prologue
    .line 709
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isVisualGuideLayoutVisible:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$2500(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 710
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isVisualGuideLayoutVisible:Z
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$2502(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Z)Z

    .line 712
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$7;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 719
    :cond_0
    return-void
.end method
