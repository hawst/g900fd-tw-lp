.class Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$2;
.super Ljava/lang/Object;
.source "WalkingMateInactiveTimeSetTimeFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->getToButtonController()Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 9
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 105
    sget-object v4, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne p1, v4, :cond_0

    .line 106
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mToPicker:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$800(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->getDialog()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v3

    .line 108
    .local v3, "todialog":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getView()Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 110
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mToPicker:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$800(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->getDialog()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getView()Landroid/view/View;

    move-result-object v4

    const v5, 0x7f080a8c

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TimePicker;

    .line 111
    .local v0, "localInstance":Landroid/widget/TimePicker;
    invoke-virtual {v0}, Landroid/widget/TimePicker;->clearFocus()V

    .line 112
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mCalendar:Ljava/util/Calendar;
    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$100(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v0}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v0}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mToPicker:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;
    invoke-static {v8}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$800(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    move-result-object v8

    # invokes: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->setTime(Ljava/util/Calendar;IILcom/sec/android/app/shealth/framework/ui/common/TimePicker;)V
    invoke-static {v4, v5, v6, v7, v8}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$200(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;Ljava/util/Calendar;IILcom/sec/android/app/shealth/framework/ui/common/TimePicker;)V

    .line 113
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->timeFormatter:Ljava/text/SimpleDateFormat;
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$400(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Ljava/text/SimpleDateFormat;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mToPicker:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$800(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    move-result-object v6

    # invokes: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->getMeasureTimeInMillis(Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;)J
    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$300(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 116
    .local v1, "selectedTime":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mToPicker:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;
    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$800(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->getCurrentHour()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mToPicker:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$800(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->getCurrentMinute()I

    move-result v6

    # invokes: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->validateToTime(II)Z
    invoke-static {v4, v5, v6}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$900(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;II)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 117
    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setInactiveTimeTrackingAvailableTimeTo(Ljava/lang/String;)V

    .line 125
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 127
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mToButton:Landroid/widget/Button;
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$1000(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Landroid/widget/Button;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 130
    .end local v0    # "localInstance":Landroid/widget/TimePicker;
    .end local v1    # "selectedTime":Ljava/lang/String;
    .end local v3    # "todialog":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    :cond_0
    return-void

    .line 119
    .restart local v0    # "localInstance":Landroid/widget/TimePicker;
    .restart local v1    # "selectedTime":Ljava/lang/String;
    .restart local v3    # "todialog":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getInactiveTimeTrackingAvailableTimeTo()Ljava/lang/String;

    move-result-object v1

    .line 120
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->getUtilDate(Ljava/lang/String;)Ljava/util/Date;
    invoke-static {v4, v1}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$600(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    .line 121
    .local v2, "tempDate":Ljava/util/Date;
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mCalendar:Ljava/util/Calendar;
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$100(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 122
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mToPicker:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$800(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mCalendar:Ljava/util/Calendar;
    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$100(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->setMeasureTime(J)Z

    goto :goto_0

    .line 125
    .end local v2    # "tempDate":Ljava/util/Date;
    :cond_2
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->get12Hrformat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
