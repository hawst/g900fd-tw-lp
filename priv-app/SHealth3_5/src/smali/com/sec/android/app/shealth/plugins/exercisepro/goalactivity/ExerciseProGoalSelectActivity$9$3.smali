.class Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9$3;
.super Ljava/lang/Object;
.source "ExerciseProGoalSelectActivity.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;->onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;)V
    .locals 0

    .prologue
    .line 320
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9$3;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Z

    .prologue
    .line 324
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9$3;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->mMaxHrEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 325
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->isHardKeyBoardPresent(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9$3;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9$3;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->mMaxHrEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->showKeyboard(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 332
    :cond_0
    :goto_0
    return-void

    .line 329
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9$3;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9$3;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->mMaxHrEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->hideKeyboard(Landroid/content/Context;Landroid/widget/EditText;)V

    goto :goto_0
.end method
