.class final enum Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;
.super Ljava/lang/Enum;
.source "SelectFavoriteActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "SortType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;

.field public static final enum CATEGORY:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;

.field public static final enum TITLE:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 55
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;

    const-string v1, "CATEGORY"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;->CATEGORY:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;

    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;

    const-string v1, "TITLE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;->TITLE:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;->CATEGORY:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;->TITLE:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;->$VALUES:[Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 55
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;->$VALUES:[Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;

    return-object v0
.end method
