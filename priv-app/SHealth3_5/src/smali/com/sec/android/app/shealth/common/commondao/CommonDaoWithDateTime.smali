.class public interface abstract Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime;
.super Ljava/lang/Object;
.source "CommonDaoWithDateTime.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commondao/CommonDao;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/sec/android/app/shealth/common/commondao/CommonDao",
        "<TT;>;"
    }
.end annotation


# virtual methods
.method public abstract deleteAllDataForDay(J)I
.end method

.method public abstract deleteAllDataForPeriod(JJ)I
.end method

.method public abstract getAggregationFunctionResultForPeriod(JJLcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;)F
.end method

.method public abstract getDataForDay(J)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract getDataForPeriod(JJ)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract getDataForPeriod(JJLcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract getDataForPeriodsWithItAggregation(JJLcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;",
            "Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime$SqlAggregateFunction;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getLastCreatedData()Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public abstract getLastDataBeforeTime(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TT;"
        }
    .end annotation
.end method

.method public abstract getLastDataForDay(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TT;"
        }
    .end annotation
.end method

.method public abstract getLastUpdatedData()Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public abstract getNearestDateWithDataAfter(J)Ljava/lang/Long;
.end method

.method public abstract getNearestDateWithDataBefore(J)Ljava/lang/Long;
.end method

.method public abstract getNextDayWithData(J)Ljava/lang/Long;
.end method

.method public abstract getPreviousDayWithData(J)Ljava/lang/Long;
.end method
