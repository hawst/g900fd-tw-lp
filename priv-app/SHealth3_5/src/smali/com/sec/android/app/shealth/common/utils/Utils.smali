.class public Lcom/sec/android/app/shealth/common/utils/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/common/utils/Utils$BoundaryChecker;,
        Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelperThatShowsToast;,
        Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelper;
    }
.end annotation


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.samsung.helphub.provider.downloadable"

.field private static final LOG_TAG:Ljava/lang/String;

.field public static final LOSSLESS_QUALITY:I = 0x64

.field private static final PROFILE_BIRTH_DATE_FORMAT:Ljava/lang/String; = "yyyyMMdd"

.field public static final PROVIDER_URI:Ljava/lang/String; = "content://com.samsung.helphub.provider.downloadable"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-class v0, Lcom/sec/android/app/shealth/common/utils/Utils;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/Utils;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 386
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/Utils;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static closeStream(Ljava/io/Closeable;)V
    .locals 3
    .param p0, "stream"    # Ljava/io/Closeable;

    .prologue
    .line 93
    if-eqz p0, :cond_0

    .line 94
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 95
    :catch_0
    move-exception v0

    .line 96
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/Utils;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Cannot close stream"

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static varargs closeStreams([Ljava/io/Closeable;)V
    .locals 4
    .param p0, "streams"    # [Ljava/io/Closeable;

    .prologue
    .line 105
    move-object v0, p0

    .local v0, "arr$":[Ljava/io/Closeable;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 106
    .local v3, "stream":Ljava/io/Closeable;
    invoke-static {v3}, Lcom/sec/android/app/shealth/common/utils/Utils;->closeStream(Ljava/io/Closeable;)V

    .line 105
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 108
    .end local v3    # "stream":Ljava/io/Closeable;
    :cond_0
    return-void
.end method

.method public static convertDateStringToLong(Ljava/lang/String;)J
    .locals 8
    .param p0, "date"    # Ljava/lang/String;

    .prologue
    const-wide/16 v3, -0x1

    .line 543
    if-nez p0, :cond_1

    .line 545
    sget-object v5, Lcom/sec/android/app/shealth/common/utils/Utils;->LOG_TAG:Ljava/lang/String;

    const-string v6, "date object is null"

    invoke-static {v5, v6}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 560
    :cond_0
    :goto_0
    return-wide v3

    .line 548
    :cond_1
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string/jumbo v5, "yyyyMMdd"

    invoke-direct {v2, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 549
    .local v2, "sdf":Ljava/text/SimpleDateFormat;
    const/4 v0, 0x0

    .line 552
    .local v0, "convertedDate":Ljava/util/Date;
    :try_start_0
    invoke-virtual {v2, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 560
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    goto :goto_0

    .line 554
    :catch_0
    move-exception v1

    .line 556
    .local v1, "e":Ljava/text/ParseException;
    sget-object v5, Lcom/sec/android/app/shealth/common/utils/Utils;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unable to parse given date : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " . date format must be "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "yyyyMMdd"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static convertDpToPx(Landroid/content/Context;I)F
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dp"    # I

    .prologue
    .line 525
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/content/res/Resources;I)F

    move-result v0

    return v0
.end method

.method public static convertDpToPx(Landroid/content/res/Resources;I)F
    .locals 1
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "dp"    # I

    .prologue
    .line 529
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/util/DisplayMetrics;I)F

    move-result v0

    return v0
.end method

.method public static convertDpToPx(Landroid/util/DisplayMetrics;I)F
    .locals 2
    .param p0, "displayMetrics"    # Landroid/util/DisplayMetrics;
    .param p1, "dp"    # I

    .prologue
    .line 533
    const/4 v0, 0x1

    int-to-float v1, p1

    invoke-static {v0, v1, p0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    return v0
.end method

.method public static disableClickFor(JLandroid/view/View;)V
    .locals 1
    .param p0, "millis"    # J
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 151
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/view/View;->setClickable(Z)V

    .line 152
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/Utils$1;

    invoke-direct {v0, p2}, Lcom/sec/android/app/shealth/common/utils/Utils$1;-><init>(Landroid/view/View;)V

    invoke-virtual {p2, v0, p0, p1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 158
    return-void
.end method

.method public static filterCollection(Ljava/util/Collection;Lcom/sec/android/app/shealth/common/utils/Filterable;)Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<TT;>;",
            "Lcom/sec/android/app/shealth/common/utils/Filterable",
            "<TT;>;)",
            "Ljava/util/Collection",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 503
    .local p0, "target":Ljava/util/Collection;, "Ljava/util/Collection<TT;>;"
    .local p1, "filter":Lcom/sec/android/app/shealth/common/utils/Filterable;, "Lcom/sec/android/app/shealth/common/utils/Filterable<TT;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 504
    .local v2, "result":Ljava/util/Collection;, "Ljava/util/Collection<TT;>;"
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 505
    .local v0, "element":Ljava/lang/Object;, "TT;"
    invoke-interface {p1, v0}, Lcom/sec/android/app/shealth/common/utils/Filterable;->isMatchingFilter(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 506
    invoke-interface {v2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 509
    .end local v0    # "element":Ljava/lang/Object;, "TT;"
    :cond_1
    return-object v2
.end method

.method public static formatDateToProfileDob(Ljava/util/Calendar;)Ljava/lang/String;
    .locals 3
    .param p0, "calendar"    # Ljava/util/Calendar;

    .prologue
    .line 571
    if-nez p0, :cond_0

    .line 573
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/Utils;->LOG_TAG:Ljava/lang/String;

    const-string v2, "calendar object is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 574
    const/4 v1, 0x0

    .line 577
    :goto_0
    return-object v1

    .line 576
    :cond_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyyMMdd"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 577
    .local v0, "sdf":Ljava/text/SimpleDateFormat;
    invoke-virtual {p0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static getCountryCode()Ljava/lang/String;
    .locals 9

    .prologue
    .line 227
    const/4 v2, 0x0

    .line 231
    .local v2, "countryCode":Ljava/lang/String;
    :try_start_0
    const-string v5, "android.os.SystemProperties"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 232
    .local v1, "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v5, "get"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v1, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 233
    .local v4, "method":Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string/jumbo v8, "ro.csc.country_code"

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 235
    const-string v5, "getSalesCode"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "ro.csc.country_code:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 241
    .end local v1    # "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v4    # "method":Ljava/lang/reflect/Method;
    :goto_0
    return-object v2

    .line 237
    :catch_0
    move-exception v3

    .line 239
    .local v3, "e":Ljava/lang/Exception;
    const-string v5, "countryCode"

    invoke-static {v5, v3}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static getFloatFromString(Ljava/lang/String;)F
    .locals 5
    .param p0, "data"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 362
    const-string v3, ""

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 370
    :goto_0
    return v2

    .line 365
    :cond_0
    const/16 v3, 0x2c

    const/16 v4, 0x2e

    :try_start_0
    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    .line 366
    .local v1, "temp":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x2

    if-ge v3, v4, :cond_1

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 367
    const-string v1, "0"

    .line 368
    :cond_1
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 369
    .end local v1    # "temp":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 370
    .local v0, "e":Ljava/lang/NumberFormatException;
    goto :goto_0
.end method

.method public static varargs getMax([I)I
    .locals 5
    .param p0, "candidates"    # [I

    .prologue
    .line 114
    const v2, -0x7fffffff

    .line 115
    .local v2, "currentMaximum":I
    move-object v0, p0

    .local v0, "arr$":[I
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget v1, v0, v3

    .line 116
    .local v1, "candidate":I
    if-le v1, v2, :cond_0

    move v2, v1

    .line 115
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 118
    .end local v1    # "candidate":I
    :cond_1
    return v2
.end method

.method public static hasPackage(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 6
    .param p0, "c"    # Landroid/content/Context;
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    .line 209
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 210
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const/4 v1, 0x1

    .line 212
    .local v1, "hasPkg":Z
    const/16 v3, 0x80

    :try_start_0
    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 218
    :goto_0
    return v1

    .line 213
    :catch_0
    move-exception v0

    .line 214
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v1, 0x0

    .line 215
    sget-object v3, Lcom/sec/android/app/shealth/common/utils/Utils;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Package not found : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static isDownloadbleHelpApp(Landroid/content/Context;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x0

    .line 167
    const/4 v6, 0x0

    .line 168
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 170
    .local v7, "isDown":Z
    :try_start_0
    const-string v0, "content://com.samsung.helphub.provider.downloadable/isdownloadable"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 171
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 172
    if-eqz v6, :cond_0

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-gtz v0, :cond_2

    .line 185
    :cond_0
    if-eqz v6, :cond_1

    .line 186
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    move v0, v8

    .line 188
    .end local v1    # "uri":Landroid/net/Uri;
    :goto_0
    return v0

    .line 176
    .restart local v1    # "uri":Landroid/net/Uri;
    :cond_2
    if-eqz v6, :cond_3

    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 177
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    .line 178
    const/4 v7, 0x1

    .line 185
    :cond_3
    if-eqz v6, :cond_4

    .line 186
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v1    # "uri":Landroid/net/Uri;
    :cond_4
    :goto_1
    move v0, v7

    .line 188
    goto :goto_0

    .line 180
    :catch_0
    move-exception v0

    .line 185
    if-eqz v6, :cond_4

    .line 186
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 185
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_5

    .line 186
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
.end method

.method public static isEmpty(Ljava/lang/CharSequence;)Z
    .locals 1
    .param p0, "charSequence"    # Ljava/lang/CharSequence;

    .prologue
    .line 444
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public static isEmptyAfterTrim(Ljava/lang/CharSequence;)Z
    .locals 1
    .param p0, "charSequence"    # Ljava/lang/CharSequence;

    .prologue
    .line 454
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Utils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isFileExist(Ljava/lang/String;)Z
    .locals 1
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 520
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method

.method public static isHestiaModel()Z
    .locals 4

    .prologue
    .line 481
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 483
    .local v0, "mDeviceId":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/Utils;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isHestiaModel() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 484
    if-eqz v0, :cond_0

    const-string v1, "SM-G739F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 486
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/Utils;->LOG_TAG:Ljava/lang/String;

    const-string v2, "isHestiaModel() : This is Hesita Model"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 487
    const/4 v1, 0x1

    .line 490
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isLDUDevice()Z
    .locals 2

    .prologue
    .line 465
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 467
    .local v0, "mDeviceId":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "SM-G900X"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 469
    const/4 v1, 0x1

    .line 472
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isSupportHelpMenu(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 195
    const/4 v0, 0x0

    .line 196
    .local v0, "helpMenuCheck":Z
    const-string v1, "com.samsung.helpplugin"

    invoke-static {p0, v1}, Lcom/sec/android/app/shealth/common/utils/Utils;->hasPackage(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 198
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/Utils;->isDownloadbleHelpApp(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 201
    .end local v0    # "helpMenuCheck":Z
    :goto_0
    return v0

    .restart local v0    # "helpMenuCheck":Z
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isSupportOnlineHelpMenu()Z
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 248
    const/4 v3, 0x0

    .line 249
    .local v3, "useOnlineHelpDevice":Z
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/Utils;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    .line 253
    .local v0, "countryCode":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string v7, "com.samsung.helphub"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 254
    .local v2, "info":Landroid/content/pm/PackageInfo;
    iget v6, v2, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v7, 0x3

    if-ne v6, v7, :cond_0

    move v3, v4

    .line 259
    .end local v2    # "info":Landroid/content/pm/PackageInfo;
    :goto_0
    if-eqz v3, :cond_1

    if-eqz v0, :cond_1

    const-string v6, "USA"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 262
    :goto_1
    return v5

    .restart local v2    # "info":Landroid/content/pm/PackageInfo;
    :cond_0
    move v3, v5

    .line 254
    goto :goto_0

    .line 255
    .end local v2    # "info":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v1

    .line 256
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    move v5, v4

    .line 262
    goto :goto_1
.end method

.method public static logThrowable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 85
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 86
    return-void
.end method

.method public static logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 73
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 74
    return-void
.end method

.method public static swapWidthAndHeight(Landroid/view/View;)V
    .locals 3
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 125
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 126
    .local v1, "viewLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    iget v0, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 127
    .local v0, "oldHeight":I
    iget v2, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 128
    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 129
    invoke-virtual {p0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 130
    return-void
.end method

.method public static varargs temporarilyDisableClick([Landroid/view/View;)V
    .locals 6
    .param p0, "views"    # [Landroid/view/View;

    .prologue
    .line 138
    move-object v0, p0

    .local v0, "arr$":[Landroid/view/View;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 139
    .local v3, "view":Landroid/view/View;
    const-wide/16 v4, 0x64

    invoke-static {v4, v5, v3}, Lcom/sec/android/app/shealth/common/utils/Utils;->disableClickFor(JLandroid/view/View;)V

    .line 138
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 141
    .end local v3    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method
