.class public interface abstract Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;
.super Ljava/lang/Object;
.source "IMealImagesFragmentAction.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/mealinput/FragmentModeSettable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction$IMealImagesQuantityWatcher;
    }
.end annotation


# virtual methods
.method public abstract addAndResizeMealImage(Ljava/lang/String;)V
.end method

.method public abstract addListMealImages(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract getAllMealImages()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getDeletedImagesList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract resetAllData()V
.end method
