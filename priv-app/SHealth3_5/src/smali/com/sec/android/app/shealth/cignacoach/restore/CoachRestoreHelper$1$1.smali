.class Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1$1;
.super Ljava/lang/Object;
.source "CoachRestoreHelper.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1$1;->this$1:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 3
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1$1;->this$1:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;

    iget-object v1, v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    const v0, 0x7f08008a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressMsg:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$302(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1$1;->this$1:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressMsg:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$300(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1$1;->this$1:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;

    iget-object v1, v1, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$200(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1$1;->this$1:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;

    iget v2, v2, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;->val$popup_msg:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1$1;->this$1:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;

    iget-object v1, v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    const v0, 0x7f08008b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    # setter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$402(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Landroid/widget/ProgressBar;)Landroid/widget/ProgressBar;

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1$1;->this$1:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;

    iget-object v1, v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    const v0, 0x7f08008d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgress:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$502(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1$1;->this$1:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgress:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$500(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1$1;->this$1:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;

    iget-object v1, v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    const v0, 0x7f08008c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$602(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1$1;->this$1:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$400(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1$1;->this$1:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$400(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 140
    return-void
.end method
