.class Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$CalendarFragmentAdapter;
.super Landroid/support/v4/app/FragmentStatePagerAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CalendarFragmentAdapter"
.end annotation


# instance fields
.field private mIsFinal:Z

.field private final mPeriodType:I

.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;Landroid/support/v4/app/FragmentManager;Ljava/util/List;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/FragmentManager;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;IZ)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$CalendarFragmentAdapter;->this$0:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;

    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentStatePagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    iput-boolean p5, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$CalendarFragmentAdapter;->mIsFinal:Z

    iput p4, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$CalendarFragmentAdapter;->mPeriodType:I

    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    if-eqz p3, :cond_0

    if-ltz p2, :cond_0

    move-object v0, p3

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentStatePagerAdapter;->destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$CalendarFragmentAdapter;->this$0:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPeriodStarts:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->access$800(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 9

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$CalendarFragmentAdapter;->this$0:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$CalendarFragmentAdapter;->this$0:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPeriodStarts:Ljava/util/List;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->access$800(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget v3, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$CalendarFragmentAdapter;->mPeriodType:I

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$CalendarFragmentAdapter;->this$0:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mSelectedTime:J
    invoke-static {v4}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->access$900(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;)J

    move-result-wide v4

    iget-object v6, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$CalendarFragmentAdapter;->this$0:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mDataType:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->access$1000(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$CalendarFragmentAdapter;->this$0:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mTimeColumnName:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->access$1100(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;)Ljava/lang/String;

    move-result-object v7

    iget-boolean v8, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$CalendarFragmentAdapter;->mIsFinal:Z

    invoke-static/range {v0 .. v8}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->getInstance(Landroid/content/Context;JIJLjava/lang/String;Ljava/lang/String;Z)Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;

    move-result-object v0

    return-object v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 3

    check-cast p1, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->getStartTime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity$CalendarFragmentAdapter;->this$0:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->mPeriodStarts:Ljava/util/List;
    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->access$800(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;)Ljava/util/List;

    move-result-object v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
