.class public Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
.super Ljava/lang/Object;
.source "RestoreHelper2.java"

# interfaces
.implements Lcom/sec/android/app/shealth/backup/server/IRestoreListener;
.implements Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$29;,
        Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ExceptionObject;,
        Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;,
        Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$IRestoreCompleteListener;,
        Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;
    }
.end annotation


# static fields
.field private static final CHECKING_SERVER_POPUP:Ljava/lang/String; = "CHECKING_SERVER_POPUP"

.field private static final CONFIRM_IMPORT:Ljava/lang/String; = "CONFIRM_IMPORT"

.field private static final CONFIRM_RESTORE:Ljava/lang/String; = "CONFIRM_RESTORE"

.field public static final CONFIRM_RESTORE_WITHOUT_CHECKING_SERVER:Ljava/lang/String; = "CONFIRM_RESTORE_WITHOUT_CHECKING_SERVER"

.field private static final CONN_TIMEOUT:I = 0x2710

.field private static final ERASING_PROFILE_IN_SERVER:Ljava/lang/String; = "PRO_5411"

.field private static final LOW_STORAGE_FAILED_POPUP:Ljava/lang/String; = "LOW_STORAGE_FAILED_POPUP"

.field private static final NETWORK_FAILED_POPUP:Ljava/lang/String; = "NETWORK_FAILED_POPUP"

.field private static final NO_DATA_TO_RESTORE:Ljava/lang/String; = "NO_DATA_TO_RESTORE"

.field private static final NO_PROFILE_IN_SERVER:Ljava/lang/String; = "PRO_5412"

.field private static final PRIVATE_ID_REQUEST_PROFILE:I = 0x65

.field private static final RESTORE_PROGRESS_POPUP:Ljava/lang/String; = "RESTORE_PROGRESS_POPUP"

.field private static final SERVER_MAINTENANCE_FAILED_POPUP:Ljava/lang/String; = "SERVER_MAINTENANCE_FAILED_POPUP"

.field private static final SO_TIMEOUT:I = 0x7530

.field private static final TAG:Ljava/lang/String;

.field private static final WIFI_NOTIFICATION:Ljava/lang/String; = "WIFI_NOTIFICATION"

.field public static final WIFI_PICK_RESTORE_ID:I = 0x138a


# instance fields
.field private ERROR_CODE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

.field private SERVER_ERROR_MESSAGE:Ljava/lang/String;

.field private cignaBackupRestoreEnable:Z

.field private currentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

.field private isBackup:Z

.field private isConfirmed:Z

.field private isFwGRestoreFromHome:Z

.field private mBackupRestore:Landroid/widget/CheckBox;

.field private mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mConnectionManagerInstance:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

.field private mContext:Landroid/content/Context;

.field private mDoNotShowAgain:Landroid/widget/CheckBox;

.field private mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mIsCancelRequestHandled:Z

.field private mMigrationState:I

.field private mNoDataDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mPopupHandler:Landroid/os/Handler;

.field private mRestoreManager:Lcom/sec/android/app/shealth/backup/server/RestoreManager;

.field private mRestoreProgress:Landroid/widget/TextView;

.field private mRestoreProgressBar:Landroid/widget/ProgressBar;

.field private mRestoreProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

.field private mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mRestoreProgressMsg:Landroid/widget/TextView;

.field private mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mServerCheckingDialogBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

.field private mSetMaxRestoreSize:D

.field private mTextPercentage:Landroid/widget/TextView;

.field private mTryToRenewToken:Z

.field private mWifiNotiDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private netUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

.field private noData1:Z

.field private noData2:Z

.field private popupDescRID:I

.field private popupLayoutRID:I

.field private popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

.field private profileRestoreReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;

.field private shcm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

.field private wl:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 88
    const-class v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mIsCancelRequestHandled:Z

    .line 116
    const v0, 0x7f030187

    iput v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupLayoutRID:I

    .line 117
    const v0, 0x7f090d43

    iput v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupDescRID:I

    .line 120
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->netUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    .line 129
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mTryToRenewToken:Z

    .line 130
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mPopupHandler:Landroid/os/Handler;

    .line 132
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 133
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 134
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 135
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mNoDataDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 136
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mWifiNotiDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 137
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 139
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->currentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 140
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 141
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->NETWORK_FAILURE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->ERROR_CODE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 142
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->SERVER_ERROR_MESSAGE:Ljava/lang/String;

    .line 154
    iput v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mMigrationState:I

    .line 155
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->isFwGRestoreFromHome:Z

    .line 157
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->cignaBackupRestoreEnable:Z

    .line 191
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    .line 192
    new-instance v0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->shcm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    .line 193
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->netUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    .line 194
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 195
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mPopupHandler:Landroid/os/Handler;

    .line 196
    new-instance v0, Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreManager:Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->getMigrationState(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mMigrationState:I

    .line 200
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->isCoachBREnabled(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->cignaBackupRestoreEnable:Z

    .line 201
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->cignaBackupRestore(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Landroid/widget/ProgressBar;)Landroid/widget/ProgressBar;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    .param p1, "x1"    # Landroid/widget/ProgressBar;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressBar:Landroid/widget/ProgressBar;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->stopBackupOrRestore()V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->noData1:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    .param p1, "x1"    # Z

    .prologue
    .line 83
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->noData1:Z

    return p1
.end method

.method static synthetic access$1302(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    .param p1, "x1"    # Z

    .prologue
    .line 83
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->noData2:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->finish()V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->restartApp()V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->requestProfileFromServer(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1900(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mIsCancelRequestHandled:Z

    return v0
.end method

.method static synthetic access$1902(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    .param p1, "x1"    # Z

    .prologue
    .line 83
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mIsCancelRequestHandled:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mTextPercentage:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->showRestoreConfirmation(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mTextPercentage:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mBackupRestore:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$2102(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    .param p1, "x1"    # Landroid/widget/CheckBox;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mBackupRestore:Landroid/widget/CheckBox;

    return-object p1
.end method

.method static synthetic access$2200(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->startBackup()V

    return-void
.end method

.method static synthetic access$2302(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    .param p1, "x1"    # Z

    .prologue
    .line 83
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->isFwGRestoreFromHome:Z

    return p1
.end method

.method static synthetic access$2400(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    iget v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupDescRID:I

    return v0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mDoNotShowAgain:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$2502(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    .param p1, "x1"    # Landroid/widget/CheckBox;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mDoNotShowAgain:Landroid/widget/CheckBox;

    return-object p1
.end method

.method static synthetic access$2600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$2602(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object p1
.end method

.method static synthetic access$2700(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/common/utils/NetworkUtils;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->netUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->checkRestoreData()V

    return-void
.end method

.method static synthetic access$2900(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->isConfirmed:Z

    return v0
.end method

.method static synthetic access$2902(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    .param p1, "x1"    # Z

    .prologue
    .line 83
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->isConfirmed:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->startImport()V

    return-void
.end method

.method static synthetic access$3100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->isBackup:Z

    return v0
.end method

.method static synthetic access$3200(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mPopupHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$3302(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object p1
.end method

.method static synthetic access$3400(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->currentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mConnectionManagerInstance:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/os/PowerManager$WakeLock;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->wl:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static synthetic access$3602(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Landroid/os/PowerManager$WakeLock;)Landroid/os/PowerManager$WakeLock;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    .param p1, "x1"    # Landroid/os/PowerManager$WakeLock;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->wl:Landroid/os/PowerManager$WakeLock;

    return-object p1
.end method

.method static synthetic access$3700(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/backup/server/RestoreManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreManager:Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->profileRestoreReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;

    return-object v0
.end method

.method static synthetic access$3802(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->profileRestoreReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    iget v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mMigrationState:I

    return v0
.end method

.method static synthetic access$4000(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .prologue
    .line 83
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->showFailurePopup(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;)V

    return-void
.end method

.method static synthetic access$4100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->showRestoreFailedPopup()V

    return-void
.end method

.method static synthetic access$4200(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->cancelImport()V

    return-void
.end method

.method static synthetic access$4300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    return-object v0
.end method

.method static synthetic access$4302(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    return-object p1
.end method

.method static synthetic access$4400(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressMsg:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4402(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressMsg:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$4500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgress:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4502(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgress:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->checkImportData()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object p1
.end method

.method static synthetic access$702(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->ERROR_CODE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->SERVER_ERROR_MESSAGE:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->SERVER_ERROR_MESSAGE:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->cignaBackupRestoreEnable:Z

    return v0
.end method

.method private cancelImport()V
    .locals 2

    .prologue
    .line 1618
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string v1, "cancelImport"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1621
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->profileRestoreReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;

    if-eqz v0, :cond_0

    .line 1622
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->profileRestoreReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1623
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->profileRestoreReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;

    .line 1626
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreManager:Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->cancel()V

    .line 1627
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->cancelMigration()V

    .line 1628
    return-void
.end method

.method private checkImportData()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 294
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string v1, "checkImportData()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->isDataIncludeImportedData(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isChinaModel()Z

    move-result v0

    if-nez v0, :cond_4

    .line 298
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->isConfirmed:Z

    if-nez v0, :cond_1

    .line 300
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->CHECK_DATA_TO_IMPORT:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->currentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 301
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreManager:Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->checkDataToRestore(Lcom/sec/android/app/shealth/backup/server/IRestoreListener;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 303
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->onException(Ljava/lang/Exception;)V

    .line 324
    :cond_0
    :goto_0
    return-void

    .line 308
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->noData1:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreManager:Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->checkDataToRestore(Lcom/sec/android/app/shealth/backup/server/IRestoreListener;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 310
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 313
    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 315
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->showPopupWithProgressBar()V

    .line 317
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->startImport()V

    goto :goto_0

    .line 322
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->onNoData()V

    goto :goto_0
.end method

.method private checkRestoreData()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 276
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string v1, "checkRestoreData()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->noData1:Z

    .line 278
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->noData2:Z

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->netUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isNetworkConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 282
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->REQUEST_TOKEN:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->currentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 283
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->showServerCheckingPopup()V

    .line 284
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->getUserTokenForRestore()V

    .line 290
    :goto_0
    return-void

    .line 288
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->onException(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private cignaBackupRestore(Ljava/lang/String;)Z
    .locals 4
    .param p1, "isBackUpRestore"    # Ljava/lang/String;

    .prologue
    .line 328
    new-instance v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;-><init>(Landroid/content/Context;)V

    .line 329
    .local v0, "helper":Lcom/cigna/coach/utils/backuprestore/JournalHelper;
    const/4 v1, 0x0

    .line 330
    .local v1, "result":Z
    const-string v2, "BACKUP"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 331
    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$1;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    invoke-virtual {v0, v2}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->backup(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z

    move-result v1

    .line 439
    :goto_0
    if-nez v1, :cond_0

    .line 440
    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string v3, "FAILED <in restoreActivity> "

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    :cond_0
    return v1

    .line 387
    :cond_1
    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$2;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    invoke-virtual {v0, v2}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->restore(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z

    move-result v1

    goto :goto_0
.end method

.method private finish()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2053
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2055
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 2056
    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 2059
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2061
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 2062
    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 2065
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->profileRestoreReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;

    if-eqz v0, :cond_2

    .line 2067
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->profileRestoreReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2068
    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->profileRestoreReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;

    .line 2071
    :cond_2
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 2073
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->wl:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_3

    .line 2075
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->wl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 2076
    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->wl:Landroid/os/PowerManager$WakeLock;

    .line 2079
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$IRestoreCompleteListener;

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->noData1:Z

    if-eqz v1, :cond_4

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->noData2:Z

    if-nez v1, :cond_5

    :cond_4
    const/4 v1, 0x1

    :goto_0
    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$IRestoreCompleteListener;->restoreCompleted(Z)V

    .line 2080
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 2081
    return-void

    .line 2079
    :cond_5
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getUserTokenForRestore()V
    .locals 5

    .prologue
    .line 793
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string v2, "3.0 Restore: get User Token about 3.0 Restoration"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 795
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mTryToRenewToken:Z

    .line 796
    invoke-static {}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->getInstance()Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    move-result-object v0

    .line 797
    .local v0, "mAccessTokenInstance":Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    const-string v2, "1y90e30264"

    const-string v3, "80E7ECD9D301CB7888C73703639302E5"

    new-instance v4, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$6;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$6;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->sendRequestForUserToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;)V

    .line 824
    return-void
.end method

.method private initConnectionManager()V
    .locals 8

    .prologue
    .line 848
    const-string v6, "checkData"

    .line 850
    .local v6, "checkData":Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mConnectionManagerInstance:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    .line 851
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mConnectionManagerInstance:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    if-nez v0, :cond_0

    .line 853
    invoke-static {v6}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->createInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    .line 854
    invoke-static {v6}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mConnectionManagerInstance:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    .line 857
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mConnectionManagerInstance:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    sget-object v1, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->HTTP_SERVER:Ljava/lang/String;

    sget v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->HTTP_PORT:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->setAddress(Ljava/lang/String;I)Z

    .line 858
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mConnectionManagerInstance:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    const/16 v1, 0x2710

    const/16 v2, 0x7530

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->initConnectionManager(IIZLjava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/sec/android/service/health/connectionmanager2/NetException; {:try_start_0 .. :try_end_0} :catch_0

    .line 867
    :cond_0
    :goto_0
    return-void

    .line 861
    :catch_0
    move-exception v7

    .line 863
    .local v7, "e":Lcom/sec/android/service/health/connectionmanager2/NetException;
    invoke-virtual {v7}, Lcom/sec/android/service/health/connectionmanager2/NetException;->printStackTrace()V

    .line 864
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->onException(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static isDataIncludeImportedData(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2160
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2161
    .local v0, "extras":Landroid/os/Bundle;
    const-string v3, "key"

    const-string v4, "SHEALTH_2_5_EXISTS"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2162
    const-string/jumbo v3, "value"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2163
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v5, "CONFIG_OPTION_GET"

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v2

    .line 2165
    .local v2, "retVal":Landroid/os/Bundle;
    const/4 v1, 0x0

    .line 2166
    .local v1, "isIncluded":Z
    if-eqz v2, :cond_0

    .line 2167
    const-string/jumbo v3, "value"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 2170
    :cond_0
    sget-object v4, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "any 2.5 data are included in the restored data? "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz v1, :cond_1

    const-string v3, "YES"

    :goto_0
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 2175
    return v1

    .line 2170
    :cond_1
    const-string v3, "NO"

    goto :goto_0
.end method

.method private requestProfileFromServer(Ljava/lang/String;)V
    .locals 11
    .param p1, "userToken"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 828
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string v1, "3.0 Restoration: callServerAPI"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 830
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/os/PowerManager;

    .line 831
    .local v10, "pm":Landroid/os/PowerManager;
    const-string v0, "RestoreManager"

    invoke-virtual {v10, v3, v0}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->wl:Landroid/os/PowerManager$WakeLock;

    .line 832
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->wl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 834
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->initConnectionManager()V

    .line 835
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->CHECK_PROFILE_FOR_RESTORE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->currentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 836
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mIsCancelRequestHandled:Z

    .line 837
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "InSide requestProfileFromServer mIsCancelRequestHandled: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mIsCancelRequestHandled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 838
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 839
    .local v9, "httpHeaders":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    const-string v0, "Accept"

    new-array v1, v3, [Ljava/lang/String;

    const-string v2, "application/json"

    aput-object v2, v1, v4

    invoke-virtual {v9, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 840
    const-string/jumbo v0, "reqAppId"

    new-array v1, v3, [Ljava/lang/String;

    const-string v2, "1y90e30264"

    aput-object v2, v1, v4

    invoke-virtual {v9, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 841
    const-string v0, "access_token"

    new-array v1, v3, [Ljava/lang/String;

    aput-object p1, v1, v4

    invoke-virtual {v9, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 842
    const-string v0, "deviceId"

    new-array v1, v3, [Ljava/lang/String;

    sget-object v2, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-virtual {v9, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 843
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mConnectionManagerInstance:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    const/16 v2, 0x65

    sget-object v3, Lcom/sec/android/service/health/connectionmanager2/MethodType;->POST:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    const-string/jumbo v4, "platform/profile/getUserProfile"

    const-string v6, ""

    move-object v1, p0

    move-object v7, p0

    move-object v8, v5

    invoke-virtual/range {v0 .. v9}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->placeRequest(Ljava/lang/Object;ILcom/sec/android/service/health/connectionmanager2/MethodType;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/RequestParam;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;Ljava/lang/Object;Ljava/util/HashMap;)J

    .line 844
    return-void
.end method

.method private restartApp()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    .line 2104
    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "restart SHealth app()"

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 2106
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->setSucceedReStoreFlag()V

    .line 2107
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->isDefaultPassword()I

    move-result v2

    if-ne v2, v6, :cond_0

    .line 2108
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->storeTemporaryPass()V

    .line 2113
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    invoke-static {v2, v4}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setRestoreAgainHomePopup(Landroid/content/Context;Z)V

    .line 2115
    iget v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mMigrationState:I

    if-eqz v2, :cond_1

    .line 2117
    invoke-static {}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->getInstance()Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->doNormalInitialization(Landroid/content/Context;)Z

    .line 2118
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/sec/android/service/health/keyManager/KeyManager;->setContentProviderState(Z)V

    .line 2121
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    invoke-static {v2, v4}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setInitializationNeeded(Landroid/content/Context;Z)V

    .line 2124
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->clearMigrationState(Landroid/content/Context;)V

    .line 2127
    :cond_1
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->isFwGRestoreFromHome:Z

    if-eqz v2, :cond_3

    .line 2128
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    invoke-static {v2, v4}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setRestoreOngoing(Landroid/content/Context;Z)V

    .line 2129
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    invoke-static {v2, v4}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setInitializationNeeded(Landroid/content/Context;Z)V

    .line 2135
    :goto_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2136
    .local v0, "extras":Landroid/os/Bundle;
    const-string v2, "key"

    const-string v3, "Is_Restore_Completed"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2137
    const-string/jumbo v2, "value"

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2138
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v4, "CONFIG_OPTION_PUT"

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 2141
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->finishAffinity()V

    .line 2143
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    const-class v3, Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2144
    .local v1, "intent_splash":Landroid/content/Intent;
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2145
    const/high16 v2, 0x20000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2146
    const-string v2, "com.sec.android.app.shealth"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2147
    iget v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mMigrationState:I

    if-eqz v2, :cond_2

    .line 2149
    const-string v2, "IS_UPGRADE_DONE"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2151
    :cond_2
    const-string/jumbo v2, "temps"

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/service/health/keyManager/KeyManager;->getRandomKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2152
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    const-class v3, Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 2153
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 2155
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-static {v2}, Landroid/os/Process;->killProcess(I)V

    .line 2157
    return-void

    .line 2131
    .end local v0    # "extras":Landroid/os/Bundle;
    .end local v1    # "intent_splash":Landroid/content/Intent;
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    invoke-static {v2, v6}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setRestoreOngoing(Landroid/content/Context;Z)V

    goto :goto_0
.end method

.method private showFailurePopup(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "dialogTag"    # Ljava/lang/String;
    .param p3, "status"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .prologue
    .line 1923
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 1924
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    iget v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mMigrationState:I

    if-nez v1, :cond_0

    .line 1925
    const v1, 0x7f090139

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1930
    :goto_0
    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1931
    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1933
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 1934
    iput-object p3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 1935
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v2, v1, p2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1936
    return-void

    .line 1927
    :cond_0
    const v1, 0x7f090055

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    goto :goto_0
.end method

.method private showRestoreConfirmation(Ljava/lang/String;)V
    .locals 8
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 919
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 921
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 922
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 924
    :cond_0
    const/4 v2, 0x0

    .line 925
    .local v2, "needToBackup":Z
    iget v6, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mMigrationState:I

    if-nez v6, :cond_1

    .line 928
    new-instance v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    invoke-direct {v3, v6}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 929
    .local v3, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getActivityType()I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_3

    const/4 v2, 0x1

    .line 932
    .end local v3    # "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    :cond_1
    :goto_0
    if-eqz v2, :cond_4

    .line 935
    const v6, 0x7f030188

    iput v6, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupLayoutRID:I

    .line 936
    const v6, 0x7f090d42

    iput v6, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupDescRID:I

    .line 937
    sget-object v6, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->CONFIRMATION_WITH_BACKUP_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    iput-object v6, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 946
    :goto_1
    const v4, 0x7f090139

    .line 949
    .local v4, "titleId":I
    const/4 v1, 0x2

    .line 950
    .local v1, "dialogButtonType":I
    iget v6, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mMigrationState:I

    if-eqz v6, :cond_2

    .line 951
    const/4 v1, 0x0

    .line 952
    const v4, 0x7f090055

    .line 956
    :cond_2
    new-instance v6, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    invoke-direct {v6, v7, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iget v7, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupLayoutRID:I

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v5

    const v6, 0x7f090139

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupLayoutRID:I

    new-instance v7, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$8;

    invoke-direct {v7, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$8;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    .line 1035
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    new-instance v5, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$9;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$9;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1044
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 1045
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    check-cast v5, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v6, v5, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1046
    return-void

    .end local v0    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .end local v1    # "dialogButtonType":I
    .end local v4    # "titleId":I
    .restart local v3    # "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    :cond_3
    move v2, v5

    .line 929
    goto :goto_0

    .line 942
    .end local v3    # "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    :cond_4
    const v6, 0x7f030187

    iput v6, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupLayoutRID:I

    .line 943
    const v6, 0x7f090d43

    iput v6, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupDescRID:I

    .line 944
    sget-object v6, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->CONFIRMATION_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    iput-object v6, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    goto :goto_1
.end method

.method private showRestoreFailedPopup()V
    .locals 4

    .prologue
    .line 1899
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "network error popup showing"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1901
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 1902
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    iget v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mMigrationState:I

    if-nez v1, :cond_0

    .line 1904
    const v1, 0x7f0907f3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1905
    const v1, 0x7f09004c

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1913
    :goto_0
    const v1, 0x7f0907f7

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1914
    const v1, 0x7f09004b

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1916
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 1917
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->FAILED_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 1918
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v3, "NETWORK_FAILED_POPUP"

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1919
    return-void

    .line 1909
    :cond_0
    const v1, 0x7f090d2c

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1910
    const v1, 0x7f090d2d

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    goto :goto_0
.end method

.method private showWiFiNotificationPopup(Ljava/lang/String;)V
    .locals 4
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 1774
    if-nez p1, :cond_0

    .line 1821
    :goto_0
    return-void

    .line 1778
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 1779
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const v1, 0x7f0907be

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1780
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    const v2, 0x7f090d27

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1781
    const v1, 0x7f09078d

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1782
    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$23;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$23;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1791
    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$24;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$24;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1807
    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$25;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$25;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1818
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->WIFI_NOTIFICATION_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 1819
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mWifiNotiDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 1820
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mWifiNotiDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v3, "WIFI_NOTIFICATION"

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private startBackup()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 448
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->isBackup:Z

    .line 449
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startBackup()"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->START_BACKUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->currentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 451
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->showPopupWithProgressBar()V

    .line 455
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->shcm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    const/4 v2, 0x1

    new-instance v3, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$3;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->startBackup(ILcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 553
    :goto_0
    return-void

    .line 547
    :catch_0
    move-exception v0

    .line 549
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 550
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->stopBackupOrRestore()V

    .line 551
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->onException(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private startImport()V
    .locals 1

    .prologue
    .line 1606
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreManager:Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->start(Lcom/sec/android/app/shealth/backup/server/IRestoreListener;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1614
    :goto_0
    return-void

    .line 1612
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->onException(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private stopBackupOrRestore()V
    .locals 4

    .prologue
    .line 771
    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "stopBackupOrRestore called for "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->isBackup:Z

    if-eqz v1, :cond_0

    const-string v1, "Backup"

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " work"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 774
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->isBackup:Z

    if-eqz v1, :cond_1

    .line 776
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->shcm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->stopBackup()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 787
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->closeProgressDialog()V

    .line 788
    return-void

    .line 771
    :cond_0
    const-string v1, "Restore"

    goto :goto_0

    .line 780
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->shcm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->stopRestore()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 783
    :catch_0
    move-exception v0

    .line 785
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public closeProgressDialog()V
    .locals 1

    .prologue
    .line 720
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 722
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 723
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 725
    :cond_0
    return-void
.end method

.method public closeServerCheckingPopup()V
    .locals 1

    .prologue
    .line 2002
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2003
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 2004
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 2006
    :cond_0
    return-void
.end method

.method public dismissAll()V
    .locals 1

    .prologue
    .line 1482
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1484
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    .line 1485
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 1488
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->closeProgressDialog()V

    .line 1489
    return-void
.end method

.method public finalize()V
    .locals 1

    .prologue
    .line 2182
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->wl:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 2184
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->wl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 2185
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->wl:Landroid/os/PowerManager$WakeLock;

    .line 2187
    :cond_0
    return-void
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 1172
    const-string v0, "NETWORK_FAILED_POPUP"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1174
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    .line 1432
    :goto_0
    return-object v0

    .line 1261
    :cond_0
    const-string v0, "CONFIRM_RESTORE"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "CONFIRM_RESTORE_WITHOUT_CHECKING_SERVER"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "CONFIRM_IMPORT"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1263
    :cond_1
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$13;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$13;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Ljava/lang/String;)V

    goto :goto_0

    .line 1333
    :cond_2
    const-string v0, "NO_DATA_TO_RESTORE"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1335
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$14;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$14;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    goto :goto_0

    .line 1347
    :cond_3
    const-string v0, "CHECKING_SERVER_POPUP"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1348
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$15;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$15;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    goto :goto_0

    .line 1391
    :cond_4
    const-string v0, "LOW_STORAGE_FAILED_POPUP"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1392
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$16;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$16;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    goto :goto_0

    .line 1410
    :cond_5
    const-string v0, "SERVER_MAINTENANCE_FAILED_POPUP"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1411
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$17;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$17;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    goto :goto_0

    .line 1432
    :cond_6
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public handleUserInteraction(IILandroid/content/Intent;)Z
    .locals 6
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    const/4 v5, -0x1

    .line 2010
    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleUserInteraction requestCode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", resultCode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 2011
    sparse-switch p1, :sswitch_data_0

    .line 2048
    :cond_0
    :goto_0
    return v1

    .line 2014
    :sswitch_0
    if-ne p2, v5, :cond_1

    .line 2016
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string v2, "Wifi connected and starting restore !!"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2017
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 2018
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->showPopupWithProgressBar()V

    .line 2019
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreManager:Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->resume()V

    .line 2027
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 2023
    :cond_1
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string v2, "Restore cancelled !!"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2024
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 2025
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->cancelImport()V

    goto :goto_1

    .line 2030
    :sswitch_1
    if-ne p2, v5, :cond_2

    .line 2032
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->getUserTokenForRestore()V

    goto :goto_0

    .line 2033
    :cond_2
    if-eqz p3, :cond_0

    .line 2034
    const-string v2, "error_code"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2035
    .local v0, "errorCode":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "errorCode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2036
    if-eqz v0, :cond_3

    const-string v2, "SAC_0204"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2037
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->requestAccessToken(Landroid/content/Context;)V

    goto :goto_0

    .line 2038
    :cond_3
    if-eqz v0, :cond_0

    const-string v2, "SAC_0203"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2039
    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string v3, "entered sac_0203!!"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2040
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    const v3, 0x7f090801

    invoke-static {v2, v3, v1}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 2011
    :sswitch_data_0
    .sparse-switch
        0xa25 -> :sswitch_1
        0xa26 -> :sswitch_1
        0x138a -> :sswitch_0
    .end sparse-switch
.end method

.method public onAccountException(Ljava/lang/String;)V
    .locals 2
    .param p1, "errMsg"    # Ljava/lang/String;

    .prologue
    .line 1714
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string v1, "Import, Samsung account app issue occured"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1716
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->closeServerCheckingPopup()V

    .line 1717
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->requestCheckValication(Landroid/content/Context;)V

    .line 1718
    return-void
.end method

.method public onData()V
    .locals 2

    .prologue
    .line 1723
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string v1, "2.5 Migrations check result : RESTORE"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1726
    const-string v0, "CONFIRM_IMPORT"

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->showRestoreConfirmation(Ljava/lang/String;)V

    .line 1727
    return-void
.end method

.method public onException(Ljava/lang/Exception;)V
    .locals 4
    .param p1, "ex"    # Ljava/lang/Exception;

    .prologue
    const/4 v3, 0x0

    .line 1633
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Import onException: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1636
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1638
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 1639
    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 1642
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->closeProgressDialog()V

    .line 1644
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->profileRestoreReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;

    if-eqz v0, :cond_1

    .line 1645
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->profileRestoreReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1646
    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->profileRestoreReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;

    .line 1649
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->currentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 1650
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 1652
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->cancelImport()V

    .line 1653
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->stopBackupOrRestore()V

    .line 1655
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$29;->$SwitchMap$com$sec$android$app$shealth$settings$samsungAccount$RestoreHelper2$Step:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->ERROR_CODE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1684
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string v1, "Default NETWORK_FAILURE popup showing because some other error faced"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1685
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mPopupHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$21;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$21;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1709
    :goto_0
    return-void

    .line 1657
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string v1, "device storage low popup showing"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1658
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mPopupHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$18;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$18;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 1666
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string v1, "SERVER_MAINTENANCE popup showing"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1667
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mPopupHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$19;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$19;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 1675
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string v1, "NETWORK_FAILURE popup showing"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1676
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mPopupHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$20;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$20;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 1655
    nop

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onExceptionReceived(JILcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 8
    .param p1, "requestId"    # J
    .param p3, "privateId"    # I
    .param p4, "ex"    # Lcom/sec/android/service/health/connectionmanager2/NetException;
    .param p5, "tag"    # Ljava/lang/Object;
    .param p6, "ReRequest"    # Ljava/lang/Object;

    .prologue
    const/4 v7, 0x0

    .line 1051
    sget-object v4, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "onExceptionReceived() code: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getCode()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1053
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->wl:Landroid/os/PowerManager$WakeLock;

    if-eqz v4, :cond_0

    .line 1055
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->wl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1056
    iput-object v7, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->wl:Landroid/os/PowerManager$WakeLock;

    .line 1059
    :cond_0
    const/4 v2, 0x0

    .line 1060
    .local v2, "exceptionObject":Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ExceptionObject;
    new-instance v3, Lcom/google/gson/Gson;

    invoke-direct {v3}, Lcom/google/gson/Gson;-><init>()V

    .line 1062
    .local v3, "gson":Lcom/google/gson/Gson;
    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getMessage()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 1066
    :try_start_0
    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getMessage()Ljava/lang/String;

    move-result-object v4

    const-class v5, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ExceptionObject;

    invoke-virtual {v3, v4, v5}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ExceptionObject;

    move-object v2, v0

    .line 1068
    sget-object v4, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "onExceptionReceived() privateId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " / ex_httpStatus: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ExceptionObject;->httpStatus:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " / ex_code"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ExceptionObject;->code:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1070
    iget v4, v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ExceptionObject;->httpStatus:I

    const/16 v5, 0x191

    if-ne v4, v5, :cond_3

    .line 1072
    iget-boolean v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mTryToRenewToken:Z

    if-eqz v4, :cond_2

    .line 1074
    sget-object v4, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string v5, "3.0 Restoration: It is failed to renew token"

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1075
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->onException(Ljava/lang/Exception;)V

    .line 1152
    :cond_1
    :goto_0
    return-void

    .line 1079
    :cond_2
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mTryToRenewToken:Z

    .line 1080
    invoke-static {}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->getInstance()Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$10;

    invoke-direct {v5, p0, p4}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$10;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/service/health/connectionmanager2/NetException;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->sendRequestForNewUserToken(Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1146
    :catch_0
    move-exception v1

    .line 1148
    .local v1, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    .line 1149
    invoke-virtual {p0, p4}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->onException(Ljava/lang/Exception;)V

    goto :goto_0

    .line 1108
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_3
    :try_start_1
    const-string v4, "PRO_5412"

    iget-object v5, v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ExceptionObject;->code:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "PRO_5411"

    iget-object v5, v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ExceptionObject;->code:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1110
    :cond_4
    sget-object v4, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string v5, "Restoration: No 3.0 data in server, start find 2.5 data"

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1111
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->noData1:Z

    .line 1113
    iget v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mMigrationState:I

    if-eqz v4, :cond_5

    .line 1115
    sget-object v4, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string v5, "it is not needed to import 2.x data, due to upgrade case"

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1116
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->noData2:Z

    .line 1119
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setRestoreOngoing(Landroid/content/Context;Z)V

    .line 1121
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setInitializationNeeded(Landroid/content/Context;Z)V

    .line 1123
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setRestoreAgainHomePopup(Landroid/content/Context;Z)V

    .line 1125
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->finish()V

    goto :goto_0

    .line 1129
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mPopupHandler:Landroid/os/Handler;

    new-instance v5, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$11;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$11;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 1142
    :cond_6
    sget-object v4, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string v5, "Exception occurred"

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1143
    invoke-virtual {p0, p4}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->onException(Ljava/lang/Exception;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public onNoData()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1733
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string v2, "Import onNoData"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1735
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1737
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 1738
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 1741
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->noData2:Z

    .line 1742
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setRestoreAgainHomePopup(Landroid/content/Context;Z)V

    .line 1744
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->noData1:Z

    if-eqz v1, :cond_1

    .line 1746
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->closeProgressDialog()V

    .line 1748
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 1750
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1751
    const v1, 0x7f0900e1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1752
    const v1, 0x7f090138

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1753
    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1754
    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$22;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$22;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1760
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mNoDataDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 1761
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mNoDataDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v3, "NO_DATA_TO_RESTORE"

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1764
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->NOTICE_FOR_NO_DATA_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 1769
    .end local v0    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    :goto_0
    return-void

    .line 1766
    :cond_1
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string v2, "3.0 restoration completed but there are no 2.5 data in server, SHealth will restart"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1767
    const/16 v1, 0x32

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->showDelayedProgressPopupAndRestart(I)V

    goto :goto_0
.end method

.method public onRequestCancelled(JILjava/lang/Object;Ljava/lang/Object;)V
    .locals 3
    .param p1, "requestId"    # J
    .param p3, "privateId"    # I
    .param p4, "tag"    # Ljava/lang/Object;
    .param p5, "ReRequest"    # Ljava/lang/Object;

    .prologue
    .line 1157
    const/16 v0, 0x65

    if-ne p3, v0, :cond_0

    .line 1159
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "InSide onRequestCancelled mIsCancelRequestHandled: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mIsCancelRequestHandled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1161
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mIsCancelRequestHandled:Z

    .line 1163
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->wl:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_1

    .line 1165
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->wl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1166
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->wl:Landroid/os/PowerManager$WakeLock;

    .line 1168
    :cond_1
    return-void
.end method

.method public onResponseReceived(JILjava/lang/Object;Ljava/lang/Object;)V
    .locals 3
    .param p1, "requestId"    # J
    .param p3, "privateId"    # I
    .param p4, "response"    # Ljava/lang/Object;
    .param p5, "tag"    # Ljava/lang/Object;

    .prologue
    .line 872
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "3.0 Restoration: 3.0 Data has been found from the server, response: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 874
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->wl:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 876
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->wl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 877
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->wl:Landroid/os/PowerManager$WakeLock;

    .line 880
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mPopupHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$7;

    invoke-direct {v1, p0, p3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$7;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 914
    return-void
.end method

.method public onRestoreProgress(Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;J)V
    .locals 22
    .param p1, "status"    # Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;
    .param p2, "size"    # J

    .prologue
    .line 1496
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->noData1:Z

    move/from16 v18, v0

    if-eqz v18, :cond_1

    .line 1497
    const/16 v17, 0x0

    .line 1498
    .local v17, "startValue":I
    const/4 v5, 0x2

    .line 1499
    .local v5, "denominatorValue":I
    const/16 v8, 0x32

    .line 1507
    .local v8, "endValue":I
    :goto_0
    sget-object v18, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;->STARTED:Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_2

    .line 1509
    sget-object v18, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string v19, "RestoreStatus.STARTED "

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1511
    const-string v18, "Restore Started"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, ""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-wide/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1512
    move-wide/from16 v0, p2

    long-to-double v0, v0

    move-wide/from16 v18, v0

    const-wide/high16 v20, 0x4090000000000000L    # 1024.0

    div-double v18, v18, v20

    const-wide/high16 v20, 0x4090000000000000L    # 1024.0

    div-double v18, v18, v20

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mSetMaxRestoreSize:D

    .line 1602
    :cond_0
    :goto_1
    return-void

    .line 1501
    .end local v5    # "denominatorValue":I
    .end local v8    # "endValue":I
    .end local v17    # "startValue":I
    :cond_1
    const/16 v17, 0x32

    .line 1502
    .restart local v17    # "startValue":I
    const/4 v5, 0x4

    .line 1503
    .restart local v5    # "denominatorValue":I
    const/16 v8, 0x4b

    .restart local v8    # "endValue":I
    goto :goto_0

    .line 1514
    :cond_2
    sget-object v18, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;->ON_WIFI_TO_CONTINUE:Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_4

    .line 1516
    const-string v18, "Backup Started"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Restore size is too big to do on Mobile network recommend turn on wifi, Size of download is: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-wide/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1518
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-object/from16 v18, v0

    if-eqz v18, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v18

    if-eqz v18, :cond_3

    .line 1520
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 1521
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 1524
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->closeProgressDialog()V

    .line 1526
    const-string v18, "WIFI_NOTIFICATION"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->showWiFiNotificationPopup(Ljava/lang/String;)V

    goto :goto_1

    .line 1528
    :cond_4
    sget-object v18, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;->ONGOING:Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_6

    .line 1530
    move-wide/from16 v0, p2

    long-to-double v0, v0

    move-wide/from16 v18, v0

    const-wide/high16 v20, 0x4090000000000000L    # 1024.0

    div-double v18, v18, v20

    const-wide/high16 v20, 0x4090000000000000L    # 1024.0

    div-double v3, v18, v20

    .line 1532
    .local v3, "curSize":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mSetMaxRestoreSize:D

    move-wide/from16 v18, v0

    div-double v18, v3, v18

    const-wide/high16 v20, 0x4059000000000000L    # 100.0

    mul-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-int v15, v0

    .line 1534
    .local v15, "percentage":I
    const/16 v18, 0x64

    move/from16 v0, v18

    if-lt v15, v0, :cond_5

    .line 1535
    const/16 v15, 0x64

    .line 1538
    :cond_5
    div-int v18, v15, v5

    add-int v18, v18, v17

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->updateProgressPopupValue(I)V

    goto/16 :goto_1

    .line 1540
    .end local v3    # "curSize":D
    .end local v15    # "percentage":I
    :cond_6
    sget-object v18, Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;->DONE:Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_b

    .line 1542
    sget-object v18, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "RestoreStatus.DONE "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-wide/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1544
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->updateProgressPopupValue(I)V

    .line 1547
    new-instance v10, Ljava/io/File;

    invoke-static {}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->getDatabasePath()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v10, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1548
    .local v10, "h_dbFile":Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_7

    .line 1550
    sget-object v18, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string v19, "H db file exists renaming it to old"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1551
    new-instance v14, Ljava/io/File;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->getDatabasePath()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "_old"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v14, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1552
    .local v14, "old_h_dbFile":Ljava/io/File;
    invoke-virtual {v10, v14}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 1555
    .end local v14    # "old_h_dbFile":Ljava/io/File;
    :cond_7
    sget-object v18, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string v19, "Renaming cache file to h db file"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1556
    new-instance v6, Ljava/io/File;

    const-string v18, "/data/data/com.sec.android.app.shealth/databases/cache_shealth2.db"

    move-object/from16 v0, v18

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1557
    .local v6, "downloadedDB":Ljava/io/File;
    invoke-virtual {v6, v10}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 1559
    new-instance v11, Ljava/io/File;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v19, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->SHAREDPREF_FOLDER_PATH:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, "restore_"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "com.sec.android.app.shealth_preferences.xml"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v11, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1560
    .local v11, "h_shared_PrefFile":Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_8

    .line 1562
    sget-object v18, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string v19, "H shared Pref deleting old restore file"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1563
    invoke-virtual {v11}, Ljava/io/File;->delete()Z

    .line 1566
    :cond_8
    sget-object v18, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string v19, "Renaming cache file to h shared_pref file"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1567
    new-instance v7, Ljava/io/File;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v19, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->SHAREDPREF_FOLDER_PATH:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "cache_shared_prefs.xml"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1568
    .local v7, "downloadedsharePref":Ljava/io/File;
    invoke-virtual {v7, v11}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 1570
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreManager:Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->copyRestoredContents()V

    .line 1573
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->profileRestoreReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;

    move-object/from16 v18, v0

    if-nez v18, :cond_9

    .line 1574
    new-instance v18, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$1;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->profileRestoreReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;

    .line 1575
    new-instance v9, Landroid/content/IntentFilter;

    invoke-direct {v9}, Landroid/content/IntentFilter;-><init>()V

    .line 1576
    .local v9, "filter":Landroid/content/IntentFilter;
    const-string v18, "com.sec.android.app.shealth.RESTORE_PROGRESS"

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1577
    const-string v18, "com.sec.android.app.shealth.RESTORE_PROGRESS_PERCENT"

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1578
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->profileRestoreReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$ProfileRestoreReceiver;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1582
    .end local v9    # "filter":Landroid/content/IntentFilter;
    :cond_9
    new-instance v12, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    const-class v19, Lcom/sec/android/app/shealth/framework/repository/MigrationService;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v12, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1583
    .local v12, "intent":Landroid/content/Intent;
    const-string v18, "isSecure"

    const/16 v19, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1584
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1586
    new-instance v16, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 1587
    .local v16, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    if-eqz v16, :cond_0

    .line 1589
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getActivityType()I

    move-result v18

    const/16 v19, -0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_a

    const/4 v13, 0x1

    .line 1590
    .local v13, "isProfileThere":Z
    :goto_2
    if-nez v13, :cond_0

    goto/16 :goto_1

    .line 1589
    .end local v13    # "isProfileThere":Z
    :cond_a
    const/4 v13, 0x0

    goto :goto_2

    .line 1598
    .end local v6    # "downloadedDB":Ljava/io/File;
    .end local v7    # "downloadedsharePref":Ljava/io/File;
    .end local v10    # "h_dbFile":Ljava/io/File;
    .end local v11    # "h_shared_PrefFile":Ljava/io/File;
    .end local v12    # "intent":Landroid/content/Intent;
    .end local v16    # "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    :cond_b
    sget-object v18, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Exception occured status: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public restoreState()V
    .locals 3

    .prologue
    .line 204
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$29;->$SwitchMap$com$sec$android$app$shealth$settings$samsungAccount$RestoreHelper2$Step:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 272
    :cond_0
    :goto_0
    return-void

    .line 206
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 207
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Show again 1:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v2, "CONFIRM_RESTORE"

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 213
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 214
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Show again 2:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v2, "CONFIRM_RESTORE_WITHOUT_CHECKING_SERVER"

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 220
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 222
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Show again "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v2, "RESTORE_PROGRESS_POPUP"

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 228
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mNoDataDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mNoDataDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 229
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Show again "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mNoDataDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v2, "NO_DATA_TO_RESTORE"

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 235
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mWifiNotiDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mWifiNotiDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 236
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Show again "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mWifiNotiDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v2, "WIFI_NOTIFICATION"

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 242
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 243
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Show again "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v2, "NETWORK_FAILED_POPUP"

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 249
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 250
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Show again "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v2, "CHECKING_SERVER_POPUP"

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 256
    :pswitch_7
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 257
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Show again "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v2, "LOW_STORAGE_FAILED_POPUP"

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 263
    :pswitch_8
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 264
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Show again "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v2, "SERVER_MAINTENANCE_FAILED_POPUP"

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 204
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public showDelayedProgressPopupAndRestart(I)V
    .locals 3
    .param p1, "startValue"    # I

    .prologue
    .line 729
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "showDelayedProgressPopupAndRestart startValue"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 730
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$5;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$5;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;I)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$5;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 767
    return-void
.end method

.method public showPopupWithProgressBar()V
    .locals 1

    .prologue
    .line 1824
    const v0, 0x7f090d3f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->showPopupWithProgressBar(I)V

    .line 1825
    return-void
.end method

.method public showPopupWithProgressBar(I)V
    .locals 4
    .param p1, "popup_msg"    # I

    .prologue
    .line 1830
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "progress popup showing"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1832
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->closeProgressDialog()V

    .line 1834
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;I)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1895
    return-void
.end method

.method public showServerCheckingPopup()V
    .locals 3

    .prologue
    .line 1940
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "server checking popup showing"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1942
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialogBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1943
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialogBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const v1, 0x7f090806

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1944
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialogBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const v1, 0x7f090048

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1945
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialogBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const v1, 0x7f030189

    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$27;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$27;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1965
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialogBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 1966
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$28;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$28;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)V

    .line 1997
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->SERVER_CHECKING_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 1998
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v2, "CHECKING_SERVER_POPUP"

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1999
    return-void
.end method

.method public startRestore()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 558
    sget-object v3, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "startRestore()"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 559
    sget-object v3, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->START_RESTORE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->currentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 560
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->isBackup:Z

    if-nez v3, :cond_0

    iget v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mMigrationState:I

    if-eqz v3, :cond_3

    :cond_0
    const/4 v1, 0x1

    .line 561
    .local v1, "restoreAfterBackup":Z
    :goto_0
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->isBackup:Z

    .line 563
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 565
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 566
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 569
    :cond_1
    if-nez v1, :cond_2

    .line 571
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->showPopupWithProgressBar()V

    .line 576
    :cond_2
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->shcm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    const/16 v3, 0xb

    new-instance v4, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;

    invoke-direct {v4, p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Z)V

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->startRestore(ILcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 702
    :goto_1
    return-void

    .end local v1    # "restoreAfterBackup":Z
    :cond_3
    move v1, v2

    .line 560
    goto :goto_0

    .line 696
    .restart local v1    # "restoreAfterBackup":Z
    :catch_0
    move-exception v0

    .line 698
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 699
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->stopBackupOrRestore()V

    .line 700
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->onException(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public startRestoreProcess()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2091
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startRestoreProcess"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 2092
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mTryToRenewToken:Z

    .line 2093
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->isConfirmed:Z

    .line 2095
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2096
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->checkRestoreData()V

    .line 2100
    :goto_0
    return-void

    .line 2098
    :cond_0
    const-string v0, "CONFIRM_RESTORE_WITHOUT_CHECKING_SERVER"

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->showRestoreConfirmation(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateProgressPopupText(I)V
    .locals 2
    .param p1, "textID"    # I

    .prologue
    .line 714
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressMsg:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 715
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressMsg:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 717
    :cond_0
    return-void
.end method

.method public updateProgressPopupValue(I)V
    .locals 4
    .param p1, "percentage"    # I

    .prologue
    .line 705
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 706
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 708
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mTextPercentage:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 709
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mTextPercentage:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;

    const v3, 0x7f090d15

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 711
    :cond_1
    return-void
.end method
