.class public Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
.super Ljava/lang/Object;
.source "MapHRMData.java"


# instance fields
.field heartRateVal:F

.field latitude:F

.field longitude:F

.field sampleTime:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    return-void
.end method

.method public constructor <init>(JFFJ)V
    .locals 0
    .param p1, "exerciseId"    # J
    .param p3, "latitude"    # F
    .param p4, "longitude"    # F
    .param p5, "sampleTime"    # J

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput p3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->latitude:F

    .line 18
    iput p4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->longitude:F

    .line 19
    iput-wide p5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->sampleTime:J

    .line 20
    return-void
.end method


# virtual methods
.method public getHeartRateVal()F
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->heartRateVal:F

    return v0
.end method

.method public getLatitude()F
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->latitude:F

    return v0
.end method

.method public getLongitude()F
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->longitude:F

    return v0
.end method

.method public getSampleTime()J
    .locals 2

    .prologue
    .line 40
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->sampleTime:J

    return-wide v0
.end method

.method public setHeartRateVal(F)V
    .locals 0
    .param p1, "heartRateVal"    # F

    .prologue
    .line 52
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->heartRateVal:F

    .line 53
    return-void
.end method

.method public setLatitude(F)V
    .locals 0
    .param p1, "latitude"    # F

    .prologue
    .line 28
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->latitude:F

    .line 29
    return-void
.end method

.method public setLongitude(F)V
    .locals 0
    .param p1, "longitude"    # F

    .prologue
    .line 36
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->longitude:F

    .line 37
    return-void
.end method

.method public setSampleTime(J)V
    .locals 0
    .param p1, "sampleTime"    # J

    .prologue
    .line 44
    iput-wide p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->sampleTime:J

    .line 45
    return-void
.end method
