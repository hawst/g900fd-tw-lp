.class Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogFilenameFilter;
.super Ljava/lang/Object;
.source "LogManager.java"

# interfaces
.implements Ljava/io/FilenameFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "LogFilenameFilter"
.end annotation


# instance fields
.field mPrefix:[Ljava/lang/String;


# direct methods
.method constructor <init>([Ljava/lang/String;)V
    .locals 0
    .param p1, "prefix"    # [Ljava/lang/String;

    .prologue
    .line 817
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 818
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogFilenameFilter;->mPrefix:[Ljava/lang/String;

    .line 819
    return-void
.end method


# virtual methods
.method public accept(Ljava/io/File;Ljava/lang/String;)Z
    .locals 5
    .param p1, "dir"    # Ljava/io/File;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 822
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogFilenameFilter;->mPrefix:[Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 823
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogFilenameFilter;->mPrefix:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 825
    .local v3, "pref":Ljava/lang/String;
    invoke-virtual {p2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 826
    const/4 v4, 0x1

    .line 830
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "pref":Ljava/lang/String;
    :goto_1
    return v4

    .line 823
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v1    # "i$":I
    .restart local v2    # "len$":I
    .restart local v3    # "pref":Ljava/lang/String;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 830
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "pref":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method
