.class public Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncDataListener;,
        Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncEventListener;,
        Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperEventListener;,
        Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperScanListener;,
        Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$SyncEventListener;,
        Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentEventListener;,
        Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentScanListener;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;


# direct methods
.method static constructor <clinit>()V
    .locals 29

    const/16 v0, 0x1a

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, 0x17

    const/16 v25, 0x18

    const/16 v26, 0x19

    const/16 v27, -0x6dd6

    aput v27, v1, v26

    const/16 v26, -0x9

    aput v26, v1, v25

    const/16 v25, 0x3860

    aput v25, v1, v24

    const/16 v24, -0x51ac

    aput v24, v1, v23

    const/16 v23, -0x3f

    aput v23, v1, v22

    const/16 v22, -0x378e

    aput v22, v1, v21

    const/16 v21, -0x44

    aput v21, v1, v20

    const/16 v20, -0x17

    aput v20, v1, v19

    const/16 v19, -0x26

    aput v19, v1, v18

    const/16 v18, -0x5a

    aput v18, v1, v17

    const/16 v17, 0x6741

    aput v17, v1, v16

    const/16 v16, -0x70f7

    aput v16, v1, v15

    const/16 v15, -0x16

    aput v15, v1, v14

    const/16 v14, -0x6e

    aput v14, v1, v13

    const/16 v13, -0x67

    aput v13, v1, v12

    const/16 v12, -0xadb

    aput v12, v1, v11

    const/16 v11, -0x79

    aput v11, v1, v10

    const/16 v10, -0x48

    aput v10, v1, v9

    const/16 v9, -0x39

    aput v9, v1, v8

    const/16 v8, -0x53

    aput v8, v1, v7

    const/16 v7, -0x30

    aput v7, v1, v6

    const/16 v6, 0x395f

    aput v6, v1, v5

    const/16 v5, -0x44a9

    aput v5, v1, v4

    const/16 v4, -0x26

    aput v4, v1, v3

    const/16 v3, 0x6041

    aput v3, v1, v2

    const/16 v2, -0x6acd

    aput v2, v1, v0

    const/16 v0, 0x1a

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, -0x6da8

    aput v28, v0, v27

    const/16 v27, -0x6e

    aput v27, v0, v26

    const/16 v26, 0x380c

    aput v26, v0, v25

    const/16 v25, -0x51c8

    aput v25, v0, v24

    const/16 v24, -0x52

    aput v24, v0, v23

    const/16 v23, -0x3800

    aput v23, v0, v22

    const/16 v22, -0x38

    aput v22, v0, v21

    const/16 v21, -0x79

    aput v21, v0, v20

    const/16 v20, -0x4b

    aput v20, v0, v19

    const/16 v19, -0x1b

    aput v19, v0, v18

    const/16 v18, 0x6735

    aput v18, v0, v17

    const/16 v17, -0x7099

    aput v17, v0, v16

    const/16 v16, -0x71

    aput v16, v0, v15

    const/4 v15, -0x1

    aput v15, v0, v14

    const/4 v14, -0x2

    aput v14, v0, v13

    const/16 v13, -0xabc

    aput v13, v0, v12

    const/16 v12, -0xb

    aput v12, v0, v11

    const/4 v11, -0x2

    aput v11, v0, v10

    const/16 v10, -0x60

    aput v10, v0, v9

    const/16 v9, -0x3d

    aput v9, v0, v8

    const/16 v8, -0x47

    aput v8, v0, v7

    const/16 v7, 0x3931

    aput v7, v0, v6

    const/16 v6, -0x44c7

    aput v6, v0, v5

    const/16 v5, -0x45

    aput v5, v0, v4

    const/16 v4, 0x6022

    aput v4, v0, v3

    const/16 v3, -0x6aa0

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->TAG:Ljava/lang/String;

    return-void

    :cond_0
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V

    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static cauL()Ljava/lang/String;
    .locals 67

    const/16 v0, 0x40

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, 0x17

    const/16 v25, 0x18

    const/16 v26, 0x19

    const/16 v27, 0x1a

    const/16 v28, 0x1b

    const/16 v29, 0x1c

    const/16 v30, 0x1d

    const/16 v31, 0x1e

    const/16 v32, 0x1f

    const/16 v33, 0x20

    const/16 v34, 0x21

    const/16 v35, 0x22

    const/16 v36, 0x23

    const/16 v37, 0x24

    const/16 v38, 0x25

    const/16 v39, 0x26

    const/16 v40, 0x27

    const/16 v41, 0x28

    const/16 v42, 0x29

    const/16 v43, 0x2a

    const/16 v44, 0x2b

    const/16 v45, 0x2c

    const/16 v46, 0x2d

    const/16 v47, 0x2e

    const/16 v48, 0x2f

    const/16 v49, 0x30

    const/16 v50, 0x31

    const/16 v51, 0x32

    const/16 v52, 0x33

    const/16 v53, 0x34

    const/16 v54, 0x35

    const/16 v55, 0x36

    const/16 v56, 0x37

    const/16 v57, 0x38

    const/16 v58, 0x39

    const/16 v59, 0x3a

    const/16 v60, 0x3b

    const/16 v61, 0x3c

    const/16 v62, 0x3d

    const/16 v63, 0x3e

    const/16 v64, 0x3f

    const/16 v65, 0x316c

    aput v65, v1, v64

    const/16 v64, -0x399b

    aput v64, v1, v63

    const/16 v63, -0x71

    aput v63, v1, v62

    const/16 v62, -0x4c

    aput v62, v1, v61

    const/16 v61, -0x62

    aput v61, v1, v60

    const/16 v60, 0x4049

    aput v60, v1, v59

    const/16 v59, 0xb03

    aput v59, v1, v58

    const/16 v58, 0x784a

    aput v58, v1, v57

    const/16 v57, 0x7027

    aput v57, v1, v56

    const/16 v56, -0x66c2

    aput v56, v1, v55

    const/16 v55, -0x30

    aput v55, v1, v54

    const/16 v54, -0x41b6

    aput v54, v1, v53

    const/16 v53, -0xd

    aput v53, v1, v52

    const/16 v52, 0x6c7a

    aput v52, v1, v51

    const/16 v51, -0x1cd7

    aput v51, v1, v50

    const/16 v50, -0x4f

    aput v50, v1, v49

    const/16 v49, -0x53c0

    aput v49, v1, v48

    const/16 v48, -0x1

    aput v48, v1, v47

    const/16 v47, -0x42

    aput v47, v1, v46

    const/16 v46, -0x69

    aput v46, v1, v45

    const/16 v45, -0x3e

    aput v45, v1, v44

    const/16 v44, -0x68

    aput v44, v1, v43

    const/16 v43, -0x78

    aput v43, v1, v42

    const/16 v42, -0x70

    aput v42, v1, v41

    const/16 v41, -0x53

    aput v41, v1, v40

    const/16 v40, -0x43

    aput v40, v1, v39

    const/16 v39, -0x4d

    aput v39, v1, v38

    const/16 v38, 0x2a32

    aput v38, v1, v37

    const/16 v37, 0x44f

    aput v37, v1, v36

    const/16 v36, -0x148a

    aput v36, v1, v35

    const/16 v35, -0x62

    aput v35, v1, v34

    const/16 v34, -0x71c4

    aput v34, v1, v33

    const/16 v33, -0x3

    aput v33, v1, v32

    const/16 v32, -0x4df0

    aput v32, v1, v31

    const/16 v31, -0x40

    aput v31, v1, v30

    const/16 v30, -0x35

    aput v30, v1, v29

    const/16 v29, -0x5

    aput v29, v1, v28

    const/16 v28, -0x57

    aput v28, v1, v27

    const/16 v27, 0x6e4f

    aput v27, v1, v26

    const/16 v26, 0x7902

    aput v26, v1, v25

    const/16 v25, -0x8e5

    aput v25, v1, v24

    const/16 v24, -0x27

    aput v24, v1, v23

    const/16 v23, -0x7bfa

    aput v23, v1, v22

    const/16 v22, -0x15

    aput v22, v1, v21

    const/16 v21, 0x4209

    aput v21, v1, v20

    const/16 v20, 0x3236

    aput v20, v1, v19

    const/16 v19, -0x58af

    aput v19, v1, v18

    const/16 v18, -0x3a

    aput v18, v1, v17

    const/16 v17, -0x26

    aput v17, v1, v16

    const/16 v16, -0x5f

    aput v16, v1, v15

    const/16 v15, -0x3c90

    aput v15, v1, v14

    const/16 v14, -0x51

    aput v14, v1, v13

    const/16 v13, -0xa

    aput v13, v1, v12

    const/16 v12, -0x44

    aput v12, v1, v11

    const/4 v11, -0x3

    aput v11, v1, v10

    const/16 v10, 0x2e58

    aput v10, v1, v9

    const/16 v9, -0x5400

    aput v9, v1, v8

    const/16 v8, -0x31

    aput v8, v1, v7

    const/16 v7, 0x2b7c

    aput v7, v1, v6

    const/16 v6, 0xb58

    aput v6, v1, v5

    const/16 v5, -0x4cdb

    aput v5, v1, v4

    const/16 v4, -0x22

    aput v4, v1, v3

    const/16 v3, -0x32

    aput v3, v1, v2

    const/16 v2, 0x1a55

    aput v2, v1, v0

    const/16 v0, 0x40

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, 0x1e

    const/16 v33, 0x1f

    const/16 v34, 0x20

    const/16 v35, 0x21

    const/16 v36, 0x22

    const/16 v37, 0x23

    const/16 v38, 0x24

    const/16 v39, 0x25

    const/16 v40, 0x26

    const/16 v41, 0x27

    const/16 v42, 0x28

    const/16 v43, 0x29

    const/16 v44, 0x2a

    const/16 v45, 0x2b

    const/16 v46, 0x2c

    const/16 v47, 0x2d

    const/16 v48, 0x2e

    const/16 v49, 0x2f

    const/16 v50, 0x30

    const/16 v51, 0x31

    const/16 v52, 0x32

    const/16 v53, 0x33

    const/16 v54, 0x34

    const/16 v55, 0x35

    const/16 v56, 0x36

    const/16 v57, 0x37

    const/16 v58, 0x38

    const/16 v59, 0x39

    const/16 v60, 0x3a

    const/16 v61, 0x3b

    const/16 v62, 0x3c

    const/16 v63, 0x3d

    const/16 v64, 0x3e

    const/16 v65, 0x3f

    const/16 v66, 0x3135

    aput v66, v0, v65

    const/16 v65, -0x39cf

    aput v65, v0, v64

    const/16 v64, -0x3a

    aput v64, v0, v63

    const/16 v63, -0x1e

    aput v63, v0, v62

    const/16 v62, -0x29

    aput v62, v0, v61

    const/16 v61, 0x401d    # 2.3E-41f

    aput v61, v0, v60

    const/16 v60, 0xb40

    aput v60, v0, v59

    const/16 v59, 0x780b

    aput v59, v0, v58

    const/16 v58, 0x7078

    aput v58, v0, v57

    const/16 v57, -0x6690

    aput v57, v0, v56

    const/16 v56, -0x67

    aput v56, v0, v55

    const/16 v55, -0x41f5

    aput v55, v0, v54

    const/16 v54, -0x42

    aput v54, v0, v53

    const/16 v53, 0x6c25

    aput v53, v0, v52

    const/16 v52, -0x1c94

    aput v52, v0, v51

    const/16 v51, -0x1d

    aput v51, v0, v50

    const/16 v50, -0x53eb

    aput v50, v0, v49

    const/16 v49, -0x54

    aput v49, v0, v48

    const/16 v48, -0x13

    aput v48, v0, v47

    const/16 v47, -0x2e

    aput v47, v0, v46

    const/16 v46, -0x70

    aput v46, v0, v45

    const/16 v45, -0x38

    aput v45, v0, v44

    const/16 v44, -0x34

    aput v44, v0, v43

    const/16 v43, -0x21

    aput v43, v0, v42

    const/16 v42, -0x1e

    aput v42, v0, v41

    const/16 v41, -0xf

    aput v41, v0, v40

    const/16 v40, -0xf

    aput v40, v0, v39

    const/16 v39, 0x2a1c

    aput v39, v0, v38

    const/16 v38, 0x42a

    aput v38, v0, v37

    const/16 v37, -0x14fc

    aput v37, v0, v36

    const/16 v36, -0x15

    aput v36, v0, v35

    const/16 v35, -0x71b1

    aput v35, v0, v34

    const/16 v34, -0x72

    aput v34, v0, v33

    const/16 v33, -0x4d8b

    aput v33, v0, v32

    const/16 v32, -0x4e

    aput v32, v0, v31

    const/16 v31, -0x45

    aput v31, v0, v30

    const/16 v30, -0x61

    aput v30, v0, v29

    const/16 v29, -0x3a

    aput v29, v0, v28

    const/16 v28, 0x6e20

    aput v28, v0, v27

    const/16 v27, 0x796e

    aput v27, v0, v26

    const/16 v26, -0x887

    aput v26, v0, v25

    const/16 v25, -0x9

    aput v25, v0, v24

    const/16 v24, -0x7b98

    aput v24, v0, v23

    const/16 v23, -0x7c

    aput v23, v0, v22

    const/16 v22, 0x4260

    aput v22, v0, v21

    const/16 v21, 0x3242

    aput v21, v0, v20

    const/16 v20, -0x58ce

    aput v20, v0, v19

    const/16 v19, -0x59

    aput v19, v0, v18

    const/16 v18, -0xc

    aput v18, v0, v17

    const/16 v17, -0x37

    aput v17, v0, v16

    const/16 v16, -0x3cfc

    aput v16, v0, v15

    const/16 v15, -0x3d

    aput v15, v0, v14

    const/16 v14, -0x69

    aput v14, v0, v13

    const/16 v13, -0x27

    aput v13, v0, v12

    const/16 v12, -0x6b

    aput v12, v0, v11

    const/16 v11, 0x2e2b

    aput v11, v0, v10

    const/16 v10, -0x53d2

    aput v10, v0, v9

    const/16 v9, -0x54

    aput v9, v0, v8

    const/16 v8, 0x2b19

    aput v8, v0, v7

    const/16 v7, 0xb2b

    aput v7, v0, v6

    const/16 v6, -0x4cf5

    aput v6, v0, v5

    const/16 v5, -0x4d

    aput v5, v0, v4

    const/16 v4, -0x5f

    aput v4, v0, v3

    const/16 v3, 0x1a36

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private createListwithConnectionStatus(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;",
            ">;"
        }
    .end annotation

    const-wide/16 v12, 0x0

    const/4 v11, 0x1

    const-wide v9, -0x54a8bd53a7531a30L    # -6.646598065332378E-100

    const/16 v8, 0x20

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v4, v0, [J

    const-wide/16 v0, 0x1

    aput-wide v0, v4, v11

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v4, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, v3

    shl-long/2addr v0, v8

    ushr-long v5, v0, v8

    aget-wide v0, v4, v3

    cmp-long v7, v0, v12

    if-eqz v7, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v8

    shl-long/2addr v0, v8

    xor-long/2addr v0, v5

    xor-long/2addr v0, v9

    aput-wide v0, v4, v3

    :goto_0
    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v4, v0

    long-to-int v0, v0

    if-gtz v0, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v11, [I

    const/16 v0, 0x1559

    aput v0, v1, v3

    new-array v0, v11, [I

    const/16 v2, 0x1569

    aput v2, v0, v3

    move v2, v3

    :goto_1
    array-length v5, v0

    if-lt v2, v5, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    :goto_2
    array-length v2, v0

    if-lt v3, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    aget-wide v0, v4, v3

    cmp-long v5, v0, v12

    if-eqz v5, :cond_5

    xor-long/2addr v0, v9

    :cond_5
    shl-long/2addr v0, v8

    shr-long/2addr v0, v8

    long-to-int v0, v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_e

    new-instance v5, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v4, v0

    long-to-int v0, v0

    if-gtz v0, :cond_8

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v11, [I

    const/16 v0, -0x2e

    aput v0, v1, v3

    new-array v0, v11, [I

    const/16 v2, -0x1e

    aput v2, v0, v3

    move v2, v3

    :goto_3
    array-length v5, v0

    if-lt v2, v5, :cond_6

    array-length v0, v1

    new-array v0, v0, [C

    :goto_4
    array-length v2, v0

    if-lt v3, v2, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_6
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_7
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_8
    aget-wide v0, v4, v3

    cmp-long v6, v0, v12

    if-eqz v6, :cond_9

    xor-long/2addr v0, v9

    :cond_9
    shl-long/2addr v0, v8

    shr-long/2addr v0, v8

    long-to-int v0, v0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-direct {v5, v0, v3}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Z)V

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v4, v0

    long-to-int v0, v0

    if-gtz v0, :cond_a

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    aget-wide v0, v4, v3

    cmp-long v5, v0, v12

    if-eqz v5, :cond_b

    xor-long/2addr v0, v9

    :cond_b
    shl-long/2addr v0, v8

    shr-long/2addr v0, v8

    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v5, v4, v1

    long-to-int v1, v5

    if-gtz v1, :cond_c

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    int-to-long v0, v0

    shl-long/2addr v0, v8

    ushr-long v5, v0, v8

    aget-wide v0, v4, v3

    cmp-long v7, v0, v12

    if-eqz v7, :cond_d

    xor-long/2addr v0, v9

    :cond_d
    ushr-long/2addr v0, v8

    shl-long/2addr v0, v8

    xor-long/2addr v0, v5

    xor-long/2addr v0, v9

    aput-wide v0, v4, v3

    goto/16 :goto_0

    :cond_e
    move-object v0, v2

    :goto_5
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_5
.end method

.method private getConnectionStatus(Ljava/lang/String;Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;",
            ">;)Z"
        }
    .end annotation

    const/4 v1, 0x1

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v0, v1

    :goto_1
    return v0

    :catch_0
    move-exception v0

    :goto_2
    move v0, v2

    goto :goto_1

    :catch_1
    move-exception v4

    :cond_1
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v4

    const/16 v5, 0x2718

    if-ne v4, v5, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getSerialNumber()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v0

    goto :goto_0
.end method

.method private getFilteredPairedDevicesFromSensorService(IZLjava/util/ArrayList;Ljava/util/List;)Ljava/util/ArrayList;
    .locals 51
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
        }
    .end annotation

    const/4 v1, 0x3

    new-array v4, v1, [J

    const/4 v1, 0x2

    const-wide/16 v2, 0x3

    aput-wide v2, v4, v1

    const/4 v1, 0x0

    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v4, v2

    long-to-int v2, v2

    if-gtz v2, :cond_0

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v3, 0x0

    move/from16 v0, p1

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    ushr-long v5, v1, v5

    aget-wide v1, v4, v3

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_1

    const-wide v7, 0x3a6b124487cb929eL    # 2.7335086244674476E-27

    xor-long/2addr v1, v7

    :cond_1
    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    const/16 v7, 0x20

    shl-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, 0x3a6b124487cb929eL    # 2.7335086244674476E-27

    xor-long/2addr v1, v5

    aput-wide v1, v4, v3

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    const/16 v1, 0x1a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, -0x5b

    aput v30, v2, v29

    const/16 v29, -0x69

    aput v29, v2, v28

    const/16 v28, -0x309d

    aput v28, v2, v27

    const/16 v27, -0x5d

    aput v27, v2, v26

    const/16 v26, -0x8

    aput v26, v2, v25

    const/16 v25, -0x50

    aput v25, v2, v24

    const/16 v24, 0x254d

    aput v24, v2, v23

    const/16 v23, 0x634b

    aput v23, v2, v22

    const/16 v22, 0x2b0c

    aput v22, v2, v21

    const/16 v21, 0x7768

    aput v21, v2, v20

    const/16 v20, 0x5b03

    aput v20, v2, v19

    const/16 v19, -0x4ecb

    aput v19, v2, v18

    const/16 v18, -0x2c

    aput v18, v2, v17

    const/16 v17, -0x7bfd

    aput v17, v2, v16

    const/16 v16, -0x1d

    aput v16, v2, v15

    const/16 v15, -0x2f

    aput v15, v2, v14

    const/16 v14, 0x724c

    aput v14, v2, v13

    const/16 v13, -0x2fcc

    aput v13, v2, v12

    const/16 v12, -0x49

    aput v12, v2, v11

    const/16 v11, -0x67

    aput v11, v2, v10

    const/16 v10, -0x70dd

    aput v10, v2, v9

    const/16 v9, -0x1f

    aput v9, v2, v8

    const/16 v8, -0x38

    aput v8, v2, v7

    const/16 v7, 0x5e49

    aput v7, v2, v6

    const/16 v6, 0x6e3d

    aput v6, v2, v3

    const/16 v3, -0x75c3

    aput v3, v2, v1

    const/16 v1, 0x1a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, -0x29

    aput v31, v1, v30

    const/16 v30, -0xe

    aput v30, v1, v29

    const/16 v29, -0x30f1

    aput v29, v1, v28

    const/16 v28, -0x31

    aput v28, v1, v27

    const/16 v27, -0x69

    aput v27, v1, v26

    const/16 v26, -0x3e

    aput v26, v1, v25

    const/16 v25, 0x2539

    aput v25, v1, v24

    const/16 v24, 0x6325

    aput v24, v1, v23

    const/16 v23, 0x2b63

    aput v23, v1, v22

    const/16 v22, 0x772b

    aput v22, v1, v21

    const/16 v21, 0x5b77

    aput v21, v1, v20

    const/16 v20, -0x4ea5

    aput v20, v1, v19

    const/16 v19, -0x4f

    aput v19, v1, v18

    const/16 v18, -0x7b92

    aput v18, v1, v17

    const/16 v17, -0x7c

    aput v17, v1, v16

    const/16 v16, -0x50

    aput v16, v1, v15

    const/16 v15, 0x723e

    aput v15, v1, v14

    const/16 v14, -0x2f8e

    aput v14, v1, v13

    const/16 v13, -0x30

    aput v13, v1, v12

    const/16 v12, -0x9

    aput v12, v1, v11

    const/16 v11, -0x70b6

    aput v11, v1, v10

    const/16 v10, -0x71

    aput v10, v1, v9

    const/16 v9, -0x5a

    aput v9, v1, v8

    const/16 v8, 0x5e28

    aput v8, v1, v7

    const/16 v7, 0x6e5e

    aput v7, v1, v6

    const/16 v6, -0x7592

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v6, v1

    if-lt v3, v6, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v6, v1

    if-lt v3, v6, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v6

    const/16 v1, 0x2c

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, 0x1a

    const/16 v32, 0x1b

    const/16 v33, 0x1c

    const/16 v34, 0x1d

    const/16 v35, 0x1e

    const/16 v36, 0x1f

    const/16 v37, 0x20

    const/16 v38, 0x21

    const/16 v39, 0x22

    const/16 v40, 0x23

    const/16 v41, 0x24

    const/16 v42, 0x25

    const/16 v43, 0x26

    const/16 v44, 0x27

    const/16 v45, 0x28

    const/16 v46, 0x29

    const/16 v47, 0x2a

    const/16 v48, 0x2b

    const/16 v49, 0x4a47

    aput v49, v2, v48

    const/16 v48, 0x439

    aput v48, v2, v47

    const/16 v47, -0x2f93

    aput v47, v2, v46

    const/16 v46, -0x64

    aput v46, v2, v45

    const/16 v45, -0xd

    aput v45, v2, v44

    const/16 v44, -0x7cea

    aput v44, v2, v43

    const/16 v43, -0x16

    aput v43, v2, v42

    const/16 v42, -0x46

    aput v42, v2, v41

    const/16 v41, -0x17

    aput v41, v2, v40

    const/16 v40, -0xe

    aput v40, v2, v39

    const/16 v39, -0x56

    aput v39, v2, v38

    const/16 v38, -0x1d

    aput v38, v2, v37

    const/16 v37, -0x4a

    aput v37, v2, v36

    const/16 v36, -0xf

    aput v36, v2, v35

    const/16 v35, -0x69

    aput v35, v2, v34

    const/16 v34, -0x3594

    aput v34, v2, v33

    const/16 v33, -0x5c

    aput v33, v2, v32

    const/16 v32, -0x7696

    aput v32, v2, v31

    const/16 v31, -0x36

    aput v31, v2, v30

    const/16 v30, -0x2a5

    aput v30, v2, v29

    const/16 v29, -0x68

    aput v29, v2, v28

    const/16 v28, -0x59

    aput v28, v2, v27

    const/16 v27, -0x22e2

    aput v27, v2, v26

    const/16 v26, -0x51

    aput v26, v2, v25

    const/16 v25, -0x4ce4

    aput v25, v2, v24

    const/16 v24, -0x40

    aput v24, v2, v23

    const/16 v23, -0x6cf1

    aput v23, v2, v22

    const/16 v22, -0xa

    aput v22, v2, v21

    const/16 v21, -0x49

    aput v21, v2, v20

    const/16 v20, -0x41aa

    aput v20, v2, v19

    const/16 v19, -0x36

    aput v19, v2, v18

    const/16 v18, -0x7c2

    aput v18, v2, v17

    const/16 v17, -0x67

    aput v17, v2, v16

    const/16 v16, -0x7baf

    aput v16, v2, v15

    const/16 v15, -0x34

    aput v15, v2, v14

    const/16 v14, -0x1b

    aput v14, v2, v13

    const/16 v13, -0x71

    aput v13, v2, v12

    const/16 v12, -0x3e

    aput v12, v2, v11

    const/16 v11, -0x1c

    aput v11, v2, v10

    const/16 v10, 0x3e0a

    aput v10, v2, v9

    const/16 v9, 0x7652

    aput v9, v2, v8

    const/16 v8, 0x561a

    aput v8, v2, v7

    const/16 v7, 0x3937

    aput v7, v2, v3

    const/16 v3, 0x285a

    aput v3, v2, v1

    const/16 v1, 0x2c

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x11

    const/16 v24, 0x12

    const/16 v25, 0x13

    const/16 v26, 0x14

    const/16 v27, 0x15

    const/16 v28, 0x16

    const/16 v29, 0x17

    const/16 v30, 0x18

    const/16 v31, 0x19

    const/16 v32, 0x1a

    const/16 v33, 0x1b

    const/16 v34, 0x1c

    const/16 v35, 0x1d

    const/16 v36, 0x1e

    const/16 v37, 0x1f

    const/16 v38, 0x20

    const/16 v39, 0x21

    const/16 v40, 0x22

    const/16 v41, 0x23

    const/16 v42, 0x24

    const/16 v43, 0x25

    const/16 v44, 0x26

    const/16 v45, 0x27

    const/16 v46, 0x28

    const/16 v47, 0x29

    const/16 v48, 0x2a

    const/16 v49, 0x2b

    const/16 v50, 0x4a33

    aput v50, v1, v49

    const/16 v49, 0x44a

    aput v49, v1, v48

    const/16 v48, -0x2ffc

    aput v48, v1, v47

    const/16 v47, -0x30

    aput v47, v1, v46

    const/16 v46, -0x6a

    aput v46, v1, v45

    const/16 v45, -0x7c8b

    aput v45, v1, v44

    const/16 v44, -0x7d

    aput v44, v1, v43

    const/16 v43, -0x34

    aput v43, v1, v42

    const/16 v42, -0x74

    aput v42, v1, v41

    const/16 v41, -0x4a

    aput v41, v1, v40

    const/16 v40, -0x32

    aput v40, v1, v39

    const/16 v39, -0x7a

    aput v39, v1, v38

    const/16 v38, -0x3e

    aput v38, v1, v37

    const/16 v37, -0x6e

    aput v37, v1, v36

    const/16 v36, -0xe

    aput v36, v1, v35

    const/16 v35, -0x35fe

    aput v35, v1, v34

    const/16 v34, -0x36

    aput v34, v1, v33

    const/16 v33, -0x76fb

    aput v33, v1, v32

    const/16 v32, -0x77

    aput v32, v1, v31

    const/16 v31, -0x2d1

    aput v31, v1, v30

    const/16 v30, -0x3

    aput v30, v1, v29

    const/16 v29, -0x40

    aput v29, v1, v28

    const/16 v28, -0x22d0

    aput v28, v1, v27

    const/16 v27, -0x23

    aput v27, v1, v26

    const/16 v26, -0x4c8d

    aput v26, v1, v25

    const/16 v25, -0x4d

    aput v25, v1, v24

    const/16 v24, -0x6c9f

    aput v24, v1, v23

    const/16 v23, -0x6d

    aput v23, v1, v22

    const/16 v22, -0x1c

    aput v22, v1, v21

    const/16 v21, -0x41c2

    aput v21, v1, v20

    const/16 v20, -0x42

    aput v20, v1, v19

    const/16 v19, -0x7ae

    aput v19, v1, v18

    const/16 v18, -0x8

    aput v18, v1, v17

    const/16 v17, -0x7bcc

    aput v17, v1, v16

    const/16 v16, -0x7c

    aput v16, v1, v15

    const/16 v15, -0x78

    aput v15, v1, v14

    const/16 v14, -0x51

    aput v14, v1, v13

    const/16 v13, -0x5b

    aput v13, v1, v12

    const/16 v12, -0x76

    aput v12, v1, v11

    const/16 v11, 0x3e63

    aput v11, v1, v10

    const/16 v10, 0x763e

    aput v10, v1, v9

    const/16 v9, 0x5676

    aput v9, v1, v8

    const/16 v8, 0x3956

    aput v8, v1, v7

    const/16 v7, 0x2839

    aput v7, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v7, v1

    if-lt v3, v7, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v7, v1

    if-lt v3, v7, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-virtual/range {p4 .. p4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x0

    const/4 v2, 0x1

    array-length v3, v4

    add-int/lit8 v3, v3, -0x1

    aget-wide v6, v4, v3

    long-to-int v3, v6

    if-lt v2, v3, :cond_6

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_3
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_4
    aget v7, v1, v3

    aget v8, v2, v3

    xor-int/2addr v7, v8

    aput v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_5
    aget v7, v2, v3

    int-to-char v7, v7

    aput-char v7, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_6
    const/4 v3, 0x0

    int-to-long v1, v1

    const/16 v6, 0x20

    shl-long v6, v1, v6

    aget-wide v1, v4, v3

    const-wide/16 v8, 0x0

    cmp-long v8, v1, v8

    if-eqz v8, :cond_7

    const-wide v8, 0x3a6b124487cb929eL    # 2.7335086244674476E-27

    xor-long/2addr v1, v8

    :cond_7
    const/16 v8, 0x20

    shl-long/2addr v1, v8

    const/16 v8, 0x20

    ushr-long/2addr v1, v8

    xor-long/2addr v1, v6

    const-wide v6, 0x3a6b124487cb929eL    # 2.7335086244674476E-27

    xor-long/2addr v1, v6

    aput-wide v1, v4, v3

    :goto_4
    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_a

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x19

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x2a

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_8

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_6
    array-length v5, v1

    if-lt v3, v5, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_8
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_9
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_a
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v3, v1, v6

    if-eqz v3, :cond_b

    const-wide v6, 0x3a6b124487cb929eL    # 2.7335086244674476E-27

    xor-long/2addr v1, v6

    :cond_b
    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_80

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_e

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x75

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x46

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_c

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_8
    array-length v5, v1

    if-lt v3, v5, :cond_d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_c
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_d
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :cond_e
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v3, v1, v6

    if-eqz v3, :cond_f

    const-wide v6, 0x3a6b124487cb929eL    # 2.7335086244674476E-27

    xor-long/2addr v1, v6

    :cond_f
    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v1

    const/4 v2, 0x2

    array-length v3, v4

    add-int/lit8 v3, v3, -0x1

    aget-wide v6, v4, v3

    long-to-int v3, v6

    if-lt v2, v3, :cond_10

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_10
    const/4 v3, 0x1

    int-to-long v1, v1

    const/16 v6, 0x20

    shl-long/2addr v1, v6

    const/16 v6, 0x20

    ushr-long v6, v1, v6

    aget-wide v1, v4, v3

    const-wide/16 v8, 0x0

    cmp-long v8, v1, v8

    if-eqz v8, :cond_11

    const-wide v8, 0x3a6b124487cb929eL    # 2.7335086244674476E-27

    xor-long/2addr v1, v8

    :cond_11
    const/16 v8, 0x20

    ushr-long/2addr v1, v8

    const/16 v8, 0x20

    shl-long/2addr v1, v8

    xor-long/2addr v1, v6

    const-wide v6, 0x3a6b124487cb929eL    # 2.7335086244674476E-27

    xor-long/2addr v1, v6

    aput-wide v1, v4, v3

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_14

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x371

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x340

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_9
    array-length v5, v1

    if-lt v3, v5, :cond_12

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_a
    array-length v5, v1

    if-lt v3, v5, :cond_13

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_12
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    :cond_13
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    :cond_14
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v3, v1, v6

    if-eqz v3, :cond_15

    const-wide v6, 0x3a6b124487cb929eL    # 2.7335086244674476E-27

    xor-long/2addr v1, v6

    :cond_15
    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->isConnected()Z

    move-result v1

    if-nez v1, :cond_23

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_18

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x2e30

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x2e01

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_b
    array-length v5, v1

    if-lt v3, v5, :cond_16

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_c
    array-length v5, v1

    if-lt v3, v5, :cond_17

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_16
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_b

    :cond_17
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_c

    :cond_18
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v3, v1, v6

    if-eqz v3, :cond_19

    const-wide v6, 0x3a6b124487cb929eL    # 2.7335086244674476E-27

    xor-long/2addr v1, v6

    :cond_19
    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_23

    const/16 v1, 0x1a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, -0x6bcb

    aput v30, v2, v29

    const/16 v29, -0xf

    aput v29, v2, v28

    const/16 v28, 0x1e2d

    aput v28, v2, v27

    const/16 v27, -0x5d8e

    aput v27, v2, v26

    const/16 v26, -0x33

    aput v26, v2, v25

    const/16 v25, -0x47ec

    aput v25, v2, v24

    const/16 v24, -0x34

    aput v24, v2, v23

    const/16 v23, -0x72

    aput v23, v2, v22

    const/16 v22, -0x74

    aput v22, v2, v21

    const/16 v21, 0x3d1b

    aput v21, v2, v20

    const/16 v20, 0x1a49

    aput v20, v2, v19

    const/16 v19, 0x6474

    aput v19, v2, v18

    const/16 v18, -0x5cff

    aput v18, v2, v17

    const/16 v17, -0x32

    aput v17, v2, v16

    const/16 v16, 0x1e55

    aput v16, v2, v15

    const/16 v15, -0x881

    aput v15, v2, v14

    const/16 v14, -0x7b

    aput v14, v2, v13

    const/16 v13, 0x1456

    aput v13, v2, v12

    const/16 v12, 0x3473

    aput v12, v2, v11

    const/16 v11, 0x585a

    aput v11, v2, v10

    const/16 v10, -0x34cf

    aput v10, v2, v9

    const/16 v9, -0x5b

    aput v9, v2, v8

    const/16 v8, 0xe57

    aput v8, v2, v7

    const/16 v7, 0x526f

    aput v7, v2, v6

    const/16 v6, 0x1e31

    aput v6, v2, v3

    const/16 v3, -0x20b3

    aput v3, v2, v1

    const/16 v1, 0x1a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, -0x6bb9

    aput v31, v1, v30

    const/16 v30, -0x6c

    aput v30, v1, v29

    const/16 v29, 0x1e41

    aput v29, v1, v28

    const/16 v28, -0x5de2

    aput v28, v1, v27

    const/16 v27, -0x5e

    aput v27, v1, v26

    const/16 v26, -0x479a

    aput v26, v1, v25

    const/16 v25, -0x48

    aput v25, v1, v24

    const/16 v24, -0x20

    aput v24, v1, v23

    const/16 v23, -0x1d

    aput v23, v1, v22

    const/16 v22, 0x3d58

    aput v22, v1, v21

    const/16 v21, 0x1a3d

    aput v21, v1, v20

    const/16 v20, 0x641a

    aput v20, v1, v19

    const/16 v19, -0x5c9c

    aput v19, v1, v18

    const/16 v18, -0x5d

    aput v18, v1, v17

    const/16 v17, 0x1e32

    aput v17, v1, v16

    const/16 v16, -0x8e2

    aput v16, v1, v15

    const/16 v15, -0x9

    aput v15, v1, v14

    const/16 v14, 0x1410

    aput v14, v1, v13

    const/16 v13, 0x3414

    aput v13, v1, v12

    const/16 v12, 0x5834

    aput v12, v1, v11

    const/16 v11, -0x34a8

    aput v11, v1, v10

    const/16 v10, -0x35

    aput v10, v1, v9

    const/16 v9, 0xe39

    aput v9, v1, v8

    const/16 v8, 0x520e

    aput v8, v1, v7

    const/16 v7, 0x1e52

    aput v7, v1, v6

    const/16 v6, -0x20e2

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_d
    array-length v6, v1

    if-lt v3, v6, :cond_1a

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_e
    array-length v6, v1

    if-lt v3, v6, :cond_1b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x26

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x11

    const/16 v24, 0x12

    const/16 v25, 0x13

    const/16 v26, 0x14

    const/16 v27, 0x15

    const/16 v28, 0x16

    const/16 v29, 0x17

    const/16 v30, 0x18

    const/16 v31, 0x19

    const/16 v32, 0x1a

    const/16 v33, 0x1b

    const/16 v34, 0x1c

    const/16 v35, 0x1d

    const/16 v36, 0x1e

    const/16 v37, 0x1f

    const/16 v38, 0x20

    const/16 v39, 0x21

    const/16 v40, 0x22

    const/16 v41, 0x23

    const/16 v42, 0x24

    const/16 v43, 0x25

    const/16 v44, -0x5a96

    aput v44, v2, v43

    const/16 v43, -0x61

    aput v43, v2, v42

    const/16 v42, -0x4

    aput v42, v2, v41

    const/16 v41, -0xf

    aput v41, v2, v40

    const/16 v40, 0x2d1a

    aput v40, v2, v39

    const/16 v39, -0x21bc

    aput v39, v2, v38

    const/16 v38, -0x4e

    aput v38, v2, v37

    const/16 v37, -0x2d

    aput v37, v2, v36

    const/16 v36, -0x70a8

    aput v36, v2, v35

    const/16 v35, -0x16

    aput v35, v2, v34

    const/16 v34, -0x4b

    aput v34, v2, v33

    const/16 v33, -0x5d

    aput v33, v2, v32

    const/16 v32, -0x62aa

    aput v32, v2, v31

    const/16 v31, -0xd

    aput v31, v2, v30

    const/16 v30, -0x2

    aput v30, v2, v29

    const/16 v29, -0xd

    aput v29, v2, v28

    const/16 v28, -0x43

    aput v28, v2, v27

    const/16 v27, 0x6f14

    aput v27, v2, v26

    const/16 v26, 0x4200

    aput v26, v2, v25

    const/16 v25, -0x42ca

    aput v25, v2, v24

    const/16 v24, -0x63

    aput v24, v2, v23

    const/16 v23, -0x4c

    aput v23, v2, v22

    const/16 v22, -0x61

    aput v22, v2, v21

    const/16 v21, -0x7c

    aput v21, v2, v20

    const/16 v20, -0x5289

    aput v20, v2, v19

    const/16 v19, -0x38

    aput v19, v2, v18

    const/16 v18, -0x7c

    aput v18, v2, v17

    const/16 v17, -0xf84

    aput v17, v2, v16

    const/16 v16, -0x69

    aput v16, v2, v15

    const/16 v15, -0x75

    aput v15, v2, v14

    const/16 v14, -0x7dd2

    aput v14, v2, v13

    const/16 v13, -0x1a

    aput v13, v2, v12

    const/16 v12, -0x23

    aput v12, v2, v11

    const/16 v11, -0x35

    aput v11, v2, v10

    const/16 v10, -0x2d

    aput v10, v2, v9

    const/16 v9, -0x5b

    aput v9, v2, v8

    const/16 v8, 0x6957

    aput v8, v2, v3

    const/16 v3, -0x6ed9

    aput v3, v2, v1

    const/16 v1, 0x26

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v11, 0x4

    const/4 v12, 0x5

    const/4 v13, 0x6

    const/4 v14, 0x7

    const/16 v15, 0x8

    const/16 v16, 0x9

    const/16 v17, 0xa

    const/16 v18, 0xb

    const/16 v19, 0xc

    const/16 v20, 0xd

    const/16 v21, 0xe

    const/16 v22, 0xf

    const/16 v23, 0x10

    const/16 v24, 0x11

    const/16 v25, 0x12

    const/16 v26, 0x13

    const/16 v27, 0x14

    const/16 v28, 0x15

    const/16 v29, 0x16

    const/16 v30, 0x17

    const/16 v31, 0x18

    const/16 v32, 0x19

    const/16 v33, 0x1a

    const/16 v34, 0x1b

    const/16 v35, 0x1c

    const/16 v36, 0x1d

    const/16 v37, 0x1e

    const/16 v38, 0x1f

    const/16 v39, 0x20

    const/16 v40, 0x21

    const/16 v41, 0x22

    const/16 v42, 0x23

    const/16 v43, 0x24

    const/16 v44, 0x25

    const/16 v45, -0x5ab6

    aput v45, v1, v44

    const/16 v44, -0x5b

    aput v44, v1, v43

    const/16 v43, -0x24

    aput v43, v1, v42

    const/16 v42, -0x7b

    aput v42, v1, v41

    const/16 v41, 0x2d69

    aput v41, v1, v40

    const/16 v40, -0x21d3

    aput v40, v1, v39

    const/16 v39, -0x22

    aput v39, v1, v38

    const/16 v38, -0xd

    aput v38, v1, v37

    const/16 v37, -0x70c4

    aput v37, v1, v36

    const/16 v36, -0x71

    aput v36, v1, v35

    const/16 v35, -0x3f

    aput v35, v1, v34

    const/16 v34, -0x40

    aput v34, v1, v33

    const/16 v33, -0x62cd

    aput v33, v1, v32

    const/16 v32, -0x63

    aput v32, v1, v31

    const/16 v31, -0x70

    aput v31, v1, v30

    const/16 v30, -0x64

    aput v30, v1, v29

    const/16 v29, -0x22

    aput v29, v1, v28

    const/16 v28, 0x6f34

    aput v28, v1, v27

    const/16 v27, 0x426f

    aput v27, v1, v26

    const/16 v26, -0x42be

    aput v26, v1, v25

    const/16 v25, -0x43

    aput v25, v1, v24

    const/16 v24, -0x2f

    aput v24, v1, v23

    const/16 v23, -0x4

    aput v23, v1, v22

    const/16 v22, -0x13

    aput v22, v1, v21

    const/16 v21, -0x52ff

    aput v21, v1, v20

    const/16 v20, -0x53

    aput v20, v1, v19

    const/16 v19, -0x20

    aput v19, v1, v18

    const/16 v18, -0xfa4

    aput v18, v1, v17

    const/16 v17, -0x10

    aput v17, v1, v16

    const/16 v16, -0x1b

    aput v16, v1, v15

    const/16 v15, -0x7db9

    aput v15, v1, v14

    const/16 v14, -0x7e

    aput v14, v1, v13

    const/16 v13, -0x47

    aput v13, v1, v12

    const/16 v12, -0x56

    aput v12, v1, v11

    const/16 v11, -0xd

    aput v11, v1, v10

    const/16 v10, -0x2f

    aput v10, v1, v9

    const/16 v9, 0x6938

    aput v9, v1, v8

    const/16 v8, -0x6e97

    aput v8, v1, v3

    const/4 v3, 0x0

    :goto_f
    array-length v8, v1

    if-lt v3, v8, :cond_1c

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_10
    array-length v8, v1

    if-lt v3, v8, :cond_1d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_20

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x69

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x5a

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_11
    array-length v5, v1

    if-lt v3, v5, :cond_1e

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_12
    array-length v5, v1

    if-lt v3, v5, :cond_1f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1a
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_d

    :cond_1b
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_e

    :cond_1c
    aget v8, v1, v3

    aget v9, v2, v3

    xor-int/2addr v8, v9

    aput v8, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_f

    :cond_1d
    aget v8, v2, v3

    int-to-char v8, v8

    aput-char v8, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_10

    :cond_1e
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_11

    :cond_1f
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_12

    :cond_20
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_21

    const-wide v7, 0x3a6b124487cb929eL    # 2.7335086244674476E-27

    xor-long/2addr v1, v7

    :cond_21
    const/16 v7, 0x20

    shr-long/2addr v1, v7

    long-to-int v1, v1

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_22
    :goto_13
    const/4 v3, 0x1

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-lt v3, v1, :cond_7c

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_23
    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_26

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x62

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x51

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_14
    array-length v5, v1

    if-lt v3, v5, :cond_24

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_15
    array-length v5, v1

    if-lt v3, v5, :cond_25

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_24
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_14

    :cond_25
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_15

    :cond_26
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v3, v1, v6

    if-eqz v3, :cond_27

    const-wide v6, 0x3a6b124487cb929eL    # 2.7335086244674476E-27

    xor-long/2addr v1, v6

    :cond_27
    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v1

    const/16 v2, 0x2713

    if-ne v1, v2, :cond_30

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->cauL()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->isPluginSupport(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_30

    const/16 v1, 0x1a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, -0x69

    aput v30, v2, v29

    const/16 v29, 0x4850

    aput v29, v2, v28

    const/16 v28, 0x6624

    aput v28, v2, v27

    const/16 v27, 0x390a

    aput v27, v2, v26

    const/16 v26, 0x2b56

    aput v26, v2, v25

    const/16 v25, 0x6559

    aput v25, v2, v24

    const/16 v24, -0x1aef

    aput v24, v2, v23

    const/16 v23, -0x75

    aput v23, v2, v22

    const/16 v22, 0x1529

    aput v22, v2, v21

    const/16 v21, 0x4456

    aput v21, v2, v20

    const/16 v20, -0x6cd0

    aput v20, v2, v19

    const/16 v19, -0x3

    aput v19, v2, v18

    const/16 v18, -0x27

    aput v18, v2, v17

    const/16 v17, -0x76

    aput v17, v2, v16

    const/16 v16, -0x76c8

    aput v16, v2, v15

    const/16 v15, -0x18

    aput v15, v2, v14

    const/16 v14, -0x3ba

    aput v14, v2, v13

    const/16 v13, -0x46

    aput v13, v2, v12

    const/16 v12, -0x5c

    aput v12, v2, v11

    const/16 v11, -0x7f

    aput v11, v2, v10

    const/16 v10, 0x3210

    aput v10, v2, v9

    const/16 v9, 0x335c

    aput v9, v2, v8

    const/16 v8, 0x5d5d

    aput v8, v2, v7

    const/16 v7, 0x93c

    aput v7, v2, v6

    const/16 v6, 0x3d6a

    aput v6, v2, v3

    const/16 v3, -0x7592

    aput v3, v2, v1

    const/16 v1, 0x1a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, -0x1b

    aput v31, v1, v30

    const/16 v30, 0x4835

    aput v30, v1, v29

    const/16 v29, 0x6648

    aput v29, v1, v28

    const/16 v28, 0x3966

    aput v28, v1, v27

    const/16 v27, 0x2b39

    aput v27, v1, v26

    const/16 v26, 0x652b

    aput v26, v1, v25

    const/16 v25, -0x1a9b

    aput v25, v1, v24

    const/16 v24, -0x1b

    aput v24, v1, v23

    const/16 v23, 0x1546

    aput v23, v1, v22

    const/16 v22, 0x4415

    aput v22, v1, v21

    const/16 v21, -0x6cbc

    aput v21, v1, v20

    const/16 v20, -0x6d

    aput v20, v1, v19

    const/16 v19, -0x44

    aput v19, v1, v18

    const/16 v18, -0x19

    aput v18, v1, v17

    const/16 v17, -0x76a1

    aput v17, v1, v16

    const/16 v16, -0x77

    aput v16, v1, v15

    const/16 v15, -0x3cc

    aput v15, v1, v14

    const/4 v14, -0x4

    aput v14, v1, v13

    const/16 v13, -0x3d

    aput v13, v1, v12

    const/16 v12, -0x11

    aput v12, v1, v11

    const/16 v11, 0x3279

    aput v11, v1, v10

    const/16 v10, 0x3332

    aput v10, v1, v9

    const/16 v9, 0x5d33

    aput v9, v1, v8

    const/16 v8, 0x95d

    aput v8, v1, v7

    const/16 v7, 0x3d09

    aput v7, v1, v6

    const/16 v6, -0x75c3

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_16
    array-length v6, v1

    if-lt v3, v6, :cond_28

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_17
    array-length v6, v1

    if-lt v3, v6, :cond_29

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x26

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x11

    const/16 v24, 0x12

    const/16 v25, 0x13

    const/16 v26, 0x14

    const/16 v27, 0x15

    const/16 v28, 0x16

    const/16 v29, 0x17

    const/16 v30, 0x18

    const/16 v31, 0x19

    const/16 v32, 0x1a

    const/16 v33, 0x1b

    const/16 v34, 0x1c

    const/16 v35, 0x1d

    const/16 v36, 0x1e

    const/16 v37, 0x1f

    const/16 v38, 0x20

    const/16 v39, 0x21

    const/16 v40, 0x22

    const/16 v41, 0x23

    const/16 v42, 0x24

    const/16 v43, 0x25

    const/16 v44, -0x38

    aput v44, v2, v43

    const/16 v43, -0x1a

    aput v43, v2, v42

    const/16 v42, -0x50

    aput v42, v2, v41

    const/16 v41, -0x3b

    aput v41, v2, v40

    const/16 v40, -0x52cb

    aput v40, v2, v39

    const/16 v39, -0x3c

    aput v39, v2, v38

    const/16 v38, -0x2

    aput v38, v2, v37

    const/16 v37, -0x45

    aput v37, v2, v36

    const/16 v36, -0x36

    aput v36, v2, v35

    const/16 v35, -0x20de

    aput v35, v2, v34

    const/16 v34, -0x55

    aput v34, v2, v33

    const/16 v33, -0x47

    aput v33, v2, v32

    const/16 v32, -0x19

    aput v32, v2, v31

    const/16 v31, -0x4

    aput v31, v2, v30

    const/16 v30, -0x2999

    aput v30, v2, v29

    const/16 v29, -0x47

    aput v29, v2, v28

    const/16 v28, -0x46

    aput v28, v2, v27

    const/16 v27, 0x6332

    aput v27, v2, v26

    const/16 v26, 0x600c

    aput v26, v2, v25

    const/16 v25, 0x5a14

    aput v25, v2, v24

    const/16 v24, -0x1986

    aput v24, v2, v23

    const/16 v23, -0x7d

    aput v23, v2, v22

    const/16 v22, -0x20

    aput v22, v2, v21

    const/16 v21, -0x5b

    aput v21, v2, v20

    const/16 v20, -0x23bb

    aput v20, v2, v19

    const/16 v19, -0x47

    aput v19, v2, v18

    const/16 v18, -0xf

    aput v18, v2, v17

    const/16 v17, -0x69

    aput v17, v2, v16

    const/16 v16, -0x4

    aput v16, v2, v15

    const/16 v15, -0x60

    aput v15, v2, v14

    const/16 v14, -0x6b

    aput v14, v2, v13

    const/16 v13, 0x353d

    aput v13, v2, v12

    const/16 v12, -0x36af

    aput v12, v2, v11

    const/16 v11, -0x58

    aput v11, v2, v10

    const/16 v10, 0x767d

    aput v10, v2, v9

    const/16 v9, 0x5b02

    aput v9, v2, v8

    const/16 v8, -0x34cc

    aput v8, v2, v3

    const/16 v3, -0x7b

    aput v3, v2, v1

    const/16 v1, 0x26

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v11, 0x4

    const/4 v12, 0x5

    const/4 v13, 0x6

    const/4 v14, 0x7

    const/16 v15, 0x8

    const/16 v16, 0x9

    const/16 v17, 0xa

    const/16 v18, 0xb

    const/16 v19, 0xc

    const/16 v20, 0xd

    const/16 v21, 0xe

    const/16 v22, 0xf

    const/16 v23, 0x10

    const/16 v24, 0x11

    const/16 v25, 0x12

    const/16 v26, 0x13

    const/16 v27, 0x14

    const/16 v28, 0x15

    const/16 v29, 0x16

    const/16 v30, 0x17

    const/16 v31, 0x18

    const/16 v32, 0x19

    const/16 v33, 0x1a

    const/16 v34, 0x1b

    const/16 v35, 0x1c

    const/16 v36, 0x1d

    const/16 v37, 0x1e

    const/16 v38, 0x1f

    const/16 v39, 0x20

    const/16 v40, 0x21

    const/16 v41, 0x22

    const/16 v42, 0x23

    const/16 v43, 0x24

    const/16 v44, 0x25

    const/16 v45, -0x18

    aput v45, v1, v44

    const/16 v44, -0x24

    aput v44, v1, v43

    const/16 v43, -0x70

    aput v43, v1, v42

    const/16 v42, -0x4f

    aput v42, v1, v41

    const/16 v41, -0x52ba

    aput v41, v1, v40

    const/16 v40, -0x53

    aput v40, v1, v39

    const/16 v39, -0x6e

    aput v39, v1, v38

    const/16 v38, -0x65

    aput v38, v1, v37

    const/16 v37, -0x52

    aput v37, v1, v36

    const/16 v36, -0x20b9

    aput v36, v1, v35

    const/16 v35, -0x21

    aput v35, v1, v34

    const/16 v34, -0x26

    aput v34, v1, v33

    const/16 v33, -0x7e

    aput v33, v1, v32

    const/16 v32, -0x6e

    aput v32, v1, v31

    const/16 v31, -0x29f7

    aput v31, v1, v30

    const/16 v30, -0x2a

    aput v30, v1, v29

    const/16 v29, -0x27

    aput v29, v1, v28

    const/16 v28, 0x6312

    aput v28, v1, v27

    const/16 v27, 0x6063

    aput v27, v1, v26

    const/16 v26, 0x5a60

    aput v26, v1, v25

    const/16 v25, -0x19a6

    aput v25, v1, v24

    const/16 v24, -0x1a

    aput v24, v1, v23

    const/16 v23, -0x7d

    aput v23, v1, v22

    const/16 v22, -0x34

    aput v22, v1, v21

    const/16 v21, -0x23cd

    aput v21, v1, v20

    const/16 v20, -0x24

    aput v20, v1, v19

    const/16 v19, -0x6b

    aput v19, v1, v18

    const/16 v18, -0x49

    aput v18, v1, v17

    const/16 v17, -0x65

    aput v17, v1, v16

    const/16 v16, -0x32

    aput v16, v1, v15

    const/4 v15, -0x4

    aput v15, v1, v14

    const/16 v14, 0x3559

    aput v14, v1, v13

    const/16 v13, -0x36cb

    aput v13, v1, v12

    const/16 v12, -0x37

    aput v12, v1, v11

    const/16 v11, 0x765d

    aput v11, v1, v10

    const/16 v10, 0x5b76

    aput v10, v1, v9

    const/16 v9, -0x34a5

    aput v9, v1, v8

    const/16 v8, -0x35

    aput v8, v1, v3

    const/4 v3, 0x0

    :goto_18
    array-length v8, v1

    if-lt v3, v8, :cond_2a

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_19
    array-length v8, v1

    if-lt v3, v8, :cond_2b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_2e

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x18

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x27

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_1a
    array-length v5, v1

    if-lt v3, v5, :cond_2c

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1b
    array-length v5, v1

    if-lt v3, v5, :cond_2d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_28
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_16

    :cond_29
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_17

    :cond_2a
    aget v8, v1, v3

    aget v9, v2, v3

    xor-int/2addr v8, v9

    aput v8, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_18

    :cond_2b
    aget v8, v2, v3

    int-to-char v8, v8

    aput-char v8, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_19

    :cond_2c
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1a

    :cond_2d
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1b

    :cond_2e
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_2f

    const-wide v7, 0x3a6b124487cb929eL    # 2.7335086244674476E-27

    xor-long/2addr v1, v7

    :cond_2f
    const/16 v7, 0x20

    shr-long/2addr v1, v7

    long-to-int v1, v1

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_13

    :cond_30
    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_33

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x2347

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x2376

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_1c
    array-length v5, v1

    if-lt v3, v5, :cond_31

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1d
    array-length v5, v1

    if-lt v3, v5, :cond_32

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_31
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1c

    :cond_32
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1d

    :cond_33
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v3, v1, v6

    if-eqz v3, :cond_34

    const-wide v6, 0x3a6b124487cb929eL    # 2.7335086244674476E-27

    xor-long/2addr v1, v6

    :cond_34
    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v1

    const/16 v2, 0x2714

    if-ne v1, v2, :cond_40

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mContext:Landroid/content/Context;

    const/16 v1, 0x24

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, 0x1a

    const/16 v32, 0x1b

    const/16 v33, 0x1c

    const/16 v34, 0x1d

    const/16 v35, 0x1e

    const/16 v36, 0x1f

    const/16 v37, 0x20

    const/16 v38, 0x21

    const/16 v39, 0x22

    const/16 v40, 0x23

    const/16 v41, -0x24

    aput v41, v2, v40

    const/16 v40, 0x3921

    aput v40, v2, v39

    const/16 v39, 0x176

    aput v39, v2, v38

    const/16 v38, -0x4abe

    aput v38, v2, v37

    const/16 v37, -0x20

    aput v37, v2, v36

    const/16 v36, -0x2b2

    aput v36, v2, v35

    const/16 v35, -0x46

    aput v35, v2, v34

    const/16 v34, 0x4f38

    aput v34, v2, v33

    const/16 v33, 0x6b0b

    aput v33, v2, v32

    const/16 v32, 0x4c24

    aput v32, v2, v31

    const/16 v31, 0x5a03

    aput v31, v2, v30

    const/16 v30, 0x1416

    aput v30, v2, v29

    const/16 v29, 0x1e56

    aput v29, v2, v28

    const/16 v28, 0x1a30

    aput v28, v2, v27

    const/16 v27, 0x974

    aput v27, v2, v26

    const/16 v26, -0x389a

    aput v26, v2, v25

    const/16 v25, -0x52

    aput v25, v2, v24

    const/16 v24, 0x6346

    aput v24, v2, v23

    const/16 v23, 0x6c00

    aput v23, v2, v22

    const/16 v22, 0x210d

    aput v22, v2, v21

    const/16 v21, -0x18f1

    aput v21, v2, v20

    const/16 v20, -0x71

    aput v20, v2, v19

    const/16 v19, -0x62

    aput v19, v2, v18

    const/16 v18, 0x2a2c

    aput v18, v2, v17

    const/16 v17, 0x524b

    aput v17, v2, v16

    const/16 v16, -0x7cc9

    aput v16, v2, v15

    const/16 v15, -0x15

    aput v15, v2, v14

    const/16 v14, -0x18

    aput v14, v2, v13

    const/16 v13, -0x41

    aput v13, v2, v12

    const/16 v12, 0x3536

    aput v12, v2, v11

    const/16 v11, -0xfb0

    aput v11, v2, v10

    const/16 v10, -0x7d

    aput v10, v2, v9

    const/16 v9, 0x3829

    aput v9, v2, v8

    const/16 v8, -0x24ab

    aput v8, v2, v7

    const/16 v7, -0x4c

    aput v7, v2, v3

    const/16 v3, -0x65

    aput v3, v2, v1

    const/16 v1, 0x24

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x11

    const/16 v24, 0x12

    const/16 v25, 0x13

    const/16 v26, 0x14

    const/16 v27, 0x15

    const/16 v28, 0x16

    const/16 v29, 0x17

    const/16 v30, 0x18

    const/16 v31, 0x19

    const/16 v32, 0x1a

    const/16 v33, 0x1b

    const/16 v34, 0x1c

    const/16 v35, 0x1d

    const/16 v36, 0x1e

    const/16 v37, 0x1f

    const/16 v38, 0x20

    const/16 v39, 0x21

    const/16 v40, 0x22

    const/16 v41, 0x23

    const/16 v42, -0x67

    aput v42, v1, v41

    const/16 v41, 0x3972

    aput v41, v1, v40

    const/16 v40, 0x139

    aput v40, v1, v39

    const/16 v39, -0x4aff

    aput v39, v1, v38

    const/16 v38, -0x4b

    aput v38, v1, v37

    const/16 v37, -0x2fe

    aput v37, v1, v36

    const/16 v36, -0x3

    aput v36, v1, v35

    const/16 v35, 0x4f67

    aput v35, v1, v34

    const/16 v34, 0x6b4f

    aput v34, v1, v33

    const/16 v33, 0x4c6b

    aput v33, v1, v32

    const/16 v32, 0x5a4c

    aput v32, v1, v31

    const/16 v31, 0x145a

    aput v31, v1, v30

    const/16 v30, 0x1e14

    aput v30, v1, v29

    const/16 v29, 0x1a1e

    aput v29, v1, v28

    const/16 v28, 0x91a

    aput v28, v1, v27

    const/16 v27, -0x38f7

    aput v27, v1, v26

    const/16 v26, -0x39

    aput v26, v1, v25

    const/16 v25, 0x6332

    aput v25, v1, v24

    const/16 v24, 0x6c63

    aput v24, v1, v23

    const/16 v23, 0x216c

    aput v23, v1, v22

    const/16 v22, -0x18df

    aput v22, v1, v21

    const/16 v21, -0x19

    aput v21, v1, v20

    const/16 v20, -0x16

    aput v20, v1, v19

    const/16 v19, 0x2a40

    aput v19, v1, v18

    const/16 v18, 0x522a

    aput v18, v1, v17

    const/16 v17, -0x7cae

    aput v17, v1, v16

    const/16 v16, -0x7d

    aput v16, v1, v15

    const/16 v15, -0x65

    aput v15, v1, v14

    const/16 v14, -0x6f

    aput v14, v1, v13

    const/16 v13, 0x3555

    aput v13, v1, v12

    const/16 v12, -0xfcb

    aput v12, v1, v11

    const/16 v11, -0x10

    aput v11, v1, v10

    const/16 v10, 0x3807

    aput v10, v1, v9

    const/16 v9, -0x24c8

    aput v9, v1, v8

    const/16 v8, -0x25

    aput v8, v1, v7

    const/4 v7, -0x8

    aput v7, v1, v3

    const/4 v3, 0x0

    :goto_1e
    array-length v7, v1

    if-lt v3, v7, :cond_36

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1f
    array-length v7, v1

    if-lt v3, v7, :cond_37

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->isPluginSupport(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_35

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/Utils;->isHestiaModel()Z

    move-result v1

    if-eqz v1, :cond_40

    :cond_35
    const/16 v1, 0x1a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, -0x31

    aput v30, v2, v29

    const/16 v29, -0x70

    aput v29, v2, v28

    const/16 v28, 0x6403

    aput v28, v2, v27

    const/16 v27, -0x29f8

    aput v27, v2, v26

    const/16 v26, -0x47

    aput v26, v2, v25

    const/16 v25, -0x2e

    aput v25, v2, v24

    const/16 v24, -0x1a2

    aput v24, v2, v23

    const/16 v23, -0x70

    aput v23, v2, v22

    const/16 v22, -0x20

    aput v22, v2, v21

    const/16 v21, -0x119a

    aput v21, v2, v20

    const/16 v20, -0x66

    aput v20, v2, v19

    const/16 v19, -0x6b

    aput v19, v2, v18

    const/16 v18, -0x3dfd

    aput v18, v2, v17

    const/16 v17, -0x51

    aput v17, v2, v16

    const/16 v16, -0x4f

    aput v16, v2, v15

    const/16 v15, -0x5d

    aput v15, v2, v14

    const/16 v14, -0x2cc9

    aput v14, v2, v13

    const/16 v13, -0x6b

    aput v13, v2, v12

    const/16 v12, -0x73

    aput v12, v2, v11

    const/16 v11, -0x41

    aput v11, v2, v10

    const/16 v10, -0x64

    aput v10, v2, v9

    const/16 v9, -0x6ce0

    aput v9, v2, v8

    const/4 v8, -0x3

    aput v8, v2, v7

    const/16 v7, -0x3f

    aput v7, v2, v6

    const/16 v6, -0x4e

    aput v6, v2, v3

    const/16 v3, 0x4944

    aput v3, v2, v1

    const/16 v1, 0x1a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, -0x43

    aput v31, v1, v30

    const/16 v30, -0xb

    aput v30, v1, v29

    const/16 v29, 0x646f

    aput v29, v1, v28

    const/16 v28, -0x299c

    aput v28, v1, v27

    const/16 v27, -0x2a

    aput v27, v1, v26

    const/16 v26, -0x60

    aput v26, v1, v25

    const/16 v25, -0x1d6

    aput v25, v1, v24

    const/16 v24, -0x2

    aput v24, v1, v23

    const/16 v23, -0x71

    aput v23, v1, v22

    const/16 v22, -0x11db

    aput v22, v1, v21

    const/16 v21, -0x12

    aput v21, v1, v20

    const/16 v20, -0x5

    aput v20, v1, v19

    const/16 v19, -0x3d9a

    aput v19, v1, v18

    const/16 v18, -0x3e

    aput v18, v1, v17

    const/16 v17, -0x2a

    aput v17, v1, v16

    const/16 v16, -0x3e

    aput v16, v1, v15

    const/16 v15, -0x2cbb

    aput v15, v1, v14

    const/16 v14, -0x2d

    aput v14, v1, v13

    const/16 v13, -0x16

    aput v13, v1, v12

    const/16 v12, -0x2f

    aput v12, v1, v11

    const/16 v11, -0xb

    aput v11, v1, v10

    const/16 v10, -0x6cb2

    aput v10, v1, v9

    const/16 v9, -0x6d

    aput v9, v1, v8

    const/16 v8, -0x60

    aput v8, v1, v7

    const/16 v7, -0x2f

    aput v7, v1, v6

    const/16 v6, 0x4917

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_20
    array-length v6, v1

    if-lt v3, v6, :cond_38

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_21
    array-length v6, v1

    if-lt v3, v6, :cond_39

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x26

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x11

    const/16 v24, 0x12

    const/16 v25, 0x13

    const/16 v26, 0x14

    const/16 v27, 0x15

    const/16 v28, 0x16

    const/16 v29, 0x17

    const/16 v30, 0x18

    const/16 v31, 0x19

    const/16 v32, 0x1a

    const/16 v33, 0x1b

    const/16 v34, 0x1c

    const/16 v35, 0x1d

    const/16 v36, 0x1e

    const/16 v37, 0x1f

    const/16 v38, 0x20

    const/16 v39, 0x21

    const/16 v40, 0x22

    const/16 v41, 0x23

    const/16 v42, 0x24

    const/16 v43, 0x25

    const/16 v44, -0x33f7

    aput v44, v2, v43

    const/16 v43, -0xa

    aput v43, v2, v42

    const/16 v42, 0xb79

    aput v42, v2, v41

    const/16 v41, -0x5981

    aput v41, v2, v40

    const/16 v40, -0x2b

    aput v40, v2, v39

    const/16 v39, 0x503e

    aput v39, v2, v38

    const/16 v38, -0x14c4

    aput v38, v2, v37

    const/16 v37, -0x35

    aput v37, v2, v36

    const/16 v36, 0x4e72

    aput v36, v2, v35

    const/16 v35, -0x41d5

    aput v35, v2, v34

    const/16 v34, -0x36

    aput v34, v2, v33

    const/16 v33, -0x7396

    aput v33, v2, v32

    const/16 v32, -0x17

    aput v32, v2, v31

    const/16 v31, 0x3a72

    aput v31, v2, v30

    const/16 v30, 0x4554

    aput v30, v2, v29

    const/16 v29, 0x12a

    aput v29, v2, v28

    const/16 v28, -0x659e

    aput v28, v2, v27

    const/16 v27, -0x46

    aput v27, v2, v26

    const/16 v26, -0x4a

    aput v26, v2, v25

    const/16 v25, -0x32

    aput v25, v2, v24

    const/16 v24, 0x1144

    aput v24, v2, v23

    const/16 v23, -0x468c

    aput v23, v2, v22

    const/16 v22, -0x26

    aput v22, v2, v21

    const/16 v21, -0x66d6

    aput v21, v2, v20

    const/16 v20, -0x11

    aput v20, v2, v19

    const/16 v19, -0x14d0

    aput v19, v2, v18

    const/16 v18, -0x71

    aput v18, v2, v17

    const/16 v17, -0x64

    aput v17, v2, v16

    const/16 v16, -0x7ddf

    aput v16, v2, v15

    const/16 v15, -0x14

    aput v15, v2, v14

    const/16 v14, -0xca

    aput v14, v2, v13

    const/16 v13, -0x65

    aput v13, v2, v12

    const/16 v12, -0xf

    aput v12, v2, v11

    const/16 v11, -0x1f

    aput v11, v2, v10

    const/16 v10, -0x42

    aput v10, v2, v9

    const/16 v9, 0x7121

    aput v9, v2, v8

    const/16 v8, -0x36e2

    aput v8, v2, v3

    const/16 v3, -0x79

    aput v3, v2, v1

    const/16 v1, 0x26

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v11, 0x4

    const/4 v12, 0x5

    const/4 v13, 0x6

    const/4 v14, 0x7

    const/16 v15, 0x8

    const/16 v16, 0x9

    const/16 v17, 0xa

    const/16 v18, 0xb

    const/16 v19, 0xc

    const/16 v20, 0xd

    const/16 v21, 0xe

    const/16 v22, 0xf

    const/16 v23, 0x10

    const/16 v24, 0x11

    const/16 v25, 0x12

    const/16 v26, 0x13

    const/16 v27, 0x14

    const/16 v28, 0x15

    const/16 v29, 0x16

    const/16 v30, 0x17

    const/16 v31, 0x18

    const/16 v32, 0x19

    const/16 v33, 0x1a

    const/16 v34, 0x1b

    const/16 v35, 0x1c

    const/16 v36, 0x1d

    const/16 v37, 0x1e

    const/16 v38, 0x1f

    const/16 v39, 0x20

    const/16 v40, 0x21

    const/16 v41, 0x22

    const/16 v42, 0x23

    const/16 v43, 0x24

    const/16 v44, 0x25

    const/16 v45, -0x33d7

    aput v45, v1, v44

    const/16 v44, -0x34

    aput v44, v1, v43

    const/16 v43, 0xb59

    aput v43, v1, v42

    const/16 v42, -0x59f5

    aput v42, v1, v41

    const/16 v41, -0x5a

    aput v41, v1, v40

    const/16 v40, 0x5057

    aput v40, v1, v39

    const/16 v39, -0x14b0

    aput v39, v1, v38

    const/16 v38, -0x15

    aput v38, v1, v37

    const/16 v37, 0x4e16

    aput v37, v1, v36

    const/16 v36, -0x41b2

    aput v36, v1, v35

    const/16 v35, -0x42

    aput v35, v1, v34

    const/16 v34, -0x73f7

    aput v34, v1, v33

    const/16 v33, -0x74

    aput v33, v1, v32

    const/16 v32, 0x3a1c

    aput v32, v1, v31

    const/16 v31, 0x453a

    aput v31, v1, v30

    const/16 v30, 0x145

    aput v30, v1, v29

    const/16 v29, -0x65ff

    aput v29, v1, v28

    const/16 v28, -0x66

    aput v28, v1, v27

    const/16 v27, -0x27

    aput v27, v1, v26

    const/16 v26, -0x46

    aput v26, v1, v25

    const/16 v25, 0x1164

    aput v25, v1, v24

    const/16 v24, -0x46ef

    aput v24, v1, v23

    const/16 v23, -0x47

    aput v23, v1, v22

    const/16 v22, -0x66bd

    aput v22, v1, v21

    const/16 v21, -0x67

    aput v21, v1, v20

    const/16 v20, -0x14ab

    aput v20, v1, v19

    const/16 v19, -0x15

    aput v19, v1, v18

    const/16 v18, -0x44

    aput v18, v1, v17

    const/16 v17, -0x7dba

    aput v17, v1, v16

    const/16 v16, -0x7e

    aput v16, v1, v15

    const/16 v15, -0xa1

    aput v15, v1, v14

    const/4 v14, -0x1

    aput v14, v1, v13

    const/16 v13, -0x6b

    aput v13, v1, v12

    const/16 v12, -0x80

    aput v12, v1, v11

    const/16 v11, -0x62

    aput v11, v1, v10

    const/16 v10, 0x7155

    aput v10, v1, v9

    const/16 v9, -0x368f

    aput v9, v1, v8

    const/16 v8, -0x37

    aput v8, v1, v3

    const/4 v3, 0x0

    :goto_22
    array-length v8, v1

    if-lt v3, v8, :cond_3a

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_23
    array-length v8, v1

    if-lt v3, v8, :cond_3b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_3e

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x77

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x48

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_24
    array-length v5, v1

    if-lt v3, v5, :cond_3c

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_25
    array-length v5, v1

    if-lt v3, v5, :cond_3d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_36
    aget v7, v1, v3

    aget v8, v2, v3

    xor-int/2addr v7, v8

    aput v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1e

    :cond_37
    aget v7, v2, v3

    int-to-char v7, v7

    aput-char v7, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1f

    :cond_38
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_20

    :cond_39
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_21

    :cond_3a
    aget v8, v1, v3

    aget v9, v2, v3

    xor-int/2addr v8, v9

    aput v8, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_22

    :cond_3b
    aget v8, v2, v3

    int-to-char v8, v8

    aput-char v8, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_23

    :cond_3c
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_24

    :cond_3d
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_25

    :cond_3e
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_3f

    const-wide v7, 0x3a6b124487cb929eL    # 2.7335086244674476E-27

    xor-long/2addr v1, v7

    :cond_3f
    const/16 v7, 0x20

    shr-long/2addr v1, v7

    long-to-int v1, v1

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_13

    :cond_40
    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x2

    if-gt v1, v2, :cond_43

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x27a4

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x2792

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_26
    array-length v5, v1

    if-lt v3, v5, :cond_41

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_27
    array-length v5, v1

    if-lt v3, v5, :cond_42

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_41
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_26

    :cond_42
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_27

    :cond_43
    const/4 v1, 0x1

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v3, v1, v6

    if-eqz v3, :cond_44

    const-wide v6, 0x3a6b124487cb929eL    # 2.7335086244674476E-27

    xor-long/2addr v1, v6

    :cond_44
    const/16 v3, 0x20

    shl-long/2addr v1, v3

    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    const/4 v2, 0x6

    if-eq v1, v2, :cond_71

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x2

    if-gt v1, v2, :cond_47

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x4c

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x7a

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_28
    array-length v5, v1

    if-lt v3, v5, :cond_45

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_29
    array-length v5, v1

    if-lt v3, v5, :cond_46

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_45
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_28

    :cond_46
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_29

    :cond_47
    const/4 v1, 0x1

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v3, v1, v6

    if-eqz v3, :cond_48

    const-wide v6, 0x3a6b124487cb929eL    # 2.7335086244674476E-27

    xor-long/2addr v1, v6

    :cond_48
    const/16 v3, 0x20

    shl-long/2addr v1, v3

    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_71

    const/16 v1, 0x1a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x7f07

    aput v30, v2, v29

    const/16 v29, 0x2b1a

    aput v29, v2, v28

    const/16 v28, 0x4747

    aput v28, v2, v27

    const/16 v27, 0x492b

    aput v27, v2, v26

    const/16 v26, -0x4da

    aput v26, v2, v25

    const/16 v25, -0x77

    aput v25, v2, v24

    const/16 v24, -0x1d

    aput v24, v2, v23

    const/16 v23, -0x17

    aput v23, v2, v22

    const/16 v22, -0x9f4

    aput v22, v2, v21

    const/16 v21, -0x4b

    aput v21, v2, v20

    const/16 v20, -0x56

    aput v20, v2, v19

    const/16 v19, -0x60bc

    aput v19, v2, v18

    const/16 v18, -0x6

    aput v18, v2, v17

    const/16 v17, -0x3e

    aput v17, v2, v16

    const/16 v16, -0x5a

    aput v16, v2, v15

    const/16 v15, -0xc

    aput v15, v2, v14

    const/16 v14, -0x3fba

    aput v14, v2, v13

    const/16 v13, -0x7a

    aput v13, v2, v12

    const/16 v12, -0x62

    aput v12, v2, v11

    const/16 v11, -0x64dd

    aput v11, v2, v10

    const/16 v10, -0xe

    aput v10, v2, v9

    const/16 v9, -0x6d

    aput v9, v2, v8

    const/16 v8, 0x5838

    aput v8, v2, v7

    const/16 v7, -0xcc7

    aput v7, v2, v6

    const/16 v6, -0x70

    aput v6, v2, v3

    const/16 v3, 0x3f35

    aput v3, v2, v1

    const/16 v1, 0x1a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, 0x7f75

    aput v31, v1, v30

    const/16 v30, 0x2b7f

    aput v30, v1, v29

    const/16 v29, 0x472b

    aput v29, v1, v28

    const/16 v28, 0x4947

    aput v28, v1, v27

    const/16 v27, -0x4b7

    aput v27, v1, v26

    const/16 v26, -0x5

    aput v26, v1, v25

    const/16 v25, -0x69

    aput v25, v1, v24

    const/16 v24, -0x79

    aput v24, v1, v23

    const/16 v23, -0x99d

    aput v23, v1, v22

    const/16 v22, -0xa

    aput v22, v1, v21

    const/16 v21, -0x22

    aput v21, v1, v20

    const/16 v20, -0x60d6

    aput v20, v1, v19

    const/16 v19, -0x61

    aput v19, v1, v18

    const/16 v18, -0x51

    aput v18, v1, v17

    const/16 v17, -0x3f

    aput v17, v1, v16

    const/16 v16, -0x6b

    aput v16, v1, v15

    const/16 v15, -0x3fcc

    aput v15, v1, v14

    const/16 v14, -0x40

    aput v14, v1, v13

    const/4 v13, -0x7

    aput v13, v1, v12

    const/16 v12, -0x64b3

    aput v12, v1, v11

    const/16 v11, -0x65

    aput v11, v1, v10

    const/4 v10, -0x3

    aput v10, v1, v9

    const/16 v9, 0x5856

    aput v9, v1, v8

    const/16 v8, -0xca8

    aput v8, v1, v7

    const/16 v7, -0xd

    aput v7, v1, v6

    const/16 v6, 0x3f66

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_2a
    array-length v6, v1

    if-lt v3, v6, :cond_49

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2b
    array-length v6, v1

    if-lt v3, v6, :cond_4a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0xe

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, -0x35

    aput v20, v2, v19

    const/16 v19, 0x2975

    aput v19, v2, v18

    const/16 v18, -0x44b6

    aput v18, v2, v17

    const/16 v17, -0x2e

    aput v17, v2, v16

    const/16 v16, 0x4843

    aput v16, v2, v15

    const/16 v15, -0x76d3

    aput v15, v2, v14

    const/16 v14, -0x13

    aput v14, v2, v13

    const/16 v13, -0x2bd3

    aput v13, v2, v12

    const/16 v12, -0x50

    aput v12, v2, v11

    const/16 v11, 0x6b66

    aput v11, v2, v10

    const/16 v10, -0x6e7

    aput v10, v2, v9

    const/16 v9, -0x70

    aput v9, v2, v8

    const/16 v8, -0x67

    aput v8, v2, v3

    const/16 v3, -0x259e

    aput v3, v2, v1

    const/16 v1, 0xe

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v11, 0x4

    const/4 v12, 0x5

    const/4 v13, 0x6

    const/4 v14, 0x7

    const/16 v15, 0x8

    const/16 v16, 0x9

    const/16 v17, 0xa

    const/16 v18, 0xb

    const/16 v19, 0xc

    const/16 v20, 0xd

    const/16 v21, -0xa

    aput v21, v1, v20

    const/16 v20, 0x2910

    aput v20, v1, v19

    const/16 v19, -0x44d7

    aput v19, v1, v18

    const/16 v18, -0x45

    aput v18, v1, v17

    const/16 v17, 0x4835

    aput v17, v1, v16

    const/16 v16, -0x76b8

    aput v16, v1, v15

    const/16 v15, -0x77

    aput v15, v1, v14

    const/16 v14, -0x2bf3

    aput v14, v1, v13

    const/16 v13, -0x2c

    aput v13, v1, v12

    const/16 v12, 0x6b03

    aput v12, v1, v11

    const/16 v11, -0x695

    aput v11, v1, v10

    const/4 v10, -0x7

    aput v10, v1, v9

    const/4 v9, -0x8

    aput v9, v1, v8

    const/16 v8, -0x25ce

    aput v8, v1, v3

    const/4 v3, 0x0

    :goto_2c
    array-length v8, v1

    if-lt v3, v8, :cond_4b

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2d
    array-length v8, v1

    if-lt v3, v8, :cond_4c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_4f

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0xcd8

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0xce7

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2e
    array-length v5, v1

    if-lt v3, v5, :cond_4d

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2f
    array-length v5, v1

    if-lt v3, v5, :cond_4e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_49
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2a

    :cond_4a
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2b

    :cond_4b
    aget v8, v1, v3

    aget v9, v2, v3

    xor-int/2addr v8, v9

    aput v8, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2c

    :cond_4c
    aget v8, v2, v3

    int-to-char v8, v8

    aput-char v8, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2d

    :cond_4d
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2e

    :cond_4e
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2f

    :cond_4f
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_50

    const-wide v7, 0x3a6b124487cb929eL    # 2.7335086244674476E-27

    xor-long/2addr v1, v7

    :cond_50
    const/16 v7, 0x20

    shr-long/2addr v1, v7

    long-to-int v1, v1

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v1, 0x7

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/16 v13, 0x2f1a

    aput v13, v2, v12

    const/16 v12, 0x344b

    aput v12, v2, v11

    const/16 v11, -0x6caf

    aput v11, v2, v10

    const/16 v10, -0x9

    aput v10, v2, v9

    const/16 v9, -0x3bb1

    aput v9, v2, v8

    const/16 v8, -0x5b

    aput v8, v2, v3

    const/16 v3, -0x1c

    aput v3, v2, v1

    const/4 v1, 0x7

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v11, 0x4

    const/4 v12, 0x5

    const/4 v13, 0x6

    const/16 v14, 0x2f34

    aput v14, v1, v13

    const/16 v13, 0x342f

    aput v13, v1, v12

    const/16 v12, -0x6ccc

    aput v12, v1, v11

    const/16 v11, -0x6d

    aput v11, v1, v10

    const/16 v10, -0x3bd5

    aput v10, v1, v9

    const/16 v9, -0x3c

    aput v9, v1, v8

    const/16 v8, -0x3c

    aput v8, v1, v3

    const/4 v3, 0x0

    :goto_30
    array-length v8, v1

    if-lt v3, v8, :cond_51

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_31
    array-length v8, v1

    if-lt v3, v8, :cond_52

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-gtz v1, :cond_55

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x4141

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x4171

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_32
    array-length v5, v1

    if-lt v3, v5, :cond_53

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_33
    array-length v5, v1

    if-lt v3, v5, :cond_54

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_51
    aget v8, v1, v3

    aget v9, v2, v3

    xor-int/2addr v8, v9

    aput v8, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_30

    :cond_52
    aget v8, v2, v3

    int-to-char v8, v8

    aput-char v8, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_31

    :cond_53
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_32

    :cond_54
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_33

    :cond_55
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v3, v1, v6

    if-eqz v3, :cond_56

    const-wide v6, 0x3a6b124487cb929eL    # 2.7335086244674476E-27

    xor-long/2addr v1, v6

    :cond_56
    const/16 v3, 0x20

    shl-long/2addr v1, v3

    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    if-eqz v1, :cond_6c

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_59

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x2a

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x19

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_34
    array-length v5, v1

    if-lt v3, v5, :cond_57

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_35
    array-length v5, v1

    if-lt v3, v5, :cond_58

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_57
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_34

    :cond_58
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_35

    :cond_59
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v3, v1, v6

    if-eqz v3, :cond_5a

    const-wide v6, 0x3a6b124487cb929eL    # 2.7335086244674476E-27

    xor-long/2addr v1, v6

    :cond_5a
    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v3

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-gtz v1, :cond_5d

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x3f

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0xf

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_36
    array-length v5, v1

    if-lt v3, v5, :cond_5b

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_37
    array-length v5, v1

    if-lt v3, v5, :cond_5c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_5b
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_36

    :cond_5c
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_37

    :cond_5d
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v6, v1, v6

    if-eqz v6, :cond_5e

    const-wide v6, 0x3a6b124487cb929eL    # 2.7335086244674476E-27

    xor-long/2addr v1, v6

    :cond_5e
    const/16 v6, 0x20

    shl-long/2addr v1, v6

    const/16 v6, 0x20

    shr-long/2addr v1, v6

    long-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_63

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_61

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0xe64

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0xe55

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_38
    array-length v5, v1

    if-lt v3, v5, :cond_5f

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_39
    array-length v5, v1

    if-lt v3, v5, :cond_60

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_5f
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_38

    :cond_60
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_39

    :cond_61
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v3, v1, v6

    if-eqz v3, :cond_62

    const-wide v6, 0x3a6b124487cb929eL    # 2.7335086244674476E-27

    xor-long/2addr v1, v6

    :cond_62
    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_63
    if-eqz p2, :cond_22

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_66

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x682c

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x681d

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_3a
    array-length v5, v1

    if-lt v3, v5, :cond_64

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3b
    array-length v5, v1

    if-lt v3, v5, :cond_65

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_64
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3a

    :cond_65
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3b

    :cond_66
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v3, v1, v6

    if-eqz v3, :cond_67

    const-wide v6, 0x3a6b124487cb929eL    # 2.7335086244674476E-27

    xor-long/2addr v1, v6

    :cond_67
    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v1

    const/4 v2, 0x7

    if-ne v1, v2, :cond_22

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_6a

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x77

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x48

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_3c
    array-length v5, v1

    if-lt v3, v5, :cond_68

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3d
    array-length v5, v1

    if-lt v3, v5, :cond_69

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_68
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3c

    :cond_69
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3d

    :cond_6a
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v3, v1, v6

    if-eqz v3, :cond_6b

    const-wide v6, 0x3a6b124487cb929eL    # 2.7335086244674476E-27

    xor-long/2addr v1, v6

    :cond_6b
    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_13

    :cond_6c
    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_6f

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x2820

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x2811

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_3e
    array-length v5, v1

    if-lt v3, v5, :cond_6d

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3f
    array-length v5, v1

    if-lt v3, v5, :cond_6e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_6d
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3e

    :cond_6e
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3f

    :cond_6f
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v3, v1, v6

    if-eqz v3, :cond_70

    const-wide v6, 0x3a6b124487cb929eL    # 2.7335086244674476E-27

    xor-long/2addr v1, v6

    :cond_70
    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_13

    :cond_71
    const/16 v1, 0x1a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, -0x3691

    aput v30, v2, v29

    const/16 v29, -0x54

    aput v29, v2, v28

    const/16 v28, 0x2839

    aput v28, v2, v27

    const/16 v27, 0x4744

    aput v27, v2, v26

    const/16 v26, -0x20d8

    aput v26, v2, v25

    const/16 v25, -0x53

    aput v25, v2, v24

    const/16 v24, -0x3bf4

    aput v24, v2, v23

    const/16 v23, -0x56

    aput v23, v2, v22

    const/16 v22, -0x4a

    aput v22, v2, v21

    const/16 v21, -0x39fb

    aput v21, v2, v20

    const/16 v20, -0x4e

    aput v20, v2, v19

    const/16 v19, 0x3e78

    aput v19, v2, v18

    const/16 v18, -0x16a5

    aput v18, v2, v17

    const/16 v17, -0x7c

    aput v17, v2, v16

    const/16 v16, -0x6e

    aput v16, v2, v15

    const/16 v15, -0x5b8c

    aput v15, v2, v14

    const/16 v14, -0x2a

    aput v14, v2, v13

    const/16 v13, -0x37e6

    aput v13, v2, v12

    const/16 v12, -0x51

    aput v12, v2, v11

    const/16 v11, -0x77

    aput v11, v2, v10

    const/16 v10, -0x6e

    aput v10, v2, v9

    const/16 v9, -0x4b

    aput v9, v2, v8

    const/16 v8, -0x4988

    aput v8, v2, v7

    const/16 v7, -0x29

    aput v7, v2, v6

    const/16 v6, -0x5df5

    aput v6, v2, v3

    const/16 v3, -0xf

    aput v3, v2, v1

    const/16 v1, 0x1a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, -0x36e3

    aput v31, v1, v30

    const/16 v30, -0x37

    aput v30, v1, v29

    const/16 v29, 0x2855

    aput v29, v1, v28

    const/16 v28, 0x4728

    aput v28, v1, v27

    const/16 v27, -0x20b9

    aput v27, v1, v26

    const/16 v26, -0x21

    aput v26, v1, v25

    const/16 v25, -0x3b88

    aput v25, v1, v24

    const/16 v24, -0x3c

    aput v24, v1, v23

    const/16 v23, -0x27

    aput v23, v1, v22

    const/16 v22, -0x39ba

    aput v22, v1, v21

    const/16 v21, -0x3a

    aput v21, v1, v20

    const/16 v20, 0x3e16

    aput v20, v1, v19

    const/16 v19, -0x16c2

    aput v19, v1, v18

    const/16 v18, -0x17

    aput v18, v1, v17

    const/16 v17, -0xb

    aput v17, v1, v16

    const/16 v16, -0x5beb

    aput v16, v1, v15

    const/16 v15, -0x5c

    aput v15, v1, v14

    const/16 v14, -0x37a4

    aput v14, v1, v13

    const/16 v13, -0x38

    aput v13, v1, v12

    const/16 v12, -0x19

    aput v12, v1, v11

    const/4 v11, -0x5

    aput v11, v1, v10

    const/16 v10, -0x25

    aput v10, v1, v9

    const/16 v9, -0x49ea

    aput v9, v1, v8

    const/16 v8, -0x4a

    aput v8, v1, v7

    const/16 v7, -0x5d98

    aput v7, v1, v6

    const/16 v6, -0x5e

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_40
    array-length v6, v1

    if-lt v3, v6, :cond_72

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_41
    array-length v6, v1

    if-lt v3, v6, :cond_73

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0xe

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, -0x50

    aput v20, v2, v19

    const/16 v19, -0x7a

    aput v19, v2, v18

    const/16 v18, -0x288b

    aput v18, v2, v17

    const/16 v17, -0x42

    aput v17, v2, v16

    const/16 v16, -0x3a

    aput v16, v2, v15

    const/16 v15, 0x1b2d

    aput v15, v2, v14

    const/16 v14, -0x6881

    aput v14, v2, v13

    const/16 v13, -0x49

    aput v13, v2, v12

    const/16 v12, 0x6e5d

    aput v12, v2, v11

    const/16 v11, 0xa0b

    aput v11, v2, v10

    const/16 v10, -0x1e88

    aput v10, v2, v9

    const/16 v9, -0x78

    aput v9, v2, v8

    const/16 v8, 0x5069

    aput v8, v2, v3

    const/16 v3, -0x7700

    aput v3, v2, v1

    const/16 v1, 0xe

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v11, 0x4

    const/4 v12, 0x5

    const/4 v13, 0x6

    const/4 v14, 0x7

    const/16 v15, 0x8

    const/16 v16, 0x9

    const/16 v17, 0xa

    const/16 v18, 0xb

    const/16 v19, 0xc

    const/16 v20, 0xd

    const/16 v21, -0x73

    aput v21, v1, v20

    const/16 v20, -0x1d

    aput v20, v1, v19

    const/16 v19, -0x28ea

    aput v19, v1, v18

    const/16 v18, -0x29

    aput v18, v1, v17

    const/16 v17, -0x50

    aput v17, v1, v16

    const/16 v16, 0x1b48

    aput v16, v1, v15

    const/16 v15, -0x68e5

    aput v15, v1, v14

    const/16 v14, -0x69

    aput v14, v1, v13

    const/16 v13, 0x6e39

    aput v13, v1, v12

    const/16 v12, 0xa6e

    aput v12, v1, v11

    const/16 v11, -0x1ef6

    aput v11, v1, v10

    const/16 v10, -0x1f

    aput v10, v1, v9

    const/16 v9, 0x5008

    aput v9, v1, v8

    const/16 v8, -0x76b0

    aput v8, v1, v3

    const/4 v3, 0x0

    :goto_42
    array-length v8, v1

    if-lt v3, v8, :cond_74

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_43
    array-length v8, v1

    if-lt v3, v8, :cond_75

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_78

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x7c

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x4b

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_44
    array-length v5, v1

    if-lt v3, v5, :cond_76

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_45
    array-length v5, v1

    if-lt v3, v5, :cond_77

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_72
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_40

    :cond_73
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_41

    :cond_74
    aget v8, v1, v3

    aget v9, v2, v3

    xor-int/2addr v8, v9

    aput v8, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_42

    :cond_75
    aget v8, v2, v3

    int-to-char v8, v8

    aput-char v8, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_43

    :cond_76
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_44

    :cond_77
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_45

    :cond_78
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_79

    const-wide v7, 0x3a6b124487cb929eL    # 2.7335086244674476E-27

    xor-long/2addr v1, v7

    :cond_79
    const/16 v7, 0x20

    shr-long/2addr v1, v7

    long-to-int v1, v1

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v1, 0x9

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x5f2f

    aput v15, v2, v14

    const/16 v14, -0x27c5

    aput v14, v2, v13

    const/16 v13, -0x43

    aput v13, v2, v12

    const/16 v12, -0x43

    aput v12, v2, v11

    const/16 v11, -0x5ac2

    aput v11, v2, v10

    const/16 v10, -0x35

    aput v10, v2, v9

    const/16 v9, -0x49

    aput v9, v2, v8

    const/4 v8, -0x4

    aput v8, v2, v3

    const/16 v3, -0x4dde

    aput v3, v2, v1

    const/16 v1, 0x9

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v11, 0x4

    const/4 v12, 0x5

    const/4 v13, 0x6

    const/4 v14, 0x7

    const/16 v15, 0x8

    const/16 v16, 0x5f01

    aput v16, v1, v15

    const/16 v15, -0x27a1

    aput v15, v1, v14

    const/16 v14, -0x28

    aput v14, v1, v13

    const/16 v13, -0x31

    aput v13, v1, v12

    const/16 v12, -0x5aaf

    aput v12, v1, v11

    const/16 v11, -0x5b

    aput v11, v1, v10

    const/16 v10, -0x30

    aput v10, v1, v9

    const/16 v9, -0x6b

    aput v9, v1, v8

    const/16 v8, -0x4dfe

    aput v8, v1, v3

    const/4 v3, 0x0

    :goto_46
    array-length v8, v1

    if-lt v3, v8, :cond_7a

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_47
    array-length v8, v1

    if-lt v3, v8, :cond_7b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_13

    :cond_7a
    aget v8, v1, v3

    aget v9, v2, v3

    xor-int/2addr v8, v9

    aput v8, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_46

    :cond_7b
    aget v8, v2, v3

    int-to-char v8, v8

    aput-char v8, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_47

    :cond_7c
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v6, v1, v6

    if-eqz v6, :cond_7d

    const-wide v6, 0x3a6b124487cb929eL    # 2.7335086244674476E-27

    xor-long/2addr v1, v6

    :cond_7d
    const/16 v6, 0x20

    shr-long/2addr v1, v6

    long-to-int v1, v1

    add-int/lit8 v1, v1, 0x1

    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget-wide v6, v4, v2

    long-to-int v2, v6

    if-lt v3, v2, :cond_7e

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_7e
    const/4 v3, 0x0

    int-to-long v1, v1

    const/16 v6, 0x20

    shl-long v6, v1, v6

    aget-wide v1, v4, v3

    const-wide/16 v8, 0x0

    cmp-long v8, v1, v8

    if-eqz v8, :cond_7f

    const-wide v8, 0x3a6b124487cb929eL    # 2.7335086244674476E-27

    xor-long/2addr v1, v8

    :cond_7f
    const/16 v8, 0x20

    shl-long/2addr v1, v8

    const/16 v8, 0x20

    ushr-long/2addr v1, v8

    xor-long/2addr v1, v6

    const-wide v6, 0x3a6b124487cb929eL    # 2.7335086244674476E-27

    xor-long/2addr v1, v6

    aput-wide v1, v4, v3

    goto/16 :goto_4

    :catch_0
    move-exception v1

    const/16 v1, 0x1a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, -0x78

    aput v29, v2, v28

    const/16 v28, -0x3e

    aput v28, v2, v27

    const/16 v27, -0x72

    aput v27, v2, v26

    const/16 v26, -0x29e6

    aput v26, v2, v25

    const/16 v25, -0x47

    aput v25, v2, v24

    const/16 v24, -0x4b

    aput v24, v2, v23

    const/16 v23, -0x71fe

    aput v23, v2, v22

    const/16 v22, -0x20

    aput v22, v2, v21

    const/16 v21, 0xb62

    aput v21, v2, v20

    const/16 v20, -0x66b8

    aput v20, v2, v19

    const/16 v19, -0x13

    aput v19, v2, v18

    const/16 v18, -0x4a

    aput v18, v2, v17

    const/16 v17, -0x18

    aput v17, v2, v16

    const/16 v16, -0x64

    aput v16, v2, v15

    const/16 v15, -0x15

    aput v15, v2, v14

    const/16 v14, -0x63

    aput v14, v2, v13

    const/16 v13, -0x548a

    aput v13, v2, v12

    const/16 v12, -0x13

    aput v12, v2, v11

    const/16 v11, 0x7d0e

    aput v11, v2, v10

    const/16 v10, -0x21ed

    aput v10, v2, v9

    const/16 v9, -0x49

    aput v9, v2, v8

    const/16 v8, -0x51

    aput v8, v2, v7

    const/16 v7, -0x11

    aput v7, v2, v6

    const/16 v6, -0x63

    aput v6, v2, v4

    const/16 v4, -0x4a

    aput v4, v2, v3

    const/16 v3, -0x63

    aput v3, v2, v1

    const/16 v1, 0x1a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, -0x6

    aput v30, v1, v29

    const/16 v29, -0x59

    aput v29, v1, v28

    const/16 v28, -0x1e

    aput v28, v1, v27

    const/16 v27, -0x298a

    aput v27, v1, v26

    const/16 v26, -0x2a

    aput v26, v1, v25

    const/16 v25, -0x39

    aput v25, v1, v24

    const/16 v24, -0x718a

    aput v24, v1, v23

    const/16 v23, -0x72

    aput v23, v1, v22

    const/16 v22, 0xb0d

    aput v22, v1, v21

    const/16 v21, -0x66f5

    aput v21, v1, v20

    const/16 v20, -0x67

    aput v20, v1, v19

    const/16 v19, -0x28

    aput v19, v1, v18

    const/16 v18, -0x73

    aput v18, v1, v17

    const/16 v17, -0xf

    aput v17, v1, v16

    const/16 v16, -0x74

    aput v16, v1, v15

    const/4 v15, -0x4

    aput v15, v1, v14

    const/16 v14, -0x54fc

    aput v14, v1, v13

    const/16 v13, -0x55

    aput v13, v1, v12

    const/16 v12, 0x7d69

    aput v12, v1, v11

    const/16 v11, -0x2183

    aput v11, v1, v10

    const/16 v10, -0x22

    aput v10, v1, v9

    const/16 v9, -0x3f

    aput v9, v1, v8

    const/16 v8, -0x7f

    aput v8, v1, v7

    const/4 v7, -0x4

    aput v7, v1, v6

    const/16 v6, -0x2b

    aput v6, v1, v4

    const/16 v4, -0x32

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_48
    array-length v4, v1

    if-lt v3, v4, :cond_82

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_49
    array-length v4, v1

    if-lt v3, v4, :cond_83

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x1d

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, -0x13b6

    aput v33, v2, v32

    const/16 v32, -0x3e

    aput v32, v2, v31

    const/16 v31, -0x5e

    aput v31, v2, v30

    const/16 v30, -0x1d

    aput v30, v2, v29

    const/16 v29, 0x3408

    aput v29, v2, v28

    const/16 v28, -0xda6

    aput v28, v2, v27

    const/16 v27, -0x2e

    aput v27, v2, v26

    const/16 v26, 0x5f69

    aput v26, v2, v25

    const/16 v25, 0xf36

    aput v25, v2, v24

    const/16 v24, -0x39d1

    aput v24, v2, v23

    const/16 v23, -0x4b

    aput v23, v2, v22

    const/16 v22, -0x2491

    aput v22, v2, v21

    const/16 v21, -0x48

    aput v21, v2, v20

    const/16 v20, -0x68

    aput v20, v2, v19

    const/16 v19, 0x5d27

    aput v19, v2, v18

    const/16 v18, 0x3038

    aput v18, v2, v17

    const/16 v17, 0x3f74

    aput v17, v2, v16

    const/16 v16, 0x686b

    aput v16, v2, v15

    const/16 v15, -0x4ad6

    aput v15, v2, v14

    const/16 v14, -0x2f

    aput v14, v2, v13

    const/16 v13, -0x5f

    aput v13, v2, v12

    const/16 v12, -0x2cb0

    aput v12, v2, v11

    const/16 v11, -0x46

    aput v11, v2, v10

    const/4 v10, -0x1

    aput v10, v2, v9

    const/16 v9, -0x69

    aput v9, v2, v8

    const/16 v8, -0x6bee

    aput v8, v2, v7

    const/16 v7, -0x19

    aput v7, v2, v6

    const/16 v6, -0x6f8c

    aput v6, v2, v3

    const/4 v3, -0x4

    aput v3, v2, v1

    const/16 v1, 0x1d

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, 0x1a

    const/16 v32, 0x1b

    const/16 v33, 0x1c

    const/16 v34, -0x1396

    aput v34, v1, v33

    const/16 v33, -0x14

    aput v33, v1, v32

    const/16 v32, -0x32

    aput v32, v1, v31

    const/16 v31, -0x71

    aput v31, v1, v30

    const/16 v30, 0x347d

    aput v30, v1, v29

    const/16 v29, -0xdcc

    aput v29, v1, v28

    const/16 v28, -0xe

    aput v28, v1, v27

    const/16 v27, 0x5f1a

    aput v27, v1, v26

    const/16 v26, 0xf5f

    aput v26, v1, v25

    const/16 v25, -0x39f1

    aput v25, v1, v24

    const/16 v24, -0x3a

    aput v24, v1, v23

    const/16 v23, -0x24f6

    aput v23, v1, v22

    const/16 v22, -0x25

    aput v22, v1, v21

    const/16 v21, -0xf

    aput v21, v1, v20

    const/16 v20, 0x5d51

    aput v20, v1, v19

    const/16 v19, 0x305d

    aput v19, v1, v18

    const/16 v18, 0x3f30

    aput v18, v1, v17

    const/16 v17, 0x683f

    aput v17, v1, v16

    const/16 v16, -0x4a98

    aput v16, v1, v15

    const/16 v15, -0x4b

    aput v15, v1, v14

    const/16 v14, -0x3c

    aput v14, v1, v13

    const/16 v13, -0x2cde

    aput v13, v1, v12

    const/16 v12, -0x2d

    aput v12, v1, v11

    const/16 v11, -0x62

    aput v11, v1, v10

    const/16 v10, -0x39

    aput v10, v1, v9

    const/16 v9, -0x6b9a

    aput v9, v1, v8

    const/16 v8, -0x6c

    aput v8, v1, v7

    const/16 v7, -0x6fe3

    aput v7, v1, v6

    const/16 v6, -0x70

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_4a
    array-length v6, v1

    if-lt v3, v6, :cond_84

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_4b
    array-length v6, v1

    if-lt v3, v6, :cond_85

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_80
    if-nez p2, :cond_81

    :try_start_1
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    const/16 v1, 0x2728

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_81

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    const/4 v2, 0x7

    const/16 v3, 0x2728

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->getConnectedDeviceList(III)Ljava/util/List;

    move-result-object v1

    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_81

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_81
    :goto_4c
    return-object v5

    :cond_82
    aget v4, v1, v3

    aget v6, v2, v3

    xor-int/2addr v4, v6

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_48

    :cond_83
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_49

    :cond_84
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4a

    :cond_85
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4b

    :catch_1
    move-exception v1

    goto :goto_4c

    :catch_2
    move-exception v1

    goto :goto_4c
.end method

.method private getPairedDevicesFromDbWhichAreAlsoInConnectedState(ILjava/util/List;Ljava/util/ArrayList;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;",
            ">;)",
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x2

    new-array v3, v0, [J

    const/4 v0, 0x1

    const-wide/16 v1, 0x1

    aput-wide v1, v3, v0

    const/4 v0, 0x0

    array-length v1, v3

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v3, v1

    long-to-int v1, v1

    if-gtz v1, :cond_0

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/4 v2, 0x0

    int-to-long v0, p1

    const/16 v4, 0x20

    shl-long/2addr v0, v4

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    aget-wide v0, v3, v2

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-eqz v6, :cond_1

    const-wide v6, -0x41089c9581a54c63L    # -2.2304862702316267E-5

    xor-long/2addr v0, v6

    :cond_1
    const/16 v6, 0x20

    ushr-long/2addr v0, v6

    const/16 v6, 0x20

    shl-long/2addr v0, v6

    xor-long/2addr v0, v4

    const-wide v4, -0x41089c9581a54c63L    # -2.2304862702316267E-5

    xor-long/2addr v0, v4

    aput-wide v0, v3, v2

    invoke-direct {p0, p3}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->createListwithConnectionStatus(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v0

    new-instance v4, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v4}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ltz v0, :cond_c

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mContext:Landroid/content/Context;

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/utils/ScanningFragmentHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/utils/ScanningFragmentHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/utils/ScanningFragmentHelper;->getPairedDevicesFromDB(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_3

    array-length v1, v3

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v3, v1

    long-to-int v1, v1

    if-gtz v1, :cond_5

    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/16 v2, -0x73

    aput v2, v1, v0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/16 v4, -0x43

    aput v4, v0, v2

    const/4 v2, 0x0

    :goto_1
    array-length v4, v0

    if-lt v2, v4, :cond_3

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_2
    array-length v4, v0

    if-lt v2, v4, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_3
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_5
    const/4 v1, 0x0

    aget-wide v1, v3, v1

    const-wide/16 v6, 0x0

    cmp-long v6, v1, v6

    if-eqz v6, :cond_6

    const-wide v6, -0x41089c9581a54c63L    # -2.2304862702316267E-5

    xor-long/2addr v1, v6

    :cond_6
    const/16 v6, 0x20

    shl-long/2addr v1, v6

    const/16 v6, 0x20

    shr-long/2addr v1, v6

    long-to-int v1, v1

    if-eqz v1, :cond_b

    array-length v1, v3

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v3, v1

    long-to-int v1, v1

    if-gtz v1, :cond_9

    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/16 v2, -0x35

    aput v2, v1, v0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v4, -0x5

    aput v4, v0, v2

    const/4 v2, 0x0

    :goto_3
    array-length v4, v0

    if-lt v2, v4, :cond_7

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_4
    array-length v4, v0

    if-lt v2, v4, :cond_8

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_7
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_8
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_9
    const/4 v1, 0x0

    aget-wide v1, v3, v1

    const-wide/16 v6, 0x0

    cmp-long v6, v1, v6

    if-eqz v6, :cond_a

    const-wide v6, -0x41089c9581a54c63L    # -2.2304862702316267E-5

    xor-long/2addr v1, v6

    :cond_a
    const/16 v6, 0x20

    shl-long/2addr v1, v6

    const/16 v6, 0x20

    shr-long/2addr v1, v6

    long-to-int v1, v1

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceDataType()I

    move-result v2

    if-ne v1, v2, :cond_2

    :cond_b
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->getConnectionStatus(Ljava/lang/String;Ljava/util/List;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->setIsConnected(Z)V

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :catch_0
    move-exception v0

    :cond_c
    :goto_5
    return-object v4

    :catch_1
    move-exception v0

    goto :goto_5

    :catch_2
    move-exception v0

    goto :goto_5

    :catch_3
    move-exception v0

    goto/16 :goto_0
.end method

.method private initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V
    .locals 3

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$1;

    invoke-direct {v2, p0, p2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    return-void
.end method

.method public static pI3HJI9QuL()Ljava/lang/String;
    .locals 60

    const/16 v0, 0x39

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, 0x17

    const/16 v25, 0x18

    const/16 v26, 0x19

    const/16 v27, 0x1a

    const/16 v28, 0x1b

    const/16 v29, 0x1c

    const/16 v30, 0x1d

    const/16 v31, 0x1e

    const/16 v32, 0x1f

    const/16 v33, 0x20

    const/16 v34, 0x21

    const/16 v35, 0x22

    const/16 v36, 0x23

    const/16 v37, 0x24

    const/16 v38, 0x25

    const/16 v39, 0x26

    const/16 v40, 0x27

    const/16 v41, 0x28

    const/16 v42, 0x29

    const/16 v43, 0x2a

    const/16 v44, 0x2b

    const/16 v45, 0x2c

    const/16 v46, 0x2d

    const/16 v47, 0x2e

    const/16 v48, 0x2f

    const/16 v49, 0x30

    const/16 v50, 0x31

    const/16 v51, 0x32

    const/16 v52, 0x33

    const/16 v53, 0x34

    const/16 v54, 0x35

    const/16 v55, 0x36

    const/16 v56, 0x37

    const/16 v57, 0x38

    const/16 v58, 0x1a71

    aput v58, v1, v57

    const/16 v57, 0x6a77

    aput v57, v1, v56

    const/16 v56, 0x6f0b

    aput v56, v1, v55

    const/16 v55, 0x7d01

    aput v55, v1, v54

    const/16 v54, -0x30e8

    aput v54, v1, v53

    const/16 v53, -0x63

    aput v53, v1, v52

    const/16 v52, -0x55f9

    aput v52, v1, v51

    const/16 v51, -0x28

    aput v51, v1, v50

    const/16 v50, 0x3058

    aput v50, v1, v49

    const/16 v49, 0x6556

    aput v49, v1, v48

    const/16 v48, -0x21bb

    aput v48, v1, v47

    const/16 v47, -0x45

    aput v47, v1, v46

    const/16 v46, -0x1a

    aput v46, v1, v45

    const/16 v45, -0x419c

    aput v45, v1, v44

    const/16 v44, -0x38

    aput v44, v1, v43

    const/16 v43, -0x32

    aput v43, v1, v42

    const/16 v42, -0x3a

    aput v42, v1, v41

    const/16 v41, 0x6d63

    aput v41, v1, v40

    const/16 v40, -0x70f7

    aput v40, v1, v39

    const/16 v39, -0x16

    aput v39, v1, v38

    const/16 v38, 0x372f

    aput v38, v1, v37

    const/16 v37, -0x12a2

    aput v37, v1, v36

    const/16 v36, -0x74

    aput v36, v1, v35

    const/16 v35, 0x7b44

    aput v35, v1, v34

    const/16 v34, -0x28a5

    aput v34, v1, v33

    const/16 v33, -0x5d

    aput v33, v1, v32

    const/16 v32, -0x35b0

    aput v32, v1, v31

    const/16 v31, -0x53

    aput v31, v1, v30

    const/16 v30, -0x22

    aput v30, v1, v29

    const/16 v29, -0x62dc

    aput v29, v1, v28

    const/16 v28, -0x46

    aput v28, v1, v27

    const/16 v27, -0x3b

    aput v27, v1, v26

    const/16 v26, -0x4d

    aput v26, v1, v25

    const/16 v25, -0x49

    aput v25, v1, v24

    const/16 v24, -0x68

    aput v24, v1, v23

    const/16 v23, 0x5b6a

    aput v23, v1, v22

    const/16 v22, 0x7c37

    aput v22, v1, v21

    const/16 v21, -0xef0

    aput v21, v1, v20

    const/16 v20, -0x7c

    aput v20, v1, v19

    const/16 v19, -0x4b

    aput v19, v1, v18

    const/16 v18, 0x654f

    aput v18, v1, v17

    const/16 v17, -0x4bea

    aput v17, v1, v16

    const/16 v16, -0x23

    aput v16, v1, v15

    const/16 v15, -0xf

    aput v15, v1, v14

    const/16 v14, -0x55

    aput v14, v1, v13

    const/16 v13, -0x73

    aput v13, v1, v12

    const/16 v12, -0xd

    aput v12, v1, v11

    const/16 v11, -0x80

    aput v11, v1, v10

    const/16 v10, -0x17

    aput v10, v1, v9

    const/16 v9, 0x1356

    aput v9, v1, v8

    const/16 v8, 0x297b

    aput v8, v1, v7

    const/16 v7, -0x7ba3

    aput v7, v1, v6

    const/16 v6, -0x18

    aput v6, v1, v5

    const/16 v5, -0xa

    aput v5, v1, v4

    const/16 v4, -0x2e

    aput v4, v1, v3

    const/16 v3, -0xb

    aput v3, v1, v2

    const/16 v2, 0x3e5c

    aput v2, v1, v0

    const/16 v0, 0x39

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, 0x1e

    const/16 v33, 0x1f

    const/16 v34, 0x20

    const/16 v35, 0x21

    const/16 v36, 0x22

    const/16 v37, 0x23

    const/16 v38, 0x24

    const/16 v39, 0x25

    const/16 v40, 0x26

    const/16 v41, 0x27

    const/16 v42, 0x28

    const/16 v43, 0x29

    const/16 v44, 0x2a

    const/16 v45, 0x2b

    const/16 v46, 0x2c

    const/16 v47, 0x2d

    const/16 v48, 0x2e

    const/16 v49, 0x2f

    const/16 v50, 0x30

    const/16 v51, 0x31

    const/16 v52, 0x32

    const/16 v53, 0x33

    const/16 v54, 0x34

    const/16 v55, 0x35

    const/16 v56, 0x36

    const/16 v57, 0x37

    const/16 v58, 0x38

    const/16 v59, 0x1a14

    aput v59, v0, v58

    const/16 v58, 0x6a1a

    aput v58, v0, v57

    const/16 v57, 0x6f6a

    aput v57, v0, v56

    const/16 v56, 0x7d6f

    aput v56, v0, v55

    const/16 v55, -0x3083

    aput v55, v0, v54

    const/16 v54, -0x31

    aput v54, v0, v53

    const/16 v53, -0x55d9

    aput v53, v0, v52

    const/16 v52, -0x56

    aput v52, v0, v51

    const/16 v51, 0x3037

    aput v51, v0, v50

    const/16 v50, 0x6530

    aput v50, v0, v49

    const/16 v49, -0x219b

    aput v49, v0, v48

    const/16 v48, -0x22

    aput v48, v0, v47

    const/16 v47, -0x7b

    aput v47, v0, v46

    const/16 v46, -0x41f3

    aput v46, v0, v45

    const/16 v45, -0x42

    aput v45, v0, v44

    const/16 v44, -0x55

    aput v44, v0, v43

    const/16 v43, -0x5e

    aput v43, v0, v42

    const/16 v42, 0x6d43

    aput v42, v0, v41

    const/16 v41, -0x7093

    aput v41, v0, v40

    const/16 v40, -0x71

    aput v40, v0, v39

    const/16 v39, 0x375d

    aput v39, v0, v38

    const/16 v38, -0x12c9

    aput v38, v0, v37

    const/16 v37, -0x13

    aput v37, v0, v36

    const/16 v36, 0x7b34

    aput v36, v0, v35

    const/16 v35, -0x2885

    aput v35, v0, v34

    const/16 v34, -0x29

    aput v34, v0, v33

    const/16 v33, -0x35cb

    aput v33, v0, v32

    const/16 v32, -0x36

    aput v32, v0, v31

    const/16 v31, -0x2

    aput v31, v0, v30

    const/16 v30, -0x62b0

    aput v30, v0, v29

    const/16 v29, -0x63

    aput v29, v0, v28

    const/16 v28, -0x55

    aput v28, v0, v27

    const/16 v27, -0x2e

    aput v27, v0, v26

    const/16 v26, -0x2c

    aput v26, v0, v25

    const/16 v25, -0x48

    aput v25, v0, v24

    const/16 v24, 0x5b46

    aput v24, v0, v23

    const/16 v23, 0x7c5b

    aput v23, v0, v22

    const/16 v22, -0xe84

    aput v22, v0, v21

    const/16 v21, -0xf

    aput v21, v0, v20

    const/16 v20, -0x25

    aput v20, v0, v19

    const/16 v19, 0x656f

    aput v19, v0, v18

    const/16 v18, -0x4b9b

    aput v18, v0, v17

    const/16 v17, -0x4c

    aput v17, v0, v16

    const/16 v16, -0x2f

    aput v16, v0, v15

    const/16 v15, -0x27

    aput v15, v0, v14

    const/16 v14, -0x1e

    aput v14, v0, v13

    const/16 v13, -0x80

    aput v13, v0, v12

    const/16 v12, -0x12

    aput v12, v0, v11

    const/16 v11, -0x74

    aput v11, v0, v10

    const/16 v10, 0x1305

    aput v10, v0, v9

    const/16 v9, 0x2913

    aput v9, v0, v8

    const/16 v8, -0x7bd7

    aput v8, v0, v7

    const/16 v7, -0x7c

    aput v7, v0, v6

    const/16 v6, -0x69

    aput v6, v0, v5

    const/16 v5, -0x49

    aput v5, v0, v4

    const/16 v4, -0x43

    aput v4, v0, v3

    const/16 v3, 0x3e31

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method


# virtual methods
.method public clearController()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public joinWithDevice(Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentEventListener;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Ljava/lang/IllegalStateException;,
            Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperEventListener;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperEventListener;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->join(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public leaveDevice(Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;Z)V
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_4

    if-eqz p2, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceConnectivityType()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    new-instance v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;

    invoke-direct {v4}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;-><init>()V

    const/16 v1, 0x11

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, -0x1193

    aput v20, v2, v19

    const/16 v19, -0x53

    aput v19, v2, v18

    const/16 v18, 0x2f7f

    aput v18, v2, v17

    const/16 v17, 0x279

    aput v17, v2, v16

    const/16 v16, 0x5d47

    aput v16, v2, v15

    const/16 v15, 0x2a19

    aput v15, v2, v14

    const/16 v14, -0x438b

    aput v14, v2, v13

    const/16 v13, -0x12

    aput v13, v2, v12

    const/16 v12, 0x4a72

    aput v12, v2, v11

    const/16 v11, 0x260b

    aput v11, v2, v10

    const/16 v10, -0x108a

    aput v10, v2, v9

    const/16 v9, -0x5f

    aput v9, v2, v8

    const/16 v8, 0x2a19

    aput v8, v2, v7

    const/16 v7, -0x658b

    aput v7, v2, v6

    const/16 v6, -0x22

    aput v6, v2, v5

    const/16 v5, 0x4851

    aput v5, v2, v3

    const/16 v3, 0x280b

    aput v3, v2, v1

    const/16 v1, 0x11

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, -0x11d8

    aput v21, v1, v20

    const/16 v20, -0x12

    aput v20, v1, v19

    const/16 v19, 0x2f36

    aput v19, v1, v18

    const/16 v18, 0x22f

    aput v18, v1, v17

    const/16 v17, 0x5d02

    aput v17, v1, v16

    const/16 v16, 0x2a5d

    aput v16, v1, v15

    const/16 v15, -0x43d6

    aput v15, v1, v14

    const/16 v14, -0x44

    aput v14, v1, v13

    const/16 v13, 0x4a3b

    aput v13, v1, v12

    const/16 v12, 0x264a

    aput v12, v1, v11

    const/16 v11, -0x10da

    aput v11, v1, v10

    const/16 v10, -0x11

    aput v10, v1, v9

    const/16 v9, 0x2a4c

    aput v9, v1, v8

    const/16 v8, -0x65d6

    aput v8, v1, v7

    const/16 v7, -0x66

    aput v7, v1, v6

    const/16 v6, 0x481c

    aput v6, v1, v5

    const/16 v5, 0x2848

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;->setCommandId(Ljava/lang/String;)V

    :try_start_1
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v1

    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_2 .. :try_end_2} :catch_3

    :try_start_3
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->request(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;)V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_3 .. :try_end_3} :catch_3

    :goto_2
    return-void

    :cond_0
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;->printStackTrace()V

    goto :goto_2

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;->printStackTrace()V

    goto :goto_2

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_2

    :catch_4
    move-exception v1

    :cond_2
    :try_start_4
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_6

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->isFromPairedDB()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    :try_start_5
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_5

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->leave()V

    goto :goto_2

    :catch_5
    move-exception v1

    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->setIsPaired(Z)V

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/utils/ScanningFragmentHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/utils/ScanningFragmentHelper;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/framework/ui/utils/ScanningFragmentHelper;->updatePairedStatusInDB(Landroid/content/Context;Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)I

    goto :goto_2

    :catch_6
    move-exception v1

    :cond_3
    :try_start_6
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_6
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_8

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    :try_start_7
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_7
    .catch Ljava/lang/NullPointerException; {:try_start_7 .. :try_end_7} :catch_9

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->leave()V

    goto :goto_2

    :catch_7
    move-exception v1

    goto :goto_2

    :catch_8
    move-exception v1

    goto :goto_2

    :catch_9
    move-exception v1

    goto :goto_2
.end method

.method public renameDevice(Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_6

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_5

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v1

    if-ne v1, v0, :cond_1

    :try_start_2
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->rename(Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceWrongStatusException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_4

    :goto_0
    return v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceWrongStatusException;->printStackTrace()V

    goto :goto_1

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_1

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1

    :catch_5
    move-exception v1

    :cond_1
    invoke-virtual {p1, p2}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->setDeviceName(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/utils/ScanningFragmentHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/utils/ScanningFragmentHelper;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, p1}, Lcom/sec/android/app/shealth/framework/ui/utils/ScanningFragmentHelper;->renamePairedDeviceInDB(Landroid/content/Context;Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)I

    goto :goto_0

    :catch_6
    move-exception v0

    goto :goto_1
.end method

.method public requestPairedDeviceForRename(IIILjava/lang/String;)Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;
    .locals 43

    const/4 v1, 0x3

    new-array v4, v1, [J

    const/4 v1, 0x2

    const-wide/16 v2, 0x4

    aput-wide v2, v4, v1

    const/4 v1, 0x0

    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v4, v2

    long-to-int v2, v2

    if-gtz v2, :cond_0

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v3, 0x0

    move/from16 v0, p1

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    ushr-long v5, v1, v5

    aget-wide v1, v4, v3

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_1

    const-wide v7, -0x5352aa35520a005eL    # -1.7579207154914662E-93

    xor-long/2addr v1, v7

    :cond_1
    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    const/16 v7, 0x20

    shl-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, -0x5352aa35520a005eL    # -1.7579207154914662E-93

    xor-long/2addr v1, v5

    aput-wide v1, v4, v3

    const/4 v1, 0x1

    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v4, v2

    long-to-int v2, v2

    if-lt v1, v2, :cond_2

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    const/4 v3, 0x0

    move/from16 v0, p2

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long v5, v1, v5

    aget-wide v1, v4, v3

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_3

    const-wide v7, -0x5352aa35520a005eL    # -1.7579207154914662E-93

    xor-long/2addr v1, v7

    :cond_3
    const/16 v7, 0x20

    shl-long/2addr v1, v7

    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, -0x5352aa35520a005eL    # -1.7579207154914662E-93

    xor-long/2addr v1, v5

    aput-wide v1, v4, v3

    const/4 v1, 0x2

    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v4, v2

    long-to-int v2, v2

    if-lt v1, v2, :cond_4

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_4
    const/4 v3, 0x1

    move/from16 v0, p3

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    ushr-long v5, v1, v5

    aget-wide v1, v4, v3

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_5

    const-wide v7, -0x5352aa35520a005eL    # -1.7579207154914662E-93

    xor-long/2addr v1, v7

    :cond_5
    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    const/16 v7, 0x20

    shl-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, -0x5352aa35520a005eL    # -1.7579207154914662E-93

    xor-long/2addr v1, v5

    aput-wide v1, v4, v3

    const/16 v1, 0x1a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, -0x6e

    aput v29, v2, v28

    const/16 v28, -0xc

    aput v28, v2, v27

    const/16 v27, -0xf

    aput v27, v2, v26

    const/16 v26, 0x6d2e

    aput v26, v2, v25

    const/16 v25, 0x2

    aput v25, v2, v24

    const/16 v24, 0x3272

    aput v24, v2, v23

    const/16 v23, -0x55ba

    aput v23, v2, v22

    const/16 v22, -0x3c

    aput v22, v2, v21

    const/16 v21, 0x2846

    aput v21, v2, v20

    const/16 v20, 0x16b

    aput v20, v2, v19

    const/16 v19, -0x158b

    aput v19, v2, v18

    const/16 v18, -0x7c

    aput v18, v2, v17

    const/16 v17, 0x161

    aput v17, v2, v16

    const/16 v16, 0x476c

    aput v16, v2, v15

    const/16 v15, -0x33e0

    aput v15, v2, v14

    const/16 v14, -0x53

    aput v14, v2, v13

    const/16 v13, 0x7f3b

    aput v13, v2, v12

    const/16 v12, 0x3939

    aput v12, v2, v11

    const/16 v11, -0xba2

    aput v11, v2, v10

    const/16 v10, -0x66

    aput v10, v2, v9

    const/16 v9, 0x3174

    aput v9, v2, v8

    const/16 v8, -0x45a1

    aput v8, v2, v7

    const/16 v7, -0x2c

    aput v7, v2, v6

    const/16 v6, 0x1242

    aput v6, v2, v5

    const/16 v5, -0x448f

    aput v5, v2, v3

    const/16 v3, -0x18

    aput v3, v2, v1

    const/16 v1, 0x1a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, -0x20

    aput v30, v1, v29

    const/16 v29, -0x6f

    aput v29, v1, v28

    const/16 v28, -0x63

    aput v28, v1, v27

    const/16 v27, 0x6d42

    aput v27, v1, v26

    const/16 v26, 0x6d

    aput v26, v1, v25

    const/16 v25, 0x3200

    aput v25, v1, v24

    const/16 v24, -0x55ce

    aput v24, v1, v23

    const/16 v23, -0x56

    aput v23, v1, v22

    const/16 v22, 0x2829

    aput v22, v1, v21

    const/16 v21, 0x128

    aput v21, v1, v20

    const/16 v20, -0x15ff

    aput v20, v1, v19

    const/16 v19, -0x16

    aput v19, v1, v18

    const/16 v18, 0x104

    aput v18, v1, v17

    const/16 v17, 0x4701

    aput v17, v1, v16

    const/16 v16, -0x33b9

    aput v16, v1, v15

    const/16 v15, -0x34

    aput v15, v1, v14

    const/16 v14, 0x7f49

    aput v14, v1, v13

    const/16 v13, 0x397f

    aput v13, v1, v12

    const/16 v12, -0xbc7

    aput v12, v1, v11

    const/16 v11, -0xc

    aput v11, v1, v10

    const/16 v10, 0x311d

    aput v10, v1, v9

    const/16 v9, -0x45cf

    aput v9, v1, v8

    const/16 v8, -0x46

    aput v8, v1, v7

    const/16 v7, 0x1223

    aput v7, v1, v6

    const/16 v6, -0x44ee

    aput v6, v1, v5

    const/16 v5, -0x45

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_7

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    const/16 v1, 0x23

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, -0xcb8

    aput v39, v2, v38

    const/16 v38, -0x6a

    aput v38, v2, v37

    const/16 v37, -0x47

    aput v37, v2, v36

    const/16 v36, -0x22

    aput v36, v2, v35

    const/16 v35, 0x6c33

    aput v35, v2, v34

    const/16 v34, -0x2df1

    aput v34, v2, v33

    const/16 v33, -0xe

    aput v33, v2, v32

    const/16 v32, -0x14

    aput v32, v2, v31

    const/16 v31, 0x134f

    aput v31, v2, v30

    const/16 v30, -0x788e

    aput v30, v2, v29

    const/16 v29, -0x17

    aput v29, v2, v28

    const/16 v28, -0x5987

    aput v28, v2, v27

    const/16 v27, -0xc

    aput v27, v2, v26

    const/16 v26, -0x5c

    aput v26, v2, v25

    const/16 v25, 0x772d

    aput v25, v2, v24

    const/16 v24, 0x5e31

    aput v24, v2, v23

    const/16 v23, 0x263b

    aput v23, v2, v22

    const/16 v22, 0x4045

    aput v22, v2, v21

    const/16 v21, -0x13d7

    aput v21, v2, v20

    const/16 v20, -0x66

    aput v20, v2, v19

    const/16 v19, -0x23

    aput v19, v2, v18

    const/16 v18, -0x22

    aput v18, v2, v17

    const/16 v17, 0x3405

    aput v17, v2, v16

    const/16 v16, 0x4251

    aput v16, v2, v15

    const/16 v15, -0x51d0

    aput v15, v2, v14

    const/16 v14, -0x39

    aput v14, v2, v13

    const/16 v13, -0x26

    aput v13, v2, v12

    const/16 v12, 0x4f20

    aput v12, v2, v11

    const/16 v11, -0x6dc5

    aput v11, v2, v10

    const/16 v10, -0x1f

    aput v10, v2, v9

    const/4 v9, -0x7

    aput v9, v2, v8

    const/16 v8, -0x20

    aput v8, v2, v7

    const/16 v7, 0x3d4a

    aput v7, v2, v6

    const/16 v6, -0x48a8

    aput v6, v2, v3

    const/16 v3, -0x3b

    aput v3, v2, v1

    const/16 v1, 0x23

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, 0x1a

    const/16 v32, 0x1b

    const/16 v33, 0x1c

    const/16 v34, 0x1d

    const/16 v35, 0x1e

    const/16 v36, 0x1f

    const/16 v37, 0x20

    const/16 v38, 0x21

    const/16 v39, 0x22

    const/16 v40, -0xcd4

    aput v40, v1, v39

    const/16 v39, -0xd

    aput v39, v1, v38

    const/16 v38, -0x2b

    aput v38, v1, v37

    const/16 v37, -0x4e

    aput v37, v1, v36

    const/16 v36, 0x6c52

    aput v36, v1, v35

    const/16 v35, -0x2d94

    aput v35, v1, v34

    const/16 v34, -0x2e

    aput v34, v1, v33

    const/16 v33, -0x77

    aput v33, v1, v32

    const/16 v32, 0x1322

    aput v32, v1, v31

    const/16 v31, -0x78ed

    aput v31, v1, v30

    const/16 v30, -0x79

    aput v30, v1, v29

    const/16 v29, -0x59e4

    aput v29, v1, v28

    const/16 v28, -0x5a

    aput v28, v1, v27

    const/16 v27, -0x2a

    aput v27, v1, v26

    const/16 v26, 0x7742

    aput v26, v1, v25

    const/16 v25, 0x5e77

    aput v25, v1, v24

    const/16 v24, 0x265e

    aput v24, v1, v23

    const/16 v23, 0x4026

    aput v23, v1, v22

    const/16 v22, -0x13c0

    aput v22, v1, v21

    const/16 v21, -0x14

    aput v21, v1, v20

    const/16 v20, -0x48

    aput v20, v1, v19

    const/16 v19, -0x66

    aput v19, v1, v18

    const/16 v18, 0x3461

    aput v18, v1, v17

    const/16 v17, 0x4234

    aput v17, v1, v16

    const/16 v16, -0x51be

    aput v16, v1, v15

    const/16 v15, -0x52

    aput v15, v1, v14

    const/16 v14, -0x45

    aput v14, v1, v13

    const/16 v13, 0x4f70

    aput v13, v1, v12

    const/16 v12, -0x6db1

    aput v12, v1, v11

    const/16 v11, -0x6e

    aput v11, v1, v10

    const/16 v10, -0x64

    aput v10, v1, v9

    const/16 v9, -0x6b

    aput v9, v1, v8

    const/16 v8, 0x3d3b

    aput v8, v1, v7

    const/16 v7, -0x48c3

    aput v7, v1, v6

    const/16 v6, -0x49

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v6, v1

    if-lt v3, v6, :cond_9

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v6, v1

    if-lt v3, v6, :cond_a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-gtz v1, :cond_f

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x4d2d

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x4d1d

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_d

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5

    :catch_0
    move-exception v1

    move-object v4, v1

    const/16 v1, 0x1a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, -0x7c

    aput v29, v2, v28

    const/16 v28, -0x3

    aput v28, v2, v27

    const/16 v27, -0x7f

    aput v27, v2, v26

    const/16 v26, -0x6e

    aput v26, v2, v25

    const/16 v25, -0xe

    aput v25, v2, v24

    const/16 v24, -0x16

    aput v24, v2, v23

    const/16 v23, -0x3bbb

    aput v23, v2, v22

    const/16 v22, -0x56

    aput v22, v2, v21

    const/16 v21, -0x1be2

    aput v21, v2, v20

    const/16 v20, -0x59

    aput v20, v2, v19

    const/16 v19, -0x61

    aput v19, v2, v18

    const/16 v18, -0x4d

    aput v18, v2, v17

    const/16 v17, -0x57

    aput v17, v2, v16

    const/16 v16, -0x2e91

    aput v16, v2, v15

    const/16 v15, -0x4a

    aput v15, v2, v14

    const/16 v14, -0x2a

    aput v14, v2, v13

    const/16 v13, -0x70d9

    aput v13, v2, v12

    const/16 v12, -0x37

    aput v12, v2, v11

    const/16 v11, -0x49

    aput v11, v2, v10

    const/16 v10, 0x52f

    aput v10, v2, v9

    const/16 v9, 0x226c

    aput v9, v2, v8

    const/16 v8, 0x624c

    aput v8, v2, v7

    const/16 v7, 0xd0c

    aput v7, v2, v6

    const/16 v6, 0x466c

    aput v6, v2, v5

    const/16 v5, 0x5f25

    aput v5, v2, v3

    const/16 v3, 0x100c

    aput v3, v2, v1

    const/16 v1, 0x1a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, -0xa

    aput v30, v1, v29

    const/16 v29, -0x68

    aput v29, v1, v28

    const/16 v28, -0x13

    aput v28, v1, v27

    const/16 v27, -0x2

    aput v27, v1, v26

    const/16 v26, -0x63

    aput v26, v1, v25

    const/16 v25, -0x68

    aput v25, v1, v24

    const/16 v24, -0x3bcf

    aput v24, v1, v23

    const/16 v23, -0x3c

    aput v23, v1, v22

    const/16 v22, -0x1b8f

    aput v22, v1, v21

    const/16 v21, -0x1c

    aput v21, v1, v20

    const/16 v20, -0x15

    aput v20, v1, v19

    const/16 v19, -0x23

    aput v19, v1, v18

    const/16 v18, -0x34

    aput v18, v1, v17

    const/16 v17, -0x2efe

    aput v17, v1, v16

    const/16 v16, -0x2f

    aput v16, v1, v15

    const/16 v15, -0x49

    aput v15, v1, v14

    const/16 v14, -0x70ab

    aput v14, v1, v13

    const/16 v13, -0x71

    aput v13, v1, v12

    const/16 v12, -0x30

    aput v12, v1, v11

    const/16 v11, 0x541

    aput v11, v1, v10

    const/16 v10, 0x2205

    aput v10, v1, v9

    const/16 v9, 0x6222

    aput v9, v1, v8

    const/16 v8, 0xd62

    aput v8, v1, v7

    const/16 v7, 0x460d

    aput v7, v1, v6

    const/16 v6, 0x5f46

    aput v6, v1, v5

    const/16 v5, 0x105f

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v5, v1

    if-lt v3, v5, :cond_3b

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_3c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v4}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_6
    :goto_8
    const/4 v1, 0x0

    :goto_9
    return-object v1

    :cond_7
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_8
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_9
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_a
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :catch_1
    move-exception v1

    const/16 v1, 0x1a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, -0xae6

    aput v28, v2, v27

    const/16 v27, -0x70

    aput v27, v2, v26

    const/16 v26, -0x39

    aput v26, v2, v25

    const/16 v25, -0x74

    aput v25, v2, v24

    const/16 v24, -0x12

    aput v24, v2, v23

    const/16 v23, -0x1e

    aput v23, v2, v22

    const/16 v22, -0x4a

    aput v22, v2, v21

    const/16 v21, -0x6b

    aput v21, v2, v20

    const/16 v20, -0x73c5

    aput v20, v2, v19

    const/16 v19, -0x31

    aput v19, v2, v18

    const/16 v18, 0x2b31

    aput v18, v2, v17

    const/16 v17, 0x6c45

    aput v17, v2, v16

    const/16 v16, -0x1df7

    aput v16, v2, v15

    const/16 v15, -0x71

    aput v15, v2, v14

    const/16 v14, 0x438

    aput v14, v2, v13

    const/16 v13, -0x19b

    aput v13, v2, v12

    const/16 v12, -0x74

    aput v12, v2, v11

    const/16 v11, -0x30bf

    aput v11, v2, v10

    const/16 v10, -0x58

    aput v10, v2, v9

    const/16 v9, -0x7ead

    aput v9, v2, v8

    const/16 v8, -0x18

    aput v8, v2, v7

    const/16 v7, -0x2f

    aput v7, v2, v6

    const/16 v6, -0x6a8b

    aput v6, v2, v5

    const/16 v5, -0xc

    aput v5, v2, v4

    const/16 v4, -0x79

    aput v4, v2, v3

    const/16 v3, -0x52

    aput v3, v2, v1

    const/16 v1, 0x1a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, -0xa98

    aput v29, v1, v28

    const/16 v28, -0xb

    aput v28, v1, v27

    const/16 v27, -0x55

    aput v27, v1, v26

    const/16 v26, -0x20

    aput v26, v1, v25

    const/16 v25, -0x7f

    aput v25, v1, v24

    const/16 v24, -0x70

    aput v24, v1, v23

    const/16 v23, -0x3e

    aput v23, v1, v22

    const/16 v22, -0x5

    aput v22, v1, v21

    const/16 v21, -0x73ac

    aput v21, v1, v20

    const/16 v20, -0x74

    aput v20, v1, v19

    const/16 v19, 0x2b45

    aput v19, v1, v18

    const/16 v18, 0x6c2b

    aput v18, v1, v17

    const/16 v17, -0x1d94

    aput v17, v1, v16

    const/16 v16, -0x1e

    aput v16, v1, v15

    const/16 v15, 0x45f

    aput v15, v1, v14

    const/16 v14, -0x1fc

    aput v14, v1, v13

    const/4 v13, -0x2

    aput v13, v1, v12

    const/16 v12, -0x30f9

    aput v12, v1, v11

    const/16 v11, -0x31

    aput v11, v1, v10

    const/16 v10, -0x7ec3

    aput v10, v1, v9

    const/16 v9, -0x7f

    aput v9, v1, v8

    const/16 v8, -0x41

    aput v8, v1, v7

    const/16 v7, -0x6ae5

    aput v7, v1, v6

    const/16 v6, -0x6b

    aput v6, v1, v5

    const/16 v5, -0x1c

    aput v5, v1, v4

    const/4 v4, -0x3

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_a
    array-length v4, v1

    if-lt v3, v4, :cond_b

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_b
    array-length v4, v1

    if-lt v3, v4, :cond_c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->pI3HJI9QuL()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto/16 :goto_9

    :cond_b
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    :cond_c
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_b

    :cond_d
    :try_start_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_e
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_f
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v5, 0x0

    cmp-long v3, v1, v5

    if-eqz v3, :cond_10

    const-wide v5, -0x5352aa35520a005eL    # -1.7579207154914662E-93

    xor-long/2addr v1, v5

    :cond_10
    const/16 v3, 0x20

    shl-long/2addr v1, v3

    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_15

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-gtz v1, :cond_13

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x15f2

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x15c2

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_c
    array-length v5, v1

    if-lt v3, v5, :cond_11

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_d
    array-length v5, v1

    if-lt v3, v5, :cond_12

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5

    :catch_2
    move-exception v1

    move-object v4, v1

    const/16 v1, 0x1a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x520

    aput v29, v2, v28

    const/16 v28, 0x6e60

    aput v28, v2, v27

    const/16 v27, 0x4c02

    aput v27, v2, v26

    const/16 v26, 0x3c20

    aput v26, v2, v25

    const/16 v25, 0x1e53

    aput v25, v2, v24

    const/16 v24, -0x6c94

    aput v24, v2, v23

    const/16 v23, -0x19

    aput v23, v2, v22

    const/16 v22, -0x9

    aput v22, v2, v21

    const/16 v21, 0x311c

    aput v21, v2, v20

    const/16 v20, -0x6a8e

    aput v20, v2, v19

    const/16 v19, -0x1f

    aput v19, v2, v18

    const/16 v18, 0x2872

    aput v18, v2, v17

    const/16 v17, 0x1c4d

    aput v17, v2, v16

    const/16 v16, -0x448f

    aput v16, v2, v15

    const/16 v15, -0x24

    aput v15, v2, v14

    const/16 v14, -0xb

    aput v14, v2, v13

    const/16 v13, 0x1e3b

    aput v13, v2, v12

    const/16 v12, -0x38a8

    aput v12, v2, v11

    const/16 v11, -0x60

    aput v11, v2, v10

    const/16 v10, -0x78

    aput v10, v2, v9

    const/16 v9, -0x1b98

    aput v9, v2, v8

    const/16 v8, -0x76

    aput v8, v2, v7

    const/16 v7, -0x15

    aput v7, v2, v6

    const/16 v6, 0x567f

    aput v6, v2, v5

    const/16 v5, -0x3ecb

    aput v5, v2, v3

    const/16 v3, -0x6e

    aput v3, v2, v1

    const/16 v1, 0x1a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x552

    aput v30, v1, v29

    const/16 v29, 0x6e05

    aput v29, v1, v28

    const/16 v28, 0x4c6e

    aput v28, v1, v27

    const/16 v27, 0x3c4c

    aput v27, v1, v26

    const/16 v26, 0x1e3c

    aput v26, v1, v25

    const/16 v25, -0x6ce2

    aput v25, v1, v24

    const/16 v24, -0x6d

    aput v24, v1, v23

    const/16 v23, -0x67

    aput v23, v1, v22

    const/16 v22, 0x3173

    aput v22, v1, v21

    const/16 v21, -0x6acf

    aput v21, v1, v20

    const/16 v20, -0x6b

    aput v20, v1, v19

    const/16 v19, 0x281c

    aput v19, v1, v18

    const/16 v18, 0x1c28

    aput v18, v1, v17

    const/16 v17, -0x44e4

    aput v17, v1, v16

    const/16 v16, -0x45

    aput v16, v1, v15

    const/16 v15, -0x6c

    aput v15, v1, v14

    const/16 v14, 0x1e49

    aput v14, v1, v13

    const/16 v13, -0x38e2

    aput v13, v1, v12

    const/16 v12, -0x39

    aput v12, v1, v11

    const/16 v11, -0x1a

    aput v11, v1, v10

    const/16 v10, -0x1bff

    aput v10, v1, v9

    const/16 v9, -0x1c

    aput v9, v1, v8

    const/16 v8, -0x7b

    aput v8, v1, v7

    const/16 v7, 0x561e

    aput v7, v1, v6

    const/16 v6, -0x3eaa

    aput v6, v1, v5

    const/16 v5, -0x3f

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_e
    array-length v5, v1

    if-lt v3, v5, :cond_3d

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_f
    array-length v5, v1

    if-lt v3, v5, :cond_3e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v4}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto/16 :goto_8

    :cond_11
    :try_start_3
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_c

    :cond_12
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_d

    :cond_13
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v5, 0x0

    cmp-long v3, v1, v5

    if-eqz v3, :cond_14

    const-wide v5, -0x5352aa35520a005eL    # -1.7579207154914662E-93

    xor-long/2addr v1, v5

    :cond_14
    const/16 v3, 0x20

    shl-long/2addr v1, v3

    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_17

    :cond_15
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/utils/ScanningFragmentHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/utils/ScanningFragmentHelper;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/utils/ScanningFragmentHelper;->getPairedDevicesFromDB(Landroid/content/Context;)Ljava/util/ArrayList;
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5

    move-result-object v1

    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_8
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5

    :try_start_5
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_16
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceId()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_16

    goto/16 :goto_9

    :cond_17
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-gtz v1, :cond_1a

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x6efc

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x6ecc

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_10
    array-length v5, v1

    if-lt v3, v5, :cond_18

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_11
    array-length v5, v1

    if-lt v3, v5, :cond_19

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5

    :catch_3
    move-exception v1

    move-object v4, v1

    const/16 v1, 0x1a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x7d26

    aput v29, v2, v28

    const/16 v28, 0x6718

    aput v28, v2, v27

    const/16 v27, -0xef5

    aput v27, v2, v26

    const/16 v26, -0x63

    aput v26, v2, v25

    const/16 v25, -0x42

    aput v25, v2, v24

    const/16 v24, -0x78

    aput v24, v2, v23

    const/16 v23, -0x6

    aput v23, v2, v22

    const/16 v22, -0x7b

    aput v22, v2, v21

    const/16 v21, -0x59

    aput v21, v2, v20

    const/16 v20, -0x188d

    aput v20, v2, v19

    const/16 v19, -0x6d

    aput v19, v2, v18

    const/16 v18, -0x70

    aput v18, v2, v17

    const/16 v17, -0x5492

    aput v17, v2, v16

    const/16 v16, -0x3a

    aput v16, v2, v15

    const/16 v15, -0x20

    aput v15, v2, v14

    const/16 v14, -0x28

    aput v14, v2, v13

    const/16 v13, 0x682f

    aput v13, v2, v12

    const/16 v12, -0xbd2

    aput v12, v2, v11

    const/16 v11, -0x6d

    aput v11, v2, v10

    const/16 v10, 0x6f65

    aput v10, v2, v9

    const/16 v9, -0x38fa

    aput v9, v2, v8

    const/16 v8, -0x57

    aput v8, v2, v7

    const/16 v7, -0x2689

    aput v7, v2, v6

    const/16 v6, -0x48

    aput v6, v2, v5

    const/16 v5, -0x50

    aput v5, v2, v3

    const/16 v3, 0x3363

    aput v3, v2, v1

    const/16 v1, 0x1a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x7d54

    aput v30, v1, v29

    const/16 v29, 0x677d

    aput v29, v1, v28

    const/16 v28, -0xe99

    aput v28, v1, v27

    const/16 v27, -0xf

    aput v27, v1, v26

    const/16 v26, -0x2f

    aput v26, v1, v25

    const/16 v25, -0x6

    aput v25, v1, v24

    const/16 v24, -0x72

    aput v24, v1, v23

    const/16 v23, -0x15

    aput v23, v1, v22

    const/16 v22, -0x38

    aput v22, v1, v21

    const/16 v21, -0x18d0

    aput v21, v1, v20

    const/16 v20, -0x19

    aput v20, v1, v19

    const/16 v19, -0x2

    aput v19, v1, v18

    const/16 v18, -0x54f5

    aput v18, v1, v17

    const/16 v17, -0x55

    aput v17, v1, v16

    const/16 v16, -0x79

    aput v16, v1, v15

    const/16 v15, -0x47

    aput v15, v1, v14

    const/16 v14, 0x685d

    aput v14, v1, v13

    const/16 v13, -0xb98

    aput v13, v1, v12

    const/16 v12, -0xc

    aput v12, v1, v11

    const/16 v11, 0x6f0b

    aput v11, v1, v10

    const/16 v10, -0x3891

    aput v10, v1, v9

    const/16 v9, -0x39

    aput v9, v1, v8

    const/16 v8, -0x26e7

    aput v8, v1, v7

    const/16 v7, -0x27

    aput v7, v1, v6

    const/16 v6, -0x2d

    aput v6, v1, v5

    const/16 v5, 0x3330

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_12
    array-length v5, v1

    if-lt v3, v5, :cond_3f

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_13
    array-length v5, v1

    if-lt v3, v5, :cond_40

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto/16 :goto_8

    :cond_18
    :try_start_6
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_10

    :cond_19
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_11

    :cond_1a
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v5, 0x0

    cmp-long v5, v1, v5

    if-eqz v5, :cond_1b

    const-wide v5, -0x5352aa35520a005eL    # -1.7579207154914662E-93

    xor-long/2addr v1, v5

    :cond_1b
    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    shr-long/2addr v1, v5

    long-to-int v5, v1

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_1e

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x62

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x51

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_14
    array-length v5, v1

    if-lt v3, v5, :cond_1c

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_15
    array-length v5, v1

    if-lt v3, v5, :cond_1d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    :catch_4
    move-exception v1

    move-object v4, v1

    const/16 v1, 0x1a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, -0x4c

    aput v29, v2, v28

    const/16 v28, -0x74

    aput v28, v2, v27

    const/16 v27, -0x2c

    aput v27, v2, v26

    const/16 v26, -0x7e

    aput v26, v2, v25

    const/16 v25, -0x30

    aput v25, v2, v24

    const/16 v24, -0x5d

    aput v24, v2, v23

    const/16 v23, -0x5a

    aput v23, v2, v22

    const/16 v22, -0x4

    aput v22, v2, v21

    const/16 v21, 0x3866

    aput v21, v2, v20

    const/16 v20, 0x577b

    aput v20, v2, v19

    const/16 v19, -0x36dd

    aput v19, v2, v18

    const/16 v18, -0x59

    aput v18, v2, v17

    const/16 v17, -0xb

    aput v17, v2, v16

    const/16 v16, -0x1a83

    aput v16, v2, v15

    const/16 v15, -0x7e

    aput v15, v2, v14

    const/16 v14, -0x2e

    aput v14, v2, v13

    const/4 v13, -0x5

    aput v13, v2, v12

    const/16 v12, -0x6992

    aput v12, v2, v11

    const/16 v11, -0xf

    aput v11, v2, v10

    const/16 v10, 0x2809

    aput v10, v2, v9

    const/16 v9, -0x21bf

    aput v9, v2, v8

    const/16 v8, -0x50

    aput v8, v2, v7

    const/16 v7, -0x2b

    aput v7, v2, v6

    const/16 v6, -0x52

    aput v6, v2, v5

    const/16 v5, -0x43

    aput v5, v2, v3

    const/16 v3, -0x2ee2

    aput v3, v2, v1

    const/16 v1, 0x1a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, -0x3a

    aput v30, v1, v29

    const/16 v29, -0x17

    aput v29, v1, v28

    const/16 v28, -0x48

    aput v28, v1, v27

    const/16 v27, -0x12

    aput v27, v1, v26

    const/16 v26, -0x41

    aput v26, v1, v25

    const/16 v25, -0x2f

    aput v25, v1, v24

    const/16 v24, -0x2e

    aput v24, v1, v23

    const/16 v23, -0x6e

    aput v23, v1, v22

    const/16 v22, 0x3809

    aput v22, v1, v21

    const/16 v21, 0x5738

    aput v21, v1, v20

    const/16 v20, -0x36a9

    aput v20, v1, v19

    const/16 v19, -0x37

    aput v19, v1, v18

    const/16 v18, -0x70

    aput v18, v1, v17

    const/16 v17, -0x1af0

    aput v17, v1, v16

    const/16 v16, -0x1b

    aput v16, v1, v15

    const/16 v15, -0x4d

    aput v15, v1, v14

    const/16 v14, -0x77

    aput v14, v1, v13

    const/16 v13, -0x69d8

    aput v13, v1, v12

    const/16 v12, -0x6a

    aput v12, v1, v11

    const/16 v11, 0x2867

    aput v11, v1, v10

    const/16 v10, -0x21d8

    aput v10, v1, v9

    const/16 v9, -0x22

    aput v9, v1, v8

    const/16 v8, -0x45

    aput v8, v1, v7

    const/16 v7, -0x31

    aput v7, v1, v6

    const/16 v6, -0x22

    aput v6, v1, v5

    const/16 v5, -0x2eb3

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_16
    array-length v5, v1

    if-lt v3, v5, :cond_41

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_17
    array-length v5, v1

    if-lt v3, v5, :cond_42

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;->printStackTrace()V

    goto/16 :goto_8

    :cond_1c
    :try_start_7
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_14

    :cond_1d
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_15

    :cond_1e
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v6, v1, v6

    if-eqz v6, :cond_1f

    const-wide v6, -0x5352aa35520a005eL    # -1.7579207154914662E-93

    xor-long/2addr v1, v6

    :cond_1f
    const/16 v6, 0x20

    shr-long/2addr v1, v6

    long-to-int v6, v1

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x2

    if-gt v1, v2, :cond_22

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x63

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x51

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_18
    array-length v5, v1

    if-lt v3, v5, :cond_20

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_19
    array-length v5, v1

    if-lt v3, v5, :cond_21

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5

    :catch_5
    move-exception v1

    move-object v4, v1

    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    const/16 v1, 0x1a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x2545

    aput v29, v2, v28

    const/16 v28, -0x57c0

    aput v28, v2, v27

    const/16 v27, -0x3c

    aput v27, v2, v26

    const/16 v26, -0x4e

    aput v26, v2, v25

    const/16 v25, -0x658e

    aput v25, v2, v24

    const/16 v24, -0x18

    aput v24, v2, v23

    const/16 v23, 0x1a40

    aput v23, v2, v22

    const/16 v22, -0x388c

    aput v22, v2, v21

    const/16 v21, -0x58

    aput v21, v2, v20

    const/16 v20, 0x6203

    aput v20, v2, v19

    const/16 v19, -0x79ea

    aput v19, v2, v18

    const/16 v18, -0x18

    aput v18, v2, v17

    const/16 v17, -0x21ba

    aput v17, v2, v16

    const/16 v16, -0x4d

    aput v16, v2, v15

    const/16 v15, 0x670d

    aput v15, v2, v14

    const/16 v14, 0x7806

    aput v14, v2, v13

    const/16 v13, 0x2d0a

    aput v13, v2, v12

    const/16 v12, -0x7d95

    aput v12, v2, v11

    const/16 v11, -0x1b

    aput v11, v2, v10

    const/16 v10, -0x6d

    aput v10, v2, v9

    const/16 v9, -0x1f

    aput v9, v2, v8

    const/16 v8, -0x4498

    aput v8, v2, v7

    const/16 v7, -0x2b

    aput v7, v2, v6

    const/16 v6, -0x44

    aput v6, v2, v5

    const/16 v5, -0x15

    aput v5, v2, v3

    const/16 v3, -0x6da6

    aput v3, v2, v1

    const/16 v1, 0x1a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x2537

    aput v30, v1, v29

    const/16 v29, -0x57db

    aput v29, v1, v28

    const/16 v28, -0x58

    aput v28, v1, v27

    const/16 v27, -0x22

    aput v27, v1, v26

    const/16 v26, -0x65e3

    aput v26, v1, v25

    const/16 v25, -0x66

    aput v25, v1, v24

    const/16 v24, 0x1a34

    aput v24, v1, v23

    const/16 v23, -0x38e6

    aput v23, v1, v22

    const/16 v22, -0x39

    aput v22, v1, v21

    const/16 v21, 0x6240

    aput v21, v1, v20

    const/16 v20, -0x799e

    aput v20, v1, v19

    const/16 v19, -0x7a

    aput v19, v1, v18

    const/16 v18, -0x21dd

    aput v18, v1, v17

    const/16 v17, -0x22

    aput v17, v1, v16

    const/16 v16, 0x676a

    aput v16, v1, v15

    const/16 v15, 0x7867

    aput v15, v1, v14

    const/16 v14, 0x2d78

    aput v14, v1, v13

    const/16 v13, -0x7dd3

    aput v13, v1, v12

    const/16 v12, -0x7e

    aput v12, v1, v11

    const/4 v11, -0x3

    aput v11, v1, v10

    const/16 v10, -0x78

    aput v10, v1, v9

    const/16 v9, -0x44fa

    aput v9, v1, v8

    const/16 v8, -0x45

    aput v8, v1, v7

    const/16 v7, -0x23

    aput v7, v1, v6

    const/16 v6, -0x78

    aput v6, v1, v5

    const/16 v5, -0x6df7

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_1a
    array-length v5, v1

    if-lt v3, v5, :cond_43

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1b
    array-length v5, v1

    if-lt v3, v5, :cond_44

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_8

    :cond_20
    :try_start_8
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_18

    :cond_21
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_19

    :cond_22
    const/4 v1, 0x1

    aget-wide v1, v4, v1

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_23

    const-wide v7, -0x5352aa35520a005eL    # -1.7579207154914662E-93

    xor-long/2addr v1, v7

    :cond_23
    const/16 v7, 0x20

    shl-long/2addr v1, v7

    const/16 v7, 0x20

    shr-long/2addr v1, v7

    long-to-int v1, v1

    invoke-virtual {v3, v5, v6, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->getConnectedDeviceList(III)Ljava/util/List;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_8 .. :try_end_8} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_8 .. :try_end_8} :catch_4
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5

    move-result-object v5

    :try_start_9
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_9
    .catch Ljava/lang/NullPointerException; {:try_start_9 .. :try_end_9} :catch_6
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_9 .. :try_end_9} :catch_4
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_5

    const/4 v1, 0x0

    const/4 v2, 0x3

    :try_start_a
    array-length v3, v4

    add-int/lit8 v3, v3, -0x1

    aget-wide v6, v4, v3

    long-to-int v3, v6

    if-lt v2, v3, :cond_28

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_6
    move-exception v1

    const/16 v1, 0x1a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, -0x5d

    aput v28, v2, v27

    const/16 v27, -0x38c3

    aput v27, v2, v26

    const/16 v26, -0x55

    aput v26, v2, v25

    const/16 v25, -0x6096

    aput v25, v2, v24

    const/16 v24, -0x10

    aput v24, v2, v23

    const/16 v23, 0x6c06

    aput v23, v2, v22

    const/16 v22, -0x2de8

    aput v22, v2, v21

    const/16 v21, -0x44

    aput v21, v2, v20

    const/16 v20, -0x2782

    aput v20, v2, v19

    const/16 v19, -0x65

    aput v19, v2, v18

    const/16 v18, -0x78

    aput v18, v2, v17

    const/16 v17, -0x6a

    aput v17, v2, v16

    const/16 v16, 0xd0e

    aput v16, v2, v15

    const/16 v15, -0x17a0

    aput v15, v2, v14

    const/16 v14, -0x71

    aput v14, v2, v13

    const/16 v13, -0x16

    aput v13, v2, v12

    const/16 v12, -0x75

    aput v12, v2, v11

    const/16 v11, -0x7c

    aput v11, v2, v10

    const/16 v10, -0x7e

    aput v10, v2, v9

    const/16 v9, 0x4c6d

    aput v9, v2, v8

    const/16 v8, 0x825

    aput v8, v2, v7

    const/16 v7, 0x2d66

    aput v7, v2, v6

    const/16 v6, 0x4b43

    aput v6, v2, v5

    const/16 v5, -0x4bd6

    aput v5, v2, v4

    const/16 v4, -0x29

    aput v4, v2, v3

    const/16 v3, -0x28c1

    aput v3, v2, v1

    const/16 v1, 0x1a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, -0x2f

    aput v29, v1, v28

    const/16 v28, -0x38a8

    aput v28, v1, v27

    const/16 v27, -0x39

    aput v27, v1, v26

    const/16 v26, -0x60fa

    aput v26, v1, v25

    const/16 v25, -0x61

    aput v25, v1, v24

    const/16 v24, 0x6c74

    aput v24, v1, v23

    const/16 v23, -0x2d94

    aput v23, v1, v22

    const/16 v22, -0x2e

    aput v22, v1, v21

    const/16 v21, -0x27ef

    aput v21, v1, v20

    const/16 v20, -0x28

    aput v20, v1, v19

    const/16 v19, -0x4

    aput v19, v1, v18

    const/16 v18, -0x8

    aput v18, v1, v17

    const/16 v17, 0xd6b

    aput v17, v1, v16

    const/16 v16, -0x17f3

    aput v16, v1, v15

    const/16 v15, -0x18

    aput v15, v1, v14

    const/16 v14, -0x75

    aput v14, v1, v13

    const/4 v13, -0x7

    aput v13, v1, v12

    const/16 v12, -0x3e

    aput v12, v1, v11

    const/16 v11, -0x1b

    aput v11, v1, v10

    const/16 v10, 0x4c03

    aput v10, v1, v9

    const/16 v9, 0x84c

    aput v9, v1, v8

    const/16 v8, 0x2d08

    aput v8, v1, v7

    const/16 v7, 0x4b2d

    aput v7, v1, v6

    const/16 v6, -0x4bb5

    aput v6, v1, v5

    const/16 v5, -0x4c

    aput v5, v1, v4

    const/16 v4, -0x2894

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_1c
    array-length v4, v1

    if-lt v3, v4, :cond_24

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1d
    array-length v4, v1

    if-lt v3, v4, :cond_25

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x26

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, -0x78

    aput v41, v2, v40

    const/16 v40, 0x6f5e

    aput v40, v2, v39

    const/16 v39, -0x18e6

    aput v39, v2, v38

    const/16 v38, -0x77

    aput v38, v2, v37

    const/16 v37, -0x2ef7

    aput v37, v2, v36

    const/16 v36, -0xf

    aput v36, v2, v35

    const/16 v35, -0x16

    aput v35, v2, v34

    const/16 v34, 0x3827

    aput v34, v2, v33

    const/16 v33, 0x5918

    aput v33, v2, v32

    const/16 v32, 0xe79

    aput v32, v2, v31

    const/16 v31, 0x76b

    aput v31, v2, v30

    const/16 v30, 0x1864

    aput v30, v2, v29

    const/16 v29, 0x2871

    aput v29, v2, v28

    const/16 v28, 0x325e

    aput v28, v2, v27

    const/16 v27, -0x75c0

    aput v27, v2, v26

    const/16 v26, -0x11

    aput v26, v2, v25

    const/16 v25, -0x18

    aput v25, v2, v24

    const/16 v24, -0x18

    aput v24, v2, v23

    const/16 v23, -0x56

    aput v23, v2, v22

    const/16 v22, 0x5566

    aput v22, v2, v21

    const/16 v21, 0x7013

    aput v21, v2, v20

    const/16 v20, 0x1103

    aput v20, v2, v19

    const/16 v19, -0x688c

    aput v19, v2, v18

    const/16 v18, -0xc

    aput v18, v2, v17

    const/16 v17, -0x4afc

    aput v17, v2, v16

    const/16 v16, -0x3d

    aput v16, v2, v15

    const/16 v15, -0x6a

    aput v15, v2, v14

    const/16 v14, -0x2b5

    aput v14, v2, v13

    const/16 v13, -0x67

    aput v13, v2, v12

    const/16 v12, 0x350

    aput v12, v2, v11

    const/16 v11, -0x248f

    aput v11, v2, v10

    const/16 v10, -0x4e

    aput v10, v2, v9

    const/16 v9, -0x2e

    aput v9, v2, v8

    const/16 v8, -0x6f

    aput v8, v2, v7

    const/16 v7, 0x1753

    aput v7, v2, v6

    const/16 v6, -0x159c

    aput v6, v2, v5

    const/16 v5, -0x7d

    aput v5, v2, v3

    const/16 v3, 0x7a71

    aput v3, v2, v1

    const/16 v1, 0x26

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, -0x1c

    aput v42, v1, v41

    const/16 v41, 0x6f32

    aput v41, v1, v40

    const/16 v40, -0x1891

    aput v40, v1, v39

    const/16 v39, -0x19

    aput v39, v1, v38

    const/16 v38, -0x2ed7

    aput v38, v1, v37

    const/16 v37, -0x2f

    aput v37, v1, v36

    const/16 v36, -0x67

    aput v36, v1, v35

    const/16 v35, 0x384e

    aput v35, v1, v34

    const/16 v34, 0x5938

    aput v34, v1, v33

    const/16 v33, 0xe59

    aput v33, v1, v32

    const/16 v32, 0x70e

    aput v32, v1, v31

    const/16 v31, 0x1807

    aput v31, v1, v30

    const/16 v30, 0x2818

    aput v30, v1, v29

    const/16 v29, 0x3228

    aput v29, v1, v28

    const/16 v28, -0x75ce

    aput v28, v1, v27

    const/16 v27, -0x76

    aput v27, v1, v26

    const/16 v26, -0x45

    aput v26, v1, v25

    const/16 v25, -0x7b

    aput v25, v1, v24

    const/16 v24, -0x3b

    aput v24, v1, v23

    const/16 v23, 0x5514

    aput v23, v1, v22

    const/16 v22, 0x7055

    aput v22, v1, v21

    const/16 v21, 0x1170

    aput v21, v1, v20

    const/16 v20, -0x68ef

    aput v20, v1, v19

    const/16 v19, -0x69

    aput v19, v1, v18

    const/16 v18, -0x4a93

    aput v18, v1, v17

    const/16 v17, -0x4b

    aput v17, v1, v16

    const/16 v16, -0xd

    aput v16, v1, v15

    const/16 v15, -0x2f1

    aput v15, v1, v14

    const/4 v14, -0x3

    aput v14, v1, v13

    const/16 v13, 0x335

    aput v13, v1, v12

    const/16 v12, -0x24fd

    aput v12, v1, v11

    const/16 v11, -0x25

    aput v11, v1, v10

    const/16 v10, -0x4d

    aput v10, v1, v9

    const/16 v9, -0x3f

    aput v9, v1, v8

    const/16 v8, 0x1727

    aput v8, v1, v7

    const/16 v7, -0x15e9

    aput v7, v1, v6

    const/16 v6, -0x16

    aput v6, v1, v5

    const/16 v5, 0x7a1d

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_1e
    array-length v5, v1

    if-lt v3, v5, :cond_26

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1f
    array-length v5, v1

    if-lt v3, v5, :cond_27

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto/16 :goto_9

    :cond_24
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1c

    :cond_25
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1d

    :cond_26
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1e

    :cond_27
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1f

    :cond_28
    const/4 v3, 0x1

    int-to-long v1, v1

    const/16 v6, 0x20

    shl-long v6, v1, v6

    aget-wide v1, v4, v3

    const-wide/16 v8, 0x0

    cmp-long v8, v1, v8

    if-eqz v8, :cond_29

    const-wide v8, -0x5352aa35520a005eL    # -1.7579207154914662E-93

    xor-long/2addr v1, v8

    :cond_29
    const/16 v8, 0x20

    shl-long/2addr v1, v8

    const/16 v8, 0x20

    ushr-long/2addr v1, v8

    xor-long/2addr v1, v6

    const-wide v6, -0x5352aa35520a005eL    # -1.7579207154914662E-93

    xor-long/2addr v1, v6

    aput-wide v1, v4, v3

    :goto_20
    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x3

    if-gt v1, v2, :cond_2c

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x6c8c

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x6cb9

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_21
    array-length v5, v1

    if-lt v3, v5, :cond_2a

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_22
    array-length v5, v1

    if-lt v3, v5, :cond_2b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2a
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_21

    :cond_2b
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_22

    :cond_2c
    const/4 v1, 0x1

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v3, v1, v6

    if-eqz v3, :cond_2d

    const-wide v6, -0x5352aa35520a005eL    # -1.7579207154914662E-93

    xor-long/2addr v1, v6

    :cond_2d
    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_6

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x3

    if-gt v1, v2, :cond_30

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0xe1c

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0xe2f

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_23
    array-length v5, v1

    if-lt v3, v5, :cond_2e

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_24
    array-length v5, v1

    if-lt v3, v5, :cond_2f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2e
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_23

    :cond_2f
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_24

    :cond_30
    const/4 v1, 0x1

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v3, v1, v6

    if-eqz v3, :cond_31

    const-wide v6, -0x5352aa35520a005eL    # -1.7579207154914662E-93

    xor-long/2addr v1, v6

    :cond_31
    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_36

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getDataType()Ljava/util/List;
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_a .. :try_end_a} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_a .. :try_end_a} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_a .. :try_end_a} :catch_4
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_5

    move-result-object v2

    :try_start_b
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_b
    .catch Ljava/lang/NullPointerException; {:try_start_b .. :try_end_b} :catch_7
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_b} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_b} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_b .. :try_end_b} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_b .. :try_end_b} :catch_4
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_5

    :try_start_c
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_36

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v6, v4, v1

    long-to-int v1, v6

    const/4 v3, 0x3

    if-gt v1, v3, :cond_34

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x70e

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x73d

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_25
    array-length v5, v1

    if-lt v3, v5, :cond_32

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_26
    array-length v5, v1

    if-lt v3, v5, :cond_33

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_32
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_25

    :cond_33
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_26

    :cond_34
    const/4 v1, 0x1

    aget-wide v3, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v1, v3, v6

    if-eqz v1, :cond_35

    const-wide v6, -0x5352aa35520a005eL    # -1.7579207154914662E-93

    xor-long/2addr v3, v6

    :cond_35
    const/16 v1, 0x20

    shr-long/2addr v3, v1

    long-to-int v1, v3

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Z)V

    move-object v1, v2

    goto/16 :goto_9

    :catch_7
    move-exception v1

    :cond_36
    const/4 v3, 0x3

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-lt v3, v1, :cond_37

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_37
    const/4 v1, 0x1

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v6, v1, v6

    if-eqz v6, :cond_38

    const-wide v6, -0x5352aa35520a005eL    # -1.7579207154914662E-93

    xor-long/2addr v1, v6

    :cond_38
    const/16 v6, 0x20

    shr-long/2addr v1, v6

    long-to-int v1, v1

    add-int/lit8 v1, v1, 0x1

    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget-wide v6, v4, v2

    long-to-int v2, v6

    if-lt v3, v2, :cond_39

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_39
    const/4 v3, 0x1

    int-to-long v1, v1

    const/16 v6, 0x20

    shl-long v6, v1, v6

    aget-wide v1, v4, v3

    const-wide/16 v8, 0x0

    cmp-long v8, v1, v8

    if-eqz v8, :cond_3a

    const-wide v8, -0x5352aa35520a005eL    # -1.7579207154914662E-93

    xor-long/2addr v1, v8

    :cond_3a
    const/16 v8, 0x20

    shl-long/2addr v1, v8

    const/16 v8, 0x20

    ushr-long/2addr v1, v8

    xor-long/2addr v1, v6

    const-wide v6, -0x5352aa35520a005eL    # -1.7579207154914662E-93

    xor-long/2addr v1, v6

    aput-wide v1, v4, v3
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_c .. :try_end_c} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_c .. :try_end_c} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_c .. :try_end_c} :catch_4
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_5

    goto/16 :goto_20

    :cond_3b
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_3c
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :cond_3d
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_e

    :cond_3e
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_f

    :cond_3f
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_12

    :cond_40
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_13

    :cond_41
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_16

    :cond_42
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_17

    :cond_43
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1a

    :cond_44
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1b

    :catch_8
    move-exception v1

    goto/16 :goto_8
.end method

.method public requestPairedDevices(IZLjava/util/ArrayList;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 54
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;",
            ">;"
        }
    .end annotation

    const/4 v3, 0x2

    new-array v6, v3, [J

    const/4 v3, 0x1

    const-wide/16 v4, 0x1

    aput-wide v4, v6, v3

    const/4 v3, 0x0

    array-length v4, v6

    add-int/lit8 v4, v4, -0x1

    aget-wide v4, v6, v4

    long-to-int v4, v4

    if-gtz v4, :cond_0

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v5, Ljava/lang/Integer;

    invoke-direct {v5, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v5}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    const/4 v5, 0x0

    move/from16 v0, p1

    int-to-long v3, v0

    const/16 v7, 0x20

    shl-long/2addr v3, v7

    const/16 v7, 0x20

    ushr-long v7, v3, v7

    aget-wide v3, v6, v5

    const-wide/16 v9, 0x0

    cmp-long v9, v3, v9

    if-eqz v9, :cond_1

    const-wide v9, 0x1b446e08170128eeL

    xor-long/2addr v3, v9

    :cond_1
    const/16 v9, 0x20

    ushr-long/2addr v3, v9

    const/16 v9, 0x20

    shl-long/2addr v3, v9

    xor-long/2addr v3, v7

    const-wide v7, 0x1b446e08170128eeL

    xor-long/2addr v3, v7

    aput-wide v3, v6, v5

    const/16 v3, 0x1a

    new-array v4, v3, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, -0x5d

    aput v31, v4, v30

    const/16 v30, 0x1a6c

    aput v30, v4, v29

    const/16 v29, -0x1f8a

    aput v29, v4, v28

    const/16 v28, -0x74

    aput v28, v4, v27

    const/16 v27, -0x1c

    aput v27, v4, v26

    const/16 v26, -0x5ccc

    aput v26, v4, v25

    const/16 v25, -0x29

    aput v25, v4, v24

    const/16 v24, -0xbef

    aput v24, v4, v23

    const/16 v23, -0x65

    aput v23, v4, v22

    const/16 v22, -0x23

    aput v22, v4, v21

    const/16 v21, -0x2c

    aput v21, v4, v20

    const/16 v20, -0x51

    aput v20, v4, v19

    const/16 v19, -0x53

    aput v19, v4, v18

    const/16 v18, -0x64

    aput v18, v4, v17

    const/16 v17, 0x910

    aput v17, v4, v16

    const/16 v16, -0x6e98

    aput v16, v4, v15

    const/16 v15, -0x1d

    aput v15, v4, v14

    const/16 v14, -0x6091

    aput v14, v4, v13

    const/4 v13, -0x8

    aput v13, v4, v12

    const/4 v12, -0x1

    aput v12, v4, v11

    const/16 v11, 0x87b

    aput v11, v4, v10

    const/16 v10, -0x79a

    aput v10, v4, v9

    const/16 v9, -0x6a

    aput v9, v4, v8

    const/16 v8, 0x786e

    aput v8, v4, v7

    const/16 v7, -0x5fe5

    aput v7, v4, v5

    const/16 v5, -0xd

    aput v5, v4, v3

    const/16 v3, 0x1a

    new-array v3, v3, [I

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x11

    const/16 v24, 0x12

    const/16 v25, 0x13

    const/16 v26, 0x14

    const/16 v27, 0x15

    const/16 v28, 0x16

    const/16 v29, 0x17

    const/16 v30, 0x18

    const/16 v31, 0x19

    const/16 v32, -0x2f

    aput v32, v3, v31

    const/16 v31, 0x1a09

    aput v31, v3, v30

    const/16 v30, -0x1fe6

    aput v30, v3, v29

    const/16 v29, -0x20

    aput v29, v3, v28

    const/16 v28, -0x75

    aput v28, v3, v27

    const/16 v27, -0x5cba

    aput v27, v3, v26

    const/16 v26, -0x5d

    aput v26, v3, v25

    const/16 v25, -0xb81

    aput v25, v3, v24

    const/16 v24, -0xc

    aput v24, v3, v23

    const/16 v23, -0x62

    aput v23, v3, v22

    const/16 v22, -0x60

    aput v22, v3, v21

    const/16 v21, -0x3f

    aput v21, v3, v20

    const/16 v20, -0x38

    aput v20, v3, v19

    const/16 v19, -0xf

    aput v19, v3, v18

    const/16 v18, 0x977

    aput v18, v3, v17

    const/16 v17, -0x6ef7

    aput v17, v3, v16

    const/16 v16, -0x6f

    aput v16, v3, v15

    const/16 v15, -0x60d7

    aput v15, v3, v14

    const/16 v14, -0x61

    aput v14, v3, v13

    const/16 v13, -0x6f

    aput v13, v3, v12

    const/16 v12, 0x812

    aput v12, v3, v11

    const/16 v11, -0x7f8

    aput v11, v3, v10

    const/4 v10, -0x8

    aput v10, v3, v9

    const/16 v9, 0x780f

    aput v9, v3, v8

    const/16 v8, -0x5f88

    aput v8, v3, v7

    const/16 v7, -0x60

    aput v7, v3, v5

    const/4 v5, 0x0

    :goto_0
    array-length v7, v3

    if-lt v5, v7, :cond_2

    array-length v3, v4

    new-array v3, v3, [C

    const/4 v5, 0x0

    :goto_1
    array-length v7, v3

    if-lt v5, v7, :cond_3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v7

    const/16 v3, 0x1b

    new-array v4, v3, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x11

    const/16 v24, 0x12

    const/16 v25, 0x13

    const/16 v26, 0x14

    const/16 v27, 0x15

    const/16 v28, 0x16

    const/16 v29, 0x17

    const/16 v30, 0x18

    const/16 v31, 0x19

    const/16 v32, 0x1a

    const/16 v33, -0x28

    aput v33, v4, v32

    const/16 v32, -0x71

    aput v32, v4, v31

    const/16 v31, -0x30

    aput v31, v4, v30

    const/16 v30, -0x1bf4

    aput v30, v4, v29

    const/16 v29, -0x7b

    aput v29, v4, v28

    const/16 v28, -0x75

    aput v28, v4, v27

    const/16 v27, -0x68

    aput v27, v4, v26

    const/16 v26, -0x30cd

    aput v26, v4, v25

    const/16 v25, -0x56

    aput v25, v4, v24

    const/16 v24, -0xa

    aput v24, v4, v23

    const/16 v23, -0x52

    aput v23, v4, v22

    const/16 v22, -0x61

    aput v22, v4, v21

    const/16 v21, -0x2d

    aput v21, v4, v20

    const/16 v20, -0x6f

    aput v20, v4, v19

    const/16 v19, 0x1c58

    aput v19, v4, v18

    const/16 v18, -0x4d87

    aput v18, v4, v17

    const/16 v17, -0x40

    aput v17, v4, v16

    const/16 v16, 0x386c

    aput v16, v4, v15

    const/16 v15, 0x1759

    aput v15, v4, v14

    const/16 v14, -0x73b9

    aput v14, v4, v13

    const/4 v13, -0x8

    aput v13, v4, v12

    const/16 v12, -0x3f

    aput v12, v4, v11

    const/16 v11, -0xb

    aput v11, v4, v10

    const/16 v10, -0xaa3

    aput v10, v4, v9

    const/16 v9, -0x7c

    aput v9, v4, v8

    const/16 v8, 0x3256

    aput v8, v4, v5

    const/16 v5, 0x2e40

    aput v5, v4, v3

    const/16 v3, 0x1b

    new-array v3, v3, [I

    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v11, 0x4

    const/4 v12, 0x5

    const/4 v13, 0x6

    const/4 v14, 0x7

    const/16 v15, 0x8

    const/16 v16, 0x9

    const/16 v17, 0xa

    const/16 v18, 0xb

    const/16 v19, 0xc

    const/16 v20, 0xd

    const/16 v21, 0xe

    const/16 v22, 0xf

    const/16 v23, 0x10

    const/16 v24, 0x11

    const/16 v25, 0x12

    const/16 v26, 0x13

    const/16 v27, 0x14

    const/16 v28, 0x15

    const/16 v29, 0x16

    const/16 v30, 0x17

    const/16 v31, 0x18

    const/16 v32, 0x19

    const/16 v33, 0x1a

    const/16 v34, -0x44

    aput v34, v3, v33

    const/16 v33, -0x16

    aput v33, v3, v32

    const/16 v32, -0x44

    aput v32, v3, v31

    const/16 v31, -0x1ba0

    aput v31, v3, v30

    const/16 v30, -0x1c

    aput v30, v3, v29

    const/16 v29, -0x18

    aput v29, v3, v28

    const/16 v28, -0x48

    aput v28, v3, v27

    const/16 v27, -0x30c0

    aput v27, v3, v26

    const/16 v26, -0x31

    aput v26, v3, v25

    const/16 v25, -0x6b

    aput v25, v3, v24

    const/16 v24, -0x39

    aput v24, v3, v23

    const/16 v23, -0x17

    aput v23, v3, v22

    const/16 v22, -0x4a

    aput v22, v3, v21

    const/16 v21, -0x2b

    aput v21, v3, v20

    const/16 v20, 0x1c3c

    aput v20, v3, v19

    const/16 v19, -0x4de4

    aput v19, v3, v18

    const/16 v18, -0x4e

    aput v18, v3, v17

    const/16 v17, 0x3805

    aput v17, v3, v16

    const/16 v16, 0x1738

    aput v16, v3, v15

    const/16 v15, -0x73e9

    aput v15, v3, v14

    const/16 v14, -0x74

    aput v14, v3, v13

    const/16 v13, -0x4e

    aput v13, v3, v12

    const/16 v12, -0x70

    aput v12, v3, v11

    const/16 v11, -0xad8

    aput v11, v3, v10

    const/16 v10, -0xb

    aput v10, v3, v9

    const/16 v9, 0x3233

    aput v9, v3, v8

    const/16 v8, 0x2e32

    aput v8, v3, v5

    const/4 v5, 0x0

    :goto_2
    array-length v8, v3

    if-lt v5, v8, :cond_4

    array-length v3, v4

    new-array v3, v3, [C

    const/4 v5, 0x0

    :goto_3
    array-length v8, v3

    if-lt v5, v8, :cond_5

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    invoke-static {v7, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    :try_start_0
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    const/4 v7, 0x0

    const/16 v8, 0x2711

    array-length v3, v6

    add-int/lit8 v3, v3, -0x1

    aget-wide v3, v6, v3

    long-to-int v3, v3

    if-gtz v3, :cond_c

    new-instance v6, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v3, 0x1

    new-array v4, v3, [I

    const/4 v3, 0x0

    const/16 v5, -0x2d

    aput v5, v4, v3

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v5, 0x0

    const/16 v7, -0x1d

    aput v7, v3, v5

    const/4 v5, 0x0

    :goto_4
    array-length v7, v3

    if-lt v5, v7, :cond_a

    array-length v3, v4

    new-array v3, v3, [C

    const/4 v5, 0x0

    :goto_5
    array-length v7, v3

    if-lt v5, v7, :cond_b

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v6, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6

    :catch_0
    move-exception v3

    move-object v6, v3

    const/16 v3, 0x1a

    new-array v4, v3, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, -0x20cf

    aput v31, v4, v30

    const/16 v30, -0x46

    aput v30, v4, v29

    const/16 v29, 0x2f1b

    aput v29, v4, v28

    const/16 v28, -0x14bd

    aput v28, v4, v27

    const/16 v27, -0x7c

    aput v27, v4, v26

    const/16 v26, -0x1389

    aput v26, v4, v25

    const/16 v25, -0x68

    aput v25, v4, v24

    const/16 v24, -0x1

    aput v24, v4, v23

    const/16 v23, -0x5b

    aput v23, v4, v22

    const/16 v22, -0x26a1

    aput v22, v4, v21

    const/16 v21, -0x53

    aput v21, v4, v20

    const/16 v20, -0x7c

    aput v20, v4, v19

    const/16 v19, -0x69c7

    aput v19, v4, v18

    const/16 v18, -0x5

    aput v18, v4, v17

    const/16 v17, 0x4f31

    aput v17, v4, v16

    const/16 v16, -0x24d2

    aput v16, v4, v15

    const/16 v15, -0x57

    aput v15, v4, v14

    const/16 v14, -0x6c

    aput v14, v4, v13

    const/16 v13, -0x19

    aput v13, v4, v12

    const/16 v12, -0x24

    aput v12, v4, v11

    const/16 v11, -0x5d

    aput v11, v4, v10

    const/16 v10, 0x1c6d

    aput v10, v4, v9

    const/16 v9, -0x348e

    aput v9, v4, v8

    const/16 v8, -0x56

    aput v8, v4, v7

    const/16 v7, 0x7975

    aput v7, v4, v5

    const/16 v5, 0x2a

    aput v5, v4, v3

    const/16 v3, 0x1a

    new-array v3, v3, [I

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x11

    const/16 v24, 0x12

    const/16 v25, 0x13

    const/16 v26, 0x14

    const/16 v27, 0x15

    const/16 v28, 0x16

    const/16 v29, 0x17

    const/16 v30, 0x18

    const/16 v31, 0x19

    const/16 v32, -0x20bd

    aput v32, v3, v31

    const/16 v31, -0x21

    aput v31, v3, v30

    const/16 v30, 0x2f77

    aput v30, v3, v29

    const/16 v29, -0x14d1

    aput v29, v3, v28

    const/16 v28, -0x15

    aput v28, v3, v27

    const/16 v27, -0x13fb

    aput v27, v3, v26

    const/16 v26, -0x14

    aput v26, v3, v25

    const/16 v25, -0x6f

    aput v25, v3, v24

    const/16 v24, -0x36

    aput v24, v3, v23

    const/16 v23, -0x26e4

    aput v23, v3, v22

    const/16 v22, -0x27

    aput v22, v3, v21

    const/16 v21, -0x16

    aput v21, v3, v20

    const/16 v20, -0x69a4

    aput v20, v3, v19

    const/16 v19, -0x6a

    aput v19, v3, v18

    const/16 v18, 0x4f56

    aput v18, v3, v17

    const/16 v17, -0x24b1

    aput v17, v3, v16

    const/16 v16, -0x25

    aput v16, v3, v15

    const/16 v15, -0x2e

    aput v15, v3, v14

    const/16 v14, -0x80

    aput v14, v3, v13

    const/16 v13, -0x4e

    aput v13, v3, v12

    const/16 v12, -0x36

    aput v12, v3, v11

    const/16 v11, 0x1c03

    aput v11, v3, v10

    const/16 v10, -0x34e4

    aput v10, v3, v9

    const/16 v9, -0x35

    aput v9, v3, v8

    const/16 v8, 0x7916

    aput v8, v3, v7

    const/16 v7, 0x79

    aput v7, v3, v5

    const/4 v5, 0x0

    :goto_6
    array-length v7, v3

    if-lt v5, v7, :cond_1e

    array-length v3, v4

    new-array v3, v3, [C

    const/4 v5, 0x0

    :goto_7
    array-length v7, v3

    if-lt v5, v7, :cond_1f

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v6}, Landroid/os/RemoteException;->printStackTrace()V

    :goto_8
    const/4 v3, 0x0

    :goto_9
    return-object v3

    :cond_2
    aget v7, v3, v5

    aget v8, v4, v5

    xor-int/2addr v7, v8

    aput v7, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    :cond_3
    aget v7, v4, v5

    int-to-char v7, v7

    aput-char v7, v3, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1

    :cond_4
    aget v8, v3, v5

    aget v9, v4, v5

    xor-int/2addr v8, v9

    aput v8, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_2

    :cond_5
    aget v8, v4, v5

    int-to-char v8, v8

    aput-char v8, v3, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_3

    :catch_1
    move-exception v3

    const/16 v3, 0x1a

    new-array v4, v3, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, -0x8ce

    aput v30, v4, v29

    const/16 v29, -0x6e

    aput v29, v4, v28

    const/16 v28, 0x911

    aput v28, v4, v27

    const/16 v27, 0x5365

    aput v27, v4, v26

    const/16 v26, -0x7ec4

    aput v26, v4, v25

    const/16 v25, -0xd

    aput v25, v4, v24

    const/16 v24, 0x1f5d

    aput v24, v4, v23

    const/16 v23, 0x6471

    aput v23, v4, v22

    const/16 v22, 0x750b

    aput v22, v4, v21

    const/16 v21, 0x5f36

    aput v21, v4, v20

    const/16 v20, -0x2d5

    aput v20, v4, v19

    const/16 v19, -0x6d

    aput v19, v4, v18

    const/16 v18, -0x6f

    aput v18, v4, v17

    const/16 v17, 0x5417

    aput v17, v4, v16

    const/16 v16, 0x4033

    aput v16, v4, v15

    const/16 v15, -0x3cdf

    aput v15, v4, v14

    const/16 v14, -0x4f

    aput v14, v4, v13

    const/16 v13, -0x22

    aput v13, v4, v12

    const/16 v12, -0x38

    aput v12, v4, v11

    const/16 v11, -0x5c

    aput v11, v4, v10

    const/16 v10, 0x901

    aput v10, v4, v9

    const/16 v9, -0x5499

    aput v9, v4, v8

    const/16 v8, -0x3b

    aput v8, v4, v7

    const/16 v7, 0x2b0b

    aput v7, v4, v6

    const/16 v6, 0x3e48

    aput v6, v4, v5

    const/16 v5, 0x226d

    aput v5, v4, v3

    const/16 v3, 0x1a

    new-array v3, v3, [I

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, -0x8c0

    aput v31, v3, v30

    const/16 v30, -0x9

    aput v30, v3, v29

    const/16 v29, 0x97d

    aput v29, v3, v28

    const/16 v28, 0x5309

    aput v28, v3, v27

    const/16 v27, -0x7ead

    aput v27, v3, v26

    const/16 v26, -0x7f

    aput v26, v3, v25

    const/16 v25, 0x1f29

    aput v25, v3, v24

    const/16 v24, 0x641f

    aput v24, v3, v23

    const/16 v23, 0x7564

    aput v23, v3, v22

    const/16 v22, 0x5f75

    aput v22, v3, v21

    const/16 v21, -0x2a1

    aput v21, v3, v20

    const/16 v20, -0x3

    aput v20, v3, v19

    const/16 v19, -0xc

    aput v19, v3, v18

    const/16 v18, 0x547a

    aput v18, v3, v17

    const/16 v17, 0x4054

    aput v17, v3, v16

    const/16 v16, -0x3cc0

    aput v16, v3, v15

    const/16 v15, -0x3d

    aput v15, v3, v14

    const/16 v14, -0x68

    aput v14, v3, v13

    const/16 v13, -0x51

    aput v13, v3, v12

    const/16 v12, -0x36

    aput v12, v3, v11

    const/16 v11, 0x968

    aput v11, v3, v10

    const/16 v10, -0x54f7

    aput v10, v3, v9

    const/16 v9, -0x55

    aput v9, v3, v8

    const/16 v8, 0x2b6a

    aput v8, v3, v7

    const/16 v7, 0x3e2b

    aput v7, v3, v6

    const/16 v6, 0x223e

    aput v6, v3, v5

    const/4 v5, 0x0

    :goto_a
    array-length v6, v3

    if-lt v5, v6, :cond_6

    array-length v3, v4

    new-array v3, v3, [C

    const/4 v5, 0x0

    :goto_b
    array-length v6, v3

    if-lt v5, v6, :cond_7

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v6

    const/16 v3, 0x2f

    new-array v4, v3, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, 0x1a

    const/16 v32, 0x1b

    const/16 v33, 0x1c

    const/16 v34, 0x1d

    const/16 v35, 0x1e

    const/16 v36, 0x1f

    const/16 v37, 0x20

    const/16 v38, 0x21

    const/16 v39, 0x22

    const/16 v40, 0x23

    const/16 v41, 0x24

    const/16 v42, 0x25

    const/16 v43, 0x26

    const/16 v44, 0x27

    const/16 v45, 0x28

    const/16 v46, 0x29

    const/16 v47, 0x2a

    const/16 v48, 0x2b

    const/16 v49, 0x2c

    const/16 v50, 0x2d

    const/16 v51, 0x2e

    const/16 v52, -0x20

    aput v52, v4, v51

    const/16 v51, -0xf92

    aput v51, v4, v50

    const/16 v50, -0x6d

    aput v50, v4, v49

    const/16 v49, -0x45

    aput v49, v4, v48

    const/16 v48, -0x43

    aput v48, v4, v47

    const/16 v47, -0x42

    aput v47, v4, v46

    const/16 v46, 0x502c

    aput v46, v4, v45

    const/16 v45, -0x2690

    aput v45, v4, v44

    const/16 v44, -0x43

    aput v44, v4, v43

    const/16 v43, 0xd05

    aput v43, v4, v42

    const/16 v42, 0x87f

    aput v42, v4, v41

    const/16 v41, 0xf61

    aput v41, v4, v40

    const/16 v40, 0x2a6e

    aput v40, v4, v39

    const/16 v39, -0xa6

    aput v39, v4, v38

    const/16 v38, -0x21

    aput v38, v4, v37

    const/16 v37, -0x67c8

    aput v37, v4, v36

    const/16 v36, -0x3

    aput v36, v4, v35

    const/16 v35, -0x47

    aput v35, v4, v34

    const/16 v34, -0x3fa0

    aput v34, v4, v33

    const/16 v33, -0x4c

    aput v33, v4, v32

    const/16 v32, -0x378a

    aput v32, v4, v31

    const/16 v31, -0x5a

    aput v31, v4, v30

    const/16 v30, -0x33d1

    aput v30, v4, v29

    const/16 v29, -0x51

    aput v29, v4, v28

    const/16 v28, -0x20

    aput v28, v4, v27

    const/16 v27, -0x5f

    aput v27, v4, v26

    const/16 v26, -0x7fb6

    aput v26, v4, v25

    const/16 v25, -0x14

    aput v25, v4, v24

    const/16 v24, -0x2efc

    aput v24, v4, v23

    const/16 v23, -0x41

    aput v23, v4, v22

    const/16 v22, -0x77

    aput v22, v4, v21

    const/16 v21, -0xde8

    aput v21, v4, v20

    const/16 v20, -0x65

    aput v20, v4, v19

    const/16 v19, -0x1d

    aput v19, v4, v18

    const/16 v18, -0x17

    aput v18, v4, v17

    const/16 v17, -0x3f

    aput v17, v4, v16

    const/16 v16, -0x2a

    aput v16, v4, v15

    const/16 v15, 0x1434

    aput v15, v4, v14

    const/16 v14, 0xa71

    aput v14, v4, v13

    const/16 v13, 0x5559

    aput v13, v4, v12

    const/16 v12, 0x4a3d

    aput v12, v4, v11

    const/16 v11, 0x333e

    aput v11, v4, v10

    const/16 v10, 0x545f

    aput v10, v4, v9

    const/16 v9, -0x26cb

    aput v9, v4, v8

    const/16 v8, -0x44

    aput v8, v4, v7

    const/4 v7, -0x7

    aput v7, v4, v5

    const/16 v5, -0x4f

    aput v5, v4, v3

    const/16 v3, 0x2f

    new-array v3, v3, [I

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x11

    const/16 v24, 0x12

    const/16 v25, 0x13

    const/16 v26, 0x14

    const/16 v27, 0x15

    const/16 v28, 0x16

    const/16 v29, 0x17

    const/16 v30, 0x18

    const/16 v31, 0x19

    const/16 v32, 0x1a

    const/16 v33, 0x1b

    const/16 v34, 0x1c

    const/16 v35, 0x1d

    const/16 v36, 0x1e

    const/16 v37, 0x1f

    const/16 v38, 0x20

    const/16 v39, 0x21

    const/16 v40, 0x22

    const/16 v41, 0x23

    const/16 v42, 0x24

    const/16 v43, 0x25

    const/16 v44, 0x26

    const/16 v45, 0x27

    const/16 v46, 0x28

    const/16 v47, 0x29

    const/16 v48, 0x2a

    const/16 v49, 0x2b

    const/16 v50, 0x2c

    const/16 v51, 0x2d

    const/16 v52, 0x2e

    const/16 v53, -0x6d

    aput v53, v3, v52

    const/16 v52, -0xff5

    aput v52, v3, v51

    const/16 v51, -0x10

    aput v51, v3, v50

    const/16 v50, -0x2e

    aput v50, v3, v49

    const/16 v49, -0x35

    aput v49, v3, v48

    const/16 v48, -0x25

    aput v48, v3, v47

    const/16 v47, 0x5048

    aput v47, v3, v46

    const/16 v46, -0x26b0

    aput v46, v3, v45

    const/16 v45, -0x27

    aput v45, v3, v44

    const/16 v44, 0xd60

    aput v44, v3, v43

    const/16 v43, 0x80d

    aput v43, v3, v42

    const/16 v42, 0xf08

    aput v42, v3, v41

    const/16 v41, 0x2a0f

    aput v41, v3, v40

    const/16 v40, -0xd6

    aput v40, v3, v39

    const/16 v39, -0x1

    aput v39, v3, v38

    const/16 v38, -0x67b4

    aput v38, v3, v37

    const/16 v37, -0x68

    aput v37, v3, v36

    const/16 v36, -0x22

    aput v36, v3, v35

    const/16 v35, -0x3fc0

    aput v35, v3, v34

    const/16 v34, -0x40

    aput v34, v3, v33

    const/16 v33, -0x37af

    aput v33, v3, v32

    const/16 v32, -0x38

    aput v32, v3, v31

    const/16 v31, -0x33b2

    aput v31, v3, v30

    const/16 v30, -0x34

    aput v30, v3, v29

    const/16 v29, -0x40

    aput v29, v3, v28

    const/16 v28, -0x73

    aput v28, v3, v27

    const/16 v27, -0x7fda

    aput v27, v3, v26

    const/16 v26, -0x80

    aput v26, v3, v25

    const/16 v25, -0x2e8f

    aput v25, v3, v24

    const/16 v24, -0x2f

    aput v24, v3, v23

    const/16 v23, -0x57

    aput v23, v3, v22

    const/16 v22, -0xd95

    aput v22, v3, v21

    const/16 v21, -0xe

    aput v21, v3, v20

    const/16 v20, -0x3d

    aput v20, v3, v19

    const/16 v19, -0x65

    aput v19, v3, v18

    const/16 v18, -0x52

    aput v18, v3, v17

    const/16 v17, -0x5b

    aput v17, v3, v16

    const/16 v16, 0x145a

    aput v16, v3, v15

    const/16 v15, 0xa14

    aput v15, v3, v14

    const/16 v14, 0x550a

    aput v14, v3, v13

    const/16 v13, 0x4a55

    aput v13, v3, v12

    const/16 v12, 0x334a

    aput v12, v3, v11

    const/16 v11, 0x5433

    aput v11, v3, v10

    const/16 v10, -0x26ac

    aput v10, v3, v9

    const/16 v9, -0x27

    aput v9, v3, v8

    const/16 v8, -0x4f

    aput v8, v3, v7

    const/16 v7, -0x24

    aput v7, v3, v5

    const/4 v5, 0x0

    :goto_c
    array-length v7, v3

    if-lt v5, v7, :cond_8

    array-length v3, v4

    new-array v3, v3, [C

    const/4 v5, 0x0

    :goto_d
    array-length v7, v3

    if-lt v5, v7, :cond_9

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    goto/16 :goto_9

    :cond_6
    aget v6, v3, v5

    aget v7, v4, v5

    xor-int/2addr v6, v7

    aput v6, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_a

    :cond_7
    aget v6, v4, v5

    int-to-char v6, v6

    aput-char v6, v3, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_b

    :cond_8
    aget v7, v3, v5

    aget v8, v4, v5

    xor-int/2addr v7, v8

    aput v7, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_c

    :cond_9
    aget v7, v4, v5

    int-to-char v7, v7

    aput-char v7, v3, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_d

    :cond_a
    :try_start_2
    aget v7, v3, v5

    aget v8, v4, v5

    xor-int/2addr v7, v8

    aput v7, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_4

    :cond_b
    aget v7, v4, v5

    int-to-char v7, v7

    aput-char v7, v3, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_5

    :cond_c
    const/4 v3, 0x0

    aget-wide v3, v6, v3

    const-wide/16 v9, 0x0

    cmp-long v9, v3, v9

    if-eqz v9, :cond_d

    const-wide v9, 0x1b446e08170128eeL

    xor-long/2addr v3, v9

    :cond_d
    const/16 v9, 0x20

    shl-long/2addr v3, v9

    const/16 v9, 0x20

    shr-long/2addr v3, v9

    long-to-int v3, v3

    invoke-virtual {v5, v7, v8, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->getConnectedDeviceList(III)Ljava/util/List;

    move-result-object v5

    array-length v3, v6

    add-int/lit8 v3, v3, -0x1

    aget-wide v3, v6, v3

    long-to-int v3, v3

    if-gtz v3, :cond_10

    new-instance v6, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v3, 0x1

    new-array v4, v3, [I

    const/4 v3, 0x0

    const/16 v5, -0x72

    aput v5, v4, v3

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v5, 0x0

    const/16 v7, -0x42

    aput v7, v3, v5

    const/4 v5, 0x0

    :goto_e
    array-length v7, v3

    if-lt v5, v7, :cond_e

    array-length v3, v4

    new-array v3, v3, [C

    const/4 v5, 0x0

    :goto_f
    array-length v7, v3

    if-lt v5, v7, :cond_f

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v6, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6

    :catch_2
    move-exception v3

    move-object v6, v3

    const/16 v3, 0x1a

    new-array v4, v3, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, -0x7cc

    aput v31, v4, v30

    const/16 v30, -0x63

    aput v30, v4, v29

    const/16 v29, -0x4d

    aput v29, v4, v28

    const/16 v28, -0x12cd

    aput v28, v4, v27

    const/16 v27, -0x7e

    aput v27, v4, v26

    const/16 v26, -0x5ef

    aput v26, v4, v25

    const/16 v25, -0x72

    aput v25, v4, v24

    const/16 v24, 0x672d

    aput v24, v4, v23

    const/16 v23, -0x5df8

    aput v23, v4, v22

    const/16 v22, -0x1f

    aput v22, v4, v21

    const/16 v21, -0x2f

    aput v21, v4, v20

    const/16 v20, -0x61

    aput v20, v4, v19

    const/16 v19, 0x6b68

    aput v19, v4, v18

    const/16 v18, -0x2ffa

    aput v18, v4, v17

    const/16 v17, -0x49

    aput v17, v4, v16

    const/16 v16, -0x43

    aput v16, v4, v15

    const/16 v15, 0xa60

    aput v15, v4, v14

    const/16 v14, 0x604c

    aput v14, v4, v13

    const/16 v13, -0x61f9

    aput v13, v4, v12

    const/16 v12, -0x10

    aput v12, v4, v11

    const/16 v11, -0x42e0

    aput v11, v4, v10

    const/16 v10, -0x2d

    aput v10, v4, v9

    const/16 v9, -0x15

    aput v9, v4, v8

    const/16 v8, 0x1a37

    aput v8, v4, v7

    const/16 v7, -0x1a87

    aput v7, v4, v5

    const/16 v5, -0x4a

    aput v5, v4, v3

    const/16 v3, 0x1a

    new-array v3, v3, [I

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x11

    const/16 v24, 0x12

    const/16 v25, 0x13

    const/16 v26, 0x14

    const/16 v27, 0x15

    const/16 v28, 0x16

    const/16 v29, 0x17

    const/16 v30, 0x18

    const/16 v31, 0x19

    const/16 v32, -0x7ba

    aput v32, v3, v31

    const/16 v31, -0x8

    aput v31, v3, v30

    const/16 v30, -0x21

    aput v30, v3, v29

    const/16 v29, -0x12a1

    aput v29, v3, v28

    const/16 v28, -0x13

    aput v28, v3, v27

    const/16 v27, -0x59d

    aput v27, v3, v26

    const/16 v26, -0x6

    aput v26, v3, v25

    const/16 v25, 0x6743

    aput v25, v3, v24

    const/16 v24, -0x5d99

    aput v24, v3, v23

    const/16 v23, -0x5e

    aput v23, v3, v22

    const/16 v22, -0x5b

    aput v22, v3, v21

    const/16 v21, -0xf

    aput v21, v3, v20

    const/16 v20, 0x6b0d

    aput v20, v3, v19

    const/16 v19, -0x2f95

    aput v19, v3, v18

    const/16 v18, -0x30

    aput v18, v3, v17

    const/16 v17, -0x24

    aput v17, v3, v16

    const/16 v16, 0xa12

    aput v16, v3, v15

    const/16 v15, 0x600a

    aput v15, v3, v14

    const/16 v14, -0x61a0

    aput v14, v3, v13

    const/16 v13, -0x62

    aput v13, v3, v12

    const/16 v12, -0x42b7

    aput v12, v3, v11

    const/16 v11, -0x43

    aput v11, v3, v10

    const/16 v10, -0x7b

    aput v10, v3, v9

    const/16 v9, 0x1a56

    aput v9, v3, v8

    const/16 v8, -0x1ae6

    aput v8, v3, v7

    const/16 v7, -0x1b

    aput v7, v3, v5

    const/4 v5, 0x0

    :goto_10
    array-length v7, v3

    if-lt v5, v7, :cond_20

    array-length v3, v4

    new-array v3, v3, [C

    const/4 v5, 0x0

    :goto_11
    array-length v7, v3

    if-lt v5, v7, :cond_21

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v6}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto/16 :goto_8

    :cond_e
    :try_start_3
    aget v7, v3, v5

    aget v8, v4, v5

    xor-int/2addr v7, v8

    aput v7, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_e

    :cond_f
    aget v7, v4, v5

    int-to-char v7, v7

    aput-char v7, v3, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_f

    :cond_10
    const/4 v3, 0x0

    aget-wide v3, v6, v3

    const-wide/16 v7, 0x0

    cmp-long v7, v3, v7

    if-eqz v7, :cond_11

    const-wide v7, 0x1b446e08170128eeL

    xor-long/2addr v3, v7

    :cond_11
    const/16 v7, 0x20

    shl-long/2addr v3, v7

    const/16 v7, 0x20

    shr-long/2addr v3, v7

    long-to-int v3, v3

    move-object/from16 v0, p0

    move/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v3, v1, v2, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->getFilteredPairedDevicesFromSensorService(IZLjava/util/ArrayList;Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v7

    array-length v3, v6

    add-int/lit8 v3, v3, -0x1

    aget-wide v3, v6, v3

    long-to-int v3, v3

    if-gtz v3, :cond_14

    new-instance v6, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v3, 0x1

    new-array v4, v3, [I

    const/4 v3, 0x0

    const/16 v5, -0x7e4

    aput v5, v4, v3

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v5, 0x0

    const/16 v7, -0x7d4

    aput v7, v3, v5

    const/4 v5, 0x0

    :goto_12
    array-length v7, v3

    if-lt v5, v7, :cond_12

    array-length v3, v4

    new-array v3, v3, [C

    const/4 v5, 0x0

    :goto_13
    array-length v7, v3

    if-lt v5, v7, :cond_13

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v6, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_6

    :catch_3
    move-exception v3

    move-object v6, v3

    const/16 v3, 0x1a

    new-array v4, v3, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, -0x61

    aput v31, v4, v30

    const/16 v30, -0x2d

    aput v30, v4, v29

    const/16 v29, -0x18

    aput v29, v4, v28

    const/16 v28, -0x37b7

    aput v28, v4, v27

    const/16 v27, -0x59

    aput v27, v4, v26

    const/16 v26, 0x3024

    aput v26, v4, v25

    const/16 v25, 0x6244

    aput v25, v4, v24

    const/16 v24, 0xc0c

    aput v24, v4, v23

    const/16 v23, -0x5c9d

    aput v23, v4, v22

    const/16 v22, -0x20

    aput v22, v4, v21

    const/16 v21, 0x2d29

    aput v21, v4, v20

    const/16 v20, 0x5a43

    aput v20, v4, v19

    const/16 v19, 0x1d3f

    aput v19, v4, v18

    const/16 v18, 0x670

    aput v18, v4, v17

    const/16 v17, 0x5e61

    aput v17, v4, v16

    const/16 v16, -0x7bc1

    aput v16, v4, v15

    const/16 v15, -0xa

    aput v15, v4, v14

    const/16 v14, -0x7d

    aput v14, v4, v13

    const/16 v13, -0x7c

    aput v13, v4, v12

    const/16 v12, 0x4057

    aput v12, v4, v11

    const/16 v11, 0x4829

    aput v11, v4, v10

    const/16 v10, 0x2126

    aput v10, v4, v9

    const/16 v9, 0x674f

    aput v9, v4, v8

    const/16 v8, -0x17fa

    aput v8, v4, v7

    const/16 v7, -0x75

    aput v7, v4, v5

    const/16 v5, -0x3e

    aput v5, v4, v3

    const/16 v3, 0x1a

    new-array v3, v3, [I

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x11

    const/16 v24, 0x12

    const/16 v25, 0x13

    const/16 v26, 0x14

    const/16 v27, 0x15

    const/16 v28, 0x16

    const/16 v29, 0x17

    const/16 v30, 0x18

    const/16 v31, 0x19

    const/16 v32, -0x13

    aput v32, v3, v31

    const/16 v31, -0x4a

    aput v31, v3, v30

    const/16 v30, -0x7c

    aput v30, v3, v29

    const/16 v29, -0x37db

    aput v29, v3, v28

    const/16 v28, -0x38

    aput v28, v3, v27

    const/16 v27, 0x3056

    aput v27, v3, v26

    const/16 v26, 0x6230

    aput v26, v3, v25

    const/16 v25, 0xc62

    aput v25, v3, v24

    const/16 v24, -0x5cf4

    aput v24, v3, v23

    const/16 v23, -0x5d

    aput v23, v3, v22

    const/16 v22, 0x2d5d

    aput v22, v3, v21

    const/16 v21, 0x5a2d

    aput v21, v3, v20

    const/16 v20, 0x1d5a

    aput v20, v3, v19

    const/16 v19, 0x61d

    aput v19, v3, v18

    const/16 v18, 0x5e06

    aput v18, v3, v17

    const/16 v17, -0x7ba2

    aput v17, v3, v16

    const/16 v16, -0x7c

    aput v16, v3, v15

    const/16 v15, -0x3b

    aput v15, v3, v14

    const/16 v14, -0x1d

    aput v14, v3, v13

    const/16 v13, 0x4039

    aput v13, v3, v12

    const/16 v12, 0x4840

    aput v12, v3, v11

    const/16 v11, 0x2148

    aput v11, v3, v10

    const/16 v10, 0x6721

    aput v10, v3, v9

    const/16 v9, -0x1799

    aput v9, v3, v8

    const/16 v8, -0x18

    aput v8, v3, v7

    const/16 v7, -0x6f

    aput v7, v3, v5

    const/4 v5, 0x0

    :goto_14
    array-length v7, v3

    if-lt v5, v7, :cond_22

    array-length v3, v4

    new-array v3, v3, [C

    const/4 v5, 0x0

    :goto_15
    array-length v7, v3

    if-lt v5, v7, :cond_23

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v6}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto/16 :goto_8

    :cond_12
    :try_start_4
    aget v7, v3, v5

    aget v8, v4, v5

    xor-int/2addr v7, v8

    aput v7, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_12

    :cond_13
    aget v7, v4, v5

    int-to-char v7, v7

    aput-char v7, v3, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_13

    :cond_14
    const/4 v3, 0x0

    aget-wide v3, v6, v3

    const-wide/16 v8, 0x0

    cmp-long v6, v3, v8

    if-eqz v6, :cond_15

    const-wide v8, 0x1b446e08170128eeL

    xor-long/2addr v3, v8

    :cond_15
    const/16 v6, 0x20

    shl-long/2addr v3, v6

    const/16 v6, 0x20

    shr-long/2addr v3, v6

    long-to-int v3, v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5, v7}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->getPairedDevicesFromDbWhichAreAlsoInConnectedState(ILjava/util/List;Ljava/util/ArrayList;)Ljava/util/concurrent/ConcurrentHashMap;
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_6

    move-result-object v11

    :try_start_5
    invoke-virtual {v11}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_6

    :goto_16
    :try_start_6
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_16
    :goto_17
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1b

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v10}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v3}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_17

    invoke-virtual {v10}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_19

    invoke-virtual {v10}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v3

    const/16 v4, 0x2718

    if-ne v3, v4, :cond_19

    invoke-virtual {v10}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getSerialNumber()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v3}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_19

    :cond_17
    invoke-virtual {v10}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_18

    invoke-virtual {v10}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v3

    const/16 v4, 0x2718

    if-ne v3, v4, :cond_18

    invoke-virtual {v10}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getSerialNumber()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    :goto_18
    invoke-virtual {v3, v10}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->setShealthSensorDevice(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->setIsPaired(Z)V

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->setIsConnected(Z)V
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6

    goto :goto_17

    :catch_4
    move-exception v3

    move-object v6, v3

    const/16 v3, 0x1a

    new-array v4, v3, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, -0x59e4

    aput v31, v4, v30

    const/16 v30, -0x3d

    aput v30, v4, v29

    const/16 v29, -0xd99

    aput v29, v4, v28

    const/16 v28, -0x62

    aput v28, v4, v27

    const/16 v27, 0x4c2d

    aput v27, v4, v26

    const/16 v26, 0x643e

    aput v26, v4, v25

    const/16 v25, 0x7b10

    aput v25, v4, v24

    const/16 v24, -0x25eb

    aput v24, v4, v23

    const/16 v23, -0x4b

    aput v23, v4, v22

    const/16 v22, -0x34c8

    aput v22, v4, v21

    const/16 v21, -0x41

    aput v21, v4, v20

    const/16 v20, -0x5d8b

    aput v20, v4, v19

    const/16 v19, -0x39

    aput v19, v4, v18

    const/16 v18, -0x6e

    aput v18, v4, v17

    const/16 v17, -0x7f

    aput v17, v4, v16

    const/16 v16, 0x4747

    aput v16, v4, v15

    const/16 v15, -0x7ccb

    aput v15, v4, v14

    const/16 v14, -0x3b

    aput v14, v4, v13

    const/16 v13, -0x53

    aput v13, v4, v12

    const/16 v12, -0x5a

    aput v12, v4, v11

    const/16 v11, -0x9

    aput v11, v4, v10

    const/16 v10, -0x39

    aput v10, v4, v9

    const/16 v9, -0x3b

    aput v9, v4, v8

    const/16 v8, -0x43

    aput v8, v4, v7

    const/4 v7, -0x1

    aput v7, v4, v5

    const/16 v5, -0x43

    aput v5, v4, v3

    const/16 v3, 0x1a

    new-array v3, v3, [I

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x11

    const/16 v24, 0x12

    const/16 v25, 0x13

    const/16 v26, 0x14

    const/16 v27, 0x15

    const/16 v28, 0x16

    const/16 v29, 0x17

    const/16 v30, 0x18

    const/16 v31, 0x19

    const/16 v32, -0x5992

    aput v32, v3, v31

    const/16 v31, -0x5a

    aput v31, v3, v30

    const/16 v30, -0xdf5

    aput v30, v3, v29

    const/16 v29, -0xe

    aput v29, v3, v28

    const/16 v28, 0x4c42

    aput v28, v3, v27

    const/16 v27, 0x644c

    aput v27, v3, v26

    const/16 v26, 0x7b64

    aput v26, v3, v25

    const/16 v25, -0x2585

    aput v25, v3, v24

    const/16 v24, -0x26

    aput v24, v3, v23

    const/16 v23, -0x3485

    aput v23, v3, v22

    const/16 v22, -0x35

    aput v22, v3, v21

    const/16 v21, -0x5de5

    aput v21, v3, v20

    const/16 v20, -0x5e

    aput v20, v3, v19

    const/16 v19, -0x1

    aput v19, v3, v18

    const/16 v18, -0x1a

    aput v18, v3, v17

    const/16 v17, 0x4726

    aput v17, v3, v16

    const/16 v16, -0x7cb9

    aput v16, v3, v15

    const/16 v15, -0x7d

    aput v15, v3, v14

    const/16 v14, -0x36

    aput v14, v3, v13

    const/16 v13, -0x38

    aput v13, v3, v12

    const/16 v12, -0x62

    aput v12, v3, v11

    const/16 v11, -0x57

    aput v11, v3, v10

    const/16 v10, -0x55

    aput v10, v3, v9

    const/16 v9, -0x24

    aput v9, v3, v8

    const/16 v8, -0x64

    aput v8, v3, v7

    const/16 v7, -0x12

    aput v7, v3, v5

    const/4 v5, 0x0

    :goto_19
    array-length v7, v3

    if-lt v5, v7, :cond_24

    array-length v3, v4

    new-array v3, v3, [C

    const/4 v5, 0x0

    :goto_1a
    array-length v7, v3

    if-lt v5, v7, :cond_25

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v6}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;->printStackTrace()V

    goto/16 :goto_8

    :catch_5
    move-exception v3

    :try_start_7
    new-instance v11, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v11}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6

    goto/16 :goto_16

    :catch_6
    move-exception v3

    move-object v6, v3

    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    const/16 v3, 0x1a

    new-array v4, v3, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, -0x21

    aput v31, v4, v30

    const/16 v30, 0x694b

    aput v30, v4, v29

    const/16 v29, 0x4005

    aput v29, v4, v28

    const/16 v28, -0x43d4

    aput v28, v4, v27

    const/16 v27, -0x2d

    aput v27, v4, v26

    const/16 v26, -0x2

    aput v26, v4, v25

    const/16 v25, -0x2

    aput v25, v4, v24

    const/16 v24, 0x5a

    aput v24, v4, v23

    const/16 v23, -0x2c91

    aput v23, v4, v22

    const/16 v22, -0x70

    aput v22, v4, v21

    const/16 v21, -0xd

    aput v21, v4, v20

    const/16 v20, -0x4

    aput v20, v4, v19

    const/16 v19, -0x50dc

    aput v19, v4, v18

    const/16 v18, -0x3e

    aput v18, v4, v17

    const/16 v17, -0x79

    aput v17, v4, v16

    const/16 v16, -0x7fc0

    aput v16, v4, v15

    const/16 v15, -0xe

    aput v15, v4, v14

    const/16 v14, 0xb31

    aput v14, v4, v13

    const/16 v13, 0x146c

    aput v13, v4, v12

    const/16 v12, -0xb86

    aput v12, v4, v11

    const/16 v11, -0x63

    aput v11, v4, v10

    const/16 v10, -0x6dcf

    aput v10, v4, v9

    const/4 v9, -0x4

    aput v9, v4, v8

    const/16 v8, -0x4a

    aput v8, v4, v7

    const/16 v7, 0x5f65

    aput v7, v4, v5

    const/16 v5, 0x5f0c

    aput v5, v4, v3

    const/16 v3, 0x1a

    new-array v3, v3, [I

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x11

    const/16 v24, 0x12

    const/16 v25, 0x13

    const/16 v26, 0x14

    const/16 v27, 0x15

    const/16 v28, 0x16

    const/16 v29, 0x17

    const/16 v30, 0x18

    const/16 v31, 0x19

    const/16 v32, -0x53

    aput v32, v3, v31

    const/16 v31, 0x692e

    aput v31, v3, v30

    const/16 v30, 0x4069

    aput v30, v3, v29

    const/16 v29, -0x43c0

    aput v29, v3, v28

    const/16 v28, -0x44

    aput v28, v3, v27

    const/16 v27, -0x74

    aput v27, v3, v26

    const/16 v26, -0x76

    aput v26, v3, v25

    const/16 v25, 0x34

    aput v25, v3, v24

    const/16 v24, -0x2d00

    aput v24, v3, v23

    const/16 v23, -0x2d

    aput v23, v3, v22

    const/16 v22, -0x79

    aput v22, v3, v21

    const/16 v21, -0x6e

    aput v21, v3, v20

    const/16 v20, -0x50bf

    aput v20, v3, v19

    const/16 v19, -0x51

    aput v19, v3, v18

    const/16 v18, -0x20

    aput v18, v3, v17

    const/16 v17, -0x7fdf

    aput v17, v3, v16

    const/16 v16, -0x80

    aput v16, v3, v15

    const/16 v15, 0xb77

    aput v15, v3, v14

    const/16 v14, 0x140b

    aput v14, v3, v13

    const/16 v13, -0xbec

    aput v13, v3, v12

    const/16 v12, -0xc

    aput v12, v3, v11

    const/16 v11, -0x6da1

    aput v11, v3, v10

    const/16 v10, -0x6e

    aput v10, v3, v9

    const/16 v9, -0x29

    aput v9, v3, v8

    const/16 v8, 0x5f06

    aput v8, v3, v7

    const/16 v7, 0x5f5f

    aput v7, v3, v5

    const/4 v5, 0x0

    :goto_1b
    array-length v7, v3

    if-lt v5, v7, :cond_26

    array-length v3, v4

    new-array v3, v3, [C

    const/4 v5, 0x0

    :goto_1c
    array-length v7, v3

    if-lt v5, v7, :cond_27

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_8

    :cond_18
    :try_start_8
    invoke-virtual {v10}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    goto/16 :goto_18

    :cond_19
    invoke-virtual {v10}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_1a

    invoke-virtual {v10}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v3

    const/16 v4, 0x2718

    if-ne v3, v4, :cond_1a

    invoke-virtual {v10}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getDataType()Ljava/util/List;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_8 .. :try_end_8} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_8 .. :try_end_8} :catch_4
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_6

    move-result-object v3

    :try_start_9
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_9
    .catch Ljava/lang/NullPointerException; {:try_start_9 .. :try_end_9} :catch_7
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_9 .. :try_end_9} :catch_4
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_6

    :try_start_a
    invoke-virtual {v10}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1a

    invoke-virtual {v10}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getSerialNumber()Ljava/lang/String;

    move-result-object v13

    new-instance v3, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v10}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getSerialNumber()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v5

    invoke-virtual {v10}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v10}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v7

    invoke-virtual {v10}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    invoke-direct/range {v3 .. v10}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;-><init>(Ljava/lang/String;IIILjava/lang/String;ZLcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V

    invoke-virtual {v11, v13, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_17

    :catch_7
    move-exception v3

    :cond_1a
    invoke-virtual {v10}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getDataType()Ljava/util/List;
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_a .. :try_end_a} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_a .. :try_end_a} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_a .. :try_end_a} :catch_4
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_6

    move-result-object v3

    :try_start_b
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_b
    .catch Ljava/lang/NullPointerException; {:try_start_b .. :try_end_b} :catch_9
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_b} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_b} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_b .. :try_end_b} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_b .. :try_end_b} :catch_4
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_6

    :try_start_c
    invoke-virtual {v10}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_16

    invoke-virtual {v10}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    const/4 v5, 0x1

    invoke-direct {v4, v10, v5}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Z)V

    invoke-virtual {v11, v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_c .. :try_end_c} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_c .. :try_end_c} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_c .. :try_end_c} :catch_4
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_6

    goto/16 :goto_17

    :cond_1b
    :try_start_d
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_d
    .catch Ljava/lang/NullPointerException; {:try_start_d .. :try_end_d} :catch_8
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_d} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_d .. :try_end_d} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_d .. :try_end_d} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_d .. :try_end_d} :catch_4
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_6

    const/16 v3, 0x2711

    :try_start_e
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_28

    new-instance v5, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v5}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-virtual {v11}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1c
    :goto_1d
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1d

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v11, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceType()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, p3

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1c

    invoke-virtual {v5, v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_e} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_e .. :try_end_e} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_e .. :try_end_e} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_e .. :try_end_e} :catch_4
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_6

    goto :goto_1d

    :cond_1d
    move-object v3, v5

    goto/16 :goto_9

    :catch_8
    move-exception v3

    move-object v3, v11

    goto/16 :goto_9

    :cond_1e
    aget v7, v3, v5

    aget v8, v4, v5

    xor-int/2addr v7, v8

    aput v7, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_6

    :cond_1f
    aget v7, v4, v5

    int-to-char v7, v7

    aput-char v7, v3, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_7

    :cond_20
    aget v7, v3, v5

    aget v8, v4, v5

    xor-int/2addr v7, v8

    aput v7, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_10

    :cond_21
    aget v7, v4, v5

    int-to-char v7, v7

    aput-char v7, v3, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_11

    :cond_22
    aget v7, v3, v5

    aget v8, v4, v5

    xor-int/2addr v7, v8

    aput v7, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_14

    :cond_23
    aget v7, v4, v5

    int-to-char v7, v7

    aput-char v7, v3, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_15

    :cond_24
    aget v7, v3, v5

    aget v8, v4, v5

    xor-int/2addr v7, v8

    aput v7, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_19

    :cond_25
    aget v7, v4, v5

    int-to-char v7, v7

    aput-char v7, v3, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1a

    :cond_26
    aget v7, v3, v5

    aget v8, v4, v5

    xor-int/2addr v7, v8

    aput v7, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1b

    :cond_27
    aget v7, v4, v5

    int-to-char v7, v7

    aput-char v7, v3, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1c

    :catch_9
    move-exception v3

    goto/16 :goto_17

    :cond_28
    move-object v3, v11

    goto/16 :goto_9
.end method

.method public startScan(ILjava/util/ArrayList;IILcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentScanListener;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;II",
            "Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentScanListener;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
        }
    .end annotation

    const/4 v0, 0x3

    new-array v5, v0, [J

    const/4 v0, 0x2

    const-wide/16 v1, 0x4

    aput-wide v1, v5, v0

    const/4 v0, 0x0

    array-length v1, v5

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v5, v1

    long-to-int v1, v1

    if-gtz v1, :cond_0

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/4 v2, 0x0

    int-to-long v0, p1

    const/16 v3, 0x20

    shl-long/2addr v0, v3

    const/16 v3, 0x20

    ushr-long v3, v0, v3

    aget-wide v0, v5, v2

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-eqz v6, :cond_1

    const-wide v6, 0x671b14074fcfd563L    # 4.7127855825244934E188

    xor-long/2addr v0, v6

    :cond_1
    const/16 v6, 0x20

    ushr-long/2addr v0, v6

    const/16 v6, 0x20

    shl-long/2addr v0, v6

    xor-long/2addr v0, v3

    const-wide v3, 0x671b14074fcfd563L    # 4.7127855825244934E188

    xor-long/2addr v0, v3

    aput-wide v0, v5, v2

    const/4 v0, 0x1

    array-length v1, v5

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v5, v1

    long-to-int v1, v1

    if-lt v0, v1, :cond_2

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    const/4 v2, 0x0

    int-to-long v0, p3

    const/16 v3, 0x20

    shl-long v3, v0, v3

    aget-wide v0, v5, v2

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-eqz v6, :cond_3

    const-wide v6, 0x671b14074fcfd563L    # 4.7127855825244934E188

    xor-long/2addr v0, v6

    :cond_3
    const/16 v6, 0x20

    shl-long/2addr v0, v6

    const/16 v6, 0x20

    ushr-long/2addr v0, v6

    xor-long/2addr v0, v3

    const-wide v3, 0x671b14074fcfd563L    # 4.7127855825244934E188

    xor-long/2addr v0, v3

    aput-wide v0, v5, v2

    const/4 v0, 0x2

    array-length v1, v5

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v5, v1

    long-to-int v1, v1

    if-lt v0, v1, :cond_4

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    const/4 v2, 0x1

    int-to-long v0, p4

    const/16 v3, 0x20

    shl-long/2addr v0, v3

    const/16 v3, 0x20

    ushr-long v3, v0, v3

    aget-wide v0, v5, v2

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-eqz v6, :cond_5

    const-wide v6, 0x671b14074fcfd563L    # 4.7127855825244934E188

    xor-long/2addr v0, v6

    :cond_5
    const/16 v6, 0x20

    ushr-long/2addr v0, v6

    const/16 v6, 0x20

    shl-long/2addr v0, v6

    xor-long/2addr v0, v3

    const-wide v3, 0x671b14074fcfd563L    # 4.7127855825244934E188

    xor-long/2addr v0, v3

    aput-wide v0, v5, v2

    const/16 v0, 0x2711

    const/4 v1, 0x3

    array-length v2, v5

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v5, v2

    long-to-int v2, v2

    if-lt v1, v2, :cond_6

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    const/4 v2, 0x1

    int-to-long v0, v0

    const/16 v3, 0x20

    shl-long v3, v0, v3

    aget-wide v0, v5, v2

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-eqz v6, :cond_7

    const-wide v6, 0x671b14074fcfd563L    # 4.7127855825244934E188

    xor-long/2addr v0, v6

    :cond_7
    const/16 v6, 0x20

    shl-long/2addr v0, v6

    const/16 v6, 0x20

    ushr-long/2addr v0, v6

    xor-long/2addr v0, v3

    const-wide v3, 0x671b14074fcfd563L    # 4.7127855825244934E188

    xor-long/2addr v0, v3

    aput-wide v0, v5, v2

    :try_start_0
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    const/16 v0, 0x2711

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_a

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x3

    array-length v2, v5

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v5, v2

    long-to-int v2, v2

    if-lt v1, v2, :cond_8

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    const/4 v2, 0x1

    int-to-long v0, v0

    const/16 v3, 0x20

    shl-long v3, v0, v3

    aget-wide v0, v5, v2

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-eqz v6, :cond_9

    const-wide v6, 0x671b14074fcfd563L    # 4.7127855825244934E188

    xor-long/2addr v0, v6

    :cond_9
    const/16 v6, 0x20

    shl-long/2addr v0, v6

    const/16 v6, 0x20

    ushr-long/2addr v0, v6

    xor-long/2addr v0, v3

    const-wide v3, 0x671b14074fcfd563L    # 4.7127855825244934E188

    xor-long/2addr v0, v3

    aput-wide v0, v5, v2

    :cond_a
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    array-length v1, v5

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v5, v1

    long-to-int v1, v1

    if-gtz v1, :cond_d

    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/16 v2, -0x55

    aput v2, v1, v0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/16 v4, -0x65

    aput v4, v0, v2

    const/4 v2, 0x0

    :goto_1
    array-length v4, v0

    if-lt v2, v4, :cond_b

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_2
    array-length v4, v0

    if-lt v2, v4, :cond_c

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_b
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_c
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_d
    const/4 v1, 0x0

    aget-wide v1, v5, v1

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-eqz v3, :cond_e

    const-wide v3, 0x671b14074fcfd563L    # 4.7127855825244934E188

    xor-long/2addr v1, v3

    :cond_e
    const/16 v3, 0x20

    shl-long/2addr v1, v3

    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    array-length v2, v5

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v5, v2

    long-to-int v2, v2

    const/4 v3, 0x3

    if-gt v2, v3, :cond_11

    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/16 v2, -0x37a9

    aput v2, v1, v0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/16 v4, -0x379c

    aput v4, v0, v2

    const/4 v2, 0x0

    :goto_3
    array-length v4, v0

    if-lt v2, v4, :cond_f

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_4
    array-length v4, v0

    if-lt v2, v4, :cond_10

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_f
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_10
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_11
    const/4 v2, 0x1

    aget-wide v2, v5, v2

    const-wide/16 v6, 0x0

    cmp-long v4, v2, v6

    if-eqz v4, :cond_12

    const-wide v6, 0x671b14074fcfd563L    # 4.7127855825244934E188

    xor-long/2addr v2, v6

    :cond_12
    const/16 v4, 0x20

    shr-long/2addr v2, v4

    long-to-int v2, v2

    array-length v3, v5

    add-int/lit8 v3, v3, -0x1

    aget-wide v3, v5, v3

    long-to-int v3, v3

    const/4 v4, 0x1

    if-gt v3, v4, :cond_15

    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/16 v2, -0x4f

    aput v2, v1, v0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/16 v4, -0x80

    aput v4, v0, v2

    const/4 v2, 0x0

    :goto_5
    array-length v4, v0

    if-lt v2, v4, :cond_13

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_6
    array-length v4, v0

    if-lt v2, v4, :cond_14

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_13
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_14
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    :cond_15
    const/4 v3, 0x0

    aget-wide v3, v5, v3

    const-wide/16 v6, 0x0

    cmp-long v6, v3, v6

    if-eqz v6, :cond_16

    const-wide v6, 0x671b14074fcfd563L    # 4.7127855825244934E188

    xor-long/2addr v3, v6

    :cond_16
    const/16 v6, 0x20

    shr-long/2addr v3, v6

    long-to-int v3, v3

    array-length v4, v5

    add-int/lit8 v4, v4, -0x1

    aget-wide v6, v5, v4

    long-to-int v4, v6

    const/4 v6, 0x2

    if-gt v4, v6, :cond_19

    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/16 v2, -0x7b

    aput v2, v1, v0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/16 v4, -0x49

    aput v4, v0, v2

    const/4 v2, 0x0

    :goto_7
    array-length v4, v0

    if-lt v2, v4, :cond_17

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_8
    array-length v4, v0

    if-lt v2, v4, :cond_18

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_17
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    :cond_18
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    :cond_19
    const/4 v4, 0x1

    aget-wide v4, v5, v4

    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-eqz v6, :cond_1a

    const-wide v6, 0x671b14074fcfd563L    # 4.7127855825244934E188

    xor-long/2addr v4, v6

    :cond_1a
    const/16 v6, 0x20

    shl-long/2addr v4, v6

    const/16 v6, 0x20

    shr-long/2addr v4, v6

    long-to-int v4, v4

    new-instance v5, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperScanListener;

    invoke-direct {v5, p0, p5, p2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperScanListener;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentScanListener;Ljava/util/ArrayList;)V

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->startScan(IIIILcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;)V

    :goto_9
    return-void

    :catch_0
    move-exception v0

    const/4 v0, 0x2

    invoke-interface {p5, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentScanListener;->onStopped(I)V

    goto :goto_9

    :catch_1
    move-exception v0

    goto/16 :goto_0
.end method

.method public startWearableSync(Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$SyncEventListener;)V
    .locals 32

    const/16 v3, 0x1a

    new-array v4, v3, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, -0x5a

    aput v30, v4, v29

    const/16 v29, -0x6c0

    aput v29, v4, v28

    const/16 v28, -0x6b

    aput v28, v4, v27

    const/16 v27, -0x6c

    aput v27, v4, v26

    const/16 v26, -0x15a7

    aput v26, v4, v25

    const/16 v25, -0x68

    aput v25, v4, v24

    const/16 v24, -0xb

    aput v24, v4, v23

    const/16 v23, -0x63

    aput v23, v4, v22

    const/16 v22, 0x217e

    aput v22, v4, v21

    const/16 v21, -0xc9e

    aput v21, v4, v20

    const/16 v20, -0x79

    aput v20, v4, v19

    const/16 v19, 0x351a

    aput v19, v4, v18

    const/16 v18, 0x2c50

    aput v18, v4, v17

    const/16 v17, -0x68bf

    aput v17, v4, v16

    const/16 v16, -0x10

    aput v16, v4, v15

    const/16 v15, -0x13

    aput v15, v4, v14

    const/16 v14, -0x3c

    aput v14, v4, v13

    const/16 v13, -0x1db2

    aput v13, v4, v12

    const/16 v12, -0x7b

    aput v12, v4, v11

    const/16 v11, -0x28

    aput v11, v4, v10

    const/16 v10, 0x5001

    aput v10, v4, v9

    const/16 v9, 0x123e

    aput v9, v4, v8

    const/16 v8, -0x4384

    aput v8, v4, v7

    const/16 v7, -0x23

    aput v7, v4, v6

    const/16 v6, 0x6f7b    # 3.9992E-41f

    aput v6, v4, v5

    const/16 v5, 0x283c

    aput v5, v4, v3

    const/16 v3, 0x1a

    new-array v3, v3, [I

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, -0x2c

    aput v31, v3, v30

    const/16 v30, -0x6db

    aput v30, v3, v29

    const/16 v29, -0x7

    aput v29, v3, v28

    const/16 v28, -0x8

    aput v28, v3, v27

    const/16 v27, -0x15ca

    aput v27, v3, v26

    const/16 v26, -0x16

    aput v26, v3, v25

    const/16 v25, -0x7f

    aput v25, v3, v24

    const/16 v24, -0xd

    aput v24, v3, v23

    const/16 v23, 0x2111

    aput v23, v3, v22

    const/16 v22, -0xcdf

    aput v22, v3, v21

    const/16 v21, -0xd

    aput v21, v3, v20

    const/16 v20, 0x3574

    aput v20, v3, v19

    const/16 v19, 0x2c35

    aput v19, v3, v18

    const/16 v18, -0x68d4

    aput v18, v3, v17

    const/16 v17, -0x69

    aput v17, v3, v16

    const/16 v16, -0x74

    aput v16, v3, v15

    const/16 v15, -0x4a

    aput v15, v3, v14

    const/16 v14, -0x1df8

    aput v14, v3, v13

    const/16 v13, -0x1e

    aput v13, v3, v12

    const/16 v12, -0x4a

    aput v12, v3, v11

    const/16 v11, 0x5068

    aput v11, v3, v10

    const/16 v10, 0x1250

    aput v10, v3, v9

    const/16 v9, -0x43ee

    aput v9, v3, v8

    const/16 v8, -0x44

    aput v8, v3, v7

    const/16 v7, 0x6f18

    aput v7, v3, v6

    const/16 v6, 0x286f

    aput v6, v3, v5

    const/4 v5, 0x0

    :goto_0
    array-length v6, v3

    if-lt v5, v6, :cond_0

    array-length v3, v4

    new-array v3, v3, [C

    const/4 v5, 0x0

    :goto_1
    array-length v6, v3

    if-lt v5, v6, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v6

    const/16 v3, 0x13

    new-array v4, v3, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x4144

    aput v24, v4, v23

    const/16 v23, -0x3d97

    aput v23, v4, v22

    const/16 v22, -0x5f

    aput v22, v4, v21

    const/16 v21, -0x53e9

    aput v21, v4, v20

    const/16 v20, -0x2b

    aput v20, v4, v19

    const/16 v19, -0x3dea

    aput v19, v4, v18

    const/16 v18, -0x59

    aput v18, v4, v17

    const/16 v17, -0x71

    aput v17, v4, v16

    const/16 v16, -0x2a

    aput v16, v4, v15

    const/16 v15, -0x33

    aput v15, v4, v14

    const/16 v14, 0x6d0a

    aput v14, v4, v13

    const/16 v13, 0x300c

    aput v13, v4, v12

    const/16 v12, 0x4d55

    aput v12, v4, v11

    const/16 v11, -0x66e6

    aput v11, v4, v10

    const/16 v10, -0x13

    aput v10, v4, v9

    const/16 v9, -0x4cd7

    aput v9, v4, v8

    const/16 v8, -0x2e

    aput v8, v4, v7

    const/16 v7, -0x19

    aput v7, v4, v5

    const/16 v5, 0x210d

    aput v5, v4, v3

    const/16 v3, 0x13

    new-array v3, v3, [I

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x11

    const/16 v24, 0x12

    const/16 v25, 0x416d

    aput v25, v3, v24

    const/16 v24, -0x3dbf

    aput v24, v3, v23

    const/16 v23, -0x3e

    aput v23, v3, v22

    const/16 v22, -0x5387

    aput v22, v3, v21

    const/16 v21, -0x54

    aput v21, v3, v20

    const/16 v20, -0x3dbb

    aput v20, v3, v19

    const/16 v19, -0x3e

    aput v19, v3, v18

    const/16 v18, -0x1d

    aput v18, v3, v17

    const/16 v17, -0x4c

    aput v17, v3, v16

    const/16 v16, -0x54

    aput v16, v3, v15

    const/16 v15, 0x6d78

    aput v15, v3, v14

    const/16 v14, 0x306d

    aput v14, v3, v13

    const/16 v13, 0x4d30

    aput v13, v3, v12

    const/16 v12, -0x66b3

    aput v12, v3, v11

    const/16 v11, -0x67

    aput v11, v3, v10

    const/16 v10, -0x4ca5

    aput v10, v3, v9

    const/16 v9, -0x4d

    aput v9, v3, v8

    const/16 v8, -0x6d

    aput v8, v3, v7

    const/16 v7, 0x217e

    aput v7, v3, v5

    const/4 v5, 0x0

    :goto_2
    array-length v7, v3

    if-lt v5, v7, :cond_2

    array-length v3, v4

    new-array v3, v3, [C

    const/4 v5, 0x0

    :goto_3
    array-length v7, v3

    if-lt v5, v7, :cond_3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_5

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v3

    :try_start_1
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_6

    :try_start_2
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncEventListener;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v4, v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncEventListener;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$SyncEventListener;)V

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->join(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_2 .. :try_end_2} :catch_4

    :goto_4
    return-void

    :cond_0
    aget v6, v3, v5

    aget v7, v4, v5

    xor-int/2addr v6, v7

    aput v6, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    :cond_1
    aget v6, v4, v5

    int-to-char v6, v6

    aput-char v6, v3, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1

    :cond_2
    aget v7, v3, v5

    aget v8, v4, v5

    xor-int/2addr v7, v8

    aput v7, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_3
    aget v7, v4, v5

    int-to-char v7, v7

    aput-char v7, v3, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_4

    :catch_1
    move-exception v3

    invoke-virtual {v3}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_4

    :catch_2
    move-exception v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_4

    :catch_3
    move-exception v3

    invoke-virtual {v3}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_4

    :catch_4
    move-exception v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_4

    :catch_5
    move-exception v3

    goto :goto_4

    :catch_6
    move-exception v3

    goto :goto_4
.end method

.method public stopScan()V
    .locals 30
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v1, 0x1a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0xe43

    aput v28, v2, v27

    const/16 v27, 0x2a6b

    aput v27, v2, v26

    const/16 v26, 0x2646

    aput v26, v2, v25

    const/16 v25, -0x45b6

    aput v25, v2, v24

    const/16 v24, -0x2b

    aput v24, v2, v23

    const/16 v23, 0x5714

    aput v23, v2, v22

    const/16 v22, -0x6dd

    aput v22, v2, v21

    const/16 v21, -0x69

    aput v21, v2, v20

    const/16 v20, -0x40b5

    aput v20, v2, v19

    const/16 v19, -0x4

    aput v19, v2, v18

    const/16 v18, -0x15f5

    aput v18, v2, v17

    const/16 v17, -0x7c

    aput v17, v2, v16

    const/16 v16, 0x169

    aput v16, v2, v15

    const/16 v15, -0x6994

    aput v15, v2, v14

    const/16 v14, -0xf

    aput v14, v2, v13

    const/16 v13, -0x7f

    aput v13, v2, v12

    const/16 v12, 0x3f6d

    aput v12, v2, v11

    const/16 v11, -0x1687

    aput v11, v2, v10

    const/16 v10, -0x72

    aput v10, v2, v9

    const/16 v9, -0x1986

    aput v9, v2, v8

    const/16 v8, -0x71

    aput v8, v2, v7

    const/16 v7, -0x45

    aput v7, v2, v6

    const/16 v6, -0x2699

    aput v6, v2, v5

    const/16 v5, -0x48

    aput v5, v2, v4

    const/16 v4, -0x15b9

    aput v4, v2, v3

    const/16 v3, -0x47

    aput v3, v2, v1

    const/16 v1, 0x1a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0xe31

    aput v29, v1, v28

    const/16 v28, 0x2a0e

    aput v28, v1, v27

    const/16 v27, 0x262a

    aput v27, v1, v26

    const/16 v26, -0x45da

    aput v26, v1, v25

    const/16 v25, -0x46

    aput v25, v1, v24

    const/16 v24, 0x5766

    aput v24, v1, v23

    const/16 v23, -0x6a9

    aput v23, v1, v22

    const/16 v22, -0x7

    aput v22, v1, v21

    const/16 v21, -0x40dc

    aput v21, v1, v20

    const/16 v20, -0x41

    aput v20, v1, v19

    const/16 v19, -0x1581

    aput v19, v1, v18

    const/16 v18, -0x16

    aput v18, v1, v17

    const/16 v17, 0x10c

    aput v17, v1, v16

    const/16 v16, -0x69ff

    aput v16, v1, v15

    const/16 v15, -0x6a

    aput v15, v1, v14

    const/16 v14, -0x20

    aput v14, v1, v13

    const/16 v13, 0x3f1f

    aput v13, v1, v12

    const/16 v12, -0x16c1

    aput v12, v1, v11

    const/16 v11, -0x17

    aput v11, v1, v10

    const/16 v10, -0x19ec

    aput v10, v1, v9

    const/16 v9, -0x1a

    aput v9, v1, v8

    const/16 v8, -0x2b

    aput v8, v1, v7

    const/16 v7, -0x26f7

    aput v7, v1, v6

    const/16 v6, -0x27

    aput v6, v1, v5

    const/16 v5, -0x15dc

    aput v5, v1, v4

    const/16 v4, -0x16

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0xe

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, -0x118b

    aput v17, v2, v16

    const/16 v16, -0x40

    aput v16, v2, v15

    const/16 v15, -0x7e

    aput v15, v2, v14

    const/16 v14, -0x13

    aput v14, v2, v13

    const/16 v13, -0x57c9

    aput v13, v2, v12

    const/16 v12, -0x78

    aput v12, v2, v11

    const/16 v11, -0x20

    aput v11, v2, v10

    const/16 v10, -0x16

    aput v10, v2, v9

    const/16 v9, -0x42

    aput v9, v2, v8

    const/16 v8, -0x40

    aput v8, v2, v7

    const/16 v7, -0x37

    aput v7, v2, v6

    const/16 v6, -0xdec

    aput v6, v2, v5

    const/16 v5, -0x7a

    aput v5, v2, v3

    const/16 v3, -0x63

    aput v3, v2, v1

    const/16 v1, 0xe

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, -0x11a5

    aput v18, v1, v17

    const/16 v17, -0x12

    aput v17, v1, v16

    const/16 v16, -0xd

    aput v16, v1, v15

    const/16 v15, -0x78

    aput v15, v1, v14

    const/16 v14, -0x57bb

    aput v14, v1, v13

    const/16 v13, -0x58

    aput v13, v1, v12

    const/16 v12, -0x72

    aput v12, v1, v11

    const/16 v11, -0x75

    aput v11, v1, v10

    const/16 v10, -0x23

    aput v10, v1, v9

    const/16 v9, -0x6d

    aput v9, v1, v8

    const/16 v8, -0x47

    aput v8, v1, v7

    const/16 v7, -0xd85

    aput v7, v1, v6

    const/16 v6, -0xe

    aput v6, v1, v5

    const/16 v5, -0x12

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->stopScan()V

    :goto_4
    return-void

    :cond_0
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :catch_0
    move-exception v1

    const/16 v1, 0x1a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, -0x62

    aput v28, v2, v27

    const/16 v27, -0x7edb

    aput v27, v2, v26

    const/16 v26, -0x13

    aput v26, v2, v25

    const/16 v25, -0x28

    aput v25, v2, v24

    const/16 v24, -0x53ed

    aput v24, v2, v23

    const/16 v23, -0x22

    aput v23, v2, v22

    const/16 v22, -0x5f

    aput v22, v2, v21

    const/16 v21, -0x5d

    aput v21, v2, v20

    const/16 v20, -0x6c

    aput v20, v2, v19

    const/16 v19, 0x614

    aput v19, v2, v18

    const/16 v18, 0x2772

    aput v18, v2, v17

    const/16 v17, -0x51b7

    aput v17, v2, v16

    const/16 v16, -0x35

    aput v16, v2, v15

    const/16 v15, 0x6816

    aput v15, v2, v14

    const/16 v14, 0x250f

    aput v14, v2, v13

    const/16 v13, -0x43bc

    aput v13, v2, v12

    const/16 v12, -0x32

    aput v12, v2, v11

    const/16 v11, -0x9

    aput v11, v2, v10

    const/16 v10, -0x1f90

    aput v10, v2, v9

    const/16 v9, -0x72

    aput v9, v2, v8

    const/16 v8, -0x788

    aput v8, v2, v7

    const/16 v7, -0x6a

    aput v7, v2, v6

    const/16 v6, -0x52

    aput v6, v2, v5

    const/16 v5, -0x6ac1

    aput v5, v2, v4

    const/16 v4, -0xa

    aput v4, v2, v3

    const/16 v3, 0x150d

    aput v3, v2, v1

    const/16 v1, 0x1a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, -0x14

    aput v29, v1, v28

    const/16 v28, -0x7ec0

    aput v28, v1, v27

    const/16 v27, -0x7f

    aput v27, v1, v26

    const/16 v26, -0x4c

    aput v26, v1, v25

    const/16 v25, -0x5384

    aput v25, v1, v24

    const/16 v24, -0x54

    aput v24, v1, v23

    const/16 v23, -0x2b

    aput v23, v1, v22

    const/16 v22, -0x33

    aput v22, v1, v21

    const/16 v21, -0x5

    aput v21, v1, v20

    const/16 v20, 0x657

    aput v20, v1, v19

    const/16 v19, 0x2706

    aput v19, v1, v18

    const/16 v18, -0x51d9

    aput v18, v1, v17

    const/16 v17, -0x52

    aput v17, v1, v16

    const/16 v16, 0x687b

    aput v16, v1, v15

    const/16 v15, 0x2568

    aput v15, v1, v14

    const/16 v14, -0x43db

    aput v14, v1, v13

    const/16 v13, -0x44

    aput v13, v1, v12

    const/16 v12, -0x4f

    aput v12, v1, v11

    const/16 v11, -0x1fe9

    aput v11, v1, v10

    const/16 v10, -0x20

    aput v10, v1, v9

    const/16 v9, -0x7ef

    aput v9, v1, v8

    const/4 v8, -0x8

    aput v8, v1, v7

    const/16 v7, -0x40

    aput v7, v1, v6

    const/16 v6, -0x6aa2

    aput v6, v1, v5

    const/16 v5, -0x6b

    aput v5, v1, v4

    const/16 v4, 0x155e

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_5
    array-length v4, v1

    if-lt v3, v4, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_6
    array-length v4, v1

    if-lt v3, v4, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x17

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, -0x14

    aput v26, v2, v25

    const/16 v25, -0x7dfc

    aput v25, v2, v24

    const/16 v24, -0x12

    aput v24, v2, v23

    const/16 v23, -0x3ef8

    aput v23, v2, v22

    const/16 v22, -0x4c

    aput v22, v2, v21

    const/16 v21, -0x5bc1

    aput v21, v2, v20

    const/16 v20, -0x7c

    aput v20, v2, v19

    const/16 v19, -0x77

    aput v19, v2, v18

    const/16 v18, 0x1561

    aput v18, v2, v17

    const/16 v17, -0x51cb

    aput v17, v2, v16

    const/16 v16, -0x24

    aput v16, v2, v15

    const/16 v15, -0x66

    aput v15, v2, v14

    const/16 v14, -0x50c6

    aput v14, v2, v13

    const/16 v13, -0x3f

    aput v13, v2, v12

    const/16 v12, -0x3e

    aput v12, v2, v11

    const/16 v11, -0x5f

    aput v11, v2, v10

    const/16 v10, -0x8a0

    aput v10, v2, v9

    const/16 v9, -0x7d

    aput v9, v2, v8

    const/16 v8, 0x1e07

    aput v8, v2, v7

    const/16 v7, -0x7981

    aput v7, v2, v6

    const/16 v6, -0x1d

    aput v6, v2, v5

    const/16 v5, -0x16

    aput v5, v2, v3

    const/16 v3, -0x4b

    aput v3, v2, v1

    const/16 v1, 0x17

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, -0x33

    aput v27, v1, v26

    const/16 v26, -0x7ddb

    aput v26, v1, v25

    const/16 v25, -0x7e

    aput v25, v1, v24

    const/16 v24, -0x3e9c

    aput v24, v1, v23

    const/16 v23, -0x3f

    aput v23, v1, v22

    const/16 v22, -0x5baf

    aput v22, v1, v21

    const/16 v21, -0x5c

    aput v21, v1, v20

    const/16 v20, -0x6

    aput v20, v1, v19

    const/16 v19, 0x1508

    aput v19, v1, v18

    const/16 v18, -0x51eb

    aput v18, v1, v17

    const/16 v17, -0x52

    aput v17, v1, v16

    const/16 v16, -0xb

    aput v16, v1, v15

    const/16 v15, -0x50b7

    aput v15, v1, v14

    const/16 v14, -0x51

    aput v14, v1, v13

    const/16 v13, -0x59

    aput v13, v1, v12

    const/16 v12, -0xe

    aput v12, v1, v11

    const/16 v11, -0x8f8

    aput v11, v1, v10

    const/16 v10, -0x9

    aput v10, v1, v9

    const/16 v9, 0x1e6b

    aput v9, v1, v8

    const/16 v8, -0x79e2

    aput v8, v1, v7

    const/16 v7, -0x7a

    aput v7, v1, v6

    const/16 v6, -0x5e

    aput v6, v1, v5

    const/16 v5, -0x28

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_8
    array-length v5, v1

    if-lt v3, v5, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_4
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_5
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_6
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_7
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_8
.end method
