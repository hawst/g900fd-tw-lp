.class Lcom/sec/android/app/shealth/settings/UnitSettingActivity$4;
.super Ljava/lang/Object;
.source "UnitSettingActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V
    .locals 0

    .prologue
    .line 489
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSave(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 493
    # getter for: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$600()Lcom/samsung/android/sdk/health/content/ShealthProfile;

    move-result-object v2

    if-nez v2, :cond_0

    .line 494
    new-instance v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$602(Lcom/samsung/android/sdk/health/content/ShealthProfile;)Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 496
    :cond_0
    # getter for: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$600()Lcom/samsung/android/sdk/health/content/ShealthProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->load()V

    .line 498
    # getter for: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$600()Lcom/samsung/android/sdk/health/content/ShealthProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeightUnit()I

    move-result v0

    .line 500
    .local v0, "unit":I
    if-nez p1, :cond_1

    .line 504
    const-string v1, "kg"

    .line 505
    .local v1, "weightUnit":Ljava/lang/String;
    # getter for: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$600()Lcom/samsung/android/sdk/health/content/ShealthProfile;

    move-result-object v2

    const v3, 0x1fbd1

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setWeightUnit(I)V

    .line 519
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$700(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putweightUnit(Ljava/lang/String;)V

    .line 521
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->saveShealthProfile()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$800(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V

    .line 522
    return-void

    .line 516
    .end local v1    # "weightUnit":Ljava/lang/String;
    :cond_1
    const-string v1, "lb"

    .line 517
    .restart local v1    # "weightUnit":Ljava/lang/String;
    # getter for: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$600()Lcom/samsung/android/sdk/health/content/ShealthProfile;

    move-result-object v2

    const v3, 0x1fbd2

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setWeightUnit(I)V

    goto :goto_0
.end method
