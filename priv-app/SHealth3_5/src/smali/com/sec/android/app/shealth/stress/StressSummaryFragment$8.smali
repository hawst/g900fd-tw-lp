.class Lcom/sec/android/app/shealth/stress/StressSummaryFragment$8;
.super Ljava/lang/Object;
.source "StressSummaryFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->measureFinishAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V
    .locals 0

    .prologue
    .line 898
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$8;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const v2, 0x3f0f5c29    # 0.56f

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v11, 0x0

    const/16 v3, 0x8

    const/4 v5, 0x1

    .line 902
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$8;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondIcon:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$500(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 903
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$8;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$000(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 905
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$8;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$200(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/RelativeLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 906
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$8;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconFinish:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$2600(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v1

    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 908
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    move v4, v2

    move v7, v5

    move v8, v6

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 909
    .local v0, "scaleAnimation":Landroid/view/animation/Animation;
    const-wide/16 v1, 0x14d

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 911
    new-instance v4, Landroid/view/animation/TranslateAnimation;

    const v8, 0x3be56042    # 0.007f

    const/high16 v12, -0x41000000    # -0.5f

    move v6, v11

    move v7, v5

    move v9, v5

    move v10, v11

    move v11, v5

    invoke-direct/range {v4 .. v12}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 913
    .local v4, "translateAnimation":Landroid/view/animation/Animation;
    const-wide/16 v1, 0x14d

    invoke-virtual {v4, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 915
    new-instance v13, Landroid/view/animation/AnimationSet;

    invoke-direct {v13, v5}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 916
    .local v13, "animationSet":Landroid/view/animation/AnimationSet;
    invoke-virtual {v13, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 917
    invoke-virtual {v13, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 918
    const/4 v1, 0x0

    invoke-virtual {v13, v1}, Landroid/view/animation/AnimationSet;->setFillAfter(Z)V

    .line 919
    invoke-virtual {v13, v5}, Landroid/view/animation/AnimationSet;->setFillBefore(Z)V

    .line 920
    new-instance v1, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$8$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$8$1;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment$8;)V

    invoke-virtual {v13, v1}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 940
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$8;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconFinish:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$2600(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v13}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 942
    return-void
.end method
