.class public Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelectorImpl;
.super Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;
.source "PageSelectorImpl.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;-><init>(Landroid/content/Context;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 15
    return-void
.end method


# virtual methods
.method protected getMarginBetweenIndicators(Landroid/content/res/Resources;)I
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 34
    const/4 v0, 0x2

    invoke-static {p1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->convertDpToPx(Landroid/content/res/Resources;I)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method protected getPageIndicatorBackgroundSelectorResource()I
    .locals 1

    .prologue
    .line 39
    const v0, 0x7f020266

    return v0
.end method

.method protected getPageIndicatorHeight(Landroid/content/res/Resources;)I
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 29
    const/16 v0, 0xf

    invoke-static {p1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->convertDpToPx(Landroid/content/res/Resources;I)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method protected getPageIndicatorWidth(Landroid/content/res/Resources;)I
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 24
    const/16 v0, 0xf

    invoke-static {p1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->convertDpToPx(Landroid/content/res/Resources;I)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method
