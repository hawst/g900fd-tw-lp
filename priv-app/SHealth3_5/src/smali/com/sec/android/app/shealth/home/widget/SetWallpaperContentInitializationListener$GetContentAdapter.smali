.class Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$GetContentAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SetWallpaperContentInitializationListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "GetContentAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$RequestInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$RequestInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 100
    .local p2, "apps":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$RequestInfo;>;"
    const v0, 0x7f030137

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 101
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v6, 0x7f080525

    const v5, 0x7f080060

    .line 107
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 108
    .local v1, "layoutInflater":Landroid/view/LayoutInflater;
    if-nez p2, :cond_0

    .line 109
    const v3, 0x7f030137

    const/4 v4, 0x0

    invoke-virtual {v1, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 110
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$GetContentAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$RequestInfo;

    .line 111
    .local v2, "requestInfo":Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$RequestInfo;
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, v2, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$RequestInfo;->labelResource:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 112
    .local v0, "labelRes":Ljava/lang/String;
    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    invoke-virtual {p2, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 114
    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iget v4, v2, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$RequestInfo;->iconResource:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 115
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 123
    :goto_0
    return-object p2

    .line 117
    .end local v0    # "labelRes":Ljava/lang/String;
    .end local v2    # "requestInfo":Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$RequestInfo;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$RequestInfo;

    .line 118
    .restart local v2    # "requestInfo":Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$RequestInfo;
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, v2, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$RequestInfo;->labelResource:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 119
    .restart local v0    # "labelRes":Ljava/lang/String;
    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    invoke-virtual {p2, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 121
    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iget v4, v2, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$RequestInfo;->iconResource:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method
