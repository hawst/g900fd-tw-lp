.class public Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity;
.super Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;
.source "ThermoHygrometerActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity$1;,
        Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity$InitialDialogButtonController;
    }
.end annotation


# static fields
.field public static final INITIAL_DIALOG_TAG:Ljava/lang/String; = "first"


# instance fields
.field private mSa:Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;-><init>()V

    .line 82
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity;)Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity;->mSa:Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;

    return-object v0
.end method

.method private getExtrasForConnectivityActivity()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 130
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 136
    .local v0, "bundle":Landroid/os/Bundle;
    return-object v0
.end method

.method private showInitialDialog()Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;-><init>(Landroid/content/Context;)V

    .line 67
    .local v0, "sa":Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;->show()V

    .line 69
    return-object v0
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f090242

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 119
    return-void
.end method

.method protected getActionBarTitleId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090242

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 2
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 75
    const-string v0, "first"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity$InitialDialogButtonController;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity$InitialDialogButtonController;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity;Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity$1;)V

    .line 78
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method protected getGraphFragmentClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 146
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getHelpItem()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    const-string v0, "com.sec.shealth.help.action.TGH"

    return-object v0
.end method

.method protected getSummaryFragmentClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    const-class v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;

    return-object v0
.end method

.method public launchSettingsActivity()V
    .locals 5

    .prologue
    .line 160
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 161
    .local v1, "intent1":Landroid/content/Intent;
    const-string v2, "com.sec.android.app.shealth"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 162
    const-string v2, "android.shealth.action.LAUNCH_SETTINGS"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 163
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    .end local v1    # "intent1":Landroid/content/Intent;
    :goto_0
    return-void

    .line 164
    :catch_0
    move-exception v0

    .line 165
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "Application is not available on your device !!!"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected onLogSelected()V
    .locals 0

    .prologue
    .line 127
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 53
    invoke-super {p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->onStart()V

    .line 54
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isModeComfortZoneInited()Z

    move-result v0

    if-nez v0, :cond_0

    .line 55
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setIsModeComfortZoneInited(Z)V

    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity;->showInitialDialog()Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity;->mSa:Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;

    .line 58
    :cond_0
    return-void
.end method

.method protected startConnectivityActivity()V
    .locals 0

    .prologue
    .line 44
    return-void
.end method
