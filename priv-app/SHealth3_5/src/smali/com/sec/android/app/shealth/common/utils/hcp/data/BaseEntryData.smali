.class public abstract Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
.super Ljava/lang/Object;
.source "BaseEntryData.java"


# static fields
.field public static final EMPTY:I = -0x1

.field public static final EMPTY_STRING:Ljava/lang/String; = ""

.field protected static final FALSE:I = 0x0

.field protected static final TRUE:I = 0x1


# instance fields
.field private mCreateTime:J

.field private mDaylightSaving:I

.field private mId:J

.field private mTimeZone:I

.field private mUpdateTime:J


# direct methods
.method protected constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const-wide/16 v0, -0x1

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mId:J

    .line 38
    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mCreateTime:J

    .line 39
    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mUpdateTime:J

    .line 40
    iput v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mTimeZone:I

    .line 41
    iput v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mDaylightSaving:I

    .line 51
    return-void
.end method

.method protected constructor <init>(J)V
    .locals 3
    .param p1, "id"    # J

    .prologue
    const/4 v2, -0x1

    const-wide/16 v0, -0x1

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mId:J

    .line 38
    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mCreateTime:J

    .line 39
    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mUpdateTime:J

    .line 40
    iput v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mTimeZone:I

    .line 41
    iput v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mDaylightSaving:I

    .line 57
    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 58
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "id should be DbConstants.NOT_INSERTED_CODE or >= 0, now id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_0
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mId:J

    .line 62
    return-void
.end method

.method protected constructor <init>(JJJI)V
    .locals 3
    .param p1, "id"    # J
    .param p3, "createTime"    # J
    .param p5, "updateTime"    # J
    .param p7, "timeZone"    # I

    .prologue
    const/4 v2, -0x1

    const-wide/16 v0, -0x1

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mId:J

    .line 38
    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mCreateTime:J

    .line 39
    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mUpdateTime:J

    .line 40
    iput v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mTimeZone:I

    .line 41
    iput v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mDaylightSaving:I

    .line 95
    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 96
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "id should be DbConstants.DATA_NOT_INSERTED_CODE or >= 0, now id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 99
    :cond_0
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mId:J

    .line 100
    iput-wide p3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mCreateTime:J

    .line 101
    iput-wide p5, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mUpdateTime:J

    .line 102
    iput p7, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mTimeZone:I

    .line 103
    return-void
.end method

.method protected constructor <init>(JJJII)V
    .locals 3
    .param p1, "id"    # J
    .param p3, "createTime"    # J
    .param p5, "updateTime"    # J
    .param p7, "timeZone"    # I
    .param p8, "daylightSaving"    # I

    .prologue
    .line 75
    invoke-direct/range {p0 .. p7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;-><init>(JJJI)V

    .line 76
    const v0, 0x445c2

    if-eq p8, v0, :cond_0

    const v0, 0x445c1

    if-eq p8, v0, :cond_0

    const/4 v0, -0x1

    if-eq p8, v0, :cond_0

    .line 79
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "See ShealthContract.Constants.DayLightSaving constants for possible values. Your: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_0
    iput p8, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mDaylightSaving:I

    .line 83
    return-void
.end method


# virtual methods
.method public getCreateTime()J
    .locals 2

    .prologue
    .line 122
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mCreateTime:J

    return-wide v0
.end method

.method public getDaylightSaving()I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mDaylightSaving:I

    return v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 107
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mId:J

    return-wide v0
.end method

.method public getTimeZone()I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mTimeZone:I

    return v0
.end method

.method public getUpdateTime()J
    .locals 2

    .prologue
    .line 139
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mUpdateTime:J

    return-wide v0
.end method

.method public setCreateTime(J)V
    .locals 0
    .param p1, "createTime"    # J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 134
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mCreateTime:J

    .line 135
    return-void
.end method

.method public setId(J)V
    .locals 0
    .param p1, "id"    # J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 117
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mId:J

    .line 118
    return-void
.end method

.method public setTimeZone(I)V
    .locals 0
    .param p1, "timeZone"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 166
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mTimeZone:I

    .line 167
    return-void
.end method

.method public setUpdateTime(J)V
    .locals 0
    .param p1, "updateTime"    # J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 149
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->mUpdateTime:J

    .line 150
    return-void
.end method
