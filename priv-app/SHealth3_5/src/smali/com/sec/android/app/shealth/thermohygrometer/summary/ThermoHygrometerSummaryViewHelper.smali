.class public Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryViewHelper;
.super Ljava/lang/Object;
.source "ThermoHygrometerSummaryViewHelper.java"


# instance fields
.field private mDateTV:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerDateView;

.field private mSummaryView:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerSummaryView;


# direct methods
.method constructor <init>(Landroid/view/View;Landroid/content/Context;)V
    .locals 1
    .param p1, "container"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const v0, 0x7f080a8a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerSummaryView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryViewHelper;->mSummaryView:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerSummaryView;

    .line 45
    const v0, 0x7f080a8b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerDateView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryViewHelper;->mDateTV:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerDateView;

    .line 46
    return-void
.end method

.method public static getBalanceState(FFZZ)Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;
    .locals 5
    .param p0, "temperature"    # F
    .param p1, "humidity"    # F
    .param p2, "temperatureCompensating"    # Z
    .param p3, "humidityCompensating"    # Z

    .prologue
    .line 117
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getTemperatureMaximal()F

    move-result v2

    .line 118
    .local v2, "tempMax":F
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getTemperatureMinimal()F

    move-result v3

    .line 119
    .local v3, "tempMin":F
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getHumidityMaximal()F

    move-result v0

    .line 120
    .local v0, "humidityMax":F
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getHumidityMinimal()F

    move-result v1

    .line 122
    .local v1, "humidityMin":F
    if-eqz p2, :cond_1

    .line 123
    if-eqz p3, :cond_0

    .line 124
    sget-object v4, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->CHECKING_TEMPERATURE_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    .line 154
    :goto_0
    return-object v4

    .line 126
    :cond_0
    sget-object v4, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->CHECKING_TEMPERATURE:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    goto :goto_0

    .line 128
    :cond_1
    if-eqz p3, :cond_2

    .line 129
    sget-object v4, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->CHECKING_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    goto :goto_0

    .line 132
    :cond_2
    cmpg-float v4, p1, v0

    if-gtz v4, :cond_5

    cmpl-float v4, p1, v1

    if-ltz v4, :cond_5

    .line 133
    cmpg-float v4, p0, v2

    if-gtz v4, :cond_3

    cmpl-float v4, p0, v3

    if-ltz v4, :cond_3

    .line 134
    sget-object v4, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->NORMAL_TEMPERATURE_NORMAL_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    goto :goto_0

    .line 135
    :cond_3
    cmpl-float v4, p0, v2

    if-lez v4, :cond_4

    .line 136
    sget-object v4, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->HIGH_TEMPERATURE_NORMAL_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    goto :goto_0

    .line 138
    :cond_4
    sget-object v4, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->LOW_TEMPERATURE_NORMAL_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    goto :goto_0

    .line 140
    :cond_5
    cmpg-float v4, p1, v1

    if-gez v4, :cond_8

    .line 141
    cmpg-float v4, p0, v2

    if-gtz v4, :cond_6

    cmpl-float v4, p0, v3

    if-ltz v4, :cond_6

    .line 142
    sget-object v4, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->NORMAL_TEMPERATURE_LOW_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    goto :goto_0

    .line 143
    :cond_6
    cmpl-float v4, p0, v2

    if-lez v4, :cond_7

    .line 144
    sget-object v4, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->HIGH_TEMPERATURE_LOW_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    goto :goto_0

    .line 146
    :cond_7
    sget-object v4, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->LOW_TEMPERATURE_LOW_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    goto :goto_0

    .line 149
    :cond_8
    cmpg-float v4, p0, v2

    if-gtz v4, :cond_9

    cmpl-float v4, p0, v3

    if-ltz v4, :cond_9

    .line 150
    sget-object v4, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->NORMAL_TEMPERATURE_HIGH_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    goto :goto_0

    .line 151
    :cond_9
    cmpl-float v4, p0, v2

    if-lez v4, :cond_a

    .line 152
    sget-object v4, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->HIGH_TEMPERATURE_HIGH_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    goto :goto_0

    .line 154
    :cond_a
    sget-object v4, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->LOW_TEMPERATURE_HIGH_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    goto :goto_0
.end method

.method private setDateValue(J)V
    .locals 2
    .param p1, "value"    # J

    .prologue
    .line 62
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryViewHelper;->mDateTV:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerDateView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerDateView;->resetView()V

    .line 67
    :goto_0
    return-void

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryViewHelper;->mDateTV:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerDateView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerDateView;->setValue(J)V

    goto :goto_0
.end method


# virtual methods
.method public setViewValues(FFJZZZZ)V
    .locals 8
    .param p1, "temperature"    # F
    .param p2, "humidity"    # F
    .param p3, "sampleTime"    # J
    .param p5, "temperatureCompensating"    # Z
    .param p6, "humidityCompensating"    # Z
    .param p7, "animationflagtemp"    # Z
    .param p8, "animationflaghumid"    # Z

    .prologue
    .line 77
    invoke-static {p1, p2, p5, p6}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryViewHelper;->getBalanceState(FFZZ)Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    move-result-object v3

    .line 82
    .local v3, "balanceState":Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryViewHelper;->mSummaryView:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerSummaryView;

    if-nez p5, :cond_1

    const/4 v4, 0x1

    :goto_0
    if-nez p6, :cond_2

    const/4 v5, 0x1

    :goto_1
    if-nez p7, :cond_3

    const/4 v6, 0x1

    :goto_2
    if-nez p8, :cond_4

    const/4 v7, 0x1

    :goto_3
    move v1, p1

    move v2, p2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerSummaryView;->updateView(FFLcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;ZZZZ)V

    .line 84
    if-nez p5, :cond_0

    if-nez p6, :cond_0

    .line 85
    invoke-direct {p0, p3, p4}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryViewHelper;->setDateValue(J)V

    .line 87
    :cond_0
    return-void

    .line 82
    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    :cond_2
    const/4 v5, 0x0

    goto :goto_1

    :cond_3
    const/4 v6, 0x0

    goto :goto_2

    :cond_4
    const/4 v7, 0x0

    goto :goto_3
.end method
