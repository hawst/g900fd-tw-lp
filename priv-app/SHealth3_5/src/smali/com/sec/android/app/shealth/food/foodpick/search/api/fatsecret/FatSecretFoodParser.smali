.class public Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;
.super Lcom/sec/android/app/shealth/food/foodpick/search/api/base/XmlFoodParser;
.source "FatSecretFoodParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$1;,
        Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;
    }
.end annotation


# static fields
.field private static final BRAND:Ljava/lang/String; = "Brand"

.field private static final BRAND_NAME:Ljava/lang/String; = "brand_name"

.field private static final CALCIUM:Ljava/lang/String; = "calcium"

.field private static final CALORIES:Ljava/lang/String; = "calories"

.field private static final CALORIES_END_STRING:Ljava/lang/String; = ": "

.field private static final CARBOHYDRATE:Ljava/lang/String; = "carbohydrate"

.field private static final CHOLESTEROL:Ljava/lang/String; = "cholesterol"

.field private static final DEFAULT_TRUE_VALUE:Ljava/lang/String; = "1"

.field private static final FAT:Ljava/lang/String; = "fat"

.field private static final FATSECRET_FOOD_DIVIDER:Ljava/lang/String; = "|"

.field private static final FIBER:Ljava/lang/String; = "fiber"

.field private static final FOOD:Ljava/lang/String; = "food"

.field private static final FOOD_CATEGORY:Ljava/lang/String; = "food_category"

.field private static final FOOD_CATEGORY_DESCRIPTION:Ljava/lang/String; = "food_category_description"

.field private static final FOOD_CATEGORY_ID:Ljava/lang/String; = "food_category_id"

.field private static final FOOD_CATEGORY_NAME:Ljava/lang/String; = "food_category_name"

.field private static final FOOD_DESCRIPTION:Ljava/lang/String; = "food_description"

.field private static final FOOD_ID:Ljava/lang/String; = "food_id"

.field private static final FOOD_NAME:Ljava/lang/String; = "food_name"

.field private static final FOOD_SUB_CATEGORIES:Ljava/lang/String; = "food_sub_categories"

.field private static final FOOD_SUB_CATEGORY:Ljava/lang/String; = "food_sub_category"

.field private static final FOOD_TYPE:Ljava/lang/String; = "food_type"

.field private static final GRAM_UNIT:Ljava/lang/String; = "gram"

.field private static final G_UNIT:Ljava/lang/String; = "g"

.field private static final IRON:Ljava/lang/String; = "iron"

.field private static final IS_DEFAULT:Ljava/lang/String; = "is_default"

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final MEASUREMENT_DESCRIPTION:Ljava/lang/String; = "measurement_description"

.field private static final METRIC_SERVING_AMOUNT:Ljava/lang/String; = "metric_serving_amount"

.field private static final METRIC_SERVING_UNIT:Ljava/lang/String; = "metric_serving_unit"

.field private static final MONOUNSATURATED_FAT:Ljava/lang/String; = "monounsaturated_fat"

.field private static final NO_FOOD_IDENTIFIER:Ljava/lang/String; = "0"

.field private static final NUMBER_OF_UNITS:Ljava/lang/String; = "number_of_units"

.field private static final OZ_UNIT:Ljava/lang/String; = "oz"

.field private static final POLYUNSATURATED_FAT:Ljava/lang/String; = "polyunsaturated_fat"

.field private static final POTASSIUM:Ljava/lang/String; = "potassium"

.field private static final PROTEIN:Ljava/lang/String; = "protein"

.field private static final SATURATED_FAT:Ljava/lang/String; = "saturated_fat"

.field private static final SERVING:Ljava/lang/String; = "serving"

.field private static final SERVINGS:Ljava/lang/String; = "servings"

.field private static final SERVING_DESCRIPTION:Ljava/lang/String; = "serving_description"

.field private static final SODIUM:Ljava/lang/String; = "sodium"

.field private static final SPACE:Ljava/lang/String; = " "

.field private static final SUB_CATEGORY_SEPARATOR:Ljava/lang/String; = ";"

.field private static final SUGAR:Ljava/lang/String; = "sugar"

.field private static final SUGGESTION_TAG_NAME:Ljava/lang/String; = "suggestion"

.field private static final TOTAL_RESULTS:Ljava/lang/String; = "total_results"

.field private static final TRANS_FAT:Ljava/lang/String; = "trans_fat"

.field private static final VITAMIN_A:Ljava/lang/String; = "vitamin_a"

.field private static final VITAMIN_C:Ljava/lang/String; = "vitamin_c"


# instance fields
.field private mDigitSearchPattern:Ljava/util/regex/Pattern;

.field private final mGUnitLocalized:Ljava/lang/String;

.field private final mOzUnitLocalized:Ljava/lang/String;

.field private mResources:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-class v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 115
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/XmlFoodParser;-><init>()V

    .line 116
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->mResources:Landroid/content/res/Resources;

    .line 117
    const-string v0, "\\d+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->mDigitSearchPattern:Ljava/util/regex/Pattern;

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0900bf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->mGUnitLocalized:Ljava/lang/String;

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0900c5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->mOzUnitLocalized:Ljava/lang/String;

    .line 120
    return-void
.end method

.method private checkForUnitInfo(Lorg/w3c/dom/Element;Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)V
    .locals 7
    .param p1, "servingElement"    # Lorg/w3c/dom/Element;
    .param p2, "xmlTag"    # Ljava/lang/String;
    .param p3, "unitHolder"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;

    .prologue
    const/4 v6, 0x0

    .line 487
    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->valueChanged()Z
    invoke-static {p3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->access$200(Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 488
    invoke-interface {p1, p2}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v5

    invoke-interface {v5, v6}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v5

    invoke-interface {v5}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v5

    invoke-interface {v5, v6}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v5

    invoke-interface {v5}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v1

    .line 490
    .local v1, "description":Ljava/lang/String;
    iget-object v0, p3, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->unitNames:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 491
    .local v4, "unitName":Ljava/lang/String;
    invoke-direct {p0, v1, p3, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->updateUnitValueInUnitHolder(Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 495
    .end local v4    # "unitName":Ljava/lang/String;
    :cond_0
    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->valueChanged()Z
    invoke-static {p3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->access$200(Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 496
    invoke-direct {p0, p1, p3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->normalizeUnits(Lorg/w3c/dom/Element;Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)V

    .line 499
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "description":Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_1
    return-void

    .line 490
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v1    # "description":Ljava/lang/String;
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    .restart local v4    # "unitName":Ljava/lang/String;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private checkUnitInformation(Lorg/w3c/dom/NodeList;Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)V
    .locals 3
    .param p1, "servingsNodeList"    # Lorg/w3c/dom/NodeList;
    .param p2, "gramInfo"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;
    .param p3, "ozInfo"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;

    .prologue
    .line 273
    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->valueChanged()Z
    invoke-static {p2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->access$200(Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)Z

    move-result v2

    if-eqz v2, :cond_0

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->valueChanged()Z
    invoke-static {p3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->access$200(Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 274
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 275
    invoke-interface {p1, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v1

    check-cast v1, Lorg/w3c/dom/Element;

    .line 276
    .local v1, "servingElement":Lorg/w3c/dom/Element;
    const-string/jumbo v2, "serving_description"

    invoke-direct {p0, v1, v2, p2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->checkForUnitInfo(Lorg/w3c/dom/Element;Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)V

    .line 277
    const-string/jumbo v2, "measurement_description"

    invoke-direct {p0, v1, v2, p2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->checkForUnitInfo(Lorg/w3c/dom/Element;Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)V

    .line 278
    const-string/jumbo v2, "serving_description"

    invoke-direct {p0, v1, v2, p3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->checkForUnitInfo(Lorg/w3c/dom/Element;Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)V

    .line 279
    const-string/jumbo v2, "measurement_description"

    invoke-direct {p0, v1, v2, p3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->checkForUnitInfo(Lorg/w3c/dom/Element;Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)V

    .line 280
    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->valueChanged()Z
    invoke-static {p2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->access$200(Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)Z

    move-result v2

    if-eqz v2, :cond_2

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->valueChanged()Z
    invoke-static {p3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->access$200(Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 285
    .end local v0    # "i":I
    .end local v1    # "servingElement":Lorg/w3c/dom/Element;
    :cond_1
    return-void

    .line 274
    .restart local v0    # "i":I
    .restart local v1    # "servingElement":Lorg/w3c/dom/Element;
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private getCategoryIdStringBuilder(Lorg/w3c/dom/Element;)Ljava/lang/StringBuilder;
    .locals 7
    .param p1, "foodNode"    # Lorg/w3c/dom/Element;

    .prologue
    .line 387
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ""

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 388
    .local v1, "categoryIdBuilder":Ljava/lang/StringBuilder;
    const-string v5, "food_sub_categories"

    invoke-interface {p1, v5}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v4

    check-cast v4, Lorg/w3c/dom/Element;

    .line 389
    .local v4, "subcategoriesNode":Lorg/w3c/dom/Element;
    if-eqz v4, :cond_0

    .line 390
    const-string v5, "food_sub_category"

    invoke-interface {v4, v5}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v3

    .line 391
    .local v3, "subCategoriesList":Lorg/w3c/dom/NodeList;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v5

    if-ge v2, v5, :cond_0

    .line 392
    invoke-interface {v3, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    .line 393
    .local v0, "categoryElement":Lorg/w3c/dom/Element;
    invoke-interface {v0}, Lorg/w3c/dom/Element;->getTextContent()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ";"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 391
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 396
    .end local v0    # "categoryElement":Lorg/w3c/dom/Element;
    .end local v2    # "i":I
    .end local v3    # "subCategoriesList":Lorg/w3c/dom/NodeList;
    :cond_0
    return-object v1
.end method

.method private getDefaultServingElement(Lorg/w3c/dom/Element;Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;)Lorg/w3c/dom/Element;
    .locals 11
    .param p1, "servingsNode"    # Lorg/w3c/dom/Element;
    .param p2, "result"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 215
    const-string/jumbo v5, "serving"

    invoke-interface {p1, v5}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v4

    .line 216
    .local v4, "servingsNodeList":Lorg/w3c/dom/NodeList;
    invoke-interface {v4, v7}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    .line 217
    .local v0, "defaultServingElement":Lorg/w3c/dom/Element;
    new-instance v2, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const-string v6, "g"

    aput-object v6, v5, v7

    const-string v6, "gram"

    aput-object v6, v5, v8

    iget-object v6, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->mGUnitLocalized:Ljava/lang/String;

    aput-object v6, v5, v9

    invoke-direct {v2, v5, v10}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;-><init>([Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$1;)V

    .line 218
    .local v2, "gramInfo":Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;
    new-instance v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;

    new-array v5, v9, [Ljava/lang/String;

    const-string/jumbo v6, "oz"

    aput-object v6, v5, v7

    iget-object v6, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->mOzUnitLocalized:Ljava/lang/String;

    aput-object v6, v5, v8

    invoke-direct {v3, v5, v10}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;-><init>([Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$1;)V

    .line 220
    .local v3, "ozInfo":Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;
    :try_start_0
    invoke-direct {p0, v4, v0, v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->updateDefaultServingAndNormalizedUnits(Lorg/w3c/dom/NodeList;Lorg/w3c/dom/Element;Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 222
    invoke-direct {p0, v4, v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->checkUnitInformation(Lorg/w3c/dom/NodeList;Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 226
    :goto_0
    if-nez v0, :cond_0

    .line 227
    new-instance v5, Lorg/apache/http/ParseException;

    const-string v6, "defaultServingElement not found!"

    invoke-direct {v5, v6}, Lorg/apache/http/ParseException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 223
    :catch_0
    move-exception v1

    .line 224
    .local v1, "ex":Ljava/lang/Exception;
    sget-object v5, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->LOG_TAG:Ljava/lang/String;

    invoke-static {v5, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 230
    .end local v1    # "ex":Ljava/lang/Exception;
    :cond_0
    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->value:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->access$100(Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)F

    move-result v5

    invoke-virtual {p2, v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setGrammInKcal(F)V

    .line 231
    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->value:F
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->access$100(Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)F

    move-result v5

    invoke-virtual {p2, v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setOzInKcal(F)V

    .line 232
    return-object v0
.end method

.method private getFoodServingDescription(Lorg/w3c/dom/Element;)Ljava/lang/String;
    .locals 5
    .param p1, "defaultServingElement"    # Lorg/w3c/dom/Element;

    .prologue
    .line 328
    const-string/jumbo v3, "measurement_description"

    invoke-static {p1, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 329
    .local v0, "desc":Ljava/lang/String;
    const-string v3, "[\n\r\u0000]"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 333
    const-string v3, "gram"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 334
    const-string v0, "g"

    .line 341
    :cond_0
    :try_start_0
    const-string v3, "UTF-8"

    invoke-static {v0, v3}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 346
    .local v1, "description":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 342
    .end local v1    # "description":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 343
    .local v2, "e":Ljava/io/UnsupportedEncodingException;
    sget-object v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->LOG_TAG:Ljava/lang/String;

    const-string v4, "Something strange! UTF-8 not supported"

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/Logging;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    const-string v1, ""

    .restart local v1    # "description":Ljava/lang/String;
    goto :goto_0
.end method

.method private getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 547
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getUnit(Ljava/lang/String;Landroid/content/res/Resources;I)F
    .locals 7
    .param p0, "unitInfo"    # Ljava/lang/String;
    .param p1, "resources"    # Landroid/content/res/Resources;
    .param p2, "unitId"    # I

    .prologue
    .line 531
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 532
    .local v2, "unit":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\\d+\\.?\\d*"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    invoke-static {v4, v5}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 533
    .local v1, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 535
    .local v0, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 536
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v3

    .line 537
    .local v3, "unitString":Ljava/lang/String;
    const/4 v4, 0x0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseFloat(Ljava/lang/String;)F

    move-result v4

    .line 542
    .end local v3    # "unitString":Ljava/lang/String;
    :goto_0
    return v4

    .line 540
    :cond_0
    sget-object v4, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "can\'t parse unit \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' for unit info: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private normalizeUnits(Lorg/w3c/dom/Element;Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)V
    .locals 2
    .param p1, "servingElement"    # Lorg/w3c/dom/Element;
    .param p2, "unitHolder"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;

    .prologue
    .line 478
    const-string v1, "calories"

    invoke-static {p1, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseFloat(Ljava/lang/String;)F

    move-result v0

    .line 479
    .local v0, "calories":F
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    .line 480
    # /= operator for: Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->value:F
    invoke-static {p2, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->access$140(Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;F)F

    .line 484
    :goto_0
    return-void

    .line 482
    :cond_0
    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->resetValue()V
    invoke-static {p2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->access$300(Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)V

    goto :goto_0
.end method

.method private parseChemicalsInfo(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/w3c/dom/Element;)V
    .locals 12
    .param p1, "result"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .param p2, "defaultServingElement"    # Lorg/w3c/dom/Element;

    .prologue
    .line 351
    const-string v11, "carbohydrate"

    invoke-static {p2, v11}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 352
    .local v1, "carbohydrate":Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseFloat(Ljava/lang/String;)F

    move-result v11

    invoke-virtual {p1, v11}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setCarbohydrate(F)V

    .line 354
    const-string v11, "fiber"

    invoke-static {p2, v11}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 355
    .local v3, "dietaryFiber":Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseFloat(Ljava/lang/String;)F

    move-result v11

    invoke-virtual {p1, v11}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setDietary(F)V

    .line 357
    const-string/jumbo v11, "sugar"

    invoke-static {p2, v11}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 358
    .local v8, "sugar":Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseFloat(Ljava/lang/String;)F

    move-result v11

    invoke-virtual {p1, v11}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setSugar(F)V

    .line 360
    const-string/jumbo v11, "protein"

    invoke-static {p2, v11}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 361
    .local v6, "protein":Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseFloat(Ljava/lang/String;)F

    move-result v11

    invoke-virtual {p1, v11}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setProtein(F)V

    .line 363
    const-string v11, "cholesterol"

    invoke-static {p2, v11}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 364
    .local v2, "cholesterol":Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseInt(Ljava/lang/String;)I

    move-result v11

    invoke-virtual {p1, v11}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setCholesterol(I)V

    .line 366
    const-string/jumbo v11, "sodium"

    invoke-static {p2, v11}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 367
    .local v7, "sodium":Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseInt(Ljava/lang/String;)I

    move-result v11

    invoke-virtual {p1, v11}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setSodium(I)V

    .line 369
    const-string/jumbo v11, "potassium"

    invoke-static {p2, v11}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 370
    .local v5, "potassium":Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseInt(Ljava/lang/String;)I

    move-result v11

    invoke-virtual {p1, v11}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setPotassium(I)V

    .line 372
    const-string/jumbo v11, "vitamin_a"

    invoke-static {p2, v11}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 373
    .local v9, "vitaminA":Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseInt(Ljava/lang/String;)I

    move-result v11

    invoke-virtual {p1, v11}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setVitaminA(I)V

    .line 375
    const-string/jumbo v11, "vitamin_c"

    invoke-static {p2, v11}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 376
    .local v10, "vitaminC":Ljava/lang/String;
    invoke-static {v10}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseInt(Ljava/lang/String;)I

    move-result v11

    invoke-virtual {p1, v11}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setVitaminC(I)V

    .line 378
    const-string v11, "calcium"

    invoke-static {p2, v11}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 379
    .local v0, "calcium":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseInt(Ljava/lang/String;)I

    move-result v11

    invoke-virtual {p1, v11}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setCalcium(I)V

    .line 381
    const-string v11, "iron"

    invoke-static {p2, v11}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 382
    .local v4, "iron":Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseInt(Ljava/lang/String;)I

    move-result v11

    invoke-virtual {p1, v11}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setIron(I)V

    .line 383
    return-void
.end method

.method private parseExtraFoodInfoParams(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/w3c/dom/Element;)V
    .locals 0
    .param p1, "result"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .param p2, "defaultServingElement"    # Lorg/w3c/dom/Element;

    .prologue
    .line 288
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseFatRelatedInfo(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/w3c/dom/Element;)V

    .line 289
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseMetricsRelatedInfo(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/w3c/dom/Element;)V

    .line 290
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseChemicalsInfo(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/w3c/dom/Element;)V

    .line 291
    return-void
.end method

.method private parseFatRelatedInfo(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/w3c/dom/Element;)V
    .locals 6
    .param p1, "result"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .param p2, "defaultServingElement"    # Lorg/w3c/dom/Element;

    .prologue
    .line 295
    const-string v5, "fat"

    invoke-static {p2, v5}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 296
    .local v0, "fatInfo":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseFloat(Ljava/lang/String;)F

    move-result v5

    invoke-virtual {p1, v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setFat(F)V

    .line 298
    const-string/jumbo v5, "saturated_fat"

    invoke-static {p2, v5}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 299
    .local v3, "saturatedInfo":Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseFloat(Ljava/lang/String;)F

    move-result v5

    invoke-virtual {p1, v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setSaturated(F)V

    .line 301
    const-string/jumbo v5, "polyunsaturated_fat"

    invoke-static {p2, v5}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 302
    .local v2, "polyunsaturatedFat":Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseFloat(Ljava/lang/String;)F

    move-result v5

    invoke-virtual {p1, v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setPolysaturated(F)V

    .line 304
    const-string/jumbo v5, "monounsaturated_fat"

    invoke-static {p2, v5}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 305
    .local v1, "monounsaturatedFat":Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseFloat(Ljava/lang/String;)F

    move-result v5

    invoke-virtual {p1, v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setMonosaturated(F)V

    .line 307
    const-string/jumbo v5, "trans_fat"

    invoke-static {p2, v5}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 308
    .local v4, "transFat":Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseFloat(Ljava/lang/String;)F

    move-result v5

    invoke-virtual {p1, v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setTransFat(F)V

    .line 309
    return-void
.end method

.method private parseFoodInfoElement(Lorg/w3c/dom/Element;)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .locals 19
    .param p1, "foodElement"    # Lorg/w3c/dom/Element;

    .prologue
    .line 434
    new-instance v8, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-direct {v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;-><init>()V

    .line 437
    .local v8, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    const-string v17, "food_id"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 438
    .local v6, "fatSecretId":J
    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setServerId(Ljava/lang/String;)V

    .line 441
    const-string v17, "food_name"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 442
    .local v9, "foodName":Ljava/lang/String;
    const-string v17, "food_type"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 443
    .local v14, "type":Ljava/lang/String;
    const-string v17, "Brand"

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 444
    const-string v17, "brand_name"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 445
    .local v2, "brandName":Ljava/lang/String;
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "("

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ")"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 447
    .end local v2    # "brandName":Ljava/lang/String;
    :cond_0
    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setName(Ljava/lang/String;)V

    .line 448
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getLanguage()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setServerLocale(Ljava/lang/String;)V

    .line 451
    const-string v17, "food_description"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 452
    .local v5, "fatSecretDescription":Ljava/lang/String;
    new-instance v13, Ljava/util/StringTokenizer;

    const-string/jumbo v17, "|"

    move-object/from16 v0, v17

    invoke-direct {v13, v5, v0}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    .local v13, "tokenizer":Ljava/util/StringTokenizer;
    invoke-virtual {v13}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v17

    if-eqz v17, :cond_3

    .line 454
    invoke-virtual {v13}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v15

    .line 455
    .local v15, "unitInfo":Ljava/lang/String;
    const-string v17, " - "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v16

    .line 456
    .local v16, "units":[Ljava/lang/String;
    if-eqz v16, :cond_2

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_2

    .line 458
    const/16 v17, 0x1

    aget-object v17, v16, v17

    const-string v18, ": "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v17

    const-string v18, ": "

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v18

    add-int v3, v17, v18

    .line 459
    .local v3, "colonPosition":I
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v18, 0x1

    aget-object v18, v16, v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ", "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const/16 v18, 0x0

    aget-object v18, v16, v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 461
    .local v4, "desc":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->mDigitSearchPattern:Ljava/util/regex/Pattern;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v12

    .line 462
    .local v12, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v12}, Ljava/util/regex/Matcher;->find()Z

    move-result v17

    if-eqz v17, :cond_1

    .line 463
    invoke-virtual {v12}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v10

    .line 464
    .local v10, "group":Ljava/lang/String;
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v4, v10, v0}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 466
    .end local v10    # "group":Ljava/lang/String;
    :cond_1
    invoke-virtual {v8, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setDescription(Ljava/lang/String;)V

    .line 468
    .end local v3    # "colonPosition":I
    .end local v4    # "desc":Ljava/lang/String;
    .end local v12    # "matcher":Ljava/util/regex/Matcher;
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->mResources:Landroid/content/res/Resources;

    move-object/from16 v17, v0

    const v18, 0x7f0900b9

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v15, v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getUnit(Ljava/lang/String;Landroid/content/res/Resources;I)F

    move-result v11

    .line 469
    .local v11, "kcal":F
    const/16 v17, 0x0

    cmpl-float v17, v11, v17

    if-lez v17, :cond_3

    .line 470
    invoke-virtual {v8, v11}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setkCal(F)V

    .line 473
    .end local v11    # "kcal":F
    .end local v15    # "unitInfo":Ljava/lang/String;
    .end local v16    # "units":[Ljava/lang/String;
    :cond_3
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->notifyFoodParsingComplete(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    .line 474
    return-object v8
.end method

.method private parseMetricsRelatedInfo(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/w3c/dom/Element;)V
    .locals 5
    .param p1, "result"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .param p2, "defaultServingElement"    # Lorg/w3c/dom/Element;

    .prologue
    .line 313
    const-string/jumbo v4, "metric_serving_unit"

    invoke-static {p2, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 314
    .local v3, "unit":Ljava/lang/String;
    invoke-virtual {p1, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setUnitName(Ljava/lang/String;)V

    .line 316
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getFoodServingDescription(Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v1

    .line 317
    .local v1, "description":Ljava/lang/String;
    invoke-virtual {p1, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setServingDescription(Ljava/lang/String;)V

    .line 319
    const-string/jumbo v4, "number_of_units"

    invoke-static {p2, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 320
    .local v2, "numberOfUnits":Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseFloat(Ljava/lang/String;)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setUnit(I)V

    .line 322
    const-string/jumbo v4, "metric_serving_amount"

    invoke-static {p2, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 323
    .local v0, "defaultNumber":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseFloat(Ljava/lang/String;)F

    move-result v4

    invoke-virtual {p1, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setDefaultNumber(F)V

    .line 324
    return-void
.end method

.method private updateDefaultServingAndNormalizedUnits(Lorg/w3c/dom/NodeList;Lorg/w3c/dom/Element;Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)Lorg/w3c/dom/Element;
    .locals 9
    .param p1, "servingsNodeList"    # Lorg/w3c/dom/NodeList;
    .param p2, "defaultServingElement"    # Lorg/w3c/dom/Element;
    .param p3, "gramInfo"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;
    .param p4, "ozInfo"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;

    .prologue
    const/4 v8, 0x0

    .line 236
    const/4 v0, 0x0

    .line 237
    .local v0, "defaultServingWasFound":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v6

    if-ge v1, v6, :cond_7

    .line 238
    invoke-interface {p1, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v3

    check-cast v3, Lorg/w3c/dom/Element;

    .line 239
    .local v3, "servingElement":Lorg/w3c/dom/Element;
    if-nez v0, :cond_0

    .line 240
    const-string v6, "is_default"

    invoke-interface {v3, v6}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v2

    .line 241
    .local v2, "isDefaultNodeList":Lorg/w3c/dom/NodeList;
    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v6

    if-lez v6, :cond_0

    invoke-interface {v2, v8}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    invoke-interface {v6}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v6

    invoke-interface {v6, v8}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    invoke-interface {v6}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v6

    const-string v7, "1"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 243
    move-object p2, v3

    .line 244
    const/4 v0, 0x1

    .line 247
    .end local v2    # "isDefaultNodeList":Lorg/w3c/dom/NodeList;
    :cond_0
    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->valueChanged()Z
    invoke-static {p3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->access$200(Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)Z

    move-result v6

    if-eqz v6, :cond_1

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->valueChanged()Z
    invoke-static {p4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->access$200(Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 248
    :cond_1
    const-string/jumbo v6, "metric_serving_unit"

    invoke-interface {v3, v6}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v6

    invoke-interface {v6, v8}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 249
    .local v4, "unitNode":Lorg/w3c/dom/Node;
    if-eqz v4, :cond_6

    .line 250
    invoke-interface {v4}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v6

    invoke-interface {v6, v8}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    invoke-interface {v6}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v5

    .line 251
    .local v5, "unitValue":Ljava/lang/String;
    if-eqz v5, :cond_6

    .line 252
    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->valueChanged()Z
    invoke-static {p3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->access$200(Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)Z

    move-result v6

    if-nez v6, :cond_3

    const-string v6, "g"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->mGUnitLocalized:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 254
    :cond_2
    const-string/jumbo v6, "metric_serving_amount"

    invoke-static {v3, v6}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseFloat(Ljava/lang/String;)F

    move-result v6

    # setter for: Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->value:F
    invoke-static {p3, v6}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->access$102(Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;F)F

    .line 255
    invoke-direct {p0, v3, p3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->normalizeUnits(Lorg/w3c/dom/Element;Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)V

    .line 257
    :cond_3
    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->valueChanged()Z
    invoke-static {p4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->access$200(Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)Z

    move-result v6

    if-nez v6, :cond_4

    const-string/jumbo v6, "oz"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_5

    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->mOzUnitLocalized:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 259
    :cond_5
    const-string/jumbo v6, "metric_serving_amount"

    invoke-static {v3, v6}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseFloat(Ljava/lang/String;)F

    move-result v6

    # setter for: Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->value:F
    invoke-static {p4, v6}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->access$102(Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;F)F

    .line 260
    invoke-direct {p0, v3, p4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->normalizeUnits(Lorg/w3c/dom/Element;Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)V

    .line 265
    .end local v4    # "unitNode":Lorg/w3c/dom/Node;
    .end local v5    # "unitValue":Ljava/lang/String;
    :cond_6
    if-eqz v0, :cond_8

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->valueChanged()Z
    invoke-static {p3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->access$200(Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)Z

    move-result v6

    if-eqz v6, :cond_8

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->valueChanged()Z
    invoke-static {p4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->access$200(Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 269
    .end local v3    # "servingElement":Lorg/w3c/dom/Element;
    :cond_7
    return-object p2

    .line 237
    .restart local v3    # "servingElement":Lorg/w3c/dom/Element;
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0
.end method

.method private updateUnitValueInUnitHolder(Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;Ljava/lang/String;)Z
    .locals 5
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "unitHolder"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;
    .param p3, "unitName"    # Ljava/lang/String;

    .prologue
    .line 502
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[\\d]+\\.?[\\d]*[\\s]?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-static {v3, v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v2

    .line 503
    .local v2, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v2, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 504
    .local v1, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 505
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v0

    .line 506
    .local v0, "group":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[\\s]?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseFloat(Ljava/lang/String;)F

    move-result v3

    # setter for: Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->value:F
    invoke-static {p2, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->access$102(Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;F)F

    .line 507
    const/4 v3, 0x1

    .line 509
    .end local v0    # "group":Ljava/lang/String;
    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic parseAutoCompleteSearch(Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 56
    check-cast p1, Lorg/w3c/dom/Document;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseAutoCompleteSearch(Lorg/w3c/dom/Document;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;

    move-result-object v0

    return-object v0
.end method

.method public parseAutoCompleteSearch(Lorg/w3c/dom/Document;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;
    .locals 5
    .param p1, "document"    # Lorg/w3c/dom/Document;

    .prologue
    .line 124
    const-string/jumbo v3, "suggestion"

    invoke-interface {p1, v3}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 125
    .local v1, "nodeList":Lorg/w3c/dom/NodeList;
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 126
    .local v2, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 127
    invoke-interface {v1, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v3

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v3

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 129
    :cond_0
    new-instance v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;

    invoke-direct {v3, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;-><init>(Ljava/util/List;)V

    return-object v3
.end method

.method public bridge synthetic parseBarcodeSearch(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 56
    check-cast p1, Lorg/w3c/dom/Document;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseBarcodeSearch(Lorg/w3c/dom/Document;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public parseBarcodeSearch(Lorg/w3c/dom/Document;)Ljava/lang/String;
    .locals 5
    .param p1, "doc"    # Lorg/w3c/dom/Document;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;
        }
    .end annotation

    .prologue
    .line 401
    invoke-interface {p1}, Lorg/w3c/dom/Document;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v3

    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 402
    const-string v3, "0"

    .line 413
    :goto_0
    return-object v3

    .line 406
    :cond_0
    :try_start_0
    const-string v3, "food_id"

    invoke-interface {p1, v3}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v3

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v3

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    .line 410
    .local v1, "id":J
    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-gtz v3, :cond_1

    .line 411
    new-instance v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodNotFoundException;

    invoke-direct {v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodNotFoundException;-><init>()V

    throw v3

    .line 407
    .end local v1    # "id":J
    :catch_0
    move-exception v0

    .line 408
    .local v0, "ex":Ljava/lang/Exception;
    new-instance v3, Lorg/apache/http/ParseException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/http/ParseException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 413
    .end local v0    # "ex":Ljava/lang/Exception;
    .restart local v1    # "id":J
    :cond_1
    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public bridge synthetic parseCategorySearch(Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 56
    check-cast p1, Lorg/w3c/dom/Document;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseCategorySearch(Lorg/w3c/dom/Document;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;

    move-result-object v0

    return-object v0
.end method

.method public parseCategorySearch(Lorg/w3c/dom/Document;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;
    .locals 8
    .param p1, "document"    # Lorg/w3c/dom/Document;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;
        }
    .end annotation

    .prologue
    .line 151
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 152
    .local v0, "categories":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategory;>;"
    const-string v7, "food_category"

    invoke-interface {p1, v7}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 153
    .local v1, "categoriesNodes":Lorg/w3c/dom/NodeList;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v7

    if-ge v6, v7, :cond_0

    .line 154
    invoke-interface {v1, v6}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v3

    check-cast v3, Lorg/w3c/dom/Element;

    .line 155
    .local v3, "categoryElement":Lorg/w3c/dom/Element;
    const-string v7, "food_category_id"

    invoke-static {v3, v7}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 156
    .local v4, "categoryId":Ljava/lang/String;
    const-string v7, "food_category_name"

    invoke-static {v3, v7}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 157
    .local v5, "categoryName":Ljava/lang/String;
    const-string v7, "food_category_description"

    invoke-static {v3, v7}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 158
    .local v2, "categoryDescription":Ljava/lang/String;
    new-instance v7, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategory;

    invoke-direct {v7, v4, v5, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategory;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 160
    .end local v2    # "categoryDescription":Ljava/lang/String;
    .end local v3    # "categoryElement":Lorg/w3c/dom/Element;
    .end local v4    # "categoryId":Ljava/lang/String;
    .end local v5    # "categoryName":Ljava/lang/String;
    :cond_0
    new-instance v7, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;

    invoke-direct {v7, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;-><init>(Ljava/util/List;)V

    return-object v7
.end method

.method public bridge synthetic parseExtraFood(Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 56
    check-cast p1, Lorg/w3c/dom/Document;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseExtraFood(Lorg/w3c/dom/Document;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;

    move-result-object v0

    return-object v0
.end method

.method public parseExtraFood(Lorg/w3c/dom/Document;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;
    .locals 12
    .param p1, "document"    # Lorg/w3c/dom/Document;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 189
    :try_start_0
    const-string v2, "food"

    invoke-interface {p1, v2}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v10

    check-cast v10, Lorg/w3c/dom/Element;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    .local v10, "foodNode":Lorg/w3c/dom/Element;
    const-string/jumbo v2, "servings"

    invoke-interface {v10, v2}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v2

    invoke-interface {v2, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v11

    check-cast v11, Lorg/w3c/dom/Element;

    .line 194
    .local v11, "servingsNode":Lorg/w3c/dom/Element;
    if-nez v11, :cond_0

    .line 195
    new-instance v2, Lorg/apache/http/ParseException;

    const-string/jumbo v3, "servingNode == null!"

    invoke-direct {v2, v3}, Lorg/apache/http/ParseException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 190
    .end local v10    # "foodNode":Lorg/w3c/dom/Element;
    .end local v11    # "servingsNode":Lorg/w3c/dom/Element;
    :catch_0
    move-exception v9

    .line 191
    .local v9, "ex":Ljava/lang/Exception;
    new-instance v2, Lorg/apache/http/ParseException;

    invoke-direct {v2}, Lorg/apache/http/ParseException;-><init>()V

    throw v2

    .line 197
    .end local v9    # "ex":Ljava/lang/Exception;
    .restart local v10    # "foodNode":Lorg/w3c/dom/Element;
    .restart local v11    # "servingsNode":Lorg/w3c/dom/Element;
    :cond_0
    new-instance v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;-><init>()V

    .line 198
    .local v1, "result":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    invoke-direct {p0, v11, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getDefaultServingElement(Lorg/w3c/dom/Element;Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;)Lorg/w3c/dom/Element;

    move-result-object v8

    .line 200
    .local v8, "defaultServingElement":Lorg/w3c/dom/Element;
    invoke-direct {p0, v1, v8}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseExtraFoodInfoParams(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Lorg/w3c/dom/Element;)V

    .line 202
    invoke-direct {p0, v10}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getCategoryIdStringBuilder(Lorg/w3c/dom/Element;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 203
    .local v7, "categoryIdBuilder":Ljava/lang/StringBuilder;
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;

    const-string v2, ""

    const-string v3, ""

    const-string v4, ""

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    .local v0, "extraFoodInfoResult":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;
    const-string v2, "calories"

    invoke-static {v8, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getTagValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 207
    .local v6, "caloriesString":Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/shealth/common/utils/Utils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 208
    invoke-static {v6}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->setCalories(I)V

    .line 211
    :cond_1
    return-object v0
.end method

.method public bridge synthetic parseFoodByCategorySearch(Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 56
    check-cast p1, Lorg/w3c/dom/Document;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseFoodByCategorySearch(Lorg/w3c/dom/Document;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;

    move-result-object v0

    return-object v0
.end method

.method public parseFoodByCategorySearch(Lorg/w3c/dom/Document;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;
    .locals 2
    .param p1, "searchResult"    # Lorg/w3c/dom/Document;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/w3c/dom/Document;",
            ")",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 146
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not permitted"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public parseFoodInfoFromExtraFoodInfoResult(Lorg/w3c/dom/Document;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .locals 4
    .param p1, "document"    # Lorg/w3c/dom/Document;
    .param p2, "extraFoodInfoResult"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;

    .prologue
    .line 425
    const-string v2, "food"

    invoke-interface {p1, v2}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    .line 426
    .local v0, "foodElement":Lorg/w3c/dom/Element;
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseFoodInfoElement(Lorg/w3c/dom/Element;)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v1

    .line 427
    .local v1, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    invoke-static {p2}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->calculateKcalForFoodInfoData(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;)F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setkCal(F)V

    .line 428
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setServerLocale(Ljava/lang/String;)V

    .line 429
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->createDefaultFoodDescription(Landroid/content/res/Resources;F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setDescription(Ljava/lang/String;)V

    .line 430
    return-object v1
.end method

.method public bridge synthetic parseFoodSearch(Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 56
    check-cast p1, Lorg/w3c/dom/Document;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseFoodSearch(Lorg/w3c/dom/Document;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;

    move-result-object v0

    return-object v0
.end method

.method public parseFoodSearch(Lorg/w3c/dom/Document;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;
    .locals 7
    .param p1, "document"    # Lorg/w3c/dom/Document;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/w3c/dom/Document;",
            ")",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 134
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 135
    .local v3, "result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    const-string v5, "food"

    invoke-interface {p1, v5}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 136
    .local v1, "foodNodes":Lorg/w3c/dom/NodeList;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v5

    if-ge v2, v5, :cond_0

    .line 137
    invoke-interface {v1, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    .line 138
    .local v0, "foodElement":Lorg/w3c/dom/Element;
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseFoodInfoElement(Lorg/w3c/dom/Element;)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 140
    .end local v0    # "foodElement":Lorg/w3c/dom/Element;
    :cond_0
    const-string/jumbo v5, "total_results"

    invoke-interface {p1, v5}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v5

    invoke-interface {v5, v6}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v5

    invoke-interface {v5}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v5

    invoke-interface {v5, v6}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v5

    invoke-interface {v5}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 141
    .local v4, "resultSize":I
    new-instance v5, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;

    invoke-direct {v5, v3, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;-><init>(Ljava/util/List;I)V

    return-object v5
.end method

.method public bridge synthetic parseSubCategorySearch(Ljava/lang/Object;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 56
    check-cast p1, Lorg/w3c/dom/Document;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;->parseSubCategorySearch(Lorg/w3c/dom/Document;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;

    move-result-object v0

    return-object v0
.end method

.method public parseSubCategorySearch(Lorg/w3c/dom/Document;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;
    .locals 6
    .param p1, "document"    # Lorg/w3c/dom/Document;

    .prologue
    .line 165
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 166
    .local v2, "subCategories":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;>;"
    const-string v4, "food_sub_category"

    invoke-interface {p1, v4}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    .line 167
    .local v0, "categoriesNodes":Lorg/w3c/dom/NodeList;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 168
    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v4

    invoke-interface {v4}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v4

    invoke-interface {v4}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v3

    .line 169
    .local v3, "subCategoryName":Ljava/lang/String;
    new-instance v4, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;

    invoke-direct {v4, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 171
    .end local v3    # "subCategoryName":Ljava/lang/String;
    :cond_0
    new-instance v4, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;

    invoke-direct {v4, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;-><init>(Ljava/util/List;)V

    return-object v4
.end method
