.class public abstract Lcom/sec/android/app/shealth/food/foodpick/loader/Task;
.super Ljava/lang/Object;
.source "Task.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final ACCOMPANY_WITH_LOADING_DIALOG_DEFAULT:Z = false

.field private static final IS_ERROR_MESSAGE_SHOWN_DEFAULT:Z = true


# instance fields
.field private mFuture:Ljava/util/concurrent/Future;

.field private mIsCanceled:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/loader/Task;, "Lcom/sec/android/app/shealth/food/foodpick/loader/Task<TE;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/Task;->mIsCanceled:Z

    return-void
.end method


# virtual methods
.method protected accompanyWithLoadingDialog()Z
    .locals 1

    .prologue
    .line 73
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/loader/Task;, "Lcom/sec/android/app/shealth/food/foodpick/loader/Task<TE;>;"
    const/4 v0, 0x0

    return v0
.end method

.method clearFuture()Ljava/util/concurrent/Future;
    .locals 2

    .prologue
    .line 98
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/loader/Task;, "Lcom/sec/android/app/shealth/food/foodpick/loader/Task<TE;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/Task;->mFuture:Ljava/util/concurrent/Future;

    .line 99
    .local v0, "result":Ljava/util/concurrent/Future;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/Task;->mFuture:Ljava/util/concurrent/Future;

    .line 100
    return-object v0
.end method

.method protected abstract doTask()Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<TE;>;"
        }
    .end annotation
.end method

.method getFuture()Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 90
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/loader/Task;, "Lcom/sec/android/app/shealth/food/foodpick/loader/Task<TE;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/Task;->mFuture:Ljava/util/concurrent/Future;

    return-object v0
.end method

.method isCanceled()Z
    .locals 1

    .prologue
    .line 104
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/loader/Task;, "Lcom/sec/android/app/shealth/food/foodpick/loader/Task<TE;>;"
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/Task;->mIsCanceled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/Task;->mFuture:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/Task;->mFuture:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onFinishedWithError()V
    .locals 0

    .prologue
    .line 64
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/loader/Task;, "Lcom/sec/android/app/shealth/food/foodpick/loader/Task<TE;>;"
    return-void
.end method

.method protected onTaskCanceled()V
    .locals 1

    .prologue
    .line 51
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/loader/Task;, "Lcom/sec/android/app/shealth/food/foodpick/loader/Task<TE;>;"
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/Task;->mIsCanceled:Z

    .line 52
    return-void
.end method

.method protected abstract onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation
.end method

.method protected onTaskStarted()V
    .locals 0

    .prologue
    .line 38
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/loader/Task;, "Lcom/sec/android/app/shealth/food/foodpick/loader/Task<TE;>;"
    return-void
.end method

.method setFuture(Ljava/util/concurrent/Future;)V
    .locals 0
    .param p1, "future"    # Ljava/util/concurrent/Future;

    .prologue
    .line 94
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/loader/Task;, "Lcom/sec/android/app/shealth/food/foodpick/loader/Task<TE;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/Task;->mFuture:Ljava/util/concurrent/Future;

    .line 95
    return-void
.end method

.method protected withErrorToast()Z
    .locals 1

    .prologue
    .line 83
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/loader/Task;, "Lcom/sec/android/app/shealth/food/foodpick/loader/Task<TE;>;"
    const/4 v0, 0x1

    return v0
.end method
