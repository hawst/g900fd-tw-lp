.class final Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog$1;
.super Ljava/lang/Object;
.source "ShareViaDialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->showShareViaDialog(Landroid/content/Context;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog$1;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog$1;->val$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 81
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog$1;->val$context:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog$1;->val$view:Landroid/view/View;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->getActivityScreenShotForShare(Landroid/content/Context;Landroid/view/View;)Landroid/graphics/Bitmap;
    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->access$000(Landroid/content/Context;Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 83
    .local v0, "bitmapToShare":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 125
    :goto_0
    return-void

    .line 86
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog$1;->val$context:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/app/shealth/common/commonui/IBuildCacheCallback;

    invoke-interface {v4}, Lcom/sec/android/app/shealth/common/commonui/IBuildCacheCallback;->onCacheBuildEnd()V

    .line 87
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog$1;->val$context:Landroid/content/Context;

    invoke-static {v4, v0}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->saveShareImageToSdCard(Landroid/content/Context;Landroid/graphics/Bitmap;)Ljava/io/File;

    move-result-object v1

    .line 89
    .local v1, "file":Ljava/io/File;
    if-eqz v0, :cond_1

    .line 91
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 94
    :cond_1
    if-nez v1, :cond_2

    .line 96
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog$1;->val$context:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    sget v5, Lcom/sec/android/app/shealth/common/commonui/R$string;->share_image_storage_error:I

    const/4 v6, 0x1

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 99
    :cond_2
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 101
    .local v2, "filePath":Ljava/lang/String;
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    .line 102
    .local v3, "shareUri":Landroid/net/Uri;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->shareUris:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->access$102(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 103
    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->shareUris:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->access$100()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 104
    new-instance v4, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog$1$1;

    invoke-direct {v4, p0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog$1$1;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog$1;Ljava/lang/String;)V

    goto :goto_0
.end method
