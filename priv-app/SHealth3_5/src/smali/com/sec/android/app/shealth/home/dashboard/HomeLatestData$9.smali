.class Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$9;
.super Ljava/lang/Object;
.source "HomeLatestData.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)V
    .locals 0

    .prologue
    .line 1452
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$9;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1456
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$9;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->workingOnClickListener:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$1900(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Z

    move-result v2

    if-nez v2, :cond_0

    if-nez p1, :cond_1

    .line 1479
    :cond_0
    :goto_0
    return-void

    .line 1459
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 1460
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1461
    .local v0, "action":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1462
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$9;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # invokes: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->checkActionForLogging(Ljava/lang/String;)V
    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$2000(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;Ljava/lang/String;)V

    .line 1464
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const/16 v3, 0x61

    if-ne v2, v3, :cond_3

    .line 1465
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$9;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->calories:[I
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$2100(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)[I

    move-result-object v2

    if-nez v2, :cond_2

    .line 1466
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$9;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # invokes: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setCaloriesExtra(III)V
    invoke-static {v2, v4, v4, v4}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$2200(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;III)V

    .line 1467
    :cond_2
    const-string v2, "burnt_calories"

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$9;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->calories:[I
    invoke-static {v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$2100(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)[I

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 1468
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$9;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$1300(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1476
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$9;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    invoke-virtual {v2, v5}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setWorkingOnClickListener(Z)V

    goto :goto_0

    .line 1472
    :cond_3
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1473
    const-string/jumbo v2, "show_graph_fragment"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1474
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$9;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$1300(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method
