.class Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;
.super Ljava/lang/Object;
.source "InitSetTermsOfUse.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->initLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)V
    .locals 0

    .prologue
    .line 557
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v9, 0x8000

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 561
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isAgreeCheck:Z
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$400(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isAgreeCheck_PP:Z
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$500(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 562
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "com.sec.android.app.shealth"

    const-string v6, "IS02"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 563
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-static {v4, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setTermsAndConditionsOf355Accepted(Landroid/content/Context;Z)V

    .line 564
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isUpgradeFrom35x:Z
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$1500(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 565
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    .line 566
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->getInstance()Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    sget-object v5, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->SAMSUNG_ACCOUNT_NOTIFICATION:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    invoke-virtual {v3, v4, v2, v5}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->scheduleReminderNotification(Landroid/content/Context;ILcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)V

    .line 572
    :cond_0
    :goto_0
    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 573
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v3, 0x4000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 574
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->startActivity(Landroid/content/Intent;)V

    .line 585
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.sec.shealth.action.FINISHED_PROFILE_SETUP"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 586
    .local v0, "finishProfile":Landroid/content/Intent;
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->sendBroadcast(Landroid/content/Intent;)V

    .line 628
    .end local v0    # "finishProfile":Landroid/content/Intent;
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_1
    return-void

    .line 568
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;->getInstance()Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;->getPeriodicSync(Landroid/content/Context;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    .line 569
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setAutoBackupSwitchStatus(Landroid/content/Context;Z)V

    .line 570
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->getInstance()Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    sget-object v5, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->AUTO_BACKUP_NOTIFICATION:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    invoke-virtual {v3, v4, v2, v5}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->scheduleReminderNotification(Landroid/content/Context;ILcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)V

    goto :goto_0

    .line 587
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_3

    .line 588
    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 589
    .restart local v1    # "intent":Landroid/content/Intent;
    invoke-virtual {v1, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 590
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 592
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$1600(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->setUserAccountInfo(Ljava/lang/String;)V

    .line 594
    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$1000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SS supoorted: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/service/health/keyManager/KeyManager;->isSecureStorageSupported()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$1000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Pincode enabled: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;
    invoke-static {v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$1700(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const-string/jumbo v8, "security_pin_enabled"

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/service/health/keyManager/KeyManager;->isSecureStorageSupported()Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$1700(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-string/jumbo v6, "security_pin_enabled"

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 601
    .local v2, "requestPasswordCondition":Z
    :goto_2
    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$1000()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "requestPasswordCondition: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 603
    if-eqz v2, :cond_5

    .line 604
    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 605
    .restart local v1    # "intent":Landroid/content/Intent;
    invoke-virtual {v1, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 606
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "requestPasswordCondition":Z
    :cond_4
    move v2, v3

    .line 597
    goto :goto_2

    .line 608
    .restart local v2    # "requestPasswordCondition":Z
    :cond_5
    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 609
    .restart local v1    # "intent":Landroid/content/Intent;
    invoke-virtual {v1, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 610
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 616
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "requestPasswordCondition":Z
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mToast1:Landroid/widget/Toast;
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$1800(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Landroid/widget/Toast;

    move-result-object v4

    if-nez v4, :cond_7

    .line 618
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    const v7, 0x7f0908c3

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v3

    # setter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mToast1:Landroid/widget/Toast;
    invoke-static {v4, v3}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$1802(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 619
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mToast1:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$1800(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 623
    :cond_7
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mToast1:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$1800(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->cancel()V

    .line 624
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    const/4 v4, 0x0

    # setter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mToast1:Landroid/widget/Toast;
    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$1802(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;Landroid/widget/Toast;)Landroid/widget/Toast;

    goto/16 :goto_1
.end method
