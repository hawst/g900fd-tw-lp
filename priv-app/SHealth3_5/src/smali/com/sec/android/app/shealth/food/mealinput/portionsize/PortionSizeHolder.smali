.class public Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;
.super Ljava/lang/Object;
.source "PortionSizeHolder.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_AMOUNT:I = 0x1

.field private static final FALSE_FOR_PARCEL:I = 0x0

.field private static final TRUE_FOR_PARCEL:I = 0x1


# instance fields
.field private mCarbohydrate:F

.field private mFat:F

.field private mFoodName:Ljava/lang/String;

.field private mGInKcal:F

.field private mIsMyFood:Z

.field private mKcal:F

.field private mOzInKcal:F

.field private mProtein:F

.field private mQuantity:F

.field private mUnit:I

.field private mUnitName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 293
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(FIFZLjava/lang/String;FFLjava/lang/String;FFF)V
    .locals 0
    .param p1, "quantity"    # F
    .param p2, "unit"    # I
    .param p3, "kcal"    # F
    .param p4, "isMyFood"    # Z
    .param p5, "foodName"    # Ljava/lang/String;
    .param p6, "gInKcal"    # F
    .param p7, "ozInKcal"    # F
    .param p8, "unitName"    # Ljava/lang/String;
    .param p9, "carbohydrate"    # F
    .param p10, "protein"    # F
    .param p11, "fat"    # F

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput p1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mQuantity:F

    .line 45
    iput p2, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mUnit:I

    .line 46
    iput p3, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mKcal:F

    .line 47
    iput p6, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mGInKcal:F

    .line 48
    iput p7, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mOzInKcal:F

    .line 49
    iput-object p8, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mUnitName:Ljava/lang/String;

    .line 50
    iput-boolean p4, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mIsMyFood:Z

    .line 51
    iput-object p5, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mFoodName:Ljava/lang/String;

    .line 53
    iput p9, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mCarbohydrate:F

    .line 55
    iput p10, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mProtein:F

    .line 57
    iput p11, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mFat:F

    .line 59
    return-void
.end method

.method public constructor <init>(FIFZLjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "quantity"    # F
    .param p2, "unit"    # I
    .param p3, "kcal"    # F
    .param p4, "isMyFood"    # Z
    .param p5, "foodName"    # Ljava/lang/String;
    .param p6, "unitName"    # Ljava/lang/String;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput p1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mQuantity:F

    .line 63
    iput p2, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mUnit:I

    .line 64
    iput p3, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mKcal:F

    .line 65
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mUnitName:Ljava/lang/String;

    .line 66
    iput-boolean p4, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mIsMyFood:Z

    .line 67
    iput-object p5, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mFoodName:Ljava/lang/String;

    .line 68
    iput-object p6, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mUnitName:Ljava/lang/String;

    .line 69
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 272
    const/4 v0, 0x0

    return v0
.end method

.method public getCarbohydrate()F
    .locals 1

    .prologue
    .line 220
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mCarbohydrate:F

    return v0
.end method

.method public getFat()F
    .locals 1

    .prologue
    .line 256
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mFat:F

    return v0
.end method

.method public getFoodName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mFoodName:Ljava/lang/String;

    return-object v0
.end method

.method public getGramInKcal()F
    .locals 1

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mGInKcal:F

    return v0
.end method

.method public getKcal()F
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mKcal:F

    return v0
.end method

.method public getOzInKcal()F
    .locals 1

    .prologue
    .line 155
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mOzInKcal:F

    return v0
.end method

.method public getProtein()F
    .locals 1

    .prologue
    .line 238
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mProtein:F

    return v0
.end method

.method public getQuantity()F
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mQuantity:F

    return v0
.end method

.method public getUnit()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mUnit:I

    return v0
.end method

.method public getUnitName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mUnitName:Ljava/lang/String;

    return-object v0
.end method

.method public ismIsMyFood()Z
    .locals 1

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mIsMyFood:Z

    return v0
.end method

.method public setQuantity(F)V
    .locals 0
    .param p1, "quantity"    # F

    .prologue
    .line 96
    iput p1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mQuantity:F

    .line 97
    return-void
.end method

.method public setUnit(I)V
    .locals 0
    .param p1, "unit"    # I

    .prologue
    .line 110
    iput p1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mUnit:I

    .line 111
    return-void
.end method

.method public setUnitName(Ljava/lang/String;)V
    .locals 0
    .param p1, "unitName"    # Ljava/lang/String;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mUnitName:Ljava/lang/String;

    .line 83
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 277
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mQuantity:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 278
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mUnit:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 279
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mKcal:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 280
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mIsMyFood:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mFoodName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 282
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mGInKcal:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 283
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mOzInKcal:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mUnitName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 286
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mCarbohydrate:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 288
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mProtein:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 290
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->mFat:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 291
    return-void

    .line 280
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
