.class public Lcom/sec/android/app/shealth/settings/UnitSettingActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "UnitSettingActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissControllerProvider;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ISaveChosenItemListenerGetter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/UnitSettingActivity$WrapperListener;,
        Lcom/sec/android/app/shealth/settings/UnitSettingActivity$ViewHodler;,
        Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;,
        Lcom/sec/android/app/shealth/settings/UnitSettingActivity$SaveUnitsDismissController;,
        Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;
    }
.end annotation


# static fields
.field private static final HEALTH_PROFILE_UPDATED:Ljava/lang/String; = "com.samsung.android.sdk.health.sensor.action.PROFILE_UPDATED"

.field private static final POPUP_DISTANCE:Ljava/lang/String; = "distance"

.field private static final POPUP_GLUCOSE:Ljava/lang/String; = "glucose"

.field private static final POPUP_HEIGHT:Ljava/lang/String; = "height"

.field private static final POPUP_TEMPERATURE:Ljava/lang/String; = "temperature"

.field private static final POPUP_WEIGHT:Ljava/lang/String; = "weight"

.field private static final TAG:Ljava/lang/String;

.field private static mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

.field private static s_intentFilter:Landroid/content/IntentFilter;


# instance fields
.field private bStateLossOnsaveInstance:Z

.field private enabledPlugIns:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;",
            ">;"
        }
    .end annotation
.end field

.field private mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

.field private final mDismissControlMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;",
            ">;"
        }
    .end annotation
.end field

.field private final mSyncedReceiver:Landroid/content/BroadcastReceiver;

.field private measurablesList:Landroid/widget/ListView;

.field private popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

.field popupCreateListener:Landroid/view/View$OnClickListener;

.field private resetDataListItem:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;",
            ">;"
        }
    .end annotation
.end field

.field serviceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

.field private unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

.field private unitsListAdapter:Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    const-class v0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->TAG:Ljava/lang/String;

    .line 86
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->s_intentFilter:Landroid/content/IntentFilter;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    .line 84
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->resetDataListItem:Ljava/util/ArrayList;

    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->bStateLossOnsaveInstance:Z

    .line 101
    new-instance v0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$1;-><init>(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mDismissControlMap:Ljava/util/Map;

    .line 442
    new-instance v0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$2;-><init>(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->popupCreateListener:Landroid/view/View$OnClickListener;

    .line 733
    new-instance v0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$8;-><init>(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mSyncedReceiver:Landroid/content/BroadcastReceiver;

    .line 755
    new-instance v0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$9;-><init>(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->serviceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    .line 809
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->createHeightUnitPopup()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->initUnitValue()V

    return-void
.end method

.method static synthetic access$1102(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/UnitSettingActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->resetDataListItem:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->measurablesList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;Landroid/widget/ListView;)Landroid/widget/ListView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/UnitSettingActivity;
    .param p1, "x1"    # Landroid/widget/ListView;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->measurablesList:Landroid/widget/ListView;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->createWeightUnitPopup()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->createDistanceUnitPopup()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->createGlucoseUnitPopup()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->createTemperatureUnitPopup()V

    return-void
.end method

.method static synthetic access$600()Lcom/samsung/android/sdk/health/content/ShealthProfile;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    return-object v0
.end method

.method static synthetic access$602(Lcom/samsung/android/sdk/health/content/ShealthProfile;)Lcom/samsung/android/sdk/health/content/ShealthProfile;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .prologue
    .line 76
    sput-object p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    return-object p0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->saveShealthProfile()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitsListAdapter:Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;)Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/UnitSettingActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitsListAdapter:Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;

    return-object p1
.end method

.method private checkEnabledPlugIns()V
    .locals 7

    .prologue
    const v6, 0x7f0901ae

    .line 217
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->enabledPlugIns:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 219
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->enabledPlugIns:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 220
    const-string v1, "glucose"

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->checkIfAlreadyUnitAdded(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 221
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitsListAdapter:Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;

    new-instance v2, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "glucose"

    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getGlucoseUnit()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;->add(Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;)V

    .line 224
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->enabledPlugIns:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09023f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->enabledPlugIns:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090242

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 225
    :cond_1
    const-string/jumbo v1, "temperature"

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->checkIfAlreadyUnitAdded(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 227
    sget-object v1, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getTemperatureUnit()I

    move-result v1

    const v2, 0x27102

    if-ne v1, v2, :cond_3

    .line 229
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    const-string v2, "F"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putTemperatureUnit(Ljava/lang/String;)V

    .line 235
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitsListAdapter:Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;

    new-instance v2, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090130

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "temperature"

    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getTemperatureUnitString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;->add(Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;)V

    .line 217
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 233
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    const-string v2, "C"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putTemperatureUnit(Ljava/lang/String;)V

    goto :goto_1

    .line 240
    :cond_4
    return-void
.end method

.method private checkIfAlreadyUnitAdded(Ljava/lang/String;)Z
    .locals 2
    .param p1, "unitType"    # Ljava/lang/String;

    .prologue
    .line 725
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitsListAdapter:Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 727
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitsListAdapter:Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;->getItem(I)Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;->mType:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 728
    const/4 v1, 0x1

    .line 730
    :goto_1
    return v1

    .line 725
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 730
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private createDistanceUnitPopup()V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 368
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    if-nez v6, :cond_0

    iget-boolean v6, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->bStateLossOnsaveInstance:Z

    if-eqz v6, :cond_1

    .line 387
    :cond_0
    :goto_0
    return-void

    .line 371
    :cond_1
    const/4 v2, 0x1

    .line 373
    .local v2, "isKM":Z
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getDistanceUnit()Ljava/lang/String;

    move-result-object v1

    .line 374
    .local v1, "distanceUnit":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 375
    const-string v6, "km"

    invoke-virtual {v1, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    .line 378
    :cond_2
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    const v6, 0x7f090132

    const/16 v7, 0xc

    invoke-direct {v0, v6, v7}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;-><init>(II)V

    .line 379
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 380
    .local v3, "unitsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0e000c

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 381
    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setItems(Ljava/util/List;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 382
    const/4 v6, 0x2

    new-array v6, v6, [Z

    aput-boolean v2, v6, v4

    if-nez v2, :cond_3

    move v4, v5

    :cond_3
    aput-boolean v4, v6, v5

    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setChecked([Z)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 384
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    .line 386
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const-string v6, "distance"

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private createGlucoseUnitPopup()V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 390
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    if-nez v6, :cond_0

    iget-boolean v6, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->bStateLossOnsaveInstance:Z

    if-eqz v6, :cond_1

    .line 417
    :cond_0
    :goto_0
    return-void

    .line 393
    :cond_1
    const/4 v2, 0x1

    .line 395
    .local v2, "isMgDl":Z
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getGlucoseUnit()Ljava/lang/String;

    move-result-object v1

    .line 396
    .local v1, "glucoseUnit":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 397
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0900cf

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    .line 400
    :cond_2
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    const v6, 0x7f0901ae

    const/16 v7, 0xc

    invoke-direct {v0, v6, v7}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;-><init>(II)V

    .line 401
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 402
    .local v3, "unitsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0e000d

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 403
    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setItems(Ljava/util/List;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 404
    const/4 v6, 0x2

    new-array v6, v6, [Z

    aput-boolean v2, v6, v4

    if-nez v2, :cond_3

    move v4, v5

    :cond_3
    aput-boolean v4, v6, v5

    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setChecked([Z)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 414
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    .line 416
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const-string v6, "glucose"

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private createHeightUnitPopup()V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 325
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    if-nez v6, :cond_0

    iget-boolean v6, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->bStateLossOnsaveInstance:Z

    if-eqz v6, :cond_1

    .line 343
    :cond_0
    :goto_0
    return-void

    .line 328
    :cond_1
    const/4 v2, 0x1

    .line 330
    .local v2, "isCm":Z
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getHeightUnit()Ljava/lang/String;

    move-result-object v1

    .line 331
    .local v1, "heightUnit":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 332
    const-string v6, "cm"

    invoke-virtual {v1, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    .line 334
    :cond_2
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    const v6, 0x7f09012f

    const/16 v7, 0xc

    invoke-direct {v0, v6, v7}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;-><init>(II)V

    .line 335
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 336
    .local v3, "unitsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0e0008

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 337
    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setItems(Ljava/util/List;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 338
    const/4 v6, 0x2

    new-array v6, v6, [Z

    aput-boolean v2, v6, v4

    if-nez v2, :cond_3

    move v4, v5

    :cond_3
    aput-boolean v4, v6, v5

    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setChecked([Z)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 340
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    .line 342
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const-string v6, "height"

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private createTemperatureUnitPopup()V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 420
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    if-nez v6, :cond_0

    iget-boolean v6, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->bStateLossOnsaveInstance:Z

    if-eqz v6, :cond_1

    .line 438
    :cond_0
    :goto_0
    return-void

    .line 423
    :cond_1
    const/4 v1, 0x1

    .line 424
    .local v1, "isCentigrade":Z
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getTemperatureUnit()Ljava/lang/String;

    move-result-object v2

    .line 425
    .local v2, "temperatureUnit":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 426
    const-string v6, "C"

    invoke-virtual {v2, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    .line 429
    :cond_2
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    const v6, 0x7f090130

    const/16 v7, 0xc

    invoke-direct {v0, v6, v7}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;-><init>(II)V

    .line 430
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 431
    .local v3, "unitsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0e000a

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 432
    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setItems(Ljava/util/List;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 433
    const/4 v6, 0x2

    new-array v6, v6, [Z

    aput-boolean v1, v6, v4

    if-nez v1, :cond_3

    move v4, v5

    :cond_3
    aput-boolean v4, v6, v5

    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setChecked([Z)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 435
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    .line 437
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const-string/jumbo v6, "temperature"

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private createWeightUnitPopup()V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 346
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    if-nez v6, :cond_0

    iget-boolean v6, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->bStateLossOnsaveInstance:Z

    if-eqz v6, :cond_1

    .line 365
    :cond_0
    :goto_0
    return-void

    .line 349
    :cond_1
    const/4 v1, 0x1

    .line 351
    .local v1, "isKg":Z
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getWeightUnit()Ljava/lang/String;

    move-result-object v3

    .line 352
    .local v3, "weightUnit":Ljava/lang/String;
    if-eqz v3, :cond_2

    .line 353
    const-string v6, "kg"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    .line 356
    :cond_2
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    const v6, 0x7f09002c

    const/16 v7, 0xc

    invoke-direct {v0, v6, v7}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;-><init>(II)V

    .line 357
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 358
    .local v2, "unitsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0e0009

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 359
    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setItems(Ljava/util/List;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 360
    const/4 v6, 0x2

    new-array v6, v6, [Z

    aput-boolean v1, v6, v4

    if-nez v1, :cond_3

    move v4, v5

    :cond_3
    aput-boolean v4, v6, v5

    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setChecked([Z)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 362
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    .line 364
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const-string/jumbo v6, "weight"

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getDistanceUnit()Ljava/lang/String;
    .locals 2

    .prologue
    .line 279
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getDistanceUnit()Ljava/lang/String;

    move-result-object v0

    .line 280
    .local v0, "distanceUnit":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 281
    const-string v0, "km"

    .line 283
    :cond_0
    return-object v0
.end method

.method private getDistanceUnitString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 287
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getDistanceUnit()Ljava/lang/String;

    move-result-object v0

    .line 288
    .local v0, "distanceUnit":Ljava/lang/String;
    const-string v1, "km"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 289
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900c7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 293
    :goto_0
    return-object v0

    .line 291
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900cc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getGlucoseUnit()Ljava/lang/String;
    .locals 3

    .prologue
    .line 297
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getGlucoseUnit()Ljava/lang/String;

    move-result-object v0

    .line 298
    .local v0, "glucoseUnit":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 299
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900cf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 301
    :cond_0
    return-object v0
.end method

.method private getHeightUnit()Ljava/lang/String;
    .locals 2

    .prologue
    .line 243
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getHeightUnit()Ljava/lang/String;

    move-result-object v0

    .line 244
    .local v0, "heightUnit":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 245
    const-string v0, "cm"

    .line 247
    :cond_0
    return-object v0
.end method

.method private getHeightUnitString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 251
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getHeightUnit()Ljava/lang/String;

    move-result-object v0

    .line 252
    .local v0, "heightUnit":Ljava/lang/String;
    const-string v1, "cm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 253
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900bc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 257
    :goto_0
    return-object v0

    .line 255
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900bd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getTemperatureUnit()Ljava/lang/String;
    .locals 2

    .prologue
    .line 305
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getTemperatureUnit()Ljava/lang/String;

    move-result-object v0

    .line 306
    .local v0, "temperatureUnit":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 307
    const-string v0, "C"

    .line 309
    :cond_0
    return-object v0
.end method

.method private getTemperatureUnitString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 313
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getTemperatureUnit()Ljava/lang/String;

    move-result-object v0

    .line 314
    .local v0, "temperatureUnit":Ljava/lang/String;
    const-string v1, "C"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 315
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900cd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 319
    :goto_0
    return-object v0

    .line 317
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900ce

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getWeightUnit()Ljava/lang/String;
    .locals 2

    .prologue
    .line 261
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getWeightUnit()Ljava/lang/String;

    move-result-object v0

    .line 262
    .local v0, "weightUnit":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 263
    const-string v0, "kg"

    .line 265
    :cond_0
    return-object v0
.end method

.method private getWeightUnitString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 269
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getWeightUnit()Ljava/lang/String;

    move-result-object v0

    .line 270
    .local v0, "weightUnit":Ljava/lang/String;
    const-string v1, "kg"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 271
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900c0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 275
    :goto_0
    return-object v0

    .line 273
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900c2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private initUnitValue()V
    .locals 5

    .prologue
    .line 164
    sget-object v0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    if-nez v0, :cond_0

    .line 181
    :goto_0
    return-void

    .line 167
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeightUnit()I

    move-result v0

    sget-object v1, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeightUnit()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->saveUnitforUnitHelper(II)V

    .line 169
    sget-object v0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getDistanceUnit()I

    move-result v0

    const v1, 0x29811

    if-ne v0, v1, :cond_1

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    const-string v1, "km"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putDistanceUnit(Ljava/lang/String;)V

    .line 174
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitsListAdapter:Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;

    new-instance v1, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09012f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "height"

    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getHeightUnitString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;->add(Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitsListAdapter:Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;

    new-instance v1, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09002c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "weight"

    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getWeightUnitString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;->add(Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;)V

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitsListAdapter:Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;

    new-instance v1, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090132

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "distance"

    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getDistanceUnitString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;->add(Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;)V

    .line 178
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->checkEnabledPlugIns()V

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitsListAdapter:Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 172
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    const-string/jumbo v1, "mi"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putDistanceUnit(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private saveShealthProfile()V
    .locals 2

    .prologue
    .line 619
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getDistanceUnit()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->sendIntentToHealthService(Ljava/lang/String;)V

    .line 621
    :try_start_0
    sget-object v1, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->save()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 625
    :goto_0
    return-void

    .line 622
    :catch_0
    move-exception v0

    .line 623
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v1, "UnitSettingActivity"

    invoke-static {v1, v0}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private sendIntentToHealthService(Ljava/lang/String;)V
    .locals 4
    .param p1, "distanceUnit"    # Ljava/lang/String;

    .prologue
    .line 628
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.shealth.UPDATE_PROFILE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 629
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "distance_unit"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 630
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 633
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-eqz v1, :cond_0

    .line 634
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->close()V

    .line 635
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 637
    :cond_0
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->serviceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 638
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 702
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 704
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f0907bc

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 705
    return-void
.end method

.method public getDismissController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 616
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mDismissControlMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;

    return-object v0
.end method

.method public getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 464
    const-string v0, "height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 465
    new-instance v0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$3;-><init>(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V

    .line 586
    :goto_0
    return-object v0

    .line 488
    :cond_0
    const-string/jumbo v0, "weight"

    invoke-virtual {v0, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 489
    new-instance v0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$4;-><init>(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V

    goto :goto_0

    .line 526
    :cond_1
    const-string v0, "distance"

    invoke-virtual {v0, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 527
    new-instance v0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$5;-><init>(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V

    goto :goto_0

    .line 549
    :cond_2
    const-string v0, "glucose"

    invoke-virtual {v0, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 550
    new-instance v0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$6;-><init>(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V

    goto :goto_0

    .line 564
    :cond_3
    const-string/jumbo v0, "temperature"

    invoke-virtual {v0, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 565
    new-instance v0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$7;-><init>(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V

    goto :goto_0

    .line 586
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 132
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 133
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v1

    if-nez v1, :cond_0

    .line 136
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.shealth.SplashScreenActivity.restart"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 137
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "temps"

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->getRandomKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 138
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 139
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 140
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 141
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->startActivity(Landroid/content/Intent;)V

    .line 143
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-static {v1}, Landroid/os/Process;->killProcess(I)V

    .line 145
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    const v1, 0x7f03025a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->setContentView(I)V

    .line 147
    new-instance v1, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->resetDataListItem:Ljava/util/ArrayList;

    invoke-direct {v1, p0, p0, v2, v3}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;-><init>(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;Landroid/content/Context;ILjava/util/List;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitsListAdapter:Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;

    .line 148
    new-instance v1, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    .line 149
    new-instance v1, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 151
    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->getAllAppRegistryDatas(ZLandroid/content/Context;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->enabledPlugIns:Ljava/util/List;

    .line 153
    const v1, 0x7f080a8d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->measurablesList:Landroid/widget/ListView;

    .line 154
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->measurablesList:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitsListAdapter:Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 156
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->initUnitValue()V

    .line 158
    sget-object v1, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->s_intentFilter:Landroid/content/IntentFilter;

    const-string v2, "com.samsung.android.sdk.health.sensor.action.PROFILE_UPDATED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 159
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mSyncedReceiver:Landroid/content/BroadcastReceiver;

    sget-object v2, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->s_intentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 160
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 192
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->bStateLossOnsaveInstance:Z

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->close()V

    .line 196
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mSyncedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->resetDataListItem:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->resetDataListItem:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 203
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->resetDataListItem:Ljava/util/ArrayList;

    .line 206
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->enabledPlugIns:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mDismissControlMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 208
    sput-object v1, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 209
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    .line 211
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    .line 213
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 185
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->bStateLossOnsaveInstance:Z

    .line 186
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 187
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 127
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->bStateLossOnsaveInstance:Z

    .line 128
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 129
    return-void
.end method
