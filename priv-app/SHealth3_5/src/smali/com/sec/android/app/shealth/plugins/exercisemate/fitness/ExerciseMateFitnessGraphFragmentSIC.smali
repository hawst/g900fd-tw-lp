.class public Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;
.super Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;
.source "ExerciseMateFitnessGraphFragmentSIC.java"


# static fields
.field private static final DAYS_IN_WEEK:B = 0x7t

.field private static final DAY_IN_MONTH:B = 0x1ft

.field private static final HOURS_IN_DAY:B = 0x18t

.field private static MARKING_INTERVAL:I = 0x0

.field private static final MILLIS_IN_DAY:I = 0x5265c00

.field private static final MILLIS_IN_HOUR:I = 0x36ee80

.field private static final MILLIS_IN_MONTH:J = 0x9fa52400L

.field private static final MILLIS_IN_SECOND:I = 0x3e8

.field private static final MILLIS_IN_WEEK:I = 0x240c8400

.field private static final MINUTES_IN_HOUR:B = 0x3ct

.field private static final MONTH_IN_YEAR:I = 0xc

.field private static final SECONDS_IN_MINUTE:B = 0x3ct


# instance fields
.field private final AVERAGE:I

.field private final BELOWAVERAGE:I

.field private final EXCELLENT:I

.field private final FAIR:I

.field private final GOOD:I

.field private final VERYGOOD:I

.field private final WELLBELOWAVERAGE:I

.field density:F

.field private exerciseUnit:Ljava/lang/String;

.field exercisemateFitnessInformationArea:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;

.field private handlerListener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

.field private isFirstTime:Z

.field private mContent:Landroid/app/Activity;

.field private mLegendView:Landroid/view/View;

.field private mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

.field private mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

.field private oneDForm:Ljava/text/DecimalFormat;

.field scaledDensity:F

.field private switchToWalkingSummary:Landroid/widget/ImageButton;

.field private timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->MARKING_INTERVAL:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;-><init>()V

    .line 42
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 57
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->EXCELLENT:I

    .line 58
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->VERYGOOD:I

    .line 59
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->GOOD:I

    .line 60
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->AVERAGE:I

    .line 61
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->FAIR:I

    .line 62
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->BELOWAVERAGE:I

    .line 63
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->WELLBELOWAVERAGE:I

    .line 96
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->isFirstTime:Z

    .line 98
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#.#"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->oneDForm:Ljava/text/DecimalFormat;

    .line 237
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->handlerListener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;)Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;D)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;
    .param p1, "x1"    # D

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getFitnessData(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getBubbleAreaUnit()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getBubbleAreaUnit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 450
    const-string v0, ""

    .line 451
    .local v0, "exerciseUnit":Ljava/lang/String;
    return-object v0
.end method

.method private getFitnessData(D)Ljava/lang/String;
    .locals 3
    .param p1, "fitnessValues"    # D

    .prologue
    .line 427
    const-string v0, ""

    .line 429
    .local v0, "sFitness":Ljava/lang/String;
    const-wide/high16 v1, 0x401c000000000000L    # 7.0

    cmpg-double v1, v1, p1

    if-gtz v1, :cond_0

    .line 430
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->mContent:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09017a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 446
    :goto_0
    return-object v0

    .line 431
    :cond_0
    const-wide/high16 v1, 0x4018000000000000L    # 6.0

    cmpg-double v1, v1, p1

    if-gtz v1, :cond_1

    .line 432
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->mContent:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090179

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 433
    :cond_1
    const-wide/high16 v1, 0x4014000000000000L    # 5.0

    cmpg-double v1, v1, p1

    if-gtz v1, :cond_2

    .line 434
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->mContent:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090178

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 435
    :cond_2
    const-wide/high16 v1, 0x4010000000000000L    # 4.0

    cmpg-double v1, v1, p1

    if-gtz v1, :cond_3

    .line 436
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->mContent:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090177

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 437
    :cond_3
    const-wide/high16 v1, 0x4008000000000000L    # 3.0

    cmpg-double v1, v1, p1

    if-gtz v1, :cond_4

    .line 438
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->mContent:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090a92

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 439
    :cond_4
    const-wide/high16 v1, 0x4000000000000000L    # 2.0

    cmpg-double v1, v1, p1

    if-gtz v1, :cond_5

    .line 440
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->mContent:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090176

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 441
    :cond_5
    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    cmpg-double v1, v1, p1

    if-gtz v1, :cond_6

    .line 442
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->mContent:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09068d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 444
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->mContent:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09006a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private initChartStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V
    .locals 14
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    .prologue
    .line 339
    new-instance v6, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v6}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 340
    .local v6, "xTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v9, 0x41700000    # 15.0f

    iget v10, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->scaledDensity:F

    mul-float/2addr v9, v10

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 341
    const/4 v9, 0x1

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 342
    const/16 v9, 0xff

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v6, v9, v10, v11, v12}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 343
    const/4 v9, 0x0

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 345
    new-instance v8, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v8}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 346
    .local v8, "yTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v9, 0x41700000    # 15.0f

    iget v10, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->scaledDensity:F

    mul-float/2addr v9, v10

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 348
    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 349
    const/16 v9, 0xff

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 350
    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 352
    new-instance v7, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v7}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 353
    .local v7, "yLabelTitleStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v9, 0x41400000    # 12.0f

    iget v10, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->scaledDensity:F

    mul-float/2addr v9, v10

    invoke-virtual {v7, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 354
    const/4 v9, 0x1

    invoke-virtual {v7, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 355
    const/16 v9, 0xff

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v7, v9, v10, v11, v12}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 356
    const/4 v9, 0x0

    invoke-virtual {v7, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 358
    new-instance v4, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 359
    .local v4, "handlerItemTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v9, 0x41900000    # 18.0f

    iget v10, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->scaledDensity:F

    mul-float/2addr v9, v10

    invoke-virtual {v4, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 360
    const/4 v9, 0x1

    invoke-virtual {v4, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 361
    const/16 v9, 0xff

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v4, v9, v10, v11, v12}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 362
    const/4 v9, 0x1

    invoke-virtual {v4, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 364
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/high16 v12, 0x41800000    # 16.0f

    iget v13, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->density:F

    mul-float/2addr v12, v13

    invoke-virtual {p1, v9, v10, v11, v12}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPadding(FFFF)V

    .line 368
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0203b2

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 369
    .local v1, "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .end local v1    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 370
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setGraphBackgroundImage(Landroid/graphics/Bitmap;)V

    .line 373
    const/high16 v9, 0x3f800000    # 1.0f

    iget v10, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->density:F

    mul-float/2addr v9, v10

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisWidth(FI)V

    .line 374
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisMarkingVisible(ZI)V

    .line 375
    const v9, -0xcbb1ec

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisColor(II)V

    .line 376
    const/4 v9, 0x0

    invoke-virtual {p1, v6, v9}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 377
    const/high16 v9, 0x41f00000    # 30.0f

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTextSpace(FI)V

    .line 380
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisVisible(ZI)V

    .line 381
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisMarkingVisible(ZI)V

    .line 382
    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextVisible(ZI)V

    .line 383
    const/4 v9, 0x0

    invoke-virtual {p1, v8, v9}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 384
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const v10, 0x7f090a0b

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitle(Ljava/lang/String;I)V

    .line 385
    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelAlign(II)V

    .line 386
    const/4 v9, 0x0

    invoke-virtual {p1, v7, v9}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitleTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 387
    iget v9, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->density:F

    const/high16 v10, 0x41000000    # 8.0f

    mul-float/2addr v9, v10

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextSpace(FI)V

    .line 388
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLineVisible(ZI)V

    .line 391
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0203c2

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 392
    .restart local v1    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .end local v1    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 393
    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemImage(Landroid/graphics/Bitmap;)V

    .line 394
    const/4 v9, 0x0

    invoke-virtual {p1, v9}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemStrokeWidth(F)V

    .line 395
    invoke-virtual {p1, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 396
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->handlerListener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    invoke-virtual {p1, v9}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerListener(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;)V

    .line 397
    const/high16 v9, 0x41f80000    # 31.0f

    iget v10, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->density:F

    mul-float/2addr v9, v10

    invoke-virtual {p1, v9}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 398
    const/high16 v9, 0x42480000    # 50.0f

    iget v10, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->density:F

    mul-float/2addr v9, v10

    invoke-virtual {p1, v9}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemHeight(F)V

    .line 399
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0a0057

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    int-to-float v9, v9

    const/high16 v10, 0x3f000000    # 0.5f

    mul-float/2addr v9, v10

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0a0058

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    int-to-float v10, v10

    add-float/2addr v9, v10

    invoke-virtual {p1, v9}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemOffset(F)V

    .line 400
    const/high16 v9, 0x41a00000    # 20.0f

    invoke-virtual {p1, v9}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextOffset(F)V

    .line 401
    const-wide/16 v9, 0x7d0

    invoke-virtual {p1, v9, v10}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerTimeOutDelay(J)V

    .line 404
    const/16 v9, 0x12c

    const/16 v10, 0x12c

    sget-object v11, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v9, v10, v11}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 405
    .local v5, "lineBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0203c4

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 406
    .local v3, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v5}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 407
    .local v2, "canvas":Landroid/graphics/Canvas;
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v2}, Landroid/graphics/Canvas;->getWidth()I

    move-result v11

    invoke-virtual {v2}, Landroid/graphics/Canvas;->getHeight()I

    move-result v12

    invoke-virtual {v3, v9, v10, v11, v12}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 408
    invoke-virtual {v3, v2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 409
    invoke-virtual {p1, v5}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerLineImage(Landroid/graphics/Bitmap;)V

    .line 411
    const/4 v9, 0x0

    invoke-virtual {p1, v9}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPopupEnable(Z)V

    .line 412
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setMarkingLineBase(I)V

    .line 413
    const/4 v9, 0x0

    invoke-virtual {p1, v9}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setLegendVisible(Z)V

    .line 414
    iget v9, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->density:F

    const/high16 v10, 0x42080000    # 34.0f

    mul-float/2addr v9, v10

    float-to-int v9, v9

    invoke-virtual {p1, v9}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setSeparatorTextSpacingTop(I)V

    .line 415
    const/4 v9, 0x0

    invoke-virtual {p1, v9}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerTooltipEnable(Z)V

    .line 416
    return-void
.end method

.method private initSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V
    .locals 8
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    .prologue
    const/4 v7, 0x1

    .line 305
    new-instance v4, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;

    invoke-direct {v4}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;-><init>()V

    .line 306
    .local v4, "seriesStyle":Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09002c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setSeriesName(Ljava/lang/String;)V

    .line 308
    const/4 v5, 0x0

    const/high16 v6, 0x40e00000    # 7.0f

    invoke-virtual {v4, v7, v7, v5, v6}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setFixedMinMaxY(ZZFF)V

    .line 310
    new-instance v1, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 311
    .local v1, "goalLineTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v5, 0x41900000    # 18.0f

    iget v6, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->scaledDensity:F

    mul-float/2addr v5, v6

    invoke-virtual {v1, v5}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 312
    invoke-virtual {v1, v7}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 313
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070162

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setColor(I)V

    .line 314
    const/4 v5, 0x2

    invoke-virtual {v1, v5}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 316
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setGoalLineVisible(Z)V

    .line 317
    const/high16 v5, 0x44960000    # 1200.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setGoalLineValue(F)V

    .line 318
    invoke-virtual {v4, v1}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setGoalLineTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 320
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0203c0

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 321
    .local v0, "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .end local v0    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 323
    .local v3, "normalBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0203c1

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 324
    .restart local v0    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .end local v0    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 326
    .local v2, "handlerOverBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v4, v7}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingVisible(Z)V

    .line 327
    invoke-virtual {v4, v3}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingNormalImage(Landroid/graphics/Bitmap;)V

    .line 328
    invoke-virtual {v4, v2}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingHandlerOverOutRangeImage(Landroid/graphics/Bitmap;)V

    .line 329
    const/high16 v5, 0x41a00000    # 20.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingSize(F)V

    .line 331
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070163

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setLineColor(I)V

    .line 332
    const/high16 v5, 0x40a00000    # 5.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setLineThickness(F)V

    .line 334
    invoke-virtual {p1, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->addSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;)V

    .line 335
    return-void
.end method

.method private refreshChartView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 10
    .param p1, "periodStart"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/high16 v2, 0x40000000    # 2.0f

    .line 189
    new-instance v6, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    invoke-direct {v6, v3, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;-><init>(II)V

    .line 191
    .local v6, "chartStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->initSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V

    .line 193
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->initChartStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getSize()I

    move-result v0

    if-nez v0, :cond_1

    .line 235
    :cond_0
    :goto_0
    return-void

    .line 199
    :cond_1
    new-instance v7, Lcom/samsung/android/sdk/chart/series/SchartDataSet;

    invoke-direct {v7}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;-><init>()V

    .line 200
    .local v7, "dataSet":Lcom/samsung/android/sdk/chart/series/SchartDataSet;
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v7, v0}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->addSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)V

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    if-eqz v0, :cond_2

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v9

    .line 204
    .local v9, "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-virtual {v9, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerListener(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;)V

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    invoke-virtual {v0, v9}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 206
    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    .line 209
    .end local v9    # "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    :cond_2
    new-instance v0, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1, v6, v7}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/samsung/android/sdk/chart/series/SchartDataSet;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    .line 211
    new-instance v8, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;

    invoke-direct {v8}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;-><init>()V

    .line 212
    .local v8, "interaction":Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;
    invoke-virtual {v8, v4}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setScaleZoomInteractionEnabled(Z)V

    .line 213
    invoke-virtual {v8, v3}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInteractionEnabled(Z)V

    .line 214
    invoke-virtual {v8, v4}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setPatialZoomInteractionEnabled(Z)V

    .line 216
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p3, v0, :cond_3

    .line 218
    const/high16 v0, 0x40800000    # 4.0f

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInRate(F)V

    .line 219
    invoke-virtual {v8, v2}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomOutRate(F)V

    .line 232
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;->setChartInteraction(Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;)V

    .line 233
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    const/4 v1, 0x5

    const-wide v2, 0x4fd292000L

    sub-long v2, p1, v2

    long-to-double v2, v2

    sget v4, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->MARKING_INTERVAL:I

    const/16 v5, 0xc

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;->setStartVisual(IDII)V

    goto :goto_0

    .line 221
    :cond_3
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p3, v0, :cond_4

    .line 223
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInRate(F)V

    .line 224
    const v0, 0x408db6db

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomOutRate(F)V

    goto :goto_1

    .line 228
    :cond_4
    invoke-virtual {v8, v2}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInRate(F)V

    .line 229
    invoke-virtual {v8, v2}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomOutRate(F)V

    goto :goto_1
.end method

.method private setUpDataSet()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v2, 0x0

    .line 135
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/_private/BaseContract$FitnessChart;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "create_time ASC "

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 136
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 137
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 138
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_0

    .line 139
    const-string v3, "fitness_class"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 140
    .local v7, "mFitness":I
    const-string v3, "create_time"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    .line 142
    .local v1, "mTime":J
    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    .line 143
    .local v5, "point":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/Double;>;"
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 145
    .local v0, "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 146
    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 148
    int-to-double v3, v7

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JDLjava/util/LinkedList;)V

    .line 149
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    .line 151
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 153
    .end local v0    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .end local v1    # "mTime":J
    .end local v5    # "point":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/Double;>;"
    .end local v7    # "mFitness":I
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 185
    :cond_1
    return-void
.end method


# virtual methods
.method protected initGraphLegendArea()Landroid/view/View;
    .locals 4

    .prologue
    .line 456
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 457
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f0300b6

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->mLegendView:Landroid/view/View;

    .line 458
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->mLegendView:Landroid/view/View;

    const v3, 0x7f080355

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 459
    .local v0, "fisrtLayOut":Landroid/widget/LinearLayout;
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 460
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->mLegendView:Landroid/view/View;

    const v3, 0x7f080356

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->switchToWalkingSummary:Landroid/widget/ImageButton;

    .line 461
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->switchToWalkingSummary:Landroid/widget/ImageButton;

    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 471
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->mLegendView:Landroid/view/View;

    return-object v2
.end method

.method protected initGraphView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
    .locals 2
    .param p1, "periodStart"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 119
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 120
    iput-object p3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 122
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->setUpDataSet()V

    .line 124
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->refreshChartView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getSize()I

    move-result v0

    if-nez v0, :cond_1

    .line 127
    :cond_0
    const v0, 0x7f02031e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->showNoData(I)V

    .line 128
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 131
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    goto :goto_0
.end method

.method protected initInformationArea(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
    .locals 2
    .param p1, "periodStart"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 421
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->exercisemateFitnessInformationArea:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;

    .line 423
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->exercisemateFitnessInformationArea:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->density:F

    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->scaledDensity:F

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->scaledDensity:F

    .line 106
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    .line 108
    .local v0, "unitSettingHelper":Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->exerciseUnit:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 110
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900b9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->exerciseUnit:Ljava/lang/String;

    .line 112
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;->mContent:Landroid/app/Activity;

    .line 113
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method
