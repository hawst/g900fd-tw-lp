.class public interface abstract Lcom/sec/android/app/shealth/framework/ui/datareceiver/CommonService;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/ui/datareceiver/CommonService$Stub;
    }
.end annotation


# virtual methods
.method public abstract getJoinedList()[Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract joinWithDevice(Ljava/lang/String;Lcom/sec/android/app/shealth/framework/ui/datareceiver/CommonSensorServiceCallback;I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract joinWithPairedDevice(Ljava/lang/String;Lcom/sec/android/app/shealth/framework/ui/datareceiver/CommonSensorServiceCallback;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract leaveDevice(Ljava/lang/String;Lcom/sec/android/app/shealth/framework/ui/datareceiver/CommonSensorServiceCallback;Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract stopCommonService()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
