.class final Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$1;
.super Ljava/util/HashMap;
.source "FoodTrackerMainContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Lcom/sec/android/app/shealth/food/constants/MealType;",
        "Lcom/sec/android/app/shealth/food/app/UserActionLog;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(I)V
    .locals 2
    .param p1, "x0"    # I

    .prologue
    .line 92
    invoke-direct {p0, p1}, Ljava/util/HashMap;-><init>(I)V

    .line 94
    sget-object v0, Lcom/sec/android/app/shealth/food/constants/MealType;->BREAKFAST:Lcom/sec/android/app/shealth/food/constants/MealType;

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_INPUT_BREAKFAST_MEAL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    sget-object v0, Lcom/sec/android/app/shealth/food/constants/MealType;->LUNCH:Lcom/sec/android/app/shealth/food/constants/MealType;

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_INPUT_LUNCH_MEAL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v0, Lcom/sec/android/app/shealth/food/constants/MealType;->DINNER:Lcom/sec/android/app/shealth/food/constants/MealType;

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_INPUT_DINNER_MEAL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    sget-object v0, Lcom/sec/android/app/shealth/food/constants/MealType;->OTHER:Lcom/sec/android/app/shealth/food/constants/MealType;

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_INPUT_SNACKS_MEAL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    return-void
.end method
