.class public Lcom/sec/android/app/shealth/heartrate/HRMGIFView;
.super Landroid/view/View;
.source "HRMGIFView.java"


# static fields
.field private static final CACHE_SIZE:I = 0x100000

.field private static final DEFAULT_MOVIEW_DURATION:I = 0x3e8


# instance fields
.field private final MOVIE_TAG:Ljava/lang/String;

.field private mCurrentAnimationTime:I

.field private mLeft:F

.field private mMeasuredMovieHeight:I

.field private mMeasuredMovieWidth:I

.field private mMovie:Landroid/graphics/Movie;

.field private mMovieGreen:Landroid/graphics/Movie;

.field private mMovieOrange:Landroid/graphics/Movie;

.field private mMovieResourceId:I

.field private mMovieStart:J

.field private volatile mPaused:Z

.field private mResID:[I

.field private mScale:F

.field private mTop:F

.field private mVisible:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 62
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovieResourceId:I

    .line 24
    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mResID:[I

    .line 26
    iput-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovieGreen:Landroid/graphics/Movie;

    .line 27
    iput-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovieOrange:Landroid/graphics/Movie;

    .line 30
    iput v2, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mCurrentAnimationTime:I

    .line 49
    const-string/jumbo v1, "movie"

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->MOVIE_TAG:Ljava/lang/String;

    .line 50
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mPaused:Z

    .line 51
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mVisible:Z

    .line 66
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->setViewAttributes(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    :goto_0
    return-void

    .line 68
    :catch_0
    move-exception v0

    .line 71
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 24
    nop

    :array_0
    .array-data 4
        -0x1
        -0x1
    .end array-data
.end method

.method private drawMovieFrame(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 340
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovie:Landroid/graphics/Movie;

    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mCurrentAnimationTime:I

    invoke-virtual {v0, v1}, Landroid/graphics/Movie;->setTime(I)Z

    .line 342
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->save(I)I

    .line 343
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mScale:F

    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mScale:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 344
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovie:Landroid/graphics/Movie;

    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mLeft:F

    iget v2, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mScale:F

    div-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mTop:F

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mScale:F

    div-float/2addr v2, v3

    invoke-virtual {v0, p1, v1, v2}, Landroid/graphics/Movie;->draw(Landroid/graphics/Canvas;FF)V

    .line 345
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 346
    return-void
.end method

.method private initMovieRes()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 126
    const/4 v1, 0x0

    .line 127
    .local v1, "inputStreamGreen":Ljava/io/InputStream;
    const/4 v2, 0x0

    .line 128
    .local v2, "inputStreamOrange":Ljava/io/InputStream;
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mResID:[I

    aget v3, v3, v5

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mResID:[I

    aget v3, v3, v6

    if-ne v3, v4, :cond_1

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 132
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mResID:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 133
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovieGreen:Landroid/graphics/Movie;

    if-nez v3, :cond_2

    if-eqz v1, :cond_2

    .line 134
    invoke-static {v1}, Landroid/graphics/Movie;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Movie;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovieGreen:Landroid/graphics/Movie;

    .line 136
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mResID:[I

    const/4 v5, 0x1

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    .line 137
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovieOrange:Landroid/graphics/Movie;

    if-nez v3, :cond_3

    if-eqz v2, :cond_3

    .line 138
    invoke-static {v2}, Landroid/graphics/Movie;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Movie;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovieOrange:Landroid/graphics/Movie;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    :cond_3
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 146
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 148
    :catch_0
    move-exception v0

    .line 150
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 143
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    .line 145
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 146
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 151
    :goto_1
    throw v3

    .line 148
    :catch_1
    move-exception v0

    .line 150
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method private invalidateView()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 307
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mVisible:Z

    if-eqz v0, :cond_0

    .line 308
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    .line 309
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->postInvalidateOnAnimation()V

    .line 314
    :cond_0
    :goto_0
    return-void

    .line 311
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->invalidate()V

    goto :goto_0
.end method

.method private setViewAttributes(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 83
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 86
    :cond_0
    return-void
.end method

.method private updateAnimationTime()V
    .locals 7

    .prologue
    .line 320
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    .line 322
    .local v1, "now":J
    iget-wide v3, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovieStart:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-nez v3, :cond_0

    .line 323
    iput-wide v1, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovieStart:J

    .line 326
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovie:Landroid/graphics/Movie;

    invoke-virtual {v3}, Landroid/graphics/Movie;->duration()I

    move-result v0

    .line 328
    .local v0, "dur":I
    if-nez v0, :cond_1

    .line 329
    const/16 v0, 0x3e8

    .line 332
    :cond_1
    iget-wide v3, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovieStart:J

    sub-long v3, v1, v3

    int-to-long v5, v0

    rem-long/2addr v3, v5

    long-to-int v3, v3

    iput v3, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mCurrentAnimationTime:I

    .line 333
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 376
    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovie:Landroid/graphics/Movie;

    .line 377
    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovieGreen:Landroid/graphics/Movie;

    .line 378
    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovieOrange:Landroid/graphics/Movie;

    .line 379
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovieResourceId:I

    .line 380
    return-void
.end method

.method public getMovie()Landroid/graphics/Movie;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovie:Landroid/graphics/Movie;

    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 288
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovie:Landroid/graphics/Movie;

    if-eqz v0, :cond_0

    .line 289
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mPaused:Z

    if-nez v0, :cond_1

    .line 290
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->updateAnimationTime()V

    .line 291
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->drawMovieFrame(Landroid/graphics/Canvas;)V

    .line 292
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->invalidateView()V

    .line 297
    :cond_0
    :goto_0
    return-void

    .line 294
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->drawMovieFrame(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 275
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 280
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMeasuredMovieWidth:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr v0, v2

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mLeft:F

    .line 281
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->getHeight()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMeasuredMovieHeight:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr v0, v2

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mTop:F

    .line 283
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mVisible:Z

    .line 284
    return-void

    .line 283
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 10
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 225
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovie:Landroid/graphics/Movie;

    if-eqz v8, :cond_2

    .line 226
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovie:Landroid/graphics/Movie;

    invoke-virtual {v8}, Landroid/graphics/Movie;->width()I

    move-result v5

    .line 227
    .local v5, "movieWidth":I
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovie:Landroid/graphics/Movie;

    invoke-virtual {v8}, Landroid/graphics/Movie;->height()I

    move-result v4

    .line 232
    .local v4, "movieHeight":I
    const/high16 v6, 0x3f800000    # 1.0f

    .line 233
    .local v6, "scaleH":F
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 235
    .local v3, "measureModeWidth":I
    if-eqz v3, :cond_0

    .line 236
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 237
    .local v1, "maximumWidth":I
    if-le v5, v1, :cond_0

    .line 238
    int-to-float v8, v5

    int-to-float v9, v1

    div-float v6, v8, v9

    .line 245
    .end local v1    # "maximumWidth":I
    :cond_0
    const/high16 v7, 0x3f800000    # 1.0f

    .line 246
    .local v7, "scaleW":F
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 248
    .local v2, "measureModeHeight":I
    if-eqz v2, :cond_1

    .line 249
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 250
    .local v0, "maximumHeight":I
    if-le v4, v0, :cond_1

    .line 251
    int-to-float v8, v4

    int-to-float v9, v0

    div-float v7, v8, v9

    .line 258
    .end local v0    # "maximumHeight":I
    :cond_1
    const/high16 v8, 0x3f800000    # 1.0f

    invoke-static {v6, v7}, Ljava/lang/Math;->max(FF)F

    move-result v9

    div-float/2addr v8, v9

    iput v8, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mScale:F

    .line 260
    int-to-float v8, v5

    iget v9, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mScale:F

    mul-float/2addr v8, v9

    float-to-int v8, v8

    iput v8, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMeasuredMovieWidth:I

    .line 261
    int-to-float v8, v4

    iget v9, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mScale:F

    mul-float/2addr v8, v9

    float-to-int v8, v8

    iput v8, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMeasuredMovieHeight:I

    .line 263
    iget v8, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMeasuredMovieWidth:I

    iget v9, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMeasuredMovieHeight:I

    invoke-virtual {p0, v8, v9}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->setMeasuredDimension(II)V

    .line 271
    .end local v2    # "measureModeHeight":I
    .end local v3    # "measureModeWidth":I
    .end local v4    # "movieHeight":I
    .end local v5    # "movieWidth":I
    .end local v6    # "scaleH":F
    .end local v7    # "scaleW":F
    :goto_0
    return-void

    .line 269
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->getSuggestedMinimumWidth()I

    move-result v8

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->getSuggestedMinimumHeight()I

    move-result v9

    invoke-virtual {p0, v8, v9}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public onScreenStateChanged(I)V
    .locals 1
    .param p1, "screenState"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 351
    invoke-super {p0, p1}, Landroid/view/View;->onScreenStateChanged(I)V

    .line 352
    if-ne p1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mVisible:Z

    .line 353
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->invalidateView()V

    .line 354
    return-void

    .line 352
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 1
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 359
    invoke-super {p0, p1, p2}, Landroid/view/View;->onVisibilityChanged(Landroid/view/View;I)V

    .line 360
    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mVisible:Z

    .line 361
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->invalidateView()V

    .line 362
    return-void

    .line 360
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 366
    invoke-super {p0, p1}, Landroid/view/View;->onWindowVisibilityChanged(I)V

    .line 367
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mVisible:Z

    .line 368
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->invalidateView()V

    .line 369
    return-void

    .line 367
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setMovie(Landroid/graphics/Movie;)V
    .locals 0
    .param p1, "movie"    # Landroid/graphics/Movie;

    .prologue
    .line 201
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovie:Landroid/graphics/Movie;

    .line 202
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->requestLayout()V

    .line 203
    return-void
.end method

.method public setMovieResource(I)V
    .locals 4
    .param p1, "movieResId"    # I

    .prologue
    .line 172
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mPaused:Z

    .line 173
    iget v2, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovieResourceId:I

    if-ne v2, p1, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovie:Landroid/graphics/Movie;

    if-eqz v2, :cond_0

    .line 194
    :goto_0
    return-void

    .line 175
    :cond_0
    iput p1, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovieResourceId:I

    .line 176
    const/4 v1, 0x0

    .line 179
    .local v1, "in":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovieResourceId:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 180
    invoke-static {v1}, Landroid/graphics/Movie;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Movie;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovie:Landroid/graphics/Movie;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 186
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 193
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->requestLayout()V

    goto :goto_0

    .line 188
    :catch_0
    move-exception v0

    .line 190
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 184
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    .line 186
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 191
    :goto_2
    throw v2

    .line 188
    :catch_1
    move-exception v0

    .line 190
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method

.method public setMovieTime(I)V
    .locals 0
    .param p1, "time"    # I

    .prologue
    .line 218
    iput p1, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mCurrentAnimationTime:I

    .line 219
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->invalidate()V

    .line 220
    return-void
.end method

.method public setMultiMovieResource([IZ)V
    .locals 7
    .param p1, "resID"    # [I
    .param p2, "isGreen"    # Z

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 89
    const/4 v1, 0x0

    .line 90
    .local v1, "inputStreamGreen":Ljava/io/InputStream;
    const/4 v2, 0x0

    .line 91
    .local v2, "inputStreamOrange":Ljava/io/InputStream;
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mResID:[I

    aget v4, p1, v5

    aput v4, v3, v5

    .line 92
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mResID:[I

    aget v4, p1, v6

    aput v4, v3, v5

    .line 93
    iput-boolean v5, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mPaused:Z

    .line 96
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/4 v4, 0x0

    aget v4, p1, v4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 97
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovieGreen:Landroid/graphics/Movie;

    if-nez v3, :cond_0

    if-eqz v1, :cond_0

    .line 98
    invoke-static {v1}, Landroid/graphics/Movie;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Movie;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovieGreen:Landroid/graphics/Movie;

    .line 100
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/4 v4, 0x1

    aget v4, p1, v4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    .line 101
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovieOrange:Landroid/graphics/Movie;

    if-nez v3, :cond_1

    if-eqz v2, :cond_1

    .line 102
    invoke-static {v2}, Landroid/graphics/Movie;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Movie;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovieOrange:Landroid/graphics/Movie;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109
    :cond_1
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 110
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 118
    :goto_0
    if-eqz p2, :cond_2

    .line 119
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovieGreen:Landroid/graphics/Movie;

    iput-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovie:Landroid/graphics/Movie;

    .line 122
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->invalidate()V

    .line 123
    return-void

    .line 112
    :catch_0
    move-exception v0

    .line 114
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 107
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    .line 109
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 110
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 115
    :goto_2
    throw v3

    .line 112
    :catch_1
    move-exception v0

    .line 114
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 121
    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovieOrange:Landroid/graphics/Movie;

    iput-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovie:Landroid/graphics/Movie;

    goto :goto_1
.end method

.method public setPulseAnimationColor(Z)V
    .locals 1
    .param p1, "isGreen"    # Z

    .prologue
    .line 156
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mPaused:Z

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovieGreen:Landroid/graphics/Movie;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovieOrange:Landroid/graphics/Movie;

    if-nez v0, :cond_1

    .line 158
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->initMovieRes()V

    .line 160
    :cond_1
    if-eqz p1, :cond_2

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovieGreen:Landroid/graphics/Movie;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovie:Landroid/graphics/Movie;

    .line 164
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->requestLayout()V

    .line 165
    return-void

    .line 163
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovieOrange:Landroid/graphics/Movie;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mMovie:Landroid/graphics/Movie;

    goto :goto_0
.end method

.method public stopAnimation()V
    .locals 1

    .prologue
    .line 372
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->mPaused:Z

    .line 373
    return-void
.end method
