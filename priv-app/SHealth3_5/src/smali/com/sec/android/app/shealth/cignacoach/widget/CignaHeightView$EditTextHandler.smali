.class Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;
.super Ljava/lang/Object;
.source "CignaHeightView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EditTextHandler"
.end annotation


# instance fields
.field pos:I

.field preData:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)V
    .locals 0

    .prologue
    .line 390
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$1;

    .prologue
    .line 390
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 451
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 454
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$300(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getSelectionStart()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;->pos:I

    .line 455
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$300(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;->preData:Ljava/lang/String;

    .line 456
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 9
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    const/4 v8, 0x1

    .line 396
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->isHeighUnitCM()Z
    invoke-static {v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$600(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 397
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$300(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 398
    .local v4, "str":Ljava/lang/String;
    const-string v6, "\\."

    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 399
    .local v0, "array":[Ljava/lang/String;
    array-length v6, v0

    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    .line 400
    aget-object v6, v0, v8

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-le v6, v8, :cond_0

    .line 401
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$300(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;->preData:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 403
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$300(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;->pos:I

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setSelection(I)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 411
    .end local v0    # "array":[Ljava/lang/String;
    .end local v4    # "str":Ljava/lang/String;
    :cond_0
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightUnit:I
    invoke-static {v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$200(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)I

    move-result v6

    const v7, 0x249f1

    if-ne v6, v7, :cond_2

    .line 413
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->getHeightTextValue()F
    invoke-static {v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$700(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    .line 414
    .local v5, "value":Ljava/lang/Float;
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v6

    const/high16 v7, -0x40800000    # -1.0f

    cmpl-float v6, v6, v7

    if-eqz v6, :cond_1

    .line 416
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v6

    const/high16 v7, 0x43960000    # 300.0f

    cmpl-float v6, v6, v7

    if-lez v6, :cond_1

    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mListener:Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;
    invoke-static {v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$800(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 418
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->setMinValueText()V

    .line 419
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$300(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v6

    const/4 v7, 0x5

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setSelection(I)V

    .line 446
    .end local v5    # "value":Ljava/lang/Float;
    :cond_1
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    iget-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;->preData:Ljava/lang/String;

    iget v8, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;->pos:I

    # invokes: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->setDatalimit(Ljava/lang/String;I)V
    invoke-static {v6, v7, v8}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$900(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;Ljava/lang/String;I)V

    .line 447
    return-void

    .line 404
    .restart local v0    # "array":[Ljava/lang/String;
    .restart local v4    # "str":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 405
    .local v1, "e":Ljava/lang/IndexOutOfBoundsException;
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$300(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v7}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$300(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-interface {v7}, Landroid/text/Editable;->length()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setSelection(I)V

    .line 406
    invoke-virtual {v1}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0

    .line 426
    .end local v0    # "array":[Ljava/lang/String;
    .end local v1    # "e":Ljava/lang/IndexOutOfBoundsException;
    .end local v4    # "str":Ljava/lang/String;
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$300(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 427
    .local v2, "ftString":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$400(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 428
    .local v3, "inchString":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_1

    .line 432
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    if-le v6, v8, :cond_3

    .line 433
    const/4 v6, 0x0

    invoke-virtual {v2, v6, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 434
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$300(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v6

    invoke-virtual {v6, v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 435
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$300(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v6

    invoke-virtual {v6, v8}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setSelection(I)V

    .line 438
    :cond_3
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    const/16 v7, 0x9

    if-gt v6, v7, :cond_4

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    const/16 v7, 0xb

    if-le v6, v7, :cond_1

    .line 440
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->setMinValueText()V

    goto :goto_1
.end method
