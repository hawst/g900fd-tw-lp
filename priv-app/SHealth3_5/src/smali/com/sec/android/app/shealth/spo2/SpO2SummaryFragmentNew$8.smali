.class Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$8;
.super Ljava/lang/Object;
.source "SpO2SummaryFragmentNew.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V
    .locals 0

    .prologue
    .line 822
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$8;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 826
    sget-object v1, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "mSpO2OnClcickListener"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 827
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 860
    :goto_0
    return-void

    .line 830
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$8;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$2600(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth.spo2"

    const-string v3, "SP04"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 831
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$8;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # invokes: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->showGraphFragment()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$2700(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V

    goto :goto_0

    .line 835
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$8;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$8;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$2600(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 839
    :sswitch_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$8;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    sget-object v2, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->effectAudio:[I

    aget v2, v2, v4

    # invokes: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->playSound(I)V
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$2800(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;I)V

    .line 840
    invoke-virtual {p1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 841
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$8;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->startSensorAndDelayUIForDetectedFinger(I)V

    .line 842
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$8;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->updateBpmDataView(Z)V

    .line 843
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$8;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # invokes: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->refreshFragmentFocusables()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$2900(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V

    goto :goto_0

    .line 846
    :sswitch_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$8;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->deleteLatestData(J)I

    move-result v0

    .line 847
    .local v0, "noOfEntrieDeleted":I
    if-ltz v0, :cond_0

    .line 848
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$8;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$2600(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$8;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$2600(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090f9d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v4}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 852
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$8;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    sget-object v2, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->effectAudio:[I

    const/4 v3, 0x2

    aget v2, v2, v3

    # invokes: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->playSound(I)V
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$2800(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;I)V

    .line 853
    invoke-virtual {p1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 854
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$8;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->startSensorAndDelayUIForDetectedFinger(I)V

    .line 855
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$8;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->updateBpmDataView(Z)V

    .line 856
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$8;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # invokes: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->refreshFragmentFocusables()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$3000(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V

    goto/16 :goto_0

    .line 827
    :sswitch_data_0
    .sparse-switch
        0x7f0805c0 -> :sswitch_2
        0x7f0805e5 -> :sswitch_1
        0x7f0805ef -> :sswitch_0
        0x7f0809b7 -> :sswitch_3
    .end sparse-switch
.end method
