.class Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;
.super Ljava/lang/Object;
.source "PortionSizePopup.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->initDropDownButtonInPrecisePopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

.field final synthetic val$unitsMap:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 409
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    iput-object p2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;->val$unitsMap:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 413
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeInputModule:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$000(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "0"

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeInputModule:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$000(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 415
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mEmptyFieldListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;->processEmptyField()V

    .line 442
    :goto_0
    return-void

    .line 420
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeInputModule:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$000(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->clearFocus()V

    .line 421
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    .line 424
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$1100(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setScrollable(Z)V

    .line 425
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$1100(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$200(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a081d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setWidth(I)V

    .line 427
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$1100(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mUnitDropDownButton:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$400(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getHeight()I

    move-result v1

    rsub-int/lit8 v1, v1, 0x0

    invoke-virtual {v0, v3, v1}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->show(II)V

    .line 428
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$1100(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5$1;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setOnItemClickedListener(Lcom/sec/android/app/shealth/common/commonui/ListPopup$OnItemClickedListener;)V

    goto :goto_0
.end method
