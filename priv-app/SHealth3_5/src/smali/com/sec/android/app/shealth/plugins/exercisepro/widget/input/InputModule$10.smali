.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$10;
.super Ljava/lang/Object;
.source "InputModule.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->showAbnormalGoalWarningPopupDistance()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

.field final synthetic val$isUnitKm:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;Z)V
    .locals 0

    .prologue
    .line 951
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    iput-boolean p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$10;->val$isUnitKm:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 6
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    const/4 v5, 0x0

    .line 954
    invoke-virtual {p3}, Landroid/app/Dialog;->dismiss()V

    .line 956
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$10;->val$isUnitKm:Z

    if-eqz v1, :cond_1

    .line 957
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->getValue()F

    move-result v1

    const/high16 v2, 0x447a0000    # 1000.0f

    mul-float/2addr v1, v2

    const v2, 0x4973e580    # 999000.0f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 958
    const v0, 0xf3e58

    .line 962
    .local v0, "goalValue":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    int-to-long v3, v0

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->getKmFromMeterByLocale(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    invoke-virtual {v1, v2, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setValue(FZ)V

    .line 971
    :goto_1
    return-void

    .line 960
    .end local v0    # "goalValue":I
    :cond_0
    const/16 v0, 0x64

    .restart local v0    # "goalValue":I
    goto :goto_0

    .line 964
    .end local v0    # "goalValue":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->getValue()F

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/Utils;->getFloatFromString(Ljava/lang/String;)F

    move-result v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/UnitUtils$UnitConverter;->convertMilesToMeters(F)I

    move-result v1

    const v2, 0xf3e58

    if-le v1, v2, :cond_2

    .line 965
    const v0, 0xf39a1

    .line 969
    .restart local v0    # "goalValue":I
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    int-to-long v3, v0

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->getMilesFromMetersByLocale(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    invoke-virtual {v1, v2, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setValue(FZ)V

    goto :goto_1

    .line 967
    .end local v0    # "goalValue":I
    :cond_2
    const/16 v0, 0xa1

    .restart local v0    # "goalValue":I
    goto :goto_2
.end method
