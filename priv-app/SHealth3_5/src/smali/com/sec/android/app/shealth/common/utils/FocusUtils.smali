.class public Lcom/sec/android/app/shealth/common/utils/FocusUtils;
.super Ljava/lang/Object;
.source "FocusUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/common/utils/FocusUtils$1;,
        Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;,
        Lcom/sec/android/app/shealth/common/utils/FocusUtils$SHealthFocusabilityComparator;,
        Lcom/sec/android/app/shealth/common/utils/FocusUtils$VisibleChildrenProvider;
    }
.end annotation


# static fields
.field public static final ALLOWED_VERTICAL_DEVIATION_OF_CENTER_FOR_FOCUSABLES_IN_SAME_LINE_IN_PX:I = 0x46

.field private static final TAG:Ljava/lang/String;

.field public static final UNSPECIFIED_VIEW_ID:I = -0x1

.field private static specialViewList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private static viewIdCunter:I

.field private static viewListId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/sec/android/app/shealth/common/utils/FocusUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->TAG:Ljava/lang/String;

    .line 36
    const/16 v0, -0x3e8

    sput v0, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->viewIdCunter:I

    .line 37
    const/16 v0, 0x3e8

    sput v0, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->viewListId:I

    .line 89
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->specialViewList:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1089
    return-void
.end method

.method private static addAllFocusableChildrenToList(Landroid/view/ViewGroup;Ljava/util/List;Z)V
    .locals 1
    .param p0, "viewGroup"    # Landroid/view/ViewGroup;
    .param p2, "dynamicIdsRequired"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 108
    .local p1, "focusableViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    if-nez p0, :cond_1

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    invoke-static {p1, p0}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->isViewGroupAlreadyExist(Ljava/util/List;Landroid/view/ViewGroup;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 116
    invoke-virtual {p0, p2}, Landroid/view/ViewGroup;->setMotionEventSplittingEnabled(Z)V

    .line 117
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->isSpecialView(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    .line 119
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->specialViewList:Ljava/util/List;

    if-nez v0, :cond_2

    .line 121
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->specialViewList:Ljava/util/List;

    .line 123
    :cond_2
    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 124
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->specialViewList:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 128
    :cond_3
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->addChildViews(Landroid/view/ViewGroup;Ljava/util/List;Z)V

    goto :goto_0
.end method

.method private static addAllFocusablesToList(Ljava/util/Collection;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1146
    .local p0, "additionalFocusables":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/view/View;>;"
    .local p1, "focusablesList":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    return-void
.end method

.method private static addChildViews(Landroid/view/ViewGroup;Ljava/util/List;Z)V
    .locals 6
    .param p0, "viewGroup"    # Landroid/view/ViewGroup;
    .param p2, "dynamicIdsRequired"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 133
    .local p1, "focusableViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    if-ge v0, v4, :cond_4

    .line 135
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 137
    .local v2, "view":Landroid/view/View;
    invoke-static {p1, v2}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->isViewAlreadyExist(Ljava/util/List;Landroid/view/View;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v2}, Landroid/view/View;->isFocusable()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->isSpecialView(Landroid/view/View;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 140
    if-eqz p2, :cond_0

    .line 142
    sget v4, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->viewListId:I

    add-int/lit8 v5, v4, 0x1

    sput v5, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->viewListId:I

    invoke-virtual {v2, v4}, Landroid/view/View;->setId(I)V

    .line 144
    :cond_0
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 147
    :cond_1
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_3

    .line 149
    instance-of v4, v2, Lcom/sec/android/app/shealth/common/utils/FocusUtils$VisibleChildrenProvider;

    if-eqz v4, :cond_2

    .line 151
    check-cast v2, Lcom/sec/android/app/shealth/common/utils/FocusUtils$VisibleChildrenProvider;

    .end local v2    # "view":Landroid/view/View;
    invoke-interface {v2}, Lcom/sec/android/app/shealth/common/utils/FocusUtils$VisibleChildrenProvider;->getVisibleChildren()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 154
    .local v3, "visibleChild":Landroid/view/View;
    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 157
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "visibleChild":Landroid/view/View;
    .restart local v2    # "view":Landroid/view/View;
    :cond_2
    instance-of v4, v2, Landroid/view/ViewGroup;

    if-eqz v4, :cond_3

    .line 159
    check-cast v2, Landroid/view/ViewGroup;

    .end local v2    # "view":Landroid/view/View;
    invoke-static {v2, p1, p2}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->addAllFocusableChildrenToList(Landroid/view/ViewGroup;Ljava/util/List;Z)V

    .line 133
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 163
    :cond_4
    return-void
.end method

.method private static checkAndCleanUpFocusablesList(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 242
    .local p0, "viewsListToCheck":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 244
    .local v2, "viewsToBeRemoved":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/view/View;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 246
    .local v1, "view":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->isFocusable()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->isViewReallyVisibleToUser(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_2

    .line 248
    :cond_1
    invoke-interface {v2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 250
    :cond_2
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->isViewReallyVisibleToUser(Landroid/view/View;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 252
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_0

    .line 255
    .end local v1    # "view":Landroid/view/View;
    :cond_3
    invoke-interface {p0, v2}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 256
    return-void
.end method

.method public static delegateDirectionsForSpecialView(Landroid/view/ViewGroup;Ljava/util/List;)V
    .locals 6
    .param p0, "specialView"    # Landroid/view/ViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "childList":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/4 v5, 0x0

    .line 430
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-gtz v4, :cond_1

    .line 454
    :cond_0
    :goto_0
    return-void

    .line 434
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 435
    .local v3, "mostRightChild":Landroid/view/View;
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 437
    .local v2, "mostLeftChild":Landroid/view/View;
    invoke-static {p1}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->getFocusLines(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 438
    .local v0, "childFocusLines":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Landroid/view/View;>;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 442
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 444
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    invoke-static {v4, p0}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->delegateNextFocusUp(Landroid/view/View;Landroid/view/View;)V

    .line 442
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 446
    :cond_2
    const/4 v1, 0x0

    :goto_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_3

    .line 448
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    invoke-static {v4, p0}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->delegateNextFocusDown(Landroid/view/View;Landroid/view/View;)V

    .line 446
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 450
    :cond_3
    invoke-static {v2, p0}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->delegateNextFocusLeft(Landroid/view/View;Landroid/view/View;)V

    .line 451
    invoke-static {v3, p0}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->delegateNextFocusRight(Landroid/view/View;Landroid/view/View;)V

    .line 452
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getNextFocusForwardId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setNextFocusForwardId(I)V

    goto :goto_0
.end method

.method private static delegateNextFocusDown(Landroid/view/View;Landroid/view/View;)V
    .locals 2
    .param p0, "viewToGetNewFocusDown"    # Landroid/view/View;
    .param p1, "viewToGetNewFocusDownFrom"    # Landroid/view/View;

    .prologue
    .line 509
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getNextFocusDownId()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 511
    invoke-virtual {p1}, Landroid/view/View;->getNextFocusDownId()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setNextFocusDownId(I)V

    .line 513
    :cond_0
    return-void
.end method

.method private static delegateNextFocusLeft(Landroid/view/View;Landroid/view/View;)V
    .locals 2
    .param p0, "viewToGetNewFocusLeft"    # Landroid/view/View;
    .param p1, "viewToGetNewFocusLeftFrom"    # Landroid/view/View;

    .prologue
    .line 525
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getNextFocusLeftId()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 527
    invoke-virtual {p1}, Landroid/view/View;->getNextFocusLeftId()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setNextFocusLeftId(I)V

    .line 529
    :cond_0
    return-void
.end method

.method private static delegateNextFocusRight(Landroid/view/View;Landroid/view/View;)V
    .locals 2
    .param p0, "viewToGetNewFocusRight"    # Landroid/view/View;
    .param p1, "viewToGetNewFocusRightFrom"    # Landroid/view/View;

    .prologue
    .line 533
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getNextFocusRightId()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 535
    invoke-virtual {p1}, Landroid/view/View;->getNextFocusRightId()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 537
    :cond_0
    return-void
.end method

.method private static delegateNextFocusUp(Landroid/view/View;Landroid/view/View;)V
    .locals 2
    .param p0, "viewToGetNewFocusUp"    # Landroid/view/View;
    .param p1, "viewToGetNewFocusUpFrom"    # Landroid/view/View;

    .prologue
    .line 517
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getNextFocusUpId()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 519
    invoke-virtual {p1}, Landroid/view/View;->getNextFocusUpId()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setNextFocusUpId(I)V

    .line 521
    :cond_0
    return-void
.end method

.method public static getAllFocusableChildViews(Landroid/view/ViewGroup;)Ljava/util/List;
    .locals 2
    .param p0, "viewGroup"    # Landroid/view/ViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 83
    .local v0, "focusableChildren":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->addAllFocusableChildrenToList(Landroid/view/ViewGroup;Ljava/util/List;Z)V

    .line 85
    return-object v0
.end method

.method private static getFocusLines(Ljava/util/List;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation

    .prologue
    .local p0, "focusables":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/4 v10, 0x0

    .line 275
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4, p0}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 276
    .local v4, "focusablesCopy":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Landroid/view/View;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 277
    .local v3, "focusLinesList":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Landroid/view/View;>;>;"
    invoke-static {v4}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->checkAndCleanUpFocusablesList(Ljava/util/List;)V

    .line 279
    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_2

    .line 281
    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v8

    const/4 v9, 0x1

    if-le v8, v9, :cond_0

    .line 283
    invoke-static {v4}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->sortFocusablesList(Ljava/util/List;)V

    .line 285
    :cond_0
    const/4 v5, 0x0

    .line 287
    .local v5, "lastLineIndex":I
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 288
    invoke-virtual {v4}, Ljava/util/LinkedList;->pop()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    .line 289
    .local v6, "mostTopMostLeftFocusable":Landroid/view/View;
    invoke-interface {v3, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/List;

    invoke-interface {v8, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 291
    :goto_0
    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_2

    .line 293
    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/List;

    .line 294
    .local v7, "viewsInCurrentLine":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-virtual {v4}, Ljava/util/LinkedList;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 295
    .local v1, "currentView":Landroid/view/View;
    invoke-interface {v7, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    invoke-static {v8}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->getViewCenterY(Landroid/view/View;)I

    move-result v0

    .line 296
    .local v0, "actualLineY":I
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->getViewCenterY(Landroid/view/View;)I

    move-result v2

    .line 298
    .local v2, "currentViewY":I
    sub-int v8, v2, v0

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    const/16 v9, 0x46

    if-ge v8, v9, :cond_1

    .line 300
    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 304
    :cond_1
    add-int/lit8 v5, v5, 0x1

    .line 305
    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V

    invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 306
    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/List;

    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 311
    .end local v0    # "actualLineY":I
    .end local v1    # "currentView":Landroid/view/View;
    .end local v2    # "currentViewY":I
    .end local v5    # "lastLineIndex":I
    .end local v6    # "mostTopMostLeftFocusable":Landroid/view/View;
    .end local v7    # "viewsInCurrentLine":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    :cond_2
    return-object v3
.end method

.method public static getRectDescribingViewFully(Landroid/view/View;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 390
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->getRectDescribingViewFully(Landroid/view/View;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public static getRectDescribingViewFully(Landroid/view/View;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 4
    .param p0, "view"    # Landroid/view/View;
    .param p1, "tempRect"    # Landroid/graphics/Rect;

    .prologue
    .line 374
    if-eqz p0, :cond_0

    .line 376
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->getRelativeLeft(Landroid/view/View;)I

    move-result v0

    .line 377
    .local v0, "relativeLeft":I
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->getRelativeTop(Landroid/view/View;)I

    move-result v1

    .line 378
    .local v1, "relativeTop":I
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v2

    add-int/2addr v2, v0

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 380
    .end local v0    # "relativeLeft":I
    .end local v1    # "relativeTop":I
    :cond_0
    return-object p1
.end method

.method private static getRelativeLeft(Landroid/view/View;)I
    .locals 3
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 697
    if-nez p0, :cond_0

    .line 698
    const/4 v1, -0x1

    .line 705
    :goto_0
    return v1

    .line 700
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 702
    .local v0, "viewParent":Landroid/view/View;
    invoke-virtual {p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 703
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v1

    goto :goto_0

    .line 705
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->getRelativeLeft(Landroid/view/View;)I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_0
.end method

.method private static getRelativeTop(Landroid/view/View;)I
    .locals 3
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 710
    if-nez p0, :cond_0

    .line 711
    const/4 v1, -0x1

    .line 717
    :goto_0
    return v1

    .line 712
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 714
    .local v0, "viewParent":Landroid/view/View;
    invoke-virtual {p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 715
    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v1

    goto :goto_0

    .line 717
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->getRelativeTop(Landroid/view/View;)I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_0
.end method

.method private static getViewCenterY(Landroid/view/View;)I
    .locals 1
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 358
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->getViewCenterY(Landroid/view/View;Landroid/graphics/Rect;)I

    move-result v0

    return v0
.end method

.method private static getViewCenterY(Landroid/view/View;Landroid/graphics/Rect;)I
    .locals 1
    .param p0, "view"    # Landroid/view/View;
    .param p1, "tempRect"    # Landroid/graphics/Rect;

    .prologue
    .line 363
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->getRectDescribingViewFully(Landroid/view/View;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v0

    return v0
.end method

.method private static isSpecialView(Landroid/view/View;)Z
    .locals 1
    .param p0, "viewGroup"    # Landroid/view/View;

    .prologue
    .line 198
    instance-of v0, p0, Landroid/widget/ListView;

    if-nez v0, :cond_0

    instance-of v0, p0, Landroid/widget/GridView;

    if-nez v0, :cond_0

    instance-of v0, p0, Landroid/widget/ScrollView;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isViewAlreadyExist(Ljava/util/List;Landroid/view/View;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Landroid/view/View;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 204
    .local p0, "focusableViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-interface {p0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static isViewGroupAlreadyExist(Ljava/util/List;Landroid/view/ViewGroup;)Z
    .locals 1
    .param p1, "viewGroup"    # Landroid/view/ViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Landroid/view/ViewGroup;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 210
    .local p0, "focusableViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-interface {p0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static isViewReallyVisibleToUser(Landroid/view/View;)Z
    .locals 5
    .param p0, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 260
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_0

    .line 268
    :goto_0
    return v3

    .line 263
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 264
    .local v0, "checkRect":Landroid/graphics/Rect;
    invoke-virtual {p0, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 266
    iget v4, v0, Landroid/graphics/Rect;->left:I

    if-nez v4, :cond_1

    iget v4, v0, Landroid/graphics/Rect;->right:I

    if-nez v4, :cond_1

    iget v4, v0, Landroid/graphics/Rect;->top:I

    if-nez v4, :cond_1

    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    if-nez v4, :cond_1

    move v1, v2

    .line 268
    .local v1, "viewOrOneOfParentsVisibilityIsNotVisible":Z
    :goto_1
    if-nez v1, :cond_2

    :goto_2
    move v3, v2

    goto :goto_0

    .end local v1    # "viewOrOneOfParentsVisibilityIsNotVisible":Z
    :cond_1
    move v1, v3

    .line 266
    goto :goto_1

    .restart local v1    # "viewOrOneOfParentsVisibilityIsNotVisible":Z
    :cond_2
    move v2, v3

    .line 268
    goto :goto_2
.end method

.method public static process(Ljava/util/List;ZZ)V
    .locals 2
    .param p1, "isAndroidUpAndDownFocusBehaviorRequired"    # Z
    .param p2, "isAndroidLeftAndRightFocusBehaviorRequired"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 802
    .local p0, "focusables":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->getFocusLines(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 803
    .local v0, "focusLines":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Landroid/view/View;>;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-gtz v1, :cond_1

    .line 810
    :cond_0
    :goto_0
    return-void

    .line 807
    :cond_1
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->setFocusUpAndDownDirectionsForViews(Ljava/util/List;Z)V

    .line 808
    invoke-static {v0, p2}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->setFocusLeftAndRightDirectionsForViews(Ljava/util/List;Z)V

    goto :goto_0
.end method

.method private static setFocusLeftAndRightDirectionsForViews(Ljava/util/List;Z)V
    .locals 5
    .param p1, "isAndroidLeftAndRightFocusBehaviorRequired"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;Z)V"
        }
    .end annotation

    .prologue
    .line 827
    .local p0, "focusLines":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Landroid/view/View;>;>;"
    if-eqz p1, :cond_1

    .line 842
    :cond_0
    :goto_0
    return-void

    .line 831
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v1, v3, :cond_2

    .line 833
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    add-int/lit8 v4, v1, 0x1

    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->setNextRightAndLeftFocuses(Ljava/util/List;Ljava/util/List;)V

    .line 831
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 835
    :cond_2
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 837
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 838
    .local v2, "lastFocusLine":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/4 v3, 0x0

    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 839
    .local v0, "firstFocusLine":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->setNextRightAndLeftFocusesForLastRow(Ljava/util/List;)V

    .line 840
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->setNextForwardFocusForLastItem(Ljava/util/List;Ljava/util/List;)V

    goto :goto_0
.end method

.method private static setFocusUpAndDownDirectionsForViews(Ljava/util/List;Z)V
    .locals 3
    .param p1, "isAndroidUpAndDownFocusBehaviorRequired"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;Z)V"
        }
    .end annotation

    .prologue
    .line 815
    .local p0, "focusLines":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Landroid/view/View;>;>;"
    if-eqz p1, :cond_1

    .line 823
    :cond_0
    return-void

    .line 819
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 821
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    add-int/lit8 v2, v0, 0x1

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->setNextUpAndDownFocuses(Ljava/util/List;Ljava/util/List;)V

    .line 819
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static setNextForwardFocusForLastItem(Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 553
    .local p0, "upperContainerSorted":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .local p1, "lowerContainerSorted":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const/4 v1, 0x0

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusForwardId(I)V

    .line 554
    return-void
.end method

.method private static setNextRightAndLeftFocuses(Ljava/util/List;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 558
    .local p0, "upperContainerSorted":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .local p1, "lowerContainerSorted":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/4 v3, 0x0

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 559
    .local v1, "mostLeftViewInLowerContainer":Landroid/view/View;
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 561
    .local v2, "mostRightViewInRightContainer":Landroid/view/View;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_0

    .line 563
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    add-int/lit8 v4, v0, 0x1

    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 564
    add-int/lit8 v3, v0, 0x1

    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setNextFocusLeftId(I)V

    .line 565
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    add-int/lit8 v4, v0, 0x1

    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setNextFocusForwardId(I)V

    .line 561
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 568
    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 569
    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/view/View;->setNextFocusLeftId(I)V

    .line 570
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setNextFocusForwardId(I)V

    .line 571
    return-void
.end method

.method private static setNextRightAndLeftFocusesForLastRow(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 543
    .local p0, "sortedViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 545
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    add-int/lit8 v2, v0, 0x1

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 546
    add-int/lit8 v1, v0, 0x1

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setNextFocusLeftId(I)V

    .line 547
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    add-int/lit8 v2, v0, 0x1

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setNextFocusForwardId(I)V

    .line 543
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 549
    :cond_0
    return-void
.end method

.method private static setNextUpAndDownFocuses(Ljava/util/List;Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 601
    .local p0, "upperContainerSorted":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .local p1, "lowerContainerSorted":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    if-nez p1, :cond_1

    .line 654
    :cond_0
    :goto_0
    return-void

    .line 606
    :cond_1
    const/4 v8, 0x0

    :try_start_0
    invoke-interface {p0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/View;

    .line 607
    .local v7, "mostLeftViewInUpperContainer":Landroid/view/View;
    const/4 v8, 0x0

    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    .line 609
    .local v6, "mostLeftViewInLowerContainer":Landroid/view/View;
    if-eqz v7, :cond_0

    .line 612
    invoke-virtual {v7}, Landroid/view/View;->getId()I

    move-result v5

    .line 614
    .local v5, "mostLeftUpperContainerViewId":I
    if-eqz v6, :cond_2

    .line 618
    invoke-virtual {v6}, Landroid/view/View;->getId()I

    move-result v4

    .line 620
    .local v4, "mostLeftLowerContainerViewId":I
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 624
    .local v2, "focusableViewInUpperContainer":Landroid/view/View;
    :try_start_1
    invoke-virtual {v2, v4}, Landroid/view/View;->setNextFocusDownId(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 626
    :catch_0
    move-exception v0

    .line 628
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v8, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->TAG:Ljava/lang/String;

    const-string v9, "Cannot set focusables down for upper container"

    invoke-static {v8, v9, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 650
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "focusableViewInUpperContainer":Landroid/view/View;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "mostLeftLowerContainerViewId":I
    .end local v5    # "mostLeftUpperContainerViewId":I
    .end local v6    # "mostLeftViewInLowerContainer":Landroid/view/View;
    .end local v7    # "mostLeftViewInUpperContainer":Landroid/view/View;
    :catch_1
    move-exception v0

    .line 652
    .restart local v0    # "e":Ljava/lang/Exception;
    sget-object v8, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->TAG:Ljava/lang/String;

    const-string v9, "Cannot set required directional focuses"

    invoke-static {v8, v9}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 633
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v5    # "mostLeftUpperContainerViewId":I
    .restart local v6    # "mostLeftViewInLowerContainer":Landroid/view/View;
    .restart local v7    # "mostLeftViewInUpperContainer":Landroid/view/View;
    :cond_2
    :try_start_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v8

    if-eqz v8, :cond_0

    .line 636
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 640
    .local v1, "focusableViewInLowerContainer":Landroid/view/View;
    :try_start_4
    invoke-virtual {v1, v5}, Landroid/view/View;->setNextFocusUpId(I)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    .line 642
    :catch_2
    move-exception v0

    .line 644
    .restart local v0    # "e":Ljava/lang/Exception;
    :try_start_5
    sget-object v8, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->TAG:Ljava/lang/String;

    const-string v9, "Cannot set focusables up for lower container"

    invoke-static {v8, v9, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_2
.end method

.method private static sortFocusablesList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 237
    .local p0, "viewsListToSort":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/FocusUtils$SHealthFocusabilityComparator;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/utils/FocusUtils$SHealthFocusabilityComparator;-><init>(Lcom/sec/android/app/shealth/common/utils/FocusUtils$1;)V

    invoke-static {p0, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 238
    return-void
.end method

.method public static uniquizeViewsIds(Landroid/view/View;I)I
    .locals 1
    .param p0, "content"    # Landroid/view/View;
    .param p1, "startFromId"    # I

    .prologue
    .line 1156
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->uniquizeViewsIds(Landroid/view/View;Ljava/util/Collection;I)I

    move-result v0

    return v0
.end method

.method public static uniquizeViewsIds(Landroid/view/View;Ljava/util/Collection;I)I
    .locals 7
    .param p0, "content"    # Landroid/view/View;
    .param p2, "startFromId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/Collection",
            "<",
            "Landroid/view/View;",
            ">;I)I"
        }
    .end annotation

    .prologue
    .line 1123
    .local p1, "additionalFocusables":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/view/View;>;"
    const/4 v1, 0x0

    .line 1125
    .local v1, "focusablesList":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    :try_start_0
    check-cast p0, Landroid/view/ViewGroup;

    .end local p0    # "content":Landroid/view/View;
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->getAllFocusableChildViews(Landroid/view/ViewGroup;)Ljava/util/List;

    move-result-object v1

    .line 1126
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1127
    invoke-static {p1, v1}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->addAllFocusablesToList(Ljava/util/Collection;Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1131
    :cond_0
    :goto_0
    new-instance v4, Ljava/util/LinkedHashSet;

    invoke-direct {v4}, Ljava/util/LinkedHashSet;-><init>()V

    .line 1132
    .local v4, "viewsIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    if-eqz v1, :cond_4

    .line 1133
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1134
    .local v0, "focusable":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1135
    :cond_2
    add-int/lit8 v3, p2, 0x1

    .end local p2    # "startFromId":I
    .local v3, "startFromId":I
    invoke-virtual {v0, p2}, Landroid/view/View;->setId(I)V

    move p2, v3

    .end local v3    # "startFromId":I
    .restart local p2    # "startFromId":I
    goto :goto_1

    .line 1137
    .end local v0    # "focusable":Landroid/view/View;
    :cond_3
    sget-object v5, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->TAG:Ljava/lang/String;

    const-string v6, "Focusables IDs were uniquized"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1139
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_4
    return p2

    .line 1128
    .end local v4    # "viewsIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :catch_0
    move-exception v5

    goto :goto_0
.end method
