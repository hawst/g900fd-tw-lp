.class Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity$2;
.super Ljava/lang/Object;
.source "WalkingMateGoalActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 1
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 128
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne v0, p1, :cond_0

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->checkForValidGoal()Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->access$000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->setMinValue()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->access$100(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;)V

    .line 140
    :cond_0
    :goto_0
    return-void

    .line 136
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->finish()V

    goto :goto_0
.end method
