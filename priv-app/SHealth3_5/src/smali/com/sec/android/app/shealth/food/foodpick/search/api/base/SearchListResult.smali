.class public Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;
.super Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;
.source "SearchListResult.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
        ">",
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase",
        "<TE;",
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/util/List;I)V
    .locals 0
    .param p2, "totalCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TE;>;I)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult<TE;>;"
    .local p1, "searchResult":Ljava/util/List;, "Ljava/util/List<TE;>;"
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;-><init>(Ljava/util/List;I)V

    .line 30
    return-void
.end method
