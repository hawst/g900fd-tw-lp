.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$1;
.super Ljava/lang/Object;
.source "PhotoHorizontalScrollView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->setPhotoListtoBitmap(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

.field final synthetic val$datas:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$1;->val$datas:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/high16 v7, 0x42860000    # 67.0f

    .line 88
    const/4 v0, 0x0

    .local v0, "a":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$1;->val$datas:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 89
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$1;->val$datas:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    .line 90
    .local v1, "data":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mObject:Ljava/lang/Object;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 91
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    iget-boolean v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->isEnable:Z

    if-nez v3, :cond_1

    .line 92
    monitor-exit v4

    .line 106
    .end local v1    # "data":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    :cond_0
    return-void

    .line 93
    .restart local v1    # "data":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    :cond_1
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$Holder;

    invoke-direct {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$Holder;-><init>()V

    .line 96
    .local v2, "holder":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$Holder;
    iput v0, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$Holder;->index:I

    .line 97
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$Holder;->filePath:Ljava/lang/String;

    .line 98
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$Holder;->filePath:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v7}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->convertDpToPx(Landroid/content/Context;F)F

    move-result v5

    float-to-int v5, v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->convertDpToPx(Landroid/content/Context;F)F

    move-result v6

    float-to-int v6, v6

    invoke-static {v3, v4, v5, v6}, Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;->getImageThumbnailFitXY(Landroid/content/Context;Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$Holder;->bitmap:Landroid/graphics/Bitmap;

    .line 100
    iget-object v3, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$Holder;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_2

    .line 101
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mUpdateHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    iget-object v4, v4, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mUpdateHandler:Landroid/os/Handler;

    const/4 v5, 0x0

    invoke-virtual {v4, v5, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 88
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 93
    .end local v2    # "holder":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$Holder;
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 103
    .restart local v2    # "holder":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$Holder;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mUpdateHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    iget-object v4, v4, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mUpdateHandler:Landroid/os/Handler;

    const/4 v5, -0x1

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1
.end method
