.class public Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FrequentFoodListItem;
.super Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;
.source "FrequentFoodListItem.java"


# instance fields
.field private mFrequency:I


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;ILcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;)V
    .locals 0
    .param p1, "foodInfoItem"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .param p2, "frequency"    # I
    .param p3, "foodInfoDao"    # Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    .prologue
    .line 24
    invoke-direct {p0, p1, p3}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;)V

    .line 25
    iput p2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FrequentFoodListItem;->mFrequency:I

    .line 26
    return-void
.end method


# virtual methods
.method public compareTo(Ljava/lang/Object;)I
    .locals 3
    .param p1, "another"    # Ljava/lang/Object;

    .prologue
    .line 30
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->compareTo(Ljava/lang/Object;)I

    move-result v1

    .line 31
    .local v1, "result":I
    if-nez v1, :cond_0

    .line 32
    check-cast p1, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FrequentFoodListItem;

    .end local p1    # "another":Ljava/lang/Object;
    iget v0, p1, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FrequentFoodListItem;->mFrequency:I

    .line 33
    .local v0, "anotherFrequency":I
    iget v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FrequentFoodListItem;->mFrequency:I

    if-ge v2, v0, :cond_1

    .line 34
    const/4 v1, 0x1

    .line 39
    .end local v0    # "anotherFrequency":I
    :cond_0
    :goto_0
    return v1

    .line 35
    .restart local v0    # "anotherFrequency":I
    :cond_1
    iget v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FrequentFoodListItem;->mFrequency:I

    if-le v2, v0, :cond_0

    .line 36
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 44
    instance-of v0, p1, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FrequentFoodListItem;

    if-nez v0, :cond_0

    move v0, v1

    .line 50
    :goto_0
    return v0

    .line 47
    :cond_0
    iget v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FrequentFoodListItem;->mFrequency:I

    move-object v0, p1

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FrequentFoodListItem;

    iget v0, v0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FrequentFoodListItem;->mFrequency:I

    if-eq v2, v0, :cond_1

    move v0, v1

    .line 48
    goto :goto_0

    .line 50
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
