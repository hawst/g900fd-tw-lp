.class public Lcom/sec/android/app/shealth/weight/service/WeightService;
.super Landroid/app/Service;
.source "WeightService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceDataListener;,
        Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceEventListener;,
        Lcom/sec/android/app/shealth/weight/service/WeightService$WeightBinder;,
        Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;,
        Lcom/sec/android/app/shealth/weight/service/WeightService$ShealthDeviceFinderServiceConnectionListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isStopReceivingDataCalled:Z

.field private final mBinder:Landroid/os/IBinder;

.field private mListPairedDevices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mShealthDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

.field private mSupportedWeighScaleTypes:[I

.field private mToJoinDevices:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mWeightDataReceivedListener:Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lcom/sec/android/app/shealth/weight/service/WeightService;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 51
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 53
    new-instance v0, Lcom/sec/android/app/shealth/weight/service/WeightService$WeightBinder;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/weight/service/WeightService$WeightBinder;-><init>(Lcom/sec/android/app/shealth/weight/service/WeightService;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mBinder:Landroid/os/IBinder;

    .line 55
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->isStopReceivingDataCalled:Z

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mListPairedDevices:Ljava/util/ArrayList;

    .line 58
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mToJoinDevices:Ljava/util/Map;

    .line 61
    new-array v0, v2, [I

    aput v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mSupportedWeighScaleTypes:[I

    .line 348
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/weight/service/WeightService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/service/WeightService;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/service/WeightService;->requestPairedDevices()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/weight/service/WeightService;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/service/WeightService;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mShealthDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/weight/service/WeightService;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/service/WeightService;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mShealthDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/weight/service/WeightService;)Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/service/WeightService;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mWeightDataReceivedListener:Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/weight/service/WeightService;Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;)Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/service/WeightService;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mWeightDataReceivedListener:Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/weight/service/WeightService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/service/WeightService;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/service/WeightService;->startWeightService()V

    return-void
.end method

.method static synthetic access$400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/weight/service/WeightService;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/service/WeightService;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mToJoinDevices:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/weight/service/WeightService;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/service/WeightService;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;
    .param p2, "x2"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/weight/service/WeightService;->startReceivingData(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/weight/service/WeightService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/service/WeightService;

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->isStopReceivingDataCalled:Z

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/shealth/weight/service/WeightService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/service/WeightService;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->isStopReceivingDataCalled:Z

    return p1
.end method

.method private initDeviceFinder()V
    .locals 2

    .prologue
    .line 178
    sget-object v0, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    const-string v1, "*****initDeviceFinder()"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mShealthDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-nez v0, :cond_0

    .line 180
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    new-instance v1, Lcom/sec/android/app/shealth/weight/service/WeightService$ShealthDeviceFinderServiceConnectionListener;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/weight/service/WeightService$ShealthDeviceFinderServiceConnectionListener;-><init>(Lcom/sec/android/app/shealth/weight/service/WeightService;)V

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mShealthDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 182
    :cond_0
    return-void
.end method

.method private declared-synchronized joinDevice(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V
    .locals 4
    .param p1, "shealthSensorDevice"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
        }
    .end annotation

    .prologue
    .line 429
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    const-string v2, "*****joinDevice()"

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    sget-object v1, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "(Before join) shealthSensorDevice.State : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getState()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getState()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    const/4 v2, 0x2

    if-gt v1, v2, :cond_0

    .line 433
    :try_start_1
    sget-object v1, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    const-string v2, "call join()"

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    new-instance v1, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceEventListener;

    invoke-direct {v1, p1, p0}, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceEventListener;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Lcom/sec/android/app/shealth/weight/service/WeightService;)V

    invoke-virtual {p1, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->join(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;)V

    .line 435
    sget-object v1, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "(After join) shealthSensorDevice State : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getState()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 446
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 436
    :catch_0
    move-exception v0

    .line 437
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_2
    sget-object v1, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 429
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 438
    :catch_1
    move-exception v0

    .line 439
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_3
    sget-object v1, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 440
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 441
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    sget-object v1, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 442
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_3
    move-exception v0

    .line 443
    .local v0, "e":Ljava/lang/IllegalStateException;
    sget-object v1, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method private refreshJoinedDeviceList()V
    .locals 10

    .prologue
    .line 135
    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mShealthDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-eqz v6, :cond_0

    .line 137
    const/4 v1, 0x0

    .line 138
    .local v1, "devices":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mListPairedDevices:Ljava/util/ArrayList;

    if-eqz v6, :cond_2

    .line 139
    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mListPairedDevices:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 140
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mSupportedWeighScaleTypes:[I

    array-length v6, v6

    if-ge v3, v6, :cond_2

    .line 141
    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mShealthDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    iget-object v7, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mSupportedWeighScaleTypes:[I

    aget v7, v7, v3

    const/16 v8, 0x2712

    const/4 v9, 0x1

    invoke-virtual {v6, v7, v8, v9}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->getConnectedDeviceList(III)Ljava/util/List;

    move-result-object v1

    .line 142
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 143
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 144
    .local v0, "device":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mListPairedDevices:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_1

    .line 160
    .end local v0    # "device":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .end local v3    # "i":I
    .end local v4    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 161
    .local v2, "e":Landroid/os/RemoteException;
    sget-object v6, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    invoke-static {v6, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 172
    .end local v1    # "devices":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .end local v2    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_2
    return-void

    .line 140
    .restart local v1    # "devices":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .restart local v3    # "i":I
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 151
    .end local v3    # "i":I
    :cond_2
    :try_start_1
    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mListPairedDevices:Ljava/util/ArrayList;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mListPairedDevices:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 152
    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mListPairedDevices:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .restart local v4    # "i$":Ljava/util/Iterator;
    :cond_3
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 153
    .local v5, "pairedDevice":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    sget-object v6, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "(refreshJoinedDeviceList) ID : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mToJoinDevices:Ljava/util/Map;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 156
    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/weight/service/WeightService;->joinDevice(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_1 .. :try_end_1} :catch_4

    goto :goto_3

    .line 162
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "pairedDevice":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    :catch_1
    move-exception v2

    .line 163
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    sget-object v6, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    invoke-static {v6, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 164
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v2

    .line 165
    .local v2, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    sget-object v6, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    invoke-static {v6, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 166
    .end local v2    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_3
    move-exception v2

    .line 167
    .local v2, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    sget-object v6, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    invoke-static {v6, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 168
    .end local v2    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    :catch_4
    move-exception v2

    .line 169
    .local v2, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    sget-object v6, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    invoke-static {v6, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method private requestPairedDevices()V
    .locals 10

    .prologue
    .line 208
    sget-object v6, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    const-string v7, "*****requestPairedDevices()"

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    sget-object v6, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    const-string/jumbo v7, "need to check if exist already started device"

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mShealthDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-eqz v6, :cond_0

    .line 215
    const/4 v1, 0x0

    .line 216
    .local v1, "devices":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mListPairedDevices:Ljava/util/ArrayList;

    if-eqz v6, :cond_2

    .line 217
    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mListPairedDevices:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 218
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mSupportedWeighScaleTypes:[I

    array-length v6, v6

    if-ge v3, v6, :cond_2

    .line 219
    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mShealthDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    iget-object v7, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mSupportedWeighScaleTypes:[I

    aget v7, v7, v3

    const/16 v8, 0x2712

    const/4 v9, 0x1

    invoke-virtual {v6, v7, v8, v9}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->getConnectedDeviceList(III)Ljava/util/List;

    move-result-object v1

    .line 220
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 221
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 222
    .local v0, "device":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mListPairedDevices:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_1

    .line 234
    .end local v0    # "device":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .end local v3    # "i":I
    .end local v4    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 235
    .local v2, "e":Landroid/os/RemoteException;
    sget-object v6, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    invoke-static {v6, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 246
    .end local v1    # "devices":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .end local v2    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_2
    return-void

    .line 218
    .restart local v1    # "devices":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .restart local v3    # "i":I
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 228
    .end local v3    # "i":I
    :cond_2
    :try_start_1
    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mListPairedDevices:Ljava/util/ArrayList;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mListPairedDevices:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 229
    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mListPairedDevices:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .restart local v4    # "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 230
    .local v5, "pairedDevice":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    sget-object v6, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "(requestPairedDevices) ID : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/weight/service/WeightService;->joinDevice(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_1 .. :try_end_1} :catch_4

    goto :goto_3

    .line 236
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "pairedDevice":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    :catch_1
    move-exception v2

    .line 237
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    sget-object v6, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    invoke-static {v6, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 238
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v2

    .line 239
    .local v2, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    sget-object v6, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    invoke-static {v6, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 240
    .end local v2    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_3
    move-exception v2

    .line 241
    .local v2, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    sget-object v6, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    invoke-static {v6, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 242
    .end local v2    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    :catch_4
    move-exception v2

    .line 243
    .local v2, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    sget-object v6, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    invoke-static {v6, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method private declared-synchronized startReceivingData(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V
    .locals 4
    .param p1, "dataListener"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;
    .param p2, "shealthSensorDevice"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .prologue
    .line 449
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    const-string v2, "*****startReceivingData()"

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    sget-object v1, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "(Before startReceivingData) State() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getState()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 452
    :try_start_1
    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getState()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 453
    invoke-virtual {p2, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->startReceivingData(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;)V

    .line 454
    sget-object v1, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "(After startReceivingData) State() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getState()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 470
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 456
    :catch_0
    move-exception v0

    .line 457
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_2
    sget-object v1, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 449
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 458
    :catch_1
    move-exception v0

    .line 459
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_3
    sget-object v1, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 460
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 461
    .local v0, "e":Ljava/lang/IllegalStateException;
    sget-object v1, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 462
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 463
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    sget-object v1, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 464
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_4
    move-exception v0

    .line 465
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    sget-object v1, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 466
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    :catch_5
    move-exception v0

    .line 467
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    sget-object v1, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method private startWeightService()V
    .locals 2

    .prologue
    .line 125
    sget-object v0, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    const-string v1, "*****startWeightService()"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mShealthDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-nez v0, :cond_0

    .line 127
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/service/WeightService;->initDeviceFinder()V

    .line 131
    :goto_0
    return-void

    .line 129
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/service/WeightService;->refreshJoinedDeviceList()V

    goto :goto_0
.end method


# virtual methods
.method public cleanDevices()V
    .locals 6

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/service/WeightService;->stopReceivingData()V

    .line 77
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mToJoinDevices:Ljava/util/Map;

    if-eqz v3, :cond_0

    .line 78
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mToJoinDevices:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 81
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mListPairedDevices:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    .line 82
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mListPairedDevices:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 83
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mListPairedDevices:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 85
    .local v0, "device":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    :try_start_0
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->close()V
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 82
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 86
    :catch_0
    move-exception v1

    .line 87
    .local v1, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    sget-object v3, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "exception while closing device: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 88
    .end local v1    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    :catch_1
    move-exception v1

    .line 89
    .local v1, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    sget-object v3, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "exception while closing device: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 92
    .end local v0    # "device":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .end local v1    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mListPairedDevices:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 96
    .end local v2    # "i":I
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mShealthDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-eqz v3, :cond_3

    .line 98
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mShealthDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->close()V

    .line 99
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mShealthDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 102
    :cond_3
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 117
    sget-object v0, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    const-string v1, "*****onBind()"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 67
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 68
    sget-object v0, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    const-string v1, "*****onCreate()"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->isStopReceivingDataCalled:Z

    .line 70
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/service/WeightService;->startWeightService()V

    .line 71
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 105
    sget-object v0, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    const-string v1, "*****onDestroy()"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/service/WeightService;->cleanDevices()V

    .line 108
    iput-object v2, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mToJoinDevices:Ljava/util/Map;

    .line 109
    iput-object v2, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mListPairedDevices:Ljava/util/ArrayList;

    .line 110
    iput-object v2, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mWeightDataReceivedListener:Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;

    .line 112
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 113
    return-void
.end method

.method public declared-synchronized stopReceivingData()V
    .locals 7

    .prologue
    .line 476
    monitor-enter p0

    :try_start_0
    sget-object v4, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    const-string v5, "*****stopReceivingData()"

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mToJoinDevices:Ljava/util/Map;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mToJoinDevices:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 480
    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->mToJoinDevices:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 481
    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 482
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 483
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 486
    .local v3, "pairedDevice":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    :try_start_1
    sget-object v4, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    const-string v5, "*****before stopReceivingData()"

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->isStopReceivingDataCalled:Z

    .line 488
    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->stopReceivingData()V

    .line 489
    sget-object v4, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    const-string v5, "*****after stopReceivingData()"

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 504
    :goto_1
    :try_start_2
    sget-object v4, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "(stopReceivingData) ID : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    sget-object v4, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "(Before leave) State : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getState()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/app/shealth/weight/service/WeightService;->isStopReceivingDataCalled:Z

    .line 507
    sget-object v4, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    const-string v5, "call leave()"

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->leave()V

    .line 509
    sget-object v4, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "(After leave) State : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getState()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 511
    :catch_0
    move-exception v0

    .line 512
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :try_start_3
    sget-object v4, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    invoke-static {v4, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 476
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .end local v2    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;>;"
    .end local v3    # "pairedDevice":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 491
    .restart local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .restart local v2    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;>;"
    .restart local v3    # "pairedDevice":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    :catch_1
    move-exception v0

    .line 493
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :try_start_4
    sget-object v4, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    invoke-static {v4, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 494
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_1

    .line 495
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :catch_2
    move-exception v0

    .line 496
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    sget-object v4, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    invoke-static {v4, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 497
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_3
    move-exception v0

    .line 498
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    invoke-static {v4, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 499
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_4
    move-exception v0

    .line 500
    .local v0, "e":Ljava/lang/IllegalStateException;
    sget-object v4, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    invoke-static {v4, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 513
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_5
    move-exception v0

    .line 514
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    invoke-static {v4, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 515
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_6
    move-exception v0

    .line 516
    .local v0, "e":Ljava/lang/IllegalStateException;
    sget-object v4, Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;

    invoke-static {v4, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 520
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .end local v2    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;>;"
    .end local v3    # "pairedDevice":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    :cond_0
    monitor-exit p0

    return-void
.end method
