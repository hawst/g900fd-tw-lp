.class public Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreAlertActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;
.source "RestoreAlertActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IBackPressControllerProvider;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreAlertActivity$4;,
        Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreAlertActivity$BackupDialogBackPressController;,
        Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreAlertActivity$RestoreDialogBackPressController;,
        Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreAlertActivity$BackupDialogButtonController;,
        Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreAlertActivity$RestoreDialogButtonController;
    }
.end annotation


# static fields
.field private static final BACKUP:Ljava/lang/String; = "backup popup"

.field private static final RESTORE:Ljava/lang/String; = "restore popup"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mBackPressMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/IBackPressController;",
            ">;"
        }
    .end annotation
.end field

.field private final mDialogControllerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreAlertActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreAlertActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;-><init>()V

    .line 52
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreAlertActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreAlertActivity$1;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreAlertActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreAlertActivity;->mDialogControllerMap:Ljava/util/Map;

    .line 64
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreAlertActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreAlertActivity$2;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreAlertActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreAlertActivity;->mBackPressMap:Ljava/util/Map;

    .line 194
    return-void
.end method

.method private showPopup(Landroid/content/Intent;)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const v6, 0x7f090139

    const/4 v5, 0x0

    .line 81
    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreAlertActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " Receive intent action = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreAlertActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "Result"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 83
    .local v1, "result":Z
    if-eqz v1, :cond_0

    .line 84
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v2, 0x2

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 85
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 86
    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 87
    const v2, 0x7f090137

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 88
    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 90
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreAlertActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string/jumbo v4, "restore popup"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 109
    :goto_0
    return-void

    .line 92
    .end local v0    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v0, p0, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 93
    .restart local v0    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 94
    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 95
    const v2, 0x7f090138

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 96
    const v2, 0x7f090047

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 98
    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreAlertActivity$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreAlertActivity$3;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreAlertActivity;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 107
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreAlertActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "backup popup"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public getBackPressController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IBackPressController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreAlertActivity;->mBackPressMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/IBackPressController;

    return-object v0
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreAlertActivity;->mDialogControllerMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 74
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 75
    const v0, 0x7f030018

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreAlertActivity;->setContentView(I)V

    .line 76
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreAlertActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreAlertActivity;->showPopup(Landroid/content/Intent;)V

    .line 77
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 114
    const/4 v0, 0x0

    return v0
.end method
