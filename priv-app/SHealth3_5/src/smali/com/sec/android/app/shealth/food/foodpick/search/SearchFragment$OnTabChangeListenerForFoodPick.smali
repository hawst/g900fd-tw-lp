.class Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$OnTabChangeListenerForFoodPick;
.super Ljava/lang/Object;
.source "SearchFragment.java"

# interfaces
.implements Landroid/widget/TabHost$OnTabChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnTabChangeListenerForFoodPick"
.end annotation


# instance fields
.field private mLight:Landroid/graphics/Typeface;

.field private mRegular:Landroid/graphics/Typeface;

.field private final tabViewMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$TabViewInformation;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$TabViewInformation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "tabViewMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$TabViewInformation;>;"
    const/4 v1, 0x0

    .line 1420
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$OnTabChangeListenerForFoodPick;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1421
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$OnTabChangeListenerForFoodPick;->tabViewMap:Ljava/util/Map;

    .line 1422
    const-string/jumbo v0, "sec-roboto-light"

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$OnTabChangeListenerForFoodPick;->mLight:Landroid/graphics/Typeface;

    .line 1423
    const-string/jumbo v0, "sans-serif"

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$OnTabChangeListenerForFoodPick;->mRegular:Landroid/graphics/Typeface;

    .line 1424
    return-void
.end method


# virtual methods
.method public onTabChanged(Ljava/lang/String;)V
    .locals 8
    .param p1, "tabTag"    # Ljava/lang/String;

    .prologue
    const v7, 0x7f090203

    .line 1428
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$OnTabChangeListenerForFoodPick;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mIsStateSaved:Z
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$3500(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1430
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$OnTabChangeListenerForFoodPick;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$1800(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v4

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->LOG_ID_BY_TAB_NAME:Ljava/util/Map;
    invoke-static {}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$3600()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v4, v3}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 1432
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$OnTabChangeListenerForFoodPick;->tabViewMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1433
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$TabViewInformation;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$TabViewInformation;

    iget-object v2, v3, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$TabViewInformation;->tabView:Landroid/view/View;

    check-cast v2, Landroid/widget/TextView;

    .line 1434
    .local v2, "tabTextView":Landroid/widget/TextView;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1435
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$OnTabChangeListenerForFoodPick;->mRegular:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1436
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$OnTabChangeListenerForFoodPick;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$OnTabChangeListenerForFoodPick;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0901f1

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1442
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$OnTabChangeListenerForFoodPick;->mLight:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1443
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$OnTabChangeListenerForFoodPick;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$OnTabChangeListenerForFoodPick;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0901f2

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1450
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$TabViewInformation;>;"
    .end local v2    # "tabTextView":Landroid/widget/TextView;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$OnTabChangeListenerForFoodPick;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/food/utils/FoodSharedPrefsHelper;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$3700(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/utils/FoodSharedPrefsHelper;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/sec/android/app/shealth/food/utils/FoodSharedPrefsHelper;->putFoodPickLastFragmentName(Ljava/lang/String;)V

    .line 1452
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    return-void
.end method
