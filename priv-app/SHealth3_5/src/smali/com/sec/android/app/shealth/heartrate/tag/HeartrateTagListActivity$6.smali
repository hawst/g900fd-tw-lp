.class Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$6;
.super Ljava/lang/Object;
.source "HeartrateTagListActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->setUpMenuDeleteMode(Ljava/util/HashSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)V
    .locals 0

    .prologue
    .line 333
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$6;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    .line 336
    instance-of v1, p1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 337
    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    .line 339
    .local v0, "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v1

    if-nez v1, :cond_0

    .line 340
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$6;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagsToDelete:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    if-le v1, v2, :cond_1

    .line 341
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$6;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # invokes: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->showDeletePopup(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$1000(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;Z)V

    .line 346
    .end local v0    # "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    :cond_0
    :goto_0
    return-void

    .line 343
    .restart local v0    # "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$6;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    const/4 v2, 0x0

    # invokes: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->showDeletePopup(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$1000(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;Z)V

    goto :goto_0
.end method
