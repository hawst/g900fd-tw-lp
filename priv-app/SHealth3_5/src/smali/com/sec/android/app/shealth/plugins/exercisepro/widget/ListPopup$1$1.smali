.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$1$1;
.super Ljava/lang/Object;
.source "ListPopup.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$1;

.field final synthetic val$selectedChild:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$1;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 409
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$1$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$1;

    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$1$1;->val$selectedChild:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 412
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$1$1;->val$selectedChild:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    move-result v0

    .line 413
    .local v0, "focusPassed":Z
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->access$1500()Ljava/lang/String;

    move-result-object v3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "focus was "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v0, :cond_0

    const-string v2, ""

    :goto_0
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, "passed"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->access$1500()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "window = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$1$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$1;

    iget-object v4, v4, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->listView:Landroid/widget/ListView;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->access$1400(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ListView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$1$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$1;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getContentView()Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$1$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$1;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->uniquizeViewIdHelper:Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->access$1600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;->getCurrentMaxViewId()I

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->uniquizeViewsIds(Landroid/view/View;I)I

    move-result v1

    .line 417
    .local v1, "updatedMaxViewId":I
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$1$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$1;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->uniquizeViewIdHelper:Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->access$1600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;->setCurrentMaxViewId(I)Z

    .line 418
    return-void

    .line 413
    .end local v1    # "updatedMaxViewId":I
    :cond_0
    const-string/jumbo v2, "not"

    goto :goto_0
.end method
