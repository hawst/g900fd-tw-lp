.class public final enum Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;
.super Ljava/lang/Enum;
.source "SpO2TimerType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;

.field public static final enum MEASURING:Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;

.field public static final enum MEASURING_ERROR:Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;

.field public static final enum READY:Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;


# instance fields
.field private timerType:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4
    new-instance v0, Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;

    const-string v1, "READY"

    invoke-direct {v0, v1, v3, v3}, Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;->READY:Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;

    .line 5
    new-instance v0, Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;

    const-string v1, "MEASURING"

    invoke-direct {v0, v1, v4, v4}, Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;->MEASURING:Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;

    .line 6
    new-instance v0, Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;

    const-string v1, "MEASURING_ERROR"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;->MEASURING_ERROR:Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;

    .line 3
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;

    sget-object v1, Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;->READY:Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;->MEASURING:Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;->MEASURING_ERROR:Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;->$VALUES:[Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "timerType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 11
    iput p3, p0, Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;->timerType:I

    .line 12
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;->$VALUES:[Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;

    return-object v0
.end method
