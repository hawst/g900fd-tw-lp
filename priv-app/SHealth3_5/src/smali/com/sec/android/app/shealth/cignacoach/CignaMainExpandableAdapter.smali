.class public Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;
.super Landroid/widget/BaseExpandableListAdapter;
.source "CignaMainExpandableAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$3;,
        Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$NewMissionClickListener;,
        Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$ArrowClickListener;,
        Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedChildChangeListener;,
        Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedGroupChangeListener;,
        Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCheckedCountListener:Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;

.field private mChildData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;>;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mGoalMissionDelete:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;",
            ">;"
        }
    .end annotation
.end field

.field private mGroupCheckBoxTouch:Z

.field private mGroupData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;"
        }
    .end annotation
.end field

.field private mMainType:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mGroupData:Ljava/util/ArrayList;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mChildData:Ljava/util/ArrayList;

    .line 40
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;->GOAL:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mMainType:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mGroupCheckBoxTouch:Z

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/HashSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;>;",
            "Ljava/util/HashSet",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 51
    .local p2, "groupList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    .local p3, "childList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;>;"
    .local p4, "mGoalMissionDelete":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;>;"
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mGroupData:Ljava/util/ArrayList;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mChildData:Ljava/util/ArrayList;

    .line 40
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;->GOAL:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mMainType:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mGroupCheckBoxTouch:Z

    .line 53
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mContext:Landroid/content/Context;

    .line 54
    iput-object p2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mGroupData:Ljava/util/ArrayList;

    .line 55
    iput-object p3, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mChildData:Ljava/util/ArrayList;

    .line 56
    iput-object p4, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mGoalMissionDelete:Ljava/util/HashSet;

    .line 58
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mGroupCheckBoxTouch:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mGroupCheckBoxTouch:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;)Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mMainType:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;)Ljava/util/HashSet;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mGoalMissionDelete:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;)Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mCheckedCountListener:Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;

    return-object v0
.end method

.method private allChildItemCheck(Z)V
    .locals 6
    .param p1, "check"    # Z

    .prologue
    .line 417
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getGroupCount()I

    move-result v4

    .line 418
    .local v4, "groupCount":I
    const/4 v3, 0x0

    .local v3, "group":I
    :goto_0
    if-ge v3, v4, :cond_1

    .line 420
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;

    .line 421
    .local v2, "goalData":Lcom/sec/android/app/shealth/cignacoach/data/GoalData;
    invoke-virtual {v2, p1}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->setChecked(Z)V

    .line 423
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getChildrenCount(I)I

    move-result v1

    .line 424
    .local v1, "childCount":I
    const/4 v0, 0x0

    .local v0, "child":I
    :goto_1
    if-ge v0, v1, :cond_0

    .line 425
    invoke-virtual {p0, v3, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getChild(II)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    .line 426
    .local v5, "missionData":Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    invoke-virtual {v5, p1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setChecked(Z)V

    .line 424
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 418
    .end local v5    # "missionData":Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 429
    .end local v0    # "child":I
    .end local v1    # "childCount":I
    .end local v2    # "goalData":Lcom/sec/android/app/shealth/cignacoach/data/GoalData;
    :cond_1
    return-void
.end method

.method private getGoalStatusString(Lcom/sec/android/app/shealth/cignacoach/data/GoalData;)Ljava/lang/String;
    .locals 4
    .param p1, "data"    # Lcom/sec/android/app/shealth/cignacoach/data/GoalData;

    .prologue
    .line 311
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$3;->$SwitchMap$com$cigna$coach$interfaces$IGoalsMissions$GoalStatus:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->getGoalStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 321
    const-string v0, ""

    :goto_0
    return-object v0

    .line 313
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09030a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 315
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090309

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->getCompleteDate()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil;->getMonthLen3DateFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 317
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0902f3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 319
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090329

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 311
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getMissionStatusString(Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;)Ljava/lang/String;
    .locals 2
    .param p1, "missionStatus"    # Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    .prologue
    .line 327
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$3;->$SwitchMap$com$cigna$coach$interfaces$IGoalsMissions$MissionStatus:[I

    invoke-virtual {p1}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 337
    const-string v0, ""

    :goto_0
    return-object v0

    .line 329
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mContext:Landroid/content/Context;

    const v1, 0x7f090309

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 331
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0902f3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 333
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090329

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 335
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090306

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 327
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public allChildItemCheckOfGroup(IZ)V
    .locals 4
    .param p1, "group"    # I
    .param p2, "check"    # Z

    .prologue
    .line 444
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mChildData:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 445
    .local v1, "childList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;

    .line 446
    .local v0, "childData":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    invoke-virtual {v0, p2}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->setChecked(Z)V

    goto :goto_0

    .line 448
    .end local v0    # "childData":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    :cond_0
    return-void
.end method

.method public getCheckedChildCount()I
    .locals 7

    .prologue
    .line 373
    const/4 v0, 0x0

    .line 374
    .local v0, "checkedCount":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getGroupCount()I

    move-result v5

    .line 375
    .local v5, "groupCount":I
    const/4 v4, 0x0

    .local v4, "group":I
    :goto_0
    if-ge v4, v5, :cond_2

    .line 376
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getChildrenCount(I)I

    move-result v2

    .line 377
    .local v2, "childCount":I
    const/4 v1, 0x0

    .local v1, "child":I
    :goto_1
    if-ge v1, v2, :cond_1

    .line 378
    invoke-virtual {p0, v4, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getChild(II)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;

    .line 379
    .local v3, "childData":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->isChecked()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 380
    add-int/lit8 v0, v0, 0x1

    .line 377
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 375
    .end local v3    # "childData":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 384
    .end local v1    # "child":I
    .end local v2    # "childCount":I
    :cond_2
    return v0
.end method

.method public getCheckedGroupCount()I
    .locals 5

    .prologue
    .line 361
    const/4 v0, 0x0

    .line 362
    .local v0, "checkedCount":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getGroupCount()I

    move-result v2

    .line 363
    .local v2, "groupCount":I
    const/4 v1, 0x0

    .local v1, "group":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 364
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;

    .line 365
    .local v3, "groupData":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 366
    add-int/lit8 v0, v0, 0x1

    .line 363
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 369
    .end local v3    # "groupData":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    :cond_1
    return v0
.end method

.method public getCheckedItemCount()I
    .locals 8

    .prologue
    .line 388
    const/4 v0, 0x0

    .line 389
    .local v0, "checkedCount":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getGroupCount()I

    move-result v5

    .line 390
    .local v5, "groupCount":I
    const/4 v4, 0x0

    .local v4, "group":I
    :goto_0
    if-ge v4, v5, :cond_3

    .line 391
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;

    .line 392
    .local v6, "groupData":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    invoke-virtual {v6}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->isChecked()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 393
    add-int/lit8 v0, v0, 0x1

    .line 395
    :cond_0
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getChildrenCount(I)I

    move-result v2

    .line 396
    .local v2, "childCount":I
    const/4 v1, 0x0

    .local v1, "child":I
    :goto_1
    if-ge v1, v2, :cond_2

    .line 397
    invoke-virtual {p0, v4, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getChild(II)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;

    .line 398
    .local v3, "childData":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->isChecked()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 399
    add-int/lit8 v0, v0, 0x1

    .line 396
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 390
    .end local v3    # "childData":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 403
    .end local v1    # "child":I
    .end local v2    # "childCount":I
    .end local v6    # "groupData":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    :cond_3
    return v0
.end method

.method public getCheckedItemCount(I)I
    .locals 5
    .param p1, "groupPosition"    # I

    .prologue
    .line 432
    const/4 v0, 0x0

    .line 433
    .local v0, "checkedCount":I
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getChildrenCount(I)I

    move-result v1

    .line 434
    .local v1, "childCount":I
    const/4 v2, 0x0

    .local v2, "childPosition":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 435
    invoke-virtual {p0, p1, v2}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getChild(II)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    .line 436
    .local v3, "missionData":Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 437
    add-int/lit8 v0, v0, 0x1

    .line 434
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 440
    .end local v3    # "missionData":Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    :cond_1
    return v0
.end method

.method public getChild(II)Ljava/lang/Object;
    .locals 3
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 72
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mChildData:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 76
    :goto_0
    return-object v1

    .line 73
    :catch_0
    move-exception v0

    .line 74
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    const/4 v1, 0x0

    .local v1, "getChildObject":Ljava/lang/Object;
    goto :goto_0
.end method

.method public getChildId(II)J
    .locals 2
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 81
    int-to-long v0, p2

    return-wide v0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 19
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I
    .param p3, "isLastChild"    # Z
    .param p4, "convertView"    # Landroid/view/View;
    .param p5, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 87
    const/4 v14, 0x0

    .line 88
    .local v14, "view":Landroid/view/View;
    if-nez p4, :cond_5

    .line 89
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mMainType:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    sget-object v16, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;->GOAL:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    move-object/from16 v0, v16

    if-ne v15, v0, :cond_3

    .line 90
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mContext:Landroid/content/Context;

    const v16, 0x7f030034

    const/16 v17, 0x0

    invoke-static/range {v15 .. v17}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v14

    .line 100
    :goto_0
    const v15, 0x7f08012d

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    .line 101
    .local v9, "icon":Landroid/widget/ImageView;
    const v15, 0x7f080126

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    .line 102
    .local v13, "title":Landroid/widget/TextView;
    const v15, 0x7f080165

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 103
    .local v5, "complete":Landroid/widget/TextView;
    const v15, 0x7f080166

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 104
    .local v6, "date":Landroid/widget/TextView;
    const v15, 0x7f080128

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    .line 105
    .local v12, "rightButton":Landroid/widget/ImageView;
    const v15, 0x7f080129

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 107
    .local v7, "divider":Landroid/view/View;
    const v15, 0x7f080127

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;

    .line 109
    .local v11, "progress":Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;
    invoke-virtual/range {p0 .. p2}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getChild(II)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    .line 111
    .local v10, "missionData":Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    if-eqz v10, :cond_a

    .line 112
    if-eqz v9, :cond_0

    .line 113
    invoke-virtual {v10}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getIconDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v15

    invoke-virtual {v9, v15}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 115
    :cond_0
    invoke-virtual {v10}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getTitleString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    if-eqz v12, :cond_1

    .line 118
    const/16 v15, 0x8

    invoke-virtual {v12, v15}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 119
    :cond_1
    const/4 v15, 0x0

    invoke-virtual {v11, v15}, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->setVisibility(I)V

    .line 120
    invoke-virtual {v10}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getSelectFrequencyDays()I

    move-result v15

    invoke-virtual {v11, v15}, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->setIndicatorCount(I)V

    .line 121
    invoke-virtual {v10}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getMissionCheckCount()I

    move-result v15

    invoke-virtual {v11, v15}, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->setProgressIndicatorCount(I)V

    .line 123
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mMainType:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    sget-object v16, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;->HISTORY:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    move-object/from16 v0, v16

    if-ne v15, v0, :cond_7

    .line 124
    const/4 v15, 0x0

    invoke-virtual {v5, v15}, Landroid/widget/TextView;->setVisibility(I)V

    .line 126
    invoke-virtual {v10}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getMissionStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getMissionStatusString(Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v5, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    invoke-virtual {v10}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getCompleteDate()Ljava/util/Calendar;

    move-result-object v15

    invoke-virtual {v15}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v15

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_6

    .line 128
    const/4 v15, 0x0

    invoke-virtual {v6, v15}, Landroid/widget/TextView;->setVisibility(I)V

    .line 129
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getCompleteDate()Ljava/util/Calendar;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v16

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil;->getMonthLen3DateFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    .end local p5    # "parent":Landroid/view/ViewGroup;
    :cond_2
    :goto_1
    invoke-virtual/range {p0 .. p2}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->isEndPositionOfGourp(II)Z

    move-result v15

    if-eqz v15, :cond_b

    .line 161
    const/16 v15, 0x8

    invoke-virtual {v7, v15}, Landroid/view/View;->setVisibility(I)V

    .line 166
    :goto_2
    return-object v14

    .line 91
    .end local v5    # "complete":Landroid/widget/TextView;
    .end local v6    # "date":Landroid/widget/TextView;
    .end local v7    # "divider":Landroid/view/View;
    .end local v9    # "icon":Landroid/widget/ImageView;
    .end local v10    # "missionData":Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    .end local v11    # "progress":Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;
    .end local v12    # "rightButton":Landroid/widget/ImageView;
    .end local v13    # "title":Landroid/widget/TextView;
    .restart local p5    # "parent":Landroid/view/ViewGroup;
    :cond_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mMainType:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    sget-object v16, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;->DELETE:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    move-object/from16 v0, v16

    if-ne v15, v0, :cond_4

    .line 92
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mContext:Landroid/content/Context;

    const v16, 0x7f030043

    const/16 v17, 0x0

    invoke-static/range {v15 .. v17}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v14

    goto/16 :goto_0

    .line 94
    :cond_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mContext:Landroid/content/Context;

    const v16, 0x7f030047

    const/16 v17, 0x0

    invoke-static/range {v15 .. v17}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v14

    goto/16 :goto_0

    .line 97
    :cond_5
    move-object/from16 v14, p4

    goto/16 :goto_0

    .line 131
    .restart local v5    # "complete":Landroid/widget/TextView;
    .restart local v6    # "date":Landroid/widget/TextView;
    .restart local v7    # "divider":Landroid/view/View;
    .restart local v9    # "icon":Landroid/widget/ImageView;
    .restart local v10    # "missionData":Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    .restart local v11    # "progress":Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;
    .restart local v12    # "rightButton":Landroid/widget/ImageView;
    .restart local v13    # "title":Landroid/widget/TextView;
    :cond_6
    const/4 v15, 0x4

    invoke-virtual {v6, v15}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 133
    :cond_7
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mMainType:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    sget-object v16, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;->DELETE:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    move-object/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 134
    const v15, 0x7f080162

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    .line 135
    .local v4, "checkBox":Landroid/widget/CheckBox;
    const/4 v15, 0x0

    invoke-virtual {v4, v15}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 136
    new-instance v15, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedChildChangeListener;

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    invoke-direct {v15, v0, v10, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedChildChangeListener;-><init>(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;Lcom/sec/android/app/shealth/cignacoach/data/MissionData;II)V

    invoke-virtual {v4, v15}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 138
    invoke-virtual {v10}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->isChecked()Z

    move-result v15

    if-eqz v15, :cond_8

    .line 139
    const/4 v15, 0x1

    invoke-virtual {v4, v15}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 144
    :goto_3
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;

    .line 145
    .local v8, "groupData":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->isChecked()Z

    move-result v15

    if-nez v15, :cond_9

    const/4 v15, 0x1

    :goto_4
    invoke-virtual {v4, v15}, Landroid/widget/CheckBox;->setEnabled(Z)V

    goto :goto_1

    .line 141
    .end local v8    # "groupData":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    :cond_8
    const/4 v15, 0x0

    invoke-virtual {v4, v15}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_3

    .line 145
    .restart local v8    # "groupData":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    :cond_9
    const/4 v15, 0x0

    goto :goto_4

    .line 149
    .end local v4    # "checkBox":Landroid/widget/CheckBox;
    .end local v8    # "groupData":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    :cond_a
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mMainType:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    sget-object v16, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;->GOAL:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    move-object/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 150
    const v15, 0x7f090c7b

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->setText(I)V

    .line 151
    const/16 v15, 0x8

    invoke-virtual {v11, v15}, Lcom/sec/android/app/shealth/cignacoach/widget/CircleProgressIndicatorView;->setVisibility(I)V

    .line 152
    const/4 v15, 0x0

    invoke-virtual {v12, v15}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 153
    const v15, 0x7f02010f

    invoke-virtual {v12, v15}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 154
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    const v17, 0x7f09003f

    invoke-virtual/range {v16 .. v17}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    const v17, 0x7f090c7b

    invoke-virtual/range {v16 .. v17}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->deCapitalizeFirstLetter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v12, v15}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 156
    new-instance v15, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$NewMissionClickListener;

    check-cast p5, Landroid/widget/ExpandableListView;

    .end local p5    # "parent":Landroid/view/ViewGroup;
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    move/from16 v2, p1

    move/from16 v3, p2

    invoke-direct {v15, v0, v1, v2, v3}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$NewMissionClickListener;-><init>(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;Landroid/widget/ExpandableListView;II)V

    invoke-virtual {v12, v15}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    .line 163
    :cond_b
    const/4 v15, 0x0

    invoke-virtual {v7, v15}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2
.end method

.method public getChildrenCount(I)I
    .locals 3
    .param p1, "groupPosition"    # I

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mChildData:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mMainType:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;->GOAL:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGroup(I)Ljava/lang/Object;
    .locals 1
    .param p1, "groupPosition"    # I

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mGroupData:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getGroupCount()I
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mGroupData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getGroupId(I)J
    .locals 2
    .param p1, "groupPosition"    # I

    .prologue
    .line 186
    int-to-long v0, p1

    return-wide v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 17
    .param p1, "groupPosition"    # I
    .param p2, "isExpanded"    # Z
    .param p3, "convertView"    # Landroid/view/View;
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 192
    const/4 v13, 0x0

    .line 194
    .local v13, "view":Landroid/view/View;
    if-nez p3, :cond_8

    .line 195
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mMainType:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    sget-object v15, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;->GOAL:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    if-ne v14, v15, :cond_6

    .line 196
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mContext:Landroid/content/Context;

    const v15, 0x7f030036

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v13

    .line 206
    :goto_0
    const v14, 0x7f08012d

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    .line 207
    .local v9, "icon":Landroid/widget/ImageView;
    const v14, 0x7f080126

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 208
    .local v12, "title":Landroid/widget/TextView;
    const v14, 0x7f08012e

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 209
    .local v7, "frequency":Landroid/widget/TextView;
    const v14, 0x7f080166

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 210
    .local v5, "date":Landroid/widget/TextView;
    const v14, 0x7f080131

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 211
    .local v3, "arrow":Landroid/widget/ImageView;
    const v14, 0x7f080164

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    .line 212
    .local v4, "checkBox":Landroid/widget/CheckBox;
    const v14, 0x7f080129

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 214
    .local v6, "divider":Landroid/view/View;
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;

    .line 215
    .local v8, "goalData":Lcom/sec/android/app/shealth/cignacoach/data/GoalData;
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->getIconDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v14

    invoke-virtual {v9, v14}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 216
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->getTitleString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 218
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->getExtraString()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_0

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->getExtraString()Ljava/lang/String;

    move-result-object v14

    const-string v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 219
    :cond_0
    const/16 v14, 0x8

    invoke-virtual {v7, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 226
    :goto_1
    if-eqz p2, :cond_a

    .line 227
    if-eqz v3, :cond_1

    .line 228
    const v14, 0x7f020846

    invoke-virtual {v3, v14}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 229
    const/4 v14, 0x0

    invoke-virtual {v3, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 230
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mContext:Landroid/content/Context;

    const v15, 0x7f090b6d

    invoke-virtual {v14, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v3, v14}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 232
    :cond_1
    const/16 v14, 0x8

    invoke-virtual {v6, v14}, Landroid/view/View;->setVisibility(I)V

    .line 233
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mMainType:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    sget-object v15, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;->DELETE:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    if-eq v14, v15, :cond_2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mMainType:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    sget-object v15, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;->HISTORY:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    if-ne v14, v15, :cond_3

    :cond_2
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getChildrenCount(I)I

    move-result v14

    if-nez v14, :cond_3

    .line 234
    const/4 v14, 0x0

    invoke-virtual {v6, v14}, Landroid/view/View;->setVisibility(I)V

    .line 235
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mMainType:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    sget-object v15, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;->HISTORY:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    if-ne v14, v15, :cond_3

    if-eqz v3, :cond_3

    .line 236
    const/16 v14, 0x8

    invoke-virtual {v3, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 253
    :cond_3
    :goto_2
    if-eqz v3, :cond_4

    .line 255
    new-instance v14, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$ArrowClickListener;

    check-cast p4, Landroid/widget/ExpandableListView;

    .end local p4    # "parent":Landroid/view/ViewGroup;
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move/from16 v2, p1

    invoke-direct {v14, v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$ArrowClickListener;-><init>(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;Landroid/widget/ExpandableListView;I)V

    invoke-virtual {v3, v14}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 258
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mMainType:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    sget-object v15, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;->GOAL:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    if-ne v14, v15, :cond_c

    .line 259
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0b0008

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 260
    const v14, 0x7f08012f

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 261
    .local v11, "missionCompleteCountTxt":Landroid/widget/TextView;
    const v14, 0x7f080130

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 262
    .local v10, "minFrequencyTxt":Landroid/widget/TextView;
    const/4 v14, 0x0

    invoke-virtual {v11, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 263
    const/4 v14, 0x0

    invoke-virtual {v10, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 264
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->getMissionCompleteCount()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 265
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->getMinFrequency()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v10, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 306
    .end local v10    # "minFrequencyTxt":Landroid/widget/TextView;
    .end local v11    # "missionCompleteCountTxt":Landroid/widget/TextView;
    :cond_5
    :goto_3
    return-object v13

    .line 197
    .end local v3    # "arrow":Landroid/widget/ImageView;
    .end local v4    # "checkBox":Landroid/widget/CheckBox;
    .end local v5    # "date":Landroid/widget/TextView;
    .end local v6    # "divider":Landroid/view/View;
    .end local v7    # "frequency":Landroid/widget/TextView;
    .end local v8    # "goalData":Lcom/sec/android/app/shealth/cignacoach/data/GoalData;
    .end local v9    # "icon":Landroid/widget/ImageView;
    .end local v12    # "title":Landroid/widget/TextView;
    .restart local p4    # "parent":Landroid/view/ViewGroup;
    :cond_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mMainType:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    sget-object v15, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;->DELETE:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    if-ne v14, v15, :cond_7

    .line 198
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mContext:Landroid/content/Context;

    const v15, 0x7f030045

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v13

    goto/16 :goto_0

    .line 200
    :cond_7
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mContext:Landroid/content/Context;

    const v15, 0x7f030048

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v13

    goto/16 :goto_0

    .line 203
    :cond_8
    move-object/from16 v13, p3

    goto/16 :goto_0

    .line 221
    .restart local v3    # "arrow":Landroid/widget/ImageView;
    .restart local v4    # "checkBox":Landroid/widget/CheckBox;
    .restart local v5    # "date":Landroid/widget/TextView;
    .restart local v6    # "divider":Landroid/view/View;
    .restart local v7    # "frequency":Landroid/widget/TextView;
    .restart local v8    # "goalData":Lcom/sec/android/app/shealth/cignacoach/data/GoalData;
    .restart local v9    # "icon":Landroid/widget/ImageView;
    .restart local v12    # "title":Landroid/widget/TextView;
    :cond_9
    const/4 v14, 0x0

    invoke-virtual {v7, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 222
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->getExtraString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v7, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 241
    :cond_a
    if-eqz v3, :cond_b

    .line 242
    const v14, 0x7f02084d

    invoke-virtual {v3, v14}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 243
    const/4 v14, 0x0

    invoke-virtual {v3, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 244
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mContext:Landroid/content/Context;

    const v15, 0x7f09021a

    invoke-virtual {v14, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v3, v14}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 246
    :cond_b
    const/4 v14, 0x0

    invoke-virtual {v6, v14}, Landroid/view/View;->setVisibility(I)V

    .line 247
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mMainType:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    sget-object v15, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;->HISTORY:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    if-ne v14, v15, :cond_3

    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getChildrenCount(I)I

    move-result v14

    if-nez v14, :cond_3

    if-eqz v3, :cond_3

    .line 248
    const/16 v14, 0x8

    invoke-virtual {v3, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 267
    .end local p4    # "parent":Landroid/view/ViewGroup;
    :cond_c
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mMainType:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    sget-object v15, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;->HISTORY:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    if-ne v14, v15, :cond_d

    .line 268
    if-eqz v5, :cond_5

    .line 269
    const/4 v14, 0x0

    invoke-virtual {v5, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 270
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getGoalStatusString(Lcom/sec/android/app/shealth/cignacoach/data/GoalData;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v5, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 272
    :cond_d
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mMainType:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    sget-object v15, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;->DELETE:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    if-ne v14, v15, :cond_5

    .line 274
    const/4 v14, 0x0

    invoke-virtual {v4, v14}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 275
    const/4 v14, 0x0

    invoke-virtual {v4, v14}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 277
    new-instance v14, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$1;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;)V

    invoke-virtual {v4, v14}, Landroid/widget/CheckBox;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 287
    new-instance v14, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedGroupChangeListener;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v14, v0, v8, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedGroupChangeListener;-><init>(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;Lcom/sec/android/app/shealth/cignacoach/data/GoalData;I)V

    invoke-virtual {v4, v14}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 288
    new-instance v14, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$2;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v14, v0, v8, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;Lcom/sec/android/app/shealth/cignacoach/data/GoalData;I)V

    invoke-virtual {v13, v14}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 299
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->isChecked()Z

    move-result v14

    if-eqz v14, :cond_e

    .line 300
    const/4 v14, 0x1

    invoke-virtual {v4, v14}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_3

    .line 302
    :cond_e
    const/4 v14, 0x0

    invoke-virtual {v4, v14}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_3
.end method

.method public getMainType()Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mMainType:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    return-object v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 343
    const/4 v0, 0x0

    return v0
.end method

.method public isChildSelectable(II)Z
    .locals 1
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 348
    const/4 v0, 0x1

    return v0
.end method

.method public isEndPositionOfGourp(II)Z
    .locals 1
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 353
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getChildrenCount(I)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p2, v0, :cond_0

    .line 354
    const/4 v0, 0x1

    .line 357
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setMainType(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;)V
    .locals 0
    .param p1, "type"    # Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mMainType:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    .line 62
    return-void
.end method

.method public setOnCheckedCountListener(Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;

    .prologue
    .line 451
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mCheckedCountListener:Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;

    .line 452
    return-void
.end method

.method public setSelectAll()V
    .locals 1

    .prologue
    .line 407
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->allChildItemCheck(Z)V

    .line 408
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->notifyDataSetChanged()V

    .line 409
    return-void
.end method

.method public setUnSelectAll()V
    .locals 1

    .prologue
    .line 412
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->allChildItemCheck(Z)V

    .line 413
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->notifyDataSetChanged()V

    .line 414
    return-void
.end method
