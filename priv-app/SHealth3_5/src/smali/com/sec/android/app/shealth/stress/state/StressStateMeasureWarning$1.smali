.class Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning$1;
.super Landroid/os/CountDownTimer;
.source "StressStateMeasureWarning.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;JJ)V
    .locals 0
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning$1;->this$0:Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 2

    .prologue
    .line 66
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning$1;->this$0:Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;

    # getter for: Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;->isInCurrentState:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;->access$000(Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 67
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning$1;->this$0:Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;->terminateState()V

    .line 68
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning$1;->this$0:Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;

    iget-object v1, v1, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getStateMeasureFailed()Lcom/sec/android/app/shealth/stress/state/StressState;

    move-result-object v0

    .line 69
    .local v0, "nextState":Lcom/sec/android/app/shealth/stress/state/StressState;
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning$1;->this$0:Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;

    iget-object v1, v1, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->setState(Lcom/sec/android/app/shealth/stress/state/StressState;)V

    .line 70
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/state/StressState;->updateViewWithAnimation(Z)V

    .line 72
    .end local v0    # "nextState":Lcom/sec/android/app/shealth/stress/state/StressState;
    :cond_0
    return-void
.end method

.method public onTick(J)V
    .locals 1
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning$1;->this$0:Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;

    # getter for: Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;->isInCurrentState:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;->access$000(Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 60
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning$1;->cancel()V

    .line 62
    :cond_0
    return-void
.end method
