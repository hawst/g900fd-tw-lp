.class public abstract Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;
.super Landroid/widget/BaseAdapter;
.source "CignaCaterogyListAdapter.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Landroid/widget/SectionIndexer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter$MorePagesListener;
    }
.end annotation


# instance fields
.field protected mLoadComplete:Z

.field mNnextLoading:Z

.field morePagesListener:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter$MorePagesListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 14
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;->mLoadComplete:Z

    .line 26
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;->mNnextLoading:Z

    return-void
.end method


# virtual methods
.method protected abstract bindSectionHeader(Landroid/view/View;IZ)V
.end method

.method public abstract getCurrentView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public abstract getPositionForSection(I)I
.end method

.method public abstract getSectionForPosition(I)I
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 74
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;->getCurrentView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 80
    .local v1, "res":Landroid/view/View;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;->getSectionForPosition(I)I

    move-result v2

    .line 81
    .local v2, "section":I
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;->getPositionForSection(I)I

    move-result v3

    if-ne v3, p1, :cond_0

    const/4 v0, 0x1

    .line 83
    .local v0, "displaySectionHeaders":Z
    :goto_0
    invoke-virtual {p0, v1, p1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;->bindSectionHeader(Landroid/view/View;IZ)V

    .line 85
    return-object v1

    .line 81
    .end local v0    # "displaySectionHeaders":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyNoMorePages()V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;->mNnextLoading:Z

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;->morePagesListener:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter$MorePagesListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;->morePagesListener:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter$MorePagesListener;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter$MorePagesListener;->noMorePages()V

    .line 91
    :cond_0
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 66
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 70
    return-void
.end method

.method setMorePagesListener(Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter$MorePagesListener;)V
    .locals 0
    .param p1, "morePagesListener"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter$MorePagesListener;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter;->morePagesListener:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCaterogyListAdapter$MorePagesListener;

    .line 31
    return-void
.end method
