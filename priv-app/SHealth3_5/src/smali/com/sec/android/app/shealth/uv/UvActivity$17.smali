.class Lcom/sec/android/app/shealth/uv/UvActivity$17;
.super Ljava/lang/Object;
.source "UvActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/uv/UvActivity;->showNoSensorInformationDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/UvActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/UvActivity;)V
    .locals 0

    .prologue
    .line 813
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvActivity$17;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/app/Activity;)V
    .locals 4
    .param p1, "parentActivity"    # Landroid/app/Activity;

    .prologue
    const/4 v3, 0x0

    .line 818
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$17;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->backpressed:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$2500(Lcom/sec/android/app/shealth/uv/UvActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 819
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$17;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # setter for: Lcom/sec/android/app/shealth/uv/UvActivity;->backpressed:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$2502(Lcom/sec/android/app/shealth/uv/UvActivity;Z)Z

    .line 832
    :goto_0
    return-void

    .line 823
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$17;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;->getDataCount()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$17;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "skin_type_checked"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 826
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$17;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$2700(Lcom/sec/android/app/shealth/uv/UvActivity;)Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity$17;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mDrawerLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$2600(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->closeDrawer(Landroid/view/View;)V

    .line 827
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$17;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mDialogHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$2100(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 829
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$17;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$400(Lcom/sec/android/app/shealth/uv/UvActivity;)Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    move-result-object v0

    iput-boolean v3, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->needsLocationPopup:Z

    .line 830
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$17;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity$17;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$400(Lcom/sec/android/app/shealth/uv/UvActivity;)Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/UvActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    goto :goto_0
.end method
