.class public Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;
.super Ljava/lang/Object;
.source "EditProfileActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "OnItemClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V
    .locals 0

    .prologue
    .line 909
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const v7, 0x7f09020b

    const v6, 0x7f09020a

    const v5, 0x7f0901f0

    const v4, 0x2e636

    const v3, 0x7f07011b

    .line 916
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->checkEditFocus()Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$200(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1005
    :goto_0
    :sswitch_0
    return-void

    .line 921
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 924
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$300(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 926
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$300(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->dismissAllowingStateLoss()V

    .line 927
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$302(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    .line 929
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->isImage:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$400(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 931
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->createSetPicturePopup(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$500(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;Z)V

    goto :goto_0

    .line 935
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->createSetPicturePopup(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$500(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;Z)V

    goto :goto_0

    .line 941
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->useRankingCheck:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$600(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/CheckBox;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->useRankingCheck:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$600(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 942
    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->useRankingCheck:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$600(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-static {p1, v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->announceForCheckBoxAccessibility(Landroid/view/View;Ljava/lang/String;ZZ)V

    .line 943
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->useRankingCheck:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$600(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 945
    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->gender:I
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$700()I

    move-result v0

    if-ne v0, v4, :cond_4

    .line 947
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->femaleIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$800(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0205dd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 948
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->femaleText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$900(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    .line 941
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 952
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->maleIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$1000(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0205ee

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 953
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->maleText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$1100(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    .line 958
    :cond_5
    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->gender:I
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$700()I

    move-result v0

    if-ne v0, v4, :cond_6

    .line 960
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->femaleIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$800(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0205dd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 961
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->femaleText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$900(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    .line 965
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->maleIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$1000(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0205ee

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 966
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->maleText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$1100(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    .line 972
    :sswitch_3
    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->gender:I
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$700()I

    move-result v0

    if-ne v0, v4, :cond_7

    .line 974
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->femaleIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$800(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0205dc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 975
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->femaleText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$900(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700e3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 977
    :cond_7
    const v0, 0x2e635

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->gender:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$702(I)I

    .line 978
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->maleIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$1000(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0205ee

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 979
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->maleText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$1100(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 980
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->genderImage:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$1200(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0205eb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 981
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mMaleLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$1300(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09081e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0901f1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 982
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mFemaleLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$1400(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09081f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0901f2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 983
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->hideParentKeyboard()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$1500(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V

    .line 984
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->hideDateKeyboard()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$1600(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V

    goto/16 :goto_0

    .line 988
    :sswitch_4
    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->gender:I
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$700()I

    move-result v0

    const v1, 0x2e635

    if-ne v0, v1, :cond_8

    .line 990
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->maleIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$1000(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0205ec

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 991
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->maleText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$1100(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700e3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 993
    :cond_8
    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->gender:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$702(I)I

    .line 994
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->femaleIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$800(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0205dd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 995
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->femaleText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$900(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 996
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->genderImage:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$1200(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0205db

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 997
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mMaleLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$1300(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09081e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0901f2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 998
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mFemaleLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$1400(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09081f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0901f1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 999
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->hideParentKeyboard()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$1500(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V

    .line 1000
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->hideDateKeyboard()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$1600(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V

    goto/16 :goto_0

    .line 921
    :sswitch_data_0
    .sparse-switch
        0x7f080332 -> :sswitch_1
        0x7f080335 -> :sswitch_0
        0x7f080848 -> :sswitch_3
        0x7f08084b -> :sswitch_4
        0x7f08084f -> :sswitch_2
    .end sparse-switch
.end method
