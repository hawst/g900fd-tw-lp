.class Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$3;
.super Ljava/lang/Object;
.source "PortionSizePopup.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)V
    .locals 0

    .prologue
    .line 257
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 3
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 260
    sget-object v1, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$7;->$SwitchMap$com$sec$android$app$shealth$common$commonui$dialog$DialogButtonType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 299
    :cond_0
    :goto_0
    return-void

    .line 262
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeInputModule:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$000(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 263
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeInputModule:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$000(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;->stopInertia()V

    goto :goto_0

    .line 267
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizePopupResultListener:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$PortionSizePopupResultListener;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$600(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$PortionSizePopupResultListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 269
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mFragmentManager:Landroid/support/v4/app/FragmentManager;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$300(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 271
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mFragmentManager:Landroid/support/v4/app/FragmentManager;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$300(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "PORTION_SIZE_POPUP_TAG"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 272
    .local v0, "dialog":Landroid/support/v4/app/DialogFragment;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeInputModule:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$000(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->length()I

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeInputModule:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$000(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;->getValue()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_2

    .line 282
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    iget-object v1, v1, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mEmptyFieldListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    invoke-interface {v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;->processEmptyField()V

    goto :goto_0

    .line 286
    :cond_2
    if-eqz v0, :cond_3

    .line 288
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 290
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizePopupResultListener:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$PortionSizePopupResultListener;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$600(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$PortionSizePopupResultListener;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeHolder:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$700(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$PortionSizePopupResultListener;->resultOk(Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;)V

    goto :goto_0

    .line 260
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
