.class public Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
.source "WalkingMateInactiveTimeFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissControllerProvider;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "DefaultLocale"
    }
.end annotation


# static fields
.field public static final INACTIVEDIALOGPOPUP:Ljava/lang/String; = "INACTIVE_TIME_SET_DIALOG"

.field private static final TAG:Ljava/lang/String; = "WalkingMateInactiveTimeFragment"


# instance fields
.field private mInactivePeriod:Landroid/widget/TextView;

.field private mInactiveSetTime:Landroid/widget/TextView;

.field private mPeriod:Landroid/widget/LinearLayout;

.field public mProcessClick:Z

.field private mSetTime:Landroid/widget/LinearLayout;

.field private mView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->mView:Landroid/view/View;

    .line 28
    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->mPeriod:Landroid/widget/LinearLayout;

    .line 29
    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->mSetTime:Landroid/widget/LinearLayout;

    .line 31
    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->mInactivePeriod:Landroid/widget/TextView;

    .line 32
    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->mInactiveSetTime:Landroid/widget/TextView;

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->mProcessClick:Z

    return-void
.end method


# virtual methods
.method public getDismissController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 36
    const/4 v0, 0x0

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 76
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->setCurrentInactiveConfig()V

    .line 78
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 12
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const v7, 0x7f090d71

    const/16 v11, 0x1e

    const/4 v10, 0x0

    const/4 v9, 0x4

    const/4 v8, 0x1

    .line 41
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 72
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 43
    :pswitch_1
    iget-boolean v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->mProcessClick:Z

    if-nez v4, :cond_0

    .line 44
    iput-boolean v8, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->mProcessClick:Z

    .line 45
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    const v4, 0x7f0907c2

    const/16 v5, 0xc

    invoke-direct {v0, v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;-><init>(II)V

    .line 46
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 47
    .local v3, "unitsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const v4, 0x7f090f6d

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v10

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 48
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 49
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090f6d

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 50
    const v4, 0x7f090d72

    new-array v5, v8, [Ljava/lang/Object;

    const/4 v6, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v10

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setItems(Ljava/util/List;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 52
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getInactiveTimeTrackingPeriod()I

    move-result v4

    if-ne v4, v11, :cond_1

    .line 53
    new-array v4, v9, [Z

    fill-array-data v4, :array_0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setChecked([Z)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 63
    :goto_1
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    move-result-object v2

    .line 64
    .local v2, "popup":Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "INACTIVE_TIME_SET_DIALOG"

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 54
    .end local v2    # "popup":Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getInactiveTimeTrackingPeriod()I

    move-result v4

    const/16 v5, 0x3c

    if-ne v4, v5, :cond_2

    .line 55
    new-array v4, v9, [Z

    fill-array-data v4, :array_1

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setChecked([Z)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    goto :goto_1

    .line 56
    :cond_2
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getInactiveTimeTrackingPeriod()I

    move-result v4

    const/16 v5, 0x5a

    if-ne v4, v5, :cond_3

    .line 57
    new-array v4, v9, [Z

    fill-array-data v4, :array_2

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setChecked([Z)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    goto :goto_1

    .line 58
    :cond_3
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getInactiveTimeTrackingPeriod()I

    move-result v4

    const/16 v5, 0x78

    if-ne v4, v5, :cond_4

    .line 59
    new-array v4, v9, [Z

    fill-array-data v4, :array_3

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setChecked([Z)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    goto :goto_1

    .line 61
    :cond_4
    const-string v4, "WalkingMateInactiveTimeFragment"

    const-string v5, "Unsupported option"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 68
    .end local v0    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;
    .end local v3    # "unitsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :pswitch_2
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const-class v5, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeActivity;

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 69
    .local v1, "intent":Landroid/content/Intent;
    const/16 v4, 0x438

    invoke-virtual {p0, v1, v4}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 41
    nop

    :pswitch_data_0
    .packed-switch 0x7f080be8
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 53
    :array_0
    .array-data 1
        0x1t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 55
    :array_1
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 57
    :array_2
    .array-data 1
        0x0t
        0x0t
        0x1t
        0x0t
    .end array-data

    .line 59
    :array_3
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x1t
    .end array-data
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 83
    const v0, 0x7f0302a7

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->mView:Landroid/view/View;

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->mView:Landroid/view/View;

    const v1, 0x7f080be8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->mPeriod:Landroid/widget/LinearLayout;

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->mView:Landroid/view/View;

    const v1, 0x7f080bea

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->mSetTime:Landroid/widget/LinearLayout;

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->mPeriod:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->mSetTime:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->mView:Landroid/view/View;

    const v1, 0x7f080be9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->mInactivePeriod:Landroid/widget/TextView;

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->mView:Landroid/view/View;

    const v1, 0x7f080beb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->mInactiveSetTime:Landroid/widget/TextView;

    .line 90
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->setCurrentInactiveConfig()V

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->mView:Landroid/view/View;

    return-object v0
.end method

.method public setCurrentInactiveConfig()V
    .locals 12

    .prologue
    const v6, 0x7f090d71

    const/4 v11, 0x2

    const/16 v8, 0x1e

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 96
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getInactiveTimeTrackingPeriod()I

    move-result v2

    .line 97
    .local v2, "timeOut":I
    const-string v1, ""

    .line 98
    .local v1, "periodText":Ljava/lang/String;
    if-ne v2, v8, :cond_1

    .line 99
    const v5, 0x7f090f6d

    new-array v6, v10, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 108
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->mInactivePeriod:Landroid/widget/TextView;

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getInactiveTimeTrackingAvailableTimeAlways()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 110
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->mInactiveSetTime:Landroid/widget/TextView;

    const v6, 0x7f0907c7

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    :goto_1
    return-void

    .line 100
    :cond_1
    const/16 v5, 0x3c

    if-ne v2, v5, :cond_2

    .line 101
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 102
    :cond_2
    const/16 v5, 0x5a

    if-ne v2, v5, :cond_3

    .line 103
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090f6d

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 104
    :cond_3
    const/16 v5, 0x78

    if-ne v2, v5, :cond_0

    .line 105
    const v5, 0x7f090d72

    new-array v6, v10, [Ljava/lang/Object;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 113
    :cond_4
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getInactiveTimeTrackingAvailableTimeFrom()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->get24Hrformat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 114
    .local v0, "fromTime":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getInactiveTimeTrackingAvailableTimeTo()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->get24Hrformat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 117
    .local v4, "toTime":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-static {v5}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 118
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->get12Hrformat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 119
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->get12Hrformat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 122
    :cond_5
    const-string v5, "%s - %s"

    new-array v6, v11, [Ljava/lang/Object;

    aput-object v0, v6, v9

    aput-object v4, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 123
    .local v3, "timeText":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->mInactiveSetTime:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
