.class Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;
.super Landroid/os/AsyncTask;
.source "FoodLogListConcreteDelegate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CalorieCounterTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mEndOfDay:J

.field private mRowView:Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;

.field private mStartOfDay:J

.field final synthetic this$0:Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;Landroid/content/Context;Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;JZ)V
    .locals 3
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "view"    # Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;
    .param p4, "time"    # J
    .param p6, "isRowChanged"    # Z

    .prologue
    .line 336
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;->this$0:Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 337
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;->mContext:Landroid/content/Context;

    .line 338
    iput-object p3, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;->mRowView:Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;

    .line 339
    invoke-static {p4, p5}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;->mStartOfDay:J

    .line 340
    invoke-static {p4, p5}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;->mEndOfDay:J

    .line 341
    if-eqz p6, :cond_0

    .line 342
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;->mRowView:Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getRightTeamHeaderTextView()Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900b9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 344
    :cond_0
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 7
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 348
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;->this$0:Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;

    iget-wide v3, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;->mStartOfDay:J

    iget-wide v5, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;->mEndOfDay:J

    # invokes: Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->countFoodSumForDay(JJ)I
    invoke-static {v2, v3, v4, v5, v6}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->access$100(Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;JJ)I

    move-result v0

    .line 349
    .local v0, "foodSumForDay":I
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Integer;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;->publishProgress([Ljava/lang/Object;)V

    .line 350
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;->mContext:Landroid/content/Context;

    iget-wide v3, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;->mEndOfDay:J

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getGoalCaloriesByDate(Landroid/content/Context;J)F

    move-result v1

    .line 351
    .local v1, "goalCalories":F
    int-to-float v2, v0

    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isCaloriesIntakeGoalAchieved(FF)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    return-object v2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 330
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 4
    .param p1, "isGoalReached"    # Ljava/lang/Boolean;

    .prologue
    const/4 v3, 0x1

    .line 362
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 363
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;->mRowView:Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;

    const v2, 0x7f02058a

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->initMedalImageView(IZ)Landroid/widget/ImageView;

    move-result-object v0

    .line 364
    .local v0, "medalImageView":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 366
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090b6b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    :cond_0
    :goto_0
    return-void

    .line 369
    .end local v0    # "medalImageView":Landroid/widget/ImageView;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;->mRowView:Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->initMedalImageView(IZ)Landroid/widget/ImageView;

    move-result-object v0

    .line 370
    .restart local v0    # "medalImageView":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 372
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_NONE:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    const-string v2, ""

    const-string v3, ""

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 330
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 5
    .param p1, "foodSum"    # [Ljava/lang/Integer;

    .prologue
    const v4, 0x7f0900b9

    const/4 v3, 0x0

    .line 356
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;->mRowView:Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getRightTeamHeaderTextView()Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v2, p1, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 357
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;->mRowView:Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getRightTeamHeaderTextView()Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v2, p1, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901fd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 358
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 330
    check-cast p1, [Ljava/lang/Integer;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
