.class public Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;
.super Landroid/widget/LinearLayout;
.source "StatisticsDialogItem.java"

# interfaces
.implements Ljava/util/Observer;


# static fields
.field public static final DEFAULT_ICON_SRC:I = 0x7f0205c1


# instance fields
.field private isComparative:Z

.field private mBackupContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 40
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->mBackupContext:Landroid/content/Context;

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->mBackupContext:Landroid/content/Context;

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->mBackupContext:Landroid/content/Context;

    .line 51
    return-void
.end method

.method private maskingProfileImage(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 13
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v12, 0x0

    const v9, 0x7f0a0a99

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 249
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    .line 251
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 252
    .local v7, "width":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 254
    .local v1, "height":I
    invoke-static {p1, v7, v1, v11}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 255
    .local v2, "mainImage":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f020616

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 256
    .local v3, "mask":Landroid/graphics/Bitmap;
    invoke-static {v3, v7, v1, v11}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 257
    .local v5, "resizedBitmap":Landroid/graphics/Bitmap;
    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v1, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 259
    .local v6, "result":Landroid/graphics/Bitmap;
    invoke-virtual {v0, v6}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 260
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 261
    .local v4, "paint":Landroid/graphics/Paint;
    invoke-virtual {v4, v11}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 262
    const/4 v8, 0x0

    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 264
    invoke-virtual {v0, v2, v10, v10, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 265
    new-instance v8, Landroid/graphics/PorterDuffXfermode;

    sget-object v9, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v8, v9}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 266
    invoke-virtual {v0, v5, v10, v10, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 267
    invoke-virtual {v4, v12}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 269
    return-object v6
.end method


# virtual methods
.method public init(Z)V
    .locals 2
    .param p1, "isVertical"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->isComparative:Z

    .line 55
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->getContext()Landroid/content/Context;

    move-result-object v1

    if-eqz p1, :cond_0

    const v0, 0x7f0302af

    :goto_0
    invoke-static {v1, v0, p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 56
    return-void

    .line 55
    :cond_0
    const v0, 0x7f0302b0

    goto :goto_0
.end method

.method public invert(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 14
    .param p1, "src"    # Landroid/graphics/Bitmap;

    .prologue
    const/16 v13, 0x10

    .line 212
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v12

    invoke-static {v10, v11, v12}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 215
    .local v5, "output":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 216
    .local v4, "height":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    .line 217
    .local v7, "width":I
    const/4 v9, 0x0

    .local v9, "y":I
    :goto_0
    if-ge v9, v4, :cond_1

    .line 218
    const/4 v8, 0x0

    .local v8, "x":I
    :goto_1
    if-ge v8, v7, :cond_0

    .line 219
    invoke-virtual {p1, v8, v9}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v6

    .line 220
    .local v6, "pixelColor":I
    invoke-static {v6}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    .line 222
    .local v0, "A":I
    const-string v10, "71"

    invoke-static {v10, v13}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 223
    .local v3, "R":I
    const-string v10, "8a"

    invoke-static {v10, v13}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 224
    .local v2, "G":I
    const-string v10, "3f"

    invoke-static {v10, v13}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 226
    .local v1, "B":I
    invoke-static {v0, v3, v2, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v10

    invoke-virtual {v5, v8, v9, v10}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 218
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 217
    .end local v0    # "A":I
    .end local v1    # "B":I
    .end local v2    # "G":I
    .end local v3    # "R":I
    .end local v6    # "pixelColor":I
    :cond_0
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 230
    .end local v8    # "x":I
    :cond_1
    return-object v5
.end method

.method public setAge(Ljava/lang/String;)V
    .locals 2
    .param p1, "age"    # Ljava/lang/String;

    .prologue
    .line 133
    const v0, 0x7f080c03

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f08080a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    return-void
.end method

.method public setDistance(Ljava/lang/String;)V
    .locals 1
    .param p1, "distance"    # Ljava/lang/String;

    .prologue
    .line 146
    const v0, 0x7f080c05

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    return-void
.end method

.method public setGender(Ljava/lang/String;)V
    .locals 3
    .param p1, "gender"    # Ljava/lang/String;

    .prologue
    const v2, 0x7f080c04

    const v1, 0x7f08080a

    .line 137
    const-string v0, "0"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->mBackupContext:Landroid/content/Context;

    const v2, 0x7f09081e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    :goto_0
    return-void

    .line 139
    :cond_0
    const-string v0, "1"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 140
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->mBackupContext:Landroid/content/Context;

    const v2, 0x7f09081f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 142
    :cond_1
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->mBackupContext:Landroid/content/Context;

    const v2, 0x7f090d03

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setImageMasking(Landroid/graphics/drawable/Drawable;)V
    .locals 7
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 234
    const v5, 0x7f080c01

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .local v2, "img":Landroid/widget/ImageView;
    move-object v1, p1

    .line 235
    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .line 236
    .local v1, "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 237
    .local v3, "mainImage":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020616

    invoke-static {v5, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 238
    .local v4, "mask":Landroid/graphics/Bitmap;
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->invert(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 239
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->maskingProfileImage(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-direct {v0, v5, v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 240
    .local v0, "background":Landroid/graphics/drawable/Drawable;
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->maskingProfileImage(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 241
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 242
    invoke-virtual {v2}, Landroid/widget/ImageView;->invalidate()V

    .line 243
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 125
    const-string/jumbo v0, "unknown"

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090d03

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 129
    :cond_0
    const v0, 0x7f080c02

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f08080a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    return-void
.end method

.method public setPhotoDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 3
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 59
    const v0, 0x7f080c01

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0205c1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .end local p1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 60
    return-void
.end method

.method public setRankingPosition(I)V
    .locals 10
    .param p1, "position"    # I

    .prologue
    const v5, 0x7f090b78

    const/4 v9, -0x1

    const v8, 0x7f080bff

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 77
    add-int/lit8 v2, p1, 0x1

    .line 78
    .local v2, "ranking":I
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->getRankingString(I)I

    move-result v1

    .line 80
    .local v1, "rankStrType":I
    packed-switch v1, :pswitch_data_0

    .line 106
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 107
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090b7b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 113
    .local v0, "number":Ljava/lang/String;
    :goto_0
    const/4 v3, 0x2

    if-gt p1, v3, :cond_0

    iget-boolean v3, p0, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->isComparative:Z

    if-eqz v3, :cond_0

    if-ne p1, v9, :cond_6

    .line 114
    :cond_0
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 115
    if-ne p1, v9, :cond_5

    .line 116
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090bdd

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 122
    :goto_1
    return-void

    .line 82
    .end local v0    # "number":Ljava/lang/String;
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 83
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "number":Ljava/lang/String;
    goto :goto_0

    .line 85
    .end local v0    # "number":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 87
    .restart local v0    # "number":Ljava/lang/String;
    goto :goto_0

    .line 90
    .end local v0    # "number":Ljava/lang/String;
    :pswitch_1
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 91
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090b79

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "number":Ljava/lang/String;
    goto :goto_0

    .line 93
    .end local v0    # "number":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090b79

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 95
    .restart local v0    # "number":Ljava/lang/String;
    goto/16 :goto_0

    .line 98
    .end local v0    # "number":Ljava/lang/String;
    :pswitch_2
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 99
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090b7a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "number":Ljava/lang/String;
    goto/16 :goto_0

    .line 101
    .end local v0    # "number":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090b7a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 103
    .restart local v0    # "number":Ljava/lang/String;
    goto/16 :goto_0

    .line 109
    .end local v0    # "number":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090b7b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "number":Ljava/lang/String;
    goto/16 :goto_0

    .line 118
    :cond_5
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 120
    :cond_6
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 80
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setSteps(I)V
    .locals 7
    .param p1, "steps"    # I

    .prologue
    const v5, 0x7f090b8d

    const v2, 0x7f080c05

    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 151
    if-ne p1, v4, :cond_0

    .line 152
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "1"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090baa

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    :goto_0
    return-void

    .line 155
    :cond_0
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 157
    .local v1, "tv":Landroid/widget/TextView;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 158
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 162
    .local v0, "stepTxt":Ljava/lang/String;
    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 160
    .end local v0    # "stepTxt":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "stepTxt":Ljava/lang/String;
    goto :goto_1
.end method

.method public setTrophySrc(II)V
    .locals 2
    .param p1, "src"    # I
    .param p2, "position"    # I

    .prologue
    .line 64
    const v1, 0x7f080c00

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 65
    .local v0, "img":Landroid/widget/ImageView;
    const/4 v1, 0x2

    if-gt p2, v1, :cond_0

    const/4 v1, -0x1

    if-ne p2, v1, :cond_1

    .line 66
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 73
    :goto_0
    return-void

    .line 68
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 70
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public setValues(Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;)V
    .locals 8
    .param p1, "holder"    # Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    .prologue
    const v7, 0x7f090b7c

    .line 167
    if-eqz p1, :cond_1

    .line 168
    invoke-virtual {p1, p0}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->addObserver(Ljava/util/Observer;)V

    .line 169
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getRanking()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->setRankingPosition(I)V

    .line 170
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getImageBitmap()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    if-nez v4, :cond_3

    .line 171
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getIconUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 172
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0205c1

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->setImageMasking(Landroid/graphics/drawable/Drawable;)V

    .line 190
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getTrophy()Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;->getDialogIcon()I

    move-result v4

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getRanking()I

    move-result v5

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->setTrophySrc(II)V

    .line 191
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->setName(Ljava/lang/String;)V

    .line 193
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "unknown"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 194
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090d03

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->setAge(Ljava/lang/String;)V

    .line 199
    :goto_1
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getGender()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->setGender(Ljava/lang/String;)V

    .line 201
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getStep()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->setSteps(I)V

    .line 203
    :cond_1
    return-void

    .line 175
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getIconUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/walkingmate/utils/BitmapUtil;->getDrawableFromUrl(Ljava/lang/String;Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 176
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_0

    move-object v0, v1

    .line 177
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 178
    .local v0, "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 179
    .local v2, "mainImage":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020616

    invoke-static {v4, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 180
    .local v3, "mask":Landroid/graphics/Bitmap;
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->invert(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 182
    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->maskingProfileImage(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {p1, v4}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->setIconSrc(Landroid/graphics/drawable/Drawable;)V

    .line 183
    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->maskingProfileImage(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {p1, v4}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->setIconback(Landroid/graphics/drawable/Drawable;)V

    .line 184
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getImageBitmap()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->setImageMasking(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 188
    .end local v0    # "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v2    # "mainImage":Landroid/graphics/Bitmap;
    .end local v3    # "mask":Landroid/graphics/Bitmap;
    :cond_3
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getImageBitmap()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->setImageMasking(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 196
    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getAge()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->setAge(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 0
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "arg"    # Ljava/lang/Object;

    .prologue
    .line 207
    check-cast p2, Landroid/graphics/drawable/Drawable;

    .end local p2    # "arg":Ljava/lang/Object;
    invoke-virtual {p0, p2}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogItem;->setPhotoDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 208
    invoke-virtual {p1, p0}, Ljava/util/Observable;->deleteObserver(Ljava/util/Observer;)V

    .line 209
    return-void
.end method
