.class Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$10;
.super Ljava/lang/Object;
.source "HeartrateActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->showDeleteDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V
    .locals 0

    .prologue
    .line 454
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$10;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    const/4 v2, 0x1

    .line 458
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$10;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->access$000(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->deleteDailyData()V

    .line 459
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$10;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->access$000(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->startSensorAndDelayUIForDetectedFinger(I)V

    .line 460
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$10;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->access$000(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->updateBpmDataView(Z)V

    .line 461
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$10;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    iput-boolean v2, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isAutoStartup:Z

    .line 462
    return-void
.end method
