.class Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$1;
.super Ljava/lang/Object;
.source "WeightSetGoalWeightFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->initFooterButtons(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$1;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 173
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$1;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    # invokes: Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->getWeightData()Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    invoke-static {v1}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->access$000(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v0

    .line 174
    .local v0, "weightData":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    if-eqz v0, :cond_2

    .line 175
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$1;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    # getter for: Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;
    invoke-static {v1}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->access$100(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 191
    :goto_0
    return-void

    .line 179
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$1;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    # getter for: Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    invoke-static {v1}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->access$200(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v1

    if-nez v1, :cond_1

    .line 180
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$1;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    new-instance v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$1;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    # getter for: Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;
    invoke-static {v3}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->access$100(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;->getValue()Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$1;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->getCurrentUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertUnitToKg(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v3

    const v4, 0x9c44

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;-><init>(FI)V

    # setter for: Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->access$202(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    .line 185
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$1;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    # getter for: Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    invoke-static {v1}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->access$200(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$1;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    # getter for: Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mCal:Ljava/util/Calendar;
    invoke-static {v2}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->access$300(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->setSetTime(J)V

    .line 187
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$1;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->getGoalActivity()Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$1;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    # getter for: Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    invoke-static {v2}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->access$200(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;->showSetGoalCaloriesFragment(Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;)V

    goto :goto_0

    .line 182
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$1;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    # getter for: Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    invoke-static {v1}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->access$200(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$1;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    # getter for: Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;
    invoke-static {v2}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->access$100(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;->getValue()Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$1;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->getCurrentUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertUnitToKg(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->setValue(F)V

    goto :goto_1

    .line 189
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "No data in DB"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
