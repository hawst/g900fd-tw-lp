.class public Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;
.super Lcom/samsung/android/sdk/health/content/ShealthProfile;
.source "HShealthProfile.java"


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private mIsUpgradeToMainDb:Z

.field private name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-string v0, "HShealthProfile"

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/NullPointerException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->name:Ljava/lang/String;

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->mIsUpgradeToMainDb:Z

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->load()V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isUpgradeToMainDb"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/NullPointerException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->name:Ljava/lang/String;

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->mIsUpgradeToMainDb:Z

    .line 57
    iput-boolean p2, p0, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->mIsUpgradeToMainDb:Z

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->load()V

    .line 59
    return-void
.end method

.method private getDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;
    .locals 1

    .prologue
    .line 147
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->mIsUpgradeToMainDb:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getBackupWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    goto :goto_0
.end method

.method public static putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p0, "db"    # Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 136
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 137
    .local v2, "values":Landroid/content/ContentValues;
    const-string/jumbo v0, "value"

    invoke-virtual {v2, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    const-string/jumbo v1, "shared_pref"

    const-string v3, "key=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getMigrationPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)I

    move-result v0

    if-nez v0, :cond_0

    .line 140
    const-string v0, "key"

    invoke-virtual {v2, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    const-string/jumbo v0, "shared_pref"

    const/4 v1, 0x0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getMigrationPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Lcom/sec/android/service/health/cp/database/ValidationPolicy;)J

    .line 143
    :cond_0
    return-void
.end method


# virtual methods
.method public isExist()Z
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->name:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 94
    const/4 v0, 0x1

    .line 96
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 68
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->TAG:Ljava/lang/String;

    const-string v3, "dummy load api used only for migration"

    invoke-static {v1, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->getDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    .line 72
    .local v0, "db":Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;
    const-string/jumbo v1, "shared_pref"

    const-string v3, "key = \'name\' "

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 76
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 78
    const-string/jumbo v1, "value"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->name:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    :cond_0
    if-eqz v8, :cond_1

    .line 84
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 88
    :cond_1
    return-void

    .line 83
    :catchall_0
    move-exception v1

    if-eqz v8, :cond_2

    .line 84
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v1
.end method

.method public save()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 106
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 107
    .local v3, "profileUpdateTime":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 112
    .local v1, "profileCreateTime":J
    const-string v5, "Profile has been updated"

    invoke-static {v5}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    .line 113
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->getDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    .line 115
    .local v0, "db":Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;
    const-string/jumbo v5, "name"

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v5, v6}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const-string v5, "activity_type"

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->getActivityType()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v5, v6}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string v5, "birth_date"

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v5, v6}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    const-string v5, "distance_unit"

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->getDistanceUnit()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v5, v6}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string v5, "gender"

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->getGender()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v5, v6}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    const-string v5, "height"

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->getHeight()F

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v5, v6}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const-string v5, "height_unit"

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->getHeightUnit()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v5, v6}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const-string/jumbo v5, "update_time"

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v5, v6}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    const-string/jumbo v5, "temperature_unit"

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->getTemperatureUnit()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v5, v6}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    const-string/jumbo v5, "weight"

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->getWeight()F

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v5, v6}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const-string/jumbo v5, "weight_unit"

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->getWeightUnit()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v5, v6}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const-string/jumbo v6, "profile_disclose_yn"

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->getProfileDisclose()Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "Y"

    :goto_0
    invoke-static {v0, v6, v5}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    const-string/jumbo v5, "profile_create_time"

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v5, v6}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    const-string v5, "in_sync"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v5, v6}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->putConfigValue(Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    return-void

    .line 127
    :cond_0
    const-string v5, "N"

    goto :goto_0
.end method
