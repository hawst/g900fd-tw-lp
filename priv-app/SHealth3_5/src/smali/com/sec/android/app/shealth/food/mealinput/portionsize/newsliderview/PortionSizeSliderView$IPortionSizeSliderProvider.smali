.class public interface abstract Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$IPortionSizeSliderProvider;
.super Ljava/lang/Object;
.source "PortionSizeSliderView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IPortionSizeSliderProvider"
.end annotation


# virtual methods
.method public abstract getFoodName()Ljava/lang/String;
.end method

.method public abstract getInitialPortionValue()F
.end method

.method public abstract getMaxValue()I
.end method

.method public abstract getUnitName()Ljava/lang/String;
.end method
