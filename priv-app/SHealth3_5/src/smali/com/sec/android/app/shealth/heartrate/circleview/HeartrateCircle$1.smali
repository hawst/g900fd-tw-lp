.class Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle$1;
.super Ljava/lang/Object;
.source "HeartrateCircle.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->startAnimation(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

.field final synthetic val$isSuccess:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;Z)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    iput-boolean p2, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle$1;->val$isSuccess:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, -0x1

    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 74
    new-instance v0, Landroid/view/animation/RotateAnimation;

    const v2, 0x43b38000    # 359.0f

    move v5, v3

    move v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 75
    .local v0, "circleRotateAnimation":Landroid/view/animation/RotateAnimation;
    const-wide/16 v4, 0x3e8

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/RotateAnimation;->setDuration(J)V

    .line 76
    invoke-virtual {v0, v8}, Landroid/view/animation/RotateAnimation;->setRepeatCount(I)V

    .line 77
    invoke-virtual {v0, v8}, Landroid/view/animation/RotateAnimation;->setRepeatMode(I)V

    .line 78
    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v2}, Landroid/view/animation/RotateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 80
    new-instance v7, Landroid/view/animation/AlphaAnimation;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v7, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 81
    .local v7, "fadeIn":Landroid/view/animation/Animation;
    const-wide/16 v1, 0x14d

    invoke-virtual {v7, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 83
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    new-instance v2, Landroid/view/animation/AnimationSet;

    invoke-direct {v2, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    # setter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->access$002(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;Landroid/view/animation/AnimationSet;)Landroid/view/animation/AnimationSet;

    .line 84
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->access$000(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;)Landroid/view/animation/AnimationSet;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 85
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->access$000(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;)Landroid/view/animation/AnimationSet;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle$1;->val$isSuccess:Z

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->changeColor(Z)V

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->mMeasuringAniLinear:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->access$100(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;)Landroid/widget/LinearLayout;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->mMeasuringAniLinear:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->access$100(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;)Landroid/widget/LinearLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->access$000(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;)Landroid/view/animation/AnimationSet;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 90
    return-void
.end method
