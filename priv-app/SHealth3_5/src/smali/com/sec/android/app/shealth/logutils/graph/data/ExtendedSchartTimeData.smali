.class public Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;
.super Lcom/samsung/android/sdk/chart/series/SchartTimeData;
.source "ExtendedSchartTimeData.java"

# interfaces
.implements Lcom/sec/android/app/shealth/logutils/graph/data/ICommonChartData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 28
    return-void
.end method

.method public constructor <init>(JD)V
    .locals 0
    .param p1, "time"    # J
    .param p3, "value"    # D

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>(JD)V

    .line 32
    return-void
.end method


# virtual methods
.method public getMaxDataValue()D
    .locals 6

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;->getValue()D

    move-result-wide v1

    .line 37
    .local v1, "sum":D
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;->getMultiValues()Ljava/util/LinkedList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    .line 38
    .local v3, "value":D
    add-double/2addr v1, v3

    .line 39
    goto :goto_0

    .line 40
    .end local v3    # "value":D
    :cond_0
    return-wide v1
.end method

.method public getMinDataValue()D
    .locals 6

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;->getValue()D

    move-result-wide v1

    .line 49
    .local v1, "sum":D
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;->getMultiValues()Ljava/util/LinkedList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    .line 51
    .local v3, "value":D
    add-double/2addr v1, v3

    .line 53
    goto :goto_0

    .line 55
    .end local v3    # "value":D
    :cond_0
    return-wide v1
.end method

.method public setData(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 65
    .local p1, "dataList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    const/4 v1, 0x0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    float-to-double v1, v1

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;->setValue(D)V

    .line 66
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 67
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    float-to-double v1, v1

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;->addMultiValue(D)V

    .line 66
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 69
    :cond_0
    return-void
.end method
