.class Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$6;
.super Ljava/lang/Object;
.source "WalkingMateRankingActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->setUserRanking()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;)V
    .locals 0

    .prologue
    .line 413
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 418
    :try_start_0
    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->access$800()Ljava/lang/String;

    move-result-object v1

    const-string v2, "call setUserRanking"

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->scm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->access$900(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;)Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    move-result-object v1

    const/16 v2, 0xc9

    new-instance v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$6$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$6$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$6;)V

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->startBackup(ILcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 448
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    # setter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->scm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->access$902(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    .line 450
    :goto_0
    return-void

    .line 445
    :catch_0
    move-exception v0

    .line 446
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_1
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 448
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    # setter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->scm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->access$902(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    goto :goto_0

    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    # setter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->scm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;
    invoke-static {v2, v4}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->access$902(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    throw v1
.end method
