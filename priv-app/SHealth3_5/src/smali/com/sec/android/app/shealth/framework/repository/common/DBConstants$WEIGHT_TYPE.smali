.class public final Lcom/sec/android/app/shealth/framework/repository/common/DBConstants$WEIGHT_TYPE;
.super Ljava/lang/Object;
.source "DBConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/repository/common/DBConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "WEIGHT_TYPE"
.end annotation


# static fields
.field public static final CM_UNIT:Ljava/lang/String; = "cm"

.field public static final ID:I = 0x3ef

.field public static final INCH_UNIT:Ljava/lang/String; = "inch"

.field public static final KG_UNIT:Ljava/lang/String; = "kg"

.field public static final LB_UNIT:Ljava/lang/String; = "lb"

.field public static final ROW_ID:I = 0x7d7


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
