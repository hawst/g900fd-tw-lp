.class Lcom/sec/android/app/shealth/SplashScreenActivity$7;
.super Ljava/lang/Object;
.source "SplashScreenActivity.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/SplashScreenActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/SplashScreenActivity;)V
    .locals 0

    .prologue
    .line 638
    iput-object p1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$7;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 6
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/16 v5, 0x8

    .line 650
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 651
    .local v0, "handler":Landroid/os/Handler;
    iget-object v2, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$7;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/SplashScreenActivity;->run:Ljava/lang/Runnable;

    const-wide/16 v3, 0x3e8

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 653
    iget-object v2, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$7;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    # getter for: Lcom/sec/android/app/shealth/SplashScreenActivity;->mCenterLayout:Landroid/widget/RelativeLayout;
    invoke-static {v2}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$500(Lcom/sec/android/app/shealth/SplashScreenActivity;)Landroid/widget/RelativeLayout;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 654
    iget-object v2, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$7;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    # getter for: Lcom/sec/android/app/shealth/SplashScreenActivity;->mBottomLayout:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/sec/android/app/shealth/SplashScreenActivity;->access$600(Lcom/sec/android/app/shealth/SplashScreenActivity;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 656
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$7;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 657
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$7;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/SplashScreenActivity;->startActivity(Landroid/content/Intent;)V

    .line 658
    iget-object v2, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$7;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/SplashScreenActivity;->finish()V

    .line 659
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 646
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 642
    return-void
.end method
