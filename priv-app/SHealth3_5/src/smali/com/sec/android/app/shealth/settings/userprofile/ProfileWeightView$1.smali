.class Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;
.super Ljava/lang/Object;
.source "ProfileWeightView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->initLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "adpater":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const v5, 0x1fbd2

    const v4, 0x1fbd1

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 118
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->getValue()F

    move-result v0

    .line 119
    .local v0, "value":F
    if-nez p3, :cond_2

    .line 120
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightUnit:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$200(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)I

    move-result v1

    if-ne v1, v5, :cond_0

    .line 122
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mPrevLbValue:F
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$300(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)F

    move-result v1

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_1

    .line 123
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mIsWeightValueChanged:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$402(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;Z)Z

    .line 124
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertLbToKg(F)F

    move-result v2

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightValue:F
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$502(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;F)F

    .line 125
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightValue:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$500(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)F

    move-result v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->floorToDigit(FI)F

    move-result v2

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mPrevKgValue:F
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$602(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;F)F

    .line 126
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mPrevLbValue:F
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$302(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;F)F

    .line 133
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightUnit:I
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$202(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;I)I

    .line 134
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->isChangeUnit:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$702(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;Z)Z

    .line 156
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightValue:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$500(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setValue(F)V

    .line 157
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$800(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->selectAll()V

    .line 158
    :cond_0
    return-void

    .line 130
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mIsWeightValueChanged:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$402(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;Z)Z

    goto :goto_0

    .line 139
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightUnit:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$200(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)I

    move-result v1

    if-ne v1, v4, :cond_0

    .line 140
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mPrevKgValue:F
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$600(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)F

    move-result v1

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_3

    .line 141
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mIsWeightValueChanged:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$402(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;Z)Z

    .line 142
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->getValue()F

    move-result v2

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightValue:F
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$502(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;F)F

    .line 143
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightValue:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$500(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)F

    move-result v2

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mPrevKgValue:F
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$602(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;F)F

    .line 149
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightUnit:I
    invoke-static {v1, v5}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$202(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;I)I

    .line 150
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->isChangeUnit:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$702(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;Z)Z

    goto :goto_1

    .line 147
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mIsWeightValueChanged:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$402(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;Z)Z

    goto :goto_2
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 162
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
