.class Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9$2;
.super Ljava/lang/Object;
.source "AMapExerciseProStatusFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;->onChannelStateChanged(ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;

.field final synthetic val$state:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;I)V
    .locals 0

    .prologue
    .line 1389
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9$2;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;

    iput p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9$2;->val$state:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1392
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9$2;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->setDefaultBackground()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->access$2400(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V

    .line 1393
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9$2;->val$state:I

    if-nez v0, :cond_1

    .line 1394
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9$2;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateDataText()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->access$2500(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V

    .line 1395
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9$2;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    const-wide/16 v1, 0x0

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->lapClockDispUpdate(J)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->access$2600(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;J)V

    .line 1396
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9$2;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->enableGoal(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->access$2700(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;Z)V

    .line 1397
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9$2;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateIndoorlayout()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->access$2800(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V

    .line 1406
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9$2;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9$2;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->reCreateActionbar(I)V

    .line 1407
    return-void

    .line 1399
    :cond_1
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9$2;->val$state:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1400
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9$2;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->enableGoal(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->access$2700(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;Z)V

    .line 1401
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9$2;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateModeState(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->access$2900(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;Z)V

    .line 1402
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9$2;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateIndoorlayout()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->access$2800(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V

    .line 1403
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9$2;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mListPopupIndoorOrOutdoor:Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->access$3000(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1404
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9$2;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mListPopupIndoorOrOutdoor:Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->access$3000(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->dismiss()V

    goto :goto_0
.end method
