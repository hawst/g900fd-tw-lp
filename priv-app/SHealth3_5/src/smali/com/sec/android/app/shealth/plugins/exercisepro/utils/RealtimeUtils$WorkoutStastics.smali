.class public Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;
.super Ljava/lang/Object;
.source "RealtimeUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "WorkoutStastics"
.end annotation


# instance fields
.field public avgHR:F

.field public avgSpeed:F

.field public declineAscent:D

.field public inclineAscent:D

.field public maxAltitude:D

.field public maxHR:F

.field public maxSpeed:F

.field public minAltitude:D


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->maxHR:F

    .line 73
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->avgHR:F

    .line 74
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->maxSpeed:F

    .line 75
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->avgSpeed:F

    .line 76
    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->minAltitude:D

    .line 77
    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->maxAltitude:D

    .line 78
    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->declineAscent:D

    .line 79
    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils$WorkoutStastics;->inclineAscent:D

    return-void
.end method
