.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;
.super Ljava/lang/Object;
.source "GradationObject.java"


# instance fields
.field private mIsBig:Z

.field private mIsMiddle:Z

.field private mOffsetMovement:I

.field private mValue:F


# direct methods
.method public constructor <init>(FI)V
    .locals 0
    .param p1, "value"    # F
    .param p2, "offsetMovement"    # I

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->mValue:F

    .line 75
    iput p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->mOffsetMovement:I

    .line 77
    return-void
.end method

.method public constructor <init>(FIZZ)V
    .locals 0
    .param p1, "value"    # F
    .param p2, "offsetMovement"    # I
    .param p3, "isBig"    # Z
    .param p4, "isMiddle"    # Z

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->mValue:F

    .line 47
    iput p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->mOffsetMovement:I

    .line 49
    iput-boolean p3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->mIsBig:Z

    .line 51
    iput-boolean p4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->mIsMiddle:Z

    .line 53
    return-void
.end method


# virtual methods
.method public getOffsetMovement()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->mOffsetMovement:I

    return v0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->mValue:F

    return v0
.end method

.method public isBig()Z
    .locals 1

    .prologue
    .line 131
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->mIsBig:Z

    return v0
.end method

.method public isMiddle()Z
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->mIsMiddle:Z

    return v0
.end method

.method public setOffsetMovement(I)V
    .locals 0
    .param p1, "offsetMovement"    # I

    .prologue
    .line 117
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->mOffsetMovement:I

    .line 119
    return-void
.end method
