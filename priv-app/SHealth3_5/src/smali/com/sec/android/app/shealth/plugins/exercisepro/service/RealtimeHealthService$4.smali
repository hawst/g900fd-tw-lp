.class Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$4;
.super Landroid/content/BroadcastReceiver;
.source "RealtimeHealthService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V
    .locals 0

    .prologue
    .line 349
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v9, 0x2718

    .line 352
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->ACTION_DEVICE_CONNECTED:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$800()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 353
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v6

    const-string v7, "ACTION_DEVICE_CONNECTED received"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->DEVICE_TYPE:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$900()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 355
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->DEVICE_TYPE:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$900()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 356
    .local v5, "mhealthDeviceType":I
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ACTION_DEVICE_CONNECTED received device type:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    if-ne v5, v9, :cond_6

    .line 358
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->DEVICE_ID:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$1000()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 359
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v6

    if-nez v6, :cond_1

    .line 360
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v6

    const-string v7, "ACTION_DEVICE_CONNECTED received but mHealthSensor is null"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    .end local v5    # "mhealthDeviceType":I
    :cond_0
    :goto_0
    return-void

    .line 362
    .restart local v5    # "mhealthDeviceType":I
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->DEVICE_ID:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$1000()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 363
    .local v4, "mhealthDeviceId":Ljava/lang/String;
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ACTION_DEVICE_CONNECTED received device id:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setHRMStatus(I)V

    .line 367
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v6

    const/4 v7, 0x5

    const/16 v8, 0x2718

    const/4 v9, 0x4

    const/4 v10, 0x0

    invoke-virtual {v6, v7, v8, v9, v10}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->isAvailable(IIILjava/lang/String;)Z

    move-result v6

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->ANTHRMisAvailable:Z
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$1202(Z)Z

    .line 368
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ANTHRMisAvailable : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->ANTHRMisAvailable:Z
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$1200()Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->ANTHRMisAvailable:Z
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$1200()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 370
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v6

    const/4 v7, 0x5

    const/16 v8, 0x2718

    const/4 v9, 0x4

    invoke-virtual {v6, v7, v8, v9}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->getConnectedDeviceList(III)Ljava/util/List;

    move-result-object v1

    .line 371
    .local v1, "devicelist":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    if-eqz v1, :cond_3

    .line 372
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 373
    .local v0, "device":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 374
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ACTION_DEVICE_CONNECTED received device matched:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v6, v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->handler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->handler:Landroid/os/Handler;

    const/16 v8, 0xfa4

    invoke-virtual {v7, v8, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 389
    .end local v0    # "device":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .end local v1    # "devicelist":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 390
    .local v2, "e":Ljava/lang/Exception;
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v6

    const-string v7, "HEART_RATE_MONITOR : getConnectedDeviceList - Exception"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 380
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_3
    :try_start_1
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v6

    const/4 v7, 0x3

    const/16 v8, 0x2718

    const/4 v9, 0x4

    invoke-virtual {v6, v7, v8, v9}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->getConnectedDeviceList(III)Ljava/util/List;

    move-result-object v1

    .line 381
    .restart local v1    # "devicelist":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    if-eqz v1, :cond_0

    .line 382
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_4
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 383
    .restart local v0    # "device":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 384
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ACTION_DEVICE_CONNECTED received device matched:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 385
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v6, v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->handler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->handler:Landroid/os/Handler;

    const/16 v8, 0xfa4

    invoke-virtual {v7, v8, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 394
    .end local v0    # "device":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .end local v1    # "devicelist":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "mhealthDeviceId":Ljava/lang/String;
    :cond_5
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v6

    const-string v7, "ACTION_DEVICE_CONNECTED received but no device id"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 397
    :cond_6
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v6

    const-string v7, "ACTION_DEVICE_CONNECTED received but not HRM"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 400
    .end local v5    # "mhealthDeviceType":I
    :cond_7
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v6

    const-string v7, "ACTION_DEVICE_CONNECTED received but no device type"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
