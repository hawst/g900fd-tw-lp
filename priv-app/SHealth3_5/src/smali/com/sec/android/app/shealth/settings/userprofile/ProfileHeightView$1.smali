.class Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$1;
.super Ljava/lang/Object;
.source "ProfileHeightView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->initLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "adapter":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const v4, 0x249f2

    const v3, 0x249f1

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->setHeightValue()V

    .line 143
    if-nez p3, :cond_1

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightUnit:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$200(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)I

    move-result v0

    if-eq v0, v3, :cond_0

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->isChangeUnit:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$302(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;Z)Z

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightUnit:I
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$202(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;I)I

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$400(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v0

    const/16 v1, 0x2002

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setInputType(I)V

    .line 160
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->heightValueConvert()V

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightValue:F
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$700(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->setValue(F)V

    .line 164
    return-void

    .line 151
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightUnit:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$200(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)I

    move-result v0

    if-eq v0, v4, :cond_2

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->isChangeUnit:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$302(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;Z)Z

    .line 154
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightUnit:I
    invoke-static {v0, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$202(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;I)I

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$500(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setInputType(I)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$1;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$600(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setInputType(I)V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 168
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
