.class public Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
.source "CategoryFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;,
        Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnSubCategoryItemClickListener;,
        Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$FoodSubCategoryResultTaskWithDialog;,
        Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnCategoryItemClickListener;,
        Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;,
        Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;,
        Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$LoadCategoriesTask;,
        Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$TaskWithDialog;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private currentStage:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;

.field private isFragmentRunning:Z

.field private mCategoryListHeader:Landroid/widget/HorizontalScrollView;

.field private mDisplayedListsMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;",
            "Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;",
            ">;"
        }
    .end annotation
.end field

.field private mHeaderLayout:Landroid/widget/LinearLayout;

.field private mInputConnection:Landroid/view/inputmethod/BaseInputConnection;

.field private mListItemsContainer:Landroid/widget/FrameLayout;

.field private mOnCategoryClick:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;",
            ">;"
        }
    .end annotation
.end field

.field private mOnFoodClickListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnFoodInfoSelectedListener;

.field private mOnScrollToEndListener:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;

.field private mOnSubCategoryClick:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;",
            ">;"
        }
    .end annotation
.end field

.field private mSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

.field private mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;

.field private mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const-class v0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 90
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 80
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;-><init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mOnScrollToEndListener:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;

    .line 81
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;->ROOT_CATEGORIES:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->currentStage:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;

    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->isFragmentRunning:Z

    .line 352
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnCategoryItemClickListener;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnCategoryItemClickListener;-><init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mOnCategoryClick:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;

    .line 355
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnSubCategoryItemClickListener;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnSubCategoryItemClickListener;-><init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mOnSubCategoryClick:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;

    .line 91
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->setRetainInstance(Z)V

    .line 92
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mDisplayedListsMap:Ljava/util/Map;

    .line 93
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->currentStage:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;Ljava/util/List;)Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->createCategoryAdapter(Ljava/util/List;)Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->isFragmentRunning:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mOnCategoryClick:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$OnScrollToEndListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;
    .param p2, "x2"    # Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;
    .param p5, "x5"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$OnScrollToEndListener;

    .prologue
    .line 72
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->setDisplayedItems(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$OnScrollToEndListener;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->refreshFragmentFocusables()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mDisplayedListsMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mOnSubCategoryClick:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnFoodInfoSelectedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mOnFoodClickListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnFoodInfoSelectedListener;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;Landroid/content/Context;Ljava/util/List;Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;)Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/util/List;
    .param p3, "x3"    # Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;
    .param p4, "x4"    # Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;

    .prologue
    .line 72
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->createAdapterForFoodItems(Landroid/content/Context;Ljava/util/List;Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;)Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mOnScrollToEndListener:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->logStagesAndThrowNpe()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Landroid/widget/HorizontalScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mCategoryListHeader:Landroid/widget/HorizontalScrollView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->performBackPress()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mHeaderLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    return-object v0
.end method

.method private addCategoryTitle(Ljava/lang/String;)V
    .locals 8
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    const v7, 0x7f0803a1

    const/16 v6, 0x8

    .line 211
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    invoke-interface {v4, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;->editSearchPhrase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 212
    .local v0, "editedSearchPhrase":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 213
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f0300c5

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 214
    .local v3, "view":Landroid/view/View;
    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 215
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mCategoryListHeader:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v4}, Landroid/widget/HorizontalScrollView;->getVisibility()I

    move-result v4

    if-ne v4, v6, :cond_0

    .line 216
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mCategoryListHeader:Landroid/widget/HorizontalScrollView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    .line 217
    const v4, 0x7f0803a0

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 224
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mHeaderLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 226
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mHeaderLayout:Landroid/widget/LinearLayout;

    new-instance v5, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$2;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$2;-><init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)V

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->post(Ljava/lang/Runnable;)Z

    .line 238
    new-instance v4, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$3;-><init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 249
    return-void

    .line 219
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->changeTitleColor()V

    .line 220
    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 221
    .local v2, "titleText":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0701eb

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method private changeTitleColor()V
    .locals 5

    .prologue
    .line 252
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mHeaderLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 253
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mHeaderLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 254
    .local v0, "childView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 256
    const v2, 0x7f0803a1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0702a4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 252
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 260
    .end local v0    # "childView":Landroid/view/View;
    :cond_1
    return-void
.end method

.method private createAdapterForFoodItems(Landroid/content/Context;Ljava/util/List;Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;)Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "taskRunner"    # Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;",
            ">;",
            "Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;",
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;",
            ">;)",
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 370
    .local p2, "foodItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;>;"
    .local p4, "onItemClicked":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener<Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;>;"
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;

    invoke-direct {v0, p1, p2}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 371
    .local v0, "searchResultAdapter":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter<Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;>;"
    new-instance v2, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;

    invoke-interface {v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;->getUserActionLogger()Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v1

    invoke-direct {v2, p1, p3, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;-><init>(Landroid/content/Context;Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->addOnFavoriteClickListener(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$OnFavoriteClickListener;)V

    .line 374
    invoke-virtual {v0, p4}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->setOnItemClickListener(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;)V

    .line 375
    return-object v0
.end method

.method private createCategoryAdapter(Ljava/util/List;)Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lcom/sec/android/app/shealth/food/foodpick/search/TextOnlyData;",
            ">(",
            "Ljava/util/List",
            "<TE;>;)",
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 345
    .local p1, "textOnlyItems":Ljava/util/List;, "Ljava/util/List<TE;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 346
    .local v1, "listItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/food/foodpick/search/TextOnlyData;

    .line 347
    .local v2, "textOnlyListItem":Lcom/sec/android/app/shealth/food/foodpick/search/TextOnlyData;, "TE;"
    new-instance v3, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;

    invoke-direct {v3, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/TextOnlyData;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 349
    .end local v2    # "textOnlyListItem":Lcom/sec/android/app/shealth/food/foodpick/search/TextOnlyData;, "TE;"
    :cond_0
    new-instance v3, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    return-object v3
.end method

.method private findViews(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 201
    const v0, 0x7f0803a1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mHeaderLayout:Landroid/widget/LinearLayout;

    .line 202
    const v0, 0x7f08044e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/HorizontalScrollView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mCategoryListHeader:Landroid/widget/HorizontalScrollView;

    .line 203
    const v0, 0x7f08044f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mListItemsContainer:Landroid/widget/FrameLayout;

    .line 204
    return-void
.end method

.method private init()V
    .locals 1

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchFactory;->getSearchApi(Landroid/content/Context;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    .line 171
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunnerAccessor;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunnerAccessor;->getTaskRunner()Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;

    .line 172
    return-void
.end method

.method private initCategoriesView()V
    .locals 3

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$LoadCategoriesTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$LoadCategoriesTask;-><init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$1;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;->addTask(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    .line 208
    return-void
.end method

.method private initHierarchyListeners()V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mListItemsContainer:Landroid/widget/FrameLayout;

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$1;-><init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    .line 135
    return-void
.end method

.method private logStagesAndThrowNpe()V
    .locals 9

    .prologue
    .line 139
    sget-object v6, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->LOG_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Current stage: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->currentStage:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 142
    .local v0, "builder":Ljava/lang/StringBuilder;
    iget-object v6, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mDisplayedListsMap:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 143
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;>;"
    const-string v6, "Key = "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 144
    const-string v6, "   value.hash = "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;

    invoke-virtual {v6}, Ljava/lang/Object;->hashCode()I

    move-result v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "    "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 146
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;>;"
    :cond_0
    sget-object v6, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->LOG_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DisplayedListMap: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    new-instance v0, Ljava/lang/StringBuilder;

    .end local v0    # "builder":Ljava/lang/StringBuilder;
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 149
    .restart local v0    # "builder":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mHeaderLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v6

    if-ge v3, v6, :cond_2

    .line 150
    iget-object v6, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mHeaderLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 151
    .local v1, "child":Landroid/view/View;
    instance-of v6, v1, Landroid/widget/TextView;

    if-eqz v6, :cond_1

    .line 152
    check-cast v1, Landroid/widget/TextView;

    .end local v1    # "child":Landroid/view/View;
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .line 153
    .local v5, "text":Ljava/lang/String;
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "   -> "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    .end local v5    # "text":Ljava/lang/String;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 156
    :cond_2
    sget-object v6, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->LOG_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Header: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    return-void
.end method

.method private performBackPress()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mInputConnection:Landroid/view/inputmethod/BaseInputConnection;

    new-instance v1, Landroid/view/KeyEvent;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/inputmethod/BaseInputConnection;->sendKeyEvent(Landroid/view/KeyEvent;)Z

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mInputConnection:Landroid/view/inputmethod/BaseInputConnection;

    new-instance v1, Landroid/view/KeyEvent;

    const/4 v2, 0x1

    invoke-direct {v1, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/inputmethod/BaseInputConnection;->sendKeyEvent(Landroid/view/KeyEvent;)Z

    .line 264
    return-void
.end method

.method private setDisplayedItems(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$OnScrollToEndListener;)V
    .locals 6
    .param p3, "categoriesTitle"    # Ljava/lang/String;
    .param p4, "stage"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;
    .param p5, "onScrollToEndListener"    # Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$OnScrollToEndListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/IListItem;",
            ">(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter",
            "<TT;>;",
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener",
            "<TT;>;",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;",
            "Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$OnScrollToEndListener;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "itemsAdapter":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter<TT;>;"
    .local p2, "onItemClick":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener<TT;>;"
    const/4 v4, -0x1

    .line 318
    iput-object p4, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->currentStage:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;

    .line 319
    invoke-direct {p0, p3}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->addCategoryTitle(Ljava/lang/String;)V

    .line 320
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;-><init>(Landroid/content/Context;)V

    .line 321
    .local v0, "categoryContentView":Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;->setFoodSearchApi(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;)V

    .line 322
    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 323
    invoke-virtual {v0, p5}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;->setOnScrollToEndListener(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$OnScrollToEndListener;)V

    .line 324
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mDisplayedListsMap:Ljava/util/Map;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->currentStage:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 326
    .local v1, "params":Landroid/view/ViewGroup$LayoutParams;
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mListItemsContainer:Landroid/widget/FrameLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 327
    if-eqz p2, :cond_0

    .line 328
    invoke-virtual {p1, p2}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;->setOnItemClickListener(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;)V

    .line 331
    :cond_0
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$4;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$4;-><init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)V

    const-wide/16 v4, 0x320

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 342
    return-void
.end method


# virtual methods
.method public backPressed()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 176
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->isResumed()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->currentStage:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;

    sget-object v5, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;->ROOT_CATEGORIES:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;

    if-eq v4, v5, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mHeaderLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    if-lez v4, :cond_1

    .line 178
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mDisplayedListsMap:Ljava/util/Map;

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->currentStage:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;

    invoke-interface {v4, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;

    .line 179
    .local v2, "viewToRemove":Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;
    invoke-static {}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;->values()[Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->currentStage:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;->ordinal()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    aget-object v4, v4, v5

    iput-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->currentStage:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;

    .line 180
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mHeaderLayout:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mHeaderLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    .line 181
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->changeTitleColor()V

    .line 182
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mHeaderLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    if-le v4, v3, :cond_0

    .line 183
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mHeaderLayout:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mHeaderLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 184
    .local v0, "childView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 185
    const v4, 0x7f0803a1

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 186
    .local v1, "titleText":Landroid/widget/TextView;
    if-eqz v1, :cond_0

    .line 187
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0701eb

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 194
    .end local v0    # "childView":Landroid/view/View;
    .end local v1    # "titleText":Landroid/widget/TextView;
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mListItemsContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v4, v2}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 197
    .end local v2    # "viewToRemove":Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 163
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onAttach(Landroid/app/Activity;)V

    .line 164
    instance-of v0, p1, Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnFoodInfoSelectedListener;

    if-eqz v0, :cond_0

    .line 165
    check-cast p1, Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnFoodInfoSelectedListener;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mOnFoodClickListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnFoodInfoSelectedListener;

    .line 167
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    .line 97
    const v1, 0x7f030102

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 98
    .local v0, "view":Landroid/view/View;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->init()V

    .line 99
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->findViews(Landroid/view/View;)V

    .line 100
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->initCategoriesView()V

    .line 101
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->initHierarchyListeners()V

    .line 102
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;

    invoke-interface {v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;->getUserActionLogger()Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    .line 103
    new-instance v1, Landroid/view/inputmethod/BaseInputConnection;

    invoke-direct {v1, v0, v3}, Landroid/view/inputmethod/BaseInputConnection;-><init>(Landroid/view/View;Z)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mInputConnection:Landroid/view/inputmethod/BaseInputConnection;

    .line 104
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->isFragmentRunning:Z

    .line 105
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 429
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->isFragmentRunning:Z

    .line 430
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onDestroy()V

    .line 431
    return-void
.end method
