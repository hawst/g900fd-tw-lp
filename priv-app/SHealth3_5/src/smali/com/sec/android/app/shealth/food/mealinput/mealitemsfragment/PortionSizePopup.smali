.class public Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;
.super Ljava/lang/Object;
.source "PortionSizePopup.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IContentInitializatorGetter;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$7;,
        Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$PortionSizePopupResultListener;
    }
.end annotation


# static fields
.field private static final FORMAT_FOR_FLOAT:Ljava/lang/String; = "%.1f"

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final PORTION_SIZE_INPUT_MODULE_MOVE_DISTANCE:F = 0.1f

.field public static final PORTION_SIZE_POPUP_TAG:Ljava/lang/String; = "PORTION_SIZE_POPUP_TAG"

.field private static final sDefaultAmountForEasyUnit:F = 1.0f

.field private static final sDefaultAmountForGramUnit:F = 100.0f

.field private static final sInputModuleValue:Ljava/lang/String; = "sInputModuleValue"

.field private static final sPortionSizeHolder:Ljava/lang/String; = "sPortionSizeHolder"


# instance fields
.field private mActivity:Landroid/app/Activity;

.field mEmptyFieldListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

.field private mFragmentManager:Landroid/support/v4/app/FragmentManager;

.field private mGramUnit:Ljava/lang/String;

.field private mInputModuleValue:F

.field private mKcalUnit:Ljava/lang/String;

.field private mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

.field private mOutOfRangeToast:Landroid/widget/Toast;

.field private mOzUnit:Ljava/lang/String;

.field private mPortionSizeHolder:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

.field private mPortionSizeInputModule:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;

.field private mPortionSizePopupResultListener:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$PortionSizePopupResultListener;

.field private mTextViewCarbProtFatInPrecisePopup:Landroid/widget/TextView;

.field private mTextViewTotalKcalInPrecisePopup:Landroid/widget/TextView;

.field private mUnitDropDownButton:Landroid/widget/TextView;

.field private mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    const-class v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 574
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$6;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mEmptyFieldListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    .line 99
    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;->getUserActionLogger()Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    .line 100
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mActivity:Landroid/app/Activity;

    .line 101
    const v0, 0x7f0900bf

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mGramUnit:Ljava/lang/String;

    .line 102
    const v0, 0x7f0900c5

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mOzUnit:Ljava/lang/String;

    .line 103
    const v0, 0x7f0900b9

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mKcalUnit:Ljava/lang/String;

    .line 104
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Landroid/support/v4/app/FragmentManager;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "fragmentManager"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 574
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$6;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mEmptyFieldListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    .line 107
    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;->getUserActionLogger()Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    .line 108
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    .line 109
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mActivity:Landroid/app/Activity;

    .line 110
    const v0, 0x7f0900bf

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mGramUnit:Ljava/lang/String;

    .line 111
    const v0, 0x7f0900c5

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mOzUnit:Ljava/lang/String;

    .line 112
    const v0, 0x7f0900b9

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mKcalUnit:Ljava/lang/String;

    .line 113
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeInputModule:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;Landroid/view/View;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->initPortionSizeInputModule(Landroid/view/View;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;Landroid/widget/TextView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->calculateAndSetTotalKcalTexView(Landroid/widget/TextView;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->updateDefaultValueOfUnit(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mOutOfRangeToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mOutOfRangeToast:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Landroid/support/v4/app/FragmentManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mUnitDropDownButton:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mUnitDropDownButton:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->initDropDownButtonInPrecisePopup()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$PortionSizePopupResultListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizePopupResultListener:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$PortionSizePopupResultListener;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeHolder:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;
    .param p1, "x1"    # F

    .prologue
    .line 64
    iput p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mInputModuleValue:F

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mTextViewTotalKcalInPrecisePopup:Landroid/widget/TextView;

    return-object v0
.end method

.method private calculateAndSetTotalKcalTexView(Landroid/widget/TextView;)V
    .locals 7
    .param p1, "textView"    # Landroid/widget/TextView;

    .prologue
    .line 364
    iget v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mInputModuleValue:F

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeHolder:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->calculateKcal(FLcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;)F

    move-result v0

    .line 365
    .local v0, "kCalValue":F
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mTextViewCarbProtFatInPrecisePopup:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeHolder:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->getKcal()F

    move-result v2

    div-float v2, v0, v2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->setCarbProTFatTexView(Landroid/widget/TextView;F)V

    .line 366
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeHolder:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    iget v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mInputModuleValue:F

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->setQuantity(F)V

    .line 367
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mActivity:Landroid/app/Activity;

    const v3, 0x7f09008a

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->UK:Ljava/util/Locale;

    const-string v3, "%.1f"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mActivity:Landroid/app/Activity;

    const v3, 0x7f0900b9

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 369
    return-void
.end method

.method private calculateCarbProTFat(FLcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;)Ljava/lang/String;
    .locals 13
    .param p1, "quantity"    # F
    .param p2, "portionHolder"    # Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    .prologue
    const/4 v6, 0x0

    const/4 v12, 0x0

    .line 537
    iget-object v10, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mActivity:Landroid/app/Activity;

    const v11, 0x7f0900bf

    invoke-virtual {v10, v11}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 538
    .local v9, "unit":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mActivity:Landroid/app/Activity;

    const v11, 0x7f0909ca

    invoke-virtual {v10, v11}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 539
    .local v0, "carbohydrate":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mActivity:Landroid/app/Activity;

    const v11, 0x7f090966

    invoke-virtual {v10, v11}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 540
    .local v3, "protein":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mActivity:Landroid/app/Activity;

    const v11, 0x7f0909cb

    invoke-virtual {v10, v11}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 541
    .local v2, "fat":Ljava/lang/String;
    const-string v4, " "

    .line 542
    .local v4, "space":Ljava/lang/String;
    const-string v1, ","

    .line 544
    .local v1, "comma":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->getCarbohydrate()F

    move-result v10

    mul-float v5, p1, v10

    .line 545
    .local v5, "totalCarb":F
    invoke-virtual {p2}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->getProtein()F

    move-result v10

    mul-float v8, p1, v10

    .line 546
    .local v8, "totalProt":F
    invoke-virtual {p2}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->getFat()F

    move-result v10

    mul-float v7, p1, v10

    .line 547
    .local v7, "totalFat":F
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isSupportsBoohee()Z

    move-result v10

    if-eqz v10, :cond_1

    const/high16 v10, -0x40800000    # -1.0f

    cmpl-float v10, v5, v10

    if-nez v10, :cond_1

    .line 559
    :cond_0
    :goto_0
    return-object v6

    .line 551
    :cond_1
    cmpl-float v10, v5, v12

    if-gtz v10, :cond_2

    cmpl-float v10, v8, v12

    if-gtz v10, :cond_2

    cmpl-float v10, v7, v12

    if-lez v10, :cond_0

    .line 557
    :cond_2
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v7}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertToSingleDecimal(F)F

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v5}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertToSingleDecimal(F)F

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v8}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertToSingleDecimal(F)F

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 559
    .local v6, "totalCarbProTFat":Ljava/lang/String;
    goto :goto_0
.end method

.method private calculateKcal(FLcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;)F
    .locals 4
    .param p1, "quantity"    # F
    .param p2, "portionHolder"    # Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    .prologue
    .line 506
    invoke-virtual {p2}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->getUnit()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 525
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unit should be one of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-class v3, Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$MealType;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 508
    :pswitch_0
    invoke-virtual {p2}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->getKcal()F

    move-result v1

    mul-float v0, v1, p1

    .line 529
    .local v0, "totalCalories":F
    :goto_0
    return v0

    .line 512
    .end local v0    # "totalCalories":F
    :pswitch_1
    invoke-virtual {p2}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->getGramInKcal()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    .line 513
    const/4 v0, 0x0

    .restart local v0    # "totalCalories":F
    goto :goto_0

    .line 515
    .end local v0    # "totalCalories":F
    :cond_0
    invoke-virtual {p2}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->getGramInKcal()F

    move-result v1

    div-float v0, p1, v1

    .line 517
    .restart local v0    # "totalCalories":F
    goto :goto_0

    .line 519
    .end local v0    # "totalCalories":F
    :pswitch_2
    invoke-virtual {p2}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->getOzInKcal()F

    move-result v1

    div-float v0, p1, v1

    .line 520
    .restart local v0    # "totalCalories":F
    goto :goto_0

    .line 522
    .end local v0    # "totalCalories":F
    :pswitch_3
    move v0, p1

    .line 523
    .restart local v0    # "totalCalories":F
    goto :goto_0

    .line 506
    nop

    :pswitch_data_0
    .packed-switch 0x1d4c1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getUnitName(Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;)Ljava/lang/String;
    .locals 2
    .param p1, "portionSizeHolder"    # Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    .prologue
    .line 489
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->getUnit()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 499
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unit in portion size holder should be one of  ShealthContract.Constants.MealItemUnit"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 491
    :pswitch_0
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->getUnitName()Ljava/lang/String;

    move-result-object v0

    .line 497
    :goto_0
    return-object v0

    .line 493
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mGramUnit:Ljava/lang/String;

    goto :goto_0

    .line 495
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mOzUnit:Ljava/lang/String;

    goto :goto_0

    .line 497
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mKcalUnit:Ljava/lang/String;

    goto :goto_0

    .line 489
    nop

    :pswitch_data_0
    .packed-switch 0x1d4c1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private initDropDownButtonInPrecisePopup()V
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 381
    new-instance v1, Ljava/util/ArrayList;

    const/4 v3, 0x4

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 382
    .local v1, "units":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 383
    .local v2, "unitsMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeHolder:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->getOzInKcal()F

    move-result v3

    cmpl-float v3, v3, v6

    if-lez v3, :cond_0

    .line 384
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mOzUnit:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 386
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeHolder:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->getGramInKcal()F

    move-result v3

    cmpl-float v3, v3, v6

    if-lez v3, :cond_1

    .line 387
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mGramUnit:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 389
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeHolder:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->getUnitName()Ljava/lang/String;

    move-result-object v0

    .line 390
    .local v0, "defaultUnit":Ljava/lang/String;
    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 391
    invoke-interface {v1, v5, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 393
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeHolder:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->getKcal()F

    move-result v3

    cmpl-float v3, v3, v6

    if-eqz v3, :cond_3

    .line 394
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mKcalUnit:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 397
    :cond_3
    const v3, 0x1d4c1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 398
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mOzUnit:Ljava/lang/String;

    const v6, 0x1d4c3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v2, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 399
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mGramUnit:Ljava/lang/String;

    const v6, 0x1d4c2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v2, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 400
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mKcalUnit:Ljava/lang/String;

    const v6, 0x1d4c4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v2, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 402
    new-instance v6, Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mActivity:Landroid/app/Activity;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mUnitDropDownButton:Landroid/widget/TextView;

    invoke-direct {v6, v7, v3, v8}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;)V

    iput-object v6, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    .line 403
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    iget-object v6, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeHolder:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->getUnitName(Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v6

    invoke-virtual {v3, v6}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setCurrentItem(I)V

    .line 405
    iget-object v6, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mUnitDropDownButton:Landroid/widget/TextView;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v4, :cond_4

    move v3, v4

    :goto_0
    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 409
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mUnitDropDownButton:Landroid/widget/TextView;

    new-instance v4, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;

    invoke-direct {v4, p0, v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$5;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;Ljava/util/Map;)V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 444
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mUnitDropDownButton:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeHolder:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->getUnitName(Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 445
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mUnitDropDownButton:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mActivity:Landroid/app/Activity;

    iget-object v6, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mUnitDropDownButton:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0901ec

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 447
    return-void

    :cond_4
    move v3, v5

    .line 405
    goto :goto_0
.end method

.method private initPortionSizeInputModule(Landroid/view/View;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 3
    .param p1, "content"    # Landroid/view/View;
    .param p2, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 306
    const v0, 0x7f08047a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeInputModule:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;

    .line 309
    const v0, 0x7f0803a7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mTextViewTotalKcalInPrecisePopup:Landroid/widget/TextView;

    .line 310
    const v0, 0x7f08047c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mTextViewCarbProtFatInPrecisePopup:Landroid/widget/TextView;

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeInputModule:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mTextViewTotalKcalInPrecisePopup:Landroid/widget/TextView;

    if-nez v0, :cond_1

    .line 314
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FATAL NullPointerException "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cannot correct initialize yourself"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    :goto_0
    return-void

    .line 320
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeInputModule:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mEmptyFieldListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;->setTextEmptyListener(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;)V

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeInputModule:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;

    new-instance v1, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$4;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;->setOnFloatInputChangedListener(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnFloatInputChangedListener;)V

    .line 342
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mTextViewCarbProtFatInPrecisePopup:Landroid/widget/TextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->setCarbProTFatTexView(Landroid/widget/TextView;F)V

    .line 344
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeInputModule:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;

    const v1, 0x3dcccccd    # 0.1f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;->setMoveDistance(F)V

    .line 348
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeHolder:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->getUnitName(Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->updateRangeOfInputModule(Ljava/lang/String;)V

    .line 349
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeInputModule:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeHolder:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->getQuantity()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;->setValue(F)V

    .line 350
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeInputModule:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeHolder:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->getUnitName(Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;->setUnit(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private setCarbProTFatTexView(Landroid/widget/TextView;F)V
    .locals 3
    .param p1, "textView"    # Landroid/widget/TextView;
    .param p2, "quantity"    # F

    .prologue
    .line 372
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeHolder:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    invoke-direct {p0, p2, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->calculateCarbProTFat(FLcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;)Ljava/lang/String;

    move-result-object v0

    .line 373
    .local v0, "kCarbProtFatValue":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 374
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mTextViewCarbProtFatInPrecisePopup:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 378
    :goto_0
    return-void

    .line 376
    :cond_0
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateDefaultValueOfUnit(Ljava/lang/String;)V
    .locals 2
    .param p1, "unitName"    # Ljava/lang/String;

    .prologue
    .line 453
    const/high16 v0, 0x3f800000    # 1.0f

    .line 455
    .local v0, "newValue":F
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->updateRangeOfInputModule(Ljava/lang/String;)V

    .line 456
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mGramUnit:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 457
    const/high16 v0, 0x42c80000    # 100.0f

    .line 461
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeInputModule:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;->setValue(F)V

    .line 463
    return-void

    .line 458
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mKcalUnit:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 459
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeHolder:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->getKcal()F

    move-result v0

    goto :goto_0
.end method

.method private updateRangeOfInputModule(Ljava/lang/String;)V
    .locals 3
    .param p1, "unitName"    # Ljava/lang/String;

    .prologue
    .line 467
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mOutOfRangeToast:Landroid/widget/Toast;

    .line 484
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeInputModule:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;

    const v1, 0x3dcccccd    # 0.1f

    const v2, 0x461c3c00    # 9999.0f

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;->setInputRange(FF)V

    .line 486
    return-void
.end method


# virtual methods
.method public getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 170
    const-string v0, "PORTION_SIZE_POPUP_TAG"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$2;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)V

    .line 251
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 256
    const-string v0, "PORTION_SIZE_POPUP_TAG"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$3;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)V

    .line 302
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->onDialogNotSupported(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    move-result-object v0

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 153
    const-string/jumbo v0, "sInputModuleValue"

    iget v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mInputModuleValue:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mInputModuleValue:F

    .line 154
    const-string/jumbo v0, "sPortionSizeHolder"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    const-string/jumbo v0, "sPortionSizeHolder"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeHolder:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    .line 157
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 143
    const-string/jumbo v0, "sPortionSizeHolder"

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeHolder:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 144
    const-string/jumbo v0, "sInputModuleValue"

    iget v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mInputModuleValue:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 145
    return-void
.end method

.method public setPortionSizeData(Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;)V
    .locals 0
    .param p1, "portionSizeHolder"    # Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    .prologue
    .line 165
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeHolder:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    .line 166
    return-void
.end method

.method public setPortionSizePopupResultListener(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$PortionSizePopupResultListener;)V
    .locals 0
    .param p1, "mPortionSizePopupResultListener"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$PortionSizePopupResultListener;

    .prologue
    .line 360
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizePopupResultListener:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$PortionSizePopupResultListener;

    .line 361
    return-void
.end method

.method public showPortionSizePopup(Landroid/support/v4/app/FragmentManager;)V
    .locals 3
    .param p1, "fragmentManager"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    .line 122
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mActivity:Landroid/app/Activity;

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f090938

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090044

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f030110

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$1;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->doNotDismissOnOkClick()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    const-string v1, "PORTION_SIZE_POPUP_TAG"

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SET_UNIT_POPUP_PRECISE:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 135
    return-void
.end method
