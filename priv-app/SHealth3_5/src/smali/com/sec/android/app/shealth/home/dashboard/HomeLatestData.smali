.class public Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;
.super Ljava/lang/Object;
.source "HomeLatestData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$HomeLatestDataRefreshInterface;,
        Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$WFLDataReceivedListener;
    }
.end annotation


# static fields
.field public static final ANIMATION_COUNT:I = 0x258

.field private static final DEFAULT_VALUE:I = -0x1

.field private static final TAG:Ljava/lang/String; = "HomeLatestData"


# instance fields
.field private final BURNT_CALORIES:I

.field private final EXERCISE_MEASURE:I

.field private final FOOD_MEASURE:I

.field private final HEART_RATE_MEASURE:I

.field private final PEDOMETER_MEASURE:I

.field private final SLEEP_MEASURE:I

.field private final SPO2_MEASURE:I

.field private final STRESS_MEASURE:I

.field private final THERMOHYGROMETER_MEASURE:I

.field private final UV_MEASURE:I

.field private arrAddTime:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private blankTextView:Landroid/widget/TextView;

.field private callbackHandler:Landroid/os/Handler;

.field private callbackRunnable:Ljava/lang/Runnable;

.field private calories:[I

.field private changeDataItemNumber:I

.field healthyCallBack:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;

.field intentAction:Landroid/view/View$OnClickListener;

.field private listContainer:Landroid/view/ViewGroup;

.field private mActivity:Landroid/app/Activity;

.field private mBurntCalories:F

.field private mContext:Landroid/content/Context;

.field private mDayStepCounterConnection:Landroid/content/ServiceConnection;

.field private mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

.field private mExerciseTime:J

.field private mHandler:Landroid/os/Handler;

.field private mHomeLatestInterface:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$HomeLatestDataRefreshInterface;

.field private mPedoCalories:F

.field private mPedoGoalValue:I

.field private mPedoTime:J

.field private mScrollView:Landroid/widget/ScrollView;

.field private mUpdateViewRunnable:Ljava/lang/Runnable;

.field private mView:Landroid/view/View;

.field private pedoData:Lcom/sec/android/app/shealth/home/data/HomePedoData;

.field private readyForSync:Z

.field private sendUpdateViewFromSync:Ljava/lang/Boolean;

.field private workingOnClickListener:Z

.field private wwflListener:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$WFLDataReceivedListener;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;)V
    .locals 6
    .param p1, "mContext"    # Landroid/app/Activity;
    .param p2, "mView"    # Landroid/view/View;

    .prologue
    const-wide/16 v4, -0x1

    const/4 v3, -0x1

    const/4 v0, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 414
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mContext:Landroid/content/Context;

    .line 102
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mActivity:Landroid/app/Activity;

    .line 103
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mView:Landroid/view/View;

    .line 104
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->listContainer:Landroid/view/ViewGroup;

    .line 105
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->blankTextView:Landroid/widget/TextView;

    .line 106
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mScrollView:Landroid/widget/ScrollView;

    .line 107
    iput v3, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->changeDataItemNumber:I

    .line 110
    iput v3, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoGoalValue:I

    .line 111
    iput-wide v4, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoTime:J

    .line 112
    iput-wide v4, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mExerciseTime:J

    .line 113
    iput v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoCalories:F

    .line 114
    iput v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mBurntCalories:F

    .line 115
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->calories:[I

    .line 117
    iput v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->PEDOMETER_MEASURE:I

    .line 118
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->EXERCISE_MEASURE:I

    .line 119
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->BURNT_CALORIES:I

    .line 120
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->HEART_RATE_MEASURE:I

    .line 121
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->STRESS_MEASURE:I

    .line 122
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->FOOD_MEASURE:I

    .line 123
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->SLEEP_MEASURE:I

    .line 124
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->UV_MEASURE:I

    .line 125
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->SPO2_MEASURE:I

    .line 126
    const/16 v0, 0x9

    iput v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->THERMOHYGROMETER_MEASURE:I

    .line 128
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->wwflListener:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$WFLDataReceivedListener;

    .line 129
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .line 131
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->readyForSync:Z

    .line 132
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->workingOnClickListener:Z

    .line 134
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->callbackHandler:Landroid/os/Handler;

    .line 137
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->arrAddTime:Ljava/util/ArrayList;

    .line 139
    new-instance v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$1;-><init>(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->healthyCallBack:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;

    .line 224
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->sendUpdateViewFromSync:Ljava/lang/Boolean;

    .line 225
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mHandler:Landroid/os/Handler;

    .line 229
    new-instance v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$2;-><init>(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mUpdateViewRunnable:Ljava/lang/Runnable;

    .line 323
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->callbackRunnable:Ljava/lang/Runnable;

    .line 384
    new-instance v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$4;-><init>(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mDayStepCounterConnection:Landroid/content/ServiceConnection;

    .line 1452
    new-instance v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$9;-><init>(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->intentAction:Landroid/view/View$OnClickListener;

    .line 415
    const-string v0, "HomeLatestData"

    const-string v1, "HomeLatestData create"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mActivity:Landroid/app/Activity;

    .line 417
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mContext:Landroid/content/Context;

    .line 418
    iput-object p2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mView:Landroid/view/View;

    .line 419
    const v0, 0x7f0804f6

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->blankTextView:Landroid/widget/TextView;

    .line 420
    const v0, 0x7f0804f3

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mScrollView:Landroid/widget/ScrollView;

    .line 421
    const-string v0, "HomeLatestData"

    const-string v1, "HomeLatestDataCreate updateView"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->arrAddTime:Ljava/util/ArrayList;

    .line 424
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->callbackHandler:Landroid/os/Handler;

    .line 425
    return-void
.end method

.method private SetPlugingAddViewArea(Ljava/util/ArrayList;Ljava/lang/Boolean;)V
    .locals 15
    .param p2, "isAnimation"    # Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .prologue
    .line 484
    .local p1, "lastDataArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v11, "HomeLatestData"

    const-string v12, "[PERF] SetPlugingAddViewArea - START"

    invoke-static {v11, v12}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 485
    const-string v11, "HomeLatestData"

    const-string v12, "HomeLatestData SetPlugingAddViewArea"

    invoke-static {v11, v12}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 487
    .local v2, "childViewList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;>;"
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v11

    new-array v6, v11, [Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;

    .line 488
    .local v6, "itemView":[Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v11

    new-array v1, v11, [J

    .line 490
    .local v1, "ViewData":[J
    const/4 v3, 0x0

    .local v3, "cnt":I
    :goto_0
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-ge v3, v11, :cond_0

    .line 491
    new-instance v11, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;

    iget-object v12, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mContext:Landroid/content/Context;

    invoke-direct {v11, v12}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;-><init>(Landroid/content/Context;)V

    aput-object v11, v6, v3

    .line 490
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 494
    :cond_0
    const/4 v3, 0x0

    :goto_1
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-ge v3, v11, :cond_2

    .line 495
    aget-object v11, v6, v3

    move-object/from16 v0, p2

    invoke-direct {p0, v3, v11, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setItemView(ILcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;Ljava/lang/Boolean;)J

    move-result-wide v11

    aput-wide v11, v1, v3

    .line 496
    aget-wide v11, v1, v3

    const-wide/16 v13, -0x1

    cmp-long v11, v11, v13

    if-lez v11, :cond_1

    .line 497
    aget-wide v11, v1, v3

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aget-object v12, v6, v3

    invoke-virtual {v2, v11, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 498
    aget-object v11, v6, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setTag(Ljava/lang/Object;)V

    .line 494
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 500
    :cond_1
    aget-object v11, v6, v3

    invoke-static {v11}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->recursiveRecycle(Landroid/view/View;)V

    .line 501
    aget-object v11, v6, v3

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->removeAllViews()V

    goto :goto_2

    .line 505
    :cond_2
    new-instance v10, Ljava/util/TreeMap;

    invoke-direct {v10, v2}, Ljava/util/TreeMap;-><init>(Ljava/util/Map;)V

    .line 506
    .local v10, "treeMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Long;Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;>;"
    invoke-virtual {v10}, Ljava/util/TreeMap;->descendingKeySet()Ljava/util/NavigableSet;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/NavigableSet;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 508
    .local v7, "iteratorKey":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Long;>;"
    const-string v8, " "

    .line 509
    .local v8, "tagItems":Ljava/lang/String;
    const/4 v3, 0x0

    .line 510
    iget-object v11, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->arrAddTime:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->clear()V

    .line 511
    :cond_3
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_6

    .line 512
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Long;

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 513
    .local v4, "data":J
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;

    .line 514
    .local v9, "tempItemView":Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    const/4 v11, 0x1

    invoke-virtual {v9, v11}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setFocusable(Z)V

    .line 515
    iget-object v11, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f020190

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v11

    invoke-virtual {v9, v11}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 516
    invoke-virtual {v9}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getTag()Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    if-lez v11, :cond_3

    .line 517
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getTag()Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 518
    iget-object v11, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->listContainer:Landroid/view/ViewGroup;

    invoke-virtual {v11, v9}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 519
    invoke-virtual {v9}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getId()I

    move-result v11

    const/16 v12, 0x61

    if-ne v11, v12, :cond_5

    .line 520
    const-wide/16 v11, 0x1

    add-long/2addr v11, v4

    invoke-direct {p0, v9, v11, v12}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setItemOnClickListener(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;J)V

    .line 523
    :goto_4
    if-nez v3, :cond_4

    .line 524
    const/16 v11, 0x8

    invoke-virtual {v9, v11}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setDividerVisible(I)V

    .line 525
    add-int/lit8 v3, v3, 0x1

    .line 527
    :cond_4
    iget-object v11, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->arrAddTime:Ljava/util/ArrayList;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 522
    :cond_5
    invoke-direct {p0, v9, v4, v5}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setItemOnClickListener(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;J)V

    goto :goto_4

    .line 530
    .end local v4    # "data":J
    .end local v9    # "tempItemView":Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    :cond_6
    const-string v11, "HomeLatestData"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "HomeLatestData SetPlugingAddViewArea items("

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ")"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 532
    iget-object v11, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mHomeLatestInterface:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$HomeLatestDataRefreshInterface;

    if-eqz v11, :cond_7

    .line 533
    iget-object v11, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mHomeLatestInterface:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$HomeLatestDataRefreshInterface;

    iget-object v12, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->listContainer:Landroid/view/ViewGroup;

    invoke-virtual {v12}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v12

    invoke-interface {v11, v12}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$HomeLatestDataRefreshInterface;->itemsNumberChange(I)V

    .line 534
    :cond_7
    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 535
    const-string v11, "HomeLatestData"

    const-string v12, "[PERF] SetPlugingAddViewArea - END"

    invoke-static {v11, v12}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->sendUpdateViewFromSync:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;
    .param p1, "x1"    # Ljava/lang/Boolean;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->sendUpdateViewFromSync:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mUpdateViewRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;J)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    .param p2, "x2"    # J

    .prologue
    .line 97
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setItemOnClickListener(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;J)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;Z)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    .param p2, "x2"    # Z

    .prologue
    .line 97
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setBurntCaloriesDataView(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;Z)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$HomeLatestDataRefreshInterface;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mHomeLatestInterface:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$HomeLatestDataRefreshInterface;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;
    .param p1, "x1"    # Ljava/lang/Runnable;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->callbackRunnable:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$1502(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$WFLDataReceivedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->wwflListener:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$WFLDataReceivedListener;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$WFLDataReceivedListener;)Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$WFLDataReceivedListener;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$WFLDataReceivedListener;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->wwflListener:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$WFLDataReceivedListener;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->listContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->blankTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->workingOnClickListener:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->checkActionForLogging(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2100(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->calories:[I

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;III)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 97
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setCaloriesExtra(III)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Lcom/sec/android/app/shealth/home/data/HomePedoData;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->pedoData:Lcom/sec/android/app/shealth/home/data/HomePedoData;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;Lcom/sec/android/app/shealth/home/data/HomePedoData;)Lcom/sec/android/app/shealth/home/data/HomePedoData;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/home/data/HomePedoData;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->pedoData:Lcom/sec/android/app/shealth/home/data/HomePedoData;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;I)Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;
    .param p1, "x1"    # I

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->findChildView(I)Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Lcom/sec/android/app/shealth/home/data/HomePedoData;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->getPedometerData()Lcom/sec/android/app/shealth/home/data/HomePedoData;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    .prologue
    .line 97
    iget v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->changeDataItemNumber:I

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;J)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;
    .param p1, "x1"    # J

    .prologue
    .line 97
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->findPositionCurrentList(J)I

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    .prologue
    .line 97
    iget-wide v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mExerciseTime:J

    return-wide v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;ZLcom/sec/android/app/shealth/home/data/HomePedoData;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Lcom/sec/android/app/shealth/home/data/HomePedoData;

    .prologue
    .line 97
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setWalkingMateView(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;ZLcom/sec/android/app/shealth/home/data/HomePedoData;)J

    move-result-wide v0

    return-wide v0
.end method

.method private checkActionForLogging(Ljava/lang/String;)V
    .locals 3
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 1483
    if-nez p1, :cond_1

    .line 1518
    :cond_0
    :goto_0
    return-void

    .line 1486
    :cond_1
    const-string v0, "com.sec.shealth.action.PEDOMETER"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1487
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HD02"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1488
    :cond_2
    const-string v0, "com.sec.shealth.action.EXERCISE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1489
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HD03"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1490
    :cond_3
    const-string v0, "com.sec.shealth.action.HEART_RATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1491
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HD04"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1492
    :cond_4
    const-string v0, "com.sec.shealth.action.FOOD"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1493
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HD05"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1494
    :cond_5
    const-string v0, "com.sec.shealth.action.WEIGHT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1495
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HD06"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1496
    :cond_6
    const-string v0, "com.sec.shealth.action.SLEEP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1497
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HD07"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1498
    :cond_7
    const-string v0, "com.sec.shealth.action.STRESS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1499
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HD08"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1500
    :cond_8
    const-string v0, "com.sec.shealth.action.UV"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1501
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HD09"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1502
    :cond_9
    const-string v0, "com.sec.shealth.action.SPO2"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1503
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HD10"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1504
    :cond_a
    const-string v0, "com.sec.shealth.action.COACH"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1505
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HD11"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1506
    :cond_b
    const-string v0, "android.shealth.action.MOREAPPS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1507
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HD12"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1508
    :cond_c
    const-string v0, "com.sec.shealth.action.BLOOD_GLUCOSE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1509
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HD13"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1510
    :cond_d
    const-string v0, "com.sec.shealth.action.ECG"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1511
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HD14"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1512
    :cond_e
    const-string v0, "com.sec.shealth.action.BODYTEMP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1513
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HD15"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1514
    :cond_f
    const-string v0, "com.sec.shealth.action.bloodpressure.BLOODPRESSURE_MAIN_ACTIVITY"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1515
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HD16"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1516
    :cond_10
    const-string v0, "com.sec.shealth.action.thermohygrometer.THERMOHYGROMETER_MAIN_ACTIVITY"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1517
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "HD17"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private checkForUpdateMoreOneMonth(J)Z
    .locals 8
    .param p1, "time"    # J

    .prologue
    const/4 v7, 0x2

    const/4 v4, 0x0

    .line 1439
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v1

    .line 1440
    .local v1, "today":Ljava/util/Date;
    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    .line 1442
    .local v2, "todayTime":J
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1443
    .local v0, "cal":Ljava/util/Calendar;
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1444
    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v0, v7, v5}, Ljava/util/Calendar;->set(II)V

    .line 1445
    const/16 v5, 0xb

    invoke-virtual {v0, v5, v4}, Ljava/util/Calendar;->set(II)V

    .line 1446
    const/16 v5, 0xc

    invoke-virtual {v0, v5, v4}, Ljava/util/Calendar;->set(II)V

    .line 1447
    const/16 v5, 0xd

    invoke-virtual {v0, v5, v4}, Ljava/util/Calendar;->set(II)V

    .line 1449
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    cmp-long v5, v2, v5

    if-ltz v5, :cond_0

    const/4 v4, 0x1

    :cond_0
    return v4
.end method

.method private findChildView(I)Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    .locals 3
    .param p1, "changedData"    # I

    .prologue
    const/4 v1, 0x0

    .line 646
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->listContainer:Landroid/view/ViewGroup;

    if-nez v2, :cond_1

    .line 656
    :cond_0
    :goto_0
    return-object v1

    .line 649
    :cond_1
    const/4 v2, -0x1

    iput v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->changeDataItemNumber:I

    .line 650
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->listContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 651
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->listContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v2, p1, :cond_2

    .line 652
    iput v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->changeDataItemNumber:I

    .line 653
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->listContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;

    goto :goto_0

    .line 650
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private findPositionCurrentList(J)I
    .locals 5
    .param p1, "time"    # J

    .prologue
    .line 313
    const/4 v0, 0x0

    .line 314
    .local v0, "cnt":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->arrAddTime:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 315
    .local v2, "refTime":Ljava/lang/Long;
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v3, v3, p1

    if-gtz v3, :cond_1

    .line 320
    .end local v2    # "refTime":Ljava/lang/Long;
    :cond_0
    return v0

    .line 318
    .restart local v2    # "refTime":Ljava/lang/Long;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 319
    goto :goto_0
.end method

.method private getDeviceTypeQuery()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1306
    const/4 v1, 0x0

    .line 1307
    .local v1, "query":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v0

    .line 1309
    .local v0, "deviceType":I
    const/16 v2, 0x272f

    if-ne v0, v2, :cond_0

    .line 1310
    const-string v1, " AND (user_device__id LIKE \'10020%\'  OR user_device__id LIKE \'10022%\'  OR user_device__id LIKE \'10030%\'  OR user_device__id LIKE \'10019%\'  OR user_device__id LIKE \'10024%\' )"

    .line 1319
    :goto_0
    return-object v1

    .line 1316
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " AND user_device__id LIKE \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%\' "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getPedometerData()Lcom/sec/android/app/shealth/home/data/HomePedoData;
    .locals 26

    .prologue
    .line 1181
    const-wide/16 v14, -0x1

    .line 1182
    .local v14, "time":J
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->getLatestDateOfData(I)J

    move-result-wide v23

    .line 1183
    .local v23, "startTime":J
    const/4 v12, 0x0

    .line 1184
    .local v12, "totalSteps":I
    const/4 v13, 0x0

    .line 1185
    .local v13, "calories":F
    const/16 v17, 0x0

    .line 1186
    .local v17, "deviceCount":I
    const/16 v16, 0x0

    .line 1187
    .local v16, "latestDeviceTotalSteps":I
    const/16 v18, 0x0

    .line 1189
    .local v18, "cursor":Landroid/database/Cursor;
    const-wide/16 v3, -0x1

    cmp-long v3, v23, v3

    if-nez v3, :cond_0

    .line 1190
    const-wide/16 v3, -0x1

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoTime:J

    .line 1191
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoCalories:F

    .line 1192
    const/4 v11, 0x0

    .line 1255
    :goto_0
    return-object v11

    .line 1196
    :cond_0
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SELECT walk_info.update_time, SUM(total_step) AS TOTAL_STEP_SUM, SUM(calorie) AS TOTAL_CALORIE, COUNT(DISTINCT user_device__id) AS DEVICE_COUNT FROM walk_info WHERE sync_status != 170004 AND start_time BETWEEN "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static/range {v23 .. v24}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static/range {v23 .. v24}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "total_step"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " > 0"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 1205
    .local v22, "query":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->getDeviceTypeQuery()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 1207
    .local v6, "firstQuery":Ljava/lang/String;
    :try_start_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v18

    .line 1211
    :goto_1
    if-eqz v18, :cond_6

    .line 1212
    :try_start_2
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1213
    const-string v3, "TOTAL_STEP_SUM"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v25

    .line 1214
    .end local v12    # "totalSteps":I
    .local v25, "totalSteps":I
    :try_start_3
    const-string v3, "TOTAL_CALORIE"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v13

    .line 1215
    const-string v3, "DEVICE_COUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 1217
    new-instance v19, Ljava/util/Date;

    move-object/from16 v0, v19

    move-wide/from16 v1, v23

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    .line 1218
    .local v19, "dataDate":Ljava/util/Date;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v3

    const/16 v4, 0x2719

    if-eq v3, v4, :cond_5

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->isDateToday(Ljava/util/Date;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 1220
    move-wide/from16 v14, v23

    .line 1225
    :goto_2
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 1226
    const/4 v3, 0x2

    move/from16 v0, v17

    if-lt v0, v3, :cond_1

    .line 1227
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getLastConnectedDeviceType()I

    move-result v20

    .line 1228
    .local v20, "deviceType":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "user_device__id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " LIKE "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%\' "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1230
    .local v10, "secondQuery":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v7 .. v12}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 1231
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1232
    const-string v3, "TOTAL_STEP_SUM"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 1234
    .end local v10    # "secondQuery":Ljava/lang/String;
    .end local v20    # "deviceType":I
    :cond_1
    invoke-static {v14, v15}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->loadGoalAndSearch(J)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoGoalValue:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move/from16 v12, v25

    .line 1244
    .end local v19    # "dataDate":Ljava/util/Date;
    .end local v25    # "totalSteps":I
    .restart local v12    # "totalSteps":I
    :cond_2
    :goto_3
    if-eqz v18, :cond_3

    .line 1245
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 1247
    :cond_3
    const-wide/16 v3, -0x1

    cmp-long v3, v14, v3

    if-nez v3, :cond_9

    .line 1248
    const-wide/16 v3, -0x1

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoTime:J

    .line 1249
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoCalories:F

    .line 1250
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 1208
    :catch_0
    move-exception v21

    .line 1209
    .local v21, "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 1241
    .end local v6    # "firstQuery":Ljava/lang/String;
    .end local v21    # "e":Ljava/lang/Exception;
    .end local v22    # "query":Ljava/lang/String;
    :catch_1
    move-exception v21

    .line 1242
    .restart local v21    # "e":Ljava/lang/Exception;
    :goto_4
    :try_start_5
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1244
    if-eqz v18, :cond_4

    .line 1245
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 1247
    :cond_4
    const-wide/16 v3, -0x1

    cmp-long v3, v14, v3

    if-nez v3, :cond_9

    .line 1248
    const-wide/16 v3, -0x1

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoTime:J

    .line 1249
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoCalories:F

    .line 1250
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 1222
    .end local v12    # "totalSteps":I
    .end local v21    # "e":Ljava/lang/Exception;
    .restart local v6    # "firstQuery":Ljava/lang/String;
    .restart local v19    # "dataDate":Ljava/util/Date;
    .restart local v22    # "query":Ljava/lang/String;
    .restart local v25    # "totalSteps":I
    :cond_5
    :try_start_6
    const-string/jumbo v3, "update_time"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result-wide v14

    goto/16 :goto_2

    .line 1237
    .end local v19    # "dataDate":Ljava/util/Date;
    .end local v25    # "totalSteps":I
    .restart local v12    # "totalSteps":I
    :cond_6
    :try_start_7
    const-string v3, "HomeLatestData"

    const-string v4, "loadLatestData() - No data"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 1238
    const/4 v12, 0x0

    .line 1239
    const/4 v13, 0x0

    goto :goto_3

    .line 1244
    .end local v6    # "firstQuery":Ljava/lang/String;
    .end local v22    # "query":Ljava/lang/String;
    :catchall_0
    move-exception v3

    :goto_5
    if-eqz v18, :cond_7

    .line 1245
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 1247
    :cond_7
    const-wide/16 v4, -0x1

    cmp-long v4, v14, v4

    if-nez v4, :cond_8

    .line 1248
    const-wide/16 v3, -0x1

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoTime:J

    .line 1249
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoCalories:F

    .line 1250
    const/4 v11, 0x0

    goto/16 :goto_0

    :cond_8
    throw v3

    .line 1254
    :cond_9
    new-instance v11, Lcom/sec/android/app/shealth/home/data/HomePedoData;

    invoke-direct/range {v11 .. v17}, Lcom/sec/android/app/shealth/home/data/HomePedoData;-><init>(IFJII)V

    .line 1255
    .local v11, "returnData":Lcom/sec/android/app/shealth/home/data/HomePedoData;
    goto/16 :goto_0

    .line 1244
    .end local v11    # "returnData":Lcom/sec/android/app/shealth/home/data/HomePedoData;
    .end local v12    # "totalSteps":I
    .restart local v6    # "firstQuery":Ljava/lang/String;
    .restart local v22    # "query":Ljava/lang/String;
    .restart local v25    # "totalSteps":I
    :catchall_1
    move-exception v3

    move/from16 v12, v25

    .end local v25    # "totalSteps":I
    .restart local v12    # "totalSteps":I
    goto :goto_5

    .line 1241
    .end local v12    # "totalSteps":I
    .restart local v25    # "totalSteps":I
    :catch_2
    move-exception v21

    move/from16 v12, v25

    .end local v25    # "totalSteps":I
    .restart local v12    # "totalSteps":I
    goto :goto_4
.end method

.method private isPLuginEnabled(I)Z
    .locals 4
    .param p1, "pluginName"    # I

    .prologue
    .line 636
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mContext:Landroid/content/Context;

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->getPluginRegistryDataFromName(Ljava/lang/String;Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    .line 637
    .local v1, "pluginlist":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    const/4 v0, 0x0

    .line 638
    .local v0, "isEnabled":Z
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 640
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-boolean v0, v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->enabled:Z

    .line 642
    :cond_0
    return v0
.end method

.method private static recursiveRecycle(Landroid/view/View;)V
    .locals 5
    .param p0, "rootView"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 1529
    if-nez p0, :cond_0

    .line 1549
    :goto_0
    return-void

    .line 1531
    :cond_0
    invoke-virtual {p0, v4}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1533
    instance-of v3, p0, Landroid/view/ViewGroup;

    if-eqz v3, :cond_2

    move-object v1, p0

    .line 1534
    check-cast v1, Landroid/view/ViewGroup;

    .line 1535
    .local v1, "group":Landroid/view/ViewGroup;
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 1536
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v0, :cond_1

    .line 1537
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->recursiveRecycle(Landroid/view/View;)V

    .line 1536
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1539
    :cond_1
    instance-of v3, p0, Landroid/widget/AdapterView;

    if-nez v3, :cond_2

    .line 1540
    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1544
    .end local v0    # "count":I
    .end local v1    # "group":Landroid/view/ViewGroup;
    .end local v2    # "i":I
    :cond_2
    instance-of v3, p0, Landroid/widget/ImageView;

    if-eqz v3, :cond_3

    .line 1545
    check-cast p0, Landroid/widget/ImageView;

    .end local p0    # "rootView":Landroid/view/View;
    invoke-virtual {p0, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1548
    :cond_3
    const/4 p0, 0x0

    .line 1549
    .restart local p0    # "rootView":Landroid/view/View;
    goto :goto_0
.end method

.method private setBurntCaloriesDataView(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;Z)J
    .locals 11
    .param p1, "itemView"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    .param p2, "isAnimation"    # Z

    .prologue
    .line 972
    const/4 v5, 0x0

    iput v5, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mBurntCalories:F

    .line 974
    iget-wide v5, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoTime:J

    iget-wide v7, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mExerciseTime:J

    cmp-long v5, v5, v7

    if-ltz v5, :cond_0

    iget-wide v5, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoTime:J

    :goto_0
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 975
    .local v4, "startTime":Ljava/lang/Long;
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    const-wide/16 v7, -0x1

    cmp-long v5, v5, v7

    if-nez v5, :cond_1

    .line 976
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    .line 1048
    :goto_1
    return-wide v5

    .line 974
    .end local v4    # "startTime":Ljava/lang/Long;
    :cond_0
    iget-wide v5, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mExerciseTime:J

    goto :goto_0

    .line 979
    .restart local v4    # "startTime":Ljava/lang/Long;
    :cond_1
    iget-wide v5, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoTime:J

    iget-wide v7, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mExerciseTime:J

    const/4 v9, 0x0

    invoke-static {v5, v6, v7, v8, v9}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->areTimesInSamePeriod(JJI)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 980
    new-instance v3, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;

    iget-object v5, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mContext:Landroid/content/Context;

    invoke-direct {v3, v5}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;-><init>(Landroid/content/Context;)V

    .line 981
    .local v3, "mergeCalrorie":Lcom/sec/android/app/shealth/common/utils/MergeCalorie;
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v5

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v7

    const-wide/16 v9, 0x1

    add-long/2addr v7, v9

    invoke-virtual {v3, v5, v6, v7, v8}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->getMergedCalorieGetEarlier(JJ)F

    move-result v1

    .line 982
    .local v1, "exerciseTotal":F
    float-to-int v5, v1

    iget v6, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoCalories:F

    float-to-int v6, v6

    add-int/2addr v5, v6

    int-to-float v5, v5

    iput v5, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mBurntCalories:F

    .line 983
    iget v5, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mBurntCalories:F

    float-to-int v5, v5

    iget v6, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoCalories:F

    float-to-int v6, v6

    float-to-int v7, v1

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setCaloriesExtra(III)V

    .line 995
    .end local v1    # "exerciseTotal":F
    .end local v3    # "mergeCalrorie":Lcom/sec/android/app/shealth/common/utils/MergeCalorie;
    :goto_2
    invoke-static {}, Lcom/sec/android/app/shealth/data/DataMonitorUtils;->getInstance()Lcom/sec/android/app/shealth/data/DataMonitorUtils;

    move-result-object v5

    const v6, 0x9c4b

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/app/shealth/data/DataMonitorUtils;->getProgressGoalValue(IJ)Lcom/sec/android/app/shealth/data/ProgressGoalData;

    move-result-object v2

    .line 997
    .local v2, "goalData":Lcom/sec/android/app/shealth/data/ProgressGoalData;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090b8c

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->getGoalValue()J

    move-result-wide v6

    long-to-int v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0900b9

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemValue(Ljava/lang/String;)V

    .line 999
    iget v5, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mBurntCalories:F

    float-to-int v5, v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemTitle(Ljava/lang/String;)V

    .line 1000
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemTitleSecond()Landroid/widget/TextView;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1001
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0900b9

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemTitleSecond(Ljava/lang/String;)V

    .line 1002
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemTitleSecond()Landroid/widget/TextView;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0900b9

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1005
    new-instance v0, Ljava/util/Date;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-direct {v0, v5, v6}, Ljava/util/Date;-><init>(J)V

    .line 1006
    .local v0, "dataDate":Ljava/util/Date;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->isDateToday(Ljava/util/Date;)Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getTimeInAndroidFormat(J)Ljava/lang/String;

    move-result-object v5

    :goto_3
    invoke-virtual {p1, v5}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemDate(Ljava/lang/String;)V

    .line 1009
    iget v5, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mBurntCalories:F

    float-to-int v5, v5

    int-to-long v5, v5

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->getGoalValue()J

    move-result-wide v7

    cmp-long v5, v5, v7

    if-ltz v5, :cond_6

    .line 1010
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getCircleIcon()Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;

    move-result-object v5

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->setVisibility(I)V

    .line 1011
    const/4 v5, 0x0

    invoke-virtual {p1, v5}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemIconVisible(I)V

    .line 1012
    const v5, 0x7f0204b9

    invoke-virtual {p1, v5}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemIcon(I)V

    .line 1013
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemIcon()Landroid/widget/ImageView;

    move-result-object v5

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090a19

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1044
    :goto_4
    const-string v5, "com.sec.shealth.action.burnt_calories"

    invoke-static {v5}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getDeleteDashboardItem(Ljava/lang/String;)J

    move-result-wide v5

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    cmp-long v5, v5, v7

    if-eqz v5, :cond_2

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-direct {p0, v5, v6}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->checkForUpdateMoreOneMonth(J)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 1046
    :cond_2
    const-wide/16 v5, -0x1

    goto/16 :goto_1

    .line 985
    .end local v0    # "dataDate":Ljava/util/Date;
    .end local v2    # "goalData":Lcom/sec/android/app/shealth/data/ProgressGoalData;
    :cond_3
    iget-wide v5, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoTime:J

    iget-wide v7, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mExerciseTime:J

    cmp-long v5, v5, v7

    if-lez v5, :cond_4

    .line 986
    iget v5, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoCalories:F

    iput v5, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mBurntCalories:F

    .line 987
    iget v5, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mBurntCalories:F

    float-to-int v5, v5

    iget v6, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoCalories:F

    float-to-int v6, v6

    const/4 v7, 0x0

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setCaloriesExtra(III)V

    goto/16 :goto_2

    .line 989
    :cond_4
    new-instance v3, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;

    iget-object v5, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mContext:Landroid/content/Context;

    invoke-direct {v3, v5}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;-><init>(Landroid/content/Context;)V

    .line 990
    .restart local v3    # "mergeCalrorie":Lcom/sec/android/app/shealth/common/utils/MergeCalorie;
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v5

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v7

    const-wide/16 v9, 0x1

    add-long/2addr v7, v9

    invoke-virtual {v3, v5, v6, v7, v8}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->getMergedCalorieGetEarlier(JJ)F

    move-result v5

    iput v5, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mBurntCalories:F

    .line 991
    iget v5, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mBurntCalories:F

    float-to-int v5, v5

    const/4 v6, 0x0

    iget v7, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mBurntCalories:F

    float-to-int v7, v7

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setCaloriesExtra(III)V

    goto/16 :goto_2

    .line 1006
    .end local v3    # "mergeCalrorie":Lcom/sec/android/app/shealth/common/utils/MergeCalorie;
    .restart local v0    # "dataDate":Ljava/util/Date;
    .restart local v2    # "goalData":Lcom/sec/android/app/shealth/data/ProgressGoalData;
    :cond_5
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getDateInAndroidFormatWithoutYear(J)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_3

    .line 1016
    :cond_6
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getCircleIcon()Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->setVisibility(I)V

    .line 1017
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemIconVisible(I)V

    .line 1018
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getCircleIcon()Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;

    move-result-object v5

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090a19

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1021
    iget v5, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mBurntCalories:F

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->getGoalValue()J

    move-result-wide v6

    long-to-float v6, v6

    div-float/2addr v5, v6

    const v6, 0x3dcccccd    # 0.1f

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_7

    iget-boolean v5, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->readyForSync:Z

    if-nez v5, :cond_7

    .line 1022
    const v5, 0x7f02042b

    invoke-virtual {p1, v5}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setCircleIcon(I)V

    .line 1023
    iget v5, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mBurntCalories:F

    float-to-int v5, v5

    int-to-float v5, v5

    invoke-virtual {p1, v5}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setCircleIconCurrentValue(F)V

    .line 1024
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->getGoalValue()J

    move-result-wide v5

    long-to-float v5, v5

    invoke-virtual {p1, v5}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setCircleIconGoalValue(F)V

    .line 1031
    :goto_5
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getCircleIcon()Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;

    move-result-object v5

    new-instance v6, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$7;

    invoke-direct {v6, p0, p2, p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$7;-><init>(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;ZLcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;)V

    const-wide/16 v7, 0x258

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_4

    .line 1026
    :cond_7
    const v5, 0x7f02042c

    invoke-virtual {p1, v5}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setCircleIcon(I)V

    .line 1027
    const/4 v5, 0x0

    invoke-virtual {p1, v5}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setCircleIconCurrentValue(F)V

    .line 1028
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->getGoalValue()J

    move-result-wide v5

    long-to-float v5, v5

    invoke-virtual {p1, v5}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setCircleIconGoalValue(F)V

    goto :goto_5

    .line 1048
    :cond_8
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    goto/16 :goto_1
.end method

.method private setCaloriesExtra(III)V
    .locals 2
    .param p1, "burnt"    # I
    .param p2, "pedo"    # I
    .param p3, "exercise"    # I

    .prologue
    .line 1052
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->calories:[I

    if-nez v0, :cond_0

    .line 1053
    const/4 v0, 0x3

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->calories:[I

    .line 1055
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->calories:[I

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 1056
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->calories:[I

    const/4 v1, 0x1

    aput p2, v0, v1

    .line 1057
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->calories:[I

    const/4 v1, 0x2

    aput p3, v0, v1

    .line 1058
    return-void
.end method

.method private setExerciseDataView(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;Z)J
    .locals 12
    .param p1, "itemView"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    .param p2, "isAnimation"    # Z

    .prologue
    const-wide/16 v4, -0x1

    const/4 v11, 0x0

    const v10, 0x7f090021

    .line 660
    new-instance v1, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v1, v6}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;-><init>(Landroid/content/Context;)V

    .line 661
    .local v1, "exerciseData":Lcom/sec/android/app/shealth/home/data/HomeExerciseData;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->getTitleText()Ljava/lang/String;

    move-result-object v3

    .line 662
    .local v3, "title":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->getTime()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mExerciseTime:J

    .line 664
    iget-wide v6, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mExerciseTime:J

    cmp-long v6, v6, v4

    if-eqz v6, :cond_0

    const-string v6, "com.sec.shealth.action.EXERCISE"

    invoke-static {v6}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getDeleteDashboardItem(Ljava/lang/String;)J

    move-result-wide v6

    iget-wide v8, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mExerciseTime:J

    cmp-long v6, v6, v8

    if-eqz v6, :cond_0

    iget-wide v6, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mExerciseTime:J

    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->checkForUpdateMoreOneMonth(J)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 718
    :cond_0
    :goto_0
    return-wide v4

    .line 669
    :cond_1
    new-instance v0, Ljava/util/Date;

    iget-wide v4, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mExerciseTime:J

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 670
    .local v0, "dataDate":Ljava/util/Date;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->isDateToday(Ljava/util/Date;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-wide v4, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mExerciseTime:J

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getTimeInAndroidFormat(J)Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-virtual {p1, v4}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemDate(Ljava/lang/String;)V

    .line 674
    if-eqz v3, :cond_6

    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    invoke-virtual {v4}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 675
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    invoke-virtual {v4, v3}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemTitle(Ljava/lang/String;)V

    .line 682
    :goto_2
    if-eqz v3, :cond_8

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->getNameExercise()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->upgradeSpecificCode(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 684
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->getNameExercise()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->upgradeSpecificCode_getImageId(Ljava/lang/String;)I

    move-result v2

    .line 685
    .local v2, "resource":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_7

    .line 686
    invoke-virtual {p1, v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemIcon(I)V

    .line 689
    :goto_3
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemIcon()Landroid/widget/ImageView;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 707
    .end local v2    # "resource":I
    :cond_2
    :goto_4
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemValue()Landroid/widget/TextView;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 708
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getExerciseItemLayout()Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 709
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->getValueText()Landroid/text/Spanned;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 710
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->getValueImage1()I

    move-result v4

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->getValueText1()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->getValueImage2()I

    move-result v6

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->getValueText2()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v4, v5, v6, v7}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setExerciseItemLayout(ILjava/lang/String;ILjava/lang/String;)V

    .line 711
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemValue()Landroid/widget/TextView;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemValue()Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 714
    :cond_3
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->isNeedShowWearableIcon()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 715
    invoke-virtual {p1, v11}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemWearableIconVisible(I)V

    .line 718
    :cond_4
    iget-wide v4, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mExerciseTime:J

    goto/16 :goto_0

    .line 670
    :cond_5
    iget-wide v4, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mExerciseTime:J

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getDateInAndroidFormatWithoutYear(J)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 679
    :cond_6
    invoke-virtual {p1, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemTitle(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 688
    .restart local v2    # "resource":I
    :cond_7
    const v4, 0x7f0204a3

    invoke-virtual {p1, v4}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemIcon(I)V

    goto :goto_3

    .line 694
    .end local v2    # "resource":I
    :cond_8
    if-eqz v3, :cond_9

    sget-object v4, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    invoke-virtual {v4}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 695
    sget-object v4, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    invoke-virtual {v4, v3}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemIcon(I)V

    .line 696
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemIcon()Landroid/widget/ImageView;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 698
    :cond_9
    if-eqz v3, :cond_2

    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    invoke-virtual {v4}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 700
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    invoke-virtual {v4, v3}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemIcon(I)V

    .line 701
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemIcon()Landroid/widget/ImageView;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_4
.end method

.method private setHRMStateBar(Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;)V
    .locals 6
    .param p1, "heartrateData"    # Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;
    .param p2, "stateBar"    # Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 785
    new-array v0, v5, [I

    aput v4, v0, v4

    .line 786
    .local v0, "avgRange":[I
    new-array v2, v5, [I

    aput v4, v2, v4

    .line 787
    .local v2, "percentile":[I
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getUserGender()I

    move-result v1

    .line 789
    .local v1, "gender":I
    const v3, 0x2e636

    if-ne v1, v3, :cond_0

    .line 790
    invoke-virtual {p1, v5, v4}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getRestingPulsePercentile_5thAnd95th(II)[I

    move-result-object v0

    .line 791
    invoke-virtual {p1, v5, v4}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getRestingPulsePercentile(II)[I

    move-result-object v2

    .line 797
    :goto_0
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getAverage()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {p2, v3, v0, v2}, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->moveToPolygon(I[I[I)V

    .line 798
    aget v3, v2, v4

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->setLowPercentile(Ljava/lang/String;)V

    .line 799
    aget v3, v2, v5

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->setHighPercentile(Ljava/lang/String;)V

    .line 800
    return-void

    .line 793
    :cond_0
    invoke-virtual {p1, v4, v4}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getRestingPulsePercentile_5thAnd95th(II)[I

    move-result-object v0

    .line 794
    invoke-virtual {p1, v4, v4}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getRestingPulsePercentile(II)[I

    move-result-object v2

    goto :goto_0
.end method

.method private setHeartRateDataView(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;Z)J
    .locals 14
    .param p1, "itemView"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    .param p2, "isAnimation"    # Z

    .prologue
    .line 724
    const v0, 0x7f090022

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->isPLuginEnabled(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 725
    const-wide/16 v12, -0x1

    .line 781
    :cond_0
    :goto_0
    return-wide v12

    .line 729
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v10

    .line 730
    .local v10, "heartrateDatabaseHelper":Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {v10, v0, v1}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getLastTwoDataByTime(J)Ljava/util/ArrayList;

    move-result-object v11

    .line 731
    .local v11, "heartrateDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;>;"
    const-wide/16 v12, -0x1

    .line 733
    .local v12, "time":J
    if-eqz v11, :cond_7

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_7

    .line 734
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x0

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getAverage()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mContext:Landroid/content/Context;

    const v3, 0x7f0900d2

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemTitle(Ljava/lang/String;)V

    .line 735
    const/4 v0, 0x0

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getStartTime()J

    move-result-wide v12

    .line 736
    new-instance v7, Ljava/util/Date;

    invoke-direct {v7, v12, v13}, Ljava/util/Date;-><init>(J)V

    .line 737
    .local v7, "dataDate":Ljava/util/Date;
    invoke-static {v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->isDateToday(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {v12, v13}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getTimeInAndroidFormat(J)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemDate(Ljava/lang/String;)V

    .line 740
    const/4 v2, 0x0

    .line 741
    .local v2, "projection":[Ljava/lang/String;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    .end local v2    # "projection":[Ljava/lang/String;
    const/4 v0, 0x0

    const-string/jumbo v1, "user_device__id"

    aput-object v1, v2, v0

    .line 742
    .restart local v2    # "projection":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 744
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRate;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string/jumbo v5, "start_time DESC LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 745
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 746
    const-string/jumbo v0, "user_device__id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 747
    .local v8, "deviceType":Ljava/lang/String;
    if-eqz v8, :cond_2

    const-string v0, "_"

    invoke-virtual {v8, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_2

    .line 748
    const-string v0, "_"

    invoke-virtual {v8, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0x2718

    if-eq v0, v1, :cond_2

    .line 749
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemWearableIconVisible(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 755
    .end local v8    # "deviceType":Ljava/lang/String;
    :cond_2
    if-eqz v6, :cond_3

    .line 756
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 763
    :cond_3
    :goto_2
    const v0, 0x7f02046c

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemIcon(I)V

    .line 764
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemIcon()Landroid/widget/ImageView;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f090022

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 765
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mContext:Landroid/content/Context;

    const v1, 0x7f090022

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemValue(Ljava/lang/String;)V

    .line 766
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemValue()Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 768
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->HeartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;->NOT_MEDICAL_ONLY:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    if-ne v0, v1, :cond_8

    .line 769
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemTitle()Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x1

    const/high16 v3, 0x41f00000    # 30.0f

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 770
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setHeartRateStateBarVisibility(I)V

    .line 777
    :goto_3
    const-string v0, "com.sec.shealth.action.HEART_RATE"

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getDeleteDashboardItem(Ljava/lang/String;)J

    move-result-wide v0

    cmp-long v0, v0, v12

    if-eqz v0, :cond_4

    invoke-direct {p0, v12, v13}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->checkForUpdateMoreOneMonth(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 779
    :cond_4
    const-wide/16 v12, -0x1

    goto/16 :goto_0

    .line 737
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_5
    invoke-static {v12, v13}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getDateInAndroidFormatWithoutYear(J)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 752
    .restart local v2    # "projection":[Ljava/lang/String;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v9

    .line 753
    .local v9, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 755
    if-eqz v6, :cond_3

    .line 756
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 755
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_6

    .line 756
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0

    .line 760
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "dataDate":Ljava/util/Date;
    :cond_7
    const-wide/16 v12, -0x1

    goto/16 :goto_0

    .line 772
    .restart local v2    # "projection":[Ljava/lang/String;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v7    # "dataDate":Ljava/util/Date;
    :cond_8
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemTitle()Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x1

    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 773
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setHeartRateStateBarVisibility(I)V

    .line 774
    const/4 v0, 0x0

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getHeartRateStateBar()Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setHRMStateBar(Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;)V

    goto :goto_3
.end method

.method private setItemOnClickListener(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;J)V
    .locals 3
    .param p1, "itemView"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    .param p2, "ViewData"    # J

    .prologue
    .line 601
    const-wide/16 v0, -0x1

    cmp-long v0, p2, v0

    if-nez v0, :cond_0

    .line 602
    const-string v0, "HomeLatestData"

    const-string v1, "fail setItemOnClickListener cause ViewData is DEFAULT_VALUE"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 631
    :goto_0
    return-void

    .line 605
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->intentAction:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 606
    new-instance v0, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$5;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$5;-><init>(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)V

    invoke-direct {v0, p1, v1, v2}, Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener;-><init>(Landroid/view/View;Ljava/lang/Object;Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$DismissCallbacks;)V

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0
.end method

.method private setItemView(ILcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;Ljava/lang/Boolean;)J
    .locals 4
    .param p1, "dataConstants"    # I
    .param p2, "itemView"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    .param p3, "isAnimation"    # Ljava/lang/Boolean;

    .prologue
    .line 539
    const-wide/16 v0, -0x1

    .line 540
    .local v0, "viewData":J
    packed-switch p1, :pswitch_data_0

    .line 597
    :goto_0
    return-wide v0

    .line 542
    :pswitch_0
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    const/4 v3, 0x0

    invoke-direct {p0, p2, v2, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setWalkingMateView(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;ZLcom/sec/android/app/shealth/home/data/HomePedoData;)J

    move-result-wide v0

    .line 543
    const/4 v2, 0x0

    invoke-virtual {p2, v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setId(I)V

    goto :goto_0

    .line 546
    :pswitch_1
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-direct {p0, p2, v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setBurntCaloriesDataView(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;Z)J

    move-result-wide v0

    .line 547
    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    .line 548
    const/16 v2, 0x61

    invoke-virtual {p2, v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setId(I)V

    goto :goto_0

    .line 551
    :pswitch_2
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-direct {p0, p2, v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setExerciseDataView(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;Z)J

    move-result-wide v0

    .line 552
    const/4 v2, 0x1

    invoke-virtual {p2, v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setId(I)V

    goto :goto_0

    .line 555
    :pswitch_3
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-direct {p0, p2, v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setHeartRateDataView(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;Z)J

    move-result-wide v0

    .line 556
    const/16 v2, 0x15

    invoke-virtual {p2, v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setId(I)V

    goto :goto_0

    .line 561
    :pswitch_4
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-direct {p0, p2, v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setStressDataView(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;Z)J

    move-result-wide v0

    .line 562
    const/4 v2, 0x7

    invoke-virtual {p2, v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setId(I)V

    goto :goto_0

    .line 568
    :pswitch_5
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p0, p2, v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setFoodDataView(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;Z)J

    move-result-wide v0

    .line 569
    const/4 v2, 0x3

    invoke-virtual {p2, v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setId(I)V

    goto :goto_0

    .line 573
    :pswitch_6
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-direct {p0, p2, v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setSleepDataView(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;Z)J

    move-result-wide v0

    .line 574
    const/16 v2, 0x8

    invoke-virtual {p2, v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setId(I)V

    goto :goto_0

    .line 577
    :pswitch_7
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-direct {p0, p2, v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setUvDataView(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;Z)J

    move-result-wide v0

    .line 578
    const/16 v2, 0x16

    invoke-virtual {p2, v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setId(I)V

    goto :goto_0

    .line 583
    :pswitch_8
    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setSPO2DataView(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;Ljava/lang/Boolean;)J

    move-result-wide v0

    .line 584
    const/16 v2, 0x13

    invoke-virtual {p2, v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setId(I)V

    goto :goto_0

    .line 590
    :pswitch_9
    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setThermoDataView(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;Ljava/lang/Boolean;)J

    move-result-wide v0

    .line 591
    const/16 v2, 0x9

    invoke-virtual {p2, v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setId(I)V

    goto/16 :goto_0

    .line 540
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private setLastDataList()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 448
    const/4 v0, 0x0

    .line 449
    .local v0, "lastDataName":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "lastDataName":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 450
    .restart local v0    # "lastDataName":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v1, "com.sec.shealth.action.PEDOMETER"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 451
    const-string v1, "com.sec.shealth.action.EXERCISE"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 452
    const-string v1, "com.sec.shealth.action.burnt_calories"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 453
    const-string v1, "com.sec.shealth.action.HEART_RATE"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 454
    const-string v1, "com.sec.shealth.action.STRESS"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 455
    const-string v1, "com.sec.shealth.action.FOOD"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 456
    const-string v1, "com.sec.shealth.action.SLEEP"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 457
    const-string v1, "com.sec.shealth.action.UV"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458
    const-string v1, "com.sec.shealth.action.SPO2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 459
    const-string v1, "com.sec.shealth.action.thermohygrometer.THERMOHYGROMETER_MAIN_ACTIVITY"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 460
    return-object v0
.end method

.method private setSPO2DataView(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;Ljava/lang/Boolean;)J
    .locals 13
    .param p1, "itemView"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    .param p2, "isAnimation"    # Ljava/lang/Boolean;

    .prologue
    const v0, 0x7f090240

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1352
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->isPLuginEnabled(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1353
    const-wide/16 v10, -0x1

    .line 1408
    :cond_0
    :goto_0
    return-wide v10

    .line 1355
    :cond_1
    const/4 v2, 0x0

    .line 1356
    .local v2, "projection":[Ljava/lang/String;
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    .end local v2    # "projection":[Ljava/lang/String;
    const-string/jumbo v0, "value"

    aput-object v0, v2, v1

    const-string/jumbo v0, "start_time"

    aput-object v0, v2, v3

    const/4 v0, 0x2

    const-string/jumbo v1, "user_device__id"

    aput-object v1, v2, v0

    .line 1357
    .restart local v2    # "projection":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 1358
    .local v6, "cursor":Landroid/database/Cursor;
    const-wide/16 v10, -0x1

    .line 1361
    .local v10, "time":J
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$PulseOximeter;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string/jumbo v5, "start_time DESC LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1362
    if-eqz v6, :cond_3

    .line 1363
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1364
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_8

    .line 1365
    const-string/jumbo v0, "value"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 1367
    .local v12, "value":I
    const v0, 0x7f020495

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemIcon(I)V

    .line 1368
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemIcon()Landroid/widget/ImageView;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f090240

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1369
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setSpo2StateBarVisibility(I)V

    .line 1370
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getSpo2StateBar()Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;

    move-result-object v0

    int-to-double v3, v12

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->moveToPolygon(D)V

    .line 1371
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemValue()Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1373
    const-string/jumbo v0, "start_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 1374
    new-instance v7, Ljava/util/Date;

    invoke-direct {v7, v10, v11}, Ljava/util/Date;-><init>(J)V

    .line 1375
    .local v7, "dataDate":Ljava/util/Date;
    invoke-static {v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->isDateToday(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {v10, v11}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getTimeInAndroidFormat(J)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemDate(Ljava/lang/String;)V

    .line 1378
    const-string/jumbo v0, "user_device__id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1379
    .local v8, "deviceType":Ljava/lang/String;
    if-eqz v8, :cond_2

    const-string v0, "_"

    invoke-virtual {v8, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_2

    .line 1380
    const-string v0, "_"

    invoke-virtual {v8, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0x272a

    if-eq v0, v1, :cond_2

    .line 1381
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemWearableIconVisible(I)V

    .line 1384
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->SPO2:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->isMedical(Landroid/content/Context;Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1385
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemTitle()Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1386
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemTitle(Ljava/lang/String;)V

    .line 1387
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemTitle()Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x1

    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setTextSize(IF)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1399
    .end local v7    # "dataDate":Ljava/util/Date;
    .end local v8    # "deviceType":Ljava/lang/String;
    .end local v12    # "value":I
    :cond_3
    :goto_2
    if-eqz v6, :cond_4

    .line 1400
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1404
    :cond_4
    :goto_3
    const-string v0, "com.sec.shealth.action.SPO2"

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getDeleteDashboardItem(Ljava/lang/String;)J

    move-result-wide v0

    cmp-long v0, v0, v10

    if-eqz v0, :cond_5

    invoke-direct {p0, v10, v11}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->checkForUpdateMoreOneMonth(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1406
    :cond_5
    const-wide/16 v10, -0x1

    goto/16 :goto_0

    .line 1375
    .restart local v7    # "dataDate":Ljava/util/Date;
    .restart local v12    # "value":I
    :cond_6
    :try_start_1
    invoke-static {v10, v11}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getDateInAndroidFormatWithoutYear(J)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 1389
    .restart local v8    # "deviceType":Ljava/lang/String;
    :cond_7
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemTitle()Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1390
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemTitleSecond()Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 1396
    .end local v7    # "dataDate":Ljava/util/Date;
    .end local v8    # "deviceType":Ljava/lang/String;
    .end local v12    # "value":I
    :catch_0
    move-exception v9

    .line 1397
    .local v9, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1399
    if-eqz v6, :cond_4

    .line 1400
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .line 1393
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_8
    const-wide/16 v10, -0x1

    goto :goto_2

    .line 1399
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_9

    .line 1400
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v0
.end method

.method private setSleepDataView(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;Z)J
    .locals 9
    .param p1, "itemView"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    .param p2, "isAnimation"    # Z

    .prologue
    const v8, 0x7f09017e

    const-wide/16 v4, -0x1

    .line 852
    invoke-direct {p0, v8}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->isPLuginEnabled(I)Z

    move-result v6

    if-nez v6, :cond_0

    move-wide v2, v4

    .line 876
    :goto_0
    return-wide v2

    .line 855
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->getInstance()Lcom/sec/android/app/shealth/home/data/HomeDefaultData;

    move-result-object v1

    .line 856
    .local v1, "sleepData":Lcom/sec/android/app/shealth/home/data/HomeDefaultData;
    const/16 v6, 0x8

    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->setLatestData(I)V

    .line 858
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->getTime()J

    move-result-wide v2

    .line 860
    .local v2, "time":J
    cmp-long v6, v2, v4

    if-eqz v6, :cond_1

    const-string v6, "com.sec.shealth.action.SLEEP"

    invoke-static {v6}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getDeleteDashboardItem(Ljava/lang/String;)J

    move-result-wide v6

    cmp-long v6, v6, v2

    if-eqz v6, :cond_1

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->checkForUpdateMoreOneMonth(J)Z

    move-result v6

    if-eqz v6, :cond_2

    :cond_1
    move-wide v2, v4

    .line 863
    goto :goto_0

    .line 865
    :cond_2
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 866
    .local v0, "dataDate":Ljava/util/Date;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->isDateToday(Ljava/util/Date;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getTimeInAndroidFormat(J)Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-virtual {p1, v4}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemDate(Ljava/lang/String;)V

    .line 870
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->getTitleText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemTitle(Ljava/lang/String;)V

    .line 871
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->getValueText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemValue(Ljava/lang/String;)V

    .line 872
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemWearableIconVisible(I)V

    .line 873
    const v4, 0x7f02048e

    invoke-virtual {p1, v4}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemIcon(I)V

    .line 874
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemIcon()Landroid/widget/ImageView;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 866
    :cond_3
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getDateInAndroidFormatWithoutYear(J)Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method

.method private setStressDataView(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;Z)J
    .locals 13
    .param p1, "itemView"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    .param p2, "isAnimation"    # Z

    .prologue
    .line 804
    const v0, 0x7f090026

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->isPLuginEnabled(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 805
    const-wide/16 v11, -0x1

    .line 847
    :cond_0
    :goto_0
    return-wide v11

    .line 807
    :cond_1
    const/4 v2, 0x0

    .line 808
    .local v2, "projection":[Ljava/lang/String;
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    .end local v2    # "projection":[Ljava/lang/String;
    const/4 v0, 0x0

    const-string/jumbo v1, "score"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string/jumbo v1, "sample_time"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string/jumbo v1, "user_device__id"

    aput-object v1, v2, v0

    .line 809
    .restart local v2    # "projection":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 810
    .local v6, "cursor":Landroid/database/Cursor;
    const-wide/16 v11, -0x1

    .line 813
    .local v11, "time":J
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Stress;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string/jumbo v5, "sample_time DESC LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 814
    if-eqz v6, :cond_2

    .line 815
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 816
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_6

    .line 817
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemTitle()Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 818
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemValue()Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 819
    const-string/jumbo v0, "score"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v9

    .line 820
    .local v9, "stressScore":D
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setStressStateBarVisibility(I)V

    .line 821
    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setStressStateBarValue(Ljava/lang/Double;)V

    .line 822
    const v0, 0x7f0204be

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setStressChangePolygonImage(I)V

    .line 824
    const-string/jumbo v0, "sample_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    .line 825
    new-instance v7, Ljava/util/Date;

    invoke-direct {v7, v11, v12}, Ljava/util/Date;-><init>(J)V

    .line 826
    .local v7, "dataDate":Ljava/util/Date;
    invoke-static {v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->isDateToday(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getTimeInAndroidFormat(J)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemDate(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 835
    .end local v7    # "dataDate":Ljava/util/Date;
    .end local v9    # "stressScore":D
    :cond_2
    :goto_2
    if-eqz v6, :cond_3

    .line 836
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 840
    :cond_3
    :goto_3
    const v0, 0x7f02049a

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemIcon(I)V

    .line 841
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemIcon()Landroid/widget/ImageView;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f090026

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 843
    const-string v0, "com.sec.shealth.action.STRESS"

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getDeleteDashboardItem(Ljava/lang/String;)J

    move-result-wide v0

    cmp-long v0, v0, v11

    if-eqz v0, :cond_4

    invoke-direct {p0, v11, v12}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->checkForUpdateMoreOneMonth(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 845
    :cond_4
    const-wide/16 v11, -0x1

    goto/16 :goto_0

    .line 826
    .restart local v7    # "dataDate":Ljava/util/Date;
    .restart local v9    # "stressScore":D
    :cond_5
    :try_start_1
    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getDateInAndroidFormatWithoutYear(J)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_1

    .line 829
    .end local v7    # "dataDate":Ljava/util/Date;
    .end local v9    # "stressScore":D
    :cond_6
    const-wide/16 v11, -0x1

    goto :goto_2

    .line 832
    :catch_0
    move-exception v8

    .line 833
    .local v8, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 835
    if-eqz v6, :cond_3

    .line 836
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .line 835
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_7

    .line 836
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v0
.end method

.method private setThermoDataView(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;Ljava/lang/Boolean;)J
    .locals 8
    .param p1, "itemView"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    .param p2, "isAnimation"    # Ljava/lang/Boolean;

    .prologue
    const-wide/16 v4, -0x1

    .line 1413
    const v6, 0x7f090242

    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->isPLuginEnabled(I)Z

    move-result v6

    if-nez v6, :cond_0

    move-wide v2, v4

    .line 1435
    :goto_0
    return-wide v2

    .line 1416
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->getInstance()Lcom/sec/android/app/shealth/home/data/HomeDefaultData;

    move-result-object v1

    .line 1417
    .local v1, "thermoData":Lcom/sec/android/app/shealth/home/data/HomeDefaultData;
    const/16 v6, 0x9

    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->setLatestData(I)V

    .line 1419
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->getTime()J

    move-result-wide v2

    .line 1420
    .local v2, "time":J
    cmp-long v6, v2, v4

    if-eqz v6, :cond_1

    const-string v6, "com.sec.shealth.action.thermohygrometer.THERMOHYGROMETER_MAIN_ACTIVITY"

    invoke-static {v6}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getDeleteDashboardItem(Ljava/lang/String;)J

    move-result-wide v6

    cmp-long v6, v6, v2

    if-eqz v6, :cond_1

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->checkForUpdateMoreOneMonth(J)Z

    move-result v6

    if-eqz v6, :cond_2

    :cond_1
    move-wide v2, v4

    .line 1423
    goto :goto_0

    .line 1425
    :cond_2
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 1426
    .local v0, "dataDate":Ljava/util/Date;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->isDateToday(Ljava/util/Date;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getTimeInAndroidFormat(J)Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-virtual {p1, v4}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemDate(Ljava/lang/String;)V

    .line 1430
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->getTitleText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemTitle(Ljava/lang/String;)V

    .line 1431
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemValue()Landroid/widget/TextView;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1432
    const v4, 0x7f02046e

    invoke-virtual {p1, v4}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemIcon(I)V

    .line 1433
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemIcon()Landroid/widget/ImageView;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090cf5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1426
    :cond_3
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getDateInAndroidFormatWithoutYear(J)Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method

.method private setUvDataView(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;Z)J
    .locals 9
    .param p1, "itemView"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    .param p2, "isAnimation"    # Z

    .prologue
    const v8, 0x7f09002f

    const-wide/16 v4, -0x1

    .line 1323
    invoke-direct {p0, v8}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->isPLuginEnabled(I)Z

    move-result v6

    if-nez v6, :cond_0

    move-wide v1, v4

    .line 1348
    :goto_0
    return-wide v1

    .line 1326
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->getInstance()Lcom/sec/android/app/shealth/home/data/HomeDefaultData;

    move-result-object v3

    .line 1327
    .local v3, "uvData":Lcom/sec/android/app/shealth/home/data/HomeDefaultData;
    const/16 v6, 0x16

    invoke-virtual {v3, v6}, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->setLatestData(I)V

    .line 1329
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->getTime()J

    move-result-wide v1

    .line 1330
    .local v1, "time":J
    cmp-long v6, v1, v4

    if-eqz v6, :cond_1

    const-string v6, "com.sec.shealth.action.UV"

    invoke-static {v6}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getDeleteDashboardItem(Ljava/lang/String;)J

    move-result-wide v6

    cmp-long v6, v6, v1

    if-eqz v6, :cond_1

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->checkForUpdateMoreOneMonth(J)Z

    move-result v6

    if-eqz v6, :cond_2

    :cond_1
    move-wide v1, v4

    .line 1333
    goto :goto_0

    .line 1335
    :cond_2
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    .line 1336
    .local v0, "dataDate":Ljava/util/Date;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->isDateToday(Ljava/util/Date;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getTimeInAndroidFormat(J)Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-virtual {p1, v4}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemDate(Ljava/lang/String;)V

    .line 1340
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->getTitleText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemTitle(Ljava/lang/String;)V

    .line 1341
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemValue()Landroid/widget/TextView;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1342
    const v4, 0x7f0204a1

    invoke-virtual {p1, v4}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemIcon(I)V

    .line 1343
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->isNeedShowWearableIcon()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1344
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemWearableIconVisible(I)V

    .line 1346
    :cond_3
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemIcon()Landroid/widget/ImageView;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1336
    :cond_4
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getDateInAndroidFormatWithoutYear(J)Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method

.method private setViewArea(Ljava/lang/Boolean;)V
    .locals 4
    .param p1, "isAnimation"    # Ljava/lang/Boolean;

    .prologue
    const/4 v3, 0x0

    .line 464
    const-string v1, "HomeLatestData"

    const-string v2, "HomeLatestData setViewArea"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->listContainer:Landroid/view/ViewGroup;

    if-nez v1, :cond_0

    .line 466
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mView:Landroid/view/View;

    const v2, 0x7f0804f5

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->listContainer:Landroid/view/ViewGroup;

    .line 467
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mView:Landroid/view/View;

    const v2, 0x7f0804f6

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->blankTextView:Landroid/widget/TextView;

    .line 470
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setLastDataList()Ljava/util/ArrayList;

    move-result-object v0

    .line 472
    .local v0, "lastDataArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->SetPlugingAddViewArea(Ljava/util/ArrayList;Ljava/lang/Boolean;)V

    .line 474
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->listContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 475
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->blankTextView:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 476
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v1, v3, v3}, Landroid/widget/ScrollView;->scrollTo(II)V

    .line 481
    :goto_0
    return-void

    .line 478
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->blankTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 479
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->blankTextView:Landroid/widget/TextView;

    const v2, 0x7f0907dd

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method private setWalkingMateView(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;ZLcom/sec/android/app/shealth/home/data/HomePedoData;)J
    .locals 12
    .param p1, "itemView"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    .param p2, "isAnimation"    # Z
    .param p3, "inData"    # Lcom/sec/android/app/shealth/home/data/HomePedoData;

    .prologue
    const/4 v11, 0x4

    const v10, 0x3ecccccd    # 0.4f

    const v9, 0x7f090020

    const v8, 0x3dcccccd    # 0.1f

    const/4 v7, 0x0

    .line 1061
    const/4 v2, 0x0

    .line 1063
    .local v2, "finalData":Lcom/sec/android/app/shealth/home/data/HomePedoData;
    if-nez p3, :cond_1

    .line 1064
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->getPedometerData()Lcom/sec/android/app/shealth/home/data/HomePedoData;

    move-result-object v2

    .line 1069
    :goto_0
    if-eqz v2, :cond_0

    invoke-direct {p0, v9}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->isPLuginEnabled(I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1070
    :cond_0
    const-wide/16 v3, -0x1

    .line 1177
    :goto_1
    return-wide v3

    .line 1066
    :cond_1
    move-object v2, p3

    goto :goto_0

    .line 1073
    :cond_2
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v0

    .line 1075
    .local v0, "currentDeviceType":I
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/HomePedoData;->getCalories()F

    move-result v3

    iput v3, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoCalories:F

    .line 1076
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/HomePedoData;->getTime()J

    move-result-wide v3

    invoke-direct {v1, v3, v4}, Ljava/util/Date;-><init>(J)V

    .line 1077
    .local v1, "dataDate":Ljava/util/Date;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->isDateToday(Ljava/util/Date;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/HomePedoData;->getTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getTimeInAndroidFormat(J)Ljava/lang/String;

    move-result-object v3

    :goto_2
    invoke-virtual {p1, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemDate(Ljava/lang/String;)V

    .line 1079
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/HomePedoData;->getTime()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoTime:J

    .line 1081
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v3

    if-nez v3, :cond_5

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isFirstStart()Z

    move-result v3

    if-nez v3, :cond_5

    const/16 v3, 0x2719

    if-ne v0, v3, :cond_5

    .line 1083
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090a17

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemValue(Ljava/lang/String;)V

    .line 1091
    :goto_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/HomePedoData;->getTotalSteps()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemTitle(Ljava/lang/String;)V

    .line 1092
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemTitleSecond()Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1093
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0907e5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemTitleSecond(Ljava/lang/String;)V

    .line 1095
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/HomePedoData;->getTotalSteps()I

    move-result v3

    iget v4, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoGoalValue:I

    if-lt v3, v4, :cond_7

    .line 1096
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getCircleIcon()Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;

    move-result-object v3

    invoke-virtual {v3, v11}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->setVisibility(I)V

    .line 1097
    invoke-virtual {p1, v7}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemIconVisible(I)V

    .line 1098
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemIcon()Landroid/widget/ImageView;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1099
    packed-switch v0, :pswitch_data_0

    .line 1105
    const v3, 0x7f0204bc

    invoke-virtual {p1, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemIcon(I)V

    .line 1106
    invoke-virtual {p1, v7}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemWearableIconVisible(I)V

    .line 1173
    :goto_4
    const-string v3, "com.sec.shealth.action.PEDOMETER"

    invoke-static {v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getDeleteDashboardItem(Ljava/lang/String;)J

    move-result-wide v3

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/HomePedoData;->getTime()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/HomePedoData;->getTime()J

    move-result-wide v3

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->checkForUpdateMoreOneMonth(J)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 1175
    :cond_3
    const-wide/16 v3, -0x1

    goto/16 :goto_1

    .line 1077
    :cond_4
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/HomePedoData;->getTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getDateInAndroidFormatWithoutYear(J)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    .line 1085
    :cond_5
    const/16 v3, 0x272f

    if-ne v0, v3, :cond_6

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/HomePedoData;->getDeviceCount()I

    move-result v3

    const/4 v4, 0x2

    if-lt v3, v4, :cond_6

    .line 1086
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoGoalValue:I

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/home/data/HomePedoData;->getLatestDeviceValueString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemValue(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1088
    :cond_6
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090b68

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoGoalValue:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemValue(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1101
    :pswitch_0
    const v3, 0x7f0204bd

    invoke-virtual {p1, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemIcon(I)V

    .line 1102
    const/16 v3, 0x8

    invoke-virtual {p1, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemWearableIconVisible(I)V

    goto :goto_4

    .line 1110
    :cond_7
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getCircleIcon()Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->setVisibility(I)V

    .line 1111
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemIcon()Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1112
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getCircleIcon()Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1114
    packed-switch v0, :pswitch_data_1

    .line 1147
    const v3, 0x7f020434

    invoke-virtual {p1, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setCircleIcon(I)V

    .line 1148
    invoke-virtual {p1, v7}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemWearableIconVisible(I)V

    .line 1152
    :cond_8
    :goto_5
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/HomePedoData;->getTotalSteps()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoGoalValue:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    cmpl-float v3, v3, v8

    if-ltz v3, :cond_f

    iget-boolean v3, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->readyForSync:Z

    if-nez v3, :cond_f

    .line 1153
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/HomePedoData;->getTotalSteps()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setCircleIconCurrentValue(F)V

    .line 1154
    iget v3, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoGoalValue:I

    int-to-float v3, v3

    invoke-virtual {p1, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setCircleIconGoalValue(F)V

    .line 1160
    :goto_6
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getCircleIcon()Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$8;

    invoke-direct {v4, p0, p2, p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$8;-><init>(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;ZLcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;)V

    const-wide/16 v5, 0x258

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_4

    .line 1116
    :pswitch_1
    const/16 v3, 0x8

    invoke-virtual {p1, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemWearableIconVisible(I)V

    .line 1117
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->isHealthyStep()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 1118
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/HomePedoData;->getTotalSteps()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoGoalValue:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    cmpl-float v3, v3, v8

    if-ltz v3, :cond_9

    iget-boolean v3, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->readyForSync:Z

    if-nez v3, :cond_9

    .line 1119
    const v3, 0x7f0204b7

    invoke-virtual {p1, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setCircleIcon(I)V

    .line 1133
    :goto_7
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v3

    if-nez v3, :cond_8

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isFirstStart()Z

    move-result v3

    if-nez v3, :cond_8

    .line 1134
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemTitle()Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setAlpha(F)V

    .line 1135
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemTitleSecond()Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setAlpha(F)V

    .line 1136
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/HomePedoData;->getTotalSteps()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoGoalValue:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    cmpl-float v3, v3, v8

    if-ltz v3, :cond_e

    iget-boolean v3, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->readyForSync:Z

    if-nez v3, :cond_e

    .line 1137
    const v3, 0x7f0204b5

    invoke-virtual {p1, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setCircleIcon(I)V

    .line 1138
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getCircleIcon()Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700f2

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->updateProgressColor(I)V

    goto/16 :goto_5

    .line 1121
    :cond_9
    const v3, 0x7f0204b8

    invoke-virtual {p1, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setCircleIcon(I)V

    goto :goto_7

    .line 1122
    :cond_a
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->isInactivetime()Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->isDateToday(Ljava/util/Date;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 1123
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/HomePedoData;->getTotalSteps()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoGoalValue:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    cmpl-float v3, v3, v8

    if-ltz v3, :cond_b

    iget-boolean v3, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->readyForSync:Z

    if-nez v3, :cond_b

    .line 1124
    const v3, 0x7f0204b3

    invoke-virtual {p1, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setCircleIcon(I)V

    goto :goto_7

    .line 1126
    :cond_b
    const v3, 0x7f0204b4

    invoke-virtual {p1, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setCircleIcon(I)V

    goto :goto_7

    .line 1128
    :cond_c
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/HomePedoData;->getTotalSteps()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoGoalValue:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    cmpl-float v3, v3, v8

    if-ltz v3, :cond_d

    iget-boolean v3, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->readyForSync:Z

    if-nez v3, :cond_d

    .line 1129
    const v3, 0x7f0204b0

    invoke-virtual {p1, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setCircleIcon(I)V

    goto/16 :goto_7

    .line 1131
    :cond_d
    const v3, 0x7f0204b1

    invoke-virtual {p1, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setCircleIcon(I)V

    goto/16 :goto_7

    .line 1142
    :cond_e
    const v3, 0x7f0204b6

    invoke-virtual {p1, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setCircleIcon(I)V

    goto/16 :goto_5

    .line 1156
    :cond_f
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setCircleIconCurrentValue(F)V

    .line 1157
    iget v3, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mPedoGoalValue:I

    int-to-float v3, v3

    invoke-virtual {p1, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setCircleIconGoalValue(F)V

    goto/16 :goto_6

    .line 1177
    :cond_10
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/HomePedoData;->getTime()J

    move-result-wide v3

    goto/16 :goto_1

    .line 1099
    :pswitch_data_0
    .packed-switch 0x2719
        :pswitch_0
    .end packed-switch

    .line 1114
    :pswitch_data_1
    .packed-switch 0x2719
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public doBindWalkStepService(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 326
    if-nez p1, :cond_0

    .line 327
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 329
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isRun()Z

    move-result v1

    if-nez v1, :cond_1

    .line 330
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 331
    .local v0, "iService":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 332
    const-string v1, "HomeLatestData"

    const-string v2, "RealtimeHealthService is started"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    .end local v0    # "iService":Landroid/content/Intent;
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mDayStepCounterConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {p1, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 338
    const-string v1, "HomeLatestData"

    const-string v2, "\t[PERF] calling adding mUpdateViewRunnable from doBindWalkStepService"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    new-instance v1, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$3;-><init>(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->callbackRunnable:Ljava/lang/Runnable;

    .line 347
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->callbackHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->callbackRunnable:Ljava/lang/Runnable;

    const-wide/16 v3, 0x7d0

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 349
    return-void
.end method

.method public doUnbindWalkStepService()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 353
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->wwflListener:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$WFLDataReceivedListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->unregisterListener(Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;)V

    .line 355
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mDayStepCounterConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 362
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->callbackRunnable:Ljava/lang/Runnable;

    if-eqz v1, :cond_1

    .line 363
    const-string v1, "HomeLatestData"

    const-string v2, "callbackRunnable is not null"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->callbackHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->callbackRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 365
    iput-object v5, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->callbackRunnable:Ljava/lang/Runnable;

    .line 372
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->healthyCallBack:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;

    if-eqz v1, :cond_0

    .line 373
    iput-object v5, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->healthyCallBack:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;

    .line 378
    :cond_0
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->readyForSync:Z

    .line 379
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setWorkingOnClickListener(Z)V

    .line 380
    const-string v1, "HomeLatestData"

    const-string v2, "\t[PERF] calling removing mUpdateViewRunnable from doUnbindWalkStepService"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mUpdateViewRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 382
    return-void

    .line 356
    :catch_0
    move-exception v0

    .line 357
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v1, "HomeLatestData"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doUnbindWalkStepService is error : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 358
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 359
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "HomeLatestData"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doUnbindWalkStepService is error : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 367
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->healthyCallBack:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->removeCallbacks(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;)V

    goto :goto_1
.end method

.method public getItemsCounts()I
    .locals 1

    .prologue
    .line 1558
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->listContainer:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    .line 1559
    const/4 v0, -0x1

    .line 1560
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->listContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    goto :goto_0
.end method

.method public getLatestDateOfData(I)J
    .locals 16
    .param p1, "type"    # I

    .prologue
    .line 1259
    const/4 v10, 0x0

    .line 1260
    .local v10, "cursor":Landroid/database/Cursor;
    const-wide/16 v12, -0x1

    .line 1261
    .local v12, "time":J
    const/4 v3, 0x0

    .line 1262
    .local v3, "selectionClause":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1264
    .local v6, "projection":[Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    move-wide v14, v12

    .line 1302
    .end local v12    # "time":J
    .local v14, "time":J
    :goto_0
    return-wide v14

    .line 1266
    .end local v14    # "time":J
    .restart local v12    # "time":J
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT start_time FROM walk_info WHERE sync_status != 170004 AND start_time <= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "total_step"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " > 0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1271
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->getDeviceTypeQuery()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1272
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ORDER BY start_time DESC LIMIT 1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1283
    :goto_1
    packed-switch p1, :pswitch_data_1

    .line 1299
    :cond_0
    :goto_2
    :pswitch_2
    if-eqz v10, :cond_1

    .line 1300
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_3
    move-wide v14, v12

    .line 1302
    .end local v12    # "time":J
    .restart local v14    # "time":J
    goto :goto_0

    .line 1275
    .end local v14    # "time":J
    .restart local v12    # "time":J
    :pswitch_3
    const/4 v0, 0x1

    new-array v6, v0, [Ljava/lang/String;

    .end local v6    # "projection":[Ljava/lang/String;
    const/4 v0, 0x0

    const-string/jumbo v1, "sample_time"

    aput-object v1, v6, v0

    .line 1276
    .restart local v6    # "projection":[Ljava/lang/String;
    goto :goto_1

    .line 1285
    :pswitch_4
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 1286
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1287
    const-string/jumbo v0, "start_time"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    goto :goto_2

    .line 1290
    :pswitch_5
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/sdk/health/content/ShealthContract$Meal;->CONTENT_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string/jumbo v9, "sample_time DESC LIMIT 1"

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 1291
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1292
    const-string/jumbo v0, "sample_time"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v12

    goto :goto_2

    .line 1295
    :catch_0
    move-exception v11

    .line 1296
    .local v11, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1299
    if-eqz v10, :cond_1

    .line 1300
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .line 1299
    .end local v11    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v10, :cond_2

    .line 1300
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 1264
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch

    .line 1283
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method

.method public recycleView()V
    .locals 1

    .prologue
    .line 1525
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->listContainer:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->recursiveRecycle(Landroid/view/View;)V

    .line 1526
    return-void
.end method

.method public refreshDashboard()V
    .locals 4

    .prologue
    .line 305
    const-string v0, "HomeLatestData"

    const-string v1, "Refreshing dashboard after wearable sync broadcast recieved"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setReadyForSync(Z)V

    .line 307
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->sendUpdateViewFromSync:Ljava/lang/Boolean;

    .line 308
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mUpdateViewRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 309
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mUpdateViewRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 310
    :cond_0
    return-void
.end method

.method public setFoodDataView(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;Z)J
    .locals 20
    .param p1, "itemView"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    .param p2, "isAnimation"    # Z

    .prologue
    .line 880
    const/4 v3, 0x2

    new-array v7, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, ""

    aput-object v4, v7, v3

    const/4 v3, 0x1

    const-string v4, ""

    aput-object v4, v7, v3

    .line 881
    .local v7, "mSelectionArgs":[Ljava/lang/String;
    const/4 v3, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->getLatestDateOfData(I)J

    move-result-wide v14

    .line 882
    .local v14, "startTime":J
    const/4 v3, 0x0

    invoke-static {v14, v15}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v3

    .line 883
    const/4 v3, 0x1

    invoke-static {v14, v15}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v3

    .line 884
    const-string/jumbo v6, "sample_time>=? AND sample_time<?"

    .line 885
    .local v6, "selectionClause":Ljava/lang/String;
    const/4 v9, 0x0

    .line 886
    .local v9, "cursor":Landroid/database/Cursor;
    const/4 v13, 0x0

    .line 887
    .local v13, "kcal":I
    const-wide/16 v16, -0x1

    .line 890
    .local v16, "time":J
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$Meal;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    const-string/jumbo v8, "sample_time DESC "

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 892
    const-string v4, "HomeLatestData"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cursor.getCount() : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz v9, :cond_1

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v3

    :goto_0
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 894
    if-eqz v9, :cond_4

    .line 895
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 896
    :goto_1
    invoke-interface {v9}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_3

    .line 897
    const-string/jumbo v3, "total_kilo_calorie"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    add-int/2addr v13, v3

    .line 898
    const-string/jumbo v3, "sample_time"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    cmp-long v3, v16, v3

    if-lez v3, :cond_2

    .line 899
    :goto_2
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 903
    :catch_0
    move-exception v11

    .line 904
    .local v11, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 906
    if-eqz v9, :cond_0

    .line 907
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 909
    :cond_0
    const-wide/16 v3, -0x1

    cmp-long v3, v16, v3

    if-nez v3, :cond_8

    move-wide/from16 v3, v16

    .line 967
    .end local v11    # "e":Ljava/lang/Exception;
    :goto_3
    return-wide v3

    .line 892
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 898
    :cond_2
    :try_start_2
    const-string/jumbo v3, "sample_time"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    goto :goto_2

    .line 901
    :cond_3
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 906
    :cond_4
    if-eqz v9, :cond_5

    .line 907
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 909
    :cond_5
    const-wide/16 v3, -0x1

    cmp-long v3, v16, v3

    if-nez v3, :cond_8

    move-wide/from16 v3, v16

    .line 910
    goto :goto_3

    .line 906
    :catchall_0
    move-exception v3

    if-eqz v9, :cond_6

    .line 907
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 909
    :cond_6
    const-wide/16 v4, -0x1

    cmp-long v4, v16, v4

    if-nez v4, :cond_7

    move-wide/from16 v3, v16

    .line 910
    goto :goto_3

    :cond_7
    throw v3

    .line 913
    :cond_8
    invoke-static {}, Lcom/sec/android/app/shealth/data/DataMonitorUtils;->getInstance()Lcom/sec/android/app/shealth/data/DataMonitorUtils;

    move-result-object v3

    const v4, 0x9c43

    move-wide/from16 v0, v16

    invoke-virtual {v3, v4, v0, v1}, Lcom/sec/android/app/shealth/data/DataMonitorUtils;->getProgressGoalValue(IJ)Lcom/sec/android/app/shealth/data/ProgressGoalData;

    move-result-object v12

    .line 914
    .local v12, "goalData":Lcom/sec/android/app/shealth/data/ProgressGoalData;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090b8c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->getGoalValue()J

    move-result-wide v4

    long-to-int v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0900b9

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemValue(Ljava/lang/String;)V

    .line 916
    invoke-virtual {v12}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->getMaxValue()F

    .line 917
    invoke-virtual {v12}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->getMaxValue()F

    .line 919
    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemTitle(Ljava/lang/String;)V

    .line 920
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemTitleSecond()Landroid/widget/TextView;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 921
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0900b9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemTitleSecond(Ljava/lang/String;)V

    .line 922
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemTitleSecond()Landroid/widget/TextView;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mContext:Landroid/content/Context;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemTitleSecond()Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 925
    new-instance v10, Ljava/util/Date;

    move-wide/from16 v0, v16

    invoke-direct {v10, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 926
    .local v10, "dataDate":Ljava/util/Date;
    invoke-static {v10}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->isDateToday(Ljava/util/Date;)Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-static/range {v16 .. v17}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getTimeInAndroidFormat(J)Ljava/lang/String;

    move-result-object v3

    :goto_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemDate(Ljava/lang/String;)V

    .line 929
    int-to-float v3, v13

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->getMinValue()F

    move-result v4

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_c

    .line 930
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getCircleIcon()Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->setVisibility(I)V

    .line 931
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemIconVisible(I)V

    .line 932
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getItemIcon()Landroid/widget/ImageView;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090028

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 934
    int-to-float v3, v13

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->getMaxValue()F

    move-result v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_b

    .line 935
    const v3, 0x7f0204bb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemIcon(I)V

    .line 963
    :goto_5
    const-string v3, "com.sec.shealth.action.FOOD"

    invoke-static {v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getDeleteDashboardItem(Ljava/lang/String;)J

    move-result-wide v3

    cmp-long v3, v3, v16

    if-eqz v3, :cond_9

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->checkForUpdateMoreOneMonth(J)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 965
    :cond_9
    const-wide/16 v3, -0x1

    goto/16 :goto_3

    .line 926
    :cond_a
    invoke-static/range {v16 .. v17}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getDateInAndroidFormatWithoutYear(J)Ljava/lang/String;

    move-result-object v3

    goto :goto_4

    .line 937
    :cond_b
    const v3, 0x7f0204ba

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemIcon(I)V

    goto :goto_5

    .line 941
    :cond_c
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getCircleIcon()Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->setVisibility(I)V

    .line 942
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setItemIconVisible(I)V

    .line 943
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getCircleIcon()Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090028

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 945
    int-to-float v3, v13

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->getGoalValue()J

    move-result-wide v4

    long-to-float v4, v4

    div-float/2addr v3, v4

    const v4, 0x3dcccccd    # 0.1f

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_d

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->readyForSync:Z

    if-nez v3, :cond_d

    .line 946
    const v3, 0x7f020432

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setCircleIcon(I)V

    .line 947
    int-to-float v3, v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setCircleIconCurrentValue(F)V

    .line 948
    invoke-virtual {v12}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->getGoalValue()J

    move-result-wide v3

    long-to-float v3, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setCircleIconGoalValue(F)V

    .line 955
    :goto_6
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getCircleIcon()Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$6;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v4, v0, v1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$6;-><init>(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;)V

    const-wide/16 v18, 0x258

    move-wide/from16 v0, v18

    invoke-virtual {v3, v4, v0, v1}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_5

    .line 950
    :cond_d
    const v3, 0x7f020433

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setCircleIcon(I)V

    .line 951
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setCircleIconCurrentValue(F)V

    .line 952
    invoke-virtual {v12}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->getGoalValue()J

    move-result-wide v3

    long-to-float v3, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setCircleIconGoalValue(F)V

    goto :goto_6

    :cond_e
    move-wide/from16 v3, v16

    .line 967
    goto/16 :goto_3
.end method

.method public setReadyForSync(Z)V
    .locals 0
    .param p1, "readyForSync"    # Z

    .prologue
    .line 439
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->readyForSync:Z

    .line 440
    return-void
.end method

.method public setRefreshListener(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$HomeLatestDataRefreshInterface;)V
    .locals 0
    .param p1, "mHomeLatestInterface"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$HomeLatestDataRefreshInterface;

    .prologue
    .line 444
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mHomeLatestInterface:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$HomeLatestDataRefreshInterface;

    .line 445
    return-void
.end method

.method public setWorkingOnClickListener(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 1521
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->workingOnClickListener:Z

    .line 1522
    return-void
.end method

.method public updateView(Ljava/lang/Boolean;)V
    .locals 3
    .param p1, "isAnimation"    # Ljava/lang/Boolean;

    .prologue
    .line 428
    const-string v0, "HomeLatestData"

    const-string v1, "[PERF] updateView - START"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    const-string v0, "HomeLatestData"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HomeLatestData updateView (isAnimation : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " )"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->recycleView()V

    .line 431
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mView:Landroid/view/View;

    const v1, 0x7f0804f5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->listContainer:Landroid/view/ViewGroup;

    .line 432
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->listContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 433
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setWorkingOnClickListener(Z)V

    .line 434
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setViewArea(Ljava/lang/Boolean;)V

    .line 435
    const-string v0, "HomeLatestData"

    const-string v1, "[PERF] updateView - END"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    return-void
.end method
