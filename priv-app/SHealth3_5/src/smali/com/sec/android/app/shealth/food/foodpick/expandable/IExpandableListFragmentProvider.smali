.class public interface abstract Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;
.super Ljava/lang/Object;
.source "IExpandableListFragmentProvider.java"


# virtual methods
.method public abstract deleteCheckedItems()Z
.end method

.method public abstract getCheckedItemsAmount()I
.end method

.method public abstract getExpandableListAdapter()Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;
.end method

.method public abstract isExpandableListEmpty()Z
.end method

.method public abstract updateFragmentState(Landroid/os/Bundle;Z)V
.end method
