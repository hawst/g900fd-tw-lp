.class Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;
.super Ljava/lang/Object;
.source "ProfileHeightView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EditTextHandler"
.end annotation


# instance fields
.field pos:I

.field preData:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)V
    .locals 0

    .prologue
    .line 412
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$1;

    .prologue
    .line 412
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 477
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->isChangeFocus:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$1200(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 478
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->isChangeValue:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$1302(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;Z)Z

    .line 479
    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 483
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$400(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getSelectionStart()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;->pos:I

    .line 484
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$400(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;->preData:Ljava/lang/String;

    .line 485
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 12
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 418
    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # invokes: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->isHeighUnitCM()Z
    invoke-static {v7}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$800(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 419
    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v7}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$400(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 420
    .local v5, "str":Ljava/lang/String;
    const-string v7, "\\."

    invoke-virtual {v5, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 421
    .local v0, "array":[Ljava/lang/String;
    array-length v7, v0

    const/4 v8, 0x2

    if-ne v7, v8, :cond_0

    .line 422
    const/4 v7, 0x1

    aget-object v7, v0, v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    const/4 v8, 0x1

    if-le v7, v8, :cond_0

    .line 423
    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v7}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$400(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v9, 0x0

    aget-object v9, v0, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget-object v9, v0, v9

    const/4 v10, 0x0

    const/4 v11, 0x1

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 425
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v7}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$400(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v7

    iget v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;->pos:I

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v7, v8}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setSelection(I)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 433
    .end local v0    # "array":[Ljava/lang/String;
    .end local v5    # "str":Ljava/lang/String;
    :cond_0
    :goto_0
    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightUnit:I
    invoke-static {v7}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$200(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)I

    move-result v7

    const v8, 0x249f1

    if-ne v7, v8, :cond_2

    .line 435
    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # invokes: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->getHeightTextValue()F
    invoke-static {v7}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$900(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    .line 436
    .local v6, "value":Ljava/lang/Float;
    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v7

    const/high16 v8, -0x40800000    # -1.0f

    cmpl-float v7, v7, v8

    if-eqz v7, :cond_1

    .line 438
    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v7

    const/high16 v8, 0x43960000    # 300.0f

    cmpl-float v7, v7, v8

    if-lez v7, :cond_1

    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mListener:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;
    invoke-static {v7}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$1000(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 440
    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->setMinValueText()V

    .line 441
    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v7}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$400(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v7

    const/4 v8, 0x5

    invoke-virtual {v7, v8}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setSelection(I)V

    .line 472
    .end local v6    # "value":Ljava/lang/Float;
    :cond_1
    :goto_1
    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;->preData:Ljava/lang/String;

    iget v9, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;->pos:I

    # invokes: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->setDatalimit(Ljava/lang/String;I)V
    invoke-static {v7, v8, v9}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$1100(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;Ljava/lang/String;I)V

    .line 473
    return-void

    .line 426
    .restart local v0    # "array":[Ljava/lang/String;
    .restart local v5    # "str":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 427
    .local v1, "e":Ljava/lang/IndexOutOfBoundsException;
    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v7}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$400(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v8}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$400(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->length()I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setSelection(I)V

    .line 428
    invoke-virtual {v1}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0

    .line 448
    .end local v0    # "array":[Ljava/lang/String;
    .end local v1    # "e":Ljava/lang/IndexOutOfBoundsException;
    .end local v5    # "str":Ljava/lang/String;
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v7}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$500(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 449
    .local v2, "ftString":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v7}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$600(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 453
    .local v3, "inchString":Ljava/lang/String;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_1

    .line 455
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    const/16 v8, 0x9

    if-gt v7, v8, :cond_3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    const/16 v8, 0xb

    if-le v7, v8, :cond_1

    .line 457
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->setMinValueText()V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 462
    :catch_1
    move-exception v4

    .line 464
    .local v4, "nfe":Ljava/lang/NumberFormatException;
    const-string v7, "ProfileHeightView"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Input values are invalid Integers ftString : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " and inchString : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->setMinValueText()V

    goto/16 :goto_1
.end method
