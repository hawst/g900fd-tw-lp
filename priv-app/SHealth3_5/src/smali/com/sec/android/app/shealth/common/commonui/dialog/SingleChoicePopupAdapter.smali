.class public Lcom/sec/android/app/shealth/common/commonui/dialog/SingleChoicePopupAdapter;
.super Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;
.source "SingleChoicePopupAdapter.java"


# instance fields
.field protected isChecked:[Z


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;[Z)V
    .locals 0
    .param p3, "checked"    # [Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;[Z)V"
        }
    .end annotation

    .prologue
    .line 34
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "descriptions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;-><init>(Ljava/util/List;Ljava/util/List;)V

    .line 35
    iput-object p3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SingleChoicePopupAdapter;->isChecked:[Z

    .line 36
    return-void
.end method


# virtual methods
.method public getIndexChecked()I
    .locals 2

    .prologue
    .line 129
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SingleChoicePopupAdapter;->isChecked:[Z

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 131
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SingleChoicePopupAdapter;->isChecked:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_1

    .line 134
    :cond_0
    return v0

    .line 129
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 41
    new-instance v2, Lcom/sec/android/app/shealth/common/commonui/dialog/CheckableTextItemModel;

    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SingleChoicePopupAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SingleChoicePopupAdapter;->descriptions:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SingleChoicePopupAdapter;->isChecked:[Z

    aget-boolean v3, v3, p1

    invoke-direct {v2, v0, v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/CheckableTextItemModel;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v2
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 140
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v7, 0x0

    .line 48
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SingleChoicePopupAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/common/commonui/dialog/CheckableTextItemModel;

    .line 49
    .local v3, "itemModel":Lcom/sec/android/app/shealth/common/commonui/dialog/CheckableTextItemModel;
    if-nez p2, :cond_1

    .line 51
    if-nez p3, :cond_0

    .line 52
    const/4 v7, 0x0

    .line 117
    :goto_0
    return-object v7

    .line 53
    :cond_0
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "layout_inflater"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 54
    .local v2, "inflater":Landroid/view/LayoutInflater;
    sget v8, Lcom/sec/android/app/shealth/common/commonui/R$layout;->radio_button_item_normal:I

    invoke-virtual {v2, v8, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 57
    sget v8, Lcom/sec/android/app/shealth/common/commonui/R$id;->listItemParent:I

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    new-instance v9, Lcom/sec/android/app/shealth/common/commonui/dialog/SingleChoicePopupAdapter$1;

    invoke-direct {v9, p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SingleChoicePopupAdapter$1;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/SingleChoicePopupAdapter;)V

    invoke-virtual {v8, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    .end local v2    # "inflater":Landroid/view/LayoutInflater;
    :cond_1
    sget v8, Lcom/sec/android/app/shealth/common/commonui/R$id;->text:I

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 79
    .local v6, "tv":Landroid/widget/TextView;
    if-eqz v6, :cond_2

    .line 81
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/CheckableTextItemModel;->getText()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    :cond_2
    sget v8, Lcom/sec/android/app/shealth/common/commonui/R$id;->btns_margin:I

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 85
    .local v1, "divider":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SingleChoicePopupAdapter;->getCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-ne p1, v8, :cond_5

    .line 87
    const/16 v8, 0x8

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 93
    :goto_1
    sget v8, Lcom/sec/android/app/shealth/common/commonui/R$id;->radio_button:I

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RadioButton;

    .line 94
    .local v4, "radioButton":Landroid/widget/RadioButton;
    if-eqz v4, :cond_3

    iget-object v8, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SingleChoicePopupAdapter;->mdisabledIndex:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 95
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/CheckableTextItemModel;->isChecked()Z

    move-result v8

    invoke-virtual {v4, v8}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 96
    :cond_3
    invoke-virtual {p2, p1}, Landroid/view/View;->setId(I)V

    .line 98
    const-string v5, " "

    .line 100
    .local v5, "space":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/CheckableTextItemModel;->getDescription()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/sec/android/app/shealth/common/commonui/R$string;->prompt_comma:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/sec/android/app/shealth/common/commonui/R$string;->prompt_radiobtn:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/sec/android/app/shealth/common/commonui/R$string;->prompt_button:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/sec/android/app/shealth/common/commonui/R$string;->prompt_comma:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 104
    .local v0, "description":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/CheckableTextItemModel;->isChecked()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 106
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/sec/android/app/shealth/common/commonui/R$string;->prompt_tick:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2, v8}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 115
    :goto_2
    iget-object v8, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SingleChoicePopupAdapter;->mdisabledIndex:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_4

    const/4 v7, 0x1

    :cond_4
    invoke-virtual {p2, v7}, Landroid/view/View;->setEnabled(Z)V

    move-object v7, p2

    .line 117
    goto/16 :goto_0

    .line 91
    .end local v0    # "description":Ljava/lang/String;
    .end local v4    # "radioButton":Landroid/widget/RadioButton;
    .end local v5    # "space":Ljava/lang/String;
    :cond_5
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 111
    .restart local v0    # "description":Ljava/lang/String;
    .restart local v4    # "radioButton":Landroid/widget/RadioButton;
    .restart local v5    # "space":Ljava/lang/String;
    :cond_6
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/sec/android/app/shealth/common/commonui/R$string;->prompt_Not_tick:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2, v8}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method
