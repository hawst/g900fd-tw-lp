.class public Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/app/shealth/framework/ui/gesture/GestureObserver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector$SwipeListener;
    }
.end annotation


# static fields
.field public static final DEFAULT_MINIMUM_DISTANCE_FOR_SWIPE:I = 0xc8

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private interrupted:Z

.field private mListener:Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector$SwipeListener;

.field private mMinimumSwipeDistance:I

.field private prevPoint:F

.field private startPoint:F

.field private swipeEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector$SwipeListener;)V
    .locals 1

    const/16 v0, 0xc8

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;-><init>(Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector$SwipeListener;I)V

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector$SwipeListener;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->interrupted:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->swipeEnabled:Z

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->mListener:Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector$SwipeListener;

    iput p2, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->mMinimumSwipeDistance:I

    return-void
.end method

.method private checkSwipeAction(Landroid/view/MotionEvent;)V
    .locals 3

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->interrupted:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->startPoint:F

    sub-float/2addr v0, v1

    iget v2, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->prevPoint:F

    sub-float/2addr v2, v1

    mul-float/2addr v0, v2

    const/4 v2, 0x0

    cmpg-float v0, v0, v2

    if-gez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->interrupted:Z

    iput v1, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->prevPoint:F

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "swipe interrupted: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->interrupted:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private endSwipeAction(Landroid/view/MotionEvent;)Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-string v2, "SwipeDetector-endSwipeAction"

    const-string v3, "SwipeDetector"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->interrupted:Z

    if-nez v2, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget v3, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->startPoint:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->mMinimumSwipeDistance:I

    int-to-float v3, v3

    cmpl-float v3, v2, v3

    if-lez v3, :cond_0

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "swipe to left"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->mListener:Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector$SwipeListener;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->mListener:Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector$SwipeListener;

    if-eqz v2, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->mListener:Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector$SwipeListener;

    invoke-interface {v1}, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector$SwipeListener;->onSwipeLeft()V

    :goto_0
    return v0

    :cond_0
    neg-float v2, v2

    iget v3, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->mMinimumSwipeDistance:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "swipe to right"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->mListener:Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector$SwipeListener;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->mListener:Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector$SwipeListener;

    if-eqz v2, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->mListener:Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector$SwipeListener;

    invoke-interface {v1}, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector$SwipeListener;->onSwipeRight()V

    goto :goto_0

    :cond_1
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->interrupted:Z

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private startSwipeAction(Landroid/view/MotionEvent;)V
    .locals 1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->startPoint:F

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->startPoint:F

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->prevPoint:F

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->interrupted:Z

    return-void
.end method


# virtual methods
.method public observeTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    const-string v0, "SwipeDetector-observeTouchEvent"

    const-string v1, "SwipeDetector"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->swipeEnabled:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return v0

    :pswitch_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->startSwipeAction(Landroid/view/MotionEvent;)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->endSwipeAction(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_1

    :pswitch_2
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/gesture/SwipeDetector;->checkSwipeAction(Landroid/view/MotionEvent;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
