.class public Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeCandleData;
.super Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;
.source "ExtendedSchartTimeCandleData.java"

# interfaces
.implements Lcom/sec/android/app/shealth/logutils/graph/data/ICommonChartData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;-><init>()V

    return-void
.end method


# virtual methods
.method public getMaxDataValue()D
    .locals 4

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeCandleData;->getValueLow()D

    move-result-wide v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeCandleData;->getValueHigh()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method public getMinDataValue()D
    .locals 4

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeCandleData;->getValueLow()D

    move-result-wide v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeCandleData;->getValueHigh()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    return-wide v0
.end method
