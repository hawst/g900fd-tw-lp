.class Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$6;
.super Ljava/lang/Object;
.source "SHealthAlertDialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->refreshAlertDialogFocusables(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field final synthetic val$parentContent:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1516
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$6;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iput-object p2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$6;->val$parentContent:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 1520
    new-instance v1, Lcom/sec/android/app/shealth/common/utils/FocusWorker;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;-><init>()V

    .line 1521
    .local v1, "worker":Lcom/sec/android/app/shealth/common/utils/FocusWorker;
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$6;->val$parentContent:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->refreshFocusables(Landroid/view/View;)V

    .line 1522
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1523
    .local v0, "focusables":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$6;->val$parentContent:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$6;->val$parentContent:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1525
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$6;->val$parentContent:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->clearFocus()V

    .line 1526
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$6;->val$parentContent:Landroid/view/View;

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->getAllFocusableChildViews(Landroid/view/ViewGroup;)Ljava/util/List;

    move-result-object v0

    .line 1527
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 1529
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    .line 1532
    :cond_0
    return-void
.end method
