.class public Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;
.super Landroid/app/IntentService;
.source "CoachIntentService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$OnCoachRestoreSuccessListener;,
        Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;
    }
.end annotation


# static fields
.field private static HANDLE_MIDNIGHT:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;

.field private static mCoachRestoreSuccessListener:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$OnCoachRestoreSuccessListener;",
            ">;"
        }
    .end annotation
.end field

.field private static mCurrentTime:J

.field private static mDateChageCallbackList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->TAG:Ljava/lang/String;

    .line 26
    const-string v0, "com.sec.android.app.shealth.cignacoach.HANDLE_MIDNIGHT"

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->HANDLE_MIDNIGHT:Ljava/lang/String;

    .line 29
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->mCurrentTime:J

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->TAG:Ljava/lang/String;

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 34
    return-void
.end method

.method public static refreshMidnightAlarm()V
    .locals 2

    .prologue
    .line 193
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->mCurrentTime:J

    .line 195
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaDaybreakService;->cancelMidNightAlarm()V

    .line 196
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaDaybreakService;->setMidNightAlarm()V

    .line 197
    return-void
.end method

.method public static declared-synchronized registerDayChangedListener(Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;)V
    .locals 4
    .param p0, "newDayCallback"    # Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

    .prologue
    .line 159
    const-class v1, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;

    monitor-enter v1

    if-eqz p0, :cond_0

    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->mDateChageCallbackList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 160
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->mDateChageCallbackList:Ljava/util/ArrayList;

    .line 163
    :cond_0
    if-eqz p0, :cond_1

    .line 164
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->mDateChageCallbackList:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 167
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v2

    sput-wide v2, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->mCurrentTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 168
    monitor-exit v1

    return-void

    .line 159
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized registerRestoreSuccessListener(Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$OnCoachRestoreSuccessListener;)V
    .locals 2
    .param p0, "listener"    # Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$OnCoachRestoreSuccessListener;

    .prologue
    .line 178
    const-class v1, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;

    monitor-enter v1

    if-eqz p0, :cond_0

    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->mCoachRestoreSuccessListener:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 179
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->mCoachRestoreSuccessListener:Ljava/util/ArrayList;

    .line 182
    :cond_0
    if-eqz p0, :cond_1

    .line 183
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->mCoachRestoreSuccessListener:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 184
    :cond_1
    monitor-exit v1

    return-void

    .line 178
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static unregisterDayChangedListener(Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;)V
    .locals 2
    .param p0, "newDayCallback"    # Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

    .prologue
    .line 171
    if-eqz p0, :cond_0

    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->mDateChageCallbackList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 172
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->mDateChageCallbackList:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 174
    :cond_0
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->mCurrentTime:J

    .line 175
    return-void
.end method

.method public static unregisterRestoreSuccessListener(Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$OnCoachRestoreSuccessListener;)V
    .locals 1
    .param p0, "listener"    # Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$OnCoachRestoreSuccessListener;

    .prologue
    .line 187
    if-eqz p0, :cond_0

    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->mCoachRestoreSuccessListener:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 188
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->mCoachRestoreSuccessListener:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 190
    :cond_0
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 18
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 39
    if-nez p1, :cond_1

    .line 156
    :cond_0
    :goto_0
    return-void

    .line 43
    :cond_1
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v14

    if-eqz v14, :cond_0

    .line 46
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v14

    const-string v15, "ACTION_NAME"

    invoke-virtual {v14, v15}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 48
    .local v1, "action":Ljava/lang/String;
    sget-object v14, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "[CoachIntentService onHandleIntent] action: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    if-eqz v1, :cond_5

    const-string v14, "com.cigna.mobile.coach.GOAL_COMPLETED"

    invoke-virtual {v1, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_2

    const-string v14, "com.cigna.mobile.coach.GOAL_CANCELLED"

    invoke-virtual {v1, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_2

    const-string v14, "com.cigna.mobile.coach.MISSION_COMPLETED"

    invoke-virtual {v1, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_2

    const-string v14, "com.cigna.mobile.coach.MISSION_FAILED"

    invoke-virtual {v1, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 56
    :cond_2
    const-string v14, "EXTRA_NAME_GOAL_ID"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 57
    .local v6, "goalIdStr":Ljava/lang/String;
    const-string v14, "EXTRA_NAME_GOAL_SEQUENCE_ID"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 58
    .local v7, "goalSeqIdStr":Ljava/lang/String;
    const-string v14, "EXTRA_NAME_MISSION_ID"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 59
    .local v11, "missionIdStr":Ljava/lang/String;
    const-string v14, "EXTRA_NAME_MISSION_SEQUENCE_ID"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 62
    .local v12, "missionSeqIdStr":Ljava/lang/String;
    sget-object v14, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "goalId: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", goalSeqId: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", missionId: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", missionSeqId: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    const/4 v5, -0x1

    .line 65
    .local v5, "goalId":I
    const/4 v10, -0x1

    .line 66
    .local v10, "missionId":I
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v14

    if-nez v14, :cond_3

    .line 68
    :try_start_0
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    .line 73
    :cond_3
    :goto_1
    if-eqz v11, :cond_4

    invoke-virtual {v11}, Ljava/lang/String;->isEmpty()Z

    move-result v14

    if-nez v14, :cond_4

    .line 75
    :try_start_1
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v10

    .line 81
    :cond_4
    :goto_2
    sget-object v14, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->mDateChageCallbackList:Ljava/util/ArrayList;

    if-eqz v14, :cond_0

    .line 82
    sget-object v14, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->mDateChageCallbackList:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

    .line 83
    .local v3, "dateChageCallback":Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;
    invoke-interface {v3, v5, v10}, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;->onGoalMissionStatusChange(II)V

    goto :goto_3

    .line 69
    .end local v3    # "dateChageCallback":Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;
    .end local v8    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v4

    .line 70
    .local v4, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v4}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_1

    .line 76
    .end local v4    # "e":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v4

    .line 77
    .restart local v4    # "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v4}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_2

    .line 88
    .end local v4    # "e":Ljava/lang/NumberFormatException;
    .end local v5    # "goalId":I
    .end local v6    # "goalIdStr":Ljava/lang/String;
    .end local v7    # "goalSeqIdStr":Ljava/lang/String;
    .end local v10    # "missionId":I
    .end local v11    # "missionIdStr":Ljava/lang/String;
    .end local v12    # "missionSeqIdStr":Ljava/lang/String;
    :cond_5
    if-eqz v1, :cond_7

    const-string v14, "android.intent.action.TIME_SET"

    invoke-virtual {v1, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 89
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaDaybreakService;->isRun()Z

    move-result v14

    if-nez v14, :cond_6

    .line 90
    sget-object v14, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->TAG:Ljava/lang/String;

    const-string/jumbo v15, "service stopped"

    invoke-static {v14, v15}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 92
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0b0006

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 93
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v14

    invoke-virtual {v14}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v14

    sget-wide v16, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->mCurrentTime:J

    invoke-static/range {v16 .. v17}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v16

    cmp-long v14, v14, v16

    if-gez v14, :cond_0

    .line 94
    sget-object v14, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->TAG:Ljava/lang/String;

    const-string/jumbo v15, "previous date changed"

    invoke-static {v14, v15}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->refreshMidnightAlarm()V

    .line 98
    sget-object v14, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->mDateChageCallbackList:Ljava/util/ArrayList;

    if-eqz v14, :cond_0

    .line 100
    sget-object v14, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->mDateChageCallbackList:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8    # "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

    .line 101
    .restart local v3    # "dateChageCallback":Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;
    invoke-interface {v3}, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;->onDateChanged()V

    goto :goto_4

    .line 106
    .end local v3    # "dateChageCallback":Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;
    .end local v8    # "i$":Ljava/util/Iterator;
    :cond_7
    if-eqz v1, :cond_9

    sget-object v14, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->HANDLE_MIDNIGHT:Ljava/lang/String;

    invoke-virtual {v1, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 107
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaDaybreakService;->isRun()Z

    move-result v14

    if-nez v14, :cond_8

    .line 108
    sget-object v14, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->TAG:Ljava/lang/String;

    const-string/jumbo v15, "service stopped"

    invoke-static {v14, v15}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 110
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0b0006

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 111
    sget-object v14, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->TAG:Ljava/lang/String;

    const-string v15, "HANDLE_MIDNIGHT Received"

    invoke-static {v14, v15}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->refreshMidnightAlarm()V

    .line 115
    sget-object v14, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->mDateChageCallbackList:Ljava/util/ArrayList;

    if-eqz v14, :cond_0

    .line 117
    sget-object v14, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->mDateChageCallbackList:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8    # "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

    .line 118
    .restart local v3    # "dateChageCallback":Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;
    invoke-interface {v3}, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;->onDateChanged()V

    goto :goto_5

    .line 122
    .end local v3    # "dateChageCallback":Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;
    .end local v8    # "i$":Ljava/util/Iterator;
    :cond_9
    if-eqz v1, :cond_a

    const-string v14, "com.cigna.mobile.coach.COACH_WIDGET_INVALIDATE"

    invoke-virtual {v1, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 123
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->invalidateWidgets()V

    .line 124
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v14

    sget-object v15, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->SHEALTH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    move-object/from16 v0, p0

    invoke-virtual {v14, v0, v15}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getWidgetText(Landroid/content/Context;Lcom/cigna/coach/interfaces/IWidget$WidgetType;)Ljava/lang/String;

    move-result-object v2

    .line 125
    .local v2, "cignaText":Ljava/lang/String;
    sget-object v14, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "COACH_WIDGET_UPDATE CIGNA_TEXT: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    invoke-static {v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setCignaText(Ljava/lang/String;)V

    .line 127
    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v14}, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->updateHomeWidgets(Landroid/content/Context;Z)V

    .line 128
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->updateDashBoardPlainWidget(Landroid/content/Context;)V

    .line 129
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardWidget;->updateDashBoardWidget(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 130
    .end local v2    # "cignaText":Ljava/lang/String;
    :cond_a
    if-eqz v1, :cond_b

    const-string v14, "com.sec.android.app.shealth.intent.action.MODE_CHANGE"

    invoke-virtual {v1, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_b

    .line 131
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->invalidateWidgets()V

    .line 132
    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v14}, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->updateHomeWidgets(Landroid/content/Context;Z)V

    .line 133
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->updateDashBoardPlainWidget(Landroid/content/Context;)V

    .line 134
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardWidget;->updateDashBoardWidget(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 135
    :cond_b
    if-eqz v1, :cond_d

    const-string v14, "com.sec.shealth.request.plugin.DELETE_APP_DATA"

    invoke-virtual {v1, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_c

    const-string v14, "com.sec.shealth.action.RESET_COACH_DATA"

    invoke-virtual {v1, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_d

    .line 136
    :cond_c
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->resetData(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 137
    :cond_d
    if-eqz v1, :cond_e

    const-string v14, "com.cigna.mobile.coach.COACH_WIDGET_INTENT_DISMISS"

    invoke-virtual {v1, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_e

    .line 138
    const-string/jumbo v14, "widgetType"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 139
    .local v13, "widgetType":Ljava/lang/String;
    if-eqz v13, :cond_0

    .line 140
    sget-object v14, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "isCoachWidgetIntent dismissCignaWidgetNotification, widgetType: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v14

    invoke-virtual {v14, v13}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->dismissCignaWidgetNotification(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 143
    .end local v13    # "widgetType":Ljava/lang/String;
    :cond_e
    if-eqz v1, :cond_f

    const-string v14, "action_deletion_started"

    invoke-virtual {v1, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_f

    .line 144
    sget-object v14, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->TAG:Ljava/lang/String;

    const-string v15, "Erase date and reset performed, reset variables"

    invoke-static {v14, v15}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    const-string v14, ""

    invoke-static {v14}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->setCignaIntroMsg(Ljava/lang/String;)V

    .line 146
    const/4 v14, 0x0

    invoke-static {v14}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->setFirstLoadingComplete(Z)V

    goto/16 :goto_0

    .line 147
    :cond_f
    if-eqz v1, :cond_0

    const-string v14, "com.sec.android.app.shealth.cignacoach.RESTORE_SUCCESS"

    invoke-virtual {v1, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->isCoachBREnabled(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 148
    sget-object v14, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->TAG:Ljava/lang/String;

    const-string/jumbo v15, "restart coach ui after restore"

    invoke-static {v14, v15}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    sget-object v14, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->mCoachRestoreSuccessListener:Ljava/util/ArrayList;

    if-eqz v14, :cond_0

    .line 151
    sget-object v14, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->mCoachRestoreSuccessListener:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8    # "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$OnCoachRestoreSuccessListener;

    .line 152
    .local v9, "listener":Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$OnCoachRestoreSuccessListener;
    invoke-interface {v9}, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$OnCoachRestoreSuccessListener;->onRestoreSuccess()V

    goto :goto_6
.end method
