.class public Lcom/sec/android/app/shealth/plugins/exercisepro/constants/RealtimeConstants;
.super Ljava/lang/Object;
.source "RealtimeConstants.java"


# static fields
.field public static final ACTIVITY_TYPE_CYCLING:I = 0x4654

.field public static final ACTIVITY_TYPE_FE_BIKE:I = 0xd

.field public static final ACTIVITY_TYPE_FE_CLIMBER:I = 0xf

.field public static final ACTIVITY_TYPE_FE_ELLIPTICAL:I = 0xc

.field public static final ACTIVITY_TYPE_FE_GENERAL:I = 0xa

.field public static final ACTIVITY_TYPE_FE_NORDICSKIER:I = 0x10

.field public static final ACTIVITY_TYPE_FE_ROWER:I = 0xe

.field public static final ACTIVITY_TYPE_FE_TREADMILL:I = 0xb

.field public static final ACTIVITY_TYPE_FITNESS_EQUIPMENT:I = 0x7d0

.field public static final ACTIVITY_TYPE_HIKING:I = 0x4655

.field public static final ACTIVITY_TYPE_NOT_AVAILABLE:I = -0x1

.field public static final ACTIVITY_TYPE_OTHER_ACTIVITYS:I = 0x32c8

.field public static final ACTIVITY_TYPE_RUNNING:I = 0x4653

.field public static final ACTIVITY_TYPE_WALKING:I = 0x4652

.field public static final AUTO_SAVE_ENABLED:Z = true

.field public static final CALORIE_CHECK_PERIOD:I = 0xa

.field public static final COACH_LOGGING:Z = false

.field public static final DATA_TYPE_ACHIEVED_PERCENTAGE:I = 0x18

.field public static final DATA_TYPE_ASCENT:I = 0x15

.field public static final DATA_TYPE_ASCENT_MILE:I = 0x16

.field public static final DATA_TYPE_BEAT_MY_PREVIOUS_RECORD_GOAL:I = 0x11

.field public static final DATA_TYPE_CADENCE:I = 0x12

.field public static final DATA_TYPE_CALORIES:I = 0x4

.field public static final DATA_TYPE_DISTANCE:I = 0x2

.field public static final DATA_TYPE_DISTANCE_LONG:I = 0x8

.field public static final DATA_TYPE_DISTANCE_MILE:I = 0xa

.field public static final DATA_TYPE_DURATION:I = 0x1

.field public static final DATA_TYPE_ELEVATION:I = 0x6

.field public static final DATA_TYPE_ELEVATION_MILE:I = 0xf

.field public static final DATA_TYPE_GOAL:I = 0xe

.field public static final DATA_TYPE_GPS_LAT:I = 0x64

.field public static final DATA_TYPE_GPS_LNG:I = 0x65

.field public static final DATA_TYPE_HEARTBEAT_TIME:I = 0xc

.field public static final DATA_TYPE_HEARTRATE:I = 0x5

.field public static final DATA_TYPE_PACE:I = 0x13

.field public static final DATA_TYPE_PACE_MILE:I = 0x14

.field public static final DATA_TYPE_PREV_HEARTBEAT_TIME:I = 0xd

.field public static final DATA_TYPE_REALELEVATION:I = 0x10

.field public static final DATA_TYPE_REMAINING_GOAL:I = 0x7

.field public static final DATA_TYPE_SPEED:I = 0x3

.field public static final DATA_TYPE_SPEED_LONG:I = 0x9

.field public static final DATA_TYPE_SPEED_MILE:I = 0xb

.field public static final DATA_TYPE_TRAINING_EFFECT:I = 0x17

.field public static final DURATION_MAX_REACHED:Ljava/lang/String; = "DURATION_MAX_REACHED"

.field public static final FXINT:I = 0x10000

.field public static final GOAL_EVENT_TE_PACE:I = 0x1

.field public static final GOAL_EVENT_TE_PERCENT:I = 0x3

.field public static final GOAL_EVENT_TE_VALUE:I = 0x2

.field public static final GOAL_TYPE_BEAT_MY_PREVIOUS_RECORD:I = 0x6

.field public static final GOAL_TYPE_CALORIES:I = 0x4

.field public static final GOAL_TYPE_DISTANCE:I = 0x2

.field public static final GOAL_TYPE_NONE:I = 0x1

.field public static final GOAL_TYPE_SET_WORKOUT_GOAL:I = -0x1

.field public static final GOAL_TYPE_TIME:I = 0x3

.field public static final GOAL_TYPE_TRAININGLEVEL:I = 0x5

.field public static final GOAL_TYPE_TRAINING_VALUE:I = 0x7

.field public static final GPS_CALIBRATION_RELEASE:Z = true

.field public static final INDOORS_MODE:I = 0x2

.field public static final OUTDOORS_MODE:I = 0x1

.field public static final REALTIME_DEFAULT_ACTIVITY_TYPE:I = 0x4653

.field public static final REALTIME_DEFAULT_GOAL_TYPE:I = 0x1

.field public static final REALTIME_DEFAULT_MODE:I = 0x1

.field public static final TE_ANANYZE_PERIOD:I = 0x5

.field public static final TE_AUDIO_FOCUS_CONTROL:Z = true

.field public static final TE_GOAL_TESTMODE:Z = false

.field public static final TE_LIB_335_PHRASE_NUMBER:Z = true

.field public static final TE_LOGGING:Z = false

.field public static final TE_PACE_ACHIEVED:I = 0x3

.field public static final TE_PACE_INCREASE:I = 0x2

.field public static final TE_PACE_KEEP:I = 0x0

.field public static final TE_PACE_SLOW_DOWN:I = 0x1

.field public static final TE_PACE_TIME_OUT:I = 0x4

.field public static final TE_PACE_UNDEFINED:I = -0x1

.field public static final TE_USER_NOTIFY_PERIOD:I = 0x96

.field public static final TRAINING_LEVEL_EASY:I = 0x0

.field public static final TRAINING_LEVEL_HARD:I = 0x3

.field public static final TRAINING_LEVEL_IMPROVING:I = 0x2

.field public static final TRAINING_LEVEL_MODERATE:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
