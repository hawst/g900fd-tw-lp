.class public Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
.super Ljava/lang/Object;
.source "SHealthAlertDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field protected OnCenterButtonClickListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCenterButtonClickListener;

.field protected alertText:Ljava/lang/String;

.field protected alertTextResId:I

.field protected alertTitleBackgroundId:I

.field protected alertTitleColorId:I

.field protected bindedData:Landroid/os/Parcelable;

.field protected cancelButtonClickListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;

.field protected cancelButtonTextResId:I

.field protected centerButtonTextResId:I

.field protected contentInitializationListener:Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

.field protected contentResource:I

.field protected context:Landroid/content/Context;

.field private dialogItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$CheckBoxAlertItem;",
            ">;"
        }
    .end annotation
.end field

.field private doNotDismissOnBackPressed:Z

.field protected doNotDismissOnOkClick:Z

.field protected flags:I

.field protected isCancelable:Z

.field private isDismissOnAddButton:Z

.field private listDisplayItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayItem;",
            ">;"
        }
    .end annotation
.end field

.field protected needRedraw:Z

.field protected needRefreshFocusables:Z

.field protected okButtonClickListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;

.field protected okButtonTextResId:I

.field protected onBackPressListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;

.field protected onDismissListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;

.field protected onSaveInstanceStateListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnSaveInstanceStateListener;

.field protected setDefaultContentMargins:Z

.field protected titleResId:I

.field private titleString:Ljava/lang/String;

.field protected type:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 910
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 878
    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->titleResId:I

    .line 879
    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->type:I

    .line 880
    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->contentResource:I

    .line 882
    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->alertTextResId:I

    .line 883
    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->alertTitleBackgroundId:I

    .line 884
    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->alertTitleColorId:I

    .line 892
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->isCancelable:Z

    .line 893
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->needRefreshFocusables:Z

    .line 894
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->needRedraw:Z

    .line 898
    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->flags:I

    .line 899
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setDefaultContentMargins:Z

    .line 900
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->doNotDismissOnOkClick:Z

    .line 901
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->bindedData:Landroid/os/Parcelable;

    .line 904
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->doNotDismissOnBackPressed:Z

    .line 905
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->isDismissOnAddButton:Z

    .line 907
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->dialogItems:Ljava/util/ArrayList;

    .line 908
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->listDisplayItems:Ljava/util/ArrayList;

    .line 911
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->context:Landroid/content/Context;

    .line 913
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I

    .prologue
    .line 916
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 917
    invoke-virtual {p0, p2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setType(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 918
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I
    .param p3, "title"    # I

    .prologue
    .line 921
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 922
    iput p3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->titleResId:I

    .line 923
    return-void
.end method


# virtual methods
.method public addFlags(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 0
    .param p1, "flags"    # I

    .prologue
    .line 1130
    iput p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->flags:I

    .line 1131
    return-object p0
.end method

.method public build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 5

    .prologue
    const/4 v3, -0x1

    .line 1188
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->checkBuild()V

    .line 1192
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->alertText:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->alertTextResId:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->contentResource:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->alertTitleBackgroundId:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->alertTitleColorId:I

    if-ne v2, v3, :cond_0

    .line 1195
    new-instance v2, Ljava/lang/IllegalStateException;

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/sec/android/app/shealth/common/commonui/R$string;->dialog_without_content_exception:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1198
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;-><init>()V

    .line 1199
    .local v0, "dialog":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 1200
    .local v1, "dialogBundle":Landroid/os/Bundle;
    if-nez v1, :cond_1

    .line 1201
    new-instance v1, Landroid/os/Bundle;

    .end local v1    # "dialogBundle":Landroid/os/Bundle;
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1203
    .restart local v1    # "dialogBundle":Landroid/os/Bundle;
    :cond_1
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->putGeneralDialogBehaviorToBundle(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    .line 1204
    const-string v2, "alert_text"

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->alertText:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1205
    const-string v2, "alert_text_res_id"

    iget v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->alertTextResId:I

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1206
    const-string v2, "alert_title_bg_res_id"

    iget v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->alertTitleBackgroundId:I

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1208
    const-string v2, "alert_title_color_res_id"

    iget v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->alertTitleColorId:I

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1210
    const-string v2, "content_resource"

    iget v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->contentResource:I

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1211
    const-string/jumbo v2, "multi_checkbox_dialog"

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->dialogItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1212
    const-string v2, "list_display_dialog"

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->listDisplayItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1213
    const-string v2, "binded_data"

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->bindedData:Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1214
    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setArguments(Landroid/os/Bundle;)V

    .line 1215
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->okButtonClickListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setOkButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)V

    .line 1216
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->cancelButtonClickListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)V

    .line 1217
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->contentInitializationListener:Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setContentInitializationListener(Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)V

    .line 1223
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->doNotDismissOnBackPressed:Z

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setDoNotDismissOnBackPressed(Z)V

    .line 1224
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->onBackPressListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)V

    .line 1225
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->onDismissListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)V

    .line 1226
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->OnCenterButtonClickListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCenterButtonClickListener;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setAddButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCenterButtonClickListener;)V

    .line 1227
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->isDismissOnAddButton:Z

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setDismissOnAddButton(Z)V

    .line 1229
    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->access$600()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Context"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->context:Landroid/content/Context;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1230
    return-object v0
.end method

.method protected checkBuild()V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 1234
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->titleResId:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->titleString:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1235
    new-instance v0, Ljava/lang/IllegalStateException;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/common/commonui/R$string;->cannot_build_dialog_without_title_exception:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1240
    :cond_0
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->type:I

    if-ne v0, v1, :cond_1

    .line 1241
    new-instance v0, Ljava/lang/IllegalStateException;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/common/commonui/R$string;->cannot_build_dialog_without_type_exception:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1246
    :cond_1
    return-void
.end method

.method public doNotDismissOnOkClick()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 1

    .prologue
    .line 1168
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->doNotDismissOnOkClick:Z

    .line 1169
    return-object p0
.end method

.method protected putGeneralDialogBehaviorToBundle(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2
    .param p1, "dialogBundle"    # Landroid/os/Bundle;

    .prologue
    .line 1249
    if-nez p1, :cond_0

    .line 1250
    const/4 p1, 0x0

    .line 1268
    .end local p1    # "dialogBundle":Landroid/os/Bundle;
    :goto_0
    return-object p1

    .line 1251
    .restart local p1    # "dialogBundle":Landroid/os/Bundle;
    :cond_0
    const-string v0, "cancelable"

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->isCancelable:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1252
    const-string/jumbo v0, "need_redraw"

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->needRedraw:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1253
    const-string/jumbo v0, "title_res_id"

    iget v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->titleResId:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1254
    const-string/jumbo v0, "title_string"

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->titleString:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1255
    const-string/jumbo v0, "type"

    iget v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->type:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1256
    const-string v0, "flags"

    iget v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->flags:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1257
    const-string/jumbo v0, "ok_text"

    iget v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->okButtonTextResId:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1258
    const-string v0, "cancel_text"

    iget v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->cancelButtonTextResId:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1259
    const-string v0, "center_text"

    iget v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->centerButtonTextResId:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1260
    const-string/jumbo v0, "set_default_content_margins"

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setDefaultContentMargins:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1262
    const-string v0, "dont_dismiss_on_OK_click"

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->doNotDismissOnOkClick:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1264
    const-string v0, "dont_dismiss_on_back_pressed"

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->doNotDismissOnBackPressed:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1266
    const-string/jumbo v0, "need_refresh_focusables"

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->needRefreshFocusables:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 0
    .param p1, "alertTextResId"    # I

    .prologue
    .line 993
    iput p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->alertTextResId:I

    .line 994
    return-object p0
.end method

.method public setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 0
    .param p1, "alertText"    # Ljava/lang/String;

    .prologue
    .line 983
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->alertText:Ljava/lang/String;

    .line 984
    return-object p0
.end method

.method public setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 0
    .param p1, "cancelButtonClickListener"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;

    .prologue
    .line 1022
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->cancelButtonClickListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;

    .line 1023
    return-object p0
.end method

.method public setCancelButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 0
    .param p1, "resId"    # I

    .prologue
    .line 1150
    iput p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->cancelButtonTextResId:I

    .line 1151
    return-object p0
.end method

.method public setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 0
    .param p1, "isCancelable"    # Z

    .prologue
    .line 1100
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->isCancelable:Z

    .line 1101
    return-object p0
.end method

.method public setCenterButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 0
    .param p1, "resId"    # I

    .prologue
    .line 1159
    iput p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->centerButtonTextResId:I

    .line 1160
    return-object p0
.end method

.method public setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 0
    .param p1, "contentResource"    # I

    .prologue
    .line 1076
    iput p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->contentResource:I

    .line 1077
    return-object p0
.end method

.method public setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 0
    .param p1, "contentResource"    # I
    .param p2, "contentInitializationListener"    # Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

    .prologue
    .line 1088
    iput p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->contentResource:I

    .line 1089
    iput-object p2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->contentInitializationListener:Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

    .line 1090
    return-object p0
.end method

.method public setDismissOnAddClick(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 0
    .param p1, "dismissOnAddClick"    # Z

    .prologue
    .line 1178
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->isDismissOnAddButton:Z

    .line 1179
    return-object p0
.end method

.method public setDoNotDismissOnBackPressed(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 0
    .param p1, "doNotDismissOnBackPressed"    # Z

    .prologue
    .line 1055
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->doNotDismissOnBackPressed:Z

    .line 1056
    return-object p0
.end method

.method public setNeedRedraw(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 0
    .param p1, "needRedraw"    # Z

    .prologue
    .line 1120
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->needRedraw:Z

    .line 1121
    return-object p0
.end method

.method public setNeedRefreshFocusables(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 0
    .param p1, "needRefreshFocusables"    # Z

    .prologue
    .line 1110
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->needRefreshFocusables:Z

    .line 1111
    return-object p0
.end method

.method public setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 0
    .param p1, "okButtonClickListener"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;

    .prologue
    .line 1008
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->okButtonClickListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;

    .line 1009
    return-object p0
.end method

.method public setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 0
    .param p1, "resId"    # I

    .prologue
    .line 1140
    iput p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->okButtonTextResId:I

    .line 1141
    return-object p0
.end method

.method public setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 0
    .param p1, "onBackPressListener"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;

    .prologue
    .line 1044
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->onBackPressListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;

    .line 1045
    return-object p0
.end method

.method public setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 0
    .param p1, "onDismissListener"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;

    .prologue
    .line 1066
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->onDismissListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;

    .line 1067
    return-object p0
.end method

.method public setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 0
    .param p1, "titleStringResId"    # I

    .prologue
    .line 946
    iput p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->titleResId:I

    .line 947
    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 0
    .param p1, "titleString"    # Ljava/lang/String;

    .prologue
    .line 956
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->titleString:Ljava/lang/String;

    .line 957
    return-object p0
.end method

.method public setType(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 966
    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    if-eqz p1, :cond_0

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    const/4 v0, 0x6

    if-eq p1, v0, :cond_0

    const/4 v0, 0x7

    if-eq p1, v0, :cond_0

    .line 970
    new-instance v0, Ljava/lang/IllegalArgumentException;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/common/commonui/R$string;->wrong_dialog_type_exception:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 973
    :cond_0
    iput p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->type:I

    .line 974
    return-object p0
.end method
