.class Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$1;
.super Ljava/lang/Object;
.source "CignaHeightView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->initLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "adapter":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v2, 0x2

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->setHeightValue()V

    .line 116
    if-nez p3, :cond_0

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    const v1, 0x249f1

    # setter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightUnit:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$202(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;I)I

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$300(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v0

    const/16 v1, 0x2002

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setInputType(I)V

    .line 127
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->heightValueConvert()V

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightValue:F
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$500(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->setValue(F)V

    .line 131
    return-void

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    const v1, 0x249f2

    # setter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightUnit:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$202(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;I)I

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$300(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setInputType(I)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->access$400(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setInputType(I)V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 134
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
