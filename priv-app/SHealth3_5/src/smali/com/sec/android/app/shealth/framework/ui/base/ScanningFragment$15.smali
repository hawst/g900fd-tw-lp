.class Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getScannedView(Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

.field final synthetic val$device:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

.field final synthetic val$item:Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->val$item:Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;

    iput-object p3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->val$device:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static lc6AXr3()Ljava/lang/String;
    .locals 58

    const/16 v0, 0x37

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, 0x17

    const/16 v25, 0x18

    const/16 v26, 0x19

    const/16 v27, 0x1a

    const/16 v28, 0x1b

    const/16 v29, 0x1c

    const/16 v30, 0x1d

    const/16 v31, 0x1e

    const/16 v32, 0x1f

    const/16 v33, 0x20

    const/16 v34, 0x21

    const/16 v35, 0x22

    const/16 v36, 0x23

    const/16 v37, 0x24

    const/16 v38, 0x25

    const/16 v39, 0x26

    const/16 v40, 0x27

    const/16 v41, 0x28

    const/16 v42, 0x29

    const/16 v43, 0x2a

    const/16 v44, 0x2b

    const/16 v45, 0x2c

    const/16 v46, 0x2d

    const/16 v47, 0x2e

    const/16 v48, 0x2f

    const/16 v49, 0x30

    const/16 v50, 0x31

    const/16 v51, 0x32

    const/16 v52, 0x33

    const/16 v53, 0x34

    const/16 v54, 0x35

    const/16 v55, 0x36

    const/16 v56, -0x15

    aput v56, v1, v55

    const/16 v55, -0x21

    aput v55, v1, v54

    const/16 v54, -0x2d

    aput v54, v1, v53

    const/16 v53, -0x46

    aput v53, v1, v52

    const/16 v52, -0x45d6

    aput v52, v1, v51

    const/16 v51, -0x2c

    aput v51, v1, v50

    const/16 v50, 0x2a78

    aput v50, v1, v49

    const/16 v49, -0xdbb

    aput v49, v1, v48

    const/16 v48, -0x48

    aput v48, v1, v47

    const/16 v47, -0x8

    aput v47, v1, v46

    const/16 v46, -0x29

    aput v46, v1, v45

    const/16 v45, -0x3891

    aput v45, v1, v44

    const/16 v44, -0x5f

    aput v44, v1, v43

    const/16 v43, -0x13

    aput v43, v1, v42

    const/16 v42, -0x22f2

    aput v42, v1, v41

    const/16 v41, -0x48

    aput v41, v1, v40

    const/16 v40, 0x485e

    aput v40, v1, v39

    const/16 v39, 0x6f24

    aput v39, v1, v38

    const/16 v38, -0x71f2

    aput v38, v1, v37

    const/16 v37, -0x8

    aput v37, v1, v36

    const/16 v36, -0x72

    aput v36, v1, v35

    const/16 v35, -0x79d9

    aput v35, v1, v34

    const/16 v34, -0x5a

    aput v34, v1, v33

    const/16 v33, -0x4e

    aput v33, v1, v32

    const/16 v32, -0x7e

    aput v32, v1, v31

    const/16 v31, -0x65a9

    aput v31, v1, v30

    const/16 v30, -0xa

    aput v30, v1, v29

    const/16 v29, -0x41da

    aput v29, v1, v28

    const/16 v28, -0x23

    aput v28, v1, v27

    const/16 v27, -0xc

    aput v27, v1, v26

    const/16 v26, -0x64

    aput v26, v1, v25

    const/16 v25, 0x374d

    aput v25, v1, v24

    const/16 v24, 0x85e

    aput v24, v1, v23

    const/16 v23, 0x7c7e

    aput v23, v1, v22

    const/16 v22, -0x3de7

    aput v22, v1, v21

    const/16 v21, -0x5a

    aput v21, v1, v20

    const/16 v20, -0x6af4

    aput v20, v1, v19

    const/16 v19, -0xf

    aput v19, v1, v18

    const/16 v18, -0x2dff

    aput v18, v1, v17

    const/16 v17, -0x44

    aput v17, v1, v16

    const/16 v16, -0x48

    aput v16, v1, v15

    const/16 v15, -0x2e8d

    aput v15, v1, v14

    const/16 v14, -0x4e

    aput v14, v1, v13

    const/16 v13, -0x1b

    aput v13, v1, v12

    const/16 v12, -0xa

    aput v12, v1, v11

    const/16 v11, 0x1346

    aput v11, v1, v10

    const/16 v10, -0x1c99

    aput v10, v1, v9

    const/16 v9, -0x76

    aput v9, v1, v8

    const/16 v8, -0x35

    aput v8, v1, v7

    const/16 v7, -0x15c1

    aput v7, v1, v6

    const/16 v6, -0x7c

    aput v6, v1, v5

    const/16 v5, -0x45

    aput v5, v1, v4

    const/16 v4, 0x4336

    aput v4, v1, v3

    const/16 v3, 0x4e29

    aput v3, v1, v2

    const/16 v2, 0x6a6e

    aput v2, v1, v0

    const/16 v0, 0x37

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, 0x1e

    const/16 v33, 0x1f

    const/16 v34, 0x20

    const/16 v35, 0x21

    const/16 v36, 0x22

    const/16 v37, 0x23

    const/16 v38, 0x24

    const/16 v39, 0x25

    const/16 v40, 0x26

    const/16 v41, 0x27

    const/16 v42, 0x28

    const/16 v43, 0x29

    const/16 v44, 0x2a

    const/16 v45, 0x2b

    const/16 v46, 0x2c

    const/16 v47, 0x2d

    const/16 v48, 0x2e

    const/16 v49, 0x2f

    const/16 v50, 0x30

    const/16 v51, 0x31

    const/16 v52, 0x32

    const/16 v53, 0x33

    const/16 v54, 0x34

    const/16 v55, 0x35

    const/16 v56, 0x36

    const/16 v57, -0x2f

    aput v57, v0, v56

    const/16 v56, -0x1

    aput v56, v0, v55

    const/16 v55, -0x4c

    aput v55, v0, v54

    const/16 v54, -0x2c

    aput v54, v0, v53

    const/16 v53, -0x45bd

    aput v53, v0, v52

    const/16 v52, -0x46

    aput v52, v0, v51

    const/16 v51, 0x2a11

    aput v51, v0, v50

    const/16 v50, -0xdd6

    aput v50, v0, v49

    const/16 v49, -0xe

    aput v49, v0, v48

    const/16 v48, -0x75

    aput v48, v0, v47

    const/16 v47, -0x62

    aput v47, v0, v46

    const/16 v46, -0x38b1

    aput v46, v0, v45

    const/16 v45, -0x39

    aput v45, v0, v44

    const/16 v44, -0x7e

    aput v44, v0, v43

    const/16 v43, -0x22d2

    aput v43, v0, v42

    const/16 v42, -0x23

    aput v42, v0, v41

    const/16 v41, 0x482b

    aput v41, v0, v40

    const/16 v40, 0x6f48

    aput v40, v0, v39

    const/16 v39, -0x7191

    aput v39, v0, v38

    const/16 v38, -0x72

    aput v38, v0, v37

    const/16 v37, -0x52

    aput v37, v0, v36

    const/16 v36, -0x79f5

    aput v36, v0, v35

    const/16 v35, -0x7a

    aput v35, v0, v34

    const/16 v34, -0x2a

    aput v34, v0, v33

    const/16 v33, -0x19

    aput v33, v0, v32

    const/16 v32, -0x65c5

    aput v32, v0, v31

    const/16 v31, -0x66

    aput v31, v0, v30

    const/16 v30, -0x41b9

    aput v30, v0, v29

    const/16 v29, -0x42

    aput v29, v0, v28

    const/16 v28, -0x2c

    aput v28, v0, v27

    const/16 v27, -0x7

    aput v27, v0, v26

    const/16 v26, 0x372e

    aput v26, v0, v25

    const/16 v25, 0x837

    aput v25, v0, v24

    const/16 v24, 0x7c08

    aput v24, v0, v23

    const/16 v23, -0x3d84

    aput v23, v0, v22

    const/16 v22, -0x3e

    aput v22, v0, v21

    const/16 v21, -0x6ad4

    aput v21, v0, v20

    const/16 v20, -0x6b

    aput v20, v0, v19

    const/16 v19, -0x2d9c

    aput v19, v0, v18

    const/16 v18, -0x2e

    aput v18, v0, v17

    const/16 v17, -0x2a

    aput v17, v0, v16

    const/16 v16, -0x2eee

    aput v16, v0, v15

    const/16 v15, -0x2f

    aput v15, v0, v14

    const/16 v14, -0x6a

    aput v14, v0, v13

    const/16 v13, -0x2a

    aput v13, v0, v12

    const/16 v12, 0x132e

    aput v12, v0, v11

    const/16 v11, -0x1ced

    aput v11, v0, v10

    const/16 v10, -0x1d

    aput v10, v0, v9

    const/16 v9, -0x44

    aput v9, v0, v8

    const/16 v8, -0x15e1

    aput v8, v0, v7

    const/16 v7, -0x16

    aput v7, v0, v6

    const/16 v6, -0x2e

    aput v6, v0, v5

    const/16 v5, 0x4359

    aput v5, v0, v4

    const/16 v4, 0x4e43

    aput v4, v0, v3

    const/16 v3, 0x6a4e

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 44

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/4 v12, -0x8

    aput v12, v2, v11

    const/16 v11, -0x43

    aput v11, v2, v10

    const/16 v10, 0x766f

    aput v10, v2, v9

    const/16 v9, -0x2b00

    aput v9, v2, v8

    const/16 v8, -0x59

    aput v8, v2, v7

    const/16 v7, -0x3b

    aput v7, v2, v6

    const/16 v6, -0x48

    aput v6, v2, v5

    const/16 v5, 0x97a

    aput v5, v2, v4

    const/16 v4, -0x53a0

    aput v4, v2, v3

    const/16 v3, -0x40

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x63

    aput v13, v1, v12

    const/16 v12, -0x22

    aput v12, v1, v11

    const/16 v11, 0x7606

    aput v11, v1, v10

    const/16 v10, -0x2a8a

    aput v10, v1, v9

    const/16 v9, -0x2b

    aput v9, v1, v8

    const/16 v8, -0x60

    aput v8, v1, v7

    const/16 v7, -0x15

    aput v7, v1, v6

    const/16 v6, 0x918

    aput v6, v1, v5

    const/16 v5, -0x53f7

    aput v5, v1, v4

    const/16 v4, -0x54

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_1

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x27

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, -0x29f8

    aput v42, v2, v41

    const/16 v41, -0x5f

    aput v41, v2, v40

    const/16 v40, -0x4b

    aput v40, v2, v39

    const/16 v39, 0x4844

    aput v39, v2, v38

    const/16 v38, -0x64c2

    aput v38, v2, v37

    const/16 v37, -0x45

    aput v37, v2, v36

    const/16 v36, -0x7a1

    aput v36, v2, v35

    const/16 v35, -0x63

    aput v35, v2, v34

    const/16 v34, 0x5859

    aput v34, v2, v33

    const/16 v33, -0x74ca

    aput v33, v2, v32

    const/16 v32, -0x16

    aput v32, v2, v31

    const/16 v31, -0xcdc

    aput v31, v2, v30

    const/16 v30, -0x60

    aput v30, v2, v29

    const/16 v29, -0x43d1

    aput v29, v2, v28

    const/16 v28, -0x26

    aput v28, v2, v27

    const/16 v27, -0x38c1

    aput v27, v2, v26

    const/16 v26, -0x19

    aput v26, v2, v25

    const/16 v25, 0x1217

    aput v25, v2, v24

    const/16 v24, 0x4877

    aput v24, v2, v23

    const/16 v23, -0x7cda

    aput v23, v2, v22

    const/16 v22, -0x1a

    aput v22, v2, v21

    const/16 v21, -0x34

    aput v21, v2, v20

    const/16 v20, -0x1b

    aput v20, v2, v19

    const/16 v19, -0x6a

    aput v19, v2, v18

    const/16 v18, 0x470f

    aput v18, v2, v17

    const/16 v17, 0x3367

    aput v17, v2, v16

    const/16 v16, -0x10a8

    aput v16, v2, v15

    const/16 v15, -0x74

    aput v15, v2, v14

    const/16 v14, -0x6d

    aput v14, v2, v13

    const/16 v13, 0x347b

    aput v13, v2, v12

    const/16 v12, -0x4aa9

    aput v12, v2, v11

    const/16 v11, -0x6b

    aput v11, v2, v10

    const/16 v10, -0x36f7

    aput v10, v2, v9

    const/16 v9, -0x53

    aput v9, v2, v8

    const/16 v8, 0x3228

    aput v8, v2, v7

    const/16 v7, -0x6bbf

    aput v7, v2, v6

    const/4 v6, -0x6

    aput v6, v2, v5

    const/16 v5, -0x54

    aput v5, v2, v3

    const/16 v3, -0x74

    aput v3, v2, v1

    const/16 v1, 0x27

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, 0x26

    const/16 v43, -0x29d8

    aput v43, v1, v42

    const/16 v42, -0x2a

    aput v42, v1, v41

    const/16 v41, -0x30

    aput v41, v1, v40

    const/16 v40, 0x482d

    aput v40, v1, v39

    const/16 v39, -0x64b8

    aput v39, v1, v38

    const/16 v38, -0x65

    aput v38, v1, v37

    const/16 v37, -0x7c5

    aput v37, v1, v36

    const/16 v36, -0x8

    aput v36, v1, v35

    const/16 v35, 0x5837

    aput v35, v1, v34

    const/16 v34, -0x74a8

    aput v34, v1, v33

    const/16 v33, -0x75

    aput v33, v1, v32

    const/16 v32, -0xcb9

    aput v32, v1, v31

    const/16 v31, -0xd

    aput v31, v1, v30

    const/16 v30, -0x43f1

    aput v30, v1, v29

    const/16 v29, -0x44

    aput v29, v1, v28

    const/16 v28, -0x38b0

    aput v28, v1, v27

    const/16 v27, -0x39

    aput v27, v1, v26

    const/16 v26, 0x1265

    aput v26, v1, v25

    const/16 v25, 0x4812

    aput v25, v1, v24

    const/16 v24, -0x7cb8

    aput v24, v1, v23

    const/16 v23, -0x7d

    aput v23, v1, v22

    const/16 v22, -0x48

    aput v22, v1, v21

    const/16 v21, -0x6a

    aput v21, v1, v20

    const/16 v20, -0x1

    aput v20, v1, v19

    const/16 v19, 0x4763

    aput v19, v1, v18

    const/16 v18, 0x3347

    aput v18, v1, v17

    const/16 v17, -0x10cd

    aput v17, v1, v16

    const/16 v16, -0x11

    aput v16, v1, v15

    const/4 v15, -0x6

    aput v15, v1, v14

    const/16 v14, 0x3417

    aput v14, v1, v13

    const/16 v13, -0x4acc

    aput v13, v1, v12

    const/16 v12, -0x4b

    aput v12, v1, v11

    const/16 v11, -0x3694

    aput v11, v1, v10

    const/16 v10, -0x37

    aput v10, v1, v9

    const/16 v9, 0x3241

    aput v9, v1, v8

    const/16 v8, -0x6bce

    aput v8, v1, v7

    const/16 v7, -0x6c

    aput v7, v1, v6

    const/16 v6, -0x3b

    aput v6, v1, v5

    const/16 v5, -0x54

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_3

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isScanning:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$2900(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->stopDiscoveringSensorDevices()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$3800(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->IsJoining:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$2300(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x4f

    aput v12, v2, v11

    const/16 v11, -0x3f5

    aput v11, v2, v10

    const/16 v10, -0x6b

    aput v10, v2, v9

    const/16 v9, -0x26

    aput v9, v2, v8

    const/16 v8, 0x330

    aput v8, v2, v7

    const/16 v7, 0x2266

    aput v7, v2, v6

    const/16 v6, -0x688f

    aput v6, v2, v5

    const/16 v5, -0xb

    aput v5, v2, v4

    const/16 v4, 0x775a

    aput v4, v2, v3

    const/16 v3, 0x141b

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x2c

    aput v13, v1, v12

    const/16 v12, -0x398

    aput v12, v1, v11

    const/4 v11, -0x4

    aput v11, v1, v10

    const/16 v10, -0x54

    aput v10, v1, v9

    const/16 v9, 0x342

    aput v9, v1, v8

    const/16 v8, 0x2203

    aput v8, v1, v7

    const/16 v7, -0x68de

    aput v7, v1, v6

    const/16 v6, -0x69

    aput v6, v1, v5

    const/16 v5, 0x7733

    aput v5, v1, v4

    const/16 v4, 0x1477

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v4, v1

    if-lt v3, v4, :cond_5

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v4, v1

    if-lt v3, v4, :cond_6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->lc6AXr3()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->IsJoining:Z
    invoke-static {v3}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$2300(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :goto_6
    return-void

    :cond_1
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_2
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_3
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_4
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_5
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_6
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->val$item:Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;

    iget-object v2, v1, Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;->deviceId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->deviceMap:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1700(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v3

    :try_start_0
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v4, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v5, 0x2

    invoke-direct {v4, v1, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v5, Lcom/sec/android/app/shealth/framework/ui/R$string;->do_you_want_t0_connect_q:I

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->val$item:Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;

    iget-object v7, v7, Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;->deviceName:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-static {v1, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v1

    const/16 v5, 0x2718

    if-ne v1, v5, :cond_9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v1

    if-lez v1, :cond_9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_8
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceType()I

    move-result v6

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v7

    if-ne v6, v7, :cond_8

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->isConnected()Z

    move-result v6

    if-eqz v6, :cond_8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    const/4 v6, 0x1

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->checkIsHrmSwitchReq:Z
    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5102(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/sec/android/app/shealth/framework/ui/R$string;->connected_device_exist_msg:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->connectedHRM:Ljava/lang/String;
    invoke-static {v3, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5202(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Ljava/lang/String;)Ljava/lang/String;

    :cond_9
    :goto_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->checkIsHrmSwitchReq:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Z

    move-result v1

    if-eqz v1, :cond_a

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->connect_accessory:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    :goto_8
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15$1;

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15$2;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15$2;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;)V

    invoke-virtual {v4, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    const/4 v1, 0x7

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/16 v11, 0x1b2c

    aput v11, v2, v10

    const/16 v10, -0x2688

    aput v10, v2, v9

    const/16 v9, -0x44

    aput v9, v2, v8

    const/16 v8, 0x7430

    aput v8, v2, v7

    const/16 v7, -0x60e6

    aput v7, v2, v6

    const/16 v6, -0x10

    aput v6, v2, v3

    const/16 v3, -0x7b

    aput v3, v2, v1

    const/4 v1, 0x7

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/16 v12, 0x1b58

    aput v12, v1, v11

    const/16 v11, -0x26e5

    aput v11, v1, v10

    const/16 v10, -0x27

    aput v10, v1, v9

    const/16 v9, 0x745e

    aput v9, v1, v8

    const/16 v8, -0x608c

    aput v8, v1, v7

    const/16 v7, -0x61

    aput v7, v1, v6

    const/16 v6, -0x1a

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_9
    array-length v6, v1

    if-lt v3, v6, :cond_b

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_a
    array-length v6, v1

    if-lt v3, v6, :cond_c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v5, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->val$item:Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;->deviceName:Ljava/lang/String;

    invoke-virtual {v4, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    goto/16 :goto_8

    :cond_b
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    :cond_c
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    :catch_0
    move-exception v1

    goto/16 :goto_6

    :catch_1
    move-exception v1

    goto/16 :goto_7
.end method
