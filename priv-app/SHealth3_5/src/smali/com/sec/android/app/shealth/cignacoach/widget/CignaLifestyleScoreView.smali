.class public Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;
.super Landroid/widget/RelativeLayout;
.source "CignaLifestyleScoreView.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView$6;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAnimDisappearEffect:Landroid/view/animation/Animation;

.field private mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

.field private mCignaGetYourScoreLayout:Landroid/widget/LinearLayout;

.field private mCignaScoreLayout:Landroid/widget/LinearLayout;

.field private mCircleLayout:Landroid/widget/LinearLayout;

.field private mCircleProgressImage:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

.field private mContext:Landroid/content/Context;

.field private mCurrentScore:I

.field private mIsCurrentGoalView:Z

.field private mLifestyleTitleTxt:Landroid/widget/TextView;

.field private mScoreMessageLayout:Landroid/widget/LinearLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mIsCurrentGoalView:Z

    .line 33
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    .line 48
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mContext:Landroid/content/Context;

    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->initLayout()V

    .line 51
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mScoreMessageLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCignaScoreLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCircleLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private initLayout()V
    .locals 3

    .prologue
    .line 55
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mContext:Landroid/content/Context;

    const v2, 0x7f03006b

    invoke-static {v1, v2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 57
    .local v0, "socreView":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07004f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->setBackgroundColor(I)V

    .line 59
    const v1, 0x7f0801f7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCircleLayout:Landroid/widget/LinearLayout;

    .line 60
    const v1, 0x7f0801f8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCircleProgressImage:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    .line 61
    const v1, 0x7f0801fc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mLifestyleTitleTxt:Landroid/widget/TextView;

    .line 62
    const v1, 0x7f0801fb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCignaScoreLayout:Landroid/widget/LinearLayout;

    .line 63
    const v1, 0x7f0801f9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCignaGetYourScoreLayout:Landroid/widget/LinearLayout;

    .line 64
    const v1, 0x7f0801fa

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mScoreMessageLayout:Landroid/widget/LinearLayout;

    .line 68
    return-void
.end method

.method private updateProgressBar(F)V
    .locals 2
    .param p1, "percent"    # F

    .prologue
    .line 81
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCircleProgressImage:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCircleProgressImage:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->setScore(I)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCircleProgressImage:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v1, p1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->setProgressWithAnimation(I)V

    goto :goto_0
.end method

.method private updateProgressColorBar(F)V
    .locals 2
    .param p1, "percent"    # F

    .prologue
    .line 93
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCircleProgressImage:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCircleProgressImage:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->setScoreForColor(I)V

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCircleProgressImage:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v1, p1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->setProgressWithAnimation(I)V

    goto :goto_0
.end method


# virtual methods
.method public clearScoreMessageAnimation()V
    .locals 2

    .prologue
    .line 311
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mScoreMessageLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCignaScoreLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mScoreMessageLayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 314
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mAnimDisappearEffect:Landroid/view/animation/Animation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mAnimDisappearEffect:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mAnimDisappearEffect:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    .line 317
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mScoreMessageLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->clearAnimation()V

    .line 319
    :cond_1
    return-void
.end method

.method public drawCircleProgress()V
    .locals 3

    .prologue
    .line 230
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    int-to-float v1, v1

    const/high16 v2, 0x42c80000    # 100.0f

    div-float v0, v1, v2

    .line 231
    .local v0, "intakeToGoalCalorieRatio":F
    const v1, 0x7f0801f8

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 232
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->updateProgressBar(F)V

    .line 233
    return-void
.end method

.method public getCurrentScore()I
    .locals 1

    .prologue
    .line 333
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    return v0
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 300
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mAnimDisappearEffect:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_1

    .line 301
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mScoreMessageLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 302
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCignaScoreLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mScoreMessageLayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCircleLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 306
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mScoreMessageLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->clearAnimation()V

    .line 308
    :cond_1
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 367
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 373
    return-void
.end method

.method public selectScoreViewMessageMode(Z)V
    .locals 3
    .param p1, "isMesssageMode"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 354
    if-eqz p1, :cond_0

    .line 355
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCignaScoreLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 356
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mScoreMessageLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 361
    :goto_0
    return-void

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCignaScoreLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 359
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mScoreMessageLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public setBackgroundCircleProgressImg(I)V
    .locals 1
    .param p1, "resid"    # I

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCircleProgressImage:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->setBackgroundResource(I)V

    .line 73
    return-void
.end method

.method public setBackgroundImageByCategory(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V
    .locals 8
    .param p1, "categoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .prologue
    const/16 v7, 0x64

    const/16 v6, 0x47

    const/16 v5, 0x46

    const/16 v4, 0x1f

    const/16 v3, 0x1e

    .line 103
    const/4 v0, -0x1

    .line 105
    .local v0, "resId":I
    if-nez p1, :cond_1

    .line 106
    const v0, 0x7f020677

    .line 108
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCircleProgressImage:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->setBackgroundResource(I)V

    .line 175
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView$6;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    invoke-virtual {p1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 162
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-ltz v1, :cond_d

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-ge v1, v4, :cond_d

    .line 163
    const v0, 0x7f020679

    .line 172
    :cond_2
    :goto_1
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 173
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCircleProgressImage:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->setBackgroundResource(I)V

    goto :goto_0

    .line 117
    :pswitch_0
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-ltz v1, :cond_3

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-ge v1, v4, :cond_3

    .line 118
    const v0, 0x7f020683

    goto :goto_1

    .line 119
    :cond_3
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-le v1, v3, :cond_4

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-ge v1, v6, :cond_4

    .line 120
    const v0, 0x7f020682

    goto :goto_1

    .line 121
    :cond_4
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-le v1, v5, :cond_2

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-gt v1, v7, :cond_2

    .line 122
    const v0, 0x7f020680

    goto :goto_1

    .line 126
    :pswitch_1
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-ltz v1, :cond_5

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-ge v1, v4, :cond_5

    .line 127
    const v0, 0x7f02068e

    goto :goto_1

    .line 128
    :cond_5
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-le v1, v3, :cond_6

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-ge v1, v6, :cond_6

    .line 129
    const v0, 0x7f02068d

    goto :goto_1

    .line 130
    :cond_6
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-le v1, v5, :cond_2

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-gt v1, v7, :cond_2

    .line 131
    const v0, 0x7f02068a

    goto :goto_1

    .line 135
    :pswitch_2
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-ltz v1, :cond_7

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-ge v1, v4, :cond_7

    .line 136
    const v0, 0x7f0206e2

    goto :goto_1

    .line 137
    :cond_7
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-le v1, v3, :cond_8

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-ge v1, v6, :cond_8

    .line 138
    const v0, 0x7f0206e1

    goto :goto_1

    .line 139
    :cond_8
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-le v1, v5, :cond_2

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-gt v1, v7, :cond_2

    .line 140
    const v0, 0x7f0206e0

    goto :goto_1

    .line 144
    :pswitch_3
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-ltz v1, :cond_9

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-ge v1, v4, :cond_9

    .line 145
    const v0, 0x7f02067c

    goto :goto_1

    .line 146
    :cond_9
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-le v1, v3, :cond_a

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-ge v1, v6, :cond_a

    .line 147
    const v0, 0x7f02067b

    goto/16 :goto_1

    .line 148
    :cond_a
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-le v1, v5, :cond_2

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-gt v1, v7, :cond_2

    .line 149
    const v0, 0x7f02067a

    goto/16 :goto_1

    .line 153
    :pswitch_4
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-ltz v1, :cond_b

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-ge v1, v4, :cond_b

    .line 154
    const v0, 0x7f0206eb

    goto/16 :goto_1

    .line 155
    :cond_b
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-le v1, v3, :cond_c

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-ge v1, v6, :cond_c

    .line 156
    const v0, 0x7f0206ea

    goto/16 :goto_1

    .line 157
    :cond_c
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-le v1, v5, :cond_2

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-gt v1, v7, :cond_2

    .line 158
    const v0, 0x7f0206e9

    goto/16 :goto_1

    .line 164
    :cond_d
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-le v1, v3, :cond_e

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-ge v1, v6, :cond_e

    .line 165
    const v0, 0x7f020678

    goto/16 :goto_1

    .line 166
    :cond_e
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-le v1, v5, :cond_2

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    if-gt v1, v7, :cond_2

    .line 167
    const v0, 0x7f020677

    goto/16 :goto_1

    .line 115
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setCurrentScore(I)V
    .locals 2
    .param p1, "score"    # I

    .prologue
    .line 225
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCircleProgressImage:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->setScore(I)V

    .line 227
    return-void
.end method

.method public setCurrentScoreInfo(ILcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "score"    # I
    .param p2, "category"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .param p3, "outOfMessage"    # Ljava/lang/String;
    .param p4, "coachMessage"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/16 v5, 0x8

    .line 178
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCurrentScore:I

    .line 179
    iput-object p2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 181
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->setBackgroundImageByCategory(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V

    .line 183
    const v1, 0x7f0801fd

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-gez p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090335

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 185
    if-gez p1, :cond_1

    .line 186
    const/4 p1, 0x0

    .line 188
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCignaGetYourScoreLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 189
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCignaGetYourScoreLayout:Landroid/widget/LinearLayout;

    const v2, 0x7f0801e6

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090c45

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09020b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09020a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 190
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCignaScoreLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 191
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mScoreMessageLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 203
    :goto_1
    int-to-float v1, p1

    const/high16 v2, 0x42c80000    # 100.0f

    div-float v0, v1, v2

    .line 205
    .local v0, "intakeToGoalCalorieRatio":F
    if-nez p2, :cond_3

    .line 206
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->updateProgressBar(F)V

    .line 210
    :goto_2
    return-void

    .line 183
    .end local v0    # "intakeToGoalCalorieRatio":F
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 193
    :cond_1
    if-eqz p4, :cond_2

    .line 194
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mScoreMessageLayout:Landroid/widget/LinearLayout;

    const v2, 0x7f0801f6

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCignaGetYourScoreLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 197
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCignaScoreLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1

    .line 208
    .restart local v0    # "intakeToGoalCalorieRatio":F
    :cond_3
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->updateProgressColorBar(F)V

    goto :goto_2
.end method

.method public setCustomHeaderMode()V
    .locals 2

    .prologue
    .line 323
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCircleLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView$5;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 330
    return-void
.end method

.method public setLifestyleTitleTxt(I)V
    .locals 2
    .param p1, "titleId"    # I

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mLifestyleTitleTxt:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    return-void
.end method

.method public setScoreCoachMessage(Ljava/lang/String;)V
    .locals 2
    .param p1, "coachMessage"    # Ljava/lang/String;

    .prologue
    .line 213
    if-eqz p1, :cond_0

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mScoreMessageLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f0801f6

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    :cond_0
    return-void
.end method

.method public setSummaryHeaderMode(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;ZZZ)V
    .locals 3
    .param p1, "activity"    # Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;
    .param p2, "switchModeEnable"    # Z
    .param p3, "isCurrentGoalView"    # Z
    .param p4, "isMainScoreView"    # Z

    .prologue
    .line 240
    iput-boolean p3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mIsCurrentGoalView:Z

    .line 241
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCignaScoreLayout:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView$1;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 249
    if-eqz p2, :cond_0

    .line 250
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCircleLayout:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 258
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mIsCurrentGoalView:Z

    if-eqz v1, :cond_1

    .line 259
    const v1, 0x7f0801e6

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 260
    .local v0, "getYourScoreBtn":Landroid/widget/LinearLayout;
    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView$3;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView$3;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 270
    .end local v0    # "getYourScoreBtn":Landroid/widget/LinearLayout;
    :cond_1
    if-eqz p4, :cond_2

    .line 271
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->clearScoreMessageAnimation()V

    .line 272
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->switchScoreRightLayout()V

    .line 274
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f040002

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mAnimDisappearEffect:Landroid/view/animation/Animation;

    .line 276
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mAnimDisappearEffect:Landroid/view/animation/Animation;

    invoke-virtual {v1, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 278
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mScoreMessageLayout:Landroid/widget/LinearLayout;

    const v2, 0x7f0801f6

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f09032d

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 279
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCircleLayout:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView$4;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 292
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mScoreMessageLayout:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mAnimDisappearEffect:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 295
    :cond_2
    return-void
.end method

.method public switchScoreRightLayout()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mScoreMessageLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCignaScoreLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 340
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mScoreMessageLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 341
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mIsCurrentGoalView:Z

    if-eqz v0, :cond_0

    .line 342
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->setisCignaCoachScoreViewMessageMode(Z)V

    .line 351
    :cond_0
    :goto_0
    return-void

    .line 344
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mScoreMessageLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCignaScoreLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 346
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mScoreMessageLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 347
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mIsCurrentGoalView:Z

    if-eqz v0, :cond_0

    .line 348
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->setisCignaCoachScoreViewMessageMode(Z)V

    goto :goto_0
.end method
