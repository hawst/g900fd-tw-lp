.class Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity$2;
.super Ljava/lang/Object;
.source "ReEngagingMissionsDetailActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->showView(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 120
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 121
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 122
    const-string v1, "EXTRA_NAME_GOAL_ID"

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->mFailedGoalId:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->access$000(Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 123
    const-string v1, "EXTRA_NAME_MISSION_ID"

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->mFailedMissionId:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->access$100(Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 124
    const-string v1, "EXTRA_NAME_MISSION_FROM_WHERE"

    const/16 v2, 0x16

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 125
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->startActivity(Landroid/content/Intent;)V

    .line 126
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->finish()V

    .line 127
    return-void
.end method
