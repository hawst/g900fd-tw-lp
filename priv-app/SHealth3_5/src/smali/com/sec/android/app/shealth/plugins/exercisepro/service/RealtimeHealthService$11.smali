.class Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$11;
.super Ljava/lang/Object;
.source "RealtimeHealthService.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V
    .locals 0

    .prologue
    .line 1636
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$11;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onJoined(I)V
    .locals 7
    .param p1, "error"    # I

    .prologue
    .line 1662
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$11;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mWorkoutState:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)I

    move-result v3

    const/16 v4, 0x7d1

    if-ne v3, v4, :cond_0

    .line 1663
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$11;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->updateBSACDataState()V
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$2600(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V

    .line 1665
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$11;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setBSACStatus(I)V

    .line 1666
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$11;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Ljava/util/ArrayList;

    move-result-object v4

    monitor-enter v4
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2

    .line 1667
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$11;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1669
    .local v2, "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :try_start_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$11;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mBSACDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$2500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x2

    const/4 v6, 0x3

    invoke-interface {v2, v3, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onChannelStateChanged(Ljava/lang/String;II)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1671
    :catch_0
    move-exception v0

    .line 1672
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v3

    const-string v5, "mBSACEventListener : onJoined.onChannelStateChanged ->Exception"

    invoke-static {v3, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1675
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v3
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_2

    .line 1676
    :catch_1
    move-exception v0

    .line 1677
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v3

    const-string v4, "mBSACEventListener : onJoined ->IllegalArgumentException"

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1681
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :goto_1
    return-void

    .line 1675
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 1678
    .end local v1    # "i$":Ljava/util/Iterator;
    :catch_2
    move-exception v0

    .line 1679
    .local v0, "e":Ljava/lang/IllegalStateException;
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v3

    const-string v4, "mBSACEventListener : onJoined ->IllegalStateException"

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onLeft(I)V
    .locals 7
    .param p1, "error"    # I

    .prologue
    const/4 v4, 0x0

    .line 1646
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$11;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setBSACStatus(I)V

    .line 1648
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$11;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Ljava/util/ArrayList;

    move-result-object v4

    monitor-enter v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1649
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$11;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mListListener:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;

    .line 1650
    .local v2, "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$11;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mBSACDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$2500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v6, 0x3

    invoke-interface {v2, v3, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;->onChannelStateChanged(Ljava/lang/String;II)V

    goto :goto_0

    .line 1651
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "listener":Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v3
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 1653
    :catch_0
    move-exception v0

    .line 1654
    .local v0, "e":Ljava/lang/Exception;
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v3

    const-string v4, "mBSACEventListener : onLeft ->Exception"

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1656
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void

    .line 1651
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public onResponseReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;)V
    .locals 3
    .param p1, "command"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;
    .param p2, "response"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;

    .prologue
    .line 1640
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Response --------- commandId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;->getCommandId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " errorCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;->getErrorCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " errorDescription:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;->getErrorDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1641
    return-void
.end method

.method public onStateChanged(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 1686
    return-void
.end method
