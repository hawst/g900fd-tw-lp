.class final enum Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;
.super Ljava/lang/Enum;
.source "GradationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "GradationType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;

.field public static final enum TYPE_BIG:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;

.field public static final enum TYPE_MIDDLE:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;

.field public static final enum TYPE_SMALL:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;


# instance fields
.field public final drawText:Z

.field public final lengthResId:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 127
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;

    const-string v1, "TYPE_SMALL"

    sget v2, Lcom/sec/android/app/shealth/common/commonui/R$dimen;->gradationview_horizontal_small_gradationbar_height:I

    invoke-direct {v0, v1, v3, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;->TYPE_SMALL:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;

    .line 128
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;

    const-string v1, "TYPE_MIDDLE"

    sget v2, Lcom/sec/android/app/shealth/common/commonui/R$dimen;->gradationview_horizontal_middle_gradationbar_height:I

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;->TYPE_MIDDLE:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;

    .line 129
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;

    const-string v1, "TYPE_BIG"

    sget v2, Lcom/sec/android/app/shealth/common/commonui/R$dimen;->gradationview_horizontal_big_gradationbar_height:I

    invoke-direct {v0, v1, v5, v2, v4}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;->TYPE_BIG:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;

    .line 126
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;

    sget-object v1, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;->TYPE_SMALL:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;->TYPE_MIDDLE:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;->TYPE_BIG:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;->$VALUES:[Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIZ)V
    .locals 0
    .param p3, "lengthResId"    # I
    .param p4, "drawText"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)V"
        }
    .end annotation

    .prologue
    .line 134
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 135
    iput p3, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;->lengthResId:I

    .line 136
    iput-boolean p4, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;->drawText:Z

    .line 137
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 126
    const-class v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;
    .locals 1

    .prologue
    .line 126
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;->$VALUES:[Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;

    return-object v0
.end method
