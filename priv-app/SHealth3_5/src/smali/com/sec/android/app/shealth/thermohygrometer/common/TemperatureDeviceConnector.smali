.class public Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;
.super Ljava/lang/Object;
.source "TemperatureDeviceConnector.java"


# static fields
.field private static final MEASURING_TIMEOUT:I = 0xea60

.field private static TAG:Ljava/lang/String;

.field private static mInstance:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;


# instance fields
.field private IsDeviceStarted:Z

.field isCountDownStarted:Z

.field public isRecord:Z

.field private mContext:Landroid/content/Context;

.field private mCountDownTimer:Landroid/os/CountDownTimer;

.field mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

.field private mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

.field mEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

.field private mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

.field mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

.field private mTempSensorListener:Lcom/sec/android/app/shealth/thermohygrometer/TempSensorListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->IsDeviceStarted:Z

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mContext:Landroid/content/Context;

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->isRecord:Z

    .line 280
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$1;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    .line 355
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$2;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    .line 392
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$3;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    .line 467
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->isCountDownStarted:Z

    .line 468
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$4;

    const-wide/32 v2, 0xea60

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$4;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;JJ)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mCountDownTimer:Landroid/os/CountDownTimer;

    .line 49
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->IsDeviceStarted:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->IsDeviceStarted:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->startDevice()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;)Lcom/sec/android/app/shealth/thermohygrometer/TempSensorListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mTempSensorListener:Lcom/sec/android/app/shealth/thermohygrometer/TempSensorListener;

    return-object v0
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mInstance:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mInstance:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    .line 43
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mInstance:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    return-object v0
.end method

.method private initSensor()V
    .locals 0

    .prologue
    .line 140
    return-void
.end method

.method private scanDevice()V
    .locals 9

    .prologue
    const/16 v8, 0x272c

    .line 57
    sget-object v4, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "scanDevice"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-eqz v4, :cond_0

    .line 63
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    const/4 v5, 0x0

    const/16 v6, 0x272c

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->getConnectedDeviceList(III)Ljava/util/List;

    move-result-object v0

    .line 64
    .local v0, "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    if-eqz v0, :cond_4

    .line 65
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 66
    sget-object v4, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;

    const-string v5, "deviceList == isEmpty"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    const/4 v5, 0x0

    const/16 v6, 0x103

    invoke-interface {v4, v5, v6}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;->onStopped(II)V

    .line 104
    .end local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    :cond_0
    :goto_0
    return-void

    .line 69
    .restart local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    :cond_1
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 70
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 71
    .local v1, "dvc":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_2

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v4

    if-ne v4, v8, :cond_2

    .line 73
    iput-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 74
    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v5, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->join(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;)V

    .line 75
    sget-object v4, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "scanDevice() device: found- "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getProtocol()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_5

    goto :goto_1

    .line 88
    .end local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .end local v1    # "dvc":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 89
    .local v2, "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 77
    .end local v2    # "e":Landroid/os/RemoteException;
    .restart local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .restart local v1    # "dvc":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_1
    sget-object v4, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "scanDevice() device: else - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getProtocol()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_1 .. :try_end_1} :catch_5

    goto/16 :goto_1

    .line 90
    .end local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .end local v1    # "dvc":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_1
    move-exception v2

    .line 91
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto/16 :goto_0

    .line 80
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_3
    :try_start_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-nez v4, :cond_0

    .line 81
    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    const/4 v5, 0x0

    const/16 v6, 0x103

    invoke-interface {v4, v5, v6}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;->onStopped(II)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_2 .. :try_end_2} :catch_5

    goto/16 :goto_0

    .line 92
    .end local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_2
    move-exception v2

    .line 93
    .local v2, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 85
    .end local v2    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    .restart local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    :cond_4
    :try_start_3
    sget-object v4, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;

    const-string v5, "deviceList == null"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    const/4 v5, 0x0

    const/16 v6, 0x103

    invoke-interface {v4, v5, v6}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;->onStopped(II)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_3 .. :try_end_3} :catch_5

    goto/16 :goto_0

    .line 94
    .end local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    :catch_3
    move-exception v2

    .line 95
    .local v2, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;->printStackTrace()V

    goto/16 :goto_0

    .line 96
    .end local v2    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    :catch_4
    move-exception v2

    .line 98
    .local v2, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto/16 :goto_0

    .line 99
    .end local v2    # "e":Ljava/lang/IllegalStateException;
    :catch_5
    move-exception v2

    .line 101
    .local v2, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private startDevice()V
    .locals 4

    .prologue
    .line 155
    sget-object v1, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startDevice"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-nez v1, :cond_0

    .line 158
    sget-object v1, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;

    const-string v2, "SHealthDeviceFinder is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 190
    :goto_0
    return-void

    .line 161
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-nez v1, :cond_1

    .line 162
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->scanDevice()V

    goto :goto_0

    .line 165
    :cond_1
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->IsDeviceStarted:Z

    if-nez v1, :cond_2

    .line 166
    sget-object v1, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startReceivingData"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->startReceivingData(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;)V

    .line 168
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->isRecord:Z

    if-eqz v1, :cond_2

    .line 169
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->startRecord()V

    .line 172
    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->IsDeviceStarted:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_5

    goto :goto_0

    .line 173
    :catch_0
    move-exception v0

    .line 174
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 175
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 176
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 177
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 178
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 179
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 180
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 181
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_4
    move-exception v0

    .line 183
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;->printStackTrace()V

    goto :goto_0

    .line 184
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    :catch_5
    move-exception v0

    .line 186
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0
.end method

.method private stopDevice()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 225
    sget-object v2, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "stopDevice"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-eqz v2, :cond_0

    .line 228
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->stopReceivingData()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_4

    .line 243
    :goto_0
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->leave()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_8

    .line 254
    :goto_1
    iput-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 257
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-eqz v2, :cond_1

    .line 259
    :try_start_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->stopScan()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_2 .. :try_end_2} :catch_a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_b

    .line 269
    :goto_2
    :try_start_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->close()V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_c
    .catch Ljava/util/ConcurrentModificationException; {:try_start_3 .. :try_end_3} :catch_d

    .line 275
    :goto_3
    iput-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 277
    :cond_1
    return-void

    .line 229
    :catch_0
    move-exception v1

    .line 230
    .local v1, "e1":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 231
    .end local v1    # "e1":Landroid/os/RemoteException;
    :catch_1
    move-exception v1

    .line 232
    .local v1, "e1":Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 233
    .end local v1    # "e1":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v1

    .line 234
    .local v1, "e1":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 235
    .end local v1    # "e1":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_3
    move-exception v0

    .line 236
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 237
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_4
    move-exception v0

    .line 239
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0

    .line 244
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :catch_5
    move-exception v0

    .line 245
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 246
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_6
    move-exception v0

    .line 247
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_1

    .line 248
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_7
    move-exception v0

    .line 249
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1

    .line 250
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_8
    move-exception v0

    .line 251
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 260
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_9
    move-exception v1

    .line 261
    .local v1, "e1":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 262
    .end local v1    # "e1":Landroid/os/RemoteException;
    :catch_a
    move-exception v1

    .line 263
    .local v1, "e1":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_2

    .line 264
    .end local v1    # "e1":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_b
    move-exception v0

    .line 265
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    .line 270
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_c
    move-exception v0

    .line 271
    .restart local v0    # "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_3

    .line 272
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_d
    move-exception v0

    .line 273
    .local v0, "e":Ljava/util/ConcurrentModificationException;
    invoke-virtual {v0}, Ljava/util/ConcurrentModificationException;->printStackTrace()V

    goto :goto_3
.end method


# virtual methods
.method public isStarted()Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->IsDeviceStarted:Z

    return v0
.end method

.method public isStripConnected()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 484
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-eqz v0, :cond_0

    .line 488
    :cond_0
    return v1
.end method

.method public startMeasuring(Landroid/content/Context;Lcom/sec/android/app/shealth/thermohygrometer/TempSensorListener;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/sec/android/app/shealth/thermohygrometer/TempSensorListener;
    .param p3, "isRecordON"    # Z

    .prologue
    .line 108
    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "startMeasuring : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    iput-object p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mContext:Landroid/content/Context;

    .line 110
    if-eqz p2, :cond_0

    .line 111
    iput-object p2, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mTempSensorListener:Lcom/sec/android/app/shealth/thermohygrometer/TempSensorListener;

    .line 113
    :cond_0
    iput-boolean p3, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->isRecord:Z

    .line 114
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->startDevice()V

    .line 115
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->initSensor()V

    .line 116
    return-void
.end method

.method public startRecord()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 195
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->record()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_5

    .line 215
    :goto_0
    return-void

    .line 196
    :catch_0
    move-exception v0

    .line 198
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 199
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 201
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 202
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 204
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 205
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 207
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 208
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_4
    move-exception v0

    .line 210
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0

    .line 211
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :catch_5
    move-exception v0

    .line 213
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;->printStackTrace()V

    goto :goto_0
.end method

.method public stopMeasuring()V
    .locals 2

    .prologue
    .line 218
    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "stopMeasuring"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->stopDevice()V

    .line 221
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->IsDeviceStarted:Z

    .line 222
    return-void
.end method
