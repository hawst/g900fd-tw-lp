.class public Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$GoalData;
.super Ljava/lang/Object;
.source "PedometerUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GoalData"
.end annotation


# instance fields
.field private set_time:J

.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

.field private value:F


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;FJ)V
    .locals 0
    .param p2, "value"    # F
    .param p3, "set_time"    # J

    .prologue
    .line 235
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$GoalData;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 236
    iput p2, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$GoalData;->value:F

    .line 237
    iput-wide p3, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$GoalData;->set_time:J

    .line 238
    return-void
.end method


# virtual methods
.method public getSetTime()J
    .locals 2

    .prologue
    .line 249
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$GoalData;->set_time:J

    return-wide v0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 241
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$GoalData;->value:F

    return v0
.end method

.method public setSetTime(I)V
    .locals 2
    .param p1, "set_time"    # I

    .prologue
    .line 253
    int-to-long v0, p1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$GoalData;->set_time:J

    .line 254
    return-void
.end method

.method public setValue(F)V
    .locals 0
    .param p1, "value"    # F

    .prologue
    .line 245
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$GoalData;->value:F

    .line 246
    return-void
.end method
