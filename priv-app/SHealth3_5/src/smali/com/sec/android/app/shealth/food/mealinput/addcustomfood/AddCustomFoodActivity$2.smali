.class Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$2;
.super Ljava/lang/Object;
.source "AddCustomFoodActivity.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->findViews()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)V
    .locals 0

    .prologue
    .line 277
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "focused"    # Z

    .prologue
    const/16 v2, 0x8

    .line 282
    if-eqz p2, :cond_2

    .line 283
    if-eqz p2, :cond_0

    instance-of v1, p1, Landroid/widget/EditText;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 284
    check-cast v0, Landroid/widget/EditText;

    .line 285
    .local v0, "et":Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 287
    .end local v0    # "et":Landroid/widget/EditText;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientFat:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->access$100(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getVisibility()I

    move-result v1

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientFat:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->access$100(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->getVisibility()I

    move-result v1

    if-ne v1, v2, :cond_2

    .line 288
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientFat:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->access$100(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->clearFocus()V

    .line 289
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientCarbo:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->access$200(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_3

    .line 290
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientCarbo:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->access$200(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->requestFocus()Z

    .line 300
    :cond_2
    :goto_0
    return-void

    .line 293
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientProtein:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->access$300(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_2

    .line 294
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mNutrientProtein:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->access$300(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->requestFocus()Z

    goto :goto_0
.end method
