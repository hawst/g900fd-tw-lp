.class public Lcom/sec/android/app/shealth/common/utils/SamsungAccount;
.super Ljava/lang/Object;
.source "SamsungAccount.java"


# static fields
.field public static final ACCESS_TOKEN_REQUEST:Ljava/lang/String; = "com.msc.action.samsungaccount.REQUEST_ACCESSTOKEN"

.field public static final ACCESS_TOKEN_REQUEST_CODE:I = 0xa26

.field public static final ACCESS_TOKEN_RESPONSE:Ljava/lang/String; = "com.msc.action.ACCESSTOKEN_V02_RESPONSE"

.field public static final ADD_SAMSUNG_ACCOUNT:Ljava/lang/String; = "com.osp.app.signin.action.ADD_SAMSUNG_ACCOUNT"

.field public static final CHECK_VALIDATION_REQUEST:Ljava/lang/String; = "com.msc.action.samsungaccount.REQUEST_CHECKLIST_VALIDATION"

.field public static final CHECK_VALIDATION_REQUEST_CODE:I = 0xa25

.field public static final CLIENT_ID:Ljava/lang/String; = "1y90e30264"

.field public static final CLIENT_SECRET:Ljava/lang/String; = "80E7ECD9D301CB7888C73703639302E5"

.field public static final OSP_VER:Ljava/lang/String; = "OSP_02"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 67
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 69
    .local v1, "manager":Landroid/accounts/AccountManager;
    if-nez v1, :cond_1

    .line 77
    :cond_0
    :goto_0
    return-object v2

    .line 72
    :cond_1
    const-string v3, "com.osp.app.signin"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 74
    .local v0, "accountArr":[Landroid/accounts/Account;
    if-eqz v0, :cond_0

    array-length v3, v0

    if-eqz v3, :cond_0

    .line 75
    const/4 v2, 0x0

    aget-object v2, v0, v2

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_0
.end method

.method public static getSamsungAccountObject(Landroid/content/Context;)Landroid/accounts/Account;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 82
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 84
    .local v1, "manager":Landroid/accounts/AccountManager;
    if-nez v1, :cond_1

    .line 92
    :cond_0
    :goto_0
    return-object v2

    .line 87
    :cond_1
    const-string v3, "com.osp.app.signin"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 89
    .local v0, "accountArr":[Landroid/accounts/Account;
    if-eqz v0, :cond_0

    array-length v3, v0

    if-eqz v3, :cond_0

    .line 90
    const/4 v2, 0x0

    aget-object v2, v0, v2

    goto :goto_0
.end method

.method public static isDeviceSignInSHealthAccount(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 107
    const-string v0, "com.samsung.health.auth"

    .line 108
    .local v0, "ACCOUNT_TYPE_HEALTH_SERVICE":Ljava/lang/String;
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 110
    .local v2, "mAccountManager":Landroid/accounts/AccountManager;
    if-nez v2, :cond_0

    .line 123
    :goto_0
    return v3

    .line 114
    :cond_0
    invoke-virtual {v2, v0}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 115
    .local v1, "accounts":[Landroid/accounts/Account;
    array-length v4, v1

    if-lez v4, :cond_1

    .line 117
    sget-object v3, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Account Present of type"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    const/4 v3, 0x1

    goto :goto_0

    .line 122
    :cond_1
    sget-object v4, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "NO ACCOUNT WITH TYPE "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isDeviceSignInSamsungAccount(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 51
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 52
    .local v1, "manager":Landroid/accounts/AccountManager;
    if-nez v1, :cond_1

    .line 55
    :cond_0
    :goto_0
    return v2

    .line 54
    :cond_1
    const-string v3, "com.osp.app.signin"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 55
    .local v0, "accountArr":[Landroid/accounts/Account;
    array-length v3, v0

    if-lez v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static requestAccessToken(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 152
    :try_start_0
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.msc.action.samsungaccount.REQUEST_ACCESSTOKEN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 153
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "client_id"

    const-string v4, "1y90e30264"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 154
    const-string v3, "client_secret"

    const-string v4, "80E7ECD9D301CB7888C73703639302E5"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 155
    const-string/jumbo v3, "progress_theme"

    const-string/jumbo v4, "white"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 156
    move-object v0, p0

    check-cast v0, Landroid/app/Activity;

    move-object v3, v0

    const/16 v4, 0xa26

    invoke-virtual {v3, v2, v4}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 160
    .end local v2    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 157
    :catch_0
    move-exception v1

    .line 158
    .local v1, "e":Ljava/lang/IllegalStateException;
    sget v3, Lcom/sec/android/app/shealth/common/utils/R$string;->installing_updates:I

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {p0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public static requestCheckValication(Landroid/content/Context;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x0

    .line 129
    :try_start_0
    new-instance v3, Landroid/content/Intent;

    const-string v5, "com.msc.action.samsungaccount.REQUEST_CHECKLIST_VALIDATION"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 130
    .local v3, "intent":Landroid/content/Intent;
    const-string v5, "client_id"

    const-string v6, "1y90e30264"

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 131
    const-string v5, "client_secret"

    const-string v6, "80E7ECD9D301CB7888C73703639302E5"

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 132
    const-string/jumbo v5, "progress_theme"

    const-string/jumbo v6, "white"

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 133
    const-string/jumbo v5, "validation_result_only"

    const/4 v6, 0x1

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 136
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 137
    .local v4, "packageManager":Landroid/content/pm/PackageManager;
    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 138
    .local v1, "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 139
    move-object v0, p0

    check-cast v0, Landroid/app/Activity;

    move-object v5, v0

    const/16 v6, 0xa25

    invoke-virtual {v5, v3, v6}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 147
    .end local v1    # "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v4    # "packageManager":Landroid/content/pm/PackageManager;
    :goto_0
    return-void

    .line 141
    .restart local v1    # "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .restart local v3    # "intent":Landroid/content/Intent;
    .restart local v4    # "packageManager":Landroid/content/pm/PackageManager;
    :cond_0
    sget-object v5, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->TAG:Ljava/lang/String;

    const-string v6, "PackageManager failed to resolve action - com.msc.action.samsungaccount.REQUEST_CHECKLIST_VALIDATION "

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 144
    .end local v1    # "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v4    # "packageManager":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v2

    .line 145
    .local v2, "e":Ljava/lang/IllegalStateException;
    sget v5, Lcom/sec/android/app/shealth/common/utils/R$string;->installing_updates:I

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
