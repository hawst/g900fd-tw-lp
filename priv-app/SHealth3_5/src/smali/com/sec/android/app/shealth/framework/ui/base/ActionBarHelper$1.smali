.class Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->createActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mIsDrwaMenuEnabled:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->access$000(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mIsDrawMenuOpen:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarListener:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$ActionBarListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$ActionBarListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$ActionBarListener;->hideDrawMenu()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarListener:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$ActionBarListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$ActionBarListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$ActionBarListener;->showDrawMenu()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mUpButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->access$300(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->access$200(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->navigate_up:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarListener:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$ActionBarListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$ActionBarListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$ActionBarListener;->closeScreen()V

    goto :goto_0
.end method
