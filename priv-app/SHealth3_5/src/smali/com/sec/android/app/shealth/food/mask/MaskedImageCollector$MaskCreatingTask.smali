.class Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;
.super Landroid/os/AsyncTask;
.source "MaskedImageCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MaskCreatingTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/sec/android/app/shealth/food/mask/MaskTools;",
        ">;"
    }
.end annotation


# instance fields
.field private mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$OnFinishCreatingMaskListener;",
            ">;"
        }
    .end annotation
.end field

.field private mMaskId:I

.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;I)V
    .locals 1
    .param p2, "maskId"    # I

    .prologue
    .line 137
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;->this$0:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 138
    iput p2, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;->mMaskId:I

    .line 139
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;->mListeners:Ljava/util/List;

    .line 140
    return-void
.end method


# virtual methods
.method addOnFinishCreatingListener(Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$OnFinishCreatingMaskListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$OnFinishCreatingMaskListener;

    .prologue
    .line 143
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;->this$0:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

    # getter for: Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->mMaskToolsArray:Landroid/util/SparseArray;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->access$300(Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;)Landroid/util/SparseArray;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;->mMaskId:I

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/mask/MaskTools;

    .line 144
    .local v0, "maskTools":Lcom/sec/android/app/shealth/food/mask/MaskTools;
    if-eqz v0, :cond_0

    .line 145
    invoke-interface {p1, v0}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$OnFinishCreatingMaskListener;->onFinishCreatingMask(Lcom/sec/android/app/shealth/food/mask/MaskTools;)V

    .line 149
    :goto_0
    return-void

    .line 147
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;->mListeners:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/sec/android/app/shealth/food/mask/MaskTools;
    .locals 2
    .param p1, "voids"    # [Ljava/lang/Void;

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;->this$0:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

    # getter for: Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->access$400(Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;->mMaskId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->convertDrawableIntoBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mask/MaskTools;->createDefaultMaskTools(Landroid/graphics/Bitmap;)Lcom/sec/android/app/shealth/food/mask/MaskTools;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 133
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;->doInBackground([Ljava/lang/Void;)Lcom/sec/android/app/shealth/food/mask/MaskTools;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/sec/android/app/shealth/food/mask/MaskTools;)V
    .locals 4
    .param p1, "maskTools"    # Lcom/sec/android/app/shealth/food/mask/MaskTools;

    .prologue
    .line 159
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;->this$0:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

    # getter for: Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->mMaskToolsArray:Landroid/util/SparseArray;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->access$300(Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;)Landroid/util/SparseArray;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;->mMaskId:I

    invoke-virtual {v2, v3, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 160
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;->this$0:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

    # getter for: Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->mMaskCreatingTasks:Landroid/util/SparseArray;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->access$500(Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;)Landroid/util/SparseArray;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;->mMaskId:I

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->remove(I)V

    .line 161
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$OnFinishCreatingMaskListener;

    .line 162
    .local v1, "listener":Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$OnFinishCreatingMaskListener;
    invoke-interface {v1, p1}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$OnFinishCreatingMaskListener;->onFinishCreatingMask(Lcom/sec/android/app/shealth/food/mask/MaskTools;)V

    goto :goto_0

    .line 164
    .end local v1    # "listener":Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$OnFinishCreatingMaskListener;
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 133
    check-cast p1, Lcom/sec/android/app/shealth/food/mask/MaskTools;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskCreatingTask;->onPostExecute(Lcom/sec/android/app/shealth/food/mask/MaskTools;)V

    return-void
.end method
