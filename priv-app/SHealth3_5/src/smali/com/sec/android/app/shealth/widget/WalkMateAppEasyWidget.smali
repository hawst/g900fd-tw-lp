.class public Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;
.super Landroid/appwidget/AppWidgetProvider;
.source "WalkMateAppEasyWidget.java"


# static fields
.field private static final MODE_GEAR:Ljava/lang/String; = "gear"

.field private static final MODE_HEALTHY:Ljava/lang/String; = "healthy"

.field private static final MODE_INACTIVE:Ljava/lang/String; = "inactive"

.field private static final MODE_NORMAL:Ljava/lang/String; = "normal"

.field public static final REQUEST_FOR_WEIGHT_INPUT:I = 0x271a

.field private static final TAG:Ljava/lang/String;

.field private static mWalkMateAppEasyWidget:Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;


# instance fields
.field private mDayStepCounterConnection:Landroid/content/ServiceConnection;

.field private mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

.field private mGoalStep:I

.field private mIsBound:Z

.field private mTotalStep:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const-class v0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    .line 60
    iput v1, p0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->mTotalStep:I

    .line 61
    iput v1, p0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->mGoalStep:I

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .line 64
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->mIsBound:Z

    .line 400
    new-instance v0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget$1;-><init>(Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->mDayStepCounterConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    return-object p1
.end method

.method private doBindWalkStepService(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 374
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isRun()Z

    move-result v1

    if-nez v1, :cond_0

    .line 376
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 377
    .local v0, "iService":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 378
    sget-object v1, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->TAG:Ljava/lang/String;

    const-string v2, "RealtimeHealthService is started"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    .end local v0    # "iService":Landroid/content/Intent;
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->mIsBound:Z

    if-nez v1, :cond_1

    .line 383
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->mDayStepCounterConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {p1, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->mIsBound:Z

    .line 385
    :cond_1
    return-void
.end method

.method private doUnbindWalkStepService()V
    .locals 3

    .prologue
    .line 389
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->mIsBound:Z

    if-eqz v1, :cond_0

    .line 392
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->mDayStepCounterConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 397
    :cond_0
    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->mIsBound:Z

    .line 398
    return-void

    .line 393
    :catch_0
    move-exception v0

    .line 394
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private getCurrentMode(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 127
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v0

    .line 129
    .local v0, "activeTimeMonitor":Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v1

    const/16 v2, 0x2719

    if-eq v1, v2, :cond_0

    .line 130
    const-string v1, "gear"

    .line 138
    :goto_0
    return-object v1

    .line 132
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->isHealthyStep()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 133
    const-string v1, "healthy"

    goto :goto_0

    .line 135
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->isInactivetime()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 136
    const-string v1, "inactive"

    goto :goto_0

    .line 138
    :cond_2
    const-string/jumbo v1, "normal"

    goto :goto_0
.end method

.method public static updateWidgets()V
    .locals 5

    .prologue
    .line 283
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    if-nez v2, :cond_1

    .line 285
    sget-object v2, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->TAG:Ljava/lang/String;

    const-string v3, "context is null"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    .local v0, "appWidgetIds":[I
    .local v1, "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    :cond_0
    :goto_0
    return-void

    .line 288
    .end local v0    # "appWidgetIds":[I
    .end local v1    # "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    .line 289
    .restart local v1    # "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    new-instance v2, Landroid/content/ComponentName;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    .line 290
    .restart local v0    # "appWidgetIds":[I
    if-eqz v0, :cond_0

    .line 292
    sget-object v2, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "count ofwidgets = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    array-length v2, v0

    if-lez v2, :cond_0

    .line 295
    sget-object v2, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->mWalkMateAppEasyWidget:Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;

    if-nez v2, :cond_2

    .line 296
    new-instance v2, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;

    invoke-direct {v2}, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;-><init>()V

    sput-object v2, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->mWalkMateAppEasyWidget:Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;

    .line 299
    :cond_2
    sget-object v2, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->mWalkMateAppEasyWidget:Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, v1, v0}, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    goto :goto_0
.end method


# virtual methods
.method public loadLatestData(Landroid/content/Context;J)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "time"    # J

    .prologue
    .line 306
    const/4 v10, 0x0

    .line 307
    .local v10, "totalSteps":I
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isRun()Z

    move-result v0

    if-nez v0, :cond_4

    .line 309
    const/4 v6, 0x0

    .line 312
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v7

    .line 313
    .local v7, "deviceType":I
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 314
    .local v9, "query":Ljava/lang/StringBuilder;
    const-string v0, " SELECT "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 315
    const-string v0, " SUM( "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 316
    const-string/jumbo v0, "total_step) AS TOTAL_STEP, "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 317
    const-string v0, " SUM( "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 318
    const-string v0, "distance) AS TOTAL_DISTANCE, "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 319
    const-string v0, " SUM( "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 320
    const-string v0, "calorie) AS TOTAL_CALORIE "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 321
    const-string v0, " FROM "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 322
    const-string/jumbo v0, "walk_info"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 323
    const-string v0, " WHERE "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 324
    const-string/jumbo v0, "sync_status != 170004"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 325
    const-string v0, " AND "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 326
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "start_time >= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 327
    const-string v0, " AND "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 328
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "start_time < "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 329
    const-string v0, " AND "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 330
    const-string/jumbo v0, "total_step > 0"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 331
    const-string v0, " AND "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 332
    invoke-static {v7}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 335
    :try_start_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v6

    .line 341
    :goto_0
    if-eqz v6, :cond_3

    .line 343
    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 345
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 346
    const-string v0, "TOTAL_STEP"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v10

    .line 356
    :cond_0
    :goto_1
    if-eqz v6, :cond_1

    .line 358
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 361
    :cond_1
    iput v10, p0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->mTotalStep:I

    .line 367
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "deviceType":I
    .end local v9    # "query":Ljava/lang/StringBuilder;
    :goto_2
    return-void

    .line 337
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v7    # "deviceType":I
    .restart local v9    # "query":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v8

    .line 339
    .local v8, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 356
    .end local v7    # "deviceType":I
    .end local v8    # "e":Ljava/lang/Exception;
    .end local v9    # "query":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 358
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 351
    .restart local v7    # "deviceType":I
    .restart local v9    # "query":Ljava/lang/StringBuilder;
    :cond_3
    const/4 v10, 0x0

    goto :goto_1

    .line 365
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "deviceType":I
    .end local v9    # "query":Ljava/lang/StringBuilder;
    :cond_4
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getTotalStep()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->mTotalStep:I

    goto :goto_2
.end method

.method public onDeleted(Landroid/content/Context;[I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetIds"    # [I

    .prologue
    .line 123
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onDeleted(Landroid/content/Context;[I)V

    .line 124
    return-void
.end method

.method public onDisabled(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 115
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onDisabled(Landroid/content/Context;)V

    .line 116
    invoke-direct {p0}, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->doUnbindWalkStepService()V

    .line 117
    sget-object v0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->TAG:Ljava/lang/String;

    const-string v1, "OnDisabled Walk"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    return-void
.end method

.method public onEnabled(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 104
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onEnabled(Landroid/content/Context;)V

    .line 105
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->doBindWalkStepService(Landroid/content/Context;)V

    .line 106
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isRun()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getTotalStep()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->mTotalStep:I

    .line 110
    :cond_0
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 74
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 75
    sget-object v3, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "onReceive"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 99
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 82
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 85
    sget-object v3, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "intent.getAction() = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    const-string v3, "android.intent.action.RESTART_WIDGET"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "com.sec.android.app.shealth.intent.action.UPDATE_WIDGET"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 91
    :cond_2
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    .line 92
    .local v2, "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    new-instance v3, Landroid/content/ComponentName;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;

    invoke-direct {v3, v4, v5}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v3}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v1

    .line 93
    .local v1, "appWidgetIds":[I
    if-eqz v1, :cond_0

    array-length v3, v1

    if-lez v3, :cond_0

    .line 95
    sget-object v3, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "count ofwidgets = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p0, v3, v2, v1}, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    goto :goto_0
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 15
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .prologue
    .line 145
    new-instance v8, Landroid/widget/RemoteViews;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v11

    const v12, 0x7f03027a

    invoke-direct {v8, v11, v12}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 146
    .local v8, "myViews":Landroid/widget/RemoteViews;
    sget-object v11, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "onUpdate() - appWidgetIds size = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p3

    array-length v13, v0

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    invoke-super/range {p0 .. p3}, Landroid/appwidget/AppWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 149
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v6

    .line 150
    .local v6, "isContentProviderAccessible":Z
    sget-object v11, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "isContentProviderAccessible : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getGoal()I

    move-result v11

    iput v11, p0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->mGoalStep:I

    .line 153
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v11

    const/16 v12, 0x2719

    if-eq v11, v12, :cond_0

    .line 155
    if-eqz p3, :cond_2

    .line 157
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    move-object/from16 v0, p3

    array-length v11, v0

    if-ge v4, v11, :cond_2

    .line 159
    aget v3, p3, v4

    .line 160
    .local v3, "appWidgetId":I
    const v11, 0x7f080b2a

    const/16 v12, 0x8

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 161
    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v8}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 157
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 166
    .end local v3    # "appWidgetId":I
    .end local v4    # "i":I
    :cond_0
    const v11, 0x7f080b2a

    const/4 v12, 0x0

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 169
    if-eqz v6, :cond_1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_3

    .line 171
    :cond_1
    const v11, 0x7f080b30

    const-string v12, ""

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 172
    const v11, 0x7f080b2b

    const/4 v12, 0x0

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 173
    const v11, 0x7f080b2c

    const/16 v12, 0x8

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 174
    const v11, 0x7f080b2d

    const/16 v12, 0x8

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 175
    const v11, 0x7f080b2e

    const/16 v12, 0x8

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 176
    const v11, 0x7f080b2f

    const/16 v12, 0x8

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 178
    new-instance v5, Landroid/content/Intent;

    const-class v11, Lcom/sec/android/app/shealth/receiver/LaunchWidgetReceiver;

    move-object/from16 v0, p1

    invoke-direct {v5, v0, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 179
    .local v5, "intent":Landroid/content/Intent;
    const-string v11, "com.sec.shealth.broadcast.LAUNCH_APP_WIDGET_ACTIVITY"

    invoke-virtual {v5, v11}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 180
    const-string/jumbo v11, "widgetActivityAction"

    const-string v12, "com.sec.shealth.action.PEDOMETER"

    invoke-virtual {v5, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 181
    const-string/jumbo v11, "widgetActivityPackage"

    const-string v12, "com.sec.android.app.shealth"

    invoke-virtual {v5, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 182
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    const/16 v12, 0x1ca8

    const/high16 v13, 0x8000000

    invoke-static {v11, v12, v5, v13}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v9

    .line 183
    .local v9, "pendingIntent":Landroid/app/PendingIntent;
    const v11, 0x7f080b2a

    invoke-virtual {v8, v11, v9}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 184
    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v0, v1, v8}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    .line 279
    .end local v5    # "intent":Landroid/content/Intent;
    .end local v9    # "pendingIntent":Landroid/app/PendingIntent;
    :cond_2
    return-void

    .line 189
    :cond_3
    if-eqz p3, :cond_4

    .line 191
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_1
    move-object/from16 v0, p3

    array-length v11, v0

    if-ge v4, v11, :cond_4

    .line 193
    sget-object v11, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "appWidgetIds = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    aget v13, p3, v4

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 197
    .end local v4    # "i":I
    :cond_4
    sget-object v11, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->TAG:Ljava/lang/String;

    const-string/jumbo v12, "onUpdate :: mDayStepService is null"

    invoke-static {v11, v12}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-virtual {p0, v11, v12, v13}, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->loadLatestData(Landroid/content/Context;J)V

    .line 199
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-direct {p0, v11}, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->doBindWalkStepService(Landroid/content/Context;)V

    .line 201
    if-eqz p3, :cond_2

    .line 203
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_2
    move-object/from16 v0, p3

    array-length v11, v0

    if-ge v4, v11, :cond_2

    .line 205
    aget v3, p3, v4

    .line 207
    .restart local v3    # "appWidgetId":I
    iget v11, p0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->mTotalStep:I

    const v12, 0x186a0

    if-lt v11, v12, :cond_5

    .line 209
    iget v11, p0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->mTotalStep:I

    div-int/lit16 v11, v11, 0x3e8

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    .line 210
    .local v10, "value":Ljava/lang/String;
    const v11, 0x7f080b30

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const v13, 0x7f090bb4

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 218
    .end local v10    # "value":Ljava/lang/String;
    :goto_3
    const-string v2, "com.sec.android.app.shealth.settings.InputPinCode"

    .line 219
    .local v2, "InputPinCodeClassName":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isPinOn()Z

    move-result v11

    if-eqz v11, :cond_6

    .line 221
    sget-object v11, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->TAG:Ljava/lang/String;

    const-string v12, "go to pincode"

    invoke-static {v11, v12}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 223
    .restart local v5    # "intent":Landroid/content/Intent;
    const v11, 0x20008000

    invoke-virtual {v5, v11}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 234
    :goto_4
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->getCurrentMode(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    .line 236
    .local v7, "mode":Ljava/lang/String;
    const v11, 0x7f080b30

    const/4 v12, -0x1

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 237
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v11

    if-nez v11, :cond_7

    .line 238
    const v11, 0x7f080b2b

    const/16 v12, 0x8

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 239
    const v11, 0x7f080b2c

    const/4 v12, 0x0

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 240
    const v11, 0x7f080b2d

    const/16 v12, 0x8

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 241
    const v11, 0x7f080b2e

    const/16 v12, 0x8

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 242
    const v11, 0x7f080b2f

    const/16 v12, 0x8

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 243
    const v11, 0x7f080b30

    const v12, -0x7f000001

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 273
    :goto_5
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v11

    const/16 v12, 0x1ca8

    const/high16 v13, 0x8000000

    invoke-static {v11, v12, v5, v13}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v9

    .line 274
    .restart local v9    # "pendingIntent":Landroid/app/PendingIntent;
    const v11, 0x7f080b2a

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f090020

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, p0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->mTotalStep:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0900c6

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 275
    const v11, 0x7f080b2a

    invoke-virtual {v8, v11, v9}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 276
    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v8}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 203
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2

    .line 214
    .end local v2    # "InputPinCodeClassName":Ljava/lang/String;
    .end local v5    # "intent":Landroid/content/Intent;
    .end local v7    # "mode":Ljava/lang/String;
    .end local v9    # "pendingIntent":Landroid/app/PendingIntent;
    :cond_5
    const v11, 0x7f080b30

    iget v12, p0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->mTotalStep:I

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 227
    .restart local v2    # "InputPinCodeClassName":Ljava/lang/String;
    :cond_6
    sget-object v11, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->TAG:Ljava/lang/String;

    const-string v12, "go to pedometer"

    invoke-static {v11, v12}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    new-instance v5, Landroid/content/Intent;

    const-class v11, Lcom/sec/android/app/shealth/receiver/LaunchWidgetReceiver;

    move-object/from16 v0, p1

    invoke-direct {v5, v0, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 229
    .restart local v5    # "intent":Landroid/content/Intent;
    const-string v11, "com.sec.shealth.broadcast.LAUNCH_APP_WIDGET_ACTIVITY"

    invoke-virtual {v5, v11}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 230
    const-string/jumbo v11, "widgetActivityAction"

    const-string v12, "com.sec.shealth.action.PEDOMETER"

    invoke-virtual {v5, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 231
    const-string/jumbo v11, "widgetActivityPackage"

    const-string v12, "com.sec.android.app.shealth"

    invoke-virtual {v5, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_4

    .line 245
    .restart local v7    # "mode":Ljava/lang/String;
    :cond_7
    const-string v11, "healthy"

    invoke-virtual {v7, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 246
    const v11, 0x7f080b2b

    const/16 v12, 0x8

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 247
    const v11, 0x7f080b2c

    const/16 v12, 0x8

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 248
    const v11, 0x7f080b2d

    const/4 v12, 0x0

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 249
    const v11, 0x7f080b2e

    const/16 v12, 0x8

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 250
    const v11, 0x7f080b2f

    const/16 v12, 0x8

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_5

    .line 251
    :cond_8
    const-string v11, "inactive"

    invoke-virtual {v7, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 252
    const v11, 0x7f080b2b

    const/16 v12, 0x8

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 253
    const v11, 0x7f080b2c

    const/16 v12, 0x8

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 254
    const v11, 0x7f080b2d

    const/16 v12, 0x8

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 255
    const v11, 0x7f080b2e

    const/4 v12, 0x0

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 256
    const v11, 0x7f080b2f

    const/16 v12, 0x8

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_5

    .line 258
    :cond_9
    iget v11, p0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->mTotalStep:I

    iget v12, p0, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->mGoalStep:I

    if-lt v11, v12, :cond_a

    .line 259
    const v11, 0x7f080b2b

    const/16 v12, 0x8

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 260
    const v11, 0x7f080b2c

    const/16 v12, 0x8

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 261
    const v11, 0x7f080b2d

    const/16 v12, 0x8

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 262
    const v11, 0x7f080b2e

    const/16 v12, 0x8

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 263
    const v11, 0x7f080b2f

    const/4 v12, 0x0

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_5

    .line 265
    :cond_a
    const v11, 0x7f080b2b

    const/4 v12, 0x0

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 266
    const v11, 0x7f080b2c

    const/16 v12, 0x8

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 267
    const v11, 0x7f080b2d

    const/16 v12, 0x8

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 268
    const v11, 0x7f080b2e

    const/16 v12, 0x8

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 269
    const v11, 0x7f080b2f

    const/16 v12, 0x8

    invoke-virtual {v8, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_5
.end method
