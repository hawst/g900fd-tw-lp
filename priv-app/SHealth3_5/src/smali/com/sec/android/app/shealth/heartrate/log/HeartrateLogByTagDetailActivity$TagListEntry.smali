.class Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;
.super Ljava/lang/Object;
.source "HeartrateLogByTagDetailActivity.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TagListEntry"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;",
        ">;"
    }
.end annotation


# instance fields
.field bpm:J

.field icon:I

.field tagId:I

.field tagName:I

.field tagTextName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 417
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(IJIILjava/lang/String;)V
    .locals 0
    .param p1, "tagName"    # I
    .param p2, "bpm"    # J
    .param p4, "icon"    # I
    .param p5, "tagIndex"    # I
    .param p6, "tagTextName"    # Ljava/lang/String;

    .prologue
    .line 420
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 422
    iput p1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;->tagName:I

    .line 423
    iput-wide p2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;->bpm:J

    .line 424
    iput p4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;->icon:I

    .line 425
    iput p5, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;->tagId:I

    .line 426
    iput-object p6, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;->tagTextName:Ljava/lang/String;

    .line 427
    return-void
.end method


# virtual methods
.method public compare(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;)I
    .locals 4
    .param p1, "lhs"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;
    .param p2, "rhs"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;

    .prologue
    .line 432
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;->getBpm()J

    move-result-wide v0

    invoke-virtual {p2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;->getBpm()J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 367
    check-cast p1, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;->compare(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;)I

    move-result v0

    return v0
.end method

.method public getBpm()J
    .locals 2

    .prologue
    .line 408
    iget-wide v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;->bpm:J

    return-wide v0
.end method

.method public getIcon()I
    .locals 1

    .prologue
    .line 399
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;->icon:I

    return v0
.end method

.method public getTagId()I
    .locals 1

    .prologue
    .line 383
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;->tagId:I

    return v0
.end method

.method public getTagName()I
    .locals 1

    .prologue
    .line 391
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;->tagName:I

    return v0
.end method

.method public getTagTextName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;->tagTextName:Ljava/lang/String;

    return-object v0
.end method

.method public setBpm(J)V
    .locals 0
    .param p1, "bpm"    # J

    .prologue
    .line 412
    iput-wide p1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;->bpm:J

    .line 413
    return-void
.end method

.method public setIcon(I)V
    .locals 0
    .param p1, "icon"    # I

    .prologue
    .line 403
    iput p1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;->icon:I

    .line 404
    return-void
.end method

.method public setTagId(I)V
    .locals 0
    .param p1, "tagId"    # I

    .prologue
    .line 387
    iput p1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;->tagId:I

    .line 388
    return-void
.end method

.method public setTagName(I)V
    .locals 0
    .param p1, "tagName"    # I

    .prologue
    .line 395
    iput p1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;->tagName:I

    .line 396
    return-void
.end method

.method public setTagTextName(Ljava/lang/String;)V
    .locals 0
    .param p1, "tagTextName"    # Ljava/lang/String;

    .prologue
    .line 379
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;->tagTextName:Ljava/lang/String;

    .line 380
    return-void
.end method
