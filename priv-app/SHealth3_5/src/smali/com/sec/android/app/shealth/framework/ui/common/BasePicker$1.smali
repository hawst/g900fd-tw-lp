.class Lcom/sec/android/app/shealth/framework/ui/common/BasePicker$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->initButton(Landroid/widget/Button;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;

.field final synthetic val$button:Landroid/widget/Button;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;Landroid/widget/Button;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker$1;->val$button:Landroid/widget/Button;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->mOnBeforeClickListener:Lcom/sec/android/app/shealth/framework/ui/common/BasePicker$OnBeforeClickListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->access$000(Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;)Lcom/sec/android/app/shealth/framework/ui/common/BasePicker$OnBeforeClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->mOnBeforeClickListener:Lcom/sec/android/app/shealth/framework/ui/common/BasePicker$OnBeforeClickListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->access$000(Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;)Lcom/sec/android/app/shealth/framework/ui/common/BasePicker$OnBeforeClickListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker$OnBeforeClickListener;->startOfClickListener()V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->getDialog()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->showPopup()V

    :cond_1
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker$1;->val$button:Landroid/widget/Button;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Utils;->temporarilyDisableClick([Landroid/view/View;)V

    return-void
.end method
