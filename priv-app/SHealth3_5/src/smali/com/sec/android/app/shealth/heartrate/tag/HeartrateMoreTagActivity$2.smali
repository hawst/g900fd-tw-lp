.class Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$2;
.super Ljava/lang/Object;
.source "HeartrateMoreTagActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->createCustomOptionMenu()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;)V
    .locals 0

    .prologue
    .line 295
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$2;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v7, 0x14

    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 298
    instance-of v3, p1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    if-eqz v3, :cond_0

    move-object v0, p1

    .line 299
    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    .line 301
    .local v0, "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v3

    if-nez v3, :cond_0

    .line 302
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$2;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    invoke-static {v3}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->access$000(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getCustomTags()Ljava/util/ArrayList;

    move-result-object v1

    .line 303
    .local v1, "customTags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v3, v7, :cond_1

    .line 304
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$2;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$2;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090f21

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v8}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 317
    .end local v0    # "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    .end local v1    # "customTags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;>;"
    :cond_0
    :goto_0
    return-void

    .line 308
    .restart local v0    # "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    .restart local v1    # "customTags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;>;"
    :cond_1
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$2;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    const-class v4, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 309
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "from_more_activity"

    invoke-virtual {v2, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 310
    const/high16 v3, 0x10000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 312
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$2;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    const/16 v4, 0x45c

    invoke-virtual {v3, v2, v4}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method
