.class Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView$4;
.super Lcom/sec/android/app/shealth/common/commonui/summary/StubAnimatorListener;
.source "TGHHeaderView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->getFadeOutAnimator()Landroid/animation/ObjectAnimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;)V
    .locals 0

    .prologue
    .line 235
    iput-object p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView$4;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/summary/StubAnimatorListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 5
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 239
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView$4;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceStateTextView:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->access$300(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;)Landroid/widget/TextView;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setAlpha(F)V

    .line 240
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView$4;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceState:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;
    invoke-static {v3}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->access$400(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;)Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->getTextIds()[I

    move-result-object v2

    .line 241
    .local v2, "textIds":[I
    const-string v1, ""

    .line 242
    .local v1, "text":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_1

    .line 243
    if-lez v0, :cond_0

    .line 244
    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 246
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView$4;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    aget v4, v2, v0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 242
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 249
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView$4;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->mBalanceStateTextView:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->access$300(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 251
    return-void
.end method
