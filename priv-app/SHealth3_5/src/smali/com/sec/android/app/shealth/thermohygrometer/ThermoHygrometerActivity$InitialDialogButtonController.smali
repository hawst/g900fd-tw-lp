.class Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity$InitialDialogButtonController;
.super Ljava/lang/Object;
.source "ThermoHygrometerActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InitialDialogButtonController"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity$InitialDialogButtonController;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity;Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity$1;

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity$InitialDialogButtonController;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity;)V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 1
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 85
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity$InitialDialogButtonController;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity;->mSa:Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity;->access$100(Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity;)Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;->dismiss()V

    .line 92
    :cond_0
    :goto_0
    return-void

    .line 88
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->NEGATIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity$InitialDialogButtonController;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity;->mSa:Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity;->access$100(Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerActivity;)Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/thermohygrometer/ThermoHygrometerDialog;->dismiss()V

    goto :goto_0
.end method
