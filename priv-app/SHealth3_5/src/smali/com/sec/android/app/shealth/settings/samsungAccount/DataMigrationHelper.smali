.class public Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;
.super Ljava/lang/Object;
.source "DataMigrationHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$17;,
        Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$IDataMigrationHelperListener;,
        Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;
    }
.end annotation


# static fields
.field public static final MIG_LOCAL_FAILED_POPUP:Ljava/lang/String; = "local_failed_popup"

.field public static final MIG_NETWORK_FAILED_POPUP:Ljava/lang/String; = "network_failed_popup"

.field public static final MIG_OVERWRITE_CONFIRM_POPUP:Ljava/lang/String; = "overwrite_confirm_popup"

.field public static final MIG_PROGRESS_POPUP:Ljava/lang/String; = "migration_progress_popup"


# instance fields
.field private TAG:Ljava/lang/String;

.field private isConfirmed:Z

.field private mAppID:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field mConverTo2dot5MigrationStatusListener:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

.field private mCurrentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

.field private mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private final mLocalFailedPopupRunnable:Ljava/lang/Runnable;

.field mLocalMigrationStatusListener:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

.field private mMigrationPercentage:Landroid/widget/TextView;

.field private mMigrationProgressBar:Landroid/widget/ProgressBar;

.field private mMigrationProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

.field private mMigrationProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mMigrationProgressMsg:Landroid/widget/TextView;

.field private mMigrationState:I

.field private mNetUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

.field private final mNetworkFailedPopupRunnable:Ljava/lang/Runnable;

.field private mOverwriteDBConfrimDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mPopupHandler:Landroid/os/Handler;

.field private mRestoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

.field private mSetMaxRestoreSize:D

.field mUploadFailureLocalMigrationStatusListener:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

.field private mUserAccount:Ljava/lang/String;

.field private popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 46
    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 47
    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mOverwriteDBConfrimDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 48
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 55
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mCurrentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 57
    iput v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationState:I

    .line 58
    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;

    .line 60
    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mNetUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    .line 62
    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mUserAccount:Ljava/lang/String;

    .line 65
    const-string v1, "com.sec.android.app.shealth"

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mAppID:Ljava/lang/String;

    .line 67
    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$1;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mNetworkFailedPopupRunnable:Ljava/lang/Runnable;

    .line 75
    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$2;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mLocalFailedPopupRunnable:Ljava/lang/Runnable;

    .line 99
    const-class v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->TAG:Ljava/lang/String;

    .line 100
    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mPopupHandler:Landroid/os/Handler;

    .line 108
    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$3;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mLocalMigrationStatusListener:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

    .line 185
    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$4;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mUploadFailureLocalMigrationStatusListener:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

    .line 235
    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$5;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mConverTo2dot5MigrationStatusListener:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

    .line 258
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;

    .line 259
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mPopupHandler:Landroid/os/Handler;

    .line 260
    new-instance v1, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    invoke-direct {v1, p1}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mNetUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    .line 261
    invoke-static {p1}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mUserAccount:Ljava/lang/String;

    .line 262
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mUserAccount:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 264
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getRestoreHelper()Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mRestoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 269
    :goto_0
    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mUserAccount:Ljava/lang/String;

    .line 272
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->getMigrationState(Landroid/content/Context;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationState:I

    .line 274
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->isConfirmed:Z

    .line 275
    return-void

    .line 265
    :catch_0
    move-exception v0

    .line 266
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mRestoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->showFailPopup(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationProgressMsg:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationProgressMsg:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mRestoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->isConfirmed:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->isConfirmed:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->showOverwriteConfirmPopup()V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mNetworkFailedPopupRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mOverwriteDBConfrimDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mOverwriteDBConfrimDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;Landroid/widget/ProgressBar;)Landroid/widget/ProgressBar;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;
    .param p1, "x1"    # Landroid/widget/ProgressBar;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationProgressBar:Landroid/widget/ProgressBar;

    return-object p1
.end method

.method static synthetic access$1802(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationPercentage:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$1900(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mAppID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Lcom/sec/android/app/shealth/common/utils/NetworkUtils;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mNetUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    .prologue
    .line 39
    iget v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationState:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mCurrentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;)Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mCurrentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;)Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;
    .param p1, "x1"    # I

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->updateProgressAsStatus(I)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->closeProgressPopupAsStatus()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mPopupHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mLocalFailedPopupRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static cancelMigration()V
    .locals 2

    .prologue
    .line 718
    const-string v0, "DataMigrationHelper"

    const-string v1, "cancelMigration()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 719
    invoke-static {}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->getInstance()Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->cancel()Z

    .line 721
    return-void
.end method

.method private closeMigrationProgressBar()V
    .locals 1

    .prologue
    .line 839
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 840
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 841
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 843
    :cond_0
    return-void
.end method

.method private closeProgressPopupAsStatus()V
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mUserAccount:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 412
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->closeMigrationProgressBar()V

    .line 416
    :goto_0
    return-void

    .line 414
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mRestoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->closeProgressDialog()V

    goto :goto_0
.end method

.method private showFailPopup(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;)V
    .locals 5
    .param p1, "failType"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .prologue
    .line 729
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "showFailPopup() type is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 730
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;

    const/4 v3, 0x2

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 731
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const v2, 0x7f090d2c

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 732
    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->NETWORK_FAILED_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    if-ne p1, v2, :cond_0

    const v1, 0x7f0907f7

    .line 733
    .local v1, "msgId":I
    :goto_0
    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 734
    const v2, 0x7f09004b

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 735
    const v2, 0x7f090d2d

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 736
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 737
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 738
    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->NETWORK_FAILED_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    if-ne p1, v2, :cond_1

    .line 740
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string/jumbo v4, "network_failed_popup"

    invoke-virtual {v3, v2, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 746
    :goto_1
    return-void

    .line 732
    .end local v1    # "msgId":I
    :cond_0
    const v1, 0x7f090d2e

    goto :goto_0

    .line 744
    .restart local v1    # "msgId":I
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v4, "local_failed_popup"

    invoke-virtual {v3, v2, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private showOverwriteConfirmPopup()V
    .locals 4

    .prologue
    .line 750
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "showOverwriteConfirmPopup()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 752
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v2, 0x7f090055

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090887

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090046

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090d2d

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$12;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$12;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    .line 766
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mOverwriteDBConfrimDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 767
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->CONFIRM_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 768
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mOverwriteDBConfrimDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v3, "overwrite_confirm_popup"

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 769
    return-void
.end method

.method private showProgressPopupAsStatus()V
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mUserAccount:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 404
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->showMigrationProgressBar()V

    .line 408
    :goto_0
    return-void

    .line 406
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mRestoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->showPopupWithProgressBar()V

    goto :goto_0
.end method

.method private updateProgressAsStatus(I)V
    .locals 1
    .param p1, "percentage"    # I

    .prologue
    .line 419
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mUserAccount:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 420
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->updateMigrationProgressBarValue(I)V

    .line 424
    :goto_0
    return-void

    .line 422
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mRestoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->updateProgressPopupValue(I)V

    goto :goto_0
.end method


# virtual methods
.method public doMigration()Z
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 427
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MIGRATION - doMigration() migrationState : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationState:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 428
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mUserAccount:Ljava/lang/String;

    .line 429
    iget v5, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationState:I

    packed-switch v5, :pswitch_data_0

    .line 509
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->TAG:Ljava/lang/String;

    const-string v5, "doMigration() BAD case"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 510
    :goto_0
    return v3

    .line 431
    :pswitch_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/health/cp/ThinCPConstants;->MIGRATION_AUTHORITY_URI:Landroid/net/Uri;

    const-string v7, "check_platform_db_exists"

    invoke-virtual {v5, v6, v7, v8, v8}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 432
    .local v0, "checkPlatformDB":Landroid/os/Bundle;
    const-string/jumbo v5, "value_of_check_if_platform_db_exists"

    invoke-virtual {v0, v5, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 433
    .local v2, "isExists":Z
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "3.x to 3.5 migration enable: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    if-nez v2, :cond_0

    .line 435
    sget-object v3, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mCurrentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 436
    invoke-static {}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->getInstance()Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    move-result-object v3

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v5}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->doNormalInitialization(Landroid/content/Context;)Z

    .line 437
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v3

    invoke-virtual {v3, v4}, Lcom/sec/android/service/health/keyManager/KeyManager;->setContentProviderState(Z)V

    .line 438
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$IRestoreCompleteListener;

    invoke-interface {v3, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$IRestoreCompleteListener;->restoreCompleted(Z)V

    :goto_1
    move v3, v4

    .line 456
    goto :goto_0

    .line 440
    :cond_0
    sget-object v3, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->LOCAL_MIGRATION:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mCurrentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 441
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mRestoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    const v5, 0x7f090888

    invoke-virtual {v3, v5}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->showPopupWithProgressBar(I)V

    .line 442
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mPopupHandler:Landroid/os/Handler;

    new-instance v5, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$9;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$9;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)V

    const-wide/16 v6, 0x7d0

    invoke-virtual {v3, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 459
    .end local v0    # "checkPlatformDB":Landroid/os/Bundle;
    .end local v2    # "isExists":Z
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mRestoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->showServerCheckingPopup()V

    .line 461
    sget-object v1, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    .line 462
    .local v1, "deviceID":Ljava/lang/String;
    sget-object v3, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->CHECK_SERVER:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mCurrentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 464
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mNetUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isNetworkConnected()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 466
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->getInstance()Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;

    move-result-object v3

    new-instance v5, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$10;

    invoke-direct {v5, p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$10;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker;->checkLastBackedUpDeviceId(Lcom/sec/android/app/shealth/framework/repository/database/old/HDeviceIdChecker$IHDeviceIdListener;)Z

    :goto_2
    move v3, v4

    .line 507
    goto/16 :goto_0

    .line 504
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->TAG:Ljava/lang/String;

    const-string v5, "MIGRATION - Network is not available."

    invoke-static {v3, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 505
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mPopupHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mNetworkFailedPopupRunnable:Ljava/lang/Runnable;

    invoke-virtual {v3, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_2

    .line 429
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method doMigrationIfNetworkFailureStartFrom2xto35()V
    .locals 4

    .prologue
    .line 516
    const/16 v0, 0x19

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->updateProgressAsStatus(I)V

    .line 517
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mRestoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    const v1, 0x7f090d3f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->updateProgressPopupText(I)V

    .line 518
    invoke-static {}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->getInstance()Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mUploadFailureLocalMigrationStatusListener:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->migrateLocalDataFrom2xTo35(Landroid/content/Context;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 519
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->TAG:Ljava/lang/String;

    const-string v1, "MIGRATION - migrateLocalDataFrom2xto35 Failed"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 520
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->closeProgressPopupAsStatus()V

    .line 521
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mPopupHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mLocalFailedPopupRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 524
    :cond_0
    return-void
.end method

.method doMigrationStartFrom2xto25()V
    .locals 5

    .prologue
    .line 527
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->TAG:Ljava/lang/String;

    const-string v2, "MIGRATION - doMigrationStartFrom2xto25()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    invoke-static {}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->getInstance()Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->check2XDBFileExist(Landroid/content/Context;)Z

    move-result v0

    .line 530
    .local v0, "is2xDBExist":Z
    if-nez v0, :cond_0

    .line 531
    invoke-static {}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->getInstance()Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->doNormalInitialization(Landroid/content/Context;)Z

    .line 532
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->setContentProviderState(Z)V

    .line 533
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mRestoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->startRestoreProcess()V

    .line 586
    :goto_0
    return-void

    .line 538
    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->LOCAL_MIGRATION:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mCurrentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 539
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mRestoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    const v2, 0x7f090888

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->showPopupWithProgressBar(I)V

    .line 541
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mPopupHandler:Landroid/os/Handler;

    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)V

    const-wide/16 v3, 0x7d0

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method exitApplication()V
    .locals 1

    .prologue
    .line 724
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finishAffinity()V

    .line 725
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    .line 726
    return-void
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 862
    const-string/jumbo v0, "network_failed_popup"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 863
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$14;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$14;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)V

    .line 963
    :goto_0
    return-object v0

    .line 913
    :cond_0
    const-string v0, "local_failed_popup"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 915
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$15;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$15;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)V

    goto :goto_0

    .line 939
    :cond_1
    const-string/jumbo v0, "overwrite_confirm_popup"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 940
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$16;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$16;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)V

    goto :goto_0

    .line 963
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public handleUserInteraction(IILandroid/content/Intent;)Z
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 969
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleUserInteraction requestCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", resultCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 970
    packed-switch p1, :pswitch_data_0

    .line 985
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 973
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 975
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->doMigration()Z

    .line 981
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 970
    nop

    :pswitch_data_0
    .packed-switch 0xa25
        :pswitch_0
    .end packed-switch
.end method

.method public migrationState()V
    .locals 3

    .prologue
    .line 278
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$17;->$SwitchMap$com$sec$android$app$shealth$settings$samsungAccount$DataMigrationHelper$Step:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 312
    :cond_0
    :goto_0
    return-void

    .line 280
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Show again "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v2, "network_failed_popup"

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 287
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 288
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Show again "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v2, "local_failed_popup"

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 294
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Show again "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v2, "migration_progress_popup"

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 302
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mOverwriteDBConfrimDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mOverwriteDBConfrimDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Show again "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mOverwriteDBConfrimDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v2, "overwrite_confirm_popup"

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 278
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public showMigrationProgressBar()V
    .locals 4

    .prologue
    .line 774
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "progress popup showing"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 775
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mPopupHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 836
    return-void
.end method

.method public startLocalMigration()V
    .locals 9

    .prologue
    const-wide/16 v7, 0x7d0

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 317
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "startLocalMigration() mMigrationState: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    iget v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationState:I

    packed-switch v2, :pswitch_data_0

    .line 398
    :goto_0
    return-void

    .line 321
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/cp/ThinCPConstants;->MIGRATION_AUTHORITY_URI:Landroid/net/Uri;

    const-string v4, "check_platform_db_exists"

    invoke-virtual {v2, v3, v4, v6, v6}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 322
    .local v0, "checkPlatformDB":Landroid/os/Bundle;
    const-string/jumbo v2, "value_of_check_if_platform_db_exists"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 323
    .local v1, "isExists":Z
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "3.x to 3.5 migration enable: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    if-nez v1, :cond_0

    .line 325
    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mCurrentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 326
    invoke-static {}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->getInstance()Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;

    new-instance v4, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$6;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$6;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)V

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->doNormalInitialization(Landroid/content/Context;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)Z

    .line 347
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/service/health/keyManager/KeyManager;->setContentProviderState(Z)V

    goto :goto_0

    .line 350
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->showMigrationProgressBar()V

    .line 351
    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->PROGRESS_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 352
    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->LOCAL_MIGRATION:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mCurrentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 353
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mPopupHandler:Landroid/os/Handler;

    new-instance v3, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$7;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$7;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)V

    invoke-virtual {v2, v3, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 373
    .end local v0    # "checkPlatformDB":Landroid/os/Bundle;
    .end local v1    # "isExists":Z
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$IRestoreCompleteListener;

    invoke-interface {v2, v5}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$IRestoreCompleteListener;->restoreCompleted(Z)V

    goto :goto_0

    .line 378
    :pswitch_2
    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->LOCAL_MIGRATION:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mCurrentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 379
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->showMigrationProgressBar()V

    .line 380
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mPopupHandler:Landroid/os/Handler;

    new-instance v3, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$8;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$8;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)V

    invoke-virtual {v2, v3, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 319
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public updateMigrationProgressBarText(I)V
    .locals 2
    .param p1, "textID"    # I

    .prologue
    .line 855
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationProgressMsg:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 856
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationProgressMsg:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 858
    :cond_0
    return-void
.end method

.method public updateMigrationProgressBarValue(I)V
    .locals 4
    .param p1, "percentage"    # I

    .prologue
    .line 846
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationProgressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 847
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 849
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationPercentage:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 850
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationPercentage:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;

    const v3, 0x7f090d15

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 852
    :cond_1
    return-void
.end method
