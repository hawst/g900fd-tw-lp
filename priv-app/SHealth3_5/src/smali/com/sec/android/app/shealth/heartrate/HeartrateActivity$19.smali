.class Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$19;
.super Ljava/lang/Object;
.source "HeartrateActivity.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->animate(Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

.field final synthetic val$imageView1:Landroid/widget/ImageView;

.field final synthetic val$imageView2:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;Landroid/widget/ImageView;Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 1059
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$19;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    iput-object p2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$19;->val$imageView2:Landroid/widget/ImageView;

    iput-object p3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$19;->val$imageView1:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v1, 0x0

    .line 1061
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$19;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->animationFadeIn:Landroid/view/animation/AnimationSet;

    if-ne p1, v0, :cond_1

    .line 1062
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$19;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isImage1Found:Z

    if-eqz v0, :cond_0

    .line 1063
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$19;->val$imageView2:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1065
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$19;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isImage2Found:Z

    if-eqz v0, :cond_1

    .line 1066
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$19;->val$imageView1:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1069
    :cond_1
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 1072
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 1075
    return-void
.end method
