.class Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity$2;
.super Ljava/lang/Object;
.source "HeartrateLogDetailActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->showDeletePopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;)V
    .locals 0

    .prologue
    .line 248
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 252
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->access$300(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->access$000(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;)Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->deleteRowById(Ljava/lang/String;)Z

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->setResult(I)V

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;->finish()V

    .line 255
    return-void
.end method
