.class public Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;
.super Ljava/lang/Object;
.source "WalkingSContextListener.java"

# interfaces
.implements Landroid/hardware/scontext/SContextListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    }
.end annotation


# static fields
.field private static final HEALTHYSTEP:I = 0x258

.field private static final MAX_DURATION_LIMIT:I = 0x927c0

.field private static final MONITORACTION:Ljava/lang/String; = "com.sec.android.app.shealth.walkingmate.walkingmateactivetimemonitor.COMMAND"

.field private static final TAG:Ljava/lang/String; = "WalkingSContextListener"

.field private static final healthyStepDebug:Z

.field private static mContext:Landroid/content/Context;

.field private static mDeviceID:Ljava/lang/String;

.field private static mWalkingSContextListener:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

.field private static sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;


# instance fields
.field private isActiveTimeSContextOn:Z

.field private isHealthyStepSContextOn:Z

.field private isInactiveTimeSContextOn:Z

.field private mFeatureManager:Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;

.field private mHealthyStepStart:Z

.field private mInactiveDuration:J

.field private mInactiveDurationSetting:I

.field private mInactiveTimeStart:Z

.field private mPreviousStepStart:Z

.field private mSContextManager:Landroid/hardware/scontext/SContextManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    const-string v0, ""

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mDeviceID:Ljava/lang/String;

    .line 75
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->isHealthyStepSContextOn:Z

    .line 64
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->isActiveTimeSContextOn:Z

    .line 65
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->isInactiveTimeSContextOn:Z

    .line 67
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mHealthyStepStart:Z

    .line 68
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mInactiveTimeStart:Z

    .line 69
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mInactiveDuration:J

    .line 71
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mPreviousStepStart:Z

    .line 112
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mFeatureManager:Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mFeatureManager:Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->checkFeature(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "scontext"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/scontext/SContextManager;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    .line 118
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->checkStatus()V

    .line 120
    :cond_0
    return-void
.end method

.method static synthetic access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    return-object v0
.end method

.method static synthetic access$100()Landroid/content/Context;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mDeviceID:Ljava/lang/String;

    return-object v0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 97
    const-class v1, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    monitor-enter v1

    :try_start_0
    const-string v0, "WalkingSContextListener"

    const-string v2, "WalkingSContextListener is initiated"

    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mWalkingSContextListener:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    if-nez v0, :cond_0

    .line 99
    sput-object p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mContext:Landroid/content/Context;

    .line 101
    invoke-static {p0}, Lcom/samsung/android/sdk/health/sensor/utils/HealthServiceUtil;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mDeviceID:Ljava/lang/String;

    .line 102
    const-string v0, "WalkingSContextListener"

    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mDeviceID:Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mWalkingSContextListener:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    .line 105
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mWalkingSContextListener:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sendCurrentStatus()V

    .line 108
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mWalkingSContextListener:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 97
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public declared-synchronized allStop()V
    .locals 2

    .prologue
    .line 169
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->isHealthyStepSContextOn:Z

    if-eqz v0, :cond_0

    .line 170
    const-string v0, "WalkingSContextListener"

    const-string/jumbo v1, "release healthy step"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    const/16 v1, 0x21

    invoke-virtual {v0, p0, v1}, Landroid/hardware/scontext/SContextManager;->unregisterListener(Landroid/hardware/scontext/SContextListener;I)V

    .line 173
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->isActiveTimeSContextOn:Z

    if-eqz v0, :cond_1

    .line 174
    const-string v0, "WalkingSContextListener"

    const-string/jumbo v1, "release active time monitor"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    const/16 v1, 0x22

    invoke-virtual {v0, p0, v1}, Landroid/hardware/scontext/SContextManager;->unregisterListener(Landroid/hardware/scontext/SContextListener;I)V

    .line 177
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->isInactiveTimeSContextOn:Z

    if-eqz v0, :cond_2

    .line 178
    const-string v0, "WalkingSContextListener"

    const-string/jumbo v1, "release inactive time monitor"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    const/16 v1, 0x23

    invoke-virtual {v0, p0, v1}, Landroid/hardware/scontext/SContextManager;->unregisterListener(Landroid/hardware/scontext/SContextListener;I)V

    .line 182
    :cond_2
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mInactiveDuration:J

    .line 183
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->isActiveTimeSContextOn:Z

    .line 184
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->isInactiveTimeSContextOn:Z

    .line 185
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->isHealthyStepSContextOn:Z

    .line 186
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mHealthyStepStart:Z

    .line 187
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mPreviousStepStart:Z

    .line 188
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->checkStatus()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 189
    monitor-exit p0

    return-void

    .line 169
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public checkInactiveTimer()Z
    .locals 1

    .prologue
    .line 220
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->isInactiveTimeSContextOn:Z

    return v0
.end method

.method public checkStatus()V
    .locals 3

    .prologue
    .line 224
    const-string v0, "WalkingSContextListener"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "healthy step = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->isHealthyStepSContextOn:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    const-string v0, "WalkingSContextListener"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "active time = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->isActiveTimeSContextOn:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    const-string v0, "WalkingSContextListener"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "inactive time = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->isInactiveTimeSContextOn:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    return-void
.end method

.method public convertUnitTime(J)J
    .locals 2
    .param p1, "utc"    # J

    .prologue
    .line 731
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->convertUnitTime(JI)J

    move-result-wide v0

    return-wide v0
.end method

.method public convertUnitTime(JI)J
    .locals 4
    .param p1, "utc"    # J
    .param p3, "minute"    # I

    .prologue
    const/16 v3, 0xc

    const/4 v2, 0x0

    .line 735
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 736
    .local v0, "cal":Ljava/util/Calendar;
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 737
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    div-int/lit8 v1, v1, 0xa

    mul-int/lit8 v1, v1, 0xa

    add-int/2addr v1, p3

    invoke-virtual {v0, v3, v1}, Ljava/util/Calendar;->set(II)V

    .line 738
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 739
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 740
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    return-wide v1
.end method

.method public onSContextChanged(Landroid/hardware/scontext/SContextEvent;)V
    .locals 23
    .param p1, "scontextEvent"    # Landroid/hardware/scontext/SContextEvent;

    .prologue
    .line 231
    if-eqz p1, :cond_2

    .line 232
    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/hardware/scontext/SContextEvent;->scontext:Landroid/hardware/scontext/SContext;

    .line 233
    .local v10, "scontext":Landroid/hardware/scontext/SContext;
    if-eqz v10, :cond_2

    .line 235
    invoke-virtual {v10}, Landroid/hardware/scontext/SContext;->getType()I

    move-result v11

    .line 236
    .local v11, "scontextType":I
    const-string v19, "WalkingSContextListener"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Type is "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    const/16 v19, 0x21

    move/from16 v0, v19

    if-ne v11, v0, :cond_5

    .line 247
    const-string v19, "WalkingSContextListener"

    const-string v20, "STEP_LEVEL_MONITOR"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextEvent;->getStepLevelMonitorContext()Landroid/hardware/scontext/SContextStepLevelMonitor;

    move-result-object v16

    .line 250
    .local v16, "stepLevelMonitorContext":Landroid/hardware/scontext/SContextStepLevelMonitor;
    const-string v19, "WalkingSContextListener"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "in StepLevel Monitor current time = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v21

    invoke-virtual/range {v20 .. v22}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    invoke-virtual/range {v16 .. v16}, Landroid/hardware/scontext/SContextStepLevelMonitor;->getStepLevel()[I

    move-result-object v15

    .line 252
    .local v15, "stepLevel":[I
    invoke-virtual/range {v16 .. v16}, Landroid/hardware/scontext/SContextStepLevelMonitor;->getStepCount()[I

    move-result-object v14

    .line 253
    .local v14, "stepCount":[I
    invoke-virtual/range {v16 .. v16}, Landroid/hardware/scontext/SContextStepLevelMonitor;->getTimeStamp()[J

    move-result-object v18

    .line 254
    .local v18, "timeStamp":[J
    invoke-virtual/range {v16 .. v16}, Landroid/hardware/scontext/SContextStepLevelMonitor;->getDuration()[I

    move-result-object v6

    .line 264
    .local v6, "duration":[I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    invoke-virtual/range {v16 .. v16}, Landroid/hardware/scontext/SContextStepLevelMonitor;->getCount()I

    move-result v19

    move/from16 v0, v19

    if-ge v8, v0, :cond_3

    .line 265
    aget-wide v12, v18, v8

    .line 266
    .local v12, "startTime":J
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5, v12, v13}, Ljava/util/Date;-><init>(J)V

    .line 268
    .local v5, "d2":Ljava/util/Date;
    aget v19, v15, v8

    const/16 v20, 0x4

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_1

    .line 269
    aget v17, v14, v8

    .line 270
    .local v17, "steps":I
    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mHealthyStepStart:Z

    .line 271
    const-string v19, "WalkingSContextListener"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "PowerStep log steps = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    if-lez v17, :cond_0

    .line 274
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1, v12, v13}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->setHealthyStepToDB(IJ)V

    .line 264
    :cond_0
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 284
    .end local v17    # "steps":I
    :cond_1
    aget v17, v14, v8

    .line 285
    .restart local v17    # "steps":I
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mHealthyStepStart:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 303
    .end local v5    # "d2":Ljava/util/Date;
    .end local v6    # "duration":[I
    .end local v8    # "i":I
    .end local v12    # "startTime":J
    .end local v14    # "stepCount":[I
    .end local v15    # "stepLevel":[I
    .end local v16    # "stepLevelMonitorContext":Landroid/hardware/scontext/SContextStepLevelMonitor;
    .end local v17    # "steps":I
    .end local v18    # "timeStamp":[J
    :catch_0
    move-exception v7

    .line 304
    .local v7, "ex":Ljava/lang/Exception;
    const-string v19, "WalkingSContextListener"

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    .end local v7    # "ex":Ljava/lang/Exception;
    .end local v10    # "scontext":Landroid/hardware/scontext/SContext;
    .end local v11    # "scontextType":I
    :cond_2
    :goto_2
    return-void

    .line 299
    .restart local v6    # "duration":[I
    .restart local v8    # "i":I
    .restart local v10    # "scontext":Landroid/hardware/scontext/SContext;
    .restart local v11    # "scontextType":I
    .restart local v14    # "stepCount":[I
    .restart local v15    # "stepLevel":[I
    .restart local v16    # "stepLevelMonitorContext":Landroid/hardware/scontext/SContextStepLevelMonitor;
    .restart local v18    # "timeStamp":[J
    :cond_3
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mPreviousStepStart:Z

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mHealthyStepStart:Z

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_4

    .line 300
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mHealthyStepStart:Z

    move/from16 v19, v0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sendHealthyStepStatus(Z)V

    .line 302
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mHealthyStepStart:Z

    move/from16 v19, v0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mPreviousStepStart:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 306
    .end local v6    # "duration":[I
    .end local v8    # "i":I
    .end local v14    # "stepCount":[I
    .end local v15    # "stepLevel":[I
    .end local v16    # "stepLevelMonitorContext":Landroid/hardware/scontext/SContextStepLevelMonitor;
    .end local v18    # "timeStamp":[J
    :cond_5
    const/16 v19, 0x23

    move/from16 v0, v19

    if-ne v11, v0, :cond_8

    .line 307
    const-string v19, "WalkingSContextListener"

    const-string v20, "INACTIVE_TIMER"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextEvent;->getInactiveTimerContext()Landroid/hardware/scontext/SContextInactiveTimer;

    move-result-object v9

    .line 309
    .local v9, "inactiveTimer":Landroid/hardware/scontext/SContextInactiveTimer;
    const-string v19, "WalkingSContextListener"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "inactive status : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v9}, Landroid/hardware/scontext/SContextInactiveTimer;->getStatus()I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    const-string v19, "WalkingSContextListener"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "inactive getDuration : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v9}, Landroid/hardware/scontext/SContextInactiveTimer;->getDuration()I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    const-string v19, "WalkingSContextListener"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "inactive isTimeOutExpired :"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v9}, Landroid/hardware/scontext/SContextInactiveTimer;->isTimeOutExpired()Z

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    invoke-virtual {v9}, Landroid/hardware/scontext/SContextInactiveTimer;->isTimeOutExpired()Z

    move-result v19

    if-eqz v19, :cond_6

    .line 315
    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mInactiveTimeStart:Z

    .line 318
    :cond_6
    invoke-virtual {v9}, Landroid/hardware/scontext/SContextInactiveTimer;->getDuration()I

    move-result v19

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v19, v0

    move-wide/from16 v0, v19

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mInactiveDuration:J

    .line 320
    invoke-virtual {v9}, Landroid/hardware/scontext/SContextInactiveTimer;->getDuration()I

    move-result v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mInactiveDurationSetting:I

    move/from16 v20, v0

    move/from16 v0, v20

    mul-int/lit16 v0, v0, 0x3e8

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_7

    .line 321
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mInactiveTimeStart:Z

    .line 323
    :cond_7
    invoke-virtual {v9}, Landroid/hardware/scontext/SContextInactiveTimer;->getDuration()I

    move-result v19

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v19, v0

    invoke-virtual {v9}, Landroid/hardware/scontext/SContextInactiveTimer;->isTimeOutExpired()Z

    move-result v21

    move-object/from16 v0, p0

    move-wide/from16 v1, v19

    move/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sendInactiveTimeStatus(JZ)V

    goto/16 :goto_2

    .line 325
    .end local v9    # "inactiveTimer":Landroid/hardware/scontext/SContextInactiveTimer;
    :cond_8
    const/16 v19, 0x22

    move/from16 v0, v19

    if-ne v11, v0, :cond_2

    .line 326
    const-string v19, "WalkingSContextListener"

    const-string v20, "ACTIVE_TIME_MONITOR"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextEvent;->getActiveTimeMonitorContext()Landroid/hardware/scontext/SContextActiveTimeMonitor;

    move-result-object v4

    .line 328
    .local v4, "activeTimeMonitorContext":Landroid/hardware/scontext/SContextActiveTimeMonitor;
    if-eqz v4, :cond_2

    .line 329
    const-string v19, "WalkingSContextListener"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "active Time:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v4}, Landroid/hardware/scontext/SContextActiveTimeMonitor;->getDuration()I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    invoke-virtual {v4}, Landroid/hardware/scontext/SContextActiveTimeMonitor;->getDuration()I

    move-result v19

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->setActiveTime(I)V

    goto/16 :goto_2
.end method

.method public resetData()V
    .locals 4

    .prologue
    .line 922
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    if-eqz v0, :cond_0

    .line 924
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    monitor-enter v1

    .line 926
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->accumlatedHealthyStep:J

    .line 927
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->accumlatedDuration:J

    .line 928
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    const-wide/16 v2, -0x1

    iput-wide v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->stepsDBid:J

    .line 929
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    const-wide/16 v2, -0x1

    iput-wide v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->activeTimeDBid:J

    .line 930
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->lastStepsUpdateTime:J

    .line 931
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->lastDurationUpdateTime:J

    .line 932
    monitor-exit v1

    .line 934
    :cond_0
    return-void

    .line 932
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public sendCurrentStatus()V
    .locals 4

    .prologue
    .line 911
    const-string/jumbo v1, "scontext_update"

    const-string/jumbo v2, "send current Status"

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 912
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.shealth.walkingmate.walkingmateactivetimemonitor.COMMAND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 913
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "data"

    const-string v2, "current_status"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 914
    const-string v1, "healthy_step"

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mHealthyStepStart:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 915
    const-string v1, "inactive_time"

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mInactiveTimeStart:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 916
    const-string v1, "inactive_time_duration"

    iget-wide v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mInactiveDuration:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 917
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 918
    return-void
.end method

.method public sendHealthyStepStatus(Z)V
    .locals 3
    .param p1, "status"    # Z

    .prologue
    .line 896
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.shealth.walkingmate.walkingmateactivetimemonitor.COMMAND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 897
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "data"

    const-string v2, "healthy_step"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 898
    const-string/jumbo v1, "status"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 899
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 900
    return-void
.end method

.method public sendInactiveTimeStatus(JZ)V
    .locals 3
    .param p1, "duration"    # J
    .param p3, "status"    # Z

    .prologue
    .line 903
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.shealth.walkingmate.walkingmateactivetimemonitor.COMMAND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 904
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "data"

    const-string v2, "inactive_time"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 905
    const-string/jumbo v1, "status"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 906
    const-string/jumbo v1, "value"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 907
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 908
    return-void
.end method

.method public setActiveTime(I)V
    .locals 7
    .param p1, "duration"    # I

    .prologue
    .line 338
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    .line 339
    .local v2, "date":Ljava/util/Date;
    const-string v4, "WalkingSContextListener"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "save time = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " duration = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    const/4 v0, 0x0

    .line 345
    .local v0, "_buffer":I
    const/16 v4, 0x5dc

    if-ge p1, v4, :cond_0

    .line 346
    const/4 v0, 0x0

    .line 351
    :goto_0
    move v1, v0

    .line 352
    .local v1, "buffer":I
    move v3, p1

    .line 354
    .local v3, "save_duration":I
    new-instance v4, Ljava/lang/Thread;

    new-instance v5, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;

    invoke-direct {v5, p0, v1, v3}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;II)V

    invoke-direct {v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    .line 728
    return-void

    .line 348
    .end local v1    # "buffer":I
    .end local v3    # "save_duration":I
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setHealthyStepToDB(IJ)V
    .locals 7
    .param p1, "steps"    # I
    .param p2, "loggingTime"    # J

    .prologue
    .line 745
    move v1, p1

    .line 746
    .local v1, "save_steps":I
    const-string v4, "WalkingSContextListener"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "setHealthyStepToDB steps = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " loggingTime = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 751
    const/4 v0, 0x0

    .line 753
    .local v0, "buffer":I
    const/4 v4, 0x2

    if-ge p1, v4, :cond_0

    .line 754
    const/4 v0, 0x0

    .line 760
    :goto_0
    mul-int/lit16 v4, v0, 0x3e8

    int-to-long v4, v4

    add-long v2, p2, v4

    .line 761
    .local v2, "save_time":J
    new-instance v4, Ljava/lang/Thread;

    new-instance v5, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$2;

    invoke-direct {v5, p0, v2, v3, v1}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$2;-><init>(Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;JI)V

    invoke-direct {v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    .line 893
    return-void

    .line 756
    .end local v2    # "save_time":J
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public declared-synchronized startActiveTime()V
    .locals 2

    .prologue
    .line 147
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->isActiveTimeSContextOn:Z

    if-nez v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mFeatureManager:Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->checkFeature(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    const-string v0, "WalkingSContextListener"

    const-string/jumbo v1, "start active time monitor"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    const/16 v1, 0x22

    invoke-virtual {v0, p0, v1}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;I)Z

    .line 152
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->isActiveTimeSContextOn:Z

    .line 155
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->checkStatus()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    monitor-exit p0

    return-void

    .line 147
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized startHealthyStep()V
    .locals 3

    .prologue
    .line 124
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->isHealthyStepSContextOn:Z

    if-nez v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mFeatureManager:Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->checkFeature(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    const-string v0, "WalkingSContextListener"

    const-string/jumbo v1, "start healthy step monitor"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    const/16 v1, 0x21

    const/16 v2, 0x258

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;II)Z

    .line 128
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->isHealthyStepSContextOn:Z

    .line 131
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->checkStatus()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132
    monitor-exit p0

    return-void

    .line 124
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized startInactiveTimer(IIII)V
    .locals 8
    .param p1, "duration"    # I
    .param p2, "alert_count"    # I
    .param p3, "startTime"    # I
    .param p4, "endTime"    # I

    .prologue
    .line 204
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mFeatureManager:Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->checkFeature(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 206
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mInactiveDurationSetting:I

    .line 207
    const-string v0, "WalkingSContextListener"

    const-string/jumbo v1, "start inactive time monitor"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    const-string v0, "WalkingSContextListener"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\t\tduration = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    const-string v0, "WalkingSContextListener"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\t\talert_count = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    const-string v0, "WalkingSContextListener"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\t\tstart_time = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    const-string v0, "WalkingSContextListener"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\t\tend_time = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    const/16 v2, 0x23

    const/4 v3, 0x1

    move-object v1, p0

    move v4, p1

    move v5, p2

    move v6, p3

    move v7, p4

    invoke-virtual/range {v0 .. v7}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;IIIIII)Z

    .line 214
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->isInactiveTimeSContextOn:Z

    .line 216
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->checkStatus()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 217
    monitor-exit p0

    return-void

    .line 204
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stopActiveTime()V
    .locals 2

    .prologue
    .line 159
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->isActiveTimeSContextOn:Z

    if-eqz v0, :cond_0

    .line 160
    const-string v0, "WalkingSContextListener"

    const-string/jumbo v1, "release active time monitor"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    const/16 v1, 0x22

    invoke-virtual {v0, p0, v1}, Landroid/hardware/scontext/SContextManager;->unregisterListener(Landroid/hardware/scontext/SContextListener;I)V

    .line 162
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->isActiveTimeSContextOn:Z

    .line 163
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sendCurrentStatus()V

    .line 165
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->checkStatus()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166
    monitor-exit p0

    return-void

    .line 159
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stopHealthyStep()V
    .locals 2

    .prologue
    .line 135
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->isHealthyStepSContextOn:Z

    if-eqz v0, :cond_0

    .line 136
    const-string v0, "WalkingSContextListener"

    const-string/jumbo v1, "release healthy step"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    const/16 v1, 0x21

    invoke-virtual {v0, p0, v1}, Landroid/hardware/scontext/SContextManager;->unregisterListener(Landroid/hardware/scontext/SContextListener;I)V

    .line 138
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->isHealthyStepSContextOn:Z

    .line 139
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mHealthyStepStart:Z

    .line 140
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mPreviousStepStart:Z

    .line 141
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sendCurrentStatus()V

    .line 143
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->checkStatus()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    monitor-exit p0

    return-void

    .line 135
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stopInactiveTimer()V
    .locals 2

    .prologue
    .line 192
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->isInactiveTimeSContextOn:Z

    if-eqz v0, :cond_0

    .line 193
    const-string v0, "WalkingSContextListener"

    const-string/jumbo v1, "release inactive time monitor"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    const/16 v1, 0x23

    invoke-virtual {v0, p0, v1}, Landroid/hardware/scontext/SContextManager;->unregisterListener(Landroid/hardware/scontext/SContextListener;I)V

    .line 195
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->isInactiveTimeSContextOn:Z

    .line 196
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mInactiveTimeStart:Z

    .line 197
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mInactiveDuration:J

    .line 198
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sendCurrentStatus()V

    .line 200
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->checkStatus()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 201
    monitor-exit p0

    return-void

    .line 192
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
