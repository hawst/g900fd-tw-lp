.class public Lcom/sec/android/app/shealth/KeyManagerReceiver;
.super Landroid/content/BroadcastReceiver;
.source "KeyManagerReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static mApplication:Lcom/sec/android/app/shealth/SHealthApplication;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/sec/android/app/shealth/KeyManagerReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/KeyManagerReceiver;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private static handleCpAccessbile()V
    .locals 3

    .prologue
    .line 82
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    sget-boolean v1, Lcom/sec/android/service/health/keyManager/KeyManager;->DEBUG:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/shealth/KeyManagerReceiver;->TAG:Ljava/lang/String;

    const-string v2, "KeyManagercom.sec.android.service.health.ContentProviderAccessible"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/KeyManagerReceiver;->mApplication:Lcom/sec/android/app/shealth/SHealthApplication;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/SHealthApplication;->isFinishedInitialization()Z

    move-result v1

    if-nez v1, :cond_1

    .line 84
    sget-object v1, Lcom/sec/android/app/shealth/KeyManagerReceiver;->mApplication:Lcom/sec/android/app/shealth/SHealthApplication;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/SHealthApplication;->init()V

    .line 88
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/keyManager/KeyManager;->startCallbackIntent()V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    :cond_2
    :goto_0
    return-void

    .line 89
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :catch_0
    move-exception v0

    .line 90
    .restart local v0    # "e":Landroid/content/ActivityNotFoundException;
    sget-boolean v1, Lcom/sec/android/service/health/keyManager/KeyManager;->DEBUG:Z

    if-eqz v1, :cond_2

    sget-object v1, Lcom/sec/android/app/shealth/KeyManagerReceiver;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static handleKeyManagerReceiver()V
    .locals 3

    .prologue
    .line 69
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    sget-boolean v1, Lcom/sec/android/service/health/keyManager/KeyManager;->DEBUG:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/shealth/KeyManagerReceiver;->TAG:Ljava/lang/String;

    const-string v2, "KeyManagercom.sec.android.service.health.KeyManagerReceiver"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/KeyManagerReceiver;->mApplication:Lcom/sec/android/app/shealth/SHealthApplication;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/SHealthApplication;->preInit()V

    .line 73
    :try_start_0
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 74
    invoke-static {}, Lcom/sec/android/app/shealth/KeyManagerReceiver;->handleCpAccessbile()V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    :cond_1
    :goto_0
    return-void

    .line 76
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :catch_0
    move-exception v0

    .line 77
    .restart local v0    # "e":Landroid/content/ActivityNotFoundException;
    sget-boolean v1, Lcom/sec/android/service/health/keyManager/KeyManager;->DEBUG:Z

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/android/app/shealth/KeyManagerReceiver;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static setApplication(Lcom/sec/android/app/shealth/SHealthApplication;)V
    .locals 0
    .param p0, "application"    # Lcom/sec/android/app/shealth/SHealthApplication;

    .prologue
    .line 38
    sput-object p0, Lcom/sec/android/app/shealth/KeyManagerReceiver;->mApplication:Lcom/sec/android/app/shealth/SHealthApplication;

    .line 39
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "arg1"    # Landroid/content/Intent;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/android/app/shealth/KeyManagerReceiver;->mContext:Landroid/content/Context;

    .line 45
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 47
    .local v1, "name":Ljava/lang/String;
    sget-boolean v2, Lcom/sec/android/service/health/keyManager/KeyManager;->DEBUG:Z

    if-eqz v2, :cond_0

    sget-object v2, Lcom/sec/android/app/shealth/KeyManagerReceiver;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " KeyManagerReceiver.onReceive : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    :cond_0
    if-eqz v1, :cond_2

    .line 49
    const-string v2, "com.sec.android.service.health.KeyManagerReceiver"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 50
    invoke-static {}, Lcom/sec/android/app/shealth/KeyManagerReceiver;->handleKeyManagerReceiver()V

    .line 60
    :cond_1
    :goto_0
    const-string v2, "com.sec.android.service.health.ContentProviderAccessible"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 61
    invoke-static {}, Lcom/sec/android/app/shealth/KeyManagerReceiver;->handleCpAccessbile()V

    .line 66
    :cond_2
    return-void

    .line 51
    :cond_3
    const-string v2, "com.sec.android.service.health.KeyManager_Password"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 53
    sget-boolean v2, Lcom/sec/android/service/health/keyManager/KeyManager;->DEBUG:Z

    if-eqz v2, :cond_4

    sget-object v2, Lcom/sec/android/app/shealth/KeyManagerReceiver;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "KeyManager KeyManagerReceiver.onReceive.KeyManager_Password ======= settingPassword"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/service/health/keyManager/KeyManager;->getApplicationPassword()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    :cond_4
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/shealth/KeyManagerReceiver;->mContext:Landroid/content/Context;

    const-class v3, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 55
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "SetPasswordActivity"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 56
    const/high16 v2, 0x34000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 57
    iget-object v2, p0, Lcom/sec/android/app/shealth/KeyManagerReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
