.class Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;
.super Ljava/lang/Object;
.source "ControllerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GradationObject"
.end annotation


# instance fields
.field private isBig:Z

.field private isThick:Z

.field private mOffsetMovement:I

.field private mValue:F

.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;FIZZ)V
    .locals 0
    .param p2, "value"    # F
    .param p3, "offsetMovement"    # I
    .param p4, "isBig"    # Z
    .param p5, "isThick"    # Z

    .prologue
    .line 564
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->this$0:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 565
    iput p2, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->mValue:F

    .line 566
    iput p3, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->mOffsetMovement:I

    .line 567
    iput-boolean p4, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->isBig:Z

    .line 568
    iput-boolean p5, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->isThick:Z

    .line 569
    return-void
.end method


# virtual methods
.method public getOffsetMovement()I
    .locals 1

    .prologue
    .line 576
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->mOffsetMovement:I

    return v0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 572
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->mValue:F

    return v0
.end method

.method public isBig()Z
    .locals 1

    .prologue
    .line 584
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->isBig:Z

    return v0
.end method

.method public isThick()Z
    .locals 1

    .prologue
    .line 588
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->isThick:Z

    return v0
.end method

.method public setOffsetMovement(I)V
    .locals 0
    .param p1, "offsetMovement"    # I

    .prologue
    .line 580
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->mOffsetMovement:I

    .line 581
    return-void
.end method
