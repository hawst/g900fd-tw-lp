.class Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$1;
.super Ljava/lang/Object;
.source "WalkingMateRankingActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->customizeActionBarWithTabs()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->isNetworkConnted()Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->access$000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->disableTabClick(Landroid/view/View;)V
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->access$100(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;Landroid/view/View;)V

    .line 184
    :cond_0
    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->currentTab:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->access$200()Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;->EVERYONE:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    if-ne v0, v1, :cond_1

    .line 191
    :goto_0
    return-void

    .line 188
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;->EVERYONE:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->switchToTab(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;)V

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->tab2:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->access$300(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    # invokes: Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->setCurrentTabContentDescription(Landroid/view/View;Z)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->access$400(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;Landroid/view/View;Z)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->tab1:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->access$500(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->setCurrentTabContentDescription(Landroid/view/View;Z)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->access$400(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;Landroid/view/View;Z)V

    goto :goto_0
.end method
