.class final Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase$1;
.super Ljava/lang/Object;
.source "SearchListResultBase.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;
    .locals 7
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 91
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-nez v6, :cond_1

    const/4 v1, 0x1

    .line 92
    .local v1, "isEmpty":Z
    :goto_0
    const/4 v3, 0x0

    .line 93
    .local v3, "loader":Ljava/lang/ClassLoader;
    if-nez v1, :cond_0

    .line 94
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 95
    .local v0, "eClass":Ljava/lang/Class;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    .line 97
    .end local v0    # "eClass":Ljava/lang/Class;
    :cond_0
    :goto_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 98
    .local v2, "list":Ljava/util/List;
    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 99
    new-instance v5, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    invoke-direct {v5, v2, v6}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;-><init>(Ljava/util/List;I)V

    .line 100
    .local v5, "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;
    const-class v6, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;

    invoke-virtual {v6}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v6

    invoke-virtual {p1, v6}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;

    .line 101
    .local v4, "request":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;
    invoke-virtual {v5, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;->setRequest(Ljava/lang/Object;)V

    .line 102
    return-object v5

    .line 91
    .end local v1    # "isEmpty":Z
    .end local v2    # "list":Ljava/util/List;
    .end local v3    # "loader":Ljava/lang/ClassLoader;
    .end local v4    # "request":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;
    .end local v5    # "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 95
    .restart local v0    # "eClass":Ljava/lang/Class;
    .restart local v1    # "isEmpty":Z
    .restart local v3    # "loader":Ljava/lang/ClassLoader;
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 87
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase$1;->createFromParcel(Landroid/os/Parcel;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 107
    new-array v0, p1, [Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 87
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase$1;->newArray(I)[Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;

    move-result-object v0

    return-object v0
.end method
