.class public Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;
.super Landroid/widget/FrameLayout;
.source "SpO2Circle.java"


# instance fields
.field private isRun:Z

.field private mActivity:Landroid/app/Activity;

.field private mCircleAnimationSet:Landroid/view/animation/AnimationSet;

.field private mMeasuringAniLinear:Landroid/widget/LinearLayout;

.field private mSecondMeasuringProgressView:Landroid/widget/ImageView;

.field private mSecondMeasuringProgressViewFail:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 56
    check-cast p1, Landroid/app/Activity;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mActivity:Landroid/app/Activity;

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->isRun:Z

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0805d1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mMeasuringAniLinear:Landroid/widget/LinearLayout;

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0805d2

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mSecondMeasuringProgressView:Landroid/widget/ImageView;

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0805d3

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mSecondMeasuringProgressViewFail:Landroid/widget/ImageView;

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 47
    check-cast p1, Landroid/app/Activity;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mActivity:Landroid/app/Activity;

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->isRun:Z

    .line 50
    const v0, 0x7f0805d1

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mMeasuringAniLinear:Landroid/widget/LinearLayout;

    .line 51
    const v0, 0x7f0805d2

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mSecondMeasuringProgressView:Landroid/widget/ImageView;

    .line 52
    const v0, 0x7f0805d3

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mSecondMeasuringProgressViewFail:Landroid/widget/ImageView;

    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;)Landroid/view/animation/AnimationSet;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;Landroid/view/animation/AnimationSet;)Landroid/view/animation/AnimationSet;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;
    .param p1, "x1"    # Landroid/view/animation/AnimationSet;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mMeasuringAniLinear:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mSecondMeasuringProgressView:Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public changeColor(Z)V
    .locals 3
    .param p1, "isGreen"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 97
    if-eqz p1, :cond_0

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mSecondMeasuringProgressView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mSecondMeasuringProgressViewFail:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 104
    :goto_0
    return-void

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mSecondMeasuringProgressView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mSecondMeasuringProgressViewFail:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public getIsRun()Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->isRun:Z

    return v0
.end method

.method public releaseAnimation()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mSecondMeasuringProgressView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mSecondMeasuringProgressView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 151
    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mSecondMeasuringProgressView:Landroid/widget/ImageView;

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mSecondMeasuringProgressViewFail:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mSecondMeasuringProgressViewFail:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 156
    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mSecondMeasuringProgressViewFail:Landroid/widget/ImageView;

    .line 159
    :cond_1
    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mMeasuringAniLinear:Landroid/widget/LinearLayout;

    .line 160
    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;

    .line 161
    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mActivity:Landroid/app/Activity;

    .line 162
    return-void
.end method

.method public startAnimation(Z)V
    .locals 2
    .param p1, "isSuccess"    # Z

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->isRun:Z

    if-nez v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle$1;-><init>(Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;Z)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 92
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->isRun:Z

    .line 94
    :cond_0
    return-void
.end method

.method public stopAnimation(Z)V
    .locals 3
    .param p1, "isEnd"    # Z

    .prologue
    .line 107
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->isRun:Z

    if-eqz v1, :cond_1

    .line 108
    if-eqz p1, :cond_2

    .line 109
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 110
    .local v0, "fadeOut":Landroid/view/animation/Animation;
    const-wide/16 v1, 0x14d

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 111
    new-instance v1, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle$2;-><init>(Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 132
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;

    if-eqz v1, :cond_0

    .line 133
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mSecondMeasuringProgressView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 144
    .end local v0    # "fadeOut":Landroid/view/animation/Animation;
    :cond_0
    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->isRun:Z

    .line 146
    :cond_1
    return-void

    .line 136
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mMeasuringAniLinear:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 137
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mMeasuringAniLinear:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->clearAnimation()V

    .line 138
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mSecondMeasuringProgressView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 139
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;

    if-eqz v1, :cond_0

    .line 140
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;

    invoke-virtual {v1}, Landroid/view/animation/AnimationSet;->cancel()V

    .line 141
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;

    goto :goto_0
.end method
