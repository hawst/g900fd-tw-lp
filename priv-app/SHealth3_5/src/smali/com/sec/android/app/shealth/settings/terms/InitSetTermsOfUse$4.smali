.class Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$4;
.super Ljava/lang/Object;
.source "InitSetTermsOfUse.java"

# interfaces
.implements Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)V
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$4;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "userToken"    # Ljava/lang/String;
    .param p2, "userId"    # Ljava/lang/String;
    .param p3, "mcc"    # Ljava/lang/String;

    .prologue
    .line 184
    invoke-static {}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->getInstance()Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->getSamsungAccountBirthday()Ljava/lang/String;

    move-result-object v0

    # setter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->sBirthday:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$1102(Ljava/lang/String;)Ljava/lang/String;

    .line 185
    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$1000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Birthday received : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->sBirthday:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$1200()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    return-void
.end method

.method public setFailureMessage(Ljava/lang/String;)V
    .locals 2
    .param p1, "errMsg"    # Ljava/lang/String;

    .prologue
    .line 179
    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$1000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Failure while requesting token from Samsung Account [BD]"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    return-void
.end method

.method public setNetworkFailure()V
    .locals 2

    .prologue
    .line 174
    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$1000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Network Failure while requesting token from Samsung Account [BD]"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    return-void
.end method
