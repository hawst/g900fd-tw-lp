.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$6;
.super Ljava/lang/Object;
.source "ExerciseProWorkoutInfoActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->customizeActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)V
    .locals 0

    .prologue
    .line 478
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 482
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mEndMode:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mEditMode:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 483
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;->clearTempData(Landroid/content/Context;)V

    .line 484
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->checkChangesAndExit()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)V

    .line 489
    :goto_0
    return-void

    .line 486
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->setResult(I)V

    .line 487
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->finish()V

    goto :goto_0
.end method
