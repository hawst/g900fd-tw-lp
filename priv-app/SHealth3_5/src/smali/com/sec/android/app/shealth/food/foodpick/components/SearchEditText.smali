.class public Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;
.super Landroid/widget/FrameLayout;
.source "SearchEditText.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$FocusHandler;,
        Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$OnSearchKeyClickListener;
    }
.end annotation


# static fields
.field private static final EMPTY_STRING:Ljava/lang/String; = ""


# instance fields
.field private mCancelIcon:Landroid/view/View;

.field private mEditText:Landroid/widget/EditText;

.field private mFakeFocusView:Landroid/view/View;

.field private mFocusHandler:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$FocusHandler;

.field private mIsAnyChildFocused:Z

.field private mOnSearchKeyClickListener:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$OnSearchKeyClickListener;

.field private mSearchIcon:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 56
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->init(Landroid/content/Context;)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 61
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->init(Landroid/content/Context;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 65
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->init(Landroid/content/Context;)V

    .line 67
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;)Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$OnSearchKeyClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mOnSearchKeyClickListener:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$OnSearchKeyClickListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;Ljava/lang/CharSequence;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;
    .param p1, "x1"    # Ljava/lang/CharSequence;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->refreshInnerIconsVisibilities(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mSearchIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->handleFocus()V

    return-void
.end method

.method private handleFocus()V
    .locals 2

    .prologue
    .line 190
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mIsAnyChildFocused:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mCancelIcon:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mFakeFocusView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mIsAnyChildFocused:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mCancelIcon:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mFakeFocusView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 192
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mIsAnyChildFocused:Z

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mIsAnyChildFocused:Z

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mFocusHandler:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$FocusHandler;

    if-eqz v0, :cond_2

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mFocusHandler:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$FocusHandler;

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mIsAnyChildFocused:Z

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$FocusHandler;->onFocusHandle(Z)V

    .line 197
    :cond_2
    return-void

    .line 192
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private init(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 70
    const v0, 0x7f030109

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 71
    const v0, 0x7f0801d7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mEditText:Landroid/widget/EditText;

    .line 73
    const v0, 0x7f0801d8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mSearchIcon:Landroid/widget/ImageView;

    .line 74
    const v0, 0x7f0801da

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mCancelIcon:Landroid/view/View;

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mCancelIcon:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mCancelIcon:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mCancelIcon:Landroid/view/View;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901e6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mCancelIcon:Landroid/view/View;

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$1;-><init>(Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$2;-><init>(Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$3;-><init>(Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$4;-><init>(Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mCancelIcon:Landroid/view/View;

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$5;-><init>(Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 175
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelperThatShowsToast;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0900b2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelperThatShowsToast;-><init>(Landroid/content/Context;I)V

    const/16 v1, 0x32

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelperThatShowsToast;->addLimit(ILandroid/widget/EditText;)V

    .line 177
    const v0, 0x7f08045a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mFakeFocusView:Landroid/view/View;

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mFakeFocusView:Landroid/view/View;

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$6;-><init>(Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mFakeFocusView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 187
    return-void
.end method

.method private refreshInnerIconsVisibilities(Ljava/lang/CharSequence;)V
    .locals 6
    .param p1, "s"    # Ljava/lang/CharSequence;

    .prologue
    const v5, 0x7f0a0746

    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 211
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_1

    const/4 v0, 0x1

    .line 212
    .local v0, "isEmpty":Z
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mCancelIcon:Landroid/view/View;

    if-eqz v0, :cond_2

    move v2, v3

    :goto_1
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 214
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mSearchIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    move v3, v1

    :cond_0
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 216
    if-eqz v0, :cond_3

    .line 218
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mEditText:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0744

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v2, v3, v1, v4, v1}, Landroid/widget/EditText;->setPadding(IIII)V

    .line 224
    :goto_2
    return-void

    .end local v0    # "isEmpty":Z
    :cond_1
    move v0, v1

    .line 211
    goto :goto_0

    .restart local v0    # "isEmpty":Z
    :cond_2
    move v2, v1

    .line 212
    goto :goto_1

    .line 222
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mEditText:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0745

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v2, v3, v1, v4, v1}, Landroid/widget/EditText;->setPadding(IIII)V

    goto :goto_2
.end method


# virtual methods
.method public addTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1
    .param p1, "textWatcher"    # Landroid/text/TextWatcher;

    .prologue
    .line 274
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 275
    return-void
.end method

.method public getEditText()Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method public getText()Landroid/text/Editable;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 228
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 229
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->refreshInnerIconsVisibilities(Ljava/lang/CharSequence;)V

    .line 230
    return-void
.end method

.method public releaseFocusInTouchMode()V
    .locals 2

    .prologue
    .line 253
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mFakeFocusView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mFakeFocusView:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mFakeFocusView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 257
    :cond_0
    return-void
.end method

.method public setCursorPosition()V
    .locals 2

    .prologue
    .line 320
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 321
    return-void
.end method

.method public setFocusHandler(Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$FocusHandler;)V
    .locals 0
    .param p1, "focusHandler"    # Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$FocusHandler;

    .prologue
    .line 207
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mFocusHandler:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$FocusHandler;

    .line 208
    return-void
.end method

.method public setOnSearchKeyClickListener(Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$OnSearchKeyClickListener;)V
    .locals 0
    .param p1, "onSearchKeyClickListener"    # Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$OnSearchKeyClickListener;

    .prologue
    .line 284
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mOnSearchKeyClickListener:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$OnSearchKeyClickListener;

    .line 285
    return-void
.end method

.method public setText(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 265
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(I)V

    .line 266
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 238
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 239
    return-void
.end method
