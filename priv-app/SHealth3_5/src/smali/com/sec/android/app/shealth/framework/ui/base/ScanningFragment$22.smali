.class Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$22;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->actionFinishedScan()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$22;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 48

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$22;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$22;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isFragmetRunning:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5600(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Z

    move-result v1

    if-nez v1, :cond_5

    :goto_0
    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x34

    aput v12, v2, v11

    const/16 v11, -0x60

    aput v11, v2, v10

    const/16 v10, 0x3b67

    aput v10, v2, v9

    const/16 v9, 0x4d4d

    aput v9, v2, v8

    const/16 v8, -0x33c1

    aput v8, v2, v7

    const/16 v7, -0x57

    aput v7, v2, v6

    const/16 v6, -0x76

    aput v6, v2, v5

    const/16 v5, -0x4f

    aput v5, v2, v4

    const/16 v4, -0x62

    aput v4, v2, v3

    const/16 v3, 0x1b55

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x57

    aput v13, v1, v12

    const/16 v12, -0x3d

    aput v12, v1, v11

    const/16 v11, 0x3b0e

    aput v11, v1, v10

    const/16 v10, 0x4d3b

    aput v10, v1, v9

    const/16 v9, -0x33b3

    aput v9, v1, v8

    const/16 v8, -0x34

    aput v8, v1, v7

    const/16 v7, -0x27

    aput v7, v1, v6

    const/16 v6, -0x2d

    aput v6, v1, v5

    const/16 v5, -0x9

    aput v5, v1, v4

    const/16 v4, 0x1b39

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_1

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2
    array-length v4, v1

    if-lt v3, v4, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x2a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, 0x26

    const/16 v43, 0x27

    const/16 v44, 0x28

    const/16 v45, 0x29

    const/16 v46, -0x1282

    aput v46, v2, v45

    const/16 v45, -0x30

    aput v45, v2, v44

    const/16 v44, -0x59

    aput v44, v2, v43

    const/16 v43, 0x6b7b

    aput v43, v2, v42

    const/16 v42, 0x2005

    aput v42, v2, v41

    const/16 v41, -0x54b7

    aput v41, v2, v40

    const/16 v40, -0x3b

    aput v40, v2, v39

    const/16 v39, 0x3f14

    aput v39, v2, v38

    const/16 v38, 0x234a

    aput v38, v2, v37

    const/16 v37, 0x2971

    aput v37, v2, v36

    const/16 v36, -0x30a3

    aput v36, v2, v35

    const/16 v35, -0x56

    aput v35, v2, v34

    const/16 v34, -0x2fc5

    aput v34, v2, v33

    const/16 v33, -0x49

    aput v33, v2, v32

    const/16 v32, 0x7451

    aput v32, v2, v31

    const/16 v31, -0xffa

    aput v31, v2, v30

    const/16 v30, -0x4a

    aput v30, v2, v29

    const/16 v29, -0x2985

    aput v29, v2, v28

    const/16 v28, -0x41

    aput v28, v2, v27

    const/16 v27, -0x3

    aput v27, v2, v26

    const/16 v26, 0x7727

    aput v26, v2, v25

    const/16 v25, -0x4cea

    aput v25, v2, v24

    const/16 v24, -0x6d

    aput v24, v2, v23

    const/16 v23, 0x1902

    aput v23, v2, v22

    const/16 v22, 0x5038

    aput v22, v2, v21

    const/16 v21, 0x2f71

    aput v21, v2, v20

    const/16 v20, 0x474a

    aput v20, v2, v19

    const/16 v19, 0x935

    aput v19, v2, v18

    const/16 v18, -0x5c94

    aput v18, v2, v17

    const/16 v17, -0x35

    aput v17, v2, v16

    const/16 v16, -0x7591

    aput v16, v2, v15

    const/16 v15, -0x12

    aput v15, v2, v14

    const/16 v14, -0x4ccf

    aput v14, v2, v13

    const/16 v13, -0x2a

    aput v13, v2, v12

    const/16 v12, -0x77

    aput v12, v2, v11

    const/16 v11, 0x5c64

    aput v11, v2, v10

    const/16 v10, 0x6228

    aput v10, v2, v9

    const/16 v9, -0x26be

    aput v9, v2, v8

    const/16 v8, -0x53

    aput v8, v2, v7

    const/16 v7, -0x5094

    aput v7, v2, v6

    const/16 v6, -0x29

    aput v6, v2, v3

    const/16 v3, 0x2e44

    aput v3, v2, v1

    const/16 v1, 0x2a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, 0x1a

    const/16 v32, 0x1b

    const/16 v33, 0x1c

    const/16 v34, 0x1d

    const/16 v35, 0x1e

    const/16 v36, 0x1f

    const/16 v37, 0x20

    const/16 v38, 0x21

    const/16 v39, 0x22

    const/16 v40, 0x23

    const/16 v41, 0x24

    const/16 v42, 0x25

    const/16 v43, 0x26

    const/16 v44, 0x27

    const/16 v45, 0x28

    const/16 v46, 0x29

    const/16 v47, -0x12a2

    aput v47, v1, v46

    const/16 v46, -0x13

    aput v46, v1, v45

    const/16 v45, -0x79

    aput v45, v1, v44

    const/16 v44, 0x6b1c

    aput v44, v1, v43

    const/16 v43, 0x206b

    aput v43, v1, v42

    const/16 v42, -0x54e0

    aput v42, v1, v41

    const/16 v41, -0x55

    aput v41, v1, v40

    const/16 v40, 0x3f7a

    aput v40, v1, v39

    const/16 v39, 0x233f

    aput v39, v1, v38

    const/16 v38, 0x2923

    aput v38, v1, v37

    const/16 v37, -0x30d7

    aput v37, v1, v36

    const/16 v36, -0x31

    aput v36, v1, v35

    const/16 v35, -0x2faa

    aput v35, v1, v34

    const/16 v34, -0x30

    aput v34, v1, v33

    const/16 v33, 0x7430

    aput v33, v1, v32

    const/16 v32, -0xf8c

    aput v32, v1, v31

    const/16 v31, -0x10

    aput v31, v1, v30

    const/16 v30, -0x29f8

    aput v30, v1, v29

    const/16 v29, -0x2a

    aput v29, v1, v28

    const/16 v28, -0x23

    aput v28, v1, v27

    const/16 v27, 0x7754

    aput v27, v1, v26

    const/16 v26, -0x4c89

    aput v26, v1, v25

    const/16 v25, -0x4d

    aput v25, v1, v24

    const/16 v24, 0x1923

    aput v24, v1, v23

    const/16 v23, 0x5019

    aput v23, v1, v22

    const/16 v22, 0x2f50

    aput v22, v1, v21

    const/16 v21, 0x472f

    aput v21, v1, v20

    const/16 v20, 0x947

    aput v20, v1, v19

    const/16 v19, -0x5cf7

    aput v19, v1, v18

    const/16 v18, -0x5d

    aput v18, v1, v17

    const/16 v17, -0x75b1

    aput v17, v1, v16

    const/16 v16, -0x76

    aput v16, v1, v15

    const/16 v15, -0x4cb0

    aput v15, v1, v14

    const/16 v14, -0x4d

    aput v14, v1, v13

    const/4 v13, -0x5

    aput v13, v1, v12

    const/16 v12, 0x5c0c

    aput v12, v1, v11

    const/16 v11, 0x625c

    aput v11, v1, v10

    const/16 v10, -0x269e

    aput v10, v1, v9

    const/16 v9, -0x27

    aput v9, v1, v8

    const/16 v8, -0x50fb

    aput v8, v1, v7

    const/16 v7, -0x51

    aput v7, v1, v6

    const/16 v6, 0x2e01

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_3
    array-length v6, v1

    if-lt v3, v6, :cond_3

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_4
    array-length v6, v1

    if-lt v3, v6, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$22;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isFragmetRunning:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5600(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_5
    return-void

    :cond_1
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_3
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_4
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$22;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->scanbutton:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$3200(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/Button;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$22;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->scan:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$22;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->spinner:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$4800(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/ProgressBar;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$22;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->scanningLayout:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$4900(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/RelativeLayout;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$22;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScannedDevicesList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1600(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$22;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->IsScreenLocked:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5700(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Z

    move-result v1

    if-nez v1, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$22;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->nodevices:Landroid/widget/FrameLayout;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1000(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/FrameLayout;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$22;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->noDeviceStringResId:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1200(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$22;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->nodevicesText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$900(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/TextView;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$22;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$22;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->noDeviceStringResId:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1200(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$22;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScannedlist:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/LinearLayout;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_5

    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$22;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->noDeviceString:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1300(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/lang/String;

    move-result-object v1

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$22;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->nodevicesText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$900(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/TextView;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$22;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->noDeviceString:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1300(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_6

    :catch_0
    move-exception v1

    goto/16 :goto_0

    :catch_1
    move-exception v1

    goto :goto_6
.end method
