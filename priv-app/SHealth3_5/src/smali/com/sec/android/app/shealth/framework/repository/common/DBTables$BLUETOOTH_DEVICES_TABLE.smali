.class public Lcom/sec/android/app/shealth/framework/repository/common/DBTables$BLUETOOTH_DEVICES_TABLE;
.super Ljava/lang/Object;
.source "DBTables.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/repository/common/DBTables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BLUETOOTH_DEVICES_TABLE"
.end annotation


# static fields
.field public static final KEY_DATA_TYPE:Ljava/lang/String; = "data_type"

.field public static final KEY_DEFAULT_WEB_SITE:Ljava/lang/String; = "def_web_site"

.field public static final KEY_MANUFACTURE:Ljava/lang/String; = "manufacture"

.field public static final KEY_ROWID:Ljava/lang/String; = "_id"

.field public static final KEY_SYS_NAME:Ljava/lang/String; = "sys_name"

.field public static final KEY_USER_NAME:Ljava/lang/String; = "user_name"

.field public static final TABLE_NAME:Ljava/lang/String; = "bluetooth_devices"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
