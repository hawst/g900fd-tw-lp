.class public Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;
.super Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;
.source "SuggestedMissionsActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$2;,
        Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mDateChangeCallback:Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

.field private mFromWhere:I

.field private mSelectedGoalId:I

.field private mTotalMissionCount:I

.field private mUpdateListAsyncTask:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;-><init>()V

    .line 31
    const/16 v0, 0xb

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mFromWhere:I

    .line 293
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mDateChangeCallback:Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

    .line 316
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mSelectedGoalId:I

    return v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mFromWhere:I

    return v0
.end method


# virtual methods
.method public closeScreen()V
    .locals 2

    .prologue
    .line 64
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->onBackPressed()V

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mFromWhere:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    .line 67
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mSelectedGoalId:I

    sget-object v1, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->USER_CANCELED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->UpdateGoalStatus(ILcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;)V

    .line 70
    :cond_0
    return-void
.end method

.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 85
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->customizeActionBar()V

    .line 86
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f090c7a

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitleIconVisibility(Z)V

    .line 88
    return-void
.end method

.method protected getAdapter()Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;
    .locals 4

    .prologue
    .line 176
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->clearAllData()V

    .line 177
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mSelectedGoalId:I

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mFromWhere:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->createMissionListData(Landroid/content/Context;II)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mListData:Ljava/util/List;

    .line 178
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mListData:Ljava/util/List;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v2, -0x1

    .line 113
    const/16 v1, 0xdc

    if-ne p1, v1, :cond_0

    .line 115
    const/16 v1, 0xdd

    if-ne p2, v1, :cond_1

    .line 117
    if-eqz p3, :cond_0

    .line 118
    const-string v1, "intent_extra_index"

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 119
    .local v0, "removeIndex":I
    if-eq v0, v2, :cond_0

    .line 120
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mSuggestedListAdapter:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->getSectionForPosition(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mSuggestedListAdapter:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->getItem(I)Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->getTitleString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->removeItem(ILjava/lang/String;)V

    .line 122
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mSuggestedListAdapter:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->notifyDataSetChanged()V

    .line 172
    .end local v0    # "removeIndex":I
    :cond_0
    :goto_0
    return-void

    .line 125
    :cond_1
    const/16 v1, 0xde

    if-ne p2, v1, :cond_0

    .line 126
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->clearAllData()V

    .line 128
    const-string v1, "EXTRA_NAME_MISSION_FROM_WHERE"

    const/16 v2, 0xb

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mFromWhere:I

    .line 129
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mSelectedGoalId:I

    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mFromWhere:I

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->createMissionListData(Landroid/content/Context;II)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mListData:Ljava/util/List;

    .line 130
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mSuggestedListAdapter:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mListData:Ljava/util/List;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->setItemsList(Ljava/util/List;)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 75
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->onBackPressed()V

    .line 76
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mFromWhere:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    .line 78
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mSelectedGoalId:I

    sget-object v1, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->USER_CANCELED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->UpdateGoalStatus(ILcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;)V

    .line 81
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 42
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 43
    const-string v1, "EXTRA_NAME_GOAL_ID"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mSelectedGoalId:I

    .line 44
    const-string v1, "EXTRA_NAME_MISSION_FROM_WHERE"

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mFromWhere:I

    .line 47
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->setActionBarNormalMode()V

    .line 49
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->setHeaderMessage()V

    .line 51
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mDateChangeCallback:Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->registerDayChangedListener(Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;)V

    .line 52
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 56
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->onDestroy()V

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mDateChangeCallback:Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->unregisterDayChangedListener(Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;)V

    .line 59
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 92
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-super/range {p0 .. p5}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 94
    if-nez p3, :cond_1

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 98
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mSuggestedListAdapter:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;

    add-int/lit8 v3, p3, -0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->getItem(I)Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    .line 99
    .local v1, "missionData":Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    if-eqz v1, :cond_0

    .line 100
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 101
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v2, 0x24000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 102
    const-string v2, "intent_extra_index"

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 103
    const-string v2, "EXTRA_NAME_GOAL_ID"

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getGoalID()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 104
    const-string v2, "EXTRA_NAME_MISSION_ID"

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getId()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 105
    const-string v2, "EXTRA_NAME_MISSION_FROM_WHERE"

    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mFromWhere:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 106
    const/16 v2, 0xdc

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public setActionBarNormalMode()V
    .locals 1

    .prologue
    .line 221
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    .line 223
    .local v0, "actoiniBarHelper":Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    if-nez v0, :cond_0

    .line 226
    :goto_0
    return-void

    .line 225
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    goto :goto_0
.end method

.method protected setHeaderMessage()V
    .locals 4

    .prologue
    .line 230
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mSuggestedListAdapter:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->getCount()I

    move-result v2

    if-nez v2, :cond_0

    .line 231
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mCommentView:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mNoSuggestTextView:Landroid/widget/TextView;

    const v3, 0x7f090333

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 233
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mNoSuggestDescView:Landroid/widget/TextView;

    const v3, 0x7f090332

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 239
    :goto_0
    return-void

    .line 235
    :cond_0
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mSelectedGoalId:I

    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->getMissionCoachMessage(I)Ljava/lang/String;

    move-result-object v0

    .line 236
    .local v0, "coachMsg":Ljava/lang/String;
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mSelectedGoalId:I

    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->getGoalInfoForMission(I)Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;

    move-result-object v1

    .line 237
    .local v1, "goalInfo":Lcom/cigna/coach/apiobjects/GoalInfo;
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->updateHeaderMessage(Ljava/lang/String;Lcom/cigna/coach/apiobjects/GoalInfo;)V

    goto :goto_0
.end method

.method public updateHeaderMessage(Ljava/lang/String;Lcom/cigna/coach/apiobjects/GoalInfo;)V
    .locals 6
    .param p1, "coachMsg"    # Ljava/lang/String;
    .param p2, "goalInfo"    # Lcom/cigna/coach/apiobjects/GoalInfo;

    .prologue
    const/4 v5, 0x0

    .line 242
    const/4 v2, 0x0

    .line 243
    .local v2, "goalName":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0004

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    if-nez v3, :cond_4

    .line 244
    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mFromWhere:I

    const/16 v4, 0x16

    if-ne v3, v4, :cond_2

    .line 245
    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mSelectedGoalId:I

    invoke-static {v3}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->getGoalInfoForMission(I)Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;

    move-result-object v1

    .line 246
    .local v1, "gimi":Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
    if-eqz v1, :cond_1

    .line 247
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->getGoal()Ljava/lang/String;

    move-result-object v2

    .line 259
    .end local v1    # "gimi":Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mHeaderTitleView:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 260
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mHeaderTitleView:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 261
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mCommentView:Landroid/widget/TextView;

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 273
    :goto_1
    return-void

    .line 249
    .restart local v1    # "gimi":Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
    :cond_1
    const-string v2, ""

    goto :goto_0

    .line 251
    .end local v1    # "gimi":Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
    :cond_2
    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mFromWhere:I

    const/16 v4, 0xb

    if-ne v3, v4, :cond_0

    .line 252
    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mSelectedGoalId:I

    invoke-static {v3}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->getAvailableGoalData(I)Lcom/cigna/coach/apiobjects/GoalInfo;

    move-result-object v0

    .line 253
    .local v0, "gi":Lcom/cigna/coach/apiobjects/GoalInfo;
    if-eqz v0, :cond_3

    .line 254
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/GoalInfo;->getGoal()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 256
    :cond_3
    const-string v2, ""

    goto :goto_0

    .line 263
    .end local v0    # "gi":Lcom/cigna/coach/apiobjects/GoalInfo;
    :cond_4
    if-nez p2, :cond_5

    .line 264
    sget-object v3, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->TAG:Ljava/lang/String;

    const-string v4, "SuggesteedMission Header GoalInfo is null~~"

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mHeaderTitleView:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 270
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mHeaderTitleView:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 271
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mCommentView:Landroid/widget/TextView;

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 267
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mHeaderTitleView:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/GoalInfo;->getGoal()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method protected updateList()V
    .locals 3

    .prologue
    .line 277
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mUpdateListAsyncTask:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;

    if-eqz v0, :cond_0

    .line 278
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UpdateListAsyncTask status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mUpdateListAsyncTask:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$2;->$SwitchMap$android$os$AsyncTask$Status:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mUpdateListAsyncTask:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/AsyncTask$Status;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 289
    :cond_0
    :goto_0
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;-><init>(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mUpdateListAsyncTask:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mUpdateListAsyncTask:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 291
    return-void

    .line 282
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;->mUpdateListAsyncTask:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity$UpdateListAsyncTask;->cancel(Z)Z

    goto :goto_0

    .line 279
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
