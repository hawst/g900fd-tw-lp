.class Lcom/sec/android/app/shealth/uv/UvSummaryFragment$24;
.super Ljava/lang/Object;
.source "UvSummaryFragment.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->animate(Landroid/widget/ImageView;[IIZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

.field final synthetic val$forever:Z

.field final synthetic val$imageIndex:I

.field final synthetic val$imageView:Landroid/widget/ImageView;

.field final synthetic val$images:[I

.field final synthetic val$issunLoading:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;[IILandroid/widget/ImageView;ZZ)V
    .locals 0

    .prologue
    .line 2506
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$24;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iput-object p2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$24;->val$images:[I

    iput p3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$24;->val$imageIndex:I

    iput-object p4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$24;->val$imageView:Landroid/widget/ImageView;

    iput-boolean p5, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$24;->val$forever:Z

    iput-boolean p6, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$24;->val$issunLoading:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 6
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 2508
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$24;->val$images:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$24;->val$imageIndex:I

    if-le v0, v1, :cond_1

    .line 2509
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$24;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$24;->val$imageView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$24;->val$images:[I

    iget v3, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$24;->val$imageIndex:I

    add-int/lit8 v3, v3, 0x1

    iget-boolean v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$24;->val$forever:Z

    iget-boolean v5, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$24;->val$issunLoading:Z

    # invokes: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->animate(Landroid/widget/ImageView;[IIZZ)V
    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$3200(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;Landroid/widget/ImageView;[IIZZ)V

    .line 2516
    :cond_0
    :goto_0
    return-void

    .line 2512
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$24;->val$forever:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2513
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$24;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$24;->val$imageView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$24;->val$images:[I

    const/4 v3, 0x0

    iget-boolean v4, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$24;->val$forever:Z

    iget-boolean v5, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$24;->val$issunLoading:Z

    # invokes: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->animate(Landroid/widget/ImageView;[IIZZ)V
    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$3200(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;Landroid/widget/ImageView;[IIZZ)V

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 2520
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 2524
    return-void
.end method
