.class public Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "ExerciseProCalorieGoalActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field protected OldValue:I

.field private averageValueTextView:Landroid/widget/TextView;

.field private bestRecordValueTextView:Landroid/widget/TextView;

.field private calorieInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

.field private doneButtonListener:Landroid/view/View$OnClickListener;

.field private editText:Landroid/widget/EditText;

.field private goalValue:I

.field private goaltitle:Landroid/widget/TextView;

.field private goalunit:Ljava/lang/String;

.field private intent:Landroid/content/Intent;

.field private mActivityType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 53
    const/16 v0, 0x4653

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->mActivityType:I

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->checkDataAndSave()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->checkChangesAndExit()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->calorieInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->goalValue:I

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->goalValue:I

    return p1
.end method

.method private checkChangesAndExit()V
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->calorieInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->getValue()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->goalValue:I

    .line 268
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->isDataChanged()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->showResetPopup()V

    .line 272
    :goto_0
    return-void

    .line 271
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->finish()V

    goto :goto_0
.end method

.method private checkDataAndSave()V
    .locals 5

    .prologue
    const/16 v4, 0x2706

    const/16 v3, 0xa

    .line 101
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->calorieInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->getValue()F

    move-result v2

    float-to-int v0, v2

    .line 102
    .local v0, "resultGoal":I
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->calorieInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->isValueEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    if-gt v0, v4, :cond_0

    if-ge v0, v3, :cond_2

    .line 104
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->showAbnormalGoalWarningPopup()V

    .line 117
    :cond_1
    :goto_0
    return-void

    .line 106
    :cond_2
    if-lt v0, v3, :cond_1

    if-gt v0, v4, :cond_1

    .line 107
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getCurrentCalorie()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 108
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->showUnderGoalWarningPopup()V

    goto :goto_0

    .line 110
    :cond_3
    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->goalValue:I

    .line 111
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->saveGoalAndFinish()V

    .line 112
    const v2, 0x7f090f72

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    .line 113
    .local v1, "toast":Landroid/widget/Toast;
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private initHorizontalInputModule()V
    .locals 4

    .prologue
    const v3, 0x461c1800    # 9990.0f

    const/high16 v2, 0x41200000    # 10.0f

    .line 275
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->calorieInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->goalunit:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->setUnit(Ljava/lang/CharSequence;)V

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->calorieInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->setMoveDistance(F)V

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->calorieInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->setInputRange(FF)V

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->calorieInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->setNormalRange(FF)V

    .line 279
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->goalValue:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->goalValue:I

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->calorieInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->goalValue:I

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->setValue(FZ)V

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->editText:Landroid/widget/EditText;

    const-string v1, "inputType=YearDateTime_edittext"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 282
    return-void
.end method

.method private isDataChanged()Z
    .locals 2

    .prologue
    .line 241
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->OldValue:I

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->goalValue:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private saveGoalAndFinish()V
    .locals 5

    .prologue
    const/16 v3, 0x2706

    const/16 v2, 0xa

    const/4 v4, 0x4

    .line 120
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->goalValue:I

    if-gt v1, v2, :cond_0

    .line 121
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->goalValue:I

    .line 123
    :cond_0
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->goalValue:I

    if-lt v1, v3, :cond_1

    .line 124
    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->goalValue:I

    .line 126
    :cond_1
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->mActivityType:I

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->goalValue:I

    invoke-static {p0, v1, v4, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->setActivityTypeGoalValue(Landroid/content/Context;III)I

    .line 127
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->mActivityType:I

    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveRealtimeActivityGoalType(II)V

    .line 129
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->TAG:Ljava/lang/String;

    const-string v2, "CalorieGoal: saveGoalAndFinish"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth.plugins.exercisepro"

    const-string v3, "G004"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 133
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "value"

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->goalValue:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 134
    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 135
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->setResult(ILandroid/content/Intent;)V

    .line 136
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->finish()V

    .line 137
    return-void
.end method

.method private setAvgText(F)V
    .locals 4
    .param p1, "avg"    # F

    .prologue
    .line 320
    invoke-static {p1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result p1

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->averageValueTextView:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0900b9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 322
    return-void
.end method

.method private setBestRecordText(F)V
    .locals 4
    .param p1, "best"    # F

    .prologue
    .line 325
    invoke-static {p1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result p1

    .line 326
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->bestRecordValueTextView:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0900b9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 327
    return-void
.end method

.method private setLastGoalValueToEditText()V
    .locals 2

    .prologue
    .line 98
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->mActivityType:I

    const/4 v1, 0x4

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getActivityTypeGoalValue(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->goalValue:I

    .line 99
    return-void
.end method

.method private setRecordTimeText()V
    .locals 15

    .prologue
    .line 285
    const-wide/16 v9, 0x0

    .local v9, "best":J
    const-wide/16 v6, 0x0

    .line 286
    .local v6, "avg":J
    const-string v8, "AVG_KCAL"

    .line 287
    .local v8, "avgColumn":Ljava/lang/String;
    const-string v11, "BEST_KCAL"

    .line 288
    .local v11, "bestColumn":Ljava/lang/String;
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string/jumbo v1, "total_calorie"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "MAX(total_calorie) AS "

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    const/4 v0, 0x3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "AVG(total_calorie) AS "

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    .line 292
    .local v2, "projection":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "exercise_info__id == "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->mActivityType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "end_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">? "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "exercise_type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " == ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " OR "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "exercise_type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " == ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 295
    .local v3, "selectionClause":Ljava/lang/String;
    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-wide/16 v13, 0x0

    invoke-static {v13, v14}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    const/16 v1, 0x4e21

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const/16 v1, 0x4e22

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 298
    .local v4, "selectionArg":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 299
    .local v5, "sortOrder":Ljava/lang/String;
    const/4 v12, 0x0

    .line 302
    .local v12, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 304
    if-eqz v12, :cond_0

    .line 305
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 306
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 307
    invoke-interface {v12, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 308
    invoke-interface {v12, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v6

    .line 312
    :cond_0
    if-eqz v12, :cond_1

    .line 313
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 315
    :cond_1
    long-to-float v0, v6

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->setAvgText(F)V

    .line 316
    long-to-float v0, v9

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->setBestRecordText(F)V

    .line 317
    return-void

    .line 312
    :catchall_0
    move-exception v0

    if-eqz v12, :cond_2

    .line 313
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private showAbnormalGoalWarningPopup()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 189
    const v4, 0x7f090f51

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/16 v6, 0xa

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    const/4 v6, 0x1

    const/16 v7, 0x2706

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 191
    .local v3, "rangeString":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0900b9

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 192
    .local v2, "kCal":Ljava/lang/String;
    invoke-virtual {v3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 193
    const-string v4, "kcal"

    invoke-virtual {v3, v4, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 195
    :cond_0
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v1, p0, v8}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 197
    .local v1, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    new-instance v4, Ljava/lang/StringBuilder;

    const v5, 0x7f090f54

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "\n\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 207
    .local v0, "alertTextString":Ljava/lang/StringBuilder;
    const v4, 0x7f090b60

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v4

    const/high16 v5, 0x20000

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->addFlags(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity$4;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity$4;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 223
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->TAG:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 224
    return-void
.end method

.method private showResetPopup()V
    .locals 4

    .prologue
    .line 175
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 177
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const v1, 0x7f090f4d

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090f4c

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090f4e

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity$3;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x20000

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->addFlags(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 185
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 186
    return-void
.end method

.method private showUnderGoalWarningPopup()V
    .locals 4

    .prologue
    .line 227
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 229
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const v1, 0x7f0900e3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090a8b

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x20000

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->addFlags(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity$5;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity$5;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 237
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 238
    return-void
.end method


# virtual methods
.method protected calculateRecommendedValue()V
    .locals 2

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->mActivityType:I

    const/4 v1, 0x4

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getActivityTypeGoalValue(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->OldValue:I

    .line 142
    return-void
.end method

.method protected customizeActionBar()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 147
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 148
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    const v4, 0x7f090a25

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 150
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;)V

    .line 166
    .local v0, "actionBarButtonListener":Landroid/view/View$OnClickListener;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setHomeButtonVisible(Z)V

    .line 167
    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v3, 0x7f090048

    invoke-direct {v2, v5, v3, v0, v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;Z)V

    .line 168
    .local v2, "actionButton3":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    new-array v4, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v2, v4, v5

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 170
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v3, 0x7f090044

    invoke-direct {v1, v5, v3, v0, v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;Z)V

    .line 171
    .local v1, "actionButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    new-array v4, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v1, v4, v5

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 172
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 253
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->finish()V

    .line 254
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 262
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 263
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->customizeActionBar()V

    .line 264
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v4, 0x7f0807e2

    const/4 v3, 0x1

    .line 58
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 59
    const v0, 0x7f0301a1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->setContentView(I)V

    .line 60
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->intent:Landroid/content/Intent;

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->intent:Landroid/content/Intent;

    const-string/jumbo v1, "pick_type"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "pick_type"

    const/16 v2, 0x4653

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->mActivityType:I

    .line 64
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->calculateRecommendedValue()V

    .line 65
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->setLastGoalValueToEditText()V

    .line 66
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->goaltitle:Landroid/widget/TextView;

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->goaltitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090a25

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    const v0, 0x7f0807e4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->averageValueTextView:Landroid/widget/TextView;

    .line 70
    const v0, 0x7f0807e7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->bestRecordValueTextView:Landroid/widget/TextView;

    .line 72
    const v0, 0x7f0900b9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->goalunit:Ljava/lang/String;

    .line 74
    const v0, 0x7f080740

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->calorieInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->calorieInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->setFilterText(Z)V

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->calorieInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->setType(I)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->calorieInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->setEnabled(Z)V

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->calorieInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->getValueEditText()Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->editText:Landroid/widget/EditText;

    .line 82
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->setRecordTimeText()V

    .line 84
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 85
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->doneButtonListener:Landroid/view/View$OnClickListener;

    .line 92
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->initHorizontalInputModule()V

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->calorieInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->setEditTextSize(I)V

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->calorieInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->removeEditTextFocus()V

    .line 95
    return-void
.end method
