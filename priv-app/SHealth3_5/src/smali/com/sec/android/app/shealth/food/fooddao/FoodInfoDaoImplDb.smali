.class public Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;
.super Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;
.source "FoodInfoDaoImplDb.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb",
        "<",
        "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
        ">;",
        "Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;"
    }
.end annotation


# static fields
.field public static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mFixServerDuplicationsWasApplied:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$FoodInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;-><init>(Landroid/net/Uri;Landroid/content/Context;)V

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->mFixServerDuplicationsWasApplied:Z

    .line 58
    return-void
.end method

.method private fixDuplicationServerData(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "duplicatedFoodInfo":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 229
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v5, :cond_0

    .line 230
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 231
    .local v1, "replaceBy":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    invoke-interface {p1, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 233
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0, v5}, Landroid/content/ContentValues;-><init>(I)V

    .line 234
    .local v0, "idField":Landroid/content/ContentValues;
    const-string v3, "food_info__id"

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 236
    new-instance v2, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string v3, "food_info__id"

    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->convertDataIdToCsv(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "IN"

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 239
    .local v2, "where":Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$MealItem;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->getSQLCode()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v0, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 242
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->deleteData(Ljava/util/List;)I

    .line 247
    .end local v0    # "idField":Landroid/content/ContentValues;
    .end local v1    # "replaceBy":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .end local v2    # "where":Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;
    :goto_0
    return-void

    .line 244
    :cond_0
    sget-object v3, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->LOG_TAG:Ljava/lang/String;

    const-string v4, "fixDuplicationServerData couldn\'t operate with input data. duplicatedFoodInfo should be more then 2 records."

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateMealSyncStatus()V
    .locals 10

    .prologue
    .line 199
    const-string/jumbo v3, "select distinct M._id from meal as M, meal_item as MI where M._id = MI.meal__id and MI.sync_status != 170001 and M.sync_status == 170001"

    .line 200
    .local v3, "rawQuery":Ljava/lang/String;
    const/4 v6, 0x0

    .line 203
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 204
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 206
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 207
    .local v9, "values":Landroid/content/ContentValues;
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_1

    .line 209
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 210
    .local v7, "mealId":J
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Meal;->CONTENT_URI:Landroid/net/Uri;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v9, v2, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 211
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 217
    .end local v7    # "mealId":J
    .end local v9    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_0

    .line 218
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 217
    :cond_1
    if-eqz v6, :cond_2

    .line 218
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 220
    :cond_2
    return-void
.end method

.method private updateUniqueServerRecord(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)I
    .locals 11
    .param p1, "item"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 101
    new-instance v5, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    new-instance v7, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string/jumbo v8, "server_food_id"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerId()Ljava/lang/String;

    move-result-object v9

    const-string v10, "="

    invoke-direct {v7, v8, v9, v10}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v5, v7}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;-><init>(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)V

    new-instance v7, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string/jumbo v8, "server_source_type"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerSourceType()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const-string v10, "="

    invoke-direct {v7, v8, v9, v10}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->AND(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v4

    .line 104
    .local v4, "serverUniqueFilterBuilder":Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->getAllDatas(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Ljava/util/List;

    move-result-object v0

    .line 105
    .local v0, "foodInfoDatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 120
    iget-boolean v5, p0, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->mFixServerDuplicationsWasApplied:Z

    if-nez v5, :cond_3

    .line 121
    sget-object v5, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->LOG_TAG:Ljava/lang/String;

    const-string v6, "Need fix duplications before update"

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/Logging;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->findAndFixAllDuplication()V

    .line 123
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->mFixServerDuplicationsWasApplied:Z

    .line 124
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->updateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)I

    move-result v6

    :goto_0
    :pswitch_0
    return v6

    .line 110
    :pswitch_1
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v7

    const-wide/16 v9, -0x1

    cmp-long v5, v7, v9

    if-nez v5, :cond_1

    .line 113
    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getFavorite()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getFavorite()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 114
    .local v3, "isFavorite":Z
    :cond_0
    :goto_1
    invoke-virtual {p1, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setFavorite(Z)V

    .line 116
    .end local v3    # "isFavorite":Z
    :cond_1
    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v1

    .line 117
    .local v1, "id":J
    invoke-virtual {p1, v1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setId(J)V

    .line 118
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->updateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)I

    move-result v6

    goto :goto_0

    .end local v1    # "id":J
    :cond_2
    move v3, v6

    .line 113
    goto :goto_1

    .line 126
    :cond_3
    new-instance v5, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Multiple FoodInfo data stored in DB("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "). Should be one!"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 105
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public deleteDataById(J)Z
    .locals 5
    .param p1, "itemId"    # J

    .prologue
    .line 77
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string v1, "_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const-string v3, "="

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    .local v0, "filter":Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->uri:Landroid/net/Uri;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public findAndFixAllDuplication()V
    .locals 15

    .prologue
    .line 137
    const-string/jumbo v12, "repeatServerRecords"

    .line 138
    .local v12, "repeatServerRecords":Ljava/lang/String;
    new-instance v10, Ljava/util/ArrayList;

    const/4 v0, 0x3

    invoke-direct {v10, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 139
    .local v10, "projection":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v0, "COUNT(_id) as repeatServerRecords"

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 140
    const-string/jumbo v0, "server_source_type"

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    const-string/jumbo v0, "server_food_id"

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 143
    const-string v8, " GROUP BY server_food_id, server_source_type"

    .line 146
    .local v8, "group":Ljava/lang/String;
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    new-instance v1, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string/jumbo v2, "server_source_type"

    const v4, 0x46cd1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const-string v5, "<>"

    invoke-direct {v1, v2, v4, v5}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;-><init>(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)V

    new-instance v1, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string/jumbo v2, "server_source_type"

    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const-string v5, "<>"

    invoke-direct {v1, v2, v4, v5}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->AND(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string/jumbo v2, "server_source_type"

    const v4, 0x46cd6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const-string v5, "<>"

    invoke-direct {v1, v2, v4, v5}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->AND(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string/jumbo v2, "server_source_type"

    const v4, 0x46cd7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const-string v5, "<>"

    invoke-direct {v1, v2, v4, v5}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->AND(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v9

    .line 162
    .local v9, "onlyServer":Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->convertCollectionToCsvString(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "food_info"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->getSQLCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 166
    .local v3, "select":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 169
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 170
    const-string/jumbo v0, "repeatServerRecords"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 171
    .local v11, "repeatColumn":I
    const-string/jumbo v0, "server_source_type"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 172
    .local v14, "serverSourceType":I
    const-string/jumbo v0, "server_food_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 174
    .local v13, "serverId":I
    :cond_0
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 176
    invoke-interface {v6, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 178
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    new-instance v1, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string/jumbo v2, "server_food_id"

    invoke-interface {v6, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "="

    invoke-direct {v1, v2, v4, v5}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;-><init>(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)V

    new-instance v1, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string/jumbo v2, "server_source_type"

    invoke-interface {v6, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const-string v5, "="

    invoke-direct {v1, v2, v4, v5}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->AND(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v7

    .line 188
    .local v7, "duplicationsClause":Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->getAllDatas(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->fixDuplicationServerData(Ljava/util/List;)V

    goto :goto_0

    .line 191
    .end local v7    # "duplicationsClause":Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 192
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->updateMealSyncStatus()V

    .line 194
    .end local v11    # "repeatColumn":I
    .end local v13    # "serverId":I
    .end local v14    # "serverSourceType":I
    :cond_2
    return-void
.end method

.method public bridge synthetic getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    .locals 1
    .param p1, "x0"    # J

    .prologue
    .line 48
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v0

    return-object v0
.end method

.method public getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .locals 5
    .param p1, "id"    # J

    .prologue
    .line 64
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string v2, "_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const-string v4, "="

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    .local v0, "filter":Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->getAllDatas(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Ljava/util/List;

    move-result-object v1

    .line 66
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 67
    :cond_0
    const/4 v2, 0x0

    .line 71
    :goto_0
    return-object v2

    .line 68
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_2

    .line 69
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "We recieve two items by unique ID, check implementation!"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 71
    :cond_2
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    goto :goto_0
.end method

.method public bridge synthetic getDataContentValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)Landroid/content/ContentValues;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    .prologue
    .line 48
    check-cast p1, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .end local p1    # "x0":Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->getDataContentValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)Landroid/content/ContentValues;

    move-result-object v0

    return-object v0
.end method

.method public getDataContentValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)Landroid/content/ContentValues;
    .locals 3
    .param p1, "item"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .prologue
    .line 251
    if-nez p1, :cond_0

    .line 252
    const/4 v1, 0x0

    .line 266
    :goto_0
    return-object v1

    .line 254
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)V

    .line 255
    .local v0, "foodInfo":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;
    const-string v1, "kcal"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 256
    const-string v1, "favorite"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getFavorite()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 257
    const-string/jumbo v1, "name"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    const-string/jumbo v1, "server_food_id"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    const-string/jumbo v1, "server_source_type"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerSourceType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 260
    const-string v1, "description"

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getDescription(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    const-string/jumbo v1, "server_locale"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerLocale()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    const-string/jumbo v1, "server_root_category"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerRootCategory()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    const-string/jumbo v1, "server_root_category_id"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerRootCategoryId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    const-string/jumbo v1, "server_sub_category"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerSubCategory()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    const-string/jumbo v1, "server_sub_category_id"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerSubCategoryId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->getContentValues()Landroid/content/ContentValues;

    move-result-object v1

    goto/16 :goto_0
.end method

.method public getFoodInfoFrequencyByMeal(JJI)Ljava/util/Map;
    .locals 19
    .param p1, "periodStart"    # J
    .param p3, "periodEnd"    # J
    .param p5, "mealType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJI)",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 293
    new-instance v17, Ljava/util/LinkedHashMap;

    invoke-direct/range {v17 .. v17}, Ljava/util/LinkedHashMap;-><init>()V

    .line 295
    .local v17, "result":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Ljava/lang/Integer;>;"
    const/4 v1, 0x3

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    const/4 v1, 0x1

    invoke-static/range {p3 .. p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    const/4 v1, 0x2

    invoke-static/range {p5 .. p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    .line 296
    .local v5, "argsMeal":[Ljava/lang/String;
    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v3, v1

    .line 297
    .local v3, "projectionMeal":[Ljava/lang/String;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    .line 298
    .local v18, "selectMeal":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "sample_time"

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " >= ? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "sample_time"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " <= ? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "type"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = ?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 307
    const/4 v13, 0x0

    .line 308
    .local v13, "cursor":Landroid/database/Cursor;
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    .line 311
    .local v16, "mealIds":Ljava/lang/StringBuilder;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Meal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 313
    if-eqz v13, :cond_1

    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 315
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id"

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 316
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 317
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    const-string v4, ""

    move-object/from16 v0, v16

    invoke-virtual {v0, v1, v2, v4}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 321
    :cond_1
    if-eqz v13, :cond_2

    .line 322
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 325
    :cond_2
    const-string v15, "FoodInfoCount"

    .line 327
    .local v15, "foodInfoIdCountField":Ljava/lang/String;
    const/4 v1, 0x2

    new-array v8, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "food_info__id"

    aput-object v2, v8, v1

    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "COUNT(food_info__id) AS "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v8, v1

    .line 329
    .local v8, "projectionMealItem":[Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "meal__id in ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") GROUP BY ( "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "food_info__id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 333
    .local v9, "selectMealItem":Ljava/lang/String;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/sdk/health/content/ShealthContract$MealItem;->CONTENT_URI:Landroid/net/Uri;

    const/4 v10, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " DESC"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 335
    if-eqz v13, :cond_5

    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 336
    const/4 v12, 0x0

    .line 339
    .local v12, "count":I
    :cond_3
    const-string v1, "food_info__id"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v14

    .line 340
    .local v14, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    if-eqz v14, :cond_4

    invoke-virtual {v14}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerSourceType()I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isQuickInputType(I)Z

    move-result v1

    if-nez v1, :cond_4

    .line 341
    invoke-interface {v13, v15}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-interface {v0, v14, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 345
    add-int/lit8 v12, v12, 0x1

    .line 348
    :cond_4
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    if-eqz v1, :cond_5

    const/16 v1, 0x1e

    if-lt v12, v1, :cond_3

    .line 353
    .end local v12    # "count":I
    .end local v14    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    :cond_5
    if-eqz v13, :cond_6

    .line 354
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 357
    :cond_6
    return-object v17

    .line 321
    .end local v8    # "projectionMealItem":[Ljava/lang/String;
    .end local v9    # "selectMealItem":Ljava/lang/String;
    .end local v15    # "foodInfoIdCountField":Ljava/lang/String;
    :catchall_0
    move-exception v1

    if-eqz v13, :cond_7

    .line 322
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v1

    .line 353
    .restart local v8    # "projectionMealItem":[Ljava/lang/String;
    .restart local v9    # "selectMealItem":Ljava/lang/String;
    .restart local v15    # "foodInfoIdCountField":Ljava/lang/String;
    :catchall_1
    move-exception v1

    if-eqz v13, :cond_8

    .line 354
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v1
.end method

.method public getFoodInfoListByMealId(J)Ljava/util/Set;
    .locals 8
    .param p1, "mealId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/Set",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 362
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 364
    .local v7, "result":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    new-array v2, v5, [Ljava/lang/String;

    const-string v0, "food_info__id"

    aput-object v0, v2, v1

    .line 365
    .local v2, "projectionMealItem":[Ljava/lang/String;
    const-string/jumbo v3, "meal__id = ? ) GROUP BY ( food_info__id"

    .line 367
    .local v3, "selectMealItem":Ljava/lang/String;
    new-array v4, v5, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    .line 368
    .local v4, "argsMealItem":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 370
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$MealItem;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 372
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 374
    :cond_0
    const-string v0, "food_info__id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 375
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 379
    :cond_1
    if-eqz v6, :cond_2

    .line 380
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 384
    :cond_2
    return-object v7

    .line 379
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 380
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method protected bridge synthetic getItemFromRow(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    .locals 1
    .param p1, "x0"    # Landroid/database/Cursor;

    .prologue
    .line 48
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->getItemFromRow(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v0

    return-object v0
.end method

.method protected getItemFromRow(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .locals 22
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 271
    new-instance v21, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;-><init>(Landroid/database/Cursor;)V

    .line 272
    .local v21, "getter":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;
    new-instance v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    const-string v3, "_id"

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    const-string/jumbo v5, "name"

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "kcal"

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getFloat(Ljava/lang/String;)F

    move-result v6

    const-string v7, "favorite"

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    const-string/jumbo v8, "server_food_id"

    move-object/from16 v0, v21

    invoke-virtual {v0, v8}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "server_source_type"

    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getInt(Ljava/lang/String;)I

    move-result v9

    const-string v10, "description"

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string/jumbo v11, "server_locale"

    move-object/from16 v0, v21

    invoke-virtual {v0, v11}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string/jumbo v12, "server_root_category"

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string/jumbo v13, "server_root_category_id"

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string/jumbo v14, "server_sub_category"

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const-string/jumbo v15, "server_sub_category_id"

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    const-string v16, "create_time"

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v16

    const-string/jumbo v18, "update_time"

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v18

    const-string/jumbo v20, "time_zone"

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getInt(Ljava/lang/String;)I

    move-result v20

    invoke-direct/range {v2 .. v20}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;-><init>(JLjava/lang/String;FZLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJI)V

    return-object v2
.end method

.method public bridge synthetic updateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)I
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    .prologue
    .line 48
    check-cast p1, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .end local p1    # "x0":Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->updateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)I

    move-result v0

    return v0
.end method

.method public updateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)I
    .locals 2
    .param p1, "item"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .prologue
    .line 83
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->isAddedByUser()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerSourceType()I

    move-result v0

    const v1, 0x46cd6

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerSourceType()I

    move-result v0

    const v1, 0x46cd7

    if-ne v0, v1, :cond_1

    .line 85
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->updateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)I

    move-result v0

    .line 88
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->updateUniqueServerRecord(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)I

    move-result v0

    goto :goto_0
.end method
