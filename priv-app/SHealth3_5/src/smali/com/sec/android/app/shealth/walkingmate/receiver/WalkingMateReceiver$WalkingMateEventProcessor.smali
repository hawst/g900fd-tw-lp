.class Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver$WalkingMateEventProcessor;
.super Ljava/lang/Thread;
.source "WalkingMateReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "WalkingMateEventProcessor"
.end annotation


# static fields
.field private static mLocaleChangedConnection:Landroid/content/ServiceConnection;


# instance fields
.field recvIntent:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 132
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver$WalkingMateEventProcessor$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver$WalkingMateEventProcessor$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver$WalkingMateEventProcessor;->mLocaleChangedConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;)V
    .locals 0
    .param p1, "_intent"    # Landroid/content/Intent;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver$WalkingMateEventProcessor;->recvIntent:Landroid/content/Intent;

    .line 52
    return-void
.end method

.method static synthetic access$400()Landroid/content/ServiceConnection;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver$WalkingMateEventProcessor;->mLocaleChangedConnection:Landroid/content/ServiceConnection;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    .line 57
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver$WalkingMateEventProcessor;->recvIntent:Landroid/content/Intent;

    invoke-virtual {v6}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 58
    .local v0, "action":Ljava/lang/String;
    # getter for: Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->access$000()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[**] action="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 60
    .local v1, "currentTime":J
    if-nez v0, :cond_0

    .line 130
    :goto_0
    return-void

    .line 64
    :cond_0
    # getter for: Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->sLock:Ljava/lang/Object;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->access$100()Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 65
    :try_start_0
    const-string v6, "com.sec.android.app.shealth.walkingmate.service.START_ALARM"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 66
    # getter for: Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->access$000()Ljava/lang/String;

    move-result-object v6

    const-string v8, "Start Alarm on receive"

    invoke-static {v6, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    monitor-exit v7

    goto :goto_0

    .line 126
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .line 69
    :cond_1
    :try_start_1
    const-string v6, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "com.sec.android.service.health.ContentProviderAccessible"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 71
    :cond_2
    # getter for: Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->access$000()Ljava/lang/String;

    move-result-object v6

    const-string v8, "[**] onReceive() - BOOT_COMPLETE THREAD START"

    invoke-static {v6, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->updatePedoWidget()V

    .line 74
    const-string v6, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 75
    const/4 v6, 0x0

    invoke-static {v6}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setNotifiedContentProviderAccessible(Z)V

    .line 77
    :cond_3
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    if-nez v6, :cond_4

    .line 78
    # getter for: Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->access$000()Ljava/lang/String;

    move-result-object v6

    const-string v8, "[**] Context() == null"

    invoke-static {v6, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    monitor-exit v7

    goto :goto_0

    .line 81
    :cond_4
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 82
    # getter for: Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->access$000()Ljava/lang/String;

    move-result-object v6

    const-string v8, "Profile is not ready."

    invoke-static {v6, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    monitor-exit v7

    goto :goto_0

    .line 86
    :cond_5
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->isSHealthMainAppinstalled()Z

    move-result v6

    if-nez v6, :cond_6

    .line 87
    new-instance v5, Landroid/content/ComponentName;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const-string v8, "com.sec.android.app.shealth.widget.homescreenwidget.WalkMateAppWidget"

    invoke-direct {v5, v6, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    .local v5, "widgetName":Landroid/content/ComponentName;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    const/4 v8, 0x0

    # invokes: Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->enableWidgets(Landroid/content/Context;Landroid/content/ComponentName;Z)V
    invoke-static {v6, v5, v8}, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->access$200(Landroid/content/Context;Landroid/content/ComponentName;Z)V

    .line 90
    # getter for: Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->access$000()Ljava/lang/String;

    move-result-object v6

    const-string v8, "[**] widget is disabled"

    invoke-static {v6, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->startClearCoverView(Landroid/content/Context;)V

    .line 93
    .end local v5    # "widgetName":Landroid/content/ComponentName;
    :cond_6
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isRun()Z

    move-result v6

    if-nez v6, :cond_7

    .line 94
    # getter for: Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->access$000()Ljava/lang/String;

    move-result-object v6

    const-string v8, "[**] ACTION_BOOT_COMPLETED - Day Step service start"

    invoke-static {v6, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    new-instance v3, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    const-class v8, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-direct {v3, v6, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 96
    .local v3, "iService":Landroid/content/Intent;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 98
    .end local v3    # "iService":Landroid/content/Intent;
    :cond_7
    # getter for: Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->access$000()Ljava/lang/String;

    move-result-object v6

    const-string v8, "[**] onReceive() - BOOT_COMPLETE THREAD ENDED"

    invoke-static {v6, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    :goto_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 128
    # getter for: Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->access$000()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "End action="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    # getter for: Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->access$000()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "End action duration = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v1

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 100
    :cond_8
    :try_start_2
    const-string v6, "com.sec.shealth.request.plugin.DELETE_APP_DATA"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 101
    # getter for: Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->access$000()Ljava/lang/String;

    move-result-object v6

    const-string v8, "ACTION_DELETE_PLUGIN_APP_DATA received"

    invoke-static {v6, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->clearWalkInfoData(Landroid/content/Context;)I

    .line 103
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->clearWalkInfoExtendedData(Landroid/content/Context;)I

    .line 104
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->clearWalkingGoalData(Landroid/content/Context;)I

    .line 106
    # getter for: Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->access$000()Ljava/lang/String;

    move-result-object v6

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ACTION_DELETE_PLUGIN_APP_DATA action="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 107
    :cond_9
    const-string v6, "android.intent.action.WEARABLE_DEVICE_SYNC_CHECK"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 108
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->getInstance()Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;

    move-result-object v6

    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->setInitialized(Z)V

    .line 109
    # getter for: Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->access$000()Ljava/lang/String;

    move-result-object v6

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SYNC_CHECK action="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    new-instance v4, Landroid/content/Intent;

    const-string v6, "android.intent.action.WEARABLE_DEVICE_SYNC"

    invoke-direct {v4, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 111
    .local v4, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 112
    .end local v4    # "intent":Landroid/content/Intent;
    :cond_a
    const-string v6, "com.sec.android.intent.action.WEARABLE_DEVICE_SYNC_AND_START"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 113
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->getInstance()Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;

    move-result-object v6

    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->setInitialized(Z)V

    .line 114
    # getter for: Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->access$000()Ljava/lang/String;

    move-result-object v6

    const-string v8, "[**] SYNC_AND_START"

    invoke-static {v6, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    # invokes: Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->getHomeScreenWidgetPendingIntent()Landroid/content/Intent;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->access$300()Landroid/content/Intent;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 116
    :cond_b
    const-string v6, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 119
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    new-instance v8, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    const-class v10, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-direct {v8, v9, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v9, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver$WalkingMateEventProcessor;->mLocaleChangedConnection:Landroid/content/ServiceConnection;

    const/4 v10, 0x1

    invoke-virtual {v6, v8, v9, v10}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    goto/16 :goto_1

    .line 124
    :cond_c
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver$WalkingMateEventProcessor;->recvIntent:Landroid/content/Intent;

    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/receiver/GearBroadcastChecker;->Checker(Landroid/content/Intent;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1
.end method
