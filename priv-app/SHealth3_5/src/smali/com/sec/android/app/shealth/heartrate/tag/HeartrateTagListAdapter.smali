.class public Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;
.super Landroid/widget/CursorAdapter;
.source "HeartrateTagListAdapter.java"


# instance fields
.field private mCheckAll:Z

.field private mCheckedItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDeleteMode:Z

.field private mMenuDeleteMode:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v0, 0x0

    .line 51
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 44
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->mDeleteMode:Z

    .line 45
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->mMenuDeleteMode:Z

    .line 46
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->mCheckAll:Z

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->mCheckedItems:Ljava/util/ArrayList;

    .line 52
    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 10
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    const v9, 0x7f02086c

    const v8, 0x106000d

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 132
    const v4, 0x7f0805fb

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 133
    .local v2, "cbBody":Landroid/widget/CheckBox;
    const v4, 0x7f0805fc

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 134
    .local v3, "tvBodyLeft":Landroid/widget/TextView;
    const-string v4, "AvgMonth"

    invoke-interface {p3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p3, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 137
    .local v0, "avgMonth":J
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    long-to-float v5, v0

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f0900d2

    invoke-virtual {p2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 140
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {p1, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 142
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->isMenuDeleteMode()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 143
    invoke-virtual {v2, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 144
    invoke-virtual {v3, v6, v6, v6, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 145
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->isCheckAll()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 146
    invoke-virtual {v2, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 172
    :goto_0
    return-void

    .line 147
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->getCheckedItems()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v2}, Landroid/widget/CheckBox;->getTag()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 148
    invoke-virtual {v2, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 150
    :cond_1
    invoke-virtual {v2, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 152
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->isDeleteMode()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 153
    invoke-virtual {v2, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 154
    invoke-virtual {v3, v6, v6, v6, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 155
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->isCheckAll()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 156
    invoke-virtual {p1, v9}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    .line 157
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->getCheckedItems()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 158
    invoke-virtual {p1, v9}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    .line 160
    :cond_4
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {p1, v4}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 165
    :cond_5
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a05e4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v3, v4, v6, v6, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 168
    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 169
    invoke-virtual {v2, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 170
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->getCheckedItems()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method

.method public clearCheckedItems()V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->mCheckedItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 109
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->mCheckAll:Z

    .line 110
    return-void
.end method

.method public getCheckedItems()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->mCheckedItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected getSelectedLogCount()I
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->mCheckedItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method protected getTotalChildCount()I
    .locals 3

    .prologue
    .line 180
    const/4 v0, 0x0

    .line 181
    .local v0, "childCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 182
    add-int/lit8 v0, v0, 0x1

    .line 181
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 185
    :cond_0
    return v0
.end method

.method public isCheckAll()Z
    .locals 1

    .prologue
    .line 91
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->mCheckAll:Z

    return v0
.end method

.method public isDeleteMode()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->mDeleteMode:Z

    return v0
.end method

.method protected isLogSelected(Ljava/lang/String;)Z
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->mCheckedItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    const/4 v0, 0x1

    .line 123
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMenuDeleteMode()Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->mMenuDeleteMode:Z

    return v0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 176
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030161

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public setCheckAll(Z)V
    .locals 0
    .param p1, "checkAll"    # Z

    .prologue
    .line 100
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->mCheckAll:Z

    .line 101
    return-void
.end method

.method public setDeleteMode(Z)V
    .locals 0
    .param p1, "deleteMode"    # Z

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->mDeleteMode:Z

    .line 74
    return-void
.end method

.method protected setLogSelected(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "isSelect"    # Z

    .prologue
    .line 113
    if-eqz p2, :cond_0

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->mCheckedItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    :goto_0
    return-void

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->mCheckedItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public setMenuDeleteMode(Z)V
    .locals 0
    .param p1, "deleteMode"    # Z

    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->mMenuDeleteMode:Z

    .line 83
    return-void
.end method
