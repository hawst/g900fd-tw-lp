.class public Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;
.super Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;
.source "ExerciseMateFitnessInformationArea.java"


# instance fields
.field public final MONTHS:[Ljava/lang/String;

.field private date:Landroid/widget/TextView;

.field protected dateFormat:Ljava/text/DateFormat;

.field private firstInforvalue:Ljava/lang/String;

.field private firstUIValue:Ljava/lang/String;

.field private first_info:Landroid/widget/LinearLayout;

.field private first_info_data:Landroid/widget/TextView;

.field private first_info_title:Landroid/widget/TextView;

.field private first_info_unit:Landroid/widget/TextView;

.field private four_info:Landroid/widget/LinearLayout;

.field private informationAreaparent:Landroid/widget/LinearLayout;

.field private isFirstInfoValueSet:Z

.field private isSecondInfoValueSet:Z

.field private isThirdInfoValueSet:Z

.field private secondInforvalue:Ljava/lang/String;

.field private second_info:Landroid/widget/LinearLayout;

.field private second_info_data:Landroid/widget/TextView;

.field private second_info_title:Landroid/widget/TextView;

.field private second_info_unit:Landroid/widget/TextView;

.field private secongUIValue:Ljava/lang/String;

.field private thirdInforvalue:Ljava/lang/String;

.field private thirdUIValue:Ljava/lang/String;

.field private third_info:Landroid/widget/LinearLayout;

.field private third_info_data:Landroid/widget/TextView;

.field private third_info_title:Landroid/widget/TextView;

.field private third_info_unit:Landroid/widget/TextView;

.field private time_str:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 57
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;-><init>(Landroid/content/Context;)V

    .line 20
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->MONTHS:[Ljava/lang/String;

    .line 49
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->isFirstInfoValueSet:Z

    .line 50
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->isSecondInfoValueSet:Z

    .line 51
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->isThirdInfoValueSet:Z

    .line 58
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->initLayout()V

    .line 59
    return-void
.end method

.method private initLayout()V
    .locals 1

    .prologue
    .line 63
    const v0, 0x7f080368

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->informationAreaparent:Landroid/widget/LinearLayout;

    .line 64
    const v0, 0x7f080358

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->date:Landroid/widget/TextView;

    .line 65
    const v0, 0x7f08035a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->first_info:Landroid/widget/LinearLayout;

    .line 66
    const v0, 0x7f08035c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->first_info_data:Landroid/widget/TextView;

    .line 67
    const v0, 0x7f08035b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->first_info_title:Landroid/widget/TextView;

    .line 68
    const v0, 0x7f08035d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->first_info_unit:Landroid/widget/TextView;

    .line 70
    const v0, 0x7f08035e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->second_info:Landroid/widget/LinearLayout;

    .line 71
    const v0, 0x7f080360

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->second_info_data:Landroid/widget/TextView;

    .line 72
    const v0, 0x7f08035f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->second_info_title:Landroid/widget/TextView;

    .line 73
    const v0, 0x7f080362

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->second_info_unit:Landroid/widget/TextView;

    .line 75
    const v0, 0x7f080364

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->third_info:Landroid/widget/LinearLayout;

    .line 76
    const v0, 0x7f080366

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->third_info_data:Landroid/widget/TextView;

    .line 77
    const v0, 0x7f080365

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->third_info_title:Landroid/widget/TextView;

    .line 78
    const v0, 0x7f080367

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->third_info_unit:Landroid/widget/TextView;

    .line 80
    const v0, 0x7f080369

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->four_info:Landroid/widget/LinearLayout;

    .line 82
    return-void
.end method


# virtual methods
.method public dimInformationAreaView()V
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->informationAreaparent:Landroid/widget/LinearLayout;

    const v1, 0x3f4ccccd    # 0.8f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 217
    return-void
.end method

.method protected initInformationAreaView()Landroid/view/View;
    .locals 2

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0300b8

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public refreshInformationAreaView()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->informationAreaparent:Landroid/widget/LinearLayout;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->date:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->time_str:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->date:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 96
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->isFirstInfoValueSet:Z

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->first_info_data:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->firstInforvalue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->first_info_title:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09002c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->first_info_unit:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->firstUIValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->first_info:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 104
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->isSecondInfoValueSet:Z

    if-eqz v0, :cond_1

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->second_info_data:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->secondInforvalue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->second_info_title:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0909e7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->second_info_title:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->second_info_unit:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->secongUIValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->second_info_unit:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->second_info:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 114
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->isThirdInfoValueSet:Z

    if-eqz v0, :cond_2

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->third_info_data:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->thirdInforvalue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->third_info_title:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0909e8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->third_info_unit:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->thirdUIValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->third_info:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 122
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->four_info:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 125
    return-void
.end method

.method public setDate(DLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 17
    .param p1, "realX"    # D
    .param p3, "periodH"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 129
    const-string v8, "+"

    .line 130
    .local v8, "REGEX_ONE_OR_MORE":Ljava/lang/String;
    const-string v7, "$"

    .line 132
    .local v7, "REGEX_END_OF_LINE":Ljava/lang/String;
    const-string v3, "d"

    .line 133
    .local v3, "DAY_CHAR":Ljava/lang/String;
    const-string v5, "M"

    .line 134
    .local v5, "MONTH_CHAR":Ljava/lang/String;
    const-string/jumbo v9, "y"

    .line 136
    .local v9, "YEAR_CHAR":Ljava/lang/String;
    const-string v2, "/"

    .line 137
    .local v2, "DATE_SEPARATOR":Ljava/lang/String;
    const-string v4, "dd"

    .line 138
    .local v4, "DAY_FORMAT_PATTERN":Ljava/lang/String;
    const-string v6, "MM"

    .line 139
    .local v6, "MONTH_FORMAT_PATTERN":Ljava/lang/String;
    const-string/jumbo v10, "yyyy"

    .line 141
    .local v10, "YEAR_FORMAT_PATTERN":Ljava/lang/String;
    new-instance v11, Ljava/util/Date;

    move-wide/from16 v0, p1

    double-to-long v15, v0

    move-wide v0, v15

    invoke-direct {v11, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 145
    .local v11, "date":Ljava/util/Date;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->getContext()Landroid/content/Context;

    move-result-object v15

    invoke-static {v15}, Lcom/sec/android/app/shealth/common/utils/DateFormatHelper;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->dateFormat:Ljava/text/DateFormat;

    .line 148
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->getContext()Landroid/content/Context;

    move-result-object v15

    invoke-static {v15}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 149
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f090258

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 154
    .local v14, "mTimeFormatPattern":Ljava/lang/String;
    :goto_0
    new-instance v12, Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->getContext()Landroid/content/Context;

    move-result-object v15

    invoke-static {v15}, Landroid/text/format/DateFormat;->getDateFormatOrder(Landroid/content/Context;)[C

    move-result-object v15

    invoke-direct {v12, v15}, Ljava/lang/String;-><init>([C)V

    .line 157
    .local v12, "dateFormatPattern":Ljava/lang/String;
    sget-object v15, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, p3

    if-ne v0, v15, :cond_2

    .line 158
    const-string v13, ""

    .line 163
    .local v13, "dayFormatPattern":Ljava/lang/String;
    :goto_1
    const-string v15, "d+"

    invoke-virtual {v12, v15, v13}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 164
    const-string v15, "M+"

    const-string v16, "MM/"

    move-object/from16 v0, v16

    invoke-virtual {v12, v15, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 165
    const-string/jumbo v15, "y+"

    const-string/jumbo v16, "yyyy/"

    move-object/from16 v0, v16

    invoke-virtual {v12, v15, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 166
    const-string v15, "/$"

    const-string v16, ""

    move-object/from16 v0, v16

    invoke-virtual {v12, v15, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 168
    sget-object v15, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, p3

    if-ne v0, v15, :cond_0

    .line 169
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 172
    :cond_0
    new-instance v15, Ljava/text/SimpleDateFormat;

    invoke-direct {v15, v12}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->dateFormat:Ljava/text/DateFormat;

    .line 174
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->dateFormat:Ljava/text/DateFormat;

    invoke-virtual {v15, v11}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->time_str:Ljava/lang/String;

    .line 175
    return-void

    .line 151
    .end local v12    # "dateFormatPattern":Ljava/lang/String;
    .end local v13    # "dayFormatPattern":Ljava/lang/String;
    .end local v14    # "mTimeFormatPattern":Ljava/lang/String;
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f090259

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    .restart local v14    # "mTimeFormatPattern":Ljava/lang/String;
    goto :goto_0

    .line 160
    .restart local v12    # "dateFormatPattern":Ljava/lang/String;
    :cond_2
    const-string v13, "dd/"

    .restart local v13    # "dayFormatPattern":Ljava/lang/String;
    goto :goto_1
.end method

.method public setSecondInformation(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "unit"    # Ljava/lang/String;

    .prologue
    .line 192
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 194
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->isSecondInfoValueSet:Z

    .line 195
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->secondInforvalue:Ljava/lang/String;

    .line 196
    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessInformationArea;->secongUIValue:Ljava/lang/String;

    .line 198
    :cond_0
    return-void
.end method

.method public update(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SchartHandlerData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 223
    .local p1, "pointData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/chart/view/SchartHandlerData;>;"
    return-void
.end method
