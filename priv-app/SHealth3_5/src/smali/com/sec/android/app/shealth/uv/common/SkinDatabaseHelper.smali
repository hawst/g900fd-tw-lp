.class public Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;
.super Ljava/lang/Object;
.source "SkinDatabaseHelper.java"


# static fields
.field private static instance:Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;->mContext:Landroid/content/Context;

    .line 35
    return-void
.end method

.method private getDeviceName(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 117
    if-eqz p1, :cond_0

    .line 118
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 119
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    const-string v0, "custom_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 123
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    const-class v1, Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;->instance:Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;

    if-nez v0, :cond_0

    .line 28
    new-instance v0, Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;->instance:Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;

    .line 30
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;->instance:Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 27
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public clearData()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Skin;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getData()Lcom/sec/android/app/shealth/uv/data/SkinData;
    .locals 9

    .prologue
    .line 56
    const/4 v7, 0x0

    .line 58
    .local v7, "mData":Lcom/sec/android/app/shealth/uv/data/SkinData;
    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v1, "skin_tone"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string/jumbo v1, "skin_type"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string/jumbo v1, "sample_time"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "_id"

    aput-object v1, v2, v0

    const/4 v0, 0x4

    const-string/jumbo v1, "user_device__id"

    aput-object v1, v2, v0

    .line 60
    .local v2, "projection":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 63
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Skin;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 64
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 65
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 66
    new-instance v8, Lcom/sec/android/app/shealth/uv/data/SkinData;

    invoke-direct {v8}, Lcom/sec/android/app/shealth/uv/data/SkinData;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 68
    .end local v7    # "mData":Lcom/sec/android/app/shealth/uv/data/SkinData;
    .local v8, "mData":Lcom/sec/android/app/shealth/uv/data/SkinData;
    :cond_0
    :try_start_1
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v8, v0, v1}, Lcom/sec/android/app/shealth/uv/data/SkinData;->setId(J)V

    .line 69
    const-string/jumbo v0, "skin_tone"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {v8, v0}, Lcom/sec/android/app/shealth/uv/data/SkinData;->setSkinTone(I)V

    .line 70
    const-string/jumbo v0, "skin_type"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {v8, v0}, Lcom/sec/android/app/shealth/uv/data/SkinData;->setSkinType(I)V

    .line 71
    const-string/jumbo v0, "sample_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v8, v0, v1}, Lcom/sec/android/app/shealth/uv/data/SkinData;->setSampleTime(J)V

    .line 72
    const-string/jumbo v0, "user_device__id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/sec/android/app/shealth/uv/data/SkinData;->setUserDeviceId(Ljava/lang/String;)V

    .line 74
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_0

    move-object v7, v8

    .line 79
    .end local v8    # "mData":Lcom/sec/android/app/shealth/uv/data/SkinData;
    .restart local v7    # "mData":Lcom/sec/android/app/shealth/uv/data/SkinData;
    :cond_1
    if-eqz v6, :cond_2

    .line 80
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 83
    :cond_2
    return-object v7

    .line 79
    :catchall_0
    move-exception v0

    :goto_0
    if-eqz v6, :cond_3

    .line 80
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 79
    .end local v7    # "mData":Lcom/sec/android/app/shealth/uv/data/SkinData;
    .restart local v8    # "mData":Lcom/sec/android/app/shealth/uv/data/SkinData;
    :catchall_1
    move-exception v0

    move-object v7, v8

    .end local v8    # "mData":Lcom/sec/android/app/shealth/uv/data/SkinData;
    .restart local v7    # "mData":Lcom/sec/android/app/shealth/uv/data/SkinData;
    goto :goto_0
.end method

.method public getDataCount()I
    .locals 7

    .prologue
    .line 127
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    .line 128
    .local v2, "projection":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 130
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Skin;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 131
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 133
    if-eqz v6, :cond_0

    .line 134
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    return v0

    .line 133
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    .line 134
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0
.end method

.method public insertData(II)V
    .locals 6
    .param p1, "skinTone"    # I
    .param p2, "skinType"    # I

    .prologue
    .line 39
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 40
    .local v1, "values":Landroid/content/ContentValues;
    const-string/jumbo v2, "skin_tone"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 41
    const-string/jumbo v2, "skin_type"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 42
    const-string v2, "input_source_type"

    const v3, 0x3f7a1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 43
    const-string/jumbo v2, "sample_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 45
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;->getDataCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 46
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$Skin;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 53
    :goto_0
    return-void

    .line 48
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$Skin;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 50
    :catch_0
    move-exception v0

    .line 51
    .local v0, "e":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    const-string v2, "UvSkinDatabaseHelper"

    const-string v3, "Got invalid Data, Unable to insert"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
