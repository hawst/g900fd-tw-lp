.class public Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "WalkingMateGearLogDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;,
        Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;,
        Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DeviceListLoader;
    }
.end annotation


# static fields
.field public static final DATE_KEY:Ljava/lang/String; = "DATE"

.field private static final DETAILS_FIELD_CALORIES_BURNT:I = 0x3

.field private static final DETAILS_FIELD_DAILY_STEPS:I = 0x0

.field private static final DETAILS_FIELD_DISTANCE:I = 0x2

.field private static final DETAILS_FIELD_HEALHTY_PACE:I = 0x4

.field private static final DETAILS_FIELD_STEPS_RAN:I = 0x5

.field private static final DETAILS_FIELD_TIME_ACTIVE:I = 0x1

.field private static final DETAILS_TOTAL_FIELDS:I = 0x6

.field public static final DEVICE_NAMES_KEY:Ljava/lang/String; = "DEVICE_NAMES"

.field private static final HEADAER_FIELD_DATE:I = 0x0

.field private static final HEADAER_TOTAL_DAILY_STEPS:I = 0x1

.field private static final HEADAER_TOTAL_FIELDS:I = 0x2

.field private static final ICON_RES_INDEX:I = 0x0

.field private static final STRING_RES_INDEX:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field public static final TODAY_STEPS_KEY:Ljava/lang/String; = "TODAY_STPES"

.field private static final mDetailsContentDesc:[Ljava/lang/String;

.field private static final mHeaderContentDesc:[Ljava/lang/String;


# instance fields
.field private final Details:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[I>;"
        }
    .end annotation
.end field

.field private final Header:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[I>;"
        }
    .end annotation
.end field

.field private mDate:Ljava/util/Date;

.field private mDateTxt:Ljava/lang/String;

.field private mDetailsContainerLayout:Landroid/widget/LinearLayout;

.field private mDetailsExtendItems:[I

.field private mDetailsItems:[I

.field private mDetailsLogLayout:Landroid/widget/LinearLayout;

.field private mDeviceNames:[Ljava/lang/String;

.field private mGearDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;",
            ">;"
        }
    .end annotation
.end field

.field private mHeaderContainerLayout:Landroid/widget/LinearLayout;

.field private mHeaderItems:[I

.field private mTag:Ljava/lang/String;

.field private mTime:J

.field private mTotalDailySteps:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->TAG:Ljava/lang/String;

    .line 62
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mHeaderContentDesc:[Ljava/lang/String;

    .line 72
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mDetailsContentDesc:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mGearDataList:Ljava/util/ArrayList;

    .line 74
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mHeaderItems:[I

    .line 75
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mDetailsItems:[I

    .line 77
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mDetailsExtendItems:[I

    .line 84
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->Header:Ljava/util/List;

    .line 91
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$2;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->Details:Ljava/util/List;

    .line 103
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mDetailsLogLayout:Landroid/widget/LinearLayout;

    .line 104
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mHeaderContainerLayout:Landroid/widget/LinearLayout;

    .line 105
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mDetailsContainerLayout:Landroid/widget/LinearLayout;

    .line 645
    return-void

    .line 74
    :array_0
    .array-data 4
        0x0
        0x1
    .end array-data

    .line 75
    :array_1
    .array-data 4
        0x0
        0x2
        0x3
        0x5
    .end array-data

    .line 77
    :array_2
    .array-data 4
        0x0
        0x1
        0x2
        0x3
        0x4
        0x5
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->updateDetails()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mDeviceNames:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;

    .prologue
    .line 42
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mTime:J

    return-wide v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;JLjava/lang/String;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;
    .param p1, "x1"    # J
    .param p3, "x2"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getDetailGearData(JLjava/lang/String;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mGearDataList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->updateDeviceList()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->prepareShareView()V

    return-void
.end method

.method private getDetailGearData(JLjava/lang/String;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;
    .locals 27
    .param p1, "time"    # J
    .param p3, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 524
    const/16 v23, 0x0

    .line 525
    .local v23, "dailySteps":I
    const/4 v9, 0x0

    .line 526
    .local v9, "distance":F
    const/4 v10, 0x0

    .line 527
    .local v10, "calories":F
    const/4 v13, 0x0

    .line 528
    .local v13, "runningSteps":I
    const-wide/16 v24, -0x1

    .line 529
    .local v24, "duration":J
    const-wide/16 v11, -0x1

    .line 531
    .local v11, "healthySteps":J
    invoke-static/range {p3 .. p3}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getCustomeNameFromDeviceName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 533
    .local v16, "gearCustomName":Ljava/lang/String;
    if-nez v16, :cond_0

    .line 534
    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to get custom name by using "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    :cond_0
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    .line 538
    .local v26, "selection":Ljava/lang/StringBuilder;
    const/16 v22, 0x0

    .line 539
    .local v22, "cursor":Landroid/database/Cursor;
    const/4 v3, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 540
    const-string v3, "SELECT"

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 541
    const-string v3, " "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 542
    const-string v3, "SUM(total_step) AS total_step"

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 543
    const-string v3, " , "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 544
    const-string v3, "SUM(distance) AS distance"

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 545
    const-string v3, " , "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 546
    const-string v3, "SUM(calorie) AS calorie"

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 547
    const-string v3, " , "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 548
    const-string v3, "SUM(run_step) AS run_step"

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 549
    const-string v3, " "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 550
    const-string v3, "FROM walk_info"

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551
    const-string v3, " "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 552
    const-string v3, "WHERE"

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 553
    const-string v3, " "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 554
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "start_time >= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static/range {p1 .. p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 555
    const-string v3, " AND "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 556
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "start_time <= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static/range {p1 .. p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 557
    const-string v3, " AND "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 558
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090190

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 559
    const-string v3, "( "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 560
    const-string/jumbo v3, "user_device__id  LIKE \'10020%\' "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 561
    const-string v3, " OR "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 562
    const-string/jumbo v3, "user_device__id  LIKE \'10024%\' "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 563
    const-string v3, " )"

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 574
    :cond_1
    :goto_0
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    .line 575
    if-eqz v22, :cond_2

    .line 576
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToFirst()Z

    .line 577
    const-string/jumbo v3, "total_step"

    move-object/from16 v0, v22

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v22

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v23

    .line 578
    const-string v3, "distance"

    move-object/from16 v0, v22

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v22

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v9

    .line 579
    const-string v3, "calorie"

    move-object/from16 v0, v22

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v22

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v10

    .line 580
    const-string/jumbo v3, "run_step"

    move-object/from16 v0, v22

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v22

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v13

    .line 583
    :cond_2
    if-eqz v22, :cond_3

    .line 584
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    .line 588
    :cond_3
    const/4 v2, 0x0

    .line 590
    .local v2, "gearData":Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090bc0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 592
    const/4 v3, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 593
    const-string v3, "SELECT"

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 594
    const-string v3, " "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 595
    const-string v3, "SUM ( power_step ) AS power_step"

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 596
    const-string v3, " , "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 597
    const-string v3, "SUM ( active_time ) AS active_time"

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 598
    const-string v3, " FROM "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 599
    const-string/jumbo v3, "walk_info_extended"

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 600
    const-string v3, " WHERE "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 601
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090190

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 602
    const-string v3, "( "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 603
    const-string/jumbo v3, "user_device__id  LIKE \'10020%\' "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 604
    const-string v3, " OR "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 605
    const-string/jumbo v3, "user_device__id  LIKE \'10024%\' "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 606
    const-string v3, " )"

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 616
    :cond_4
    :goto_1
    const-string v3, " AND "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 617
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "start_time >= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static/range {p1 .. p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 618
    const-string v3, " AND "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 619
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "start_time <= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static/range {p1 .. p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 622
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .end local v2    # "gearData":Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;
    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    .line 623
    if-eqz v22, :cond_11

    .line 624
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToFirst()Z

    .line 625
    const-string/jumbo v3, "power_step"

    move-object/from16 v0, v22

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v22

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    .line 626
    const-string v3, "active_time"

    move-object/from16 v0, v22

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v22

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v7

    .line 627
    .end local v24    # "duration":J
    .local v7, "duration":J
    const-wide/16 v3, 0x3e8

    :try_start_2
    div-long v3, v7, v3

    const-wide/16 v5, 0x3c

    div-long/2addr v3, v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    long-to-int v3, v3

    int-to-long v7, v3

    .line 630
    :goto_2
    if-eqz v22, :cond_5

    .line 631
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    .line 635
    :cond_5
    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;

    move/from16 v0, v23

    int-to-long v5, v0

    move-object/from16 v3, p3

    move-object/from16 v4, v16

    invoke-direct/range {v2 .. v13}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;-><init>(Ljava/lang/String;Ljava/lang/String;JJFFJI)V

    .line 642
    .restart local v2    # "gearData":Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;
    :goto_3
    return-object v2

    .line 564
    .end local v2    # "gearData":Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;
    .end local v7    # "duration":J
    .restart local v24    # "duration":J
    :cond_6
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090bc3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 565
    const-string/jumbo v3, "user_device__id  LIKE \'10022%\' "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 566
    :cond_7
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090bc0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 567
    const-string/jumbo v3, "user_device__id  LIKE \'10030%\' "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 568
    :cond_8
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090bc1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 569
    const-string/jumbo v3, "user_device__id  LIKE \'10032%\' "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 570
    :cond_9
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090bc2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 571
    const-string/jumbo v3, "user_device__id  LIKE \'10019%\' "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 583
    :catchall_0
    move-exception v3

    if-eqz v22, :cond_a

    .line 584
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    :cond_a
    throw v3

    .line 607
    .restart local v2    # "gearData":Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;
    :cond_b
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090bc3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 608
    const-string/jumbo v3, "user_device__id  LIKE \'10022%\' "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 609
    :cond_c
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090bc0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 610
    const-string/jumbo v3, "user_device__id  LIKE \'10030%\' "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 611
    :cond_d
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090bc1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 612
    const-string/jumbo v3, "user_device__id  LIKE \'10032%\' "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 613
    :cond_e
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090bc2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 614
    const-string/jumbo v3, "user_device__id  LIKE \'10019%\' "

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 630
    .end local v2    # "gearData":Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;
    :catchall_1
    move-exception v3

    move-wide/from16 v7, v24

    .end local v24    # "duration":J
    .restart local v7    # "duration":J
    :goto_4
    if-eqz v22, :cond_f

    .line 631
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    :cond_f
    throw v3

    .line 639
    .end local v7    # "duration":J
    .restart local v2    # "gearData":Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;
    .restart local v24    # "duration":J
    :cond_10
    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;

    .end local v2    # "gearData":Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;
    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v17, v0

    move-object v14, v2

    move-object/from16 v15, p3

    move/from16 v19, v9

    move/from16 v20, v10

    move/from16 v21, v13

    invoke-direct/range {v14 .. v21}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;-><init>(Ljava/lang/String;Ljava/lang/String;JFFI)V

    .restart local v2    # "gearData":Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;
    move-wide/from16 v7, v24

    .end local v24    # "duration":J
    .restart local v7    # "duration":J
    goto/16 :goto_3

    .line 630
    .end local v2    # "gearData":Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;
    :catchall_2
    move-exception v3

    goto :goto_4

    .end local v7    # "duration":J
    .restart local v24    # "duration":J
    :cond_11
    move-wide/from16 v7, v24

    .end local v24    # "duration":J
    .restart local v7    # "duration":J
    goto/16 :goto_2
.end method

.method private getDetailsContents(I)Ljava/util/List;
    .locals 11
    .param p1, "gearIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 297
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 299
    .local v6, "holder":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;>;"
    const/4 v8, 0x0

    .line 301
    .local v8, "itemArray":[I
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mGearDataList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;->mIsSupportHealhtyPace:Z

    if-eqz v0, :cond_2

    .line 302
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mDetailsExtendItems:[I

    .line 307
    :goto_0
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    array-length v0, v8

    if-ge v7, v0, :cond_3

    .line 309
    aget v0, v8, v7

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getDetailsValue(II)[Ljava/lang/String;

    move-result-object v2

    .line 311
    .local v2, "value":[Ljava/lang/String;
    if-eqz v2, :cond_1

    array-length v0, v2

    if-lez v0, :cond_1

    aget-object v0, v2, v10

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->Details:Ljava/util/List;

    aget v4, v8, v7

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    aget v3, v0, v10

    .line 313
    .local v3, "iconResId":I
    const-string v1, ""

    .line 314
    .local v1, "itemName":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->Details:Ljava/util/List;

    aget v4, v8, v7

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    const/4 v4, 0x1

    aget v9, v0, v4

    .line 315
    .local v9, "stringResId":I
    const/4 v0, -0x1

    if-eq v9, v0, :cond_0

    .line 316
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 318
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;

    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mDetailsContentDesc:[Ljava/lang/String;

    aget v5, v8, v7

    aget-object v4, v4, v5

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;-><init>(Ljava/lang/String;[Ljava/lang/String;ILjava/lang/String;Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$1;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 307
    .end local v1    # "itemName":Ljava/lang/String;
    .end local v3    # "iconResId":I
    .end local v9    # "stringResId":I
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 304
    .end local v2    # "value":[Ljava/lang/String;
    .end local v7    # "i":I
    :cond_2
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mDetailsItems:[I

    goto :goto_0

    .line 322
    .restart local v7    # "i":I
    :cond_3
    return-object v6
.end method

.method private getDetailsView(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;)Landroid/view/View;
    .locals 8
    .param p1, "item"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;

    .prologue
    const v7, 0x7f080bd3

    const/4 v6, 0x1

    const v5, 0x7f080bd1

    const/4 v4, 0x0

    .line 397
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0302a0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 399
    .local v0, "contentView":Landroid/view/View;
    iget-object v1, p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;->title:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 400
    const v1, 0x7f080bce

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 413
    :goto_0
    iget-object v1, p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;->contentDesc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 415
    return-object v0

    .line 402
    :cond_0
    const v1, 0x7f0807f8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget v2, p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;->itemIconResId:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 403
    const v1, 0x7f080bcf

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;->itemName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 404
    iget-object v1, p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;->itemValue:[Ljava/lang/String;

    array-length v1, v1

    if-le v1, v6, :cond_1

    .line 405
    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 406
    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;->itemValue:[Ljava/lang/String;

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 407
    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;->itemValue:[Ljava/lang/String;

    aget-object v2, v2, v6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 409
    :cond_1
    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 410
    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;->itemValue:[Ljava/lang/String;

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private getHeaderContents()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 210
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 212
    .local v6, "holder":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;>;"
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mHeaderItems:[I

    array-length v0, v0

    if-ge v7, v0, :cond_2

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mHeaderItems:[I

    aget v0, v0, v7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getHeaderValue(I)[Ljava/lang/String;

    move-result-object v2

    .line 215
    .local v2, "value":[Ljava/lang/String;
    if-eqz v2, :cond_1

    array-length v0, v2

    if-lez v0, :cond_1

    aget-object v0, v2, v9

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->Header:Ljava/util/List;

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mHeaderItems:[I

    aget v4, v4, v7

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    aget v3, v0, v9

    .line 217
    .local v3, "iconResId":I
    const-string v1, ""

    .line 218
    .local v1, "itemName":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->Header:Ljava/util/List;

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mHeaderItems:[I

    aget v4, v4, v7

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    const/4 v4, 0x1

    aget v8, v0, v4

    .line 219
    .local v8, "stringResId":I
    const/4 v0, -0x1

    if-eq v8, v0, :cond_0

    .line 220
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 222
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;

    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mHeaderContentDesc:[Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mHeaderItems:[I

    aget v5, v5, v7

    aget-object v4, v4, v5

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;-><init>(Ljava/lang/String;[Ljava/lang/String;ILjava/lang/String;Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$1;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    .end local v1    # "itemName":Ljava/lang/String;
    .end local v3    # "iconResId":I
    .end local v8    # "stringResId":I
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 225
    .end local v2    # "value":[Ljava/lang/String;
    :cond_2
    return-object v6
.end method

.method private getHeaderView(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;)Landroid/view/View;
    .locals 8
    .param p1, "item"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;

    .prologue
    const v7, 0x7f080bd3

    const/4 v6, 0x1

    const v5, 0x7f080bd1

    const/4 v4, 0x0

    .line 251
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03029f

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 252
    .local v0, "contentView":Landroid/view/View;
    const v1, 0x7f0807f8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget v2, p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;->itemIconResId:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 253
    const v1, 0x7f080bcf

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;->itemName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 254
    iget-object v1, p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;->itemValue:[Ljava/lang/String;

    array-length v1, v1

    if-le v1, v6, :cond_0

    .line 255
    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 256
    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;->itemValue:[Ljava/lang/String;

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 257
    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;->itemValue:[Ljava/lang/String;

    aget-object v2, v2, v6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 263
    :goto_0
    iget-object v1, p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;->contentDesc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 265
    return-object v0

    .line 259
    :cond_0
    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 260
    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;->itemValue:[Ljava/lang/String;

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateDetails()V
    .locals 10

    .prologue
    .line 271
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mDetailsContainerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v6

    if-lez v6, :cond_0

    .line 272
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mDetailsContainerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 274
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mGearDataList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v1, v6, :cond_3

    .line 276
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090a8c

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v6, 0x1

    new-array v8, v6, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mGearDataList:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;

    iget-object v6, v6, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;->mCustomName:Ljava/lang/String;

    aput-object v6, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 277
    .local v3, "title":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f03029e

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 278
    .local v4, "titleText":Landroid/widget/TextView;
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 279
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mDetailsContainerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 281
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getDetailsContents(I)Ljava/util/List;

    move-result-object v0

    .line 283
    .local v0, "holderList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;>;"
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    if-ge v2, v6, :cond_2

    .line 285
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;

    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getDetailsView(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;)Landroid/view/View;

    move-result-object v5

    .line 288
    .local v5, "view":Landroid/view/View;
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mGearDataList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-eq v1, v6, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-ne v2, v6, :cond_1

    .line 289
    const v6, 0x7f080bd4

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 291
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mDetailsContainerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 283
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 274
    .end local v5    # "view":Landroid/view/View;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 294
    .end local v0    # "holderList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;>;"
    .end local v2    # "j":I
    .end local v3    # "title":Ljava/lang/String;
    .end local v4    # "titleText":Landroid/widget/TextView;
    :cond_3
    return-void
.end method

.method private updateDeviceList()V
    .locals 1

    .prologue
    .line 160
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$3;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 166
    return-void
.end method

.method private updateHeader()V
    .locals 5

    .prologue
    .line 193
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mHeaderContainerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    if-lez v3, :cond_0

    .line 194
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mHeaderContainerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 196
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getHeaderContents()Ljava/util/List;

    move-result-object v0

    .line 198
    .local v0, "holderList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 200
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getHeaderView(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$ItemHolder;)Landroid/view/View;

    move-result-object v2

    .line 202
    .local v2, "view":Landroid/view/View;
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v1, v3, :cond_1

    .line 203
    const v3, 0x7f080bd4

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 205
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mHeaderContainerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 198
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 207
    .end local v2    # "view":Landroid/view/View;
    :cond_2
    return-void
.end method


# virtual methods
.method public arabNumberChangeLocale(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "stepStr"    # Ljava/lang/String;

    .prologue
    const v3, 0x7f090b8d

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 482
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 483
    .local v1, "language":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 484
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 489
    .local v0, "concatStep":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 486
    .end local v0    # "concatStep":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "concatStep":Ljava/lang/String;
    goto :goto_0
.end method

.method public distanceConverter(FLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "distance"    # F
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "unit"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    const/4 v10, 0x0

    const/high16 v7, 0x42c80000    # 100.0f

    .line 493
    new-instance v3, Ljava/text/DecimalFormat;

    const-string v6, "0.00"

    invoke-direct {v3, v6}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 494
    .local v3, "twoDForm":Ljava/text/DecimalFormat;
    new-instance v5, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    .line 495
    .local v5, "unitSettingHelper":Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getDistanceUnit()Ljava/lang/String;

    move-result-object v1

    .line 497
    .local v1, "distanceUnit":Ljava/lang/String;
    const-string/jumbo v6, "mi"

    invoke-virtual {v1, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 498
    const v6, 0x3a22e36f

    mul-float/2addr v6, p1

    mul-float/2addr v6, v7

    float-to-int v6, v6

    int-to-float v6, v6

    div-float v0, v6, v7

    .line 499
    .local v0, "convert_distance":F
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    iget-object v6, v6, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v6}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 500
    .local v2, "language":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 501
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "%.2f"

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 505
    :goto_0
    const v6, 0x7f0900cc

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 506
    const v6, 0x7f0900cb

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 518
    .local v4, "unitContentDesc":Ljava/lang/String;
    :goto_1
    sget-object v7, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mDetailsContentDesc:[Ljava/lang/String;

    const/4 v8, 0x2

    float-to-int v6, p1

    if-eqz v6, :cond_3

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, " "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    :goto_2
    aput-object v6, v7, v8

    .line 519
    float-to-int v6, p1

    if-eqz v6, :cond_4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    :goto_3
    return-object v6

    .line 503
    .end local v4    # "unitContentDesc":Ljava/lang/String;
    :cond_0
    float-to-double v6, v0

    invoke-virtual {v3, v6, v7}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 508
    .end local v0    # "convert_distance":F
    .end local v2    # "language":Ljava/lang/String;
    :cond_1
    const/high16 v6, 0x41200000    # 10.0f

    div-float v6, p1, v6

    float-to-int v6, v6

    int-to-float v6, v6

    div-float v0, v6, v7

    .line 509
    .restart local v0    # "convert_distance":F
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    iget-object v6, v6, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v6}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 510
    .restart local v2    # "language":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 511
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "%.2f"

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 515
    :goto_4
    const v6, 0x7f0900c7

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 516
    const v6, 0x7f0901c3

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "unitContentDesc":Ljava/lang/String;
    goto :goto_1

    .line 513
    .end local v4    # "unitContentDesc":Ljava/lang/String;
    :cond_2
    float-to-double v6, v0

    invoke-virtual {v3, v6, v7}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object p2

    goto :goto_4

    .line 518
    .restart local v4    # "unitContentDesc":Ljava/lang/String;
    :cond_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "0 "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    .line 519
    :cond_4
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "0 "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_3
.end method

.method public getDetailsValue(II)[Ljava/lang/String;
    .locals 21
    .param p1, "gearIndex"    # I
    .param p2, "itemIndex"    # I

    .prologue
    .line 326
    new-instance v8, Ljava/text/SimpleDateFormat;

    const-string v16, "MMMM dd yyyy"

    move-object/from16 v0, v16

    invoke-direct {v8, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 327
    .local v8, "format":Ljava/text/DateFormat;
    const-string v15, ""

    .line 328
    .local v15, "value":Ljava/lang/String;
    const-string v13, ""

    .line 330
    .local v13, "unit":Ljava/lang/String;
    packed-switch p2, :pswitch_data_0

    .line 392
    :goto_0
    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object v15, v16, v17

    return-object v16

    .line 332
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mGearDataList:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;

    move-object/from16 v0, v16

    iget-wide v3, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;->mDailySteps:J

    .line 333
    .local v3, "dailySteps":J
    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->arabNumberChangeLocale(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 334
    goto :goto_0

    .line 336
    .end local v3    # "dailySteps":J
    :pswitch_1
    const v16, 0x7f0900eb

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 337
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mGearDataList:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;

    move-object/from16 v0, v16

    iget-wide v6, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;->mDuration:J

    .line 338
    .local v6, "duration":J
    const-wide/16 v16, -0x1

    cmp-long v16, v6, v16

    if-nez v16, :cond_0

    .line 339
    const-wide/16 v6, 0x0

    .line 342
    :cond_0
    const-wide/16 v16, 0x1

    cmp-long v16, v6, v16

    if-nez v16, :cond_1

    .line 343
    const v16, 0x7f0900ea

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 345
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mGearDataList:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;

    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;->mDuration:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v15

    .line 346
    sget-object v16, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mDetailsContentDesc:[Ljava/lang/String;

    const/16 v17, 0x1

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const v19, 0x7f0900ec

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    aput-object v18, v16, v17

    .line 347
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 348
    goto/16 :goto_0

    .line 350
    .end local v6    # "duration":J
    :pswitch_2
    new-instance v14, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-direct {v14, v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    .line 351
    .local v14, "unitSettingHelper":Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    invoke-virtual {v14}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getDistanceUnit()Ljava/lang/String;

    move-result-object v5

    .line 352
    .local v5, "distanceUnit":Ljava/lang/String;
    if-nez v5, :cond_2

    .line 353
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0900c7

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 355
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mGearDataList:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;

    move-object/from16 v0, v16

    iget v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;->mDistance:F

    move/from16 v16, v0

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15, v13}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->distanceConverter(FLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 356
    goto/16 :goto_0

    .line 358
    .end local v5    # "distanceUnit":Ljava/lang/String;
    .end local v14    # "unitSettingHelper":Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mGearDataList:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;

    move-object/from16 v0, v16

    iget v2, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;->mCalories:F

    .line 360
    .local v2, "calories":F
    new-instance v9, Ljava/text/DecimalFormat;

    const-string v16, "#.##"

    move-object/from16 v0, v16

    invoke-direct {v9, v0}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 364
    .local v9, "ft":Ljava/text/DecimalFormat;
    const v16, 0x7f0900b9

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 365
    const-string v16, "Cal"

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_3

    .line 366
    sget-object v16, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mDetailsContentDesc:[Ljava/lang/String;

    const/16 v17, 0x3

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    float-to-double v0, v2

    move-wide/from16 v19, v0

    move-wide/from16 v0, v19

    invoke-virtual {v9, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const v19, 0x7f0900bb

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    aput-object v18, v16, v17

    .line 373
    :goto_1
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    float-to-double v0, v2

    move-wide/from16 v17, v0

    move-wide/from16 v0, v17

    invoke-virtual {v9, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 374
    goto/16 :goto_0

    .line 370
    :cond_3
    sget-object v16, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mDetailsContentDesc:[Ljava/lang/String;

    const/16 v17, 0x3

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    float-to-double v0, v2

    move-wide/from16 v19, v0

    move-wide/from16 v0, v19

    invoke-virtual {v9, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const v19, 0x7f0901cb

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    aput-object v18, v16, v17

    goto :goto_1

    .line 376
    .end local v2    # "calories":F
    .end local v9    # "ft":Ljava/text/DecimalFormat;
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mGearDataList:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;

    move-object/from16 v0, v16

    iget-wide v10, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;->mHealthyPace:J

    .line 377
    .local v10, "healthyPace":J
    const v16, 0x7f0907e5

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 379
    const-wide/16 v16, -0x1

    cmp-long v16, v10, v16

    if-nez v16, :cond_4

    .line 380
    const-wide/16 v10, 0x0

    .line 383
    :cond_4
    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->arabNumberChangeLocale(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 384
    goto/16 :goto_0

    .line 386
    .end local v10    # "healthyPace":J
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mGearDataList:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;

    move-object/from16 v0, v16

    iget v12, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DetailGearData;->mRunningSteps:I

    .line 387
    .local v12, "runningSteps":I
    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->arabNumberChangeLocale(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 388
    goto/16 :goto_0

    .line 330
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public getHeaderValue(I)[Ljava/lang/String;
    .locals 8
    .param p1, "index"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 229
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v4, "MMMM dd yyyy"

    invoke-direct {v1, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 230
    .local v1, "format":Ljava/text/DateFormat;
    const-string v3, ""

    .line 233
    .local v3, "value":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 246
    new-array v4, v7, [Ljava/lang/String;

    aput-object v3, v4, v6

    :goto_0
    return-object v4

    .line 235
    :pswitch_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mDate:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const-string v5, "/"

    invoke-static {p0, v4, v5}, Lcom/sec/android/app/shealth/common/utils/DateFormatHelper;->getSystemDateFormat(Landroid/content/Context;Ljava/lang/Long;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 236
    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mHeaderContentDesc:[Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mDate:Ljava/util/Date;

    invoke-virtual {v1, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    .line 237
    new-array v4, v7, [Ljava/lang/String;

    aput-object v3, v4, v6

    goto :goto_0

    .line 240
    :pswitch_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mTotalDailySteps:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->arabNumberChangeLocale(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 241
    .local v0, "concatStep":Ljava/lang/String;
    if-eqz v0, :cond_0

    move-object v2, v0

    .line 242
    .local v2, "ret":Ljava/lang/String;
    :goto_1
    new-array v4, v7, [Ljava/lang/String;

    aput-object v2, v4, v6

    goto :goto_0

    .line 241
    .end local v2    # "ret":Ljava/lang/String;
    :cond_0
    const-string v2, ""

    goto :goto_1

    .line 233
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const-wide/16 v8, 0x0

    const/4 v7, 0x0

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 112
    .local v2, "intent":Landroid/content/Intent;
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v4, "dd/MM/yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 114
    .local v0, "dateFormatter":Ljava/text/DateFormat;
    const-string v4, "DATE"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mDateTxt:Ljava/lang/String;

    .line 115
    const-string v4, "TODAY_STPES"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mTotalDailySteps:Ljava/lang/String;

    .line 116
    const-string v4, "DEVICE_NAMES"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mDeviceNames:[Ljava/lang/String;

    .line 118
    iput-wide v8, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mTime:J

    .line 120
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mDateTxt:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mTime:J
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 125
    :goto_0
    iget-wide v4, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mTime:J

    cmp-long v4, v4, v8

    if-nez v4, :cond_0

    .line 126
    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "time is null"

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->finish()V

    .line 147
    :goto_1
    return-void

    .line 121
    :catch_0
    move-exception v1

    .line 122
    .local v1, "e":Ljava/text/ParseException;
    invoke-virtual {v1}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0

    .line 131
    .end local v1    # "e":Ljava/text/ParseException;
    :cond_0
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mDate:Ljava/util/Date;

    .line 132
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mDate:Ljava/util/Date;

    iget-wide v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mTime:J

    invoke-virtual {v4, v5, v6}, Ljava/util/Date;->setTime(J)V

    .line 134
    new-instance v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DeviceListLoader;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DeviceListLoader;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;)V

    .line 135
    .local v3, "loader":Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DeviceListLoader;
    new-array v4, v7, [Ljava/lang/Void;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$DeviceListLoader;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 137
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 139
    const v4, 0x7f03029d

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->setContentView(I)V

    .line 141
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v4

    const v5, 0x7f09005b

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    .line 142
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 144
    const v4, 0x7f080bcc

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mDetailsLogLayout:Landroid/widget/LinearLayout;

    .line 145
    const v4, 0x7f080bcd

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mHeaderContainerLayout:Landroid/widget/LinearLayout;

    .line 146
    const v4, 0x7f080738

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->mDetailsContainerLayout:Landroid/widget/LinearLayout;

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v6, 0x1

    .line 444
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    .line 445
    .local v2, "inflater":Landroid/view/MenuInflater;
    const v3, 0x7f10002e

    invoke-virtual {v2, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 446
    const v3, 0x7f080c8d

    invoke-interface {p1, v3}, Landroid/view/Menu;->removeItem(I)V

    .line 447
    const v3, 0x7f080c98

    invoke-interface {p1, v3}, Landroid/view/Menu;->removeItem(I)V

    .line 448
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getTotalActionBarButtonCount()I

    move-result v3

    if-ge v3, v6, :cond_0

    .line 449
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v3, 0x7f0208e3

    new-instance v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$4;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity$4;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;)V

    invoke-direct {v1, v3, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    .line 459
    .local v1, "actionBarShareviaButtonBuilder":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    const v3, 0x7f090033

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 460
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v0

    .line 461
    .local v0, "actionBarShareviaButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    new-array v4, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 466
    .end local v0    # "actionBarShareviaButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    .end local v1    # "actionBarShareviaButtonBuilder":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    :cond_0
    return v6
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 472
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f080c98

    if-ne v0, v1, :cond_0

    .line 473
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.walkingmate"

    const-string v2, "W021"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 151
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 152
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGearLogDetailsActivity;->updateHeader()V

    .line 153
    return-void
.end method
