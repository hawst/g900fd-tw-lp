.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$1;
.super Ljava/lang/Object;
.source "InputModule.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->registerListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)V
    .locals 0

    .prologue
    .line 283
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 286
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->isOccupiedControllerTab:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 293
    :cond_0
    :goto_0
    return-void

    .line 288
    :cond_1
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onFocusChange is true : isOccupiedControllerTab : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->isOccupiedControllerTab:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",  length : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->isOccupiedControllerTab:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setCursorVisible(Z)V

    goto :goto_0
.end method
