.class public Lcom/sec/android/app/shealth/uv/data/SkinData;
.super Ljava/lang/Object;
.source "SkinData.java"


# instance fields
.field private applicationId:Ljava/lang/String;

.field private id:J

.field private sampleTime:J

.field private skinTone:I

.field private skinType:I

.field private updateTime:J

.field private userDeviceId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    sget-object v0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;->INVALID:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/uv/data/SkinData;->skinTone:I

    .line 13
    sget-object v0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->INVALID:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/uv/data/SkinData;->skinType:I

    return-void
.end method


# virtual methods
.method public getApplicationId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/data/SkinData;->applicationId:Ljava/lang/String;

    return-object v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 16
    iget-wide v0, p0, Lcom/sec/android/app/shealth/uv/data/SkinData;->id:J

    return-wide v0
.end method

.method public getSampleTime()J
    .locals 2

    .prologue
    .line 34
    iget-wide v0, p0, Lcom/sec/android/app/shealth/uv/data/SkinData;->sampleTime:J

    return-wide v0
.end method

.method public getSkinTone()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/sec/android/app/shealth/uv/data/SkinData;->skinTone:I

    return v0
.end method

.method public getSkinType()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/shealth/uv/data/SkinData;->skinType:I

    return v0
.end method

.method public getUpdateTime()J
    .locals 2

    .prologue
    .line 40
    iget-wide v0, p0, Lcom/sec/android/app/shealth/uv/data/SkinData;->updateTime:J

    return-wide v0
.end method

.method public getUserDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/data/SkinData;->userDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method public setApplicationId(Ljava/lang/String;)V
    .locals 0
    .param p1, "applicationId"    # Ljava/lang/String;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/data/SkinData;->applicationId:Ljava/lang/String;

    .line 26
    return-void
.end method

.method public setId(J)V
    .locals 0
    .param p1, "id"    # J

    .prologue
    .line 19
    iput-wide p1, p0, Lcom/sec/android/app/shealth/uv/data/SkinData;->id:J

    .line 20
    return-void
.end method

.method public setSampleTime(J)V
    .locals 0
    .param p1, "sampleTime"    # J

    .prologue
    .line 37
    iput-wide p1, p0, Lcom/sec/android/app/shealth/uv/data/SkinData;->sampleTime:J

    .line 38
    return-void
.end method

.method public setSkinTone(I)V
    .locals 0
    .param p1, "skinTone"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/sec/android/app/shealth/uv/data/SkinData;->skinTone:I

    .line 50
    return-void
.end method

.method public setSkinType(I)V
    .locals 0
    .param p1, "skinType"    # I

    .prologue
    .line 55
    iput p1, p0, Lcom/sec/android/app/shealth/uv/data/SkinData;->skinType:I

    .line 56
    return-void
.end method

.method public setUpdateTime(J)V
    .locals 0
    .param p1, "updateTime"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/sec/android/app/shealth/uv/data/SkinData;->updateTime:J

    .line 44
    return-void
.end method

.method public setUserDeviceId(Ljava/lang/String;)V
    .locals 0
    .param p1, "userDeviceId"    # Ljava/lang/String;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/data/SkinData;->userDeviceId:Ljava/lang/String;

    .line 32
    return-void
.end method
