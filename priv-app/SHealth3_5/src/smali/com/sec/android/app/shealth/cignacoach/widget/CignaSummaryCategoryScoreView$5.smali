.class Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$5;
.super Ljava/lang/Object;
.source "CignaSummaryCategoryScoreView.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->onAnimationEnd(Landroid/animation/Animator;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)V
    .locals 0

    .prologue
    .line 562
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 566
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 568
    .local v1, "leftMargin":F
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mStartIconBGSize:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->access$700(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)F

    move-result v2

    float-to-int v2, v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mStartIconBGSize:F
    invoke-static {v3}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->access$700(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)F

    move-result v3

    float-to-int v3, v3

    invoke-direct {v0, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 569
    .local v0, "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    float-to-int v2, v1

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 570
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationIconImg:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->access$800(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 571
    return-void
.end method
