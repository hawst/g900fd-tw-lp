.class public interface abstract Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;
.super Ljava/lang/Object;
.source "IMealItemsFragmentAction.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IContentInitializatorGetter;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;
.implements Lcom/sec/android/app/shealth/food/mealinput/FragmentModeSettable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction$IMealItemsQuantityWatcher;
    }
.end annotation


# static fields
.field public static final ALL_DIALOGS_TAGS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final REMOVE_MEAL_ITEM_DIALOG_TAG:Ljava/lang/String; = "REMOVE_MEAL_ITEM_DIALOG_TAG"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->ALL_DIALOGS_TAGS:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public abstract addMealItem(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)V
.end method

.method public abstract addMealItemList(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract getAllMealItems()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getDeletedMealItemsList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getTotalKcal()I
.end method

.method public abstract onNewIntent(Landroid/content/Intent;)V
.end method

.method public abstract resetAllData()V
.end method
