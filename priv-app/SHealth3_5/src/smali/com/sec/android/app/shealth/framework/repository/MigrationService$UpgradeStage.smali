.class public final enum Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;
.super Ljava/lang/Enum;
.source "MigrationService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/repository/MigrationService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UpgradeStage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;

.field public static final enum DB:Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;

.field public static final enum FILES:Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;

.field public static final enum PREFERENCES:Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 53
    new-instance v0, Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;

    const-string v1, "PREFERENCES"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;->PREFERENCES:Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;

    new-instance v0, Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;

    const-string v1, "FILES"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;->FILES:Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;

    new-instance v0, Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;

    const-string v1, "DB"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;->DB:Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;

    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;->PREFERENCES:Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;->FILES:Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;->DB:Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;->$VALUES:[Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 53
    const-class v0, Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;->$VALUES:[Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/framework/repository/MigrationService$UpgradeStage;

    return-object v0
.end method
