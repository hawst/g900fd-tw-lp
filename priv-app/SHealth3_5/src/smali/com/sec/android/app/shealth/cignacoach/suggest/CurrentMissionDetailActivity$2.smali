.class Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$2;
.super Ljava/lang/Object;
.source "CurrentMissionDetailActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->updateTrackingView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;

.field final synthetic val$index:I

.field final synthetic val$timeMill:J


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;IJ)V
    .locals 0

    .prologue
    .line 261
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;

    iput p2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$2;->val$index:I

    iput-wide p3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$2;->val$timeMill:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    .line 265
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mTrakingDayOfWeekView:[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->access$100(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;)[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    move-result-object v1

    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$2;->val$index:I

    aget-object v3, v1, v3

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mTrakingDayOfWeekView:[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->access$100(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;)[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    move-result-object v1

    iget v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$2;->val$index:I

    aget-object v1, v1, v4

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->isSelection()Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v3, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->setSelection(Z)V

    .line 267
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mTrakingDayOfWeekView:[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->access$100(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;)[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    move-result-object v1

    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$2;->val$index:I

    aget-object v1, v1, v3

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->isSelection()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 269
    new-instance v0, Lcom/cigna/coach/apiobjects/MissionDetails;

    invoke-direct {v0}, Lcom/cigna/coach/apiobjects/MissionDetails;-><init>()V

    .line 270
    .local v0, "missionDetails":Lcom/cigna/coach/apiobjects/MissionDetails;
    iget-wide v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$2;->val$timeMill:J

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance(J)Ljava/util/GregorianCalendar;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/apiobjects/MissionDetails;->setMissionActivityCompletedDate(Ljava/util/Calendar;)V

    .line 271
    invoke-virtual {v0, v2}, Lcom/cigna/coach/apiobjects/MissionDetails;->setNoOfActivities(I)V

    .line 272
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->addMissionDetails(Lcom/cigna/coach/apiobjects/MissionDetails;)V
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;Lcom/cigna/coach/apiobjects/MissionDetails;)V

    .line 274
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mTrakingDayOfWeekView:[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->access$100(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;)[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$2;->val$index:I

    aget-object v1, v1, v2

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->setTag(Ljava/lang/Object;)V

    .line 281
    :goto_1
    return-void

    .line 265
    .end local v0    # "missionDetails":Lcom/cigna/coach/apiobjects/MissionDetails;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 277
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mTrakingDayOfWeekView:[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->access$100(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;)[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$2;->val$index:I

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/MissionDetails;

    .line 279
    .restart local v0    # "missionDetails":Lcom/cigna/coach/apiobjects/MissionDetails;
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->removeMissionDetails(Lcom/cigna/coach/apiobjects/MissionDetails;)V
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->access$300(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;Lcom/cigna/coach/apiobjects/MissionDetails;)V

    goto :goto_1
.end method
