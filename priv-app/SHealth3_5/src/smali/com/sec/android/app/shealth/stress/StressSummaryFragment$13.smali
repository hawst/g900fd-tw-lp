.class Lcom/sec/android/app/shealth/stress/StressSummaryFragment$13;
.super Ljava/lang/Object;
.source "StressSummaryFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->showErrorDialog(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V
    .locals 0

    .prologue
    .line 1536
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$13;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 3
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 1540
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$13;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # setter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorView:Landroid/view/View;
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$3102(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Landroid/view/View;)Landroid/view/View;

    .line 1541
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$13;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$13;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$3100(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0809d1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    # setter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mAnimationViewErr0:Landroid/widget/ImageView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$3202(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 1542
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$13;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$13;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$3100(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0809d2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    # setter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mAnimationViewErr1:Landroid/widget/ImageView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$3302(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 1543
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$13;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$13;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$3100(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0809d3

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorDialogMsg1:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$3402(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 1544
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$13;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$13;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$3100(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0809d4

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorDialogMsg2:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$3502(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 1545
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$13;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$13;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$3100(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0809d5

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorDialogMsg3:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$3602(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 1546
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$13;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$13;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$3100(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/view/View;

    move-result-object v0

    const v2, 0x7f080554

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorDialogMsg4:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$3702(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 1549
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$13;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # invokes: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->initDialogAnimation()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$900(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V

    .line 1550
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$13;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->startDialogAnimation()V

    .line 1551
    return-void
.end method
