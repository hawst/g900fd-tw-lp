.class public abstract Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;
.super Ljava/lang/Object;
.source "CommonDaoImplDb.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commondao/CommonDao;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/sec/android/app/shealth/common/commondao/CommonDao",
        "<TT;>;"
    }
.end annotation


# static fields
.field protected static final DAO_SHARED_PREFERENCES:Ljava/lang/String; = "dao_shared_preferences"

.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field protected final context:Landroid/content/Context;

.field private final mIdColumnName:Ljava/lang/String;

.field private mIdColumnNameCheckedStatus:Z

.field protected final uri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Landroid/net/Uri;Landroid/content/Context;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 63
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const-string v0, "_id"

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->mIdColumnName:Ljava/lang/String;

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->mIdColumnNameCheckedStatus:Z

    .line 64
    if-nez p2, :cond_0

    .line 65
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Context value is not allowed to be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_0
    if-nez p1, :cond_1

    .line 67
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "URI value is not allowed to be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_1
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->uri:Landroid/net/Uri;

    .line 70
    iput-object p2, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->context:Landroid/content/Context;

    .line 71
    return-void
.end method


# virtual methods
.method protected convertCollectionToCsvString(Ljava/util/Collection;)Ljava/lang/String;
    .locals 5
    .param p1, "items"    # Ljava/util/Collection;

    .prologue
    .line 347
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb<TT;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 349
    .local v0, "builder":Ljava/lang/StringBuilder;
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 350
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 351
    .local v2, "item":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 352
    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 355
    .end local v2    # "item":Ljava/lang/Object;
    :cond_0
    const/4 v3, 0x0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 357
    .end local v1    # "i$":Ljava/util/Iterator;
    :goto_1
    return-object v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method protected convertDataIdToCsv(Ljava/util/Collection;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<TT;>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 367
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb<TT;>;"
    .local p1, "items":Ljava/util/Collection;, "Ljava/util/Collection<TT;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 369
    .local v0, "builder":Ljava/lang/StringBuilder;
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 370
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    .line 371
    .local v2, "item":Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;, "TT;"
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->getId()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 372
    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 375
    .end local v2    # "item":Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;, "TT;"
    :cond_0
    const/4 v3, 0x0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 377
    .end local v1    # "i$":Ljava/util/Iterator;
    :goto_1
    return-object v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public deleteAll()I
    .locals 1

    .prologue
    .line 232
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb<TT;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->getCommonMethodsFilter()Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->deleteAll(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)I

    move-result v0

    return v0
.end method

.method protected deleteAll(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)I
    .locals 4
    .param p1, "filter"    # Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;

    .prologue
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb<TT;>;"
    const/4 v3, 0x0

    .line 244
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->uri:Landroid/net/Uri;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;->getSQLCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 250
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->uri:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public deleteData(Ljava/util/List;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)I"
        }
    .end annotation

    .prologue
    .line 206
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb<TT;>;"
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<TT;>;"
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->getIdColumnName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->convertDataIdToCsv(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "IN"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 207
    .local v0, "ids":Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->deleteAll(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)I

    move-result v1

    return v1
.end method

.method public deleteDataById(J)Z
    .locals 1
    .param p1, "itemId"    # J

    .prologue
    .line 212
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb<TT;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->getCommonMethodsFilter()Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->deleteDataById(JLcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Z

    move-result v0

    return v0
.end method

.method protected deleteDataById(JLcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Z
    .locals 5
    .param p1, "itemId"    # J
    .param p3, "filter"    # Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;

    .prologue
    .line 222
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb<TT;>;"
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->getIdColumnName()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const-string v4, "="

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 223
    .local v0, "filterById":Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->getSQLCode()Ljava/lang/String;

    move-result-object v1

    .line 224
    .local v1, "whereClause":Ljava/lang/String;
    if-eqz p3, :cond_0

    .line 225
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;->getSQLCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 227
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->uri:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public generateStubDb()Z
    .locals 2

    .prologue
    .line 285
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "You need to implement it in concrete *DaoImplDb"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getAllDatas()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 90
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb<TT;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->getCommonMethodsFilter()Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->getAllDatas(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected final getAllDatas(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Ljava/util/List;
    .locals 2
    .param p1, "filter"    # Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb<TT;>;"
    const/4 v1, 0x0

    .line 101
    if-nez p1, :cond_0

    move-object v0, v1

    .line 102
    .local v0, "filterRule":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0, v1, v0, v1, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->getAllDatas([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    return-object v1

    .line 101
    .end local v0    # "filterRule":Ljava/lang/String;
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected final getAllDatas([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 8
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 123
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb<TT;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->uri:Landroid/net/Uri;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 124
    .local v6, "cursor":Landroid/database/Cursor;
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->getDatasFromCursor(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v7

    .line 125
    .local v7, "result":Ljava/util/List;, "Ljava/util/List<TT;>;"
    if-eqz v6, :cond_0

    .line 126
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 128
    :cond_0
    if-nez v7, :cond_1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v7

    .end local v7    # "result":Ljava/util/List;, "Ljava/util/List<TT;>;"
    :cond_1
    return-object v7
.end method

.method protected getCommonMethodsFilter()Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    .locals 1

    .prologue
    .line 303
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb<TT;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getDaoPrefernces()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 56
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb<TT;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->context:Landroid/content/Context;

    const-string v1, "dao_shared_preferences"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getDataContentValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Landroid/content/ContentValues;"
        }
    .end annotation
.end method

.method protected final getDatasFromCursor(Landroid/database/Cursor;)Ljava/util/List;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 267
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb<TT;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 268
    .local v0, "items":Ljava/util/List;, "Ljava/util/List<TT;>;"
    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 270
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->getItemFromRow(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 271
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 273
    :cond_1
    if-eqz p1, :cond_2

    .line 274
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 276
    :cond_2
    return-object v0
.end method

.method public getFirstItemFromQuery([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    .locals 8
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 324
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb<TT;>;"
    invoke-virtual {p4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, " LIMIT "

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 325
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LIMIT 1 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    .line 327
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->uri:Landroid/net/Uri;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 328
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 330
    .local v7, "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;, "TT;"
    if-eqz v6, :cond_1

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 331
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->getItemFromRow(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 334
    :cond_1
    if-eqz v6, :cond_2

    .line 335
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 338
    :cond_2
    return-object v7

    .line 334
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 335
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method protected getIdColumnName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 79
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb<TT;>;"
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->mIdColumnNameCheckedStatus:Z

    if-nez v0, :cond_2

    .line 80
    const-string v0, "_id"

    if-eqz v0, :cond_0

    const-string v0, "_id"

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This operation requires idColumnName to be set!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->mIdColumnNameCheckedStatus:Z

    .line 85
    :cond_2
    const-string v0, "_id"

    return-object v0
.end method

.method protected abstract getItemFromRow(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")TT;"
        }
    .end annotation
.end method

.method public insertData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)J"
        }
    .end annotation

    .prologue
    .line 133
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb<TT;>;"
    .local p1, "item":Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;, "TT;"
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->getDataContentValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)Landroid/content/ContentValues;

    move-result-object v1

    .line 134
    .local v1, "itemContent":Landroid/content/ContentValues;
    if-eqz v1, :cond_0

    .line 136
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->uri:Landroid/net/Uri;

    invoke-virtual {v3, v4, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    .line 137
    .local v2, "rawContactUri":Landroid/net/Uri;
    if-eqz v2, :cond_0

    .line 138
    sget-object v3, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Data inserted: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; in ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->uri:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v3

    .line 147
    .end local v2    # "rawContactUri":Landroid/net/Uri;
    :goto_0
    return-wide v3

    .line 141
    :catch_0
    move-exception v0

    .line 142
    .local v0, "e":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    sget-object v3, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insertData Got invalid Data, Unable to insert "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    .end local v0    # "e":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    :cond_0
    sget-object v3, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Data not inserted: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; in ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->uri:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const-wide/16 v3, -0x1

    goto :goto_0
.end method

.method public insertData(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb<TT;>;"
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<TT;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 153
    .local v2, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    .line 154
    .local v1, "item":Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;, "TT;"
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->insertData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 156
    .end local v1    # "item":Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;, "TT;"
    :cond_0
    return-object v2
.end method

.method public insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)J"
        }
    .end annotation

    .prologue
    .line 173
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb<TT;>;"
    .local p1, "item":Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;, "TT;"
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->updateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    .line 174
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->insertData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->setId(J)V

    .line 176
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->getId()J

    move-result-wide v0

    return-wide v0
.end method

.method public insertOrUpdateData(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 161
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb<TT;>;"
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<TT;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 162
    .local v2, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    .line 163
    .local v1, "item":Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;, "TT;"
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 165
    .end local v1    # "item":Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;, "TT;"
    :cond_0
    return-object v2
.end method

.method public updateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)I
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)I"
        }
    .end annotation

    .prologue
    .line 181
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb<TT;>;"
    .local p1, "item":Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;, "TT;"
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->getDataContentValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)Landroid/content/ContentValues;

    move-result-object v2

    .line 182
    .local v2, "itemContent":Landroid/content/ContentValues;
    if-eqz v2, :cond_0

    .line 183
    const-string v3, "_id"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    .line 184
    .local v1, "id":Ljava/lang/Long;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/16 v5, -0x1

    cmp-long v3, v3, v5

    if-eqz v3, :cond_0

    .line 186
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->uri:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_id = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 192
    .end local v1    # "id":Ljava/lang/Long;
    :goto_0
    return v3

    .line 187
    .restart local v1    # "id":Ljava/lang/Long;
    :catch_0
    move-exception v0

    .line 188
    .local v0, "e":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    sget-object v3, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " updateData Got invalid Data, Unable to update"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    .end local v0    # "e":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    .end local v1    # "id":Ljava/lang/Long;
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public updateData(Ljava/util/List;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)I"
        }
    .end annotation

    .prologue
    .line 197
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb<TT;>;"
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<TT;>;"
    const/4 v2, 0x0

    .line 198
    .local v2, "result":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    .line 199
    .local v1, "item":Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;, "TT;"
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->updateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)I

    move-result v3

    add-int/2addr v2, v3

    .line 200
    goto :goto_0

    .line 201
    .end local v1    # "item":Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;, "TT;"
    :cond_0
    return v2
.end method
