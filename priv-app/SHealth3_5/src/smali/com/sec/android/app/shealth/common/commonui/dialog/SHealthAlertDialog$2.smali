.class Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$2;
.super Landroid/app/Dialog;
.source "SHealthAlertDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;Landroid/content/Context;I)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # I

    .prologue
    .line 205
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$2;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-direct {p0, p2, p3}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 3

    .prologue
    .line 209
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$2;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->doNotDismissOnBackPressed:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->access$100(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 211
    invoke-super {p0}, Landroid/app/Dialog;->onBackPressed()V

    .line 213
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$2;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/IBackPressControllerProvider;

    if-eqz v1, :cond_2

    .line 215
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$2;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/commonui/dialog/IBackPressControllerProvider;

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$2;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getTag()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/IBackPressControllerProvider;->getBackPressController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IBackPressController;

    move-result-object v0

    .line 216
    .local v0, "backController":Lcom/sec/android/app/shealth/common/commonui/dialog/IBackPressController;
    if-eqz v0, :cond_2

    .line 218
    invoke-interface {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/IBackPressController;->onBackPress()V

    .line 226
    .end local v0    # "backController":Lcom/sec/android/app/shealth/common/commonui/dialog/IBackPressController;
    :cond_1
    :goto_0
    return-void

    .line 222
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$2;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->onBackPressListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->access$200(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 224
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$2;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->onBackPressListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->access$200(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$2;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;->onBackPress(Landroid/app/Activity;)V

    goto :goto_0
.end method
