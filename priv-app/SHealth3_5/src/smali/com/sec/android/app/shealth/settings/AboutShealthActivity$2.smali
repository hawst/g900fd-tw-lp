.class Lcom/sec/android/app/shealth/settings/AboutShealthActivity$2;
.super Ljava/lang/Object;
.source "AboutShealthActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/AboutShealthActivity;

.field final synthetic val$help:Landroid/widget/Button;

.field final synthetic val$koreanPopupButton:Landroid/widget/Button;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/AboutShealthActivity;Landroid/widget/Button;Landroid/widget/Button;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/AboutShealthActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/AboutShealthActivity;

    iput-object p2, p0, Lcom/sec/android/app/shealth/settings/AboutShealthActivity$2;->val$help:Landroid/widget/Button;

    iput-object p3, p0, Lcom/sec/android/app/shealth/settings/AboutShealthActivity$2;->val$koreanPopupButton:Landroid/widget/Button;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 75
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/AboutShealthActivity$2;->val$help:Landroid/widget/Button;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/AboutShealthActivity$2;->val$koreanPopupButton:Landroid/widget/Button;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Utils;->temporarilyDisableClick([Landroid/view/View;)V

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/AboutShealthActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/AboutShealthActivity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/AboutShealthActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/AboutShealthActivity;

    const-class v3, Lcom/sec/android/app/shealth/settings/AboutKoreanPopup;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->startActivity(Landroid/content/Intent;)V

    .line 77
    return-void
.end method
