.class public Lcom/sec/android/app/shealth/logutils/graph/GraphFragmentUtils;
.super Ljava/lang/Object;
.source "GraphFragmentUtils.java"


# static fields
.field static final PATTERN_24_HOURS:Ljava/lang/String; = "HH"

.field static final PATTERN_DAY_FORMAT:Ljava/lang/String; = "dd"

.field static final PATTERN_MONTH_FORMAT:Ljava/lang/String; = "MM"

.field public static final ROBOTO_LIGHT_FONT_FAMILY:Ljava/lang/String; = "sec-roboto-light"

.field private static final SERIES_HEIGHT_TO_SCALE_HEIGHT_FACTOR:F = 1.1f

.field static final VALUE_NOT_SET:I = -0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createColoredLegendMark(II)Lcom/sec/android/app/shealth/logutils/graph/LegendMark;
    .locals 2
    .param p0, "colorId"    # I
    .param p1, "stringId"    # I

    .prologue
    .line 809
    new-instance v0, Lcom/sec/android/app/shealth/logutils/graph/LegendMark;

    const/4 v1, -0x1

    invoke-direct {v0, v1, p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/LegendMark;-><init>(III)V

    return-object v0
.end method

.method public static createDrawabledLegendMark(II)Lcom/sec/android/app/shealth/logutils/graph/LegendMark;
    .locals 2
    .param p0, "drawableId"    # I
    .param p1, "stringId"    # I

    .prologue
    .line 835
    new-instance v0, Lcom/sec/android/app/shealth/logutils/graph/LegendMark;

    const/4 v1, -0x1

    invoke-direct {v0, p0, v1, p1}, Lcom/sec/android/app/shealth/logutils/graph/LegendMark;-><init>(III)V

    return-object v0
.end method

.method public static createHandlerLine(Landroid/content/Context;I)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "lineDrawable"    # I

    .prologue
    const/4 v6, 0x0

    .line 351
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/sec/android/app/shealth/logutils/R$integer;->graph_handler_width:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/sec/android/app/shealth/logutils/R$integer;->graph_handler_height:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 358
    .local v2, "handlerLineBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 359
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 360
    .local v0, "canvas":Landroid/graphics/Canvas;
    if-eqz v1, :cond_0

    .line 361
    invoke-virtual {v0}, Landroid/graphics/Canvas;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Canvas;->getHeight()I

    move-result v4

    invoke-virtual {v1, v6, v6, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 362
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 365
    :cond_0
    return-object v2
.end method

.method public static getChartHeight(F)F
    .locals 1
    .param p0, "dataMaxValue"    # F

    .prologue
    .line 707
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/logutils/graph/GraphFragmentUtils;->getChartHeight(FF)F

    move-result v0

    return v0
.end method

.method public static getChartHeight(FF)F
    .locals 2
    .param p0, "dataMaxValue"    # F
    .param p1, "goalValue"    # F

    .prologue
    .line 685
    invoke-static {p0, p1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    const v1, 0x3f8ccccd    # 1.1f

    mul-float/2addr v0, v1

    return v0
.end method

.method public static getChartInteraction(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;
    .locals 3
    .param p0, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    const/4 v2, 0x0

    .line 313
    new-instance v0, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;-><init>()V

    .line 315
    .local v0, "chartInteraction":Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInteractionEnabled(Z)V

    .line 317
    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setScaleZoomInteractionEnabled(Z)V

    .line 319
    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setPatialZoomInteractionEnabled(Z)V

    .line 321
    invoke-static {p0}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getGraphParams(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getMinZoomFactor()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInRate(F)V

    .line 325
    invoke-static {p0}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getGraphParams(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getMaxZoomFactor()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomOutRate(F)V

    .line 329
    return-object v0
.end method

.method public static getDefaultChartStyle(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;IILcom/sec/android/app/shealth/framework/ui/graph/PeriodH;Ljava/lang/String;)Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .locals 14
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "handlerBackgroundBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "chartBackgroundBitmap"    # Landroid/graphics/Bitmap;
    .param p3, "handlerListener"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;
    .param p4, "xAxisMarkCount"    # I
    .param p5, "yAxisMarkCount"    # I
    .param p6, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .param p7, "yAxisLabelTitle"    # Ljava/lang/String;

    .prologue
    .line 123
    new-instance v2, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move/from16 v0, p4

    move/from16 v1, p5

    invoke-direct {v2, v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;-><init>(II)V

    .line 127
    .local v2, "chartStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 131
    .local v12, "resources":Landroid/content/res/Resources;
    sget v3, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_chart_left_chart_padding:I

    invoke-virtual {v12, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sget v4, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_chart_right_chart_padding:I

    invoke-virtual {v12, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    sget v6, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_chart_top_chart_padding:I

    invoke-virtual {v12, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    sget v7, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_chart_bottom_chart_padding:I

    invoke-virtual {v12, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    invoke-virtual {v2, v3, v4, v6, v7}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPadding(FFFF)V

    .line 141
    sget v3, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_chart_x_axis_text_size:I

    invoke-virtual {v12, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    const-string/jumbo v4, "sec-roboto-light"

    const/4 v6, 0x1

    sget v7, Lcom/sec/android/app/shealth/logutils/R$color;->graph_chart_text_color:I

    invoke-virtual {v12, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    const/4 v8, 0x0

    invoke-static {v3, v4, v6, v7, v8}, Lcom/sec/android/app/shealth/logutils/graph/GraphFragmentUtils;->getTextStyle(FLjava/lang/String;ZII)Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v11

    .line 155
    .local v11, "axisTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    sget v3, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_chart_y_axis_label_text_size:I

    invoke-virtual {v12, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    const-string/jumbo v4, "sec-roboto-light"

    const/4 v6, 0x1

    sget v7, Lcom/sec/android/app/shealth/logutils/R$color;->graph_chart_text_color:I

    invoke-virtual {v12, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    const/4 v8, 0x0

    invoke-static {v3, v4, v6, v7, v8}, Lcom/sec/android/app/shealth/logutils/graph/GraphFragmentUtils;->getTextStyle(FLjava/lang/String;ZII)Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v13

    .line 169
    .local v13, "yLabelTitleStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    sget v3, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_chart_handler_text_size:I

    invoke-virtual {v12, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    const-string/jumbo v4, "sec-roboto-light"

    const/4 v6, 0x1

    sget v7, Lcom/sec/android/app/shealth/logutils/R$color;->graph_chart_text_color:I

    invoke-virtual {v12, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    const/4 v8, 0x1

    invoke-static {v3, v4, v6, v7, v8}, Lcom/sec/android/app/shealth/logutils/graph/GraphFragmentUtils;->getTextStyle(FLjava/lang/String;ZII)Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v5

    .line 183
    .local v5, "handlerItemTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTitleAlign(II)V

    .line 185
    const/4 v3, 0x0

    invoke-virtual {v2, v11, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTitleTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 191
    sget v3, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_chart_x_axis_stroke_width:I

    invoke-virtual {v12, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisWidth(FI)V

    .line 193
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisMarkingVisible(ZI)V

    .line 195
    sget v3, Lcom/sec/android/app/shealth/logutils/R$color;->graph_chart_x_axis_color:I

    invoke-virtual {v12, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisColor(II)V

    .line 197
    sget v3, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_chart_x_axis_text_space:I

    invoke-virtual {v12, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTextSpace(FI)V

    .line 199
    const/4 v3, 0x0

    invoke-virtual {v2, v11, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 201
    invoke-static/range {p6 .. p6}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getGraphParams(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->dateFormatPattern:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisDateFormat(Ljava/lang/String;I)V

    .line 207
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisMarkingVisible(ZI)V

    .line 209
    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextVisible(ZI)V

    .line 211
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLineVisible(ZI)V

    .line 213
    const/4 v3, 0x0

    invoke-virtual {v2, v11, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 215
    const/4 v3, 0x0

    move-object/from16 v0, p7

    invoke-virtual {v2, v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitle(Ljava/lang/String;I)V

    .line 217
    const/4 v3, 0x0

    invoke-virtual {v2, v13, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitleTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 219
    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelAlign(II)V

    .line 221
    sget v3, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_chart_y_axis_text_space:I

    invoke-virtual {v12, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextSpace(FI)V

    .line 225
    sget v3, Lcom/sec/android/app/shealth/logutils/R$integer;->graph_handler_item_stroke_width:I

    invoke-virtual {v12, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-float v4, v3

    sget v3, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_chart_holding_handler_item_width:I

    invoke-virtual {v12, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v6, v3

    sget v3, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_chart_holding_handler_item_height:I

    invoke-virtual {v12, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v7, v3

    sget v3, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_chart_holding_handler_item_height:I

    invoke-virtual {v12, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    const/high16 v8, 0x3f000000    # 0.5f

    mul-float/2addr v3, v8

    sget v8, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_chart_holding_handler_item_space:I

    invoke-virtual {v12, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    int-to-float v8, v8

    add-float/2addr v8, v3

    sget v3, Lcom/sec/android/app/shealth/logutils/R$integer;->graph_handler_item_text_offset:I

    invoke-virtual {v12, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-float v9, v3

    move-object v3, p1

    move-object/from16 v10, p6

    invoke-static/range {v2 .. v10}, Lcom/sec/android/app/shealth/logutils/graph/GraphFragmentUtils;->setHandlerItem(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Landroid/graphics/Bitmap;FLcom/samsung/android/sdk/chart/style/SchartTextStyle;FFFFLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 241
    sget v3, Lcom/sec/android/app/shealth/logutils/R$integer;->graph_chart_handler_time_out_delay:I

    invoke-virtual {v12, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerTimeOutDelay(J)V

    .line 243
    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerListener(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;)V

    .line 249
    sget v3, Lcom/sec/android/app/shealth/logutils/R$drawable;->s_health_graph_handler_line:I

    invoke-static {p0, v3}, Lcom/sec/android/app/shealth/logutils/graph/GraphFragmentUtils;->createHandlerLine(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerLineImage(Landroid/graphics/Bitmap;)V

    .line 255
    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setGraphBackgroundImage(Landroid/graphics/Bitmap;)V

    .line 257
    sget v3, Lcom/sec/android/app/shealth/logutils/R$color;->graph_bg_color:I

    invoke-virtual {v12, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setChartBackgroundColor(I)V

    .line 263
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setGraphSeparatorVisible(Z)V

    .line 265
    sget v3, Lcom/sec/android/app/shealth/logutils/R$color;->graph_chart_separator_color:I

    invoke-virtual {v12, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setGraphSeparatorColor(I)V

    .line 267
    sget v3, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_chart_separator_width:I

    invoke-virtual {v12, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setGraphSeparatorWidth(F)V

    .line 269
    sget v3, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_chart_separator_text_spacing_top:I

    invoke-virtual {v12, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setSeparatorTextSpacingTop(I)V

    .line 273
    move-object/from16 v0, p6

    invoke-static {p0, v2, v0}, Lcom/sec/android/app/shealth/logutils/graph/GraphFragmentUtils;->setSeparatorDateFormat(Landroid/content/Context;Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 277
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPopupEnable(Z)V

    .line 279
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerTooltipEnable(Z)V

    .line 283
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setLegendVisible(Z)V

    .line 285
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setMarkingLineBase(I)V

    .line 289
    return-object v2
.end method

.method public static getTextStyle(FLjava/lang/String;ZII)Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 3
    .param p0, "textSize"    # F
    .param p1, "fontFamily"    # Ljava/lang/String;
    .param p2, "antiAlias"    # Z
    .param p3, "textColor"    # I
    .param p4, "textAlign"    # I

    .prologue
    const/4 v2, 0x0

    .line 545
    new-instance v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 547
    .local v0, "textStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 549
    if-eqz p1, :cond_0

    .line 553
    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 559
    :cond_0
    invoke-virtual {v0, p2}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 561
    const/16 v1, 0xff

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 563
    invoke-virtual {v0, p3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setColor(I)V

    .line 565
    invoke-virtual {v0, p4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 567
    return-object v0
.end method

.method public static setGoal(Landroid/content/Context;Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;FI)V
    .locals 8
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "seriesStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;
    .param p2, "goalValue"    # F
    .param p3, "goalOverColor"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 463
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 465
    .local v2, "resources":Landroid/content/res/Resources;
    sget v3, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_chart_goal_value_text_size:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sget v4, Lcom/sec/android/app/shealth/logutils/R$color;->graph_chart_goal_text_color:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-static {v3, v7, v6, v4, v5}, Lcom/sec/android/app/shealth/logutils/graph/GraphFragmentUtils;->getTextStyle(FLjava/lang/String;ZII)Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v1

    .line 477
    .local v1, "goalValueTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    sget v3, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_chart_goal_label_text_size:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sget v4, Lcom/sec/android/app/shealth/logutils/R$color;->graph_chart_goal_text_color:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-static {v3, v7, v6, v4, v5}, Lcom/sec/android/app/shealth/logutils/graph/GraphFragmentUtils;->getTextStyle(FLjava/lang/String;ZII)Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v0

    .line 489
    .local v0, "goalLabelTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    invoke-virtual {p1, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setGoalLineTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 491
    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setGoalLineTextPostfixStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 493
    sget v3, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_chart_goal_text_margin_right:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    neg-float v3, v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setGoalLineTextOffsetX(F)V

    .line 495
    sget v3, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_chart_goal_value_text_size:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    neg-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setGoalLineTextOffsetY(F)V

    .line 499
    invoke-virtual {p1, p2}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setGoalLineValue(F)V

    .line 501
    sget v3, Lcom/sec/android/app/shealth/logutils/R$string;->goal_upper:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setGoalLineTextPostfix(Ljava/lang/String;)V

    .line 505
    invoke-virtual {p1, p3}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setGoalOverColor(I)V

    .line 507
    sget v3, Lcom/sec/android/app/shealth/logutils/R$color;->graph_chart_goal_line_color:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setGoalLineColor(I)V

    .line 509
    sget v3, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_chart_goal_line_thickness:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setGoalLineThickness(F)V

    .line 511
    return-void
.end method

.method public static setHandlerItem(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Landroid/graphics/Bitmap;FLcom/samsung/android/sdk/chart/style/SchartTextStyle;FFFFLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 1
    .param p0, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .param p1, "handlerImage"    # Landroid/graphics/Bitmap;
    .param p2, "strokeWidth"    # F
    .param p3, "textStyle"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .param p4, "itemWidth"    # F
    .param p5, "itemHeight"    # F
    .param p6, "itemOffSet"    # F
    .param p7, "itemTextOffset"    # F
    .param p8, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 605
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemImage(Landroid/graphics/Bitmap;)V

    .line 607
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemStrokeWidth(F)V

    .line 609
    invoke-virtual {p0, p3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 611
    invoke-virtual {p0, p4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 613
    invoke-virtual {p0, p5}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemHeight(F)V

    .line 615
    invoke-virtual {p0, p6}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemOffset(F)V

    .line 617
    invoke-virtual {p0, p7}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextOffset(F)V

    .line 619
    invoke-static {p8}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getGraphParams(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->dateFormatPattern:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemDateFormat(Ljava/lang/String;)V

    .line 621
    return-void
.end method

.method public static setSeparatorDateFormat(Landroid/content/Context;Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 7
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .param p2, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 637
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "date_format"

    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 641
    .local v3, "systemDateFormat":Ljava/lang/String;
    invoke-static {p2}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getGraphParams(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->bottomSeparatorDateFormatContents:[Ljava/lang/String;

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 645
    .local v0, "dateFormatContents":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v5, "[-/]"

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    .line 647
    .local v4, "systemDateFormatTokens":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {v4, v0}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->filterList(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 651
    .local v1, "filteredSystemDateFormatTokensIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    move-object v2, v5

    .line 655
    .local v2, "resultDateFormat":Ljava/lang/String;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 657
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x2f

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 651
    .end local v2    # "resultDateFormat":Ljava/lang/String;
    :cond_0
    const-string v2, ""

    goto :goto_0

    .line 661
    .restart local v2    # "resultDateFormat":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setSeparatorDateFormat(Ljava/lang/String;)V

    .line 663
    return-void
.end method

.method public static setSeriesNormalRange(Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;FFII)V
    .locals 2
    .param p0, "seriesStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;
    .param p1, "minValue"    # F
    .param p2, "maxValue"    # F
    .param p3, "colorInRange"    # I
    .param p4, "fillColor"    # I

    .prologue
    const/4 v1, -0x1

    .line 391
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setNormalRangeVisible(Z)V

    .line 392
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setNormalRangeMinValue(F)V

    .line 393
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setNormalRangeMaxValue(F)V

    .line 394
    if-eq p3, v1, :cond_0

    .line 395
    invoke-virtual {p0, p3}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setNormalRangeColorInRange(I)V

    .line 397
    :cond_0
    if-eq p4, v1, :cond_1

    .line 398
    invoke-virtual {p0, p4}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setNormalRangeFillColor(I)V

    .line 400
    :cond_1
    return-void
.end method

.method public static setSeriesValueMarking(Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p0, "seriesStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;
    .param p1, "normalBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "inRangeBitmap"    # Landroid/graphics/Bitmap;
    .param p3, "overInRangeBitmap"    # Landroid/graphics/Bitmap;
    .param p4, "overOutRangeBitmap"    # Landroid/graphics/Bitmap;
    .param p5, "selectedImage"    # Landroid/graphics/Bitmap;

    .prologue
    .line 431
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setValueMarkingVisible(Z)V

    .line 433
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setValueMarkingNormalImage(Landroid/graphics/Bitmap;)V

    .line 435
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setValueMarkingInRangeImage(Landroid/graphics/Bitmap;)V

    .line 437
    invoke-virtual {p0, p3}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setValueMarkingHandlerOverInRangeImage(Landroid/graphics/Bitmap;)V

    .line 439
    invoke-virtual {p0, p4}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setValueMarkingHandlerOverOutRangeImage(Landroid/graphics/Bitmap;)V

    .line 441
    invoke-virtual {p0, p5}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setValueMarkingSelectedImage(Landroid/graphics/Bitmap;)V

    .line 443
    return-void
.end method

.method public static shrinkHandler(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V
    .locals 1
    .param p0, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    .prologue
    const/4 v0, 0x0

    .line 725
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 727
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemHeight(F)V

    .line 729
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemOffset(F)V

    .line 731
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextVisible(Z)V

    .line 733
    return-void
.end method

.method public static swellHandler(Landroid/content/Context;Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Landroid/graphics/Bitmap;)V
    .locals 4
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .param p2, "handlerBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v3, 0x1

    .line 755
    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemVisible(Z)V

    .line 757
    invoke-virtual {p1, p2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemImage(Landroid/graphics/Bitmap;)V

    .line 759
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_chart_holding_handler_item_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 763
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_chart_holding_handler_item_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemHeight(F)V

    .line 769
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_chart_holding_handler_item_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/logutils/R$dimen;->graph_chart_holding_handler_item_space:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemOffset(F)V

    .line 779
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/logutils/R$integer;->graph_handler_item_text_offset:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextOffset(F)V

    .line 783
    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextVisible(Z)V

    .line 785
    return-void
.end method
