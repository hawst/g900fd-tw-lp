.class Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment$1;
.super Ljava/lang/Object;
.source "LibraryHomeFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;->setActionBarNormalMode()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 86
    instance-of v2, p1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 87
    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    .line 88
    .local v0, "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v2

    if-nez v2, :cond_1

    .line 89
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 90
    .local v1, "intentFavorite":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;->startActivity(Landroid/content/Intent;)V

    .line 99
    .end local v0    # "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    .end local v1    # "intentFavorite":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 92
    .restart local v0    # "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    goto :goto_0
.end method
