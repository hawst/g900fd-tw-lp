.class Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FragmentEventListener"
.end annotation


# instance fields
.field private isPaired:Z

.field public mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Z)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iput-boolean p3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->isPaired:Z

    return-void
.end method


# virtual methods
.method public onJoined(I)V
    .locals 50

    const/4 v1, 0x2

    new-array v4, v1, [J

    const/4 v1, 0x1

    const-wide/16 v2, 0x1

    aput-wide v2, v4, v1

    const/4 v1, 0x0

    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v4, v2

    long-to-int v2, v2

    if-gtz v2, :cond_0

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v3, 0x0

    move/from16 v0, p1

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    ushr-long v5, v1, v5

    aget-wide v1, v4, v3

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_1

    const-wide v7, -0x5c459428096def5aL    # -1.419958229901947E-136

    xor-long/2addr v1, v7

    :cond_1
    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    const/16 v7, 0x20

    shl-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, -0x5c459428096def5aL    # -1.419958229901947E-136

    xor-long/2addr v1, v5

    aput-wide v1, v4, v3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isAdded()Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    :goto_0
    return-void

    :cond_3
    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x7198

    aput v13, v2, v12

    const/16 v12, -0x13

    aput v12, v2, v11

    const/16 v11, -0x53

    aput v11, v2, v10

    const/16 v10, 0x449

    aput v10, v2, v9

    const/16 v9, -0x238a

    aput v9, v2, v8

    const/16 v8, -0x47

    aput v8, v2, v7

    const/16 v7, -0x21

    aput v7, v2, v6

    const/16 v6, -0x4e

    aput v6, v2, v5

    const/16 v5, -0x13

    aput v5, v2, v3

    const/16 v3, 0x7606

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, -0x71f3

    aput v14, v1, v13

    const/16 v13, -0x72

    aput v13, v1, v12

    const/16 v12, -0x3c

    aput v12, v1, v11

    const/16 v11, 0x43f

    aput v11, v1, v10

    const/16 v10, -0x23fc

    aput v10, v1, v9

    const/16 v9, -0x24

    aput v9, v1, v8

    const/16 v8, -0x74

    aput v8, v1, v7

    const/16 v7, -0x30

    aput v7, v1, v6

    const/16 v6, -0x7c

    aput v6, v1, v5

    const/16 v5, 0x766a

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x25

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, 0x1a

    const/16 v32, 0x1b

    const/16 v33, 0x1c

    const/16 v34, 0x1d

    const/16 v35, 0x1e

    const/16 v36, 0x1f

    const/16 v37, 0x20

    const/16 v38, 0x21

    const/16 v39, 0x22

    const/16 v40, 0x23

    const/16 v41, 0x24

    const/16 v42, 0x940

    aput v42, v2, v41

    const/16 v41, -0x1b86

    aput v41, v2, v40

    const/16 v40, -0x6f

    aput v40, v2, v39

    const/16 v39, -0x7c00

    aput v39, v2, v38

    const/16 v38, -0x1b

    aput v38, v2, v37

    const/16 v37, -0x49

    aput v37, v2, v36

    const/16 v36, 0x446d

    aput v36, v2, v35

    const/16 v35, 0x1964

    aput v35, v2, v34

    const/16 v34, -0x1b83

    aput v34, v2, v33

    const/16 v33, -0x7f

    aput v33, v2, v32

    const/16 v32, 0x460

    aput v32, v2, v31

    const/16 v31, 0x2c6d

    aput v31, v2, v30

    const/16 v30, -0x7ebd

    aput v30, v2, v29

    const/16 v29, -0x35

    aput v29, v2, v28

    const/16 v28, 0x2412

    aput v28, v2, v27

    const/16 v27, 0x564b

    aput v27, v2, v26

    const/16 v26, 0x2a6c

    aput v26, v2, v25

    const/16 v25, -0x2aa8

    aput v25, v2, v24

    const/16 v24, -0x50

    aput v24, v2, v23

    const/16 v23, -0x4b

    aput v23, v2, v22

    const/16 v22, -0x1a

    aput v22, v2, v21

    const/16 v21, -0x28

    aput v21, v2, v20

    const/16 v20, -0x38

    aput v20, v2, v19

    const/16 v19, 0x487d

    aput v19, v2, v18

    const/16 v18, 0x2604

    aput v18, v2, v17

    const/16 v17, -0x78ae

    aput v17, v2, v16

    const/16 v16, -0x17

    aput v16, v2, v15

    const/16 v15, -0x5ad5

    aput v15, v2, v14

    const/16 v14, -0x2d

    aput v14, v2, v13

    const/16 v13, -0x4f

    aput v13, v2, v12

    const/16 v12, -0x79ce

    aput v12, v2, v11

    const/16 v11, -0x1d

    aput v11, v2, v10

    const/16 v10, 0x4417

    aput v10, v2, v9

    const/16 v9, 0x7f34

    aput v9, v2, v8

    const/16 v8, -0x59e2

    aput v8, v2, v7

    const/16 v7, -0x2c

    aput v7, v2, v3

    const/16 v3, 0x4834

    aput v3, v2, v1

    const/16 v1, 0x25

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x11

    const/16 v24, 0x12

    const/16 v25, 0x13

    const/16 v26, 0x14

    const/16 v27, 0x15

    const/16 v28, 0x16

    const/16 v29, 0x17

    const/16 v30, 0x18

    const/16 v31, 0x19

    const/16 v32, 0x1a

    const/16 v33, 0x1b

    const/16 v34, 0x1c

    const/16 v35, 0x1d

    const/16 v36, 0x1e

    const/16 v37, 0x1f

    const/16 v38, 0x20

    const/16 v39, 0x21

    const/16 v40, 0x22

    const/16 v41, 0x23

    const/16 v42, 0x24

    const/16 v43, 0x97d

    aput v43, v1, v42

    const/16 v42, -0x1bf7

    aput v42, v1, v41

    const/16 v41, -0x1c

    aput v41, v1, v40

    const/16 v40, -0x7b8c

    aput v40, v1, v39

    const/16 v39, -0x7c

    aput v39, v1, v38

    const/16 v38, -0x3d

    aput v38, v1, v37

    const/16 v37, 0x441e

    aput v37, v1, v36

    const/16 v36, 0x1944

    aput v36, v1, v35

    const/16 v35, -0x1be7

    aput v35, v1, v34

    const/16 v34, -0x1c

    aput v34, v1, v33

    const/16 v33, 0x40e

    aput v33, v1, v32

    const/16 v32, 0x2c04

    aput v32, v1, v31

    const/16 v31, -0x7ed4

    aput v31, v1, v30

    const/16 v30, -0x7f

    aput v30, v1, v29

    const/16 v29, 0x247c

    aput v29, v1, v28

    const/16 v28, 0x5624

    aput v28, v1, v27

    const/16 v27, 0x2a56

    aput v27, v1, v26

    const/16 v26, -0x2ad6

    aput v26, v1, v25

    const/16 v25, -0x2b

    aput v25, v1, v24

    const/16 v24, -0x25

    aput v24, v1, v23

    const/16 v23, -0x7d

    aput v23, v1, v22

    const/16 v22, -0x54

    aput v22, v1, v21

    const/16 v21, -0x45

    aput v21, v1, v20

    const/16 v20, 0x4814

    aput v20, v1, v19

    const/16 v19, 0x2648

    aput v19, v1, v18

    const/16 v18, -0x78da

    aput v18, v1, v17

    const/16 v17, -0x79

    aput v17, v1, v16

    const/16 v16, -0x5ab2

    aput v16, v1, v15

    const/16 v15, -0x5b

    aput v15, v1, v14

    const/16 v14, -0xc

    aput v14, v1, v13

    const/16 v13, -0x79c0

    aput v13, v1, v12

    const/16 v12, -0x7a

    aput v12, v1, v11

    const/16 v11, 0x4467

    aput v11, v1, v10

    const/16 v10, 0x7f44

    aput v10, v1, v9

    const/16 v9, -0x5981

    aput v9, v1, v8

    const/16 v8, -0x5a

    aput v8, v1, v7

    const/16 v7, 0x4863

    aput v7, v1, v3

    const/4 v3, 0x0

    :goto_3
    array-length v7, v1

    if-lt v3, v7, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_4
    array-length v7, v1

    if-lt v3, v7, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-gtz v1, :cond_a

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, -0x4

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x34

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_8

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_6
    array-length v5, v1

    if-lt v3, v5, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_4
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_5
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_6
    aget v7, v1, v3

    aget v8, v2, v3

    xor-int/2addr v7, v8

    aput v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_7
    aget v7, v2, v3

    int-to-char v7, v7

    aput-char v7, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_8
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_9
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_a
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v6, v1, v6

    if-eqz v6, :cond_b

    const-wide v6, -0x5c459428096def5aL    # -1.419958229901947E-136

    xor-long/2addr v1, v6

    :cond_b
    const/16 v6, 0x20

    shl-long/2addr v1, v6

    const/16 v6, 0x20

    shr-long/2addr v1, v6

    long-to-int v1, v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-gtz v1, :cond_e

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0xf07

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0xf37

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_c

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_8
    array-length v5, v1

    if-lt v3, v5, :cond_d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_c
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_d
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :cond_e
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v5, 0x0

    cmp-long v3, v1, v5

    if-eqz v3, :cond_f

    const-wide v5, -0x5c459428096def5aL    # -1.419958229901947E-136

    xor-long/2addr v1, v5

    :cond_f
    const/16 v3, 0x20

    shl-long/2addr v1, v3

    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_12

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1400(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    move-result-object v1

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1400(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_10

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1400(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->cancel()V

    :cond_10
    :goto_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->scanningfragment_failed_to_connect:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->showToast(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$2100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->progressDlg:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5500(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v1

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->progressDlg:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5500(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_11

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->progressDlg:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5500(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->dismiss()V

    :cond_11
    :goto_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->IsJoining:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$2302(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z

    goto/16 :goto_0

    :cond_12
    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-gtz v1, :cond_15

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x816

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x826

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_b
    array-length v5, v1

    if-lt v3, v5, :cond_13

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_c
    array-length v5, v1

    if-lt v3, v5, :cond_14

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_13
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_b

    :cond_14
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_c

    :cond_15
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v5, 0x0

    cmp-long v3, v1, v5

    if-eqz v3, :cond_16

    const-wide v5, -0x5c459428096def5aL    # -1.419958229901947E-136

    xor-long/2addr v1, v5

    :cond_16
    const/16 v3, 0x20

    shl-long/2addr v1, v3

    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    if-nez v1, :cond_17

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->progressDlg:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5500(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v1

    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->progressDlg:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5500(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_17

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->progressDlg:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5500(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->dismiss()V

    :cond_17
    :goto_d
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->connectToastReq:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5300(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Z

    move-result v1

    if-eqz v1, :cond_18

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    :try_start_3
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener$1;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_18
    :goto_e
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->connectToastReq:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5302(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->isPaired:Z

    if-nez v1, :cond_25

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x3c

    aput v13, v2, v12

    const/16 v12, 0x3e40

    aput v12, v2, v11

    const/16 v11, 0x57

    aput v11, v2, v10

    const/16 v10, 0x5576

    aput v10, v2, v9

    const/16 v9, 0x4327

    aput v9, v2, v8

    const/16 v8, -0x39da

    aput v8, v2, v7

    const/16 v7, -0x6b

    aput v7, v2, v6

    const/16 v6, 0x3c53

    aput v6, v2, v5

    const/16 v5, 0x5d55

    aput v5, v2, v3

    const/16 v3, -0x78cf

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, -0x5f

    aput v14, v1, v13

    const/16 v13, 0x3e23

    aput v13, v1, v12

    const/16 v12, 0x3e

    aput v12, v1, v11

    const/16 v11, 0x5500

    aput v11, v1, v10

    const/16 v10, 0x4355

    aput v10, v1, v9

    const/16 v9, -0x39bd

    aput v9, v1, v8

    const/16 v8, -0x3a

    aput v8, v1, v7

    const/16 v7, 0x3c31

    aput v7, v1, v6

    const/16 v6, 0x5d3c

    aput v6, v1, v5

    const/16 v5, -0x78a3

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_f
    array-length v5, v1

    if-lt v3, v5, :cond_19

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_10
    array-length v5, v1

    if-lt v3, v5, :cond_1a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x23

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, 0x1a

    const/16 v32, 0x1b

    const/16 v33, 0x1c

    const/16 v34, 0x1d

    const/16 v35, 0x1e

    const/16 v36, 0x1f

    const/16 v37, 0x20

    const/16 v38, 0x21

    const/16 v39, 0x22

    const/16 v40, -0x6c

    aput v40, v2, v39

    const/16 v39, -0x3b

    aput v39, v2, v38

    const/16 v38, -0x3c

    aput v38, v2, v37

    const/16 v37, -0x2cca

    aput v37, v2, v36

    const/16 v36, -0x4e

    aput v36, v2, v35

    const/16 v35, 0x7f1b

    aput v35, v2, v34

    const/16 v34, 0x3f0c

    aput v34, v2, v33

    const/16 v33, -0x35e1

    aput v33, v2, v32

    const/16 v32, -0x42

    aput v32, v2, v31

    const/16 v31, -0x3d

    aput v31, v2, v30

    const/16 v30, -0x37

    aput v30, v2, v29

    const/16 v29, -0x27

    aput v29, v2, v28

    const/16 v28, -0x44e5

    aput v28, v2, v27

    const/16 v27, -0x2c

    aput v27, v2, v26

    const/16 v26, -0x47

    aput v26, v2, v25

    const/16 v25, -0x52

    aput v25, v2, v24

    const/16 v24, -0x4b

    aput v24, v2, v23

    const/16 v23, -0x56

    aput v23, v2, v22

    const/16 v22, -0x7a

    aput v22, v2, v21

    const/16 v21, -0x46f9

    aput v21, v2, v20

    const/16 v20, -0x36

    aput v20, v2, v19

    const/16 v19, -0x26

    aput v19, v2, v18

    const/16 v18, 0x2164

    aput v18, v2, v17

    const/16 v17, -0x31ab

    aput v17, v2, v16

    const/16 v16, -0x60

    aput v16, v2, v15

    const/16 v15, 0x7d03

    aput v15, v2, v14

    const/16 v14, -0x48f5

    aput v14, v2, v13

    const/16 v13, -0xe

    aput v13, v2, v12

    const/16 v12, 0x553f

    aput v12, v2, v11

    const/16 v11, 0x5730

    aput v11, v2, v10

    const/16 v10, -0x29d9

    aput v10, v2, v9

    const/16 v9, -0x5a

    aput v9, v2, v8

    const/16 v8, -0x53

    aput v8, v2, v7

    const/16 v7, -0x12

    aput v7, v2, v3

    const/16 v3, -0x14

    aput v3, v2, v1

    const/16 v1, 0x23

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x11

    const/16 v24, 0x12

    const/16 v25, 0x13

    const/16 v26, 0x14

    const/16 v27, 0x15

    const/16 v28, 0x16

    const/16 v29, 0x17

    const/16 v30, 0x18

    const/16 v31, 0x19

    const/16 v32, 0x1a

    const/16 v33, 0x1b

    const/16 v34, 0x1c

    const/16 v35, 0x1d

    const/16 v36, 0x1e

    const/16 v37, 0x1f

    const/16 v38, 0x20

    const/16 v39, 0x21

    const/16 v40, 0x22

    const/16 v41, -0x57

    aput v41, v1, v40

    const/16 v40, -0x4a

    aput v40, v1, v39

    const/16 v39, -0x4f

    aput v39, v1, v38

    const/16 v38, -0x2cbe

    aput v38, v1, v37

    const/16 v37, -0x2d

    aput v37, v1, v36

    const/16 v36, 0x7f6f

    aput v36, v1, v35

    const/16 v35, 0x3f7f

    aput v35, v1, v34

    const/16 v34, -0x35c1

    aput v34, v1, v33

    const/16 v33, -0x36

    aput v33, v1, v32

    const/16 v32, -0x5b

    aput v32, v1, v31

    const/16 v31, -0x54

    aput v31, v1, v30

    const/16 v30, -0x6b

    aput v30, v1, v29

    const/16 v29, -0x448b

    aput v29, v1, v28

    const/16 v28, -0x45

    aput v28, v1, v27

    const/16 v27, -0x7d

    aput v27, v1, v26

    const/16 v26, -0x24

    aput v26, v1, v25

    const/16 v25, -0x30

    aput v25, v1, v24

    const/16 v24, -0x3c

    aput v24, v1, v23

    const/16 v23, -0x1d

    aput v23, v1, v22

    const/16 v22, -0x468d

    aput v22, v1, v21

    const/16 v21, -0x47

    aput v21, v1, v20

    const/16 v20, -0x4d

    aput v20, v1, v19

    const/16 v19, 0x2128

    aput v19, v1, v18

    const/16 v18, -0x31df

    aput v18, v1, v17

    const/16 v17, -0x32

    aput v17, v1, v16

    const/16 v16, 0x7d66

    aput v16, v1, v15

    const/16 v15, -0x4883

    aput v15, v1, v14

    const/16 v14, -0x49

    aput v14, v1, v13

    const/16 v13, 0x554d

    aput v13, v1, v12

    const/16 v12, 0x5755

    aput v12, v1, v11

    const/16 v11, -0x29a9

    aput v11, v1, v10

    const/16 v10, -0x2a

    aput v10, v1, v9

    const/16 v9, -0x34

    aput v9, v1, v8

    const/16 v8, -0x64

    aput v8, v1, v7

    const/16 v7, -0x45

    aput v7, v1, v3

    const/4 v3, 0x0

    :goto_11
    array-length v7, v1

    if-lt v3, v7, :cond_1b

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_12
    array-length v7, v1

    if-lt v3, v7, :cond_1c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-gtz v1, :cond_1f

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x5a

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x6a

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_13
    array-length v5, v1

    if-lt v3, v5, :cond_1d

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_14
    array-length v5, v1

    if-lt v3, v5, :cond_1e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_19
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_f

    :cond_1a
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_10

    :cond_1b
    aget v7, v1, v3

    aget v8, v2, v3

    xor-int/2addr v7, v8

    aput v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_11

    :cond_1c
    aget v7, v2, v3

    int-to-char v7, v7

    aput-char v7, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_12

    :cond_1d
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_13

    :cond_1e
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_14

    :cond_1f
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v6, v1, v6

    if-eqz v6, :cond_20

    const-wide v6, -0x5c459428096def5aL    # -1.419958229901947E-136

    xor-long/2addr v1, v6

    :cond_20
    const/16 v6, 0x20

    shl-long/2addr v1, v6

    const/16 v6, 0x20

    shr-long/2addr v1, v6

    long-to-int v1, v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->handler:Landroid/os/Handler;

    const/16 v2, 0x6c

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-gtz v1, :cond_23

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x45

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x75

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_15
    array-length v5, v1

    if-lt v3, v5, :cond_21

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_16
    array-length v5, v1

    if-lt v3, v5, :cond_22

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_21
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_15

    :cond_22
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_16

    :cond_23
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v4, 0x0

    cmp-long v4, v1, v4

    if-eqz v4, :cond_24

    const-wide v4, -0x5c459428096def5aL    # -1.419958229901947E-136

    xor-long/2addr v1, v4

    :cond_24
    const/16 v4, 0x20

    shl-long/2addr v1, v4

    const/16 v4, 0x20

    shr-long/2addr v1, v4

    long-to-int v1, v1

    iput v1, v3, Landroid/os/Message;->arg2:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_25
    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-gtz v1, :cond_28

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x33

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, -0x3

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_17
    array-length v5, v1

    if-lt v3, v5, :cond_26

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_18
    array-length v5, v1

    if-lt v3, v5, :cond_27

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_26
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_17

    :cond_27
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_18

    :cond_28
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-eqz v3, :cond_29

    const-wide v3, -0x5c459428096def5aL    # -1.419958229901947E-136

    xor-long/2addr v1, v3

    :cond_29
    const/16 v3, 0x20

    shl-long/2addr v1, v3

    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    if-nez v1, :cond_2

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x6cec

    aput v12, v2, v11

    const/16 v11, -0x10

    aput v11, v2, v10

    const/16 v10, 0x2a12

    aput v10, v2, v9

    const/16 v9, -0x32a4

    aput v9, v2, v8

    const/16 v8, -0x41

    aput v8, v2, v7

    const/16 v7, -0x11fa

    aput v7, v2, v6

    const/16 v6, -0x43

    aput v6, v2, v5

    const/16 v5, 0x350c

    aput v5, v2, v4

    const/16 v4, 0x175c

    aput v4, v2, v3

    const/16 v3, 0x6f7b    # 3.9992E-41f

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x6c8f

    aput v13, v1, v12

    const/16 v12, -0x6d

    aput v12, v1, v11

    const/16 v11, 0x2a7b

    aput v11, v1, v10

    const/16 v10, -0x32d6

    aput v10, v1, v9

    const/16 v9, -0x33

    aput v9, v1, v8

    const/16 v8, -0x119d

    aput v8, v1, v7

    const/16 v7, -0x12

    aput v7, v1, v6

    const/16 v6, 0x356e

    aput v6, v1, v5

    const/16 v5, 0x1735

    aput v5, v1, v4

    const/16 v4, 0x6f17

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_19
    array-length v4, v1

    if-lt v3, v4, :cond_2a

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1a
    array-length v4, v1

    if-lt v3, v4, :cond_2b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x15

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, -0x19

    aput v24, v2, v23

    const/16 v23, -0x59c5

    aput v23, v2, v22

    const/16 v22, -0x31

    aput v22, v2, v21

    const/16 v21, 0x733

    aput v21, v2, v20

    const/16 v20, -0x89e

    aput v20, v2, v19

    const/16 v19, -0x6d

    aput v19, v2, v18

    const/16 v18, -0x70

    aput v18, v2, v17

    const/16 v17, 0x6d54

    aput v17, v2, v16

    const/16 v16, 0x808

    aput v16, v2, v15

    const/16 v15, -0x7286

    aput v15, v2, v14

    const/16 v14, -0x1c

    aput v14, v2, v13

    const/16 v13, -0x5f

    aput v13, v2, v12

    const/16 v12, 0x152a

    aput v12, v2, v11

    const/16 v11, 0x4735

    aput v11, v2, v10

    const/16 v10, 0x6d6a

    aput v10, v2, v9

    const/16 v9, 0x4329

    aput v9, v2, v8

    const/16 v8, 0x6606

    aput v8, v2, v7

    const/16 v7, -0x31d8

    aput v7, v2, v6

    const/16 v6, -0x79

    aput v6, v2, v5

    const/16 v5, -0x16

    aput v5, v2, v3

    const/16 v3, -0x2c

    aput v3, v2, v1

    const/16 v1, 0x15

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, -0x7e

    aput v25, v1, v24

    const/16 v24, -0x59a8

    aput v24, v1, v23

    const/16 v23, -0x5a

    aput v23, v1, v22

    const/16 v22, 0x745

    aput v22, v1, v21

    const/16 v21, -0x8f9

    aput v21, v1, v20

    const/16 v20, -0x9

    aput v20, v1, v19

    const/16 v19, -0x50

    aput v19, v1, v18

    const/16 v18, 0x6d30

    aput v18, v1, v17

    const/16 v17, 0x86d

    aput v17, v1, v16

    const/16 v16, -0x72f8

    aput v16, v1, v15

    const/16 v15, -0x73

    aput v15, v1, v14

    const/16 v14, -0x40

    aput v14, v1, v13

    const/16 v13, 0x157a

    aput v13, v1, v12

    const/16 v12, 0x4715

    aput v12, v1, v11

    const/16 v11, 0x6d47

    aput v11, v1, v10

    const/16 v10, 0x436d

    aput v10, v1, v9

    const/16 v9, 0x6643

    aput v9, v1, v8

    const/16 v8, -0x319a

    aput v8, v1, v7

    const/16 v7, -0x32

    aput v7, v1, v6

    const/16 v6, -0x5b

    aput v6, v1, v5

    const/16 v5, -0x62

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_1b
    array-length v5, v1

    if-lt v3, v5, :cond_2c

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1c
    array-length v5, v1

    if-lt v3, v5, :cond_2d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0x462a

    aput v12, v2, v11

    const/16 v11, -0x4ddb

    aput v11, v2, v10

    const/16 v10, -0x25

    aput v10, v2, v9

    const/16 v9, 0x6f5e

    aput v9, v2, v8

    const/16 v8, -0x53e3

    aput v8, v2, v7

    const/16 v7, -0x37

    aput v7, v2, v6

    const/16 v6, -0x36

    aput v6, v2, v5

    const/16 v5, 0x394a

    aput v5, v2, v4

    const/16 v4, -0x7cb0

    aput v4, v2, v3

    const/16 v3, -0x11

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0x464f

    aput v13, v1, v12

    const/16 v12, -0x4dba

    aput v12, v1, v11

    const/16 v11, -0x4e

    aput v11, v1, v10

    const/16 v10, 0x6f28

    aput v10, v1, v9

    const/16 v9, -0x5391

    aput v9, v1, v8

    const/16 v8, -0x54

    aput v8, v1, v7

    const/16 v7, -0x67

    aput v7, v1, v6

    const/16 v6, 0x3928

    aput v6, v1, v5

    const/16 v5, -0x7cc7

    aput v5, v1, v4

    const/16 v4, -0x7d

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_1d
    array-length v4, v1

    if-lt v3, v4, :cond_2e

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1e
    array-length v4, v1

    if-lt v3, v4, :cond_2f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x17

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, -0x3198

    aput v26, v2, v25

    const/16 v25, -0x63

    aput v25, v2, v24

    const/16 v24, -0x27

    aput v24, v2, v23

    const/16 v23, -0x4a

    aput v23, v2, v22

    const/16 v22, -0x24

    aput v22, v2, v21

    const/16 v21, -0x68

    aput v21, v2, v20

    const/16 v20, 0x6161

    aput v20, v2, v19

    const/16 v19, 0x525

    aput v19, v2, v18

    const/16 v18, 0x45a

    aput v18, v2, v17

    const/16 v17, -0x1fc0

    aput v17, v2, v16

    const/16 v16, -0x5b

    aput v16, v2, v15

    const/16 v15, -0x2c98

    aput v15, v2, v14

    const/16 v14, -0x66

    aput v14, v2, v13

    const/16 v13, -0x20

    aput v13, v2, v12

    const/16 v12, -0x8b7

    aput v12, v2, v11

    const/16 v11, -0x58

    aput v11, v2, v10

    const/16 v10, -0x24

    aput v10, v2, v9

    const/16 v9, -0x40

    aput v9, v2, v8

    const/16 v8, -0x19b9

    aput v8, v2, v7

    const/16 v7, -0x4a

    aput v7, v2, v6

    const/16 v6, 0x385e

    aput v6, v2, v5

    const/16 v5, -0x28f

    aput v5, v2, v3

    const/16 v3, -0x47

    aput v3, v2, v1

    const/16 v1, 0x17

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, -0x31b8

    aput v27, v1, v26

    const/16 v26, -0x32

    aput v26, v1, v25

    const/16 v25, -0x64

    aput v25, v1, v24

    const/16 v24, -0xb

    aput v24, v1, v23

    const/16 v23, -0x6b

    aput v23, v1, v22

    const/16 v22, -0x32

    aput v22, v1, v21

    const/16 v21, 0x6124

    aput v21, v1, v20

    const/16 v20, 0x561

    aput v20, v1, v19

    const/16 v19, 0x405

    aput v19, v1, v18

    const/16 v18, -0x1ffc

    aput v18, v1, v17

    const/16 v17, -0x20

    aput v17, v1, v16

    const/16 v16, -0x2cc6

    aput v16, v1, v15

    const/16 v15, -0x2d

    aput v15, v1, v14

    const/16 v14, -0x5f

    aput v14, v1, v13

    const/16 v13, -0x8e7

    aput v13, v1, v12

    const/16 v12, -0x9

    aput v12, v1, v11

    const/16 v11, -0x7b

    aput v11, v1, v10

    const/16 v10, -0x7f

    aput v10, v1, v9

    const/16 v9, -0x19f5

    aput v9, v1, v8

    const/16 v8, -0x1a

    aput v8, v1, v7

    const/16 v7, 0x380d

    aput v7, v1, v6

    const/16 v6, -0x2c8

    aput v6, v1, v5

    const/4 v5, -0x3

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_1f
    array-length v5, v1

    if-lt v3, v5, :cond_30

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_20
    array-length v5, v1

    if-lt v3, v5, :cond_31

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->handler:Landroid/os/Handler;

    const/16 v2, 0x65

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x3a

    aput v13, v2, v12

    const/16 v12, -0x16

    aput v12, v2, v11

    const/16 v11, -0x49

    aput v11, v2, v10

    const/16 v10, -0x58b7

    aput v10, v2, v9

    const/16 v9, -0x2b

    aput v9, v2, v8

    const/16 v8, -0x1a

    aput v8, v2, v7

    const/16 v7, 0x2e64

    aput v7, v2, v6

    const/16 v6, -0x7b4

    aput v6, v2, v5

    const/16 v5, -0x6f

    aput v5, v2, v3

    const/16 v3, -0x4c

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, -0x5d

    aput v14, v1, v13

    const/16 v13, -0x77

    aput v13, v1, v12

    const/16 v12, -0x22

    aput v12, v1, v11

    const/16 v11, -0x58c1

    aput v11, v1, v10

    const/16 v10, -0x59

    aput v10, v1, v9

    const/16 v9, -0x7d

    aput v9, v1, v8

    const/16 v8, 0x2e37

    aput v8, v1, v7

    const/16 v7, -0x7d2

    aput v7, v1, v6

    const/4 v6, -0x8

    aput v6, v1, v5

    const/16 v5, -0x28

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_21
    array-length v5, v1

    if-lt v3, v5, :cond_32

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_22
    array-length v5, v1

    if-lt v3, v5, :cond_33

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    const/16 v1, 0x2c

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, 0x26

    const/16 v43, 0x27

    const/16 v44, 0x28

    const/16 v45, 0x29

    const/16 v46, 0x2a

    const/16 v47, 0x2b

    const/16 v48, 0x6a15

    aput v48, v2, v47

    const/16 v47, -0x2cc7

    aput v47, v2, v46

    const/16 v46, -0x6a

    aput v46, v2, v45

    const/16 v45, -0x53

    aput v45, v2, v44

    const/16 v44, -0x50

    aput v44, v2, v43

    const/16 v43, -0x70

    aput v43, v2, v42

    const/16 v42, -0x13ab

    aput v42, v2, v41

    const/16 v41, -0x58

    aput v41, v2, v40

    const/16 v40, 0x6606

    aput v40, v2, v39

    const/16 v39, 0x3922

    aput v39, v2, v38

    const/16 v38, 0x287c

    aput v38, v2, v37

    const/16 v37, -0x5886

    aput v37, v2, v36

    const/16 v36, -0x12

    aput v36, v2, v35

    const/16 v35, -0x78

    aput v35, v2, v34

    const/16 v34, 0x214a

    aput v34, v2, v33

    const/16 v33, -0x5582

    aput v33, v2, v32

    const/16 v32, -0xd

    aput v32, v2, v31

    const/16 v31, -0x68

    aput v31, v2, v30

    const/16 v30, -0x4c

    aput v30, v2, v29

    const/16 v29, -0x12

    aput v29, v2, v28

    const/16 v28, 0x150a

    aput v28, v2, v27

    const/16 v27, -0x6da4

    aput v27, v2, v26

    const/16 v26, -0x2a

    aput v26, v2, v25

    const/16 v25, 0x525c

    aput v25, v2, v24

    const/16 v24, 0x3b6f

    aput v24, v2, v23

    const/16 v23, 0x561b

    aput v23, v2, v22

    const/16 v22, 0x2222

    aput v22, v2, v21

    const/16 v21, -0x34bd

    aput v21, v2, v20

    const/16 v20, -0x5d

    aput v20, v2, v19

    const/16 v19, -0x4e

    aput v19, v2, v18

    const/16 v18, -0x2dce

    aput v18, v2, v17

    const/16 v17, -0x55

    aput v17, v2, v16

    const/16 v16, -0x1fa1

    aput v16, v2, v15

    const/16 v15, -0x7f

    aput v15, v2, v14

    const/16 v14, 0x5d69

    aput v14, v2, v13

    const/16 v13, -0x5ac5

    aput v13, v2, v12

    const/16 v12, -0x34

    aput v12, v2, v11

    const/16 v11, -0x43

    aput v11, v2, v10

    const/16 v10, -0x2a

    aput v10, v2, v9

    const/16 v9, 0x417a

    aput v9, v2, v8

    const/16 v8, 0x752e

    aput v8, v2, v7

    const/16 v7, -0x2ce8

    aput v7, v2, v6

    const/16 v6, -0x4a

    aput v6, v2, v3

    const/16 v3, -0x33

    aput v3, v2, v1

    const/16 v1, 0x2c

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, 0x1a

    const/16 v32, 0x1b

    const/16 v33, 0x1c

    const/16 v34, 0x1d

    const/16 v35, 0x1e

    const/16 v36, 0x1f

    const/16 v37, 0x20

    const/16 v38, 0x21

    const/16 v39, 0x22

    const/16 v40, 0x23

    const/16 v41, 0x24

    const/16 v42, 0x25

    const/16 v43, 0x26

    const/16 v44, 0x27

    const/16 v45, 0x28

    const/16 v46, 0x29

    const/16 v47, 0x2a

    const/16 v48, 0x2b

    const/16 v49, 0x6a35

    aput v49, v1, v48

    const/16 v48, -0x2c96

    aput v48, v1, v47

    const/16 v47, -0x2d

    aput v47, v1, v46

    const/16 v46, -0x12

    aput v46, v1, v45

    const/16 v45, -0x7

    aput v45, v1, v44

    const/16 v44, -0x3a

    aput v44, v1, v43

    const/16 v43, -0x13f0

    aput v43, v1, v42

    const/16 v42, -0x14

    aput v42, v1, v41

    const/16 v41, 0x6659

    aput v41, v1, v40

    const/16 v40, 0x3966

    aput v40, v1, v39

    const/16 v39, 0x2839

    aput v39, v1, v38

    const/16 v38, -0x58d8

    aput v38, v1, v37

    const/16 v37, -0x59

    aput v37, v1, v36

    const/16 v36, -0x37

    aput v36, v1, v35

    const/16 v35, 0x211a

    aput v35, v1, v34

    const/16 v34, -0x55df

    aput v34, v1, v33

    const/16 v33, -0x56

    aput v33, v1, v32

    const/16 v32, -0x27

    aput v32, v1, v31

    const/16 v31, -0x8

    aput v31, v1, v30

    const/16 v30, -0x42

    aput v30, v1, v29

    const/16 v29, 0x1559

    aput v29, v1, v28

    const/16 v28, -0x6deb

    aput v28, v1, v27

    const/16 v27, -0x6e

    aput v27, v1, v26

    const/16 v26, 0x527c

    aput v26, v1, v25

    const/16 v25, 0x3b52

    aput v25, v1, v24

    const/16 v24, 0x563b

    aput v24, v1, v23

    const/16 v23, 0x2256

    aput v23, v1, v22

    const/16 v22, -0x34de

    aput v22, v1, v21

    const/16 v21, -0x35

    aput v21, v1, v20

    const/16 v20, -0x3b

    aput v20, v1, v19

    const/16 v19, -0x2dee

    aput v19, v1, v18

    const/16 v18, -0x2e

    aput v18, v1, v17

    const/16 v17, -0x1fcf

    aput v17, v1, v16

    const/16 v16, -0x20

    aput v16, v1, v15

    const/16 v15, 0x5d49

    aput v15, v1, v14

    const/16 v14, -0x5aa3

    aput v14, v1, v13

    const/16 v13, -0x5b

    aput v13, v1, v12

    const/16 v12, -0x63

    aput v12, v1, v11

    const/16 v11, -0x4d

    aput v11, v1, v10

    const/16 v10, 0x410c

    aput v10, v1, v9

    const/16 v9, 0x7541

    aput v9, v1, v8

    const/16 v8, -0x2c8b

    aput v8, v1, v7

    const/16 v7, -0x2d

    aput v7, v1, v6

    const/16 v6, -0x41

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_23
    array-length v6, v1

    if-lt v3, v6, :cond_34

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_24
    array-length v6, v1

    if-lt v3, v6, :cond_35

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->handler:Landroid/os/Handler;

    const/16 v2, 0x65

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_2a
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_19

    :cond_2b
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1a

    :cond_2c
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1b

    :cond_2d
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1c

    :cond_2e
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1d

    :cond_2f
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1e

    :cond_30
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1f

    :cond_31
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_20

    :cond_32
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_21

    :cond_33
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_22

    :cond_34
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_23

    :cond_35
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_24

    :catch_0
    move-exception v1

    goto/16 :goto_9

    :catch_1
    move-exception v1

    goto/16 :goto_a

    :catch_2
    move-exception v1

    goto/16 :goto_d

    :catch_3
    move-exception v1

    goto/16 :goto_e
.end method

.method public onLeft(I)V
    .locals 42

    const/4 v1, 0x2

    new-array v4, v1, [J

    const/4 v1, 0x1

    const-wide/16 v2, 0x1

    aput-wide v2, v4, v1

    const/4 v1, 0x0

    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v4, v2

    long-to-int v2, v2

    if-gtz v2, :cond_0

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v3, 0x0

    move/from16 v0, p1

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    ushr-long v5, v1, v5

    aget-wide v1, v4, v3

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_1

    const-wide v7, -0x7d1a108fa7d8d031L

    xor-long/2addr v1, v7

    :cond_1
    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    const/16 v7, 0x20

    shl-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, -0x7d1a108fa7d8d031L

    xor-long/2addr v1, v5

    aput-wide v1, v4, v3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->IsAutoScanRequired:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1902(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0x3567

    aput v13, v2, v12

    const/16 v12, 0x2256

    aput v12, v2, v11

    const/16 v11, 0x1c4b

    aput v11, v2, v10

    const/16 v10, -0x996

    aput v10, v2, v9

    const/16 v9, -0x7c

    aput v9, v2, v8

    const/16 v8, 0x740b

    aput v8, v2, v7

    const/16 v7, 0x6b27

    aput v7, v2, v6

    const/16 v6, -0x2f7

    aput v6, v2, v5

    const/16 v5, -0x6c

    aput v5, v2, v3

    const/16 v3, -0x4abf

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0x3502

    aput v14, v1, v13

    const/16 v13, 0x2235

    aput v13, v1, v12

    const/16 v12, 0x1c22

    aput v12, v1, v11

    const/16 v11, -0x9e4

    aput v11, v1, v10

    const/16 v10, -0xa

    aput v10, v1, v9

    const/16 v9, 0x746e

    aput v9, v1, v8

    const/16 v8, 0x6b74

    aput v8, v1, v7

    const/16 v7, -0x295

    aput v7, v1, v6

    const/4 v6, -0x3

    aput v6, v1, v5

    const/16 v5, -0x4ad3

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x23

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, 0x1a

    const/16 v32, 0x1b

    const/16 v33, 0x1c

    const/16 v34, 0x1d

    const/16 v35, 0x1e

    const/16 v36, 0x1f

    const/16 v37, 0x20

    const/16 v38, 0x21

    const/16 v39, 0x22

    const/16 v40, -0x2f

    aput v40, v2, v39

    const/16 v39, -0x47e4

    aput v39, v2, v38

    const/16 v38, -0x33

    aput v38, v2, v37

    const/16 v37, -0x1a

    aput v37, v2, v36

    const/16 v36, -0x44

    aput v36, v2, v35

    const/16 v35, -0x5d99

    aput v35, v2, v34

    const/16 v34, -0x2f

    aput v34, v2, v33

    const/16 v33, -0x1fc1

    aput v33, v2, v32

    const/16 v32, -0x6c

    aput v32, v2, v31

    const/16 v31, -0x3fb8

    aput v31, v2, v30

    const/16 v30, -0x5b

    aput v30, v2, v29

    const/16 v29, -0x80

    aput v29, v2, v28

    const/16 v28, -0x3b

    aput v28, v2, v27

    const/16 v27, -0x65b0

    aput v27, v2, v26

    const/16 v26, -0x60

    aput v26, v2, v25

    const/16 v25, -0x5ecb

    aput v25, v2, v24

    const/16 v24, -0x3c

    aput v24, v2, v23

    const/16 v23, -0x77

    aput v23, v2, v22

    const/16 v22, -0x12

    aput v22, v2, v21

    const/16 v21, -0x27dc

    aput v21, v2, v20

    const/16 v20, -0x55

    aput v20, v2, v19

    const/16 v19, -0x59

    aput v19, v2, v18

    const/16 v18, -0x6cae

    aput v18, v2, v17

    const/16 v17, -0x19

    aput v17, v2, v16

    const/16 v16, -0x1fc5

    aput v16, v2, v15

    const/16 v15, -0x7b

    aput v15, v2, v14

    const/16 v14, -0x60

    aput v14, v2, v13

    const/16 v13, -0x2d

    aput v13, v2, v12

    const/16 v12, 0x507

    aput v12, v2, v11

    const/16 v11, 0x1c60

    aput v11, v2, v10

    const/16 v10, -0x4d94

    aput v10, v2, v9

    const/16 v9, -0x3e

    aput v9, v2, v8

    const/16 v8, 0x746a

    aput v8, v2, v7

    const/16 v7, 0x4c06

    aput v7, v2, v3

    const/16 v3, 0x731b

    aput v3, v2, v1

    const/16 v1, 0x23

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x11

    const/16 v24, 0x12

    const/16 v25, 0x13

    const/16 v26, 0x14

    const/16 v27, 0x15

    const/16 v28, 0x16

    const/16 v29, 0x17

    const/16 v30, 0x18

    const/16 v31, 0x19

    const/16 v32, 0x1a

    const/16 v33, 0x1b

    const/16 v34, 0x1c

    const/16 v35, 0x1d

    const/16 v36, 0x1e

    const/16 v37, 0x1f

    const/16 v38, 0x20

    const/16 v39, 0x21

    const/16 v40, 0x22

    const/16 v41, -0x14

    aput v41, v1, v40

    const/16 v40, -0x4791

    aput v40, v1, v39

    const/16 v39, -0x48

    aput v39, v1, v38

    const/16 v38, -0x6e

    aput v38, v1, v37

    const/16 v37, -0x23

    aput v37, v1, v36

    const/16 v36, -0x5ded

    aput v36, v1, v35

    const/16 v35, -0x5e

    aput v35, v1, v34

    const/16 v34, -0x1fe1

    aput v34, v1, v33

    const/16 v33, -0x20

    aput v33, v1, v32

    const/16 v32, -0x3fd2

    aput v32, v1, v31

    const/16 v31, -0x40

    aput v31, v1, v30

    const/16 v30, -0x34

    aput v30, v1, v29

    const/16 v29, -0x55

    aput v29, v1, v28

    const/16 v28, -0x65c1

    aput v28, v1, v27

    const/16 v27, -0x66

    aput v27, v1, v26

    const/16 v26, -0x5eb9

    aput v26, v1, v25

    const/16 v25, -0x5f

    aput v25, v1, v24

    const/16 v24, -0x19

    aput v24, v1, v23

    const/16 v23, -0x75

    aput v23, v1, v22

    const/16 v22, -0x27b0

    aput v22, v1, v21

    const/16 v21, -0x28

    aput v21, v1, v20

    const/16 v20, -0x32

    aput v20, v1, v19

    const/16 v19, -0x6ce2

    aput v19, v1, v18

    const/16 v18, -0x6d

    aput v18, v1, v17

    const/16 v17, -0x1fab

    aput v17, v1, v16

    const/16 v16, -0x20

    aput v16, v1, v15

    const/16 v15, -0x2a

    aput v15, v1, v14

    const/16 v14, -0x6a

    aput v14, v1, v13

    const/16 v13, 0x575

    aput v13, v1, v12

    const/16 v12, 0x1c05

    aput v12, v1, v11

    const/16 v11, -0x4de4

    aput v11, v1, v10

    const/16 v10, -0x4e

    aput v10, v1, v9

    const/16 v9, 0x740b

    aput v9, v1, v8

    const/16 v8, 0x4c74

    aput v8, v1, v7

    const/16 v7, 0x734c

    aput v7, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v7, v1

    if-lt v3, v7, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v7, v1

    if-lt v3, v7, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-gtz v1, :cond_8

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x5731

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x5701

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_4
    aget v7, v1, v3

    aget v8, v2, v3

    xor-int/2addr v7, v8

    aput v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_5
    aget v7, v2, v3

    int-to-char v7, v7

    aput-char v7, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_6
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_7
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_8
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v6, v1, v6

    if-eqz v6, :cond_9

    const-wide v6, -0x7d1a108fa7d8d031L

    xor-long/2addr v1, v6

    :cond_9
    const/16 v6, 0x20

    shl-long/2addr v1, v6

    const/16 v6, 0x20

    shr-long/2addr v1, v6

    long-to-int v1, v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->handler:Landroid/os/Handler;

    const/16 v2, 0x6b

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-gtz v1, :cond_c

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x4f

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x7f

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v5, v1

    if-lt v3, v5, :cond_a

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_a
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_b
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_c
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v4, 0x0

    cmp-long v4, v1, v4

    if-eqz v4, :cond_d

    const-wide v4, -0x7d1a108fa7d8d031L

    xor-long/2addr v1, v4

    :cond_d
    const/16 v4, 0x20

    shl-long/2addr v1, v4

    const/16 v4, 0x20

    shr-long/2addr v1, v4

    long-to-int v1, v1

    iput v1, v3, Landroid/os/Message;->arg2:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->unpairedSensorDeviceDetails:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$2200(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    move-result-object v1

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->unpairedSensorDeviceDetails:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$2200(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    move-result-object v1

    iput-object v1, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    :goto_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :catch_0
    move-exception v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iput-object v1, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_8
.end method

.method public onResponseReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;)V
    .locals 54

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->IsAutoScanRequired:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1902(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0x256

    aput v12, v2, v11

    const/16 v11, 0x2661

    aput v11, v2, v10

    const/16 v10, 0x444f

    aput v10, v2, v9

    const/16 v9, -0x23ce

    aput v9, v2, v8

    const/16 v8, -0x52

    aput v8, v2, v7

    const/16 v7, -0x7f

    aput v7, v2, v6

    const/4 v6, -0x1

    aput v6, v2, v5

    const/16 v5, -0x38

    aput v5, v2, v4

    const/16 v4, -0x6f

    aput v4, v2, v3

    const/16 v3, -0x51

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0x233

    aput v13, v1, v12

    const/16 v12, 0x2602

    aput v12, v1, v11

    const/16 v11, 0x4426

    aput v11, v1, v10

    const/16 v10, -0x23bc

    aput v10, v1, v9

    const/16 v9, -0x24

    aput v9, v1, v8

    const/16 v8, -0x1c

    aput v8, v1, v7

    const/16 v7, -0x54

    aput v7, v1, v6

    const/16 v6, -0x56

    aput v6, v1, v5

    const/4 v5, -0x8

    aput v5, v1, v4

    const/16 v4, -0x3d

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x30

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, 0x26

    const/16 v43, 0x27

    const/16 v44, 0x28

    const/16 v45, 0x29

    const/16 v46, 0x2a

    const/16 v47, 0x2b

    const/16 v48, 0x2c

    const/16 v49, 0x2d

    const/16 v50, 0x2e

    const/16 v51, 0x2f

    const/16 v52, -0x5d

    aput v52, v2, v51

    const/16 v51, -0x698e

    aput v51, v2, v50

    const/16 v50, -0x8

    aput v50, v2, v49

    const/16 v49, -0x15

    aput v49, v2, v48

    const/16 v48, -0x3b

    aput v48, v2, v47

    const/16 v47, -0x24

    aput v47, v2, v46

    const/16 v46, 0x8

    aput v46, v2, v45

    const/16 v45, 0x1063

    aput v45, v2, v44

    const/16 v44, 0x1b30

    aput v44, v2, v43

    const/16 v43, -0x7481

    aput v43, v2, v42

    const/16 v42, -0x12

    aput v42, v2, v41

    const/16 v41, 0x7331

    aput v41, v2, v40

    const/16 v40, 0x431a

    aput v40, v2, v39

    const/16 v39, 0x4326

    aput v39, v2, v38

    const/16 v38, -0x3fe0

    aput v38, v2, v37

    const/16 v37, -0x5b

    aput v37, v2, v36

    const/16 v36, -0x28

    aput v36, v2, v35

    const/16 v35, -0x80

    aput v35, v2, v34

    const/16 v34, -0x1e

    aput v34, v2, v33

    const/16 v33, -0x26b7

    aput v33, v2, v32

    const/16 v32, -0x4a

    aput v32, v2, v31

    const/16 v31, -0x1b

    aput v31, v2, v30

    const/16 v30, -0x4c

    aput v30, v2, v29

    const/16 v29, -0x5e

    aput v29, v2, v28

    const/16 v28, -0x63

    aput v28, v2, v27

    const/16 v27, -0x6b

    aput v27, v2, v26

    const/16 v26, -0x35

    aput v26, v2, v25

    const/16 v25, 0xf46

    aput v25, v2, v24

    const/16 v24, -0x6983

    aput v24, v2, v23

    const/16 v23, -0xd

    aput v23, v2, v22

    const/16 v22, -0xbe0

    aput v22, v2, v21

    const/16 v21, -0x6f

    aput v21, v2, v20

    const/16 v20, -0x2c

    aput v20, v2, v19

    const/16 v19, -0x1aff

    aput v19, v2, v18

    const/16 v18, -0x74

    aput v18, v2, v17

    const/16 v17, -0x7f

    aput v17, v2, v16

    const/16 v16, -0x65

    aput v16, v2, v15

    const/16 v15, 0x2415

    aput v15, v2, v14

    const/16 v14, -0x22bf

    aput v14, v2, v13

    const/16 v13, -0x55

    aput v13, v2, v12

    const/16 v12, -0x79

    aput v12, v2, v11

    const/16 v11, -0x60

    aput v11, v2, v10

    const/16 v10, -0x75

    aput v10, v2, v9

    const/16 v9, -0x7b

    aput v9, v2, v8

    const/4 v8, -0x1

    aput v8, v2, v7

    const/16 v7, -0x4cb7

    aput v7, v2, v6

    const/16 v6, -0x3f

    aput v6, v2, v3

    const/16 v3, -0xeb0

    aput v3, v2, v1

    const/16 v1, 0x30

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, 0x1a

    const/16 v32, 0x1b

    const/16 v33, 0x1c

    const/16 v34, 0x1d

    const/16 v35, 0x1e

    const/16 v36, 0x1f

    const/16 v37, 0x20

    const/16 v38, 0x21

    const/16 v39, 0x22

    const/16 v40, 0x23

    const/16 v41, 0x24

    const/16 v42, 0x25

    const/16 v43, 0x26

    const/16 v44, 0x27

    const/16 v45, 0x28

    const/16 v46, 0x29

    const/16 v47, 0x2a

    const/16 v48, 0x2b

    const/16 v49, 0x2c

    const/16 v50, 0x2d

    const/16 v51, 0x2e

    const/16 v52, 0x2f

    const/16 v53, -0x62

    aput v53, v1, v52

    const/16 v52, -0x69ea

    aput v52, v1, v51

    const/16 v51, -0x6a

    aput v51, v1, v50

    const/16 v50, -0x76

    aput v50, v1, v49

    const/16 v49, -0x58

    aput v49, v1, v48

    const/16 v48, -0x4f

    aput v48, v1, v47

    const/16 v47, 0x67

    aput v47, v1, v46

    const/16 v46, 0x1000

    aput v46, v1, v45

    const/16 v45, 0x1b10

    aput v45, v1, v44

    const/16 v44, -0x74e5

    aput v44, v1, v43

    const/16 v43, -0x75

    aput v43, v1, v42

    const/16 v42, 0x7347

    aput v42, v1, v41

    const/16 v41, 0x4373

    aput v41, v1, v40

    const/16 v40, 0x4343

    aput v40, v1, v39

    const/16 v39, -0x3fbd

    aput v39, v1, v38

    const/16 v38, -0x40

    aput v38, v1, v37

    const/16 v37, -0x76

    aput v37, v1, v36

    const/16 v36, -0x1b

    aput v36, v1, v35

    const/16 v35, -0x6f

    aput v35, v1, v34

    const/16 v34, -0x26d9

    aput v34, v1, v33

    const/16 v33, -0x27

    aput v33, v1, v32

    const/16 v32, -0x6b

    aput v32, v1, v31

    const/16 v31, -0x39

    aput v31, v1, v30

    const/16 v30, -0x39

    aput v30, v1, v29

    const/16 v29, -0x31

    aput v29, v1, v28

    const/16 v28, -0x5

    aput v28, v1, v27

    const/16 v27, -0x5c

    aput v27, v1, v26

    const/16 v26, 0xf7c

    aput v26, v1, v25

    const/16 v25, -0x69f1

    aput v25, v1, v24

    const/16 v24, -0x6a

    aput v24, v1, v23

    const/16 v23, -0xbb2

    aput v23, v1, v22

    const/16 v22, -0xc

    aput v22, v1, v21

    const/16 v21, -0x60

    aput v21, v1, v20

    const/16 v20, -0x1a8e

    aput v20, v1, v19

    const/16 v19, -0x1b

    aput v19, v1, v18

    const/16 v18, -0x33

    aput v18, v1, v17

    const/16 v17, -0x11

    aput v17, v1, v16

    const/16 v16, 0x247b

    aput v16, v1, v15

    const/16 v15, -0x22dc

    aput v15, v1, v14

    const/16 v14, -0x23

    aput v14, v1, v13

    const/16 v13, -0x3e

    aput v13, v1, v12

    const/16 v12, -0x2e

    aput v12, v1, v11

    const/16 v11, -0x12

    aput v11, v1, v10

    const/16 v10, -0xb

    aput v10, v1, v9

    const/16 v9, -0x71

    aput v9, v1, v8

    const/16 v8, -0x4cd8

    aput v8, v1, v7

    const/16 v7, -0x4d

    aput v7, v1, v6

    const/16 v6, -0xef9

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v6, v1

    if-lt v3, v6, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v6, v1

    if-lt v3, v6, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x4692

    aput v12, v2, v11

    const/16 v11, -0x26

    aput v11, v2, v10

    const/16 v10, 0x1c74

    aput v10, v2, v9

    const/16 v9, -0x7596

    aput v9, v2, v8

    const/4 v8, -0x8

    aput v8, v2, v7

    const/16 v7, -0x61a1

    aput v7, v2, v6

    const/16 v6, -0x33

    aput v6, v2, v5

    const/16 v5, 0x335d

    aput v5, v2, v4

    const/16 v4, 0x3c5a

    aput v4, v2, v3

    const/16 v3, 0x7450

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x46f5

    aput v13, v1, v12

    const/16 v12, -0x47

    aput v12, v1, v11

    const/16 v11, 0x1c1d

    aput v11, v1, v10

    const/16 v10, -0x75e4

    aput v10, v1, v9

    const/16 v9, -0x76

    aput v9, v1, v8

    const/16 v8, -0x61c6

    aput v8, v1, v7

    const/16 v7, -0x62

    aput v7, v1, v6

    const/16 v6, 0x333f

    aput v6, v1, v5

    const/16 v5, 0x3c33

    aput v5, v1, v4

    const/16 v4, 0x743c

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v4, v1

    if-lt v3, v4, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v4, v1

    if-lt v3, v4, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x29

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, 0x26

    const/16 v43, 0x27

    const/16 v44, 0x28

    const/16 v45, 0x576f

    aput v45, v2, v44

    const/16 v44, 0x477

    aput v44, v2, v43

    const/16 v43, -0x28a0

    aput v43, v2, v42

    const/16 v42, -0x4e

    aput v42, v2, v41

    const/16 v41, -0x2c82

    aput v41, v2, v40

    const/16 v40, -0x46

    aput v40, v2, v39

    const/16 v39, -0x57

    aput v39, v2, v38

    const/16 v38, -0x43

    aput v38, v2, v37

    const/16 v37, 0x3b4e

    aput v37, v2, v36

    const/16 v36, 0x4369

    aput v36, v2, v35

    const/16 v35, -0x6dda

    aput v35, v2, v34

    const/16 v34, -0x1f

    aput v34, v2, v33

    const/16 v33, -0x41

    aput v33, v2, v32

    const/16 v32, 0x2831

    aput v32, v2, v31

    const/16 v31, 0x7b58

    aput v31, v2, v30

    const/16 v30, -0x46f8

    aput v30, v2, v29

    const/16 v29, -0x24

    aput v29, v2, v28

    const/16 v28, -0x30

    aput v28, v2, v27

    const/16 v27, -0x72

    aput v27, v2, v26

    const/16 v26, -0xe

    aput v26, v2, v25

    const/16 v25, -0x44

    aput v25, v2, v24

    const/16 v24, -0x3

    aput v24, v2, v23

    const/16 v23, 0x6369

    aput v23, v2, v22

    const/16 v22, -0x37f3

    aput v22, v2, v21

    const/16 v21, -0x53

    aput v21, v2, v20

    const/16 v20, -0x48

    aput v20, v2, v19

    const/16 v19, -0x58

    aput v19, v2, v18

    const/16 v18, -0xc

    aput v18, v2, v17

    const/16 v17, -0x69

    aput v17, v2, v16

    const/16 v16, -0x68

    aput v16, v2, v15

    const/16 v15, -0xeca

    aput v15, v2, v14

    const/16 v14, -0x6c

    aput v14, v2, v13

    const/16 v13, -0xc

    aput v13, v2, v12

    const/16 v12, -0x2c

    aput v12, v2, v11

    const/16 v11, -0x30

    aput v11, v2, v10

    const/4 v10, -0x7

    aput v10, v2, v9

    const/16 v9, -0x12

    aput v9, v2, v8

    const/16 v8, -0x21d3

    aput v8, v2, v7

    const/16 v7, -0x41

    aput v7, v2, v6

    const/16 v6, -0x449d

    aput v6, v2, v3

    const/16 v3, -0x14

    aput v3, v2, v1

    const/16 v1, 0x29

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, 0x1a

    const/16 v32, 0x1b

    const/16 v33, 0x1c

    const/16 v34, 0x1d

    const/16 v35, 0x1e

    const/16 v36, 0x1f

    const/16 v37, 0x20

    const/16 v38, 0x21

    const/16 v39, 0x22

    const/16 v40, 0x23

    const/16 v41, 0x24

    const/16 v42, 0x25

    const/16 v43, 0x26

    const/16 v44, 0x27

    const/16 v45, 0x28

    const/16 v46, 0x5752

    aput v46, v1, v45

    const/16 v45, 0x457

    aput v45, v1, v44

    const/16 v44, -0x28fc

    aput v44, v1, v43

    const/16 v43, -0x29

    aput v43, v1, v42

    const/16 v42, -0x2cf8

    aput v42, v1, v41

    const/16 v41, -0x2d

    aput v41, v1, v40

    const/16 v40, -0x34

    aput v40, v1, v39

    const/16 v39, -0x22

    aput v39, v1, v38

    const/16 v38, 0x3b2b

    aput v38, v1, v37

    const/16 v37, 0x433b

    aput v37, v1, v36

    const/16 v36, -0x6dbd

    aput v36, v1, v35

    const/16 v35, -0x6e

    aput v35, v1, v34

    const/16 v34, -0x2f

    aput v34, v1, v33

    const/16 v33, 0x285e

    aput v33, v1, v32

    const/16 v32, 0x7b28

    aput v32, v1, v31

    const/16 v31, -0x4685

    aput v31, v1, v30

    const/16 v30, -0x47

    aput v30, v1, v29

    const/16 v29, -0x7e

    aput v29, v1, v28

    const/16 v28, -0x20

    aput v28, v1, v27

    const/16 v27, -0x63

    aput v27, v1, v26

    const/16 v26, -0x7a

    aput v26, v1, v25

    const/16 v25, -0x71

    aput v25, v1, v24

    const/16 v24, 0x630c

    aput v24, v1, v23

    const/16 v23, -0x379d

    aput v23, v1, v22

    const/16 v22, -0x38

    aput v22, v1, v21

    const/16 v21, -0x34

    aput v21, v1, v20

    const/16 v20, -0x25

    aput v20, v1, v19

    const/16 v19, -0x63

    aput v19, v1, v18

    const/16 v18, -0x25

    aput v18, v1, v17

    const/16 v17, -0x14

    aput v17, v1, v16

    const/16 v16, -0xea8

    aput v16, v1, v15

    const/16 v15, -0xf

    aput v15, v1, v14

    const/16 v14, -0x7e

    aput v14, v1, v13

    const/16 v13, -0x6f

    aput v13, v1, v12

    const/16 v12, -0x5e

    aput v12, v1, v11

    const/16 v11, -0x64

    aput v11, v1, v10

    const/16 v10, -0x62

    aput v10, v1, v9

    const/16 v9, -0x21a3

    aput v9, v1, v8

    const/16 v8, -0x22

    aput v8, v1, v7

    const/16 v7, -0x44ef

    aput v7, v1, v6

    const/16 v6, -0x45

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v6, v1

    if-lt v3, v6, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v6, v1

    if-lt v3, v6, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;->getErrorCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->handler:Landroid/os/Handler;

    const/16 v2, 0x6b

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;->getErrorCode()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->arg2:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_0
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_3
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_4
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_5
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_6
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_7
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7
.end method

.method public onStateChanged(I)V
    .locals 50

    const/4 v1, 0x2

    new-array v4, v1, [J

    const/4 v1, 0x1

    const-wide/16 v2, 0x1

    aput-wide v2, v4, v1

    const/4 v1, 0x0

    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v4, v2

    long-to-int v2, v2

    if-gtz v2, :cond_0

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v3, 0x0

    move/from16 v0, p1

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    ushr-long v5, v1, v5

    aget-wide v1, v4, v3

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_1

    const-wide v7, 0x57ea6a23ac0835ffL    # 3.2524640664473247E115

    xor-long/2addr v1, v7

    :cond_1
    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    const/16 v7, 0x20

    shl-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, 0x57ea6a23ac0835ffL    # 3.2524640664473247E115

    xor-long/2addr v1, v5

    aput-wide v1, v4, v3

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x74

    aput v13, v2, v12

    const/16 v12, -0x6a

    aput v12, v2, v11

    const/16 v11, -0x75

    aput v11, v2, v10

    const/16 v10, 0x157a

    aput v10, v2, v9

    const/16 v9, -0x2299

    aput v9, v2, v8

    const/16 v8, -0x48

    aput v8, v2, v7

    const/16 v7, -0x5ef6

    aput v7, v2, v6

    const/16 v6, -0x3d

    aput v6, v2, v5

    const/16 v5, 0x2d4f

    aput v5, v2, v3

    const/16 v3, 0x4841

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, -0x17

    aput v14, v1, v13

    const/16 v13, -0xb

    aput v13, v1, v12

    const/16 v12, -0x1e

    aput v12, v1, v11

    const/16 v11, 0x150c

    aput v11, v1, v10

    const/16 v10, -0x22eb

    aput v10, v1, v9

    const/16 v9, -0x23

    aput v9, v1, v8

    const/16 v8, -0x5ea7

    aput v8, v1, v7

    const/16 v7, -0x5f

    aput v7, v1, v6

    const/16 v6, 0x2d26

    aput v6, v1, v5

    const/16 v5, 0x482d

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x2b

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, 0x1a

    const/16 v32, 0x1b

    const/16 v33, 0x1c

    const/16 v34, 0x1d

    const/16 v35, 0x1e

    const/16 v36, 0x1f

    const/16 v37, 0x20

    const/16 v38, 0x21

    const/16 v39, 0x22

    const/16 v40, 0x23

    const/16 v41, 0x24

    const/16 v42, 0x25

    const/16 v43, 0x26

    const/16 v44, 0x27

    const/16 v45, 0x28

    const/16 v46, 0x29

    const/16 v47, 0x2a

    const/16 v48, 0x4736

    aput v48, v2, v47

    const/16 v47, -0x4acc

    aput v47, v2, v46

    const/16 v46, -0x40

    aput v46, v2, v45

    const/16 v45, 0xf0d

    aput v45, v2, v44

    const/16 v44, 0x756e

    aput v44, v2, v43

    const/16 v43, 0xf01

    aput v43, v2, v42

    const/16 v42, -0x1c84

    aput v42, v2, v41

    const/16 v41, -0x3d

    aput v41, v2, v40

    const/16 v40, 0x162e

    aput v40, v2, v39

    const/16 v39, -0x448d

    aput v39, v2, v38

    const/16 v38, -0x24

    aput v38, v2, v37

    const/16 v37, -0x25bd

    aput v37, v2, v36

    const/16 v36, -0x45

    aput v36, v2, v35

    const/16 v35, -0x1f

    aput v35, v2, v34

    const/16 v34, -0x39f

    aput v34, v2, v33

    const/16 v33, -0x67

    aput v33, v2, v32

    const/16 v32, -0x13a7

    aput v32, v2, v31

    const/16 v31, -0x73

    aput v31, v2, v30

    const/16 v30, -0x5f

    aput v30, v2, v29

    const/16 v29, -0x44fc

    aput v29, v2, v28

    const/16 v28, -0x2b

    aput v28, v2, v27

    const/16 v27, -0x6e

    aput v27, v2, v26

    const/16 v26, -0x16

    aput v26, v2, v25

    const/16 v25, -0x7d

    aput v25, v2, v24

    const/16 v24, 0x5363

    aput v24, v2, v23

    const/16 v23, -0x52c3

    aput v23, v2, v22

    const/16 v22, -0x38

    aput v22, v2, v21

    const/16 v21, -0x19

    aput v21, v2, v20

    const/16 v20, 0x3b47

    aput v20, v2, v19

    const/16 v19, 0x3e52

    aput v19, v2, v18

    const/16 v18, -0x3a8e

    aput v18, v2, v17

    const/16 v17, -0x4f

    aput v17, v2, v16

    const/16 v16, 0x5c22

    aput v16, v2, v15

    const/16 v15, 0x1a39

    aput v15, v2, v14

    const/16 v14, 0x96c

    aput v14, v2, v13

    const/16 v13, -0x48b4

    aput v13, v2, v12

    const/16 v12, -0x3b

    aput v12, v2, v11

    const/16 v11, -0x5da7

    aput v11, v2, v10

    const/16 v10, -0x2e

    aput v10, v2, v9

    const/16 v9, -0x31

    aput v9, v2, v8

    const/16 v8, 0x92f

    aput v8, v2, v7

    const/16 v7, -0x785

    aput v7, v2, v3

    const/16 v3, -0x51

    aput v3, v2, v1

    const/16 v1, 0x2b

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x11

    const/16 v24, 0x12

    const/16 v25, 0x13

    const/16 v26, 0x14

    const/16 v27, 0x15

    const/16 v28, 0x16

    const/16 v29, 0x17

    const/16 v30, 0x18

    const/16 v31, 0x19

    const/16 v32, 0x1a

    const/16 v33, 0x1b

    const/16 v34, 0x1c

    const/16 v35, 0x1d

    const/16 v36, 0x1e

    const/16 v37, 0x1f

    const/16 v38, 0x20

    const/16 v39, 0x21

    const/16 v40, 0x22

    const/16 v41, 0x23

    const/16 v42, 0x24

    const/16 v43, 0x25

    const/16 v44, 0x26

    const/16 v45, 0x27

    const/16 v46, 0x28

    const/16 v47, 0x29

    const/16 v48, 0x2a

    const/16 v49, 0x470b

    aput v49, v1, v48

    const/16 v48, -0x4ab9

    aput v48, v1, v47

    const/16 v47, -0x4b

    aput v47, v1, v46

    const/16 v46, 0xf79

    aput v46, v1, v45

    const/16 v45, 0x750f

    aput v45, v1, v44

    const/16 v44, 0xf75

    aput v44, v1, v43

    const/16 v43, -0x1cf1

    aput v43, v1, v42

    const/16 v42, -0x1d

    aput v42, v1, v41

    const/16 v41, 0x164a

    aput v41, v1, v40

    const/16 v40, -0x44ea

    aput v40, v1, v39

    const/16 v39, -0x45

    aput v39, v1, v38

    const/16 v38, -0x25d3

    aput v38, v1, v37

    const/16 v37, -0x26

    aput v37, v1, v36

    const/16 v36, -0x77

    aput v36, v1, v35

    const/16 v35, -0x3de

    aput v35, v1, v34

    const/16 v34, -0x4

    aput v34, v1, v33

    const/16 v33, -0x13d3

    aput v33, v1, v32

    const/16 v32, -0x14

    aput v32, v1, v31

    const/16 v31, -0x2b

    aput v31, v1, v30

    const/16 v30, -0x44a9

    aput v30, v1, v29

    const/16 v29, -0x45

    aput v29, v1, v28

    const/16 v28, -0x3

    aput v28, v1, v27

    const/16 v27, -0x30

    aput v27, v1, v26

    const/16 v26, -0xf

    aput v26, v1, v25

    const/16 v25, 0x5306

    aput v25, v1, v24

    const/16 v24, -0x52ad

    aput v24, v1, v23

    const/16 v23, -0x53

    aput v23, v1, v22

    const/16 v22, -0x6d

    aput v22, v1, v21

    const/16 v21, 0x3b34

    aput v21, v1, v20

    const/16 v20, 0x3e3b

    aput v20, v1, v19

    const/16 v19, -0x3ac2

    aput v19, v1, v18

    const/16 v18, -0x3b

    aput v18, v1, v17

    const/16 v17, 0x5c4c

    aput v17, v1, v16

    const/16 v16, 0x1a5c

    aput v16, v1, v15

    const/16 v15, 0x91a

    aput v15, v1, v14

    const/16 v14, -0x48f7

    aput v14, v1, v13

    const/16 v13, -0x49

    aput v13, v1, v12

    const/16 v12, -0x5dc4

    aput v12, v1, v11

    const/16 v11, -0x5e

    aput v11, v1, v10

    const/16 v10, -0x41

    aput v10, v1, v9

    const/16 v9, 0x94e

    aput v9, v1, v8

    const/16 v8, -0x7f7

    aput v8, v1, v7

    const/4 v7, -0x8

    aput v7, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v7, v1

    if-lt v3, v7, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v7, v1

    if-lt v3, v7, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-gtz v1, :cond_8

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x136e

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x135e

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_4
    aget v7, v1, v3

    aget v8, v2, v3

    xor-int/2addr v7, v8

    aput v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_5
    aget v7, v2, v3

    int-to-char v7, v7

    aput-char v7, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_6
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_7
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_8
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v4, v1, v6

    if-eqz v4, :cond_9

    const-wide v6, 0x57ea6a23ac0835ffL    # 3.2524640664473247E115

    xor-long/2addr v1, v6

    :cond_9
    const/16 v4, 0x20

    shl-long/2addr v1, v4

    const/16 v4, 0x20

    shr-long/2addr v1, v4

    long-to-int v1, v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
