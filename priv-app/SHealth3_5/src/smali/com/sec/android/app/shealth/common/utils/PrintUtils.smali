.class public Lcom/sec/android/app/shealth/common/utils/PrintUtils;
.super Ljava/lang/Object;
.source "PrintUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/sec/android/app/shealth/common/utils/PrintUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/PrintUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 293
    return-void
.end method

.method private static createPrintFileUri(Landroid/graphics/Bitmap;Ljava/io/File;Landroid/content/Context;)Landroid/net/Uri;
    .locals 17
    .param p0, "printBitmap"    # Landroid/graphics/Bitmap;
    .param p1, "fileSource"    # Ljava/io/File;
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;
        }
    .end annotation

    .prologue
    .line 148
    if-eqz p2, :cond_0

    if-nez p0, :cond_1

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_1

    .line 151
    :cond_0
    new-instance v14, Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;

    const-string v15, "Invalid parameter"

    const/16 v16, -0x1

    invoke-direct/range {v14 .. v16}, Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;-><init>(Ljava/lang/String;I)V

    throw v14

    .line 156
    :cond_1
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v15

    invoke-virtual {v15}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    sget-object v15, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, "print"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 158
    .local v11, "printDirPath":Ljava/lang/String;
    const-string/jumbo v12, "printImage.jpg"

    .line 161
    .local v12, "printFileName":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 162
    .local v3, "dirToPrint":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->mkdir()Z

    .line 164
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_2

    .line 167
    new-instance v14, Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;

    const-string v15, "Failed to create print directory"

    const/16 v16, -0x2

    invoke-direct/range {v14 .. v16}, Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;-><init>(Ljava/lang/String;I)V

    throw v14

    .line 178
    :cond_2
    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 179
    .local v2, "childFile":[Ljava/io/File;
    if-eqz v2, :cond_3

    .line 181
    array-length v14, v2

    if-lez v14, :cond_3

    .line 183
    array-length v13, v2

    .line 184
    .local v13, "size":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    if-ge v9, v13, :cond_3

    .line 186
    aget-object v14, v2, v9

    invoke-virtual {v14}, Ljava/io/File;->delete()Z

    .line 184
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 192
    .end local v9    # "i":I
    .end local v13    # "size":I
    :cond_3
    new-instance v5, Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v5, v14, v12}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    .local v5, "fileToPrint":Ljava/io/File;
    const/4 v7, 0x0

    .line 197
    .local v7, "fout":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v8, Ljava/io/FileOutputStream;

    invoke-direct {v8, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5

    .line 199
    .end local v7    # "fout":Ljava/io/FileOutputStream;
    .local v8, "fout":Ljava/io/FileOutputStream;
    if-eqz p0, :cond_4

    .line 202
    :try_start_1
    sget-object v14, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v15, 0x64

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15, v8}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 203
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->recycle()V

    .line 204
    const/16 p0, 0x0

    .line 229
    :goto_1
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 255
    :try_start_2
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    .line 266
    :goto_2
    invoke-static {v5}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v14

    return-object v14

    .line 209
    :cond_4
    :try_start_3
    new-instance v6, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v6, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 211
    .local v6, "fin":Ljava/io/FileInputStream;
    const/16 v14, 0x2000

    new-array v1, v14, [B
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 215
    .local v1, "bBuffer":[B
    :goto_3
    :try_start_4
    invoke-virtual {v6, v1}, Ljava/io/FileInputStream;->read([B)I

    move-result v10

    .local v10, "nRead":I
    const/4 v14, -0x1

    if-eq v10, v14, :cond_5

    .line 217
    const/4 v14, 0x0

    invoke-virtual {v8, v1, v14, v10}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3

    .line 220
    .end local v10    # "nRead":I
    :catch_0
    move-exception v4

    .line 222
    .local v4, "e":Ljava/io/IOException;
    :try_start_5
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 226
    :try_start_6
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_1

    .line 231
    .end local v1    # "bBuffer":[B
    .end local v4    # "e":Ljava/io/IOException;
    .end local v6    # "fin":Ljava/io/FileInputStream;
    :catch_1
    move-exception v4

    move-object v7, v8

    .line 234
    .end local v8    # "fout":Ljava/io/FileOutputStream;
    .local v4, "e":Ljava/io/FileNotFoundException;
    .restart local v7    # "fout":Ljava/io/FileOutputStream;
    :goto_4
    new-instance v14, Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;

    const-string v15, "File Not found Exception"

    const/16 v16, -0x3

    invoke-direct/range {v14 .. v16}, Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;-><init>(Ljava/lang/String;I)V

    throw v14

    .line 226
    .end local v4    # "e":Ljava/io/FileNotFoundException;
    .end local v7    # "fout":Ljava/io/FileOutputStream;
    .restart local v1    # "bBuffer":[B
    .restart local v6    # "fin":Ljava/io/FileInputStream;
    .restart local v8    # "fout":Ljava/io/FileOutputStream;
    .restart local v10    # "nRead":I
    :cond_5
    :try_start_7
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_1

    .line 237
    .end local v1    # "bBuffer":[B
    .end local v6    # "fin":Ljava/io/FileInputStream;
    .end local v10    # "nRead":I
    :catch_2
    move-exception v4

    move-object v7, v8

    .line 239
    .end local v8    # "fout":Ljava/io/FileOutputStream;
    .local v4, "e":Ljava/lang/NullPointerException;
    .restart local v7    # "fout":Ljava/io/FileOutputStream;
    :goto_5
    if-eqz v5, :cond_6

    .line 240
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 242
    :cond_6
    new-instance v14, Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;

    const-string v15, "Null pointer exception"

    const/16 v16, -0x4

    invoke-direct/range {v14 .. v16}, Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;-><init>(Ljava/lang/String;I)V

    throw v14

    .line 226
    .end local v4    # "e":Ljava/lang/NullPointerException;
    .end local v7    # "fout":Ljava/io/FileOutputStream;
    .restart local v1    # "bBuffer":[B
    .restart local v6    # "fin":Ljava/io/FileInputStream;
    .restart local v8    # "fout":Ljava/io/FileOutputStream;
    :catchall_0
    move-exception v14

    :try_start_8
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    throw v14
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    .line 244
    .end local v1    # "bBuffer":[B
    .end local v6    # "fin":Ljava/io/FileInputStream;
    :catch_3
    move-exception v4

    move-object v7, v8

    .line 246
    .end local v8    # "fout":Ljava/io/FileOutputStream;
    .local v4, "e":Ljava/io/IOException;
    .restart local v7    # "fout":Ljava/io/FileOutputStream;
    :goto_6
    if-eqz v5, :cond_7

    .line 247
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 250
    :cond_7
    new-instance v14, Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;

    const-string v15, "IO exception"

    const/16 v16, -0x4

    invoke-direct/range {v14 .. v16}, Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;-><init>(Ljava/lang/String;I)V

    throw v14

    .line 257
    .end local v4    # "e":Ljava/io/IOException;
    .end local v7    # "fout":Ljava/io/FileOutputStream;
    .restart local v8    # "fout":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v4

    .line 259
    .restart local v4    # "e":Ljava/io/IOException;
    sget-object v14, Lcom/sec/android/app/shealth/common/utils/PrintUtils;->TAG:Ljava/lang/String;

    const-string v15, "IO exception"

    invoke-static {v14, v15}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 244
    .end local v4    # "e":Ljava/io/IOException;
    .end local v8    # "fout":Ljava/io/FileOutputStream;
    .restart local v7    # "fout":Ljava/io/FileOutputStream;
    :catch_5
    move-exception v4

    goto :goto_6

    .line 237
    :catch_6
    move-exception v4

    goto :goto_5

    .line 231
    :catch_7
    move-exception v4

    goto :goto_4
.end method

.method public static print(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;)V
    .locals 5
    .param p0, "view"    # Landroid/view/View;
    .param p1, "subject"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    .line 70
    if-eqz p0, :cond_0

    if-nez p2, :cond_1

    .line 73
    :cond_0
    new-instance v2, Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;

    const-string v3, "Invalid parameter"

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;-><init>(Ljava/lang/String;I)V

    throw v2

    .line 76
    :cond_1
    const/4 v0, 0x0

    .line 78
    .local v0, "printBitmap":Landroid/graphics/Bitmap;
    const/4 v2, 0x0

    invoke-static {p0, v2}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->getScreenshot(Landroid/view/View;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 79
    if-eqz v0, :cond_2

    .line 82
    const/4 v2, 0x0

    invoke-static {v0, v2, p2}, Lcom/sec/android/app/shealth/common/utils/PrintUtils;->createPrintFileUri(Landroid/graphics/Bitmap;Ljava/io/File;Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    .line 85
    .local v1, "printFileURI":Landroid/net/Uri;
    invoke-static {v1, p1, p2}, Lcom/sec/android/app/shealth/common/utils/PrintUtils;->sendIntentToPrint(Landroid/net/Uri;Ljava/lang/String;Landroid/content/Context;)V

    .line 91
    return-void

    .line 89
    .end local v1    # "printFileURI":Landroid/net/Uri;
    :cond_2
    new-instance v2, Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;

    const-string v3, "Invalid parameter"

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;-><init>(Ljava/lang/String;I)V

    throw v2
.end method

.method public static printSnapShot(Ljava/lang/String;Landroid/app/Activity;)V
    .locals 3
    .param p0, "subject"    # Ljava/lang/String;
    .param p1, "activity"    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;
        }
    .end annotation

    .prologue
    .line 46
    if-nez p1, :cond_0

    .line 49
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;

    const-string v1, "Invalid parameter"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 52
    :cond_0
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lcom/sec/android/app/shealth/common/utils/PrintUtils;->print(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;)V

    .line 53
    return-void
.end method

.method public static printSnapShotForGraph(Ljava/lang/String;Landroid/app/Activity;Landroid/widget/LinearLayout;)V
    .locals 6
    .param p0, "subject"    # Ljava/lang/String;
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "lyt"    # Landroid/widget/LinearLayout;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;
        }
    .end annotation

    .prologue
    const/4 v5, -0x1

    .line 110
    if-nez p1, :cond_0

    .line 113
    new-instance v3, Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;

    const-string v4, "Invalid parameter"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;-><init>(Ljava/lang/String;I)V

    throw v3

    .line 115
    :cond_0
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v2

    .line 116
    .local v2, "view":Landroid/view/View;
    if-eqz v2, :cond_1

    if-nez p1, :cond_2

    .line 119
    :cond_1
    new-instance v3, Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;

    const-string v4, "Invalid parameter"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;-><init>(Ljava/lang/String;I)V

    throw v3

    .line 121
    :cond_2
    const/4 v0, 0x0

    .line 123
    .local v0, "printBitmap":Landroid/graphics/Bitmap;
    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->getScreenshot(Landroid/view/View;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 124
    if-eqz v0, :cond_3

    .line 126
    invoke-static {v0, p2}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->prepareGraphScreenshot(Landroid/graphics/Bitmap;Landroid/widget/LinearLayout;)V

    .line 128
    const/4 v3, 0x0

    invoke-static {v0, v3, p1}, Lcom/sec/android/app/shealth/common/utils/PrintUtils;->createPrintFileUri(Landroid/graphics/Bitmap;Ljava/io/File;Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    .line 131
    .local v1, "printFileURI":Landroid/net/Uri;
    const/4 v0, 0x0

    .line 134
    invoke-static {v1, p0, p1}, Lcom/sec/android/app/shealth/common/utils/PrintUtils;->sendIntentToPrint(Landroid/net/Uri;Ljava/lang/String;Landroid/content/Context;)V

    .line 140
    return-void

    .line 138
    .end local v1    # "printFileURI":Landroid/net/Uri;
    :cond_3
    new-instance v3, Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;

    const-string v4, "Invalid parameter"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;-><init>(Ljava/lang/String;I)V

    throw v3
.end method

.method private static sendIntentToPrint(Landroid/net/Uri;Ljava/lang/String;Landroid/content/Context;)V
    .locals 7
    .param p0, "printFileURI"    # Landroid/net/Uri;
    .param p1, "subject"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;
        }
    .end annotation

    .prologue
    .line 273
    const-string v1, "com.sec.android.app.mobileprint.PRINT"

    .line 274
    .local v1, "intentPrint":Ljava/lang/String;
    const-string v3, "SHealth"

    .line 275
    .local v3, "title":Ljava/lang/String;
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.sec.android.app.mobileprint.PRINT"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 276
    .local v2, "intentPrintImage":Landroid/content/Intent;
    const-string v4, "android.intent.extra.STREAM"

    invoke-virtual {v2, v4, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 277
    const-string v4, "android.intent.extra.TITLE"

    const-string v5, "SHealth"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 278
    const-string v4, "android.intent.extra.SUBJECT"

    invoke-virtual {v2, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 282
    :try_start_0
    invoke-virtual {p2, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 291
    return-void

    .line 284
    :catch_0
    move-exception v0

    .line 288
    .local v0, "ex":Landroid/content/ActivityNotFoundException;
    new-instance v4, Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;

    const-string v5, "There is no Print Activity"

    const/4 v6, -0x5

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;-><init>(Ljava/lang/String;I)V

    throw v4
.end method
