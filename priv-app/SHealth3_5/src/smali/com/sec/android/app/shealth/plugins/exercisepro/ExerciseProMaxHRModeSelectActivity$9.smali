.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$9;
.super Ljava/lang/Object;
.source "ExerciseProMaxHRModeSelectActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->showMaxHrWarningPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)V
    .locals 0

    .prologue
    .line 367
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/app/Activity;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;

    .prologue
    .line 371
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 372
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)Landroid/widget/EditText;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/EditText;->setSelection(II)V

    .line 373
    return-void
.end method
