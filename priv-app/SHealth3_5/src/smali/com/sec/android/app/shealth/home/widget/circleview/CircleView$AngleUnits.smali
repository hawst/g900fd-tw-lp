.class public final enum Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;
.super Ljava/lang/Enum;
.source "CircleView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AngleUnits"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;

.field public static final enum DEGREES:Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;

.field public static final enum RADIANS:Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 117
    new-instance v0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;

    const-string v1, "DEGREES"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;->DEGREES:Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;

    new-instance v0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;

    const-string v1, "RADIANS"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;->RADIANS:Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;

    .line 116
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;

    sget-object v1, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;->DEGREES:Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;->RADIANS:Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;->$VALUES:[Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 116
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 116
    const-class v0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;
    .locals 1

    .prologue
    .line 116
    sget-object v0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;->$VALUES:[Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;

    return-object v0
.end method


# virtual methods
.method getAngle(F)Lcom/sec/android/app/shealth/home/widget/circleview/Angle;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 120
    sget-object v0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$1;->$SwitchMap$com$sec$android$app$shealth$home$widget$circleview$CircleView$AngleUnits:[I

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 126
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 122
    :pswitch_0
    invoke-static {p1}, Lcom/sec/android/app/shealth/home/widget/circleview/Angle;->fromDegrees(F)Lcom/sec/android/app/shealth/home/widget/circleview/Angle;

    move-result-object v0

    .line 124
    :goto_0
    return-object v0

    :pswitch_1
    invoke-static {p1}, Lcom/sec/android/app/shealth/home/widget/circleview/Angle;->fromRadians(F)Lcom/sec/android/app/shealth/home/widget/circleview/Angle;

    move-result-object v0

    goto :goto_0

    .line 120
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
