.class public Lcom/sec/android/app/shealth/food/fooddao/MealImageDaoImplDb;
.super Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;
.source "MealImageDaoImplDb.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb",
        "<",
        "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;",
        ">;",
        "Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$MealImage;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "create_time"

    const-string v4, "_id"

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;-><init>(Landroid/net/Uri;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    return-void
.end method


# virtual methods
.method public bridge synthetic getDataContentValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)Landroid/content/ContentValues;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    .prologue
    .line 33
    check-cast p1, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;

    .end local p1    # "x0":Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/fooddao/MealImageDaoImplDb;->getDataContentValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;)Landroid/content/ContentValues;

    move-result-object v0

    return-object v0
.end method

.method public getDataContentValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;)Landroid/content/ContentValues;
    .locals 4
    .param p1, "item"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;

    .prologue
    .line 43
    if-nez p1, :cond_0

    .line 44
    const/4 v1, 0x0

    .line 50
    :goto_0
    return-object v1

    .line 46
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)V

    .line 47
    .local v0, "foodInfo":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;
    const-string v1, "file_path"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->getFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    const-string/jumbo v1, "update_time"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->getUpdateTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 49
    const-string/jumbo v1, "meal__id"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->getMealId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 50
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->getContentValues()Landroid/content/ContentValues;

    move-result-object v1

    goto :goto_0
.end method

.method public getImagesListByMealId(J)Ljava/util/List;
    .locals 4
    .param p1, "mealId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string/jumbo v1, "meal__id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const-string v3, "="

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    .local v0, "filter":Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/fooddao/MealImageDaoImplDb;->getAllDatas(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method protected bridge synthetic getItemFromRow(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    .locals 1
    .param p1, "x0"    # Landroid/database/Cursor;

    .prologue
    .line 33
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/fooddao/MealImageDaoImplDb;->getItemFromRow(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;

    move-result-object v0

    return-object v0
.end method

.method protected getItemFromRow(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;
    .locals 12
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 55
    new-instance v11, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;

    invoke-direct {v11, p1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;-><init>(Landroid/database/Cursor;)V

    .line 56
    .local v11, "getter":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;

    const-string/jumbo v1, "meal__id"

    invoke-virtual {v11, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    const-string v3, "file_path"

    invoke-virtual {v11, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "_id"

    invoke-virtual {v11, v4}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    const-string v6, "create_time"

    invoke-virtual {v11, v6}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    const-string/jumbo v8, "update_time"

    invoke-virtual {v11, v8}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    const-string/jumbo v10, "time_zone"

    invoke-virtual {v11, v10}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getInt(Ljava/lang/String;)I

    move-result v10

    invoke-direct/range {v0 .. v10}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;-><init>(JLjava/lang/String;JJJI)V

    .line 64
    .local v0, "mealImage":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;
    return-object v0
.end method
