.class public Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;
.super Ljava/lang/Object;
.source "ThermoHygrometerData.java"


# instance fields
.field private applicationId:Ljava/lang/String;

.field private averageHumidity:F

.field private averageTemperature:F

.field private comment:Ljava/lang/String;

.field private createTime:J

.field private humidity:F

.field private id:J

.field private max:I

.field private min:I

.field private sampleTime:J

.field private state:I

.field private temperature:F

.field private updateTime:J

.field private userDeviceId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getApplicationId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->applicationId:Ljava/lang/String;

    return-object v0
.end method

.method public getAverageHumidity()F
    .locals 1

    .prologue
    .line 124
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->averageHumidity:F

    return v0
.end method

.method public getAverageTemperature()F
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->averageTemperature:F

    return v0
.end method

.method public getComment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->comment:Ljava/lang/String;

    return-object v0
.end method

.method public getCreateTime()J
    .locals 2

    .prologue
    .line 76
    iget-wide v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->createTime:J

    return-wide v0
.end method

.method public getHumidity()F
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->humidity:F

    return v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 20
    iget-wide v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->id:J

    return-wide v0
.end method

.method public getMax()I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->max:I

    return v0
.end method

.method public getMin()I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->min:I

    return v0
.end method

.method public getSampleTime()J
    .locals 2

    .prologue
    .line 68
    iget-wide v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->sampleTime:J

    return-wide v0
.end method

.method public getState()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->state:I

    return v0
.end method

.method public getTemperature()F
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->temperature:F

    return v0
.end method

.method public getUpdateTime()J
    .locals 2

    .prologue
    .line 84
    iget-wide v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->updateTime:J

    return-wide v0
.end method

.method public getUserDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->userDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method public setApplicationId(Ljava/lang/String;)V
    .locals 0
    .param p1, "applicationId"    # Ljava/lang/String;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->applicationId:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public setAverageHumidity(F)V
    .locals 0
    .param p1, "averageHumidity"    # F

    .prologue
    .line 128
    iput p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->averageHumidity:F

    .line 129
    return-void
.end method

.method public setAverageTemperature(F)V
    .locals 0
    .param p1, "averageTemperature"    # F

    .prologue
    .line 120
    iput p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->averageTemperature:F

    .line 121
    return-void
.end method

.method public setComment(Ljava/lang/String;)V
    .locals 0
    .param p1, "comment"    # Ljava/lang/String;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->comment:Ljava/lang/String;

    .line 97
    return-void
.end method

.method public setCreateTime(J)V
    .locals 0
    .param p1, "createTime"    # J

    .prologue
    .line 80
    iput-wide p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->createTime:J

    .line 81
    return-void
.end method

.method public setHumidity(F)V
    .locals 0
    .param p1, "humidity"    # F

    .prologue
    .line 56
    iput p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->humidity:F

    .line 57
    return-void
.end method

.method public setId(J)V
    .locals 0
    .param p1, "id"    # J

    .prologue
    .line 24
    iput-wide p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->id:J

    .line 25
    return-void
.end method

.method public setMax(I)V
    .locals 0
    .param p1, "max"    # I

    .prologue
    .line 112
    iput p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->max:I

    .line 113
    return-void
.end method

.method public setMin(I)V
    .locals 0
    .param p1, "min"    # I

    .prologue
    .line 104
    iput p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->min:I

    .line 105
    return-void
.end method

.method public setSampleTime(J)V
    .locals 0
    .param p1, "sampleTime"    # J

    .prologue
    .line 72
    iput-wide p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->sampleTime:J

    .line 73
    return-void
.end method

.method public setState(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 48
    iput p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->state:I

    .line 49
    return-void
.end method

.method public setTemperature(F)V
    .locals 0
    .param p1, "temperature"    # F

    .prologue
    .line 64
    iput p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->temperature:F

    .line 65
    return-void
.end method

.method public setUpdateTime(J)V
    .locals 0
    .param p1, "updateTime"    # J

    .prologue
    .line 88
    iput-wide p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->updateTime:J

    .line 89
    return-void
.end method

.method public setUserDeviceId(Ljava/lang/String;)V
    .locals 0
    .param p1, "userDeviceId"    # Ljava/lang/String;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->userDeviceId:Ljava/lang/String;

    .line 41
    return-void
.end method
