.class Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$21;
.super Ljava/lang/Object;
.source "HeartrateSummaryFragmentNew.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->animate(Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

.field final synthetic val$imageView1:Landroid/widget/ImageView;

.field final synthetic val$imageView2:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Landroid/widget/ImageView;Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 2045
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$21;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iput-object p2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$21;->val$imageView2:Landroid/widget/ImageView;

    iput-object p3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$21;->val$imageView1:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v1, 0x0

    .line 2047
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$21;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->animationFadeIn:Landroid/view/animation/AnimationSet;

    if-ne p1, v0, :cond_1

    .line 2048
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$21;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isImage1Found:Z

    if-eqz v0, :cond_0

    .line 2049
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$21;->val$imageView2:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2051
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$21;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isImage2Found:Z

    if-eqz v0, :cond_1

    .line 2052
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$21;->val$imageView1:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2055
    :cond_1
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 2058
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 2061
    return-void
.end method
