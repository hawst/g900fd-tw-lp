.class Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC$1;
.super Ljava/lang/Object;
.source "WalkingMateGraphFragmentSIC.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getMonthSchartTimeData()Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;)V
    .locals 0

    .prologue
    .line 430
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;)I
    .locals 4
    .param p1, "lhs"    # Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    .param p2, "rhs"    # Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    .prologue
    .line 433
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getStartTime()J

    move-result-wide v0

    invoke-virtual {p2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getStartTime()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 434
    const/4 v0, -0x1

    .line 438
    :goto_0
    return v0

    .line 435
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getStartTime()J

    move-result-wide v0

    invoke-virtual {p2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getStartTime()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 436
    const/4 v0, 0x1

    goto :goto_0

    .line 438
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 430
    check-cast p1, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC$1;->compare(Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;)I

    move-result v0

    return v0
.end method
