.class public Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
.super Ljava/lang/Object;
.source "WalkingMateDataUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ChartData"
.end annotation


# instance fields
.field private mAvgTotalStep:D

.field private mHealthyStep:D

.field private mIsGoalAchived:Z

.field private mIsMaxSteps:Z

.field private mRunStep:D

.field private mStartTime:J

.field private mUpStep:D

.field private mWalkStep:D


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 526
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mStartTime:J

    .line 527
    iput-wide v2, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mWalkStep:D

    .line 528
    iput-wide v2, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mRunStep:D

    .line 529
    iput-wide v2, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mUpStep:D

    .line 530
    iput-wide v2, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mHealthyStep:D

    .line 531
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mIsGoalAchived:Z

    .line 532
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mIsMaxSteps:Z

    .line 533
    iput-wide v2, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mAvgTotalStep:D

    .line 534
    return-void
.end method

.method public constructor <init>(JDDD)V
    .locals 3
    .param p1, "startTime"    # J
    .param p3, "walkStep"    # D
    .param p5, "runStep"    # D
    .param p7, "upStep"    # D

    .prologue
    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    .line 536
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 537
    iput-wide p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mStartTime:J

    .line 538
    iput-wide p3, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mWalkStep:D

    .line 539
    iput-wide p5, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mRunStep:D

    .line 540
    iput-wide p7, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mUpStep:D

    .line 541
    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mHealthyStep:D

    .line 542
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mIsGoalAchived:Z

    .line 543
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mIsMaxSteps:Z

    .line 544
    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mAvgTotalStep:D

    .line 545
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    .prologue
    .line 498
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mStartTime:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;)D
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    .prologue
    .line 498
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mHealthyStep:D

    return-wide v0
.end method

.method static synthetic access$118(Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;D)D
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    .param p1, "x1"    # D

    .prologue
    .line 498
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mHealthyStep:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mHealthyStep:D

    return-wide v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;)D
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    .prologue
    .line 498
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mRunStep:D

    return-wide v0
.end method

.method static synthetic access$218(Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;D)D
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    .param p1, "x1"    # D

    .prologue
    .line 498
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mRunStep:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mRunStep:D

    return-wide v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;)D
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    .prologue
    .line 498
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mUpStep:D

    return-wide v0
.end method

.method static synthetic access$318(Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;D)D
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    .param p1, "x1"    # D

    .prologue
    .line 498
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mUpStep:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mUpStep:D

    return-wide v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;)D
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    .prologue
    .line 498
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mWalkStep:D

    return-wide v0
.end method

.method static synthetic access$418(Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;D)D
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    .param p1, "x1"    # D

    .prologue
    .line 498
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mWalkStep:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mWalkStep:D

    return-wide v0
.end method


# virtual methods
.method public addHealthyStep(D)V
    .locals 2
    .param p1, "step"    # D

    .prologue
    .line 601
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mHealthyStep:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mHealthyStep:D

    .line 602
    return-void
.end method

.method public addWalkRunUpSteps(DDD)V
    .locals 2
    .param p1, "walkStep"    # D
    .param p3, "runStep"    # D
    .param p5, "upStep"    # D

    .prologue
    .line 520
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mWalkStep:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mWalkStep:D

    .line 521
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mRunStep:D

    add-double/2addr v0, p3

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mRunStep:D

    .line 522
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mUpStep:D

    add-double/2addr v0, p5

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mUpStep:D

    .line 523
    return-void
.end method

.method public getAvgTotalStep()D
    .locals 2

    .prologue
    .line 585
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mAvgTotalStep:D

    return-wide v0
.end method

.method public getHealtyStep()D
    .locals 2

    .prologue
    .line 580
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mHealthyStep:D

    return-wide v0
.end method

.method public getRunStep()D
    .locals 2

    .prologue
    .line 568
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mRunStep:D

    return-wide v0
.end method

.method public getSchartTimeData()Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .locals 6

    .prologue
    .line 549
    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    .line 550
    .local v5, "point":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/Double;>;"
    iget-wide v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mRunStep:D

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 551
    iget-wide v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mWalkStep:D

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 552
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 553
    .local v0, "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    iget-wide v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mStartTime:J

    iget-wide v3, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mUpStep:D

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JDLjava/util/LinkedList;)V

    .line 555
    return-object v0
.end method

.method public getStartTime()J
    .locals 2

    .prologue
    .line 560
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mStartTime:J

    return-wide v0
.end method

.method public getTotalStep()D
    .locals 4

    .prologue
    .line 576
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mWalkStep:D

    iget-wide v2, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mRunStep:D

    add-double/2addr v0, v2

    iget-wide v2, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mUpStep:D

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public getUpStep()D
    .locals 2

    .prologue
    .line 572
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mUpStep:D

    return-wide v0
.end method

.method public getWalkStep()D
    .locals 2

    .prologue
    .line 564
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mWalkStep:D

    return-wide v0
.end method

.method public isGoalAchived()Z
    .locals 1

    .prologue
    .line 609
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mIsGoalAchived:Z

    return v0
.end method

.method public isMaxSteps()Z
    .locals 1

    .prologue
    .line 613
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mIsMaxSteps:Z

    return v0
.end method

.method public setAvgTotalStep(D)V
    .locals 0
    .param p1, "step"    # D

    .prologue
    .line 605
    iput-wide p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mAvgTotalStep:D

    .line 606
    return-void
.end method

.method public setHealthyStep(D)V
    .locals 0
    .param p1, "step"    # D

    .prologue
    .line 597
    iput-wide p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mHealthyStep:D

    .line 598
    return-void
.end method

.method public setIsGoalAchived(Z)V
    .locals 0
    .param p1, "isGoalAchived"    # Z

    .prologue
    .line 593
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mIsGoalAchived:Z

    .line 594
    return-void
.end method

.method public setIsMaxSteps(Z)V
    .locals 0
    .param p1, "isMaxSteps"    # Z

    .prologue
    .line 589
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mIsMaxSteps:Z

    .line 590
    return-void
.end method

.method public setStartTime(J)V
    .locals 0
    .param p1, "start_time"    # J

    .prologue
    .line 510
    iput-wide p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mStartTime:J

    .line 511
    return-void
.end method

.method public setWalkRunUpSteps(DDD)V
    .locals 0
    .param p1, "walkStep"    # D
    .param p3, "runStep"    # D
    .param p5, "upStep"    # D

    .prologue
    .line 514
    iput-wide p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mWalkStep:D

    .line 515
    iput-wide p3, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mRunStep:D

    .line 516
    iput-wide p5, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mUpStep:D

    .line 517
    return-void
.end method
