.class public Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;
.super Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;
.source "WalkingMateGraphFragmentSIC.java"


# static fields
.field private static final BAR_CHART_INDEX:I

.field private static HOURS_IN_DAY_MARKING_INTERVAL:I

.field private static HOUR_BAR_WIDTH:I

.field private static final TAG:Ljava/lang/String;

.field public static selectedTime:J


# instance fields
.field density:F

.field private mCalendar:Ljava/util/Calendar;

.field private mDayDataList:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;",
            ">;"
        }
    .end annotation
.end field

.field private mDayStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

.field mGoalValue:F

.field private mHourDataList:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;",
            ">;"
        }
    .end annotation
.end field

.field private mHourStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

.field private mLegendView:Landroid/view/View;

.field private mMonthDataList:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;",
            ">;"
        }
    .end annotation
.end field

.field private mMonthStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

.field private mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

.field private mRootView:Landroid/view/View;

.field scaledDensity:F

.field private stepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

.field private switchListener:Landroid/view/View$OnClickListener;

.field private switchToWalkingSummary:Landroid/widget/ImageButton;

.field private walkingmateInformationArea:Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 66
    const-class v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->TAG:Ljava/lang/String;

    .line 73
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->selectedTime:J

    .line 76
    const/16 v0, 0x3c

    sput v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->HOURS_IN_DAY_MARKING_INTERVAL:I

    .line 101
    const v0, 0x927c0

    sput v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->HOUR_BAR_WIDTH:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 104
    invoke-direct {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;-><init>()V

    .line 70
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->switchListener:Landroid/view/View$OnClickListener;

    .line 71
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->stepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    .line 74
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mCalendar:Ljava/util/Calendar;

    .line 83
    const v0, 0x461c4000    # 10000.0f

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mGoalValue:F

    .line 91
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mHourStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    .line 92
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    .line 93
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mMonthStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    .line 95
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mHourDataList:Ljava/util/Vector;

    .line 96
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayDataList:Ljava/util/Vector;

    .line 97
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mMonthDataList:Ljava/util/Vector;

    .line 105
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;)Ljava/util/Vector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayDataList:Ljava/util/Vector;

    return-object v0
.end method

.method private getMaximumTimeLessThanTargetTime(Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;J)J
    .locals 11
    .param p1, "stepBarSeries"    # Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;
    .param p2, "targetTime"    # J

    .prologue
    .line 505
    const-wide/16 v2, 0x0

    .line 507
    .local v2, "endOfTime":J
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->values()[Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-result-object v9

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getGrapthSelectedTab()I

    move-result v10

    aget-object v7, v9, v10

    .line 508
    .local v7, "prevPeriodType":Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {v9, v7}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 510
    invoke-static {p2, p3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v2

    .line 549
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getSize()I

    move-result v8

    .line 550
    .local v8, "size":I
    const/4 v4, 0x0

    .line 551
    .local v4, "index":I
    const-wide/16 v5, -0x1

    .line 553
    .local v5, "maximumTime":J
    add-int/lit8 v4, v8, -0x1

    :goto_1
    if-ltz v4, :cond_1

    .line 554
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v0

    .line 555
    .local v0, "currentTime":J
    cmp-long v9, v0, v2

    if-gtz v9, :cond_9

    .line 556
    move-wide v5, v0

    .line 561
    .end local v0    # "currentTime":J
    :cond_1
    const-wide/16 v9, -0x1

    cmp-long v9, v5, v9

    if-nez v9, :cond_2

    .line 562
    invoke-static {p2, p3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfMonth(J)J

    move-result-wide v2

    .line 563
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v2

    .line 565
    add-int/lit8 v4, v8, -0x1

    :goto_2
    if-ltz v4, :cond_2

    .line 566
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v0

    .line 567
    .restart local v0    # "currentTime":J
    cmp-long v9, v0, v2

    if-gtz v9, :cond_a

    .line 568
    move-wide v5, v0

    .line 574
    .end local v0    # "currentTime":J
    :cond_2
    return-wide v5

    .line 512
    .end local v4    # "index":I
    .end local v5    # "maximumTime":J
    .end local v8    # "size":I
    :cond_3
    sget-object v9, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v7, v9, :cond_5

    .line 514
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v10, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v9, v10, :cond_4

    .line 515
    const-string v9, "HOUR -> DAY"

    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 516
    invoke-static {p2, p3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v2

    goto :goto_0

    .line 518
    :cond_4
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v10, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v9, v10, :cond_0

    .line 519
    const-string v9, "HOUR -> MONTH"

    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 520
    invoke-static {p2, p3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfMonth(J)J

    move-result-wide v2

    goto :goto_0

    .line 523
    :cond_5
    sget-object v9, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v7, v9, :cond_7

    .line 525
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v10, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v9, v10, :cond_6

    .line 526
    const-string v9, "DAY -> HOUR"

    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 527
    invoke-static {p2, p3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v2

    goto :goto_0

    .line 529
    :cond_6
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v10, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v9, v10, :cond_0

    .line 530
    const-string v9, "DAY -> MONTH"

    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 531
    invoke-static {p2, p3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfMonth(J)J

    move-result-wide v2

    goto/16 :goto_0

    .line 534
    :cond_7
    sget-object v9, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v7, v9, :cond_0

    .line 536
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v10, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v9, v10, :cond_8

    .line 537
    const-string v9, "MONTH - > HOUR"

    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 538
    invoke-static {p2, p3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfMonth(J)J

    move-result-wide v2

    .line 539
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v2

    goto/16 :goto_0

    .line 541
    :cond_8
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v10, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v9, v10, :cond_0

    .line 542
    const-string v9, "MONTH - > DAY"

    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 543
    invoke-static {p2, p3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfMonth(J)J

    move-result-wide v2

    goto/16 :goto_0

    .line 553
    .restart local v0    # "currentTime":J
    .restart local v4    # "index":I
    .restart local v5    # "maximumTime":J
    .restart local v8    # "size":I
    :cond_9
    add-int/lit8 v4, v4, -0x1

    goto/16 :goto_1

    .line 565
    :cond_a
    add-int/lit8 v4, v4, -0x1

    goto/16 :goto_2
.end method

.method private getStartOfCanonicalPeriod(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)J
    .locals 3
    .param p1, "time"    # J
    .param p3, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 970
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p3, v0, :cond_0

    .line 971
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfHour(J)J

    move-result-wide v0

    .line 975
    :goto_0
    return-wide v0

    .line 972
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p3, v0, :cond_1

    .line 973
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v0

    goto :goto_0

    .line 974
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p3, v0, :cond_2

    .line 975
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v0

    goto :goto_0

    .line 977
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "periodType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public clearDatas()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1054
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mHourStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    .line 1055
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    .line 1056
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mMonthStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    .line 1057
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mHourDataList:Ljava/util/Vector;

    if-eqz v0, :cond_0

    .line 1058
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mHourDataList:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 1059
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mHourDataList:Ljava/util/Vector;

    .line 1061
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayDataList:Ljava/util/Vector;

    if-eqz v0, :cond_1

    .line 1062
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayDataList:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 1063
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayDataList:Ljava/util/Vector;

    .line 1065
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mMonthDataList:Ljava/util/Vector;

    if-eqz v0, :cond_2

    .line 1066
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mMonthDataList:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 1067
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mMonthDataList:Ljava/util/Vector;

    .line 1069
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->switchToWalkingSummary:Landroid/widget/ImageButton;

    if-eqz v0, :cond_3

    .line 1070
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->switchToWalkingSummary:Landroid/widget/ImageButton;

    .line 1071
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->switchListener:Landroid/view/View$OnClickListener;

    .line 1073
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->walkingmateInformationArea:Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;

    if-eqz v0, :cond_4

    .line 1074
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->walkingmateInformationArea:Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;

    .line 1076
    :cond_4
    return-void
.end method

.method protected declared-synchronized createDataSet(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;
    .locals 4
    .param p1, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 461
    monitor-enter p0

    :try_start_0
    const-string v2, " "

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 462
    new-instance v0, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;-><init>()V

    .line 464
    .local v0, "dataSet":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;
    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v2, :cond_3

    .line 465
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mHourStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    if-nez v2, :cond_0

    .line 466
    new-instance v2, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-direct {v2}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mHourStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    .line 467
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getHourSchartTimeData()Ljava/util/List;

    move-result-object v1

    .line 469
    .local v1, "datas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    if-eqz v1, :cond_0

    .line 470
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mHourStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->setAllData(Ljava/util/List;)V

    .line 473
    .end local v1    # "datas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    :cond_0
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mHourStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->addSeries(ILcom/samsung/android/sdk/chart/series/SchartSeries;)V

    .line 496
    :cond_1
    :goto_0
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 497
    const-string v2, "dataSet is empty"

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 500
    :cond_2
    monitor-exit p0

    return-object v0

    .line 474
    :cond_3
    :try_start_1
    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v2, :cond_5

    .line 475
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    if-nez v2, :cond_4

    .line 476
    new-instance v2, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-direct {v2}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    .line 477
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getDaySchartTimeData()Ljava/util/List;

    move-result-object v1

    .line 479
    .restart local v1    # "datas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    if-eqz v1, :cond_4

    .line 480
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->setAllData(Ljava/util/List;)V

    .line 483
    .end local v1    # "datas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    :cond_4
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->addSeries(ILcom/samsung/android/sdk/chart/series/SchartSeries;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 461
    .end local v0    # "dataSet":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 484
    .restart local v0    # "dataSet":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;
    :cond_5
    :try_start_2
    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v2, :cond_1

    .line 485
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mMonthStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    if-nez v2, :cond_6

    .line 486
    new-instance v2, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-direct {v2}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mMonthStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    .line 487
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getMonthSchartTimeData()Ljava/util/List;

    move-result-object v1

    .line 489
    .restart local v1    # "datas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    if-eqz v1, :cond_6

    .line 490
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mMonthStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->setAllData(Ljava/util/List;)V

    .line 493
    .end local v1    # "datas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    :cond_6
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mMonthStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->addSeries(ILcom/samsung/android/sdk/chart/series/SchartSeries;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method protected customizeChartStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V
    .locals 10
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    .prologue
    const v9, 0x7f0a04fe

    const/16 v8, 0xff

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 837
    const-string v3, " "

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 838
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->customizeChartStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V

    .line 840
    const/high16 v3, 0x41800000    # 16.0f

    iget v4, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->density:F

    mul-float/2addr v3, v4

    invoke-virtual {p1, v6, v6, v6, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPadding(FFFF)V

    .line 841
    invoke-virtual {p1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getSeparatorTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v0

    .line 842
    .local v0, "sepTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/16 v3, 0x50

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAlpha(I)V

    .line 843
    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->density:F

    const/high16 v4, 0x42080000    # 34.0f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setSeparatorTextSpacingTop(I)V

    .line 844
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a04fc

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 846
    new-instance v1, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 847
    .local v1, "xTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 848
    invoke-virtual {v1, v7}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 849
    invoke-virtual {v1, v8, v5, v5, v5}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 850
    invoke-virtual {v1, v5}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 851
    const-string/jumbo v3, "sec-roboto-light"

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 852
    invoke-virtual {p1, v1, v5}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 854
    new-instance v2, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v2}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 855
    .local v2, "yTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 856
    invoke-virtual {v2, v7}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 857
    invoke-virtual {v2, v8, v5, v5, v5}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 858
    invoke-virtual {v2, v5}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 859
    const-string/jumbo v3, "sec-roboto-light"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 860
    invoke-virtual {p1, v2, v5}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 863
    invoke-virtual {p1, v7, v5}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisMaxRounding(II)V

    .line 864
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v5}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisMaxRoundDigit(II)V

    .line 865
    const v3, 0x3f8ccccd    # 1.1f

    invoke-virtual {p1, v3, v5}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisMaxRoundMagnification(FI)V

    .line 866
    return-void
.end method

.method protected customizeHandlerItemWhenVisible(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V
    .locals 5
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 871
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->customizeHandlerItemWhenVisible(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V

    .line 872
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04f9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {p1, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemHeight(F)V

    .line 873
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04fa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {p1, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 874
    new-instance v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 875
    .local v0, "handlerItemTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04fb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 876
    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 877
    const/16 v1, 0xff

    invoke-virtual {v0, v1, v3, v3, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 878
    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 879
    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 880
    return-void
.end method

.method public getAvgTotalStepData(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/util/HashMap;
    .locals 6
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 930
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 932
    .local v2, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Double;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mMonthDataList:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 933
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mMonthDataList:Ljava/util/Vector;

    invoke-virtual {v3, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    .line 934
    .local v0, "data":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getStartTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getAvgTotalStep()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 932
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 936
    .end local v0    # "data":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    :cond_0
    return-object v2
.end method

.method protected getContentUriList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 997
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/net/Uri;

    const/4 v1, 0x0

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method getDaySchartTimeData()Ljava/util/List;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartTimeData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 216
    const-string v2, " "

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 217
    new-instance v9, Ljava/util/Vector;

    invoke-direct {v9}, Ljava/util/Vector;-><init>()V

    .line 218
    .local v9, "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mHourDataList:Ljava/util/Vector;

    if-nez v2, :cond_0

    .line 219
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getHourSchartTimeData()Ljava/util/List;

    .line 221
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mHourDataList:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-nez v2, :cond_2

    .line 315
    :cond_1
    :goto_0
    return-object v9

    .line 224
    :cond_2
    const/4 v13, 0x0

    .line 225
    .local v13, "max_step":I
    const/4 v14, 0x0

    .line 226
    .local v14, "max_step_index":I
    const-wide/16 v15, 0x0

    .line 227
    .local v15, "startTime":J
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayDataList:Ljava/util/Vector;

    .line 229
    const/4 v1, 0x0

    .line 231
    .local v1, "prevData":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mHourDataList:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    .line 235
    .local v8, "currentData":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    if-nez v1, :cond_3

    .line 236
    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    .end local v1    # "prevData":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    invoke-direct {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;-><init>()V

    .line 237
    .restart local v1    # "prevData":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getStartTime()J

    move-result-wide v2

    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getStartOfCanonicalPeriod(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->setStartTime(J)V

    .line 238
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getWalkStep()D

    move-result-wide v2

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getRunStep()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->setWalkRunUpSteps(DDD)V

    .line 239
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getHealtyStep()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->setHealthyStep(D)V

    goto :goto_1

    .line 241
    :cond_3
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getStartTime()J

    move-result-wide v2

    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getStartOfCanonicalPeriod(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)J

    move-result-wide v15

    .line 243
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getStartTime()J

    move-result-wide v2

    cmp-long v2, v2, v15

    if-nez v2, :cond_4

    .line 244
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getWalkStep()D

    move-result-wide v2

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getRunStep()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->addWalkRunUpSteps(DDD)V

    .line 246
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getHealtyStep()D

    move-result-wide v10

    .line 266
    .local v10, "healthyStep":D
    invoke-virtual {v1, v10, v11}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->addHealthyStep(D)V

    goto :goto_1

    .line 269
    .end local v10    # "healthyStep":D
    :cond_4
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getTotalStep()D

    move-result-wide v2

    double-to-int v0, v2

    move/from16 v17, v0

    .line 270
    .local v17, "total_step":I
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getStartTime()J

    move-result-wide v3

    move/from16 v0, v17

    invoke-virtual {v2, v3, v4, v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->isGoalAchived(JI)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 271
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->setIsGoalAchived(Z)V

    .line 274
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayDataList:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 275
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getSchartTimeData()Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    move-result-object v2

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 277
    move/from16 v0, v17

    if-ge v13, v0, :cond_6

    .line 278
    move/from16 v13, v17

    .line 279
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayDataList:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    add-int/lit8 v14, v2, -0x1

    .line 282
    :cond_6
    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    .end local v1    # "prevData":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    invoke-direct {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;-><init>()V

    .line 283
    .restart local v1    # "prevData":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getStartTime()J

    move-result-wide v2

    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getStartOfCanonicalPeriod(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->setStartTime(J)V

    .line 284
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getWalkStep()D

    move-result-wide v2

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getRunStep()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->setWalkRunUpSteps(DDD)V

    .line 285
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getHealtyStep()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->setHealthyStep(D)V

    goto/16 :goto_1

    .line 293
    .end local v8    # "currentData":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    .end local v17    # "total_step":I
    :cond_7
    const/16 v17, 0x0

    .line 294
    .restart local v17    # "total_step":I
    if-eqz v1, :cond_9

    .line 296
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getTotalStep()D

    move-result-wide v2

    double-to-int v0, v2

    move/from16 v17, v0

    .line 297
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getStartTime()J

    move-result-wide v3

    move/from16 v0, v17

    invoke-virtual {v2, v3, v4, v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->isGoalAchived(JI)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 298
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->setIsGoalAchived(Z)V

    .line 301
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayDataList:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 302
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getSchartTimeData()Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    move-result-object v2

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 305
    :cond_9
    move/from16 v0, v17

    if-ge v13, v0, :cond_a

    .line 306
    move/from16 v13, v17

    .line 307
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayDataList:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    add-int/lit8 v14, v2, -0x1

    .line 311
    :cond_a
    const/16 v2, 0x2710

    if-lt v13, v2, :cond_1

    .line 312
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "max steps : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayDataList:Ljava/util/Vector;

    invoke-virtual {v2, v14}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getTotalStep()D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 313
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayDataList:Ljava/util/Vector;

    invoke-virtual {v2, v14}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->setIsMaxSteps(Z)V

    goto/16 :goto_0
.end method

.method public getHealthyStepData(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/util/HashMap;
    .locals 8
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 909
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 910
    .local v5, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Double;>;"
    const/4 v1, 0x0

    .line 911
    .local v1, "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;>;"
    sget-object v6, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v6, :cond_1

    .line 912
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mHourDataList:Ljava/util/Vector;

    .line 919
    :cond_0
    :goto_0
    if-eqz v1, :cond_3

    .line 920
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    if-ge v4, v6, :cond_3

    .line 921
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    .line 922
    .local v0, "data":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getHealtyStep()D

    move-result-wide v2

    .line 923
    .local v2, "healthyStep":D
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getStartTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 920
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 913
    .end local v0    # "data":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    .end local v2    # "healthyStep":D
    .end local v4    # "i":I
    :cond_1
    sget-object v6, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v6, :cond_2

    .line 914
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayDataList:Ljava/util/Vector;

    goto :goto_0

    .line 915
    :cond_2
    sget-object v6, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v6, :cond_0

    .line 916
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mMonthDataList:Ljava/util/Vector;

    goto :goto_0

    .line 926
    :cond_3
    return-object v5
.end method

.method getHourSchartTimeData()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartTimeData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 197
    const-string v3, " "

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 199
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v1

    .line 200
    .local v1, "deviceType":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "connected device type :  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 202
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getWalkInfoDataForHourChart(I)Ljava/util/Vector;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mHourDataList:Ljava/util/Vector;

    .line 204
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    .line 206
    .local v0, "chartTimeDataList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mHourDataList:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 208
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mHourDataList:Ljava/util/Vector;

    invoke-virtual {v3, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getSchartTimeData()Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 206
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 211
    :cond_0
    return-object v0
.end method

.method protected getInfoView()Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;
    .locals 1

    .prologue
    .line 1012
    const-string v0, " "

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 1013
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->walkingmateInformationArea:Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;

    return-object v0
.end method

.method protected getLegendButtonBackgroundResourceId()I
    .locals 1

    .prologue
    .line 1002
    const/4 v0, 0x0

    return v0
.end method

.method protected getLegendMarks()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/logutils/graph/LegendMark;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1007
    const/4 v0, 0x0

    return-object v0
.end method

.method getMonthSchartTimeData()Ljava/util/List;
    .locals 27
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartTimeData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 320
    const-string v9, " "

    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 321
    new-instance v21, Ljava/util/Vector;

    invoke-direct/range {v21 .. v21}, Ljava/util/Vector;-><init>()V

    .line 322
    .local v21, "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v23

    .line 323
    .local v23, "deviceType":I
    const/16 v20, 0x0

    .line 324
    .local v20, "cursor":Landroid/database/Cursor;
    new-instance v9, Ljava/util/Vector;

    invoke-direct {v9}, Ljava/util/Vector;-><init>()V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mMonthDataList:Ljava/util/Vector;

    .line 325
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    .line 327
    .local v25, "selection":Ljava/lang/StringBuilder;
    sget-object v9, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->mGraphCommonQuery:Ljava/lang/StringBuilder;

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 328
    const-string v9, " WHERE "

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 329
    const-string/jumbo v9, "total_step > 0 "

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 330
    const-string v9, " AND "

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 331
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 332
    const-string v9, " GROUP BY "

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 333
    const-string v9, "(strftime(\"%m-%Y\",([start_time]/1000),\'unixepoch\',\'localtime\'))"

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 334
    const-string v9, " ORDER BY start_time ASC"

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 338
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v20

    .line 340
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Month step query : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 342
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->getCount()I

    move-result v9

    if-eqz v9, :cond_6

    .line 344
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToFirst()Z

    .line 346
    :goto_0
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v9

    if-nez v9, :cond_6

    .line 347
    const-string/jumbo v9, "start_time"

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 348
    .local v3, "month_start_time":J
    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v3

    .line 349
    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getPassedDaysForMonthAverage(J)I

    move-result v22

    .line 350
    .local v22, "days":I
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "days : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " for Avg daily steps."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 352
    if-eqz v22, :cond_1

    .line 353
    const-string v9, "SUM_WALK_STEP"

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v9

    move/from16 v0, v22

    int-to-double v11, v0

    div-double v5, v9, v11

    .line 354
    .local v5, "avg_walk_step":D
    const-string v9, "SUM_RUN_STEP"

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v9

    move/from16 v0, v22

    int-to-double v11, v0

    div-double v7, v9, v11

    .line 355
    .local v7, "avg_run_step":D
    const-string v9, "SUM_TOTAL_STEP"

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v9

    move/from16 v0, v22

    int-to-double v11, v0

    div-double v17, v9, v11

    .line 356
    .local v17, "avg_total_step":D
    sub-double v5, v17, v7

    .line 370
    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    const-wide/16 v9, 0x0

    invoke-direct/range {v2 .. v10}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;-><init>(JDDD)V

    .line 372
    .local v2, "data":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    const-wide/16 v9, 0x0

    cmpg-double v9, v17, v9

    if-gez v9, :cond_0

    const-wide/high16 v17, 0x3ff0000000000000L    # 1.0

    .line 374
    :cond_0
    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->floor(D)D

    move-result-wide v17

    .line 376
    move-wide/from16 v0, v17

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->setAvgTotalStep(D)V

    .line 377
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mMonthDataList:Ljava/util/Vector;

    invoke-virtual {v9, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 378
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getSchartTimeData()Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    move-result-object v9

    move-object/from16 v0, v21

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 381
    .end local v2    # "data":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    .end local v5    # "avg_walk_step":D
    .end local v7    # "avg_run_step":D
    .end local v17    # "avg_total_step":D
    :cond_1
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto/16 :goto_0

    .line 385
    .end local v3    # "month_start_time":J
    .end local v22    # "days":I
    :catch_0
    move-exception v24

    .line 386
    .local v24, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 390
    if-eqz v20, :cond_2

    .line 391
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    .line 395
    .end local v24    # "e":Ljava/lang/Exception;
    :cond_2
    :goto_1
    const/4 v9, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 396
    const-string v9, "SELECT"

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 397
    const-string v9, " "

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 398
    const-string v9, "MIN(start_time) AS START_TIME"

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 399
    const-string v9, " , "

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 400
    const-string v9, "ifnull(SUM(power_step), 0) AS SUM_HEALTHY_STEP"

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 401
    const-string v9, " FROM walk_info_extended"

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 402
    const-string v9, " WHERE "

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 403
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 404
    const-string v9, " GROUP BY "

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 405
    const-string v9, "(strftime(\"%m-%Y\",([start_time]/1000),\'unixepoch\',\'localtime\'))"

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 406
    const-string v9, " ORDER BY start_time ASC"

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 409
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    sget-object v10, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v11, 0x0

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual/range {v9 .. v14}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v20

    .line 411
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "healty step query to get AVG: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 413
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->getCount()I

    move-result v9

    if-eqz v9, :cond_8

    .line 414
    new-instance v19, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    invoke-direct/range {v19 .. v19}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;-><init>()V

    .line 415
    .local v19, "comparetime":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    :cond_3
    :goto_2
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToNext()Z

    move-result v9

    if-eqz v9, :cond_8

    .line 416
    const-string v9, "START_TIME"

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 417
    .restart local v3    # "month_start_time":J
    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v3

    .line 418
    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getPassedDaysForMonthAverage(J)I

    move-result v22

    .line 420
    .restart local v22    # "days":I
    if-eqz v22, :cond_3

    .line 422
    const-string v9, "SUM_HEALTHY_STEP"

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v9

    move/from16 v0, v22

    int-to-double v11, v0

    div-double v15, v9, v11

    .line 424
    .local v15, "avg_healhty_step":D
    const-wide/16 v9, 0x0

    cmpl-double v9, v15, v9

    if-lez v9, :cond_4

    const-wide/high16 v9, 0x3ff0000000000000L    # 1.0

    cmpg-double v9, v15, v9

    if-gez v9, :cond_4

    const-wide/high16 v15, 0x3ff0000000000000L    # 1.0

    .line 426
    :cond_4
    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->floor(D)D

    move-result-wide v15

    .line 428
    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->setStartTime(J)V

    .line 430
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mMonthDataList:Ljava/util/Vector;

    new-instance v10, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC$1;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;)V

    move-object/from16 v0, v19

    invoke-static {v9, v0, v10}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v26

    .line 443
    .local v26, "walkInfoIndex":I
    if-ltz v26, :cond_3

    .line 444
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mMonthDataList:Ljava/util/Vector;

    move/from16 v0, v26

    invoke-virtual {v9, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    move-wide v0, v15

    invoke-virtual {v9, v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->setHealthyStep(D)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 450
    .end local v3    # "month_start_time":J
    .end local v15    # "avg_healhty_step":D
    .end local v19    # "comparetime":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    .end local v22    # "days":I
    .end local v26    # "walkInfoIndex":I
    :catchall_0
    move-exception v9

    if-eqz v20, :cond_5

    .line 451
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v9

    .line 390
    :cond_6
    if-eqz v20, :cond_2

    .line 391
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 390
    :catchall_1
    move-exception v9

    if-eqz v20, :cond_7

    .line 391
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v9

    .line 450
    :cond_8
    if-eqz v20, :cond_9

    .line 451
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    .line 455
    :cond_9
    return-object v21
.end method

.method protected getNoDataIcon()I
    .locals 1

    .prologue
    .line 992
    const v0, 0x7f0205b7

    return v0
.end method

.method protected getYAxisLabelTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 983
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f0907e5

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected initDateBar()V
    .locals 11

    .prologue
    const v10, 0x7f09020b

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 152
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->initDateBar()V

    .line 153
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090211

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fb0

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 154
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090213

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fb0

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 155
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090212

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fb0

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 156
    return-void
.end method

.method protected initGraphLegendArea()Landroid/view/View;
    .locals 6

    .prologue
    const v5, 0x7f080b9c

    const/16 v4, 0x8

    .line 941
    const-string v2, " "

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 942
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 943
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030290

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mLegendView:Landroid/view/View;

    .line 944
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mLegendView:Landroid/view/View;

    const v3, 0x7f080b9d

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->switchToWalkingSummary:Landroid/widget/ImageButton;

    .line 946
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    iget-boolean v1, v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mIsSensorHubInstalled:Z

    .line 948
    .local v1, "isSensorHubInstalled":Z
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->isMobilePedometerDisabled(Landroid/content/Context;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 949
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->switchToWalkingSummary:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 951
    :cond_0
    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC$3;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->switchListener:Landroid/view/View$OnClickListener;

    .line 958
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->switchToWalkingSummary:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->switchListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 960
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getWalkInfoDataCount()I

    move-result v2

    if-nez v2, :cond_1

    .line 961
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mLegendView:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 966
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mLegendView:Landroid/view/View;

    return-object v2

    .line 963
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mLegendView:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected initGraphView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
    .locals 3
    .param p1, "periodStart"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 177
    const-string v0, " "

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 179
    iput-object p3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->values()[Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getGrapthSelectedTab()I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 182
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->initializeChartData()V

    .line 185
    :cond_0
    invoke-static {p3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setGrapthSelectedTab(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 188
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isWalkingDataExist()Z

    move-result v0

    if-nez v0, :cond_1

    .line 189
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->removeShareViaButton()V

    .line 192
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->initGraphView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected initInformationArea(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
    .locals 4
    .param p1, "periodStart"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 884
    const-string v2, " "

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 886
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v0

    .line 887
    .local v0, "deviceType":I
    const/4 v1, 0x0

    .line 888
    .local v1, "isSupportHealthyPace":Z
    const/16 v2, 0x2719

    if-ne v0, v2, :cond_3

    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->isSupportDurationAndHealthPace()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 889
    const/4 v1, 0x1

    .line 894
    :cond_0
    :goto_0
    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3, p3, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;-><init>(Landroid/content/Context;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;Z)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->walkingmateInformationArea:Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;

    .line 895
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->walkingmateInformationArea:Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;

    invoke-virtual {v2, p3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->setDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 897
    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 898
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->walkingmateInformationArea:Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;

    invoke-virtual {p0, p3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getHealthyStepData(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->setHealthyStepData(Ljava/util/HashMap;)V

    .line 901
    :cond_1
    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p3, v2, :cond_2

    .line 902
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->walkingmateInformationArea:Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;

    invoke-virtual {p0, p3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getAvgTotalStepData(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->setAvgTotalStepData(Ljava/util/HashMap;)V

    .line 905
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->walkingmateInformationArea:Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;

    return-object v2

    .line 890
    :cond_3
    const/16 v2, 0x272f

    if-ne v0, v2, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->isSupportDurationAndHealthPaceForGear()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 891
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected initSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;)V
    .locals 19
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .param p2, "dataSet"    # Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;

    .prologue
    .line 676
    new-instance v15, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;

    invoke-direct {v15}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;-><init>()V

    .line 677
    .local v15, "seriesStyle":Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;
    new-instance v15, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;

    .end local v15    # "seriesStyle":Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;
    invoke-direct {v15}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;-><init>()V

    .line 678
    .restart local v15    # "seriesStyle":Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0907e5

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setSeriesName(Ljava/lang/String;)V

    .line 679
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0701a4

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getColor(I)I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->addItemColor(I)V

    .line 680
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0701a3

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getColor(I)I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->addItemColor(I)V

    .line 681
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0701a2

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getColor(I)I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->addItemColor(I)V

    .line 683
    sget-object v16, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-ne v0, v1, :cond_1

    .line 685
    new-instance v9, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 686
    .local v9, "goalLineTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0700fa

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getColor(I)I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v9, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setColor(I)V

    .line 687
    invoke-virtual {v15, v9}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 690
    const/16 v11, 0x514

    .line 691
    .local v11, "hourMinGoalLine":I
    const v16, 0x44a28000    # 1300.0f

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineValue(F)V

    .line 692
    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineVisible(Z)V

    .line 693
    sget v16, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->HOUR_BAR_WIDTH:I

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v16, v0

    invoke-virtual/range {v15 .. v17}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setStackedBarWidth(J)V

    .line 694
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0700fa

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getColor(I)I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineColor(I)V

    .line 695
    const-string v16, ""

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineTextPrefix(Ljava/lang/String;)V

    .line 724
    .end local v11    # "hourMinGoalLine":I
    :goto_0
    sget-object v16, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-ne v0, v1, :cond_0

    .line 725
    const/4 v3, 0x0

    .line 726
    .local v3, "ICON_SHOES":I
    const/4 v2, 0x1

    .line 727
    .local v2, "ICON_CROWN":I
    const/4 v4, 0x2

    .line 728
    .local v4, "ICON_SHOES_CROWN":I
    new-instance v12, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC$2;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC$2;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;)V

    .line 786
    .local v12, "listener":Lcom/samsung/android/sdk/chart/style/SchartEventIconListener;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 787
    .local v7, "eventicons":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v6

    .line 789
    .local v6, "deviceType":I
    const/16 v16, 0x2719

    move/from16 v0, v16

    if-ne v6, v0, :cond_3

    .line 790
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0205bb

    invoke-static/range {v16 .. v17}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 791
    .local v5, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 792
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0205ba

    invoke-static/range {v16 .. v17}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 793
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 794
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0205bd

    invoke-static/range {v16 .. v17}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 795
    if-nez v5, :cond_2

    const/4 v10, 0x0

    .line 796
    .local v10, "height":I
    :goto_1
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 797
    invoke-virtual {v15, v7}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setEventIconBitmapList(Ljava/util/ArrayList;)V

    .line 799
    const/16 v16, 0x1

    const/high16 v17, 0x40400000    # 3.0f

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v18

    invoke-static/range {v16 .. v18}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v14

    .line 800
    .local v14, "px":F
    const/high16 v16, -0x40800000    # -1.0f

    div-int/lit8 v17, v10, 0x2

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    add-float v17, v17, v14

    mul-float v13, v16, v17

    .line 802
    .local v13, "margin":F
    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setEventIconVisible(Z)V

    .line 803
    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setEventIconScalable(Z)V

    .line 804
    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setEventIconOffsetX(F)V

    .line 805
    invoke-virtual {v15, v13}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setEventIconOffsetY(F)V

    .line 806
    invoke-virtual {v15, v12}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setEventIconListener(Lcom/samsung/android/sdk/chart/style/SchartEventIconListener;)V

    .line 832
    .end local v2    # "ICON_CROWN":I
    .end local v3    # "ICON_SHOES":I
    .end local v4    # "ICON_SHOES_CROWN":I
    .end local v5    # "bitmap":Landroid/graphics/Bitmap;
    .end local v6    # "deviceType":I
    .end local v7    # "eventicons":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    .end local v10    # "height":I
    .end local v12    # "listener":Lcom/samsung/android/sdk/chart/style/SchartEventIconListener;
    .end local v13    # "margin":F
    .end local v14    # "px":F
    :cond_0
    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->addSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;)V

    .line 833
    return-void

    .line 698
    .end local v9    # "goalLineTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    :cond_1
    new-instance v8, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v8}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 699
    .local v8, "goalLineLabelStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0a0063

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 700
    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 701
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f070162

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getColor(I)I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setColor(I)V

    .line 702
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 703
    const-string/jumbo v16, "sec-roboto-light"

    move-object/from16 v0, v16

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 704
    invoke-virtual {v15, v8}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineTextPrefixStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 706
    new-instance v9, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 707
    .restart local v9    # "goalLineTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0a0062

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v9, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 708
    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v9, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 709
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f070162

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getColor(I)I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v9, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setColor(I)V

    .line 710
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v9, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 711
    const-string/jumbo v16, "sec-roboto-light"

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 712
    invoke-virtual {v15, v9}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 714
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0a0064

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v16

    move/from16 v0, v16

    neg-float v0, v0

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineTextOffsetX(F)V

    .line 715
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0a0062

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v16

    const/high16 v17, 0x40000000    # 2.0f

    div-float v16, v16, v17

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineTextOffsetY(F)V

    .line 716
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mGoalValue:F

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineValue(F)V

    .line 717
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const v17, 0x7f090b8c

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineTextPrefix(Ljava/lang/String;)V

    .line 718
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0701a5

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getColor(I)I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineColor(I)V

    .line 719
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0a0061

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineThickness(F)V

    .line 720
    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineVisible(Z)V

    goto/16 :goto_0

    .line 795
    .end local v8    # "goalLineLabelStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .restart local v2    # "ICON_CROWN":I
    .restart local v3    # "ICON_SHOES":I
    .restart local v4    # "ICON_SHOES_CROWN":I
    .restart local v5    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v6    # "deviceType":I
    .restart local v7    # "eventicons":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    .restart local v12    # "listener":Lcom/samsung/android/sdk/chart/style/SchartEventIconListener;
    :cond_2
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    goto/16 :goto_1

    .line 809
    .end local v5    # "bitmap":Landroid/graphics/Bitmap;
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0205bc

    invoke-static/range {v16 .. v17}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 810
    .restart local v5    # "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 811
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0205ba

    invoke-static/range {v16 .. v17}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 812
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 813
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0205be

    invoke-static/range {v16 .. v17}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 814
    if-nez v5, :cond_4

    const/4 v10, 0x0

    .line 815
    .restart local v10    # "height":I
    :goto_3
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 817
    invoke-virtual {v15, v7}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setEventIconBitmapList(Ljava/util/ArrayList;)V

    .line 819
    const/16 v16, 0x1

    const/high16 v17, 0x40400000    # 3.0f

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v18

    invoke-static/range {v16 .. v18}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v14

    .line 820
    .restart local v14    # "px":F
    const/high16 v16, -0x40800000    # -1.0f

    div-int/lit8 v17, v10, 0x2

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    add-float v17, v17, v14

    mul-float v13, v16, v17

    .line 822
    .restart local v13    # "margin":F
    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setEventIconVisible(Z)V

    .line 823
    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setEventIconScalable(Z)V

    .line 824
    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setEventIconOffsetX(F)V

    .line 825
    invoke-virtual {v15, v13}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setEventIconOffsetY(F)V

    .line 826
    invoke-virtual {v15, v12}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setEventIconListener(Lcom/samsung/android/sdk/chart/style/SchartEventIconListener;)V

    goto/16 :goto_2

    .line 814
    .end local v10    # "height":I
    .end local v13    # "margin":F
    .end local v14    # "px":F
    :cond_4
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    goto :goto_3
.end method

.method public initializeChartData()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 108
    const-string v0, " "

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 109
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mHourStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    .line 110
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    .line 111
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mMonthStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    .line 112
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mHourDataList:Ljava/util/Vector;

    .line 113
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayDataList:Ljava/util/Vector;

    .line 114
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mMonthDataList:Ljava/util/Vector;

    .line 115
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 119
    const-string v0, " "

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 120
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.walkingmate"

    const-string v2, "W006"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->density:F

    .line 123
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->scaledDensity:F

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->scaledDensity:F

    .line 125
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->isSummaryFragment:Z

    .line 127
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->initializeChartData()V

    .line 128
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->values()[Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getGrapthSelectedTab()I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 134
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->refreshGoalInfo()V

    .line 136
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->loadGoalAndSearch(J)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mGoalValue:F

    .line 138
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mRootView:Landroid/view/View;

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mRootView:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 1050
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onDestroyView()V

    .line 1051
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->clearDatas()V

    .line 1052
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    const v1, 0x7f080b9c

    .line 160
    const-string v0, " "

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mHourDataList:Ljava/util/Vector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mHourDataList:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 162
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->initializeChartData()V

    .line 165
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getWalkInfoDataCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mLegendView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 172
    :goto_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onResume()V

    .line 173
    return-void

    .line 168
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mLegendView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public reloadOnTimeZoneChg()V
    .locals 2

    .prologue
    .line 1017
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mCalendar:Ljava/util/Calendar;

    .line 1018
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1046
    :cond_0
    :goto_0
    return-void

    .line 1021
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->initializeChartData()V

    .line 1022
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mHourStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    if-nez v1, :cond_2

    .line 1023
    new-instance v1, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mHourStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    .line 1024
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getHourSchartTimeData()Ljava/util/List;

    move-result-object v0

    .line 1026
    .local v0, "datas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    if-eqz v0, :cond_2

    .line 1027
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mHourStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->setAllData(Ljava/util/List;)V

    .line 1030
    .end local v0    # "datas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    if-nez v1, :cond_3

    .line 1031
    new-instance v1, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    .line 1032
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getDaySchartTimeData()Ljava/util/List;

    move-result-object v0

    .line 1034
    .restart local v0    # "datas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    if-eqz v0, :cond_3

    .line 1035
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->setAllData(Ljava/util/List;)V

    .line 1038
    .end local v0    # "datas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mMonthStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    if-nez v1, :cond_0

    .line 1039
    new-instance v1, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mMonthStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    .line 1040
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getMonthSchartTimeData()Ljava/util/List;

    move-result-object v0

    .line 1042
    .restart local v0    # "datas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    if-eqz v0, :cond_0

    .line 1043
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mMonthStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->setAllData(Ljava/util/List;)V

    goto :goto_0
.end method

.method protected setAdditionalChartViewArguments(Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;)V
    .locals 13
    .param p1, "chartView"    # Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    .prologue
    .line 579
    const-string v0, " "

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 580
    if-eqz p1, :cond_5

    .line 582
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v1, :cond_6

    .line 583
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mHourStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->stepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    .line 590
    :cond_0
    :goto_0
    const-wide/16 v6, 0x0

    .line 593
    .local v6, "dataToShow":J
    sget-wide v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->selectedTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->stepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getSize()I

    move-result v0

    if-lez v0, :cond_8

    .line 595
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->stepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->stepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getSize()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getX(I)J

    move-result-wide v6

    .line 608
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->stepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getSize()I

    move-result v0

    if-eqz v0, :cond_5

    .line 610
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->stepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-direct {p0, v0, v6, v7}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getMaximumTimeLessThanTargetTime(Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;J)J

    move-result-wide v6

    .line 612
    sget-wide v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->selectedTime:J

    cmp-long v0, v6, v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-eq v0, v1, :cond_3

    .line 613
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mCalendar:Ljava/util/Calendar;

    if-eqz v0, :cond_a

    .line 614
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v0, v6, v7}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 615
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mCalendar:Ljava/util/Calendar;

    const/16 v1, 0xb

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 616
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mCalendar:Ljava/util/Calendar;

    const/16 v1, 0xc

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 617
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mCalendar:Ljava/util/Calendar;

    const/16 v1, 0xd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 618
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mCalendar:Ljava/util/Calendar;

    const/16 v1, 0xe

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 619
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v1, :cond_2

    .line 620
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mCalendar:Ljava/util/Calendar;

    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 622
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    .line 628
    :cond_3
    :goto_2
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p1, v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setGraphType(II)V

    .line 630
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v1, :cond_b

    .line 632
    new-instance v9, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;

    invoke-direct {v9}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;-><init>()V

    .line 633
    .local v9, "interaction":Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;
    const/4 v0, 0x1

    invoke-virtual {v9, v0}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInteractionEnabled(Z)V

    .line 634
    const/16 v0, 0x3c

    const/16 v1, 0x3c

    invoke-virtual {v9, v0, v1}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomIntervalStep(II)V

    .line 635
    invoke-virtual {p1, v9}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setChartInteraction(Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;)V

    .line 637
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v8

    .line 638
    .local v8, "hourStartDate":Ljava/util/Calendar;
    invoke-virtual {v8, v6, v7}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 640
    const/16 v0, 0xc

    invoke-virtual {v8, v0}, Ljava/util/Calendar;->get(I)I

    move-result v10

    .line 641
    .local v10, "minutes":I
    if-eqz v10, :cond_4

    .line 642
    const-wide/32 v0, 0x36ee80

    add-long/2addr v6, v0

    .line 646
    :cond_4
    invoke-virtual {v8, v6, v7}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 647
    const/16 v0, 0xc

    const/4 v1, 0x0

    invoke-virtual {v8, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 648
    const/16 v0, 0xd

    const/4 v1, 0x0

    invoke-virtual {v8, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 649
    const/16 v0, 0xe

    const/4 v1, 0x0

    invoke-virtual {v8, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 650
    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    .line 652
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setScrollRangeType(I)V

    .line 653
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mHourStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v0

    const-wide/32 v2, 0x337f980

    sub-long v1, v0, v2

    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mHourStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mHourStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getSize()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v3

    const-wide/32 v11, 0x1ee6280

    add-long/2addr v3, v11

    invoke-virtual {p1, v1, v2, v3, v4}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setUserScrollRange(JJ)V

    .line 654
    const/4 v1, 0x0

    const-wide/32 v2, 0x1ee6280

    sub-long v2, v6, v2

    long-to-double v2, v2

    sget v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->HOURS_IN_DAY_MARKING_INTERVAL:I

    const/16 v5, 0xc

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setStartVisual(IDII)V

    .line 665
    .end local v8    # "hourStartDate":Ljava/util/Calendar;
    .end local v9    # "interaction":Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;
    .end local v10    # "minutes":I
    :goto_3
    const-string v0, "finally time to show chart view "

    invoke-static {v0, v6, v7}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->printDate(Ljava/lang/String;J)V

    .line 667
    long-to-double v0, v6

    invoke-virtual {p1, v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setHandlerStartDate(D)V

    .line 672
    .end local v6    # "dataToShow":J
    :cond_5
    return-void

    .line 584
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v1, :cond_7

    .line 585
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->stepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    goto/16 :goto_0

    .line 586
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v1, :cond_0

    .line 587
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mMonthStepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->stepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    goto/16 :goto_0

    .line 598
    .restart local v6    # "dataToShow":J
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->stepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getSize()I

    move-result v0

    if-lez v0, :cond_1

    .line 600
    sget-wide v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->selectedTime:J

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->stepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getX(I)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_9

    .line 601
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->stepBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getX(I)J

    move-result-wide v6

    goto/16 :goto_1

    .line 603
    :cond_9
    sget-wide v6, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->selectedTime:J

    goto/16 :goto_1

    .line 624
    :cond_a
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->TAG:Ljava/lang/String;

    const-string v1, "There is no mCalendar instance."

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 657
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v1, :cond_c

    .line 658
    const/4 v1, 0x2

    const-wide/32 v2, 0x19bfcc00

    sub-long v2, v6, v2

    long-to-double v2, v2

    const/4 v4, 0x1

    const/4 v5, 0x7

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setStartVisual(IDII)V

    goto :goto_3

    .line 661
    :cond_c
    const/4 v1, 0x5

    const-wide v2, 0x59cce4400L

    sub-long v2, v6, v2

    long-to-double v2, v2

    const/4 v4, 0x1

    const/16 v5, 0xc

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setStartVisual(IDII)V

    goto :goto_3
.end method

.method public updateGraphFragment()V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mRootView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 146
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->updateGraphFragment()V

    .line 148
    :cond_0
    return-void
.end method

.method protected updateLatestMeasureInHourTimeMap(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 0
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 988
    return-void
.end method
