.class public Lcom/sec/android/app/shealth/common/utils/MergeCalorie;
.super Ljava/lang/Object;
.source "MergeCalorie.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;,
        Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;
    }
.end annotation


# static fields
.field private static final MERGE_CALROIE_TYPE_EARIER:I = 0x2

.field private static final MERGE_CALROIE_TYPE_EARIER_WITH_PEDO:I = 0x3

.field private static final MERGE_CALROIE_TYPE_MAX:I = 0x1

.field private static final TAG:Ljava/lang/String;


# instance fields
.field mContext:Landroid/content/Context;

.field mPrevTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->mContext:Landroid/content/Context;

    .line 58
    return-void
.end method

.method private calculatePartialCalorie(Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;JJ)F
    .locals 6
    .param p1, "exercise"    # Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;
    .param p2, "fromTime"    # J
    .param p4, "toTime"    # J

    .prologue
    .line 394
    iget v1, p1, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;->calorie:F

    iget-wide v2, p1, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;->end:J

    iget-wide v4, p1, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;->start:J

    sub-long/2addr v2, v4

    long-to-float v2, v2

    div-float/2addr v1, v2

    sub-long v2, p4, p2

    long-to-float v2, v2

    mul-float v0, v1, v2

    .line 395
    .local v0, "partialCalorie":F
    return v0
.end method

.method private compareCalorie(Ljava/util/LinkedList;Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;)F
    .locals 11
    .param p2, "item"    # Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;",
            ">;",
            "Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;",
            ")F"
        }
    .end annotation

    .prologue
    .line 355
    .local p1, "pool":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;>;"
    const/4 v10, 0x0

    .line 356
    .local v10, "totalCalorie":F
    const/4 v7, 0x0

    .line 357
    .local v7, "maxCal":F
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Inserted : time = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p2, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;->time:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    invoke-virtual {p1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;

    .line 361
    .local v9, "t":Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;
    iget-object v0, v9, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;->exer:Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    iget-wide v0, v0, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;->end:J

    iget-wide v2, p2, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;->time:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 363
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Will be removed : time = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, v9, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;->time:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 368
    :cond_1
    iget-object v1, v9, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;->exer:Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    iget-wide v2, p0, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->mPrevTime:J

    iget-wide v4, p2, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;->time:J

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->calculatePartialCalorie(Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;JJ)F

    move-result v8

    .line 369
    .local v8, "partialCalorie":F
    cmpg-float v0, v7, v8

    if-gez v0, :cond_0

    .line 371
    move v7, v8

    .line 372
    iget-wide v0, p2, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;->time:J

    iput-wide v0, v9, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;->time:J

    goto :goto_0

    .line 380
    .end local v8    # "partialCalorie":F
    .end local v9    # "t":Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;
    :cond_2
    iget-wide v0, p2, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;->time:J

    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->mPrevTime:J

    .line 381
    add-float/2addr v10, v7

    .line 382
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TotalCalorie : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " currentMaxCalorie : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    invoke-virtual {p1, p2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 385
    return v10
.end method

.method private getMergedCalorie(JJII)F
    .locals 19
    .param p1, "startTime"    # J
    .param p3, "endTime"    # J
    .param p5, "type"    # I
    .param p6, "deviceType"    # I

    .prologue
    .line 82
    const/16 v18, 0x0

    .line 83
    .local v18, "totalCalorie":F
    const/4 v2, 0x3

    new-array v4, v2, [Ljava/lang/String;

    .line 84
    .local v4, "projection":[Ljava/lang/String;
    const/4 v2, 0x0

    const-string/jumbo v3, "start_time"

    aput-object v3, v4, v2

    .line 85
    const/4 v2, 0x1

    const-string v3, "end_time"

    aput-object v3, v4, v2

    .line 86
    const/4 v2, 0x2

    const-string/jumbo v3, "total_calorie"

    aput-object v3, v4, v2

    .line 88
    sget-object v2, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getMergedCalorie startTime :  "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, " endTime : "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    const-string/jumbo v5, "start_time >= ? AND start_time < ?  AND exercise_info__id != ? "

    .line 92
    .local v5, "selection":Ljava/lang/String;
    const/4 v2, 0x3

    new-array v6, v2, [Ljava/lang/String;

    .line 93
    .local v6, "selectionArgs":[Ljava/lang/String;
    const/4 v2, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    .line 94
    const/4 v2, 0x1

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    .line 95
    const/4 v2, 0x2

    const/16 v3, 0x4651

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    .line 97
    const/4 v14, 0x0

    .line 98
    .local v14, "cursor":Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 101
    const/16 v17, 0x0

    .line 102
    .local v17, "itemCount":I
    const/16 v16, 0x0

    .line 105
    .local v16, "i":I
    if-eqz v14, :cond_6

    :try_start_0
    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 107
    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v17

    .line 108
    sget-object v2, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exercise Count = "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    move/from16 v0, v17

    new-array v8, v0, [Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    .line 113
    .local v8, "exercise":[Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;
    :cond_0
    new-instance v2, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    invoke-direct {v2}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;-><init>()V

    aput-object v2, v8, v16

    .line 114
    aget-object v2, v8, v16

    const-string/jumbo v3, "start_time"

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    iput-wide v9, v2, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;->start:J

    .line 115
    aget-object v2, v8, v16

    const-string v3, "end_time"

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    iput-wide v9, v2, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;->end:J

    .line 116
    aget-object v2, v8, v16

    const-string/jumbo v3, "total_calorie"

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    iput v3, v2, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;->calorie:F

    .line 117
    add-int/lit8 v16, v16, 0x1

    .line 118
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 120
    const/4 v2, 0x1

    move/from16 v0, p5

    if-ne v0, v2, :cond_3

    .line 122
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->getMergedCalorieGetMax([Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;)F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v18

    .line 165
    .end local v8    # "exercise":[Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;
    :cond_1
    :goto_0
    if-eqz v14, :cond_2

    .line 166
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 169
    :cond_2
    :goto_1
    return v18

    .line 124
    .restart local v8    # "exercise":[Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;
    :cond_3
    const/4 v2, 0x2

    move/from16 v0, p5

    if-ne v0, v2, :cond_4

    .line 126
    const/4 v2, 0x0

    :try_start_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v8, v2}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->getMergedCalorieGetEarlier([Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;Ljava/util/LinkedList;)F

    move-result v18

    goto :goto_0

    .line 128
    :cond_4
    const/4 v2, 0x3

    move/from16 v0, p5

    if-ne v0, v2, :cond_1

    .line 130
    if-lez p6, :cond_5

    move-object/from16 v7, p0

    move-wide/from16 v9, p1

    move-wide/from16 v11, p3

    move/from16 v13, p6

    .line 132
    invoke-virtual/range {v7 .. v13}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->getMergedCalorieGetEarlierWithPedometer([Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;JJI)F

    move-result v18

    goto :goto_0

    :cond_5
    move-object/from16 v7, p0

    move-wide/from16 v9, p1

    move-wide/from16 v11, p3

    .line 136
    invoke-virtual/range {v7 .. v12}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->getMergedCalorieGetEarlierWithPedometer([Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;JJ)F

    move-result v18

    goto :goto_0

    .line 141
    .end local v8    # "exercise":[Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;
    :cond_6
    if-eqz v14, :cond_1

    .line 143
    const/4 v2, 0x3

    move/from16 v0, p5

    if-ne v0, v2, :cond_1

    .line 145
    move/from16 v0, v17

    new-array v8, v0, [Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    .line 146
    .restart local v8    # "exercise":[Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;
    if-lez p6, :cond_7

    move-object/from16 v7, p0

    move-wide/from16 v9, p1

    move-wide/from16 v11, p3

    move/from16 v13, p6

    .line 148
    invoke-virtual/range {v7 .. v13}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->getMergedCalorieGetEarlierWithPedometer([Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;JJI)F

    move-result v18

    goto :goto_0

    :cond_7
    move-object/from16 v7, p0

    move-wide/from16 v9, p1

    move-wide/from16 v11, p3

    .line 152
    invoke-virtual/range {v7 .. v12}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->getMergedCalorieGetEarlierWithPedometer([Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;JJ)F
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v18

    goto :goto_0

    .line 159
    .end local v8    # "exercise":[Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;
    :catch_0
    move-exception v15

    .line 161
    .local v15, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v2, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception : "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v15}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 165
    if-eqz v14, :cond_2

    .line 166
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 165
    .end local v15    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v14, :cond_8

    .line 166
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v2
.end method


# virtual methods
.method public getIntegratedWalkInfo(Landroid/content/Context;JJ)Landroid/database/Cursor;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "start_time"    # J
    .param p4, "end_time"    # J

    .prologue
    const/4 v2, 0x0

    .line 401
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT SUM(total_step) AS TOTAL_STEP,  SUM(distance) AS TOTAL_DISTANCE,  SUM(calorie) AS TOTAL_CALORIE  FROM (SELECT * FROM walk_info WHERE start_time >= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND start_time < "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " GROUP BY start_time HAVING(MAX(total_step)))"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 409
    .local v3, "selectionClause":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 410
    .local v6, "cursor":Landroid/database/Cursor;
    return-object v6
.end method

.method public getMergedCalorieGetEarlier(JJ)F
    .locals 7
    .param p1, "startTime"    # J
    .param p3, "endTime"    # J

    .prologue
    .line 67
    const/4 v5, 0x2

    const/4 v6, -0x1

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->getMergedCalorie(JJII)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public getMergedCalorieGetEarlier([Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;Ljava/util/LinkedList;)F
    .locals 12
    .param p1, "exercise"    # [Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;",
            ">;)F"
        }
    .end annotation

    .prologue
    .line 209
    .local p2, "selectedList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;>;"
    const/4 v7, 0x0

    .line 210
    .local v7, "totalCalorie":F
    new-instance v6, Ljava/util/LinkedList;

    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    .line 212
    .local v6, "timeList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;>;"
    array-length v8, p1

    if-gtz v8, :cond_0

    .line 213
    const/4 v8, 0x0

    .line 254
    :goto_0
    return v8

    .line 215
    :cond_0
    move-object v0, p1

    .local v0, "arr$":[Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v1, v0, v2

    .line 217
    .local v1, "e":Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;
    new-instance v3, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;

    iget-wide v8, v1, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;->start:J

    const/4 v10, 0x1

    invoke-direct {v3, v8, v9, v1, v10}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;-><init>(JLcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;Z)V

    .line 218
    .local v3, "itemS":Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;
    invoke-virtual {v6, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 215
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 224
    .end local v1    # "e":Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;
    .end local v3    # "itemS":Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;
    :cond_1
    invoke-static {v6}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 226
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;

    iget-object v1, v8, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;->exer:Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    .line 227
    .restart local v1    # "e":Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;

    iget-object v8, v8, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;->exer:Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    iget v8, v8, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;->calorie:F

    float-to-double v8, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    double-to-float v7, v8

    .line 229
    iget v8, v1, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;->calorie:F

    const/4 v9, 0x0

    cmpl-float v8, v8, v9

    if-lez v8, :cond_2

    .line 231
    if-eqz p2, :cond_2

    .line 233
    invoke-virtual {p2, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 237
    :cond_2
    invoke-virtual {v6}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;

    .line 239
    .local v5, "t":Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;
    iget-object v8, v5, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;->exer:Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    iget-wide v8, v8, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;->start:J

    iget-object v10, v5, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;->exer:Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    iget-wide v10, v10, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;->end:J

    cmp-long v8, v8, v10

    if-gez v8, :cond_4

    iget-wide v8, v5, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;->time:J

    iget-wide v10, v1, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;->end:J

    cmp-long v8, v8, v10

    if-ltz v8, :cond_4

    iget-object v8, v5, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;->exer:Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    iget v8, v8, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;->calorie:F

    const/4 v9, 0x0

    cmpl-float v8, v8, v9

    if-lez v8, :cond_4

    .line 241
    float-to-double v8, v7

    iget-object v10, v5, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;->exer:Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    iget v10, v10, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;->calorie:F

    float-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->floor(D)D

    move-result-wide v10

    add-double/2addr v8, v10

    double-to-float v7, v8

    .line 242
    iget-object v1, v5, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;->exer:Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    .line 243
    if-eqz p2, :cond_3

    .line 245
    invoke-virtual {p2, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 248
    :cond_3
    sget-object v8, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Selected Exercise StartTime : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, v5, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;->time:J

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " EndTime : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, v1, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;->end:J

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " currentItemCalorie : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v5, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;->exer:Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    iget v10, v10, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;->calorie:F

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    :cond_4
    sget-object v8, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "TotalCalorie : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .end local v5    # "t":Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;
    :cond_5
    move v8, v7

    .line 254
    goto/16 :goto_0
.end method

.method public getMergedCalorieGetEarlierWithPedometer(JJ)F
    .locals 7
    .param p1, "startTime"    # J
    .param p3, "endTime"    # J

    .prologue
    .line 72
    const/4 v5, 0x3

    const/4 v6, -0x1

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->getMergedCalorie(JJII)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public getMergedCalorieGetEarlierWithPedometer(JJI)F
    .locals 7
    .param p1, "startTime"    # J
    .param p3, "endTime"    # J
    .param p5, "deviceType"    # I

    .prologue
    .line 77
    const/4 v5, 0x3

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->getMergedCalorie(JJII)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public getMergedCalorieGetEarlierWithPedometer([Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;JJ)F
    .locals 14
    .param p1, "exercise"    # [Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;
    .param p2, "startTime"    # J
    .param p4, "endTime"    # J

    .prologue
    .line 259
    const/4 v13, 0x0

    .line 260
    .local v13, "totlaCalorie":F
    const/4 v11, 0x0

    .line 261
    .local v11, "selectedList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;>;"
    const-string v12, ""

    .line 262
    .local v12, "timeCondition":Ljava/lang/String;
    const/4 v8, 0x0

    .line 264
    .local v8, "cursor":Landroid/database/Cursor;
    sget-object v2, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->TAG:Ljava/lang/String;

    const-string v3, "getMergedCalorieGetEarlierWithPedometer "

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    array-length v2, p1

    if-lez v2, :cond_1

    .line 267
    new-instance v11, Ljava/util/LinkedList;

    .end local v11    # "selectedList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;>;"
    invoke-direct {v11}, Ljava/util/LinkedList;-><init>()V

    .line 268
    .restart local v11    # "selectedList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;>;"
    invoke-virtual {p0, p1, v11}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->getMergedCalorieGetEarlier([Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;Ljava/util/LinkedList;)F

    move-result v13

    .line 269
    invoke-virtual {v11}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    .line 273
    .local v9, "e":Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND (start_time < "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, v9, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;->start:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " OR start_time > "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, v9, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;->end:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 274
    goto :goto_0

    .line 275
    .end local v9    # "e":Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;
    :cond_0
    sget-object v2, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getMergedCalorieGetEarlierWithPedometer timeCondition : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    .end local v10    # "i$":Ljava/util/Iterator;
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SELECT  SUM(calorie) AS TOTAL_CALORIE  FROM (SELECT * FROM walk_info WHERE (start_time >= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND start_time < "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p4

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " GROUP BY start_time HAVING(MAX(total_step)))"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 286
    .local v5, "selectionClause":Ljava/lang/String;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 287
    if-eqz v8, :cond_2

    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 289
    const-string/jumbo v2, "total_calorie"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    long-to-float v2, v2

    add-float/2addr v13, v2

    .line 298
    :cond_2
    if-eqz v8, :cond_3

    .line 299
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 301
    :cond_3
    :goto_1
    return v13

    .line 292
    :catch_0
    move-exception v9

    .line 294
    .local v9, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v2, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v9}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 298
    if-eqz v8, :cond_3

    .line 299
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 298
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v8, :cond_4

    .line 299
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v2
.end method

.method public getMergedCalorieGetEarlierWithPedometer([Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;JJI)F
    .locals 14
    .param p1, "exercise"    # [Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;
    .param p2, "startTime"    # J
    .param p4, "endTime"    # J
    .param p6, "deviceType"    # I

    .prologue
    .line 306
    const/4 v13, 0x0

    .line 307
    .local v13, "totlaCalorie":F
    const/4 v11, 0x0

    .line 308
    .local v11, "selectedList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;>;"
    const-string v12, ""

    .line 309
    .local v12, "timeCondition":Ljava/lang/String;
    const/4 v8, 0x0

    .line 311
    .local v8, "cursor":Landroid/database/Cursor;
    sget-object v2, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->TAG:Ljava/lang/String;

    const-string v3, "getMergedCalorieGetEarlierWithPedometer "

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    array-length v2, p1

    if-lez v2, :cond_1

    .line 314
    new-instance v11, Ljava/util/LinkedList;

    .end local v11    # "selectedList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;>;"
    invoke-direct {v11}, Ljava/util/LinkedList;-><init>()V

    .line 315
    .restart local v11    # "selectedList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;>;"
    invoke-virtual {p0, p1, v11}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->getMergedCalorieGetEarlier([Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;Ljava/util/LinkedList;)F

    move-result v13

    .line 316
    invoke-virtual {v11}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    .line 320
    .local v9, "e":Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND (start_time < "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, v9, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;->start:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " OR start_time > "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, v9, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;->end:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 321
    goto :goto_0

    .line 322
    .end local v9    # "e":Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;
    :cond_0
    sget-object v2, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getMergedCalorieGetEarlierWithPedometer timeCondition : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    .end local v10    # "i$":Ljava/util/Iterator;
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SELECT  SUM(calorie) AS TOTAL_CALORIE  FROM walk_info, user_device WHERE (start_time >= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND start_time < "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p4

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND device_type = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p6

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND user_device__id = user_device._id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 334
    .local v5, "selectionClause":Ljava/lang/String;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 335
    if-eqz v8, :cond_2

    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 337
    const-string/jumbo v2, "total_calorie"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    long-to-float v2, v2

    add-float/2addr v13, v2

    .line 346
    :cond_2
    if-eqz v8, :cond_3

    .line 347
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 349
    :cond_3
    :goto_1
    return v13

    .line 340
    :catch_0
    move-exception v9

    .line 342
    .local v9, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v2, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v9}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 346
    if-eqz v8, :cond_3

    .line 347
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 346
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v8, :cond_4

    .line 347
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v2
.end method

.method public getMergedCalorieGetMax(JJ)F
    .locals 7
    .param p1, "startTime"    # J
    .param p3, "endTime"    # J

    .prologue
    .line 62
    const/4 v5, 0x1

    const/4 v6, -0x1

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->getMergedCalorie(JJII)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public getMergedCalorieGetMax([Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;)F
    .locals 14
    .param p1, "exercise"    # [Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;

    .prologue
    const/4 v13, 0x0

    .line 174
    const/4 v9, 0x0

    .line 175
    .local v9, "totalCalorie":F
    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V

    .line 176
    .local v8, "timeList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;>;"
    new-instance v6, Ljava/util/LinkedList;

    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    .line 178
    .local v6, "pool":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;>;"
    move-object v0, p1

    .local v0, "arr$":[Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v1, v0, v2

    .line 180
    .local v1, "e":Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;
    new-instance v4, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;

    iget-wide v10, v1, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;->start:J

    const/4 v12, 0x1

    invoke-direct {v4, v10, v11, v1, v12}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;-><init>(JLcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;Z)V

    .line 181
    .local v4, "itemS":Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;
    new-instance v3, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;

    iget-wide v10, v1, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;->end:J

    invoke-direct {v3, v10, v11, v1, v13}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;-><init>(JLcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;Z)V

    .line 183
    .local v3, "itemE":Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;
    invoke-virtual {v8, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 184
    invoke-virtual {v8, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 178
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 191
    .end local v1    # "e":Lcom/sec/android/app/shealth/common/utils/MergeCalorie$Exercise;
    .end local v3    # "itemE":Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;
    .end local v4    # "itemS":Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;
    :cond_0
    invoke-static {v8}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 198
    invoke-virtual {v8, v13}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;

    iget-wide v10, v10, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;->time:J

    iput-wide v10, p0, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->mPrevTime:J

    .line 199
    invoke-virtual {v8}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;

    .line 201
    .local v7, "t":Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;
    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->compareCalorie(Ljava/util/LinkedList;Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;)F

    move-result v9

    .line 202
    goto :goto_1

    .line 204
    .end local v7    # "t":Lcom/sec/android/app/shealth/common/utils/MergeCalorie$TimeItem;
    :cond_1
    return v9
.end method

.method public getStepCountByDevice(Landroid/content/Context;JJI)Landroid/database/Cursor;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "start_time"    # J
    .param p4, "end_time"    # J
    .param p6, "deviceType"    # I

    .prologue
    const/4 v2, 0x0

    .line 417
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT SUM(total_step) AS TOTAL_STEP,  SUM(distance) AS TOTAL_DISTANCE,  SUM(calorie) AS TOTAL_CALORIE  FROM walk_info, user_device WHERE start_time >= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND start_time < "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND device_type = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND user_device__id = user_device._id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 426
    .local v3, "selectionClause":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 427
    .local v6, "cursor":Landroid/database/Cursor;
    return-object v6
.end method
