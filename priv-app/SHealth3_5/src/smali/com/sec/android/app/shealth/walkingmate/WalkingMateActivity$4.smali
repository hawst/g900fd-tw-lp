.class Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$4;
.super Ljava/lang/Object;
.source "WalkingMateActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->customizeActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)V
    .locals 0

    .prologue
    .line 703
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 707
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth.walkingmate"

    const-string v3, "W002"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    const-class v2, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 711
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->SCANNING_TYPE_INFO_TEXT_RES_ID_KEY:Ljava/lang/String;

    const v2, 0x7f090bd7

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 713
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->HEADER_TEXT_ID:Ljava/lang/String;

    const v2, 0x7f090bd2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 714
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->NO_DEVICES_TEXT_RES_ID_KEY:Ljava/lang/String;

    const v2, 0x7f090092

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 715
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->DATA_TYPE_KEY:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 717
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$4;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->ACCESSARY_ADD_REQ_CODE:I
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->access$600()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 718
    return-void
.end method
