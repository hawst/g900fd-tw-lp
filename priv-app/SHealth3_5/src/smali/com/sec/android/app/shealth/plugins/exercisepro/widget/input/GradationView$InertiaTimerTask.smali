.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;
.super Ljava/util/TimerTask;
.source "GradationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InertiaTimerTask"
.end annotation


# static fields
.field private static final MIN_INERTION_SPEED:F = 2.0f

.field private static final SLOW_RATIO:F = 0.88f

.field private static final START_SPEED_RATIO:F = 1.0f


# instance fields
.field private mDistanceCounter:F

.field private mSpeed:F

.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;F)V
    .locals 1
    .param p2, "startSpeed"    # F

    .prologue
    .line 905
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 907
    const/high16 v0, 0x3f800000    # 1.0f

    mul-float/2addr v0, p2

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->mSpeed:F

    .line 909
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;

    .prologue
    .line 881
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->scroll()V

    return-void
.end method

.method private scroll()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 937
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->mDistanceCounter:F

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->mSpeed:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->mDistanceCounter:F

    .line 939
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->mSpeed:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mIsInertia:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 941
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->mDistanceCounter:F

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    iget v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mDistanceToRedrawGradationView:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 943
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mShouldRedrawGradationView:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->access$202(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;Z)Z

    .line 945
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->mSpeed:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    .line 947
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->getValue()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->setValue(I)V

    .line 955
    :goto_0
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->mDistanceCounter:F

    .line 978
    :goto_1
    return-void

    .line 951
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->getValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->setValue(I)V

    goto :goto_0

    .line 961
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->mSpeed:F

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->moveGradationView(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;I)V

    .line 963
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->invalidate()V

    .line 965
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->changeValue()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;)V

    .line 967
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->mSpeed:F

    const v1, 0x3f6147ae    # 0.88f

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->mSpeed:F

    goto :goto_1

    .line 971
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->stopInertia()V

    .line 972
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->setControllerToNearestDiscreteValue()V

    .line 973
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mOnControllerTapListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnControllerTapListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnControllerTapListener;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnControllerTapListener;->onControllerTapped(Z)V

    .line 975
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->getValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->setValue(I)V

    goto :goto_1
.end method

.method private setControllerToNearestDiscreteValue()V
    .locals 7

    .prologue
    .line 984
    const/16 v3, 0x2710

    .line 986
    .local v3, "minDistance":I
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    iget-object v5, v5, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;

    .line 988
    .local v4, "object":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;->getOffsetMovement()I

    move-result v1

    .line 990
    .local v1, "changedMovement":I
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v5

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 992
    .local v0, "absMinMovement":I
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v5

    if-ne v0, v5, :cond_0

    .line 994
    move v3, v1

    goto :goto_0

    .line 1000
    .end local v0    # "absMinMovement":I
    .end local v1    # "changedMovement":I
    .end local v4    # "object":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationObject;
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    neg-int v6, v3

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->moveGradationView(I)V
    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;I)V

    .line 1002
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->invalidate()V

    .line 1004
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 917
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$InertiaTimerTask;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->post(Ljava/lang/Runnable;)Z

    .line 931
    return-void
.end method
