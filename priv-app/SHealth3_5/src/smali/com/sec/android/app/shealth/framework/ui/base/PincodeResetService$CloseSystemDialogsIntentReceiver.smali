.class Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService$CloseSystemDialogsIntentReceiver;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CloseSystemDialogsIntentReceiver"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService$CloseSystemDialogsIntentReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 38

    :try_start_0
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x6

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/16 v8, -0x29

    aput v8, v2, v7

    const/16 v7, -0x7b

    aput v7, v2, v6

    const/16 v6, -0x5a

    aput v6, v2, v5

    const/16 v5, 0x212

    aput v5, v2, v4

    const/16 v4, 0xc67

    aput v4, v2, v3

    const/16 v3, 0x527e

    aput v3, v2, v1

    const/4 v1, 0x6

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/16 v9, -0x47

    aput v9, v1, v8

    const/16 v8, -0x16

    aput v8, v1, v7

    const/16 v7, -0x2b

    aput v7, v1, v6

    const/16 v6, 0x273

    aput v6, v1, v5

    const/16 v5, 0xc02

    aput v5, v1, v4

    const/16 v4, 0x520c

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :try_start_1
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    const/4 v1, 0x7

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/16 v10, 0x7b1a

    aput v10, v2, v9

    const/16 v9, 0x391e

    aput v9, v2, v8

    const/16 v8, -0x4aae

    aput v8, v2, v7

    const/16 v7, -0x30

    aput v7, v2, v6

    const/16 v6, -0x12

    aput v6, v2, v5

    const/16 v5, -0x5c

    aput v5, v2, v3

    const/16 v3, -0x70bb

    aput v3, v2, v1

    const/4 v1, 0x7

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/16 v11, 0x7b63

    aput v11, v1, v10

    const/16 v10, 0x397b

    aput v10, v1, v9

    const/16 v9, -0x4ac7

    aput v9, v1, v8

    const/16 v8, -0x4b

    aput v8, v1, v7

    const/16 v7, -0x7d

    aput v7, v1, v6

    const/16 v6, -0x35

    aput v6, v1, v5

    const/16 v5, -0x70d3

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0x4a02

    aput v13, v2, v12

    const/16 v12, -0x4fc6

    aput v12, v2, v11

    const/16 v11, -0x40

    aput v11, v2, v10

    const/16 v10, 0x2d7b

    aput v10, v2, v9

    const/16 v9, -0x12a7

    aput v9, v2, v8

    const/16 v8, -0x7d

    aput v8, v2, v7

    const/16 v7, -0x45ae

    aput v7, v2, v6

    const/16 v6, -0x27

    aput v6, v2, v5

    const/4 v5, -0x2

    aput v5, v2, v3

    const/16 v3, -0x7395

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0x4a71

    aput v14, v1, v13

    const/16 v13, -0x4fb6

    aput v13, v1, v12

    const/16 v12, -0x50

    aput v12, v1, v11

    const/16 v11, 0x2d1a

    aput v11, v1, v10

    const/16 v10, -0x12d3

    aput v10, v1, v9

    const/16 v9, -0x13

    aput v9, v1, v8

    const/16 v8, -0x45c9

    aput v8, v1, v7

    const/16 v7, -0x46

    aput v7, v1, v6

    const/16 v6, -0x65

    aput v6, v1, v5

    const/16 v5, -0x73e7

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_6
    const/16 v1, 0x22

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, 0x1e

    const/16 v33, 0x1f

    const/16 v34, 0x20

    const/16 v35, 0x21

    const/16 v36, -0x60

    aput v36, v2, v35

    const/16 v35, -0x4d8a

    aput v35, v2, v34

    const/16 v34, -0x9

    aput v34, v2, v33

    const/16 v33, 0x7c0c

    aput v33, v2, v32

    const/16 v32, -0x71c7

    aput v32, v2, v31

    const/16 v31, -0x24

    aput v31, v2, v30

    const/16 v30, -0x4b

    aput v30, v2, v29

    const/16 v29, -0x678b

    aput v29, v2, v28

    const/16 v28, -0x36

    aput v28, v2, v27

    const/16 v27, 0x7f68

    aput v27, v2, v26

    const/16 v26, 0x5b2c

    aput v26, v2, v25

    const/16 v25, 0x3f0e

    aput v25, v2, v24

    const/16 v24, 0x1411

    aput v24, v2, v23

    const/16 v23, -0x3c86

    aput v23, v2, v22

    const/16 v22, -0x54

    aput v22, v2, v21

    const/16 v21, -0x42

    aput v21, v2, v20

    const/16 v20, -0x13

    aput v20, v2, v19

    const/16 v19, -0xbbf

    aput v19, v2, v18

    const/16 v18, -0x6b

    aput v18, v2, v17

    const/16 v17, -0x77bd

    aput v17, v2, v16

    const/16 v16, -0x4

    aput v16, v2, v15

    const/16 v15, -0x9

    aput v15, v2, v14

    const/16 v14, -0x72

    aput v14, v2, v13

    const/16 v13, -0x5b

    aput v13, v2, v12

    const/16 v12, 0x527c

    aput v12, v2, v11

    const/16 v11, -0x2bc5

    aput v11, v2, v10

    const/4 v10, -0x6

    aput v10, v2, v9

    const/16 v9, -0x44

    aput v9, v2, v8

    const/16 v8, 0xb5b

    aput v8, v2, v7

    const/16 v7, -0xe9c

    aput v7, v2, v6

    const/16 v6, -0x7d

    aput v6, v2, v5

    const/16 v5, 0x5b4b

    aput v5, v2, v4

    const/16 v4, -0x22cb

    aput v4, v2, v3

    const/16 v3, -0x44

    aput v3, v2, v1

    const/16 v1, 0x22

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, -0xc

    aput v37, v1, v36

    const/16 v36, -0x4dc8

    aput v36, v1, v35

    const/16 v35, -0x4e

    aput v35, v1, v34

    const/16 v34, 0x7c5f

    aput v34, v1, v33

    const/16 v33, -0x7184

    aput v33, v1, v32

    const/16 v32, -0x72

    aput v32, v1, v31

    const/16 v31, -0x1b

    aput v31, v1, v30

    const/16 v30, -0x67d6

    aput v30, v1, v29

    const/16 v29, -0x68

    aput v29, v1, v28

    const/16 v28, 0x7f2d

    aput v28, v1, v27

    const/16 v27, 0x5b7f

    aput v27, v1, v26

    const/16 v26, 0x3f5b

    aput v26, v1, v25

    const/16 v25, 0x143f

    aput v25, v1, v24

    const/16 v24, -0x3cec

    aput v24, v1, v23

    const/16 v23, -0x3d

    aput v23, v1, v22

    const/16 v22, -0x29

    aput v22, v1, v21

    const/16 v21, -0x67

    aput v21, v1, v20

    const/16 v20, -0xbde

    aput v20, v1, v19

    const/16 v19, -0xc

    aput v19, v1, v18

    const/16 v18, -0x7793

    aput v18, v1, v17

    const/16 v17, -0x78

    aput v17, v1, v16

    const/16 v16, -0x67

    aput v16, v1, v15

    const/16 v15, -0x15

    aput v15, v1, v14

    const/16 v14, -0x2f

    aput v14, v1, v13

    const/16 v13, 0x5212

    aput v13, v1, v12

    const/16 v12, -0x2bae

    aput v12, v1, v11

    const/16 v11, -0x2c

    aput v11, v1, v10

    const/16 v10, -0x28

    aput v10, v1, v9

    const/16 v9, 0xb32

    aput v9, v1, v8

    const/16 v8, -0xef5

    aput v8, v1, v7

    const/16 v7, -0xf

    aput v7, v1, v6

    const/16 v6, 0x5b2f

    aput v6, v1, v5

    const/16 v5, -0x22a5

    aput v5, v1, v4

    const/16 v4, -0x23

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_7
    array-length v4, v1

    if-lt v3, v4, :cond_8

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_8
    array-length v4, v1

    if-lt v3, v4, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService;->isPasswordServiceReceived:Z

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    move-result-object v4

    const/16 v1, 0x10

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, -0x299e

    aput v19, v2, v18

    const/16 v18, -0x5c

    aput v18, v2, v17

    const/16 v17, -0x1c

    aput v17, v2, v16

    const/16 v16, 0x580a

    aput v16, v2, v15

    const/16 v15, 0x1d2b

    aput v15, v2, v14

    const/16 v14, 0x606e

    aput v14, v2, v13

    const/16 v13, -0xbff

    aput v13, v2, v12

    const/16 v12, -0x7c

    aput v12, v2, v11

    const/16 v11, -0x10

    aput v11, v2, v10

    const/16 v10, -0x56

    aput v10, v2, v9

    const/16 v9, 0x673

    aput v9, v2, v8

    const/16 v8, 0xe63

    aput v8, v2, v7

    const/16 v7, -0x7485

    aput v7, v2, v6

    const/4 v6, -0x6

    aput v6, v2, v5

    const/16 v5, -0x66

    aput v5, v2, v3

    const/16 v3, -0x3de

    aput v3, v2, v1

    const/16 v1, 0x10

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, -0x29fa

    aput v20, v1, v19

    const/16 v19, -0x2a

    aput v19, v1, v18

    const/16 v18, -0x75

    aput v18, v1, v17

    const/16 v17, 0x587d

    aput v17, v1, v16

    const/16 v16, 0x1d58

    aput v16, v1, v15

    const/16 v15, 0x601d

    aput v15, v1, v14

    const/16 v14, -0xba0

    aput v14, v1, v13

    const/16 v13, -0xc

    aput v13, v1, v12

    const/16 v12, -0x51

    aput v12, v1, v11

    const/16 v11, -0x22

    aput v11, v1, v10

    const/16 v10, 0x600

    aput v10, v1, v9

    const/16 v9, 0xe06

    aput v9, v1, v8

    const/16 v8, -0x74f2

    aput v8, v1, v7

    const/16 v7, -0x75

    aput v7, v1, v6

    const/4 v6, -0x1

    aput v6, v1, v5

    const/16 v5, -0x3b0

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_9
    array-length v5, v1

    if-lt v3, v5, :cond_a

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_a
    array-length v5, v1

    if-lt v3, v5, :cond_b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->saveBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;Z)V

    :cond_1
    :goto_b
    return-void

    :cond_2
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_3
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_4
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_5
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_6
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_7
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_8
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :cond_9
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_8

    :cond_a
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    :cond_b
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    :catch_0
    move-exception v1

    goto :goto_b

    :catch_1
    move-exception v1

    goto/16 :goto_6
.end method
