.class Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$4;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$4;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 6

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$4;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$4;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->connectToastReq:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5302(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$4;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->connectedHrmUIName:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$4;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->checkIsHrmSwitchReq:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$4;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$4;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->connectedHRM:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5200(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->leaveDeviceWithOutRefresh(Ljava/lang/String;Z)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5400(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Ljava/lang/String;Z)V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$4;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$4;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_1 .. :try_end_1} :catch_4

    :try_start_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$4;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$2700(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;
    :try_end_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_2 .. :try_end_2} :catch_4

    move-result-object v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_3 .. :try_end_3} :catch_4

    :try_start_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$4;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$2700(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$4;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$4;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v3, v3, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$4;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v4, v4, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$FragmentEventListener;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Z)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->joinWithDevice(Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$ScanningFragmentEventListener;)V
    :try_end_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_4 .. :try_end_4} :catch_4

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0

    :catch_5
    move-exception v0

    goto :goto_0

    :catch_6
    move-exception v0

    goto :goto_0
.end method
