.class public Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "HomeWallpaperChooser.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$GalleryImageAdapter;
    }
.end annotation


# instance fields
.field private WALLPAPER_POSITION:Ljava/lang/String;

.field private backgroundView:Landroid/view/View;

.field private backgrounds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private currentWallpaper:I

.field private isKeyboardEvent:Z

.field private mAdapter:Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$GalleryImageAdapter;

.field private previewGallery:Landroid/widget/Gallery;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 56
    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->backgrounds:Ljava/util/ArrayList;

    .line 58
    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->mAdapter:Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$GalleryImageAdapter;

    .line 60
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->currentWallpaper:I

    .line 61
    const-string v0, "current_wallpaper"

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->WALLPAPER_POSITION:Ljava/lang/String;

    .line 201
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->backgrounds:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->backgroundView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->currentWallpaper:I

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;
    .param p1, "x1"    # I

    .prologue
    .line 52
    iput p1, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->currentWallpaper:I

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->isKeyboardEvent:Z

    return v0
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 245
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 247
    new-instance v0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$3;-><init>(Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;)V

    .line 266
    .local v0, "actionBarButtonListener":Landroid/view/View$OnClickListener;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setHomeButtonVisible(Z)V

    .line 267
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v3

    const-string v4, "#73b90f"

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 269
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v3, 0x7f090048

    invoke-direct {v1, v5, v3, v0, v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;Z)V

    .line 270
    .local v1, "actionButton1":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    new-array v4, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v1, v4, v5

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 272
    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v3, 0x7f09004f

    invoke-direct {v2, v5, v3, v0, v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;Z)V

    .line 273
    .local v2, "actionButton2":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    new-array v4, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v2, v4, v5

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 274
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 169
    invoke-static {p1}, Lcom/sec/android/app/shealth/common/utils/EventKeyUtils;->isExternalKeyEvent2(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->isKeyboardEvent:Z

    .line 177
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0

    .line 175
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->isKeyboardEvent:Z

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 186
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 67
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 68
    const v0, 0x7f030136

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->setContentView(I)V

    .line 70
    if-eqz p1, :cond_0

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->WALLPAPER_POSITION:Ljava/lang/String;

    iget v1, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->currentWallpaper:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->currentWallpaper:I

    .line 74
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->backgrounds:Ljava/util/ArrayList;

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->backgrounds:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0204a9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->backgrounds:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0204aa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->backgrounds:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0204ab

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    const v0, 0x7f080523

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->backgroundView:Landroid/view/View;

    .line 81
    new-instance v0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$GalleryImageAdapter;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->backgrounds:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$GalleryImageAdapter;-><init>(Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->mAdapter:Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$GalleryImageAdapter;

    .line 83
    const v0, 0x7f080524

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Gallery;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->previewGallery:Landroid/widget/Gallery;

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->previewGallery:Landroid/widget/Gallery;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Gallery;->setSpacing(I)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->previewGallery:Landroid/widget/Gallery;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->mAdapter:Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$GalleryImageAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Gallery;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->previewGallery:Landroid/widget/Gallery;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Gallery;->setCallbackDuringFling(Z)V

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->backgroundView:Landroid/view/View;

    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->backgrounds:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->currentWallpaper:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->previewGallery:Landroid/widget/Gallery;

    new-instance v1, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$1;-><init>(Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;)V

    invoke-virtual {v0, v1}, Landroid/widget/Gallery;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->previewGallery:Landroid/widget/Gallery;

    new-instance v1, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$2;-><init>(Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;)V

    invoke-virtual {v0, v1}, Landroid/widget/Gallery;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 160
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 181
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 195
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 196
    if-eqz p1, :cond_0

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->WALLPAPER_POSITION:Ljava/lang/String;

    iget v1, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->currentWallpaper:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 199
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 163
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->isKeyboardEvent:Z

    .line 164
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setGalleryBackgroundFocus(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const v4, 0x7f0a05cf

    .line 188
    if-eqz p1, :cond_0

    .line 189
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070044

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 190
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 192
    :cond_0
    return-void
.end method
