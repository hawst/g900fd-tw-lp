.class Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$ExceptionLoggingThreadFactory;
.super Ljava/lang/Object;
.source "BarcodeScanner.java"

# interfaces
.implements Ljava/util/concurrent/ThreadFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ExceptionLoggingThreadFactory"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 576
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$1;

    .prologue
    .line 576
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$ExceptionLoggingThreadFactory;-><init>()V

    return-void
.end method


# virtual methods
.method public newThread(Ljava/lang/Runnable;)Ljava/lang/Thread;
    .locals 3
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 579
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 580
    .local v0, "thread":Ljava/lang/Thread;
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$ExceptionLoggingUncaughtExceptionHandler;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$ExceptionLoggingUncaughtExceptionHandler;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$1;)V

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 581
    return-object v0
.end method
