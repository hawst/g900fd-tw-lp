.class public Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;
.super Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
.source "HeartrateLogAdapter.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private dateFormatter:Ljava/text/SimpleDateFormat;

.field dateFormatterNew:Ljava/text/SimpleDateFormat;

.field private mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

.field private mTagId:I

.field private mTagName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;Landroid/content/Context;)V
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;-><init>(Landroid/database/Cursor;Landroid/content/Context;)V

    .line 59
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd/MM/yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->dateFormatter:Ljava/text/SimpleDateFormat;

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->mTagName:Ljava/lang/String;

    .line 61
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->mTagId:I

    .line 62
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, " dd"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->dateFormatterNew:Ljava/text/SimpleDateFormat;

    .line 65
    invoke-static {p2}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;Landroid/content/Context;I)V
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "tagId"    # I

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;-><init>(Landroid/database/Cursor;Landroid/content/Context;)V

    .line 59
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "dd/MM/yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->dateFormatter:Ljava/text/SimpleDateFormat;

    .line 60
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->mTagName:Ljava/lang/String;

    .line 61
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->mTagId:I

    .line 62
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, " dd"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->dateFormatterNew:Ljava/text/SimpleDateFormat;

    .line 69
    invoke-static {p2}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    .line 71
    iput p3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->mTagId:I

    .line 72
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    iget v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->mTagId:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getTagByIndex(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v0

    .line 73
    .local v0, "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    if-eqz v0, :cond_0

    .line 74
    iget-object v1, v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mType:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    sget-object v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    if-ne v1, v2, :cond_1

    .line 75
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mNameId:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->mTagName:Ljava/lang/String;

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 77
    :cond_1
    iget-object v1, v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->mTagName:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method protected bindChildView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 43
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "isLastChild"    # Z

    .prologue
    .line 85
    const v39, 0x7f080588

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v25

    check-cast v25, Landroid/widget/LinearLayout;

    .line 86
    .local v25, "llHeader":Landroid/widget/LinearLayout;
    const v39, 0x7f080589

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    check-cast v37, Landroid/widget/TextView;

    .line 87
    .local v37, "tvHeaderLeft":Landroid/widget/TextView;
    const v39, 0x7f08058a

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v38

    check-cast v38, Landroid/widget/TextView;

    .line 88
    .local v38, "tvHeaderRight":Landroid/widget/TextView;
    const v39, 0x7f08058e

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v36

    check-cast v36, Landroid/widget/TextView;

    .line 89
    .local v36, "tvBodyTopLeft":Landroid/widget/TextView;
    const v39, 0x7f0804e9

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/ImageView;

    .line 90
    .local v21, "ivBodyTopLeftTag":Landroid/widget/ImageView;
    const v39, 0x7f08058f

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v34

    check-cast v34, Landroid/widget/TextView;

    .line 91
    .local v34, "tvBodyBottomLeft":Landroid/widget/TextView;
    const v39, 0x7f080591

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/ImageButton;

    .line 92
    .local v19, "ivBodyAccessory":Landroid/widget/ImageButton;
    const v39, 0x7f080592

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/ImageView;

    .line 93
    .local v20, "ivBodyMemo":Landroid/widget/ImageView;
    const v39, 0x7f080593

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v35

    check-cast v35, Landroid/widget/TextView;

    .line 94
    .local v35, "tvBodyRight":Landroid/widget/TextView;
    const v39, 0x7f08058d

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/CheckBox;

    .line 95
    .local v10, "cbBody":Landroid/widget/CheckBox;
    const v39, 0x7f08058b

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v24

    .line 96
    .local v24, "listTopDivider":Landroid/view/View;
    const v39, 0x7f080594

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v23

    .line 97
    .local v23, "listBottomDivider":Landroid/view/View;
    const-string v39, "_id"

    move-object/from16 v0, p3

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v39

    move-object/from16 v0, p3

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 98
    .local v16, "heartrateId":J
    const-string/jumbo v39, "start_time"

    move-object/from16 v0, p3

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v39

    move-object/from16 v0, p3

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v32

    .line 99
    .local v32, "time":J
    const-string v39, "AvgMonth"

    move-object/from16 v0, p3

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v39

    move-object/from16 v0, p3

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 101
    .local v7, "avgMonth":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->mTagName:Ljava/lang/String;

    move-object/from16 v39, v0

    if-eqz v39, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->mTagName:Ljava/lang/String;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Ljava/lang/String;->length()I

    move-result v39

    if-lez v39, :cond_5

    .line 104
    move-object/from16 v0, p2

    move-wide/from16 v1, v32

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getChildFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v12

    .line 105
    .local v12, "currentTime":Ljava/lang/String;
    move-object/from16 v29, v12

    .line 106
    .local v29, "previousRecordTimeStr":Ljava/lang/String;
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->isFirst()Z

    move-result v39

    if-nez v39, :cond_0

    .line 108
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToPrevious()Z

    .line 109
    const-string/jumbo v39, "sample_time"

    move-object/from16 v0, p3

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v39

    move-object/from16 v0, p3

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v27

    .line 110
    .local v27, "previousRecordTime":J
    move-object/from16 v0, p2

    move-wide/from16 v1, v27

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getChildFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v29

    .line 111
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToNext()Z

    .line 115
    .end local v27    # "previousRecordTime":J
    :goto_0
    move-object/from16 v0, v29

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-nez v39, :cond_4

    .line 116
    const/16 v26, 0x0

    .line 117
    .local v26, "offset":I
    move-object/from16 v6, p3

    .line 118
    .local v6, "avgC":Landroid/database/Cursor;
    :goto_1
    const-wide/16 v39, 0x0

    cmp-long v39, v7, v39

    if-nez v39, :cond_1

    .line 119
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 120
    add-int/lit8 v26, v26, -0x1

    .line 121
    const-string v39, "AvgMonth"

    move-object/from16 v0, v39

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v39

    move/from16 v0, v39

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    goto :goto_1

    .line 114
    .end local v6    # "avgC":Landroid/database/Cursor;
    .end local v26    # "offset":I
    :cond_0
    const-string v29, ""

    goto :goto_0

    .line 123
    .restart local v6    # "avgC":Landroid/database/Cursor;
    .restart local v26    # "offset":I
    :cond_1
    move-object/from16 v0, p3

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->move(I)Z

    .line 124
    move-object/from16 v0, p2

    move-wide/from16 v1, v32

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getChildFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v37

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    const/16 v39, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 126
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string v40, " ("

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const v40, 0x7f090f20

    move-object/from16 v0, p2

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, " "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    long-to-float v0, v7

    move/from16 v40, v0

    invoke-static/range {v40 .. v40}, Ljava/lang/Math;->round(F)I

    move-result v40

    invoke-static/range {v40 .. v40}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, " "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const v40, 0x7f0900d2

    move-object/from16 v0, p2

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, ")"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    invoke-virtual/range {v38 .. v39}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    const/16 v39, 0x8

    move-object/from16 v0, v24

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 129
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v9

    .line 130
    .local v9, "cal":Ljava/util/Calendar;
    move-wide/from16 v0, v32

    invoke-virtual {v9, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 131
    new-instance v39, Ljava/text/DateFormatSymbols;

    invoke-direct/range {v39 .. v39}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual/range {v39 .. v39}, Ljava/text/DateFormatSymbols;->getMonths()[Ljava/lang/String;

    move-result-object v39

    const/16 v40, 0x2

    move/from16 v0, v40

    invoke-virtual {v9, v0}, Ljava/util/Calendar;->get(I)I

    move-result v40

    aget-object v13, v39, v40

    .line 132
    .local v13, "dMonth":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->dateFormatterNew:Ljava/text/SimpleDateFormat;

    move-object/from16 v39, v0

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    .line 133
    .local v14, "dateNew":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v39

    move-object/from16 v0, v39

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v22

    .line 134
    .local v22, "language":Ljava/lang/String;
    const-string v39, "ko"

    move-object/from16 v0, v22

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_3

    .line 135
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v40, 0x2

    move/from16 v0, v40

    invoke-virtual {v9, v0}, Ljava/util/Calendar;->get(I)I

    move-result v40

    add-int/lit8 v40, v40, 0x1

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v40

    const v41, 0x7f090212

    invoke-virtual/range {v40 .. v41}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, " "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v40

    const v41, 0x7f090213

    invoke-virtual/range {v40 .. v41}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, " "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    new-instance v40, Ljava/text/DateFormatSymbols;

    invoke-direct/range {v40 .. v40}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual/range {v40 .. v40}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object v40

    const/16 v41, 0x7

    move/from16 v0, v41

    invoke-virtual {v9, v0}, Ljava/util/Calendar;->get(I)I

    move-result v41

    aget-object v40, v40, v41

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v37

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 187
    .end local v6    # "avgC":Landroid/database/Cursor;
    .end local v9    # "cal":Ljava/util/Calendar;
    .end local v12    # "currentTime":Ljava/lang/String;
    .end local v13    # "dMonth":Ljava/lang/String;
    .end local v14    # "dateNew":Ljava/lang/String;
    .end local v22    # "language":Ljava/lang/String;
    .end local v26    # "offset":I
    .end local v29    # "previousRecordTimeStr":Ljava/lang/String;
    :goto_2
    if-eqz p4, :cond_8

    const/16 v39, 0x0

    :goto_3
    move-object/from16 v0, v23

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 189
    const-string v39, "AvgHeartRate"

    move-object/from16 v0, p3

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v39

    move-object/from16 v0, p3

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v15

    .line 190
    .local v15, "heartRate":F
    const-string v39, "comment"

    move-object/from16 v0, p3

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v39

    move-object/from16 v0, p3

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 192
    .local v11, "comment":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getTag(J)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v30

    .line 193
    .local v30, "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    const v18, 0x7f02053d

    .line 194
    .local v18, "iconId":I
    const/16 v31, 0x0

    .line 195
    .local v31, "tagName":Ljava/lang/String;
    if-eqz v30, :cond_2

    .line 196
    move-object/from16 v0, v30

    iget v0, v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mIconId:I

    move/from16 v18, v0

    .line 198
    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mType:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    move-object/from16 v39, v0

    sget-object v40, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    move-object/from16 v0, v39

    move-object/from16 v1, v40

    if-ne v0, v1, :cond_9

    .line 199
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v39

    move-object/from16 v0, v30

    iget v0, v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mNameId:I

    move/from16 v40, v0

    invoke-virtual/range {v39 .. v40}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v31

    .line 204
    :cond_2
    :goto_4
    const/16 v39, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 206
    if-eqz v31, :cond_b

    .line 207
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v15}, Ljava/lang/Math;->round(F)I

    move-result v40

    invoke-static/range {v40 .. v40}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, " "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const v40, 0x7f0900d2

    move-object/from16 v0, p2

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v36

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 208
    const/16 v39, 0x0

    move-object/from16 v0, v34

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 209
    move-object/from16 v0, v34

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    const v39, 0x7f02051c

    move-object/from16 v0, v21

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 211
    if-eqz v18, :cond_a

    const v39, 0x7f02053d

    move/from16 v0, v18

    move/from16 v1, v39

    if-eq v0, v1, :cond_a

    .line 212
    move-object/from16 v0, v21

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 227
    :goto_5
    move-object/from16 v0, p2

    move-wide/from16 v1, v32

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v35

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 229
    if-eqz v11, :cond_c

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v39

    if-lez v39, :cond_c

    .line 230
    const/16 v39, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 231
    sget-object v39, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_USER_CUSTOM:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    const v40, 0x7f09007c

    move-object/from16 v0, p2

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v40

    move-object/from16 v0, v20

    move-object/from16 v1, v39

    move-object/from16 v2, v40

    invoke-static {v0, v1, v2, v11}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    :goto_6
    const-string/jumbo v39, "user_device__id"

    move-object/from16 v0, p3

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v39

    move-object/from16 v0, p3

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 239
    .local v5, "accessory":Ljava/lang/String;
    const/16 v39, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 240
    if-eqz v5, :cond_e

    .line 242
    const-string v39, "10008"

    move-object/from16 v0, v39

    invoke-virtual {v5, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v39

    if-eqz v39, :cond_d

    .line 243
    const/16 v39, 0x4

    move-object/from16 v0, v19

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 252
    :goto_7
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->dateFormatter:Ljava/text/SimpleDateFormat;

    move-object/from16 v40, v0

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, "_"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, "_id"

    move-object/from16 v0, p3

    move-object/from16 v1, v40

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v40

    move-object/from16 v0, p3

    move/from16 v1, v40

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 253
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->dateFormatter:Ljava/text/SimpleDateFormat;

    move-object/from16 v40, v0

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, "_"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, "_id"

    move-object/from16 v0, p3

    move-object/from16 v1, v40

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v40

    move-object/from16 v0, p3

    move/from16 v1, v40

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v10, v0}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 254
    move-object/from16 v0, p0

    invoke-virtual {v10, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 255
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v39

    const v40, 0x106000d

    invoke-virtual/range {v39 .. v40}, Landroid/content/res/Resources;->getColor(I)I

    move-result v39

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 257
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->isMenuDeleteMode()Z

    move-result v39

    if-eqz v39, :cond_11

    .line 258
    const/16 v39, 0x0

    move/from16 v0, v39

    invoke-virtual {v10, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 259
    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    const/16 v42, 0x0

    move-object/from16 v0, v36

    move/from16 v1, v39

    move/from16 v2, v40

    move/from16 v3, v41

    move/from16 v4, v42

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 260
    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    const/16 v42, 0x0

    move-object/from16 v0, v34

    move/from16 v1, v39

    move/from16 v2, v40

    move/from16 v3, v41

    move/from16 v4, v42

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 261
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->isCheckAll()Z

    move-result v39

    if-eqz v39, :cond_f

    .line 262
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-virtual {v10, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 290
    :goto_8
    return-void

    .line 144
    .end local v5    # "accessory":Ljava/lang/String;
    .end local v11    # "comment":Ljava/lang/String;
    .end local v15    # "heartRate":F
    .end local v18    # "iconId":I
    .end local v30    # "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    .end local v31    # "tagName":Ljava/lang/String;
    .restart local v6    # "avgC":Landroid/database/Cursor;
    .restart local v9    # "cal":Ljava/util/Calendar;
    .restart local v12    # "currentTime":Ljava/lang/String;
    .restart local v13    # "dMonth":Ljava/lang/String;
    .restart local v14    # "dateNew":Ljava/lang/String;
    .restart local v22    # "language":Ljava/lang/String;
    .restart local v26    # "offset":I
    .restart local v29    # "previousRecordTimeStr":Ljava/lang/String;
    :cond_3
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v39

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, " "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, " "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    new-instance v40, Ljava/text/DateFormatSymbols;

    invoke-direct/range {v40 .. v40}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual/range {v40 .. v40}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object v40

    const/16 v41, 0x7

    move/from16 v0, v41

    invoke-virtual {v9, v0}, Ljava/util/Calendar;->get(I)I

    move-result v41

    aget-object v40, v40, v41

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v37

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 151
    .end local v6    # "avgC":Landroid/database/Cursor;
    .end local v9    # "cal":Ljava/util/Calendar;
    .end local v13    # "dMonth":Ljava/lang/String;
    .end local v14    # "dateNew":Ljava/lang/String;
    .end local v22    # "language":Ljava/lang/String;
    .end local v26    # "offset":I
    :cond_4
    const/16 v39, 0x8

    move-object/from16 v0, v25

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 152
    const/16 v39, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    .line 157
    .end local v12    # "currentTime":Ljava/lang/String;
    .end local v29    # "previousRecordTimeStr":Ljava/lang/String;
    :cond_5
    const-wide/16 v39, 0x0

    cmp-long v39, v7, v39

    if-eqz v39, :cond_7

    .line 158
    move-object/from16 v0, p2

    move-wide/from16 v1, v32

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getChildFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v37

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string v40, " ("

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const v40, 0x7f090f20

    move-object/from16 v0, p2

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, " "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    long-to-float v0, v7

    move/from16 v40, v0

    invoke-static/range {v40 .. v40}, Ljava/lang/Math;->round(F)I

    move-result v40

    invoke-static/range {v40 .. v40}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, " "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const v40, 0x7f0900d2

    move-object/from16 v0, p2

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, ")"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    invoke-virtual/range {v38 .. v39}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    const/16 v39, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 161
    const/16 v39, 0x8

    move-object/from16 v0, v24

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 163
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v9

    .line 164
    .restart local v9    # "cal":Ljava/util/Calendar;
    move-wide/from16 v0, v32

    invoke-virtual {v9, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 165
    new-instance v39, Ljava/text/DateFormatSymbols;

    invoke-direct/range {v39 .. v39}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual/range {v39 .. v39}, Ljava/text/DateFormatSymbols;->getMonths()[Ljava/lang/String;

    move-result-object v39

    const/16 v40, 0x2

    move/from16 v0, v40

    invoke-virtual {v9, v0}, Ljava/util/Calendar;->get(I)I

    move-result v40

    aget-object v13, v39, v40

    .line 166
    .restart local v13    # "dMonth":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->dateFormatterNew:Ljava/text/SimpleDateFormat;

    move-object/from16 v39, v0

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    .line 167
    .restart local v14    # "dateNew":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v39

    move-object/from16 v0, v39

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v22

    .line 168
    .restart local v22    # "language":Ljava/lang/String;
    const-string v39, "ko"

    move-object/from16 v0, v22

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_6

    .line 169
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v40, 0x2

    move/from16 v0, v40

    invoke-virtual {v9, v0}, Ljava/util/Calendar;->get(I)I

    move-result v40

    add-int/lit8 v40, v40, 0x1

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v40

    const v41, 0x7f090212

    invoke-virtual/range {v40 .. v41}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, " "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v40

    const v41, 0x7f090213

    invoke-virtual/range {v40 .. v41}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, " "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    new-instance v40, Ljava/text/DateFormatSymbols;

    invoke-direct/range {v40 .. v40}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual/range {v40 .. v40}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object v40

    const/16 v41, 0x7

    move/from16 v0, v41

    invoke-virtual {v9, v0}, Ljava/util/Calendar;->get(I)I

    move-result v41

    aget-object v40, v40, v41

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v37

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 178
    :cond_6
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v39

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, " "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, " "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    new-instance v40, Ljava/text/DateFormatSymbols;

    invoke-direct/range {v40 .. v40}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual/range {v40 .. v40}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object v40

    const/16 v41, 0x7

    move/from16 v0, v41

    invoke-virtual {v9, v0}, Ljava/util/Calendar;->get(I)I

    move-result v41

    aget-object v40, v40, v41

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v37

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 182
    .end local v9    # "cal":Ljava/util/Calendar;
    .end local v13    # "dMonth":Ljava/lang/String;
    .end local v14    # "dateNew":Ljava/lang/String;
    .end local v22    # "language":Ljava/lang/String;
    :cond_7
    const/16 v39, 0x8

    move-object/from16 v0, v25

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 183
    const/16 v39, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    .line 187
    :cond_8
    const/16 v39, 0x8

    goto/16 :goto_3

    .line 201
    .restart local v11    # "comment":Ljava/lang/String;
    .restart local v15    # "heartRate":F
    .restart local v18    # "iconId":I
    .restart local v30    # "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    .restart local v31    # "tagName":Ljava/lang/String;
    :cond_9
    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    move-object/from16 v31, v0

    goto/16 :goto_4

    .line 215
    :cond_a
    const v39, 0x7f02051d

    move-object/from16 v0, v21

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    .line 220
    :cond_b
    const/16 v39, 0x0

    move-object/from16 v0, v34

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 221
    const v39, 0x7f090c0d

    move-object/from16 v0, v34

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 223
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v15}, Ljava/lang/Math;->round(F)I

    move-result v40

    invoke-static/range {v40 .. v40}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, " "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const v40, 0x7f0900d2

    move-object/from16 v0, p2

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v36

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 224
    const v39, 0x7f02051c

    move-object/from16 v0, v21

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 225
    const v39, 0x7f02051d

    move-object/from16 v0, v21

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    .line 233
    :cond_c
    const/16 v39, 0x8

    move-object/from16 v0, v20

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_6

    .line 245
    .restart local v5    # "accessory":Ljava/lang/String;
    :cond_d
    const/16 v39, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 246
    const-string v39, "custom_name"

    move-object/from16 v0, p3

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v39

    move-object/from16 v0, p3

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v19

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_7

    .line 249
    :cond_e
    const/16 v39, 0x4

    move-object/from16 v0, v19

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_7

    .line 263
    :cond_f
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->getCheckedItems()Ljava/util/ArrayList;

    move-result-object v39

    invoke-virtual {v10}, Landroid/widget/CheckBox;->getTag()Ljava/lang/Object;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_10

    .line 264
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-virtual {v10, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_8

    .line 266
    :cond_10
    const/16 v39, 0x0

    move/from16 v0, v39

    invoke-virtual {v10, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_8

    .line 268
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->isDeleteMode()Z

    move-result v39

    if-eqz v39, :cond_14

    .line 269
    const/16 v39, 0x0

    move/from16 v0, v39

    invoke-virtual {v10, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 270
    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    const/16 v42, 0x0

    move-object/from16 v0, v36

    move/from16 v1, v39

    move/from16 v2, v40

    move/from16 v3, v41

    move/from16 v4, v42

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 271
    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    const/16 v42, 0x0

    move-object/from16 v0, v34

    move/from16 v1, v39

    move/from16 v2, v40

    move/from16 v3, v41

    move/from16 v4, v42

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 272
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->isCheckAll()Z

    move-result v39

    if-eqz v39, :cond_12

    .line 273
    const v39, 0x7f02086c

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_8

    .line 274
    :cond_12
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->getCheckedItems()Ljava/util/ArrayList;

    move-result-object v39

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_13

    .line 275
    const v39, 0x7f02086c

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_8

    .line 277
    :cond_13
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v39

    const v40, 0x106000d

    invoke-virtual/range {v39 .. v40}, Landroid/content/res/Resources;->getColor(I)I

    move-result v39

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_8

    .line 283
    :cond_14
    const/16 v39, 0x8

    move/from16 v0, v39

    invoke-virtual {v10, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 284
    const/16 v39, 0x0

    move/from16 v0, v39

    invoke-virtual {v10, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 285
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->getCheckedItems()Ljava/util/ArrayList;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_8
.end method

.method protected bindGroupView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 10
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "isExpanded"    # Z

    .prologue
    const v9, 0x7f0900d2

    .line 294
    const v7, 0x7f080583

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 295
    .local v4, "tvFirst":Landroid/widget/TextView;
    const v7, 0x7f080584

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 296
    .local v5, "tvSecond":Landroid/widget/TextView;
    const v7, 0x7f080586

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 299
    .local v1, "ivExpand":Landroid/widget/ImageView;
    const-string/jumbo v7, "start_time"

    invoke-interface {p3, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {p3, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 303
    .local v2, "time":J
    invoke-static {p2, v2, v3}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getYearShortMonthText(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    .line 304
    .local v6, "yearMonth":Ljava/lang/String;
    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 305
    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 307
    const-string v7, "AvgMonth"

    invoke-interface {p3, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {p3, v7}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    .line 308
    .local v0, "avgMonth":F
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const v8, 0x7f090ae0

    invoke-virtual {p2, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p2, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 310
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const v8, 0x7f090b9a

    invoke-virtual {p2, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p2, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09021c

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    if-eqz p4, :cond_0

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v9, 0x7f0901ef

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    :goto_0
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 322
    if-eqz p4, :cond_1

    .line 323
    const v7, 0x7f020845

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 324
    sget-object v7, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090b6d

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, ""

    invoke-static {v1, v7, v8, v9}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    :goto_1
    invoke-virtual {v1, p4}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 336
    return-void

    .line 310
    :cond_0
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v9, 0x7f090217

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 329
    :cond_1
    const v7, 0x7f02084c

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 330
    sget-object v7, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090b6e

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, ""

    invoke-static {v1, v7, v8, v9}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected getChildrenCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 2
    .param p1, "groupCursor"    # Landroid/database/Cursor;

    .prologue
    .line 351
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->mTagId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 352
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->mTagId:I

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getChildrenCursorByTag(Landroid/database/Cursor;I)Landroid/database/Cursor;

    move-result-object v0

    .line 354
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getChildrenCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method

.method protected getColumnNameForCreateTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 376
    const-string/jumbo v0, "start_time"

    return-object v0
.end method

.method protected getColumnNameForID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 371
    const-string v0, "_id"

    return-object v0
.end method

.method protected getTotalChildCount()I
    .locals 4

    .prologue
    .line 359
    const/4 v0, 0x0

    .line 360
    .local v0, "childCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->getGroupCount()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 361
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->getChildrenCount(I)I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 362
    add-int/lit8 v0, v0, 0x1

    .line 361
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 360
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 366
    .end local v2    # "j":I
    :cond_1
    return v0
.end method

.method protected newChildView(Landroid/content/Context;Landroid/database/Cursor;ZLandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "isLastChild"    # Z
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 340
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030149

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected newGroupView(Landroid/content/Context;Landroid/database/Cursor;ZLandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "isExpanded"    # Z
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 345
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030148

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p1, "button"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 392
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter$1;-><init>(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;Landroid/widget/CompoundButton;Z)V

    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->post(Ljava/lang/Runnable;)Z

    .line 398
    return-void
.end method

.method protected setLogSelected(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "isSelect"    # Z

    .prologue
    .line 382
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->mTagName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->mTagName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 383
    const-string v0, "HeartrateLogAdapter"

    const-string/jumbo v1, "view by tag mode"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    :goto_0
    return-void

    .line 386
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->setLogSelected(Ljava/lang/String;Z)V

    goto :goto_0
.end method
