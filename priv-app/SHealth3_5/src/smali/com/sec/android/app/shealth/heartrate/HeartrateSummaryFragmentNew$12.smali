.class Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$12;
.super Ljava/lang/Object;
.source "HeartrateSummaryFragmentNew.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->measureFinishAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V
    .locals 0

    .prologue
    .line 1221
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$12;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v11, 0x0

    const/16 v6, 0x8

    const/high16 v2, 0x3f000000    # 0.5f

    const/4 v5, 0x1

    .line 1224
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$12;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondIcon:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$2300(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/RelativeLayout;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1225
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$12;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$1500(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1226
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$12;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterPulseLayout:Landroid/widget/FrameLayout;
    invoke-static {v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$3500(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/FrameLayout;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1227
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$12;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterIconBig:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$3600(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/ImageView;

    move-result-object v3

    const/4 v6, 0x4

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1229
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    move v3, v1

    move v4, v2

    move v6, v2

    move v7, v5

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 1231
    .local v0, "scaleAnimation":Landroid/view/animation/Animation;
    const-wide/16 v1, 0x14d

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1233
    new-instance v4, Landroid/view/animation/TranslateAnimation;

    const v8, 0x3be56042    # 0.007f

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->HRM_FINISH_ANIMATION_Y_AXIS:F
    invoke-static {}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$3700()F

    move-result v12

    move v6, v11

    move v7, v5

    move v9, v5

    move v10, v11

    move v11, v5

    invoke-direct/range {v4 .. v12}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 1236
    .local v4, "translateAnimation":Landroid/view/animation/Animation;
    const-wide/16 v1, 0x14d

    invoke-virtual {v4, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1238
    new-instance v13, Landroid/view/animation/AnimationSet;

    invoke-direct {v13, v5}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 1239
    .local v13, "animationSet":Landroid/view/animation/AnimationSet;
    invoke-virtual {v13, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1240
    invoke-virtual {v13, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1241
    const/4 v1, 0x0

    invoke-virtual {v13, v1}, Landroid/view/animation/AnimationSet;->setFillAfter(Z)V

    .line 1242
    invoke-virtual {v13, v5}, Landroid/view/animation/AnimationSet;->setFillBefore(Z)V

    .line 1243
    new-instance v1, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$12$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$12$1;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$12;)V

    invoke-virtual {v13, v1}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1267
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$12;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterIconBig:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$3600(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v13}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1268
    return-void
.end method
