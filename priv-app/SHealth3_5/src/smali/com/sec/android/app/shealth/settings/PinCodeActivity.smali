.class public Lcom/sec/android/app/shealth/settings/PinCodeActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "PinCodeActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$IRestoreCompleteListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/PinCodeActivity$SipAndRecentKeyReceiver;
    }
.end annotation


# static fields
.field private static final ACTION_CLOSE_SYSTEM_DIALOGS_REASON:Ljava/lang/String; = "reason"

.field private static final IS_VISIBLE_WINDOW:Ljava/lang/String; = "AxT9IME.isVisibleWindow"

.field private static MAX_LENGTH:I = 0x0

.field private static MINIMUM_LENGTH:I = 0x0

.field private static NOT_ENTERED:I = 0x0

.field private static final RECENT_KEY:Ljava/lang/String; = "recentapps"

.field private static final RESPONSE_AXT9INFO:Ljava/lang/String; = "ResponseAxT9Info"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private OldPassword:Ljava/lang/String;

.field private final ServiceName:Ljava/lang/String;

.field private isFirst:Z

.field private mAppID:Ljava/lang/String;

.field private mCancelBtn:Landroid/widget/TextView;

.field private final mContainsLetterPattern:Ljava/util/regex/Pattern;

.field private mContinueButton:Landroid/widget/TextView;

.field private mHandler:Landroid/os/Handler;

.field mIsChangePassword:Z

.field mIsInitiatePasswordChange:Z

.field private mIsKeyBoardShown:Z

.field private mIsPasswordOK:Z

.field private mIsRecentKeyPressed:Z

.field mIsSecurityDisable:Z

.field private mKeyBoardStatus:Z

.field private mOutStateBundle:Landroid/os/Bundle;

.field private mPinEditText:Landroid/widget/EditText;

.field private mPinString:Ljava/lang/String;

.field private mPinTitle:Landroid/widget/TextView;

.field private mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

.field private mSipAndRecentKeyReceiver:Lcom/sec/android/app/shealth/settings/PinCodeActivity$SipAndRecentKeyReceiver;

.field private mSipIntentFilter:Landroid/content/IntentFilter;

.field private mWrongPasswordcount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    const-class v0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->TAG:Ljava/lang/String;

    .line 78
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->NOT_ENTERED:I

    .line 79
    const/4 v0, 0x4

    sput v0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->MINIMUM_LENGTH:I

    .line 80
    const/16 v0, 0x10

    sput v0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->MAX_LENGTH:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 69
    const-string v0, "[a-zA-Z]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mContainsLetterPattern:Ljava/util/regex/Pattern;

    .line 85
    iput v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mWrongPasswordcount:I

    .line 86
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mIsPasswordOK:Z

    .line 87
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mIsKeyBoardShown:Z

    .line 88
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mKeyBoardStatus:Z

    .line 89
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mIsRecentKeyPressed:Z

    .line 91
    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mOutStateBundle:Landroid/os/Bundle;

    .line 92
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mHandler:Landroid/os/Handler;

    .line 102
    const-string v0, "com.sec.android.app.shealth"

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mAppID:Ljava/lang/String;

    .line 104
    const-string v0, "KeyManager : "

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->ServiceName:Ljava/lang/String;

    .line 105
    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->OldPassword:Ljava/lang/String;

    .line 106
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->isFirst:Z

    .line 194
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/settings/PinCodeActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mContinueButton:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/settings/PinCodeActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->setPinContinueMethod()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/settings/PinCodeActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/PinCodeActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->setButtonStyle(Z)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/settings/PinCodeActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->refreshFocusables()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/settings/PinCodeActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mIsRecentKeyPressed:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/settings/PinCodeActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/PinCodeActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mIsRecentKeyPressed:Z

    return p1
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/settings/PinCodeActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/PinCodeActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mIsKeyBoardShown:Z

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/settings/PinCodeActivity;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mOutStateBundle:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/settings/PinCodeActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mKeyBoardStatus:Z

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/shealth/settings/PinCodeActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/PinCodeActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mKeyBoardStatus:Z

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/settings/PinCodeActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/settings/PinCodeActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/settings/PinCodeActivity;Landroid/text/Editable;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/PinCodeActivity;
    .param p1, "x1"    # Landroid/text/Editable;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->updateTitleAndContinueButtonState(Landroid/text/Editable;)V

    return-void
.end method

.method private registerSipAndRecentKeyReceiver()V
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mSipAndRecentKeyReceiver:Lcom/sec/android/app/shealth/settings/PinCodeActivity$SipAndRecentKeyReceiver;

    if-nez v0, :cond_0

    .line 227
    new-instance v0, Lcom/sec/android/app/shealth/settings/PinCodeActivity$SipAndRecentKeyReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/settings/PinCodeActivity$SipAndRecentKeyReceiver;-><init>(Lcom/sec/android/app/shealth/settings/PinCodeActivity;Lcom/sec/android/app/shealth/settings/PinCodeActivity$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mSipAndRecentKeyReceiver:Lcom/sec/android/app/shealth/settings/PinCodeActivity$SipAndRecentKeyReceiver;

    .line 228
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mSipIntentFilter:Landroid/content/IntentFilter;

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mSipIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "ResponseAxT9Info"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mSipIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mSipAndRecentKeyReceiver:Lcom/sec/android/app/shealth/settings/PinCodeActivity$SipAndRecentKeyReceiver;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mSipIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 234
    :cond_0
    return-void
.end method

.method private setButtonStyle(Z)V
    .locals 3
    .param p1, "flag"    # Z

    .prologue
    const v2, 0x7f070082

    .line 397
    if-eqz p1, :cond_0

    .line 399
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mContinueButton:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 400
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mContinueButton:Landroid/widget/TextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 401
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mContinueButton:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 409
    :goto_0
    return-void

    .line 405
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mContinueButton:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 406
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mContinueButton:Landroid/widget/TextView;

    const v1, 0x3ecccccd    # 0.4f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 407
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mContinueButton:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0
.end method

.method private setListenersAccordingToAction()V
    .locals 2

    .prologue
    .line 360
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/shealth/settings/PinCodeActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity$3;-><init>(Lcom/sec/android/app/shealth/settings/PinCodeActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 386
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mContinueButton:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 387
    return-void
.end method

.method private setPasswordOK(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 391
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mIsPasswordOK:Z

    .line 393
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mIsPasswordOK:Z

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->setButtonStyle(Z)V

    .line 394
    return-void
.end method

.method private setPinContinueMethod()V
    .locals 13

    .prologue
    const v12, 0x7f090890

    const v10, 0x7f090047

    const/4 v11, -0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 465
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->OldPassword:Ljava/lang/String;

    if-nez v8, :cond_0

    .line 466
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinEditText:Landroid/widget/EditText;

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->OldPassword:Ljava/lang/String;

    .line 469
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    const-string/jumbo v9, "oldPassword"

    invoke-virtual {v8, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 471
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    const-string/jumbo v9, "oldPassword"

    invoke-virtual {v8, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->OldPassword:Ljava/lang/String;

    .line 475
    :cond_1
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinString:Ljava/lang/String;

    if-nez v8, :cond_3

    .line 478
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinTitle:Landroid/widget/TextView;

    invoke-virtual {v6, v12}, Landroid/widget/TextView;->setText(I)V

    .line 479
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinEditText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinString:Ljava/lang/String;

    .line 480
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinEditText:Landroid/widget/EditText;

    const-string v7, ""

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 481
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    invoke-virtual {v6, v12}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 482
    iget-boolean v6, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mIsSecurityDisable:Z

    if-nez v6, :cond_2

    iget-boolean v6, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mIsChangePassword:Z

    if-nez v6, :cond_2

    .line 484
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mContinueButton:Landroid/widget/TextView;

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setText(I)V

    .line 485
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mContinueButton:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09020a

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinEditText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->requestFocus()Z

    .line 633
    :goto_0
    return-void

    .line 492
    :cond_3
    iget-boolean v8, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mIsSecurityDisable:Z

    if-nez v8, :cond_4

    iget-boolean v8, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mIsChangePassword:Z

    if-eqz v8, :cond_c

    .line 494
    :cond_4
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinEditText:Landroid/widget/EditText;

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinEditText:Landroid/widget/EditText;

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_5

    .line 496
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinEditText:Landroid/widget/EditText;

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinString:Ljava/lang/String;

    .line 498
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    const-string/jumbo v9, "security_lock_password"

    invoke-virtual {v8, p0, v9}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getStringValueForConfigKey(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 502
    .local v4, "passcode":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinString:Ljava/lang/String;

    invoke-static {p0, v8, v4}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->doesPinCodeMatch(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    .line 505
    .local v2, "isPasscodeCorrect":Z
    if-eqz v2, :cond_7

    .line 507
    iget-boolean v8, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mIsChangePassword:Z

    if-eqz v8, :cond_6

    .line 509
    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinString:Ljava/lang/String;

    iput-object v7, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->OldPassword:Ljava/lang/String;

    .line 510
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->finish()V

    .line 511
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->hideHeightKeyboard()V

    .line 512
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->hideHeightKeyboard()V

    .line 513
    new-instance v1, Landroid/content/Intent;

    const-class v7, Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    invoke-direct {v1, p0, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 514
    .local v1, "intent":Landroid/content/Intent;
    const-string v7, "initiatePasswordChange"

    invoke-virtual {v1, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 515
    const-string/jumbo v6, "oldPassword"

    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->OldPassword:Ljava/lang/String;

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 516
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->startActivity(Landroid/content/Intent;)V

    .line 631
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "isPasscodeCorrect":Z
    .end local v4    # "passcode":Ljava/lang/String;
    :cond_5
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinEditText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    .line 520
    .restart local v2    # "isPasscodeCorrect":Z
    .restart local v4    # "passcode":Ljava/lang/String;
    :cond_6
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    const-string/jumbo v9, "security_pin_enabled"

    invoke-virtual {v8, p0, v9, v7}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->saveBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 521
    new-instance v5, Landroid/content/Intent;

    const-class v8, Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService;

    invoke-direct {v5, p0, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 522
    .local v5, "pinCodeResetService":Landroid/content/Intent;
    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->stopService(Landroid/content/Intent;)Z

    .line 523
    invoke-virtual {p0, v11}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->setResult(I)V

    .line 524
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->hideHeightKeyboard()V

    .line 525
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->finish()V

    .line 526
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->hideHeightKeyboard()V

    .line 528
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/service/health/keyManager/KeyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->OldPassword:Ljava/lang/String;

    .line 529
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->OldPassword:Ljava/lang/String;

    invoke-virtual {v8, v9, v6}, Lcom/sec/android/service/health/keyManager/KeyManager;->setDbPassword(Ljava/lang/String;I)V

    .line 530
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v6

    iput-boolean v7, v6, Lcom/sec/android/service/health/keyManager/KeyManager;->setting_password:Z

    goto :goto_1

    .line 535
    .end local v5    # "pinCodeResetService":Landroid/content/Intent;
    :cond_7
    iget-boolean v6, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mIsChangePassword:Z

    if-nez v6, :cond_8

    iget-boolean v6, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mIsSecurityDisable:Z

    if-eqz v6, :cond_9

    .line 537
    :cond_8
    iget v6, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mWrongPasswordcount:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mWrongPasswordcount:I

    .line 540
    :cond_9
    iget v6, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mWrongPasswordcount:I

    if-eqz v6, :cond_b

    iget v6, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mWrongPasswordcount:I

    rem-int/lit8 v6, v6, 0x5

    if-nez v6, :cond_b

    .line 542
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->isDeviceSignInSamsungAccount(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 544
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getAccountPasswordVerifyIntent()Landroid/content/Intent;

    move-result-object v6

    const/16 v7, 0x1c6d

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    .line 548
    :cond_a
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinTitle:Landroid/widget/TextView;

    const v8, 0x7f090895

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(I)V

    .line 549
    iput v7, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mWrongPasswordcount:I

    .line 550
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinEditText:Landroid/widget/EditText;

    const-string v7, ""

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 555
    :cond_b
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinTitle:Landroid/widget/TextView;

    const v7, 0x7f090895

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 556
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinEditText:Landroid/widget/EditText;

    const-string v7, ""

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 563
    .end local v2    # "isPasscodeCorrect":Z
    .end local v4    # "passcode":Ljava/lang/String;
    :cond_c
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinEditText:Landroid/widget/EditText;

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinString:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_14

    .line 565
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    const-string/jumbo v9, "security_pin_enabled"

    invoke-virtual {v8, p0, v9, v6}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->saveBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 567
    new-instance v5, Landroid/content/Intent;

    const-class v8, Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService;

    invoke-direct {v5, p0, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 568
    .restart local v5    # "pinCodeResetService":Landroid/content/Intent;
    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 572
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    const-string/jumbo v9, "security_lock_password"

    iget-object v10, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinEditText:Landroid/widget/EditText;

    invoke-virtual {v10}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getEncryptedPinCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, p0, v9, v10}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->saveStringValueForConfigKey(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 574
    invoke-virtual {p0, v11}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->setResult(I)V

    .line 575
    iget-boolean v8, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mIsInitiatePasswordChange:Z

    if-eqz v8, :cond_d

    .line 577
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "com.sec.android.app.shealth"

    const-string v10, "S015"

    const-string v11, "CHANGE"

    invoke-static {v8, v9, v10, v11}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    const v8, 0x7f09088d

    invoke-static {p0, v8, v7}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    .line 582
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->hideHeightKeyboard()V

    .line 584
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/service/health/keyManager/KeyManager;->getResetStatus()Z

    move-result v8

    if-eqz v8, :cond_e

    .line 585
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinString:Ljava/lang/String;

    const/4 v10, 0x2

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/service/health/keyManager/KeyManager;->setDbPassword(Ljava/lang/String;I)V

    .line 590
    :goto_3
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v8

    iput-boolean v6, v8, Lcom/sec/android/service/health/keyManager/KeyManager;->setting_password:Z

    .line 592
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->getMigrationState(Landroid/content/Context;)I

    move-result v3

    .line 594
    .local v3, "migrationState":I
    sget-object v8, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "migrationState: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    sget-object v8, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Has Samsung account?: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    if-nez v10, :cond_f

    :goto_4
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v6}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_12

    .line 598
    if-eqz v3, :cond_10

    .line 599
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;-><init>(Landroid/content/Context;)V

    .line 600
    .local v0, "dataMigrationHelper":Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mAppID:Ljava/lang/String;

    const-string v7, "MI01"

    invoke-static {p0, v6, v7}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->startLocalMigration()V

    goto/16 :goto_1

    .line 580
    .end local v0    # "dataMigrationHelper":Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;
    .end local v3    # "migrationState":I
    :cond_d
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "com.sec.android.app.shealth"

    const-string v10, "S015"

    const-string v11, "SET"

    invoke-static {v8, v9, v10, v11}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 587
    :cond_e
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinString:Ljava/lang/String;

    invoke-virtual {v8, v9, v7}, Lcom/sec/android/service/health/keyManager/KeyManager;->setDbPassword(Ljava/lang/String;I)V

    goto :goto_3

    .restart local v3    # "migrationState":I
    :cond_f
    move v6, v7

    .line 595
    goto :goto_4

    .line 602
    :cond_10
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 603
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->finish()V

    .line 604
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const-class v7, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;

    invoke-direct {v1, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 606
    .restart local v1    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 610
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_11
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->finish()V

    goto/16 :goto_1

    .line 612
    :cond_12
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 613
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->finish()V

    .line 614
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const-class v7, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-direct {v1, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 616
    .restart local v1    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 620
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_13
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->finish()V

    goto/16 :goto_1

    .line 627
    .end local v3    # "migrationState":I
    .end local v5    # "pinCodeResetService":Landroid/content/Intent;
    :cond_14
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinTitle:Landroid/widget/TextView;

    const v7, 0x7f090894

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 629
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinEditText:Landroid/widget/EditText;

    const-string v7, ""

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method

.method private toggleKeyBoard()V
    .locals 4

    .prologue
    .line 328
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/shealth/settings/PinCodeActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity$2;-><init>(Lcom/sec/android/app/shealth/settings/PinCodeActivity;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 348
    return-void
.end method

.method private unregisterSipAndRecentKeyReceiver()V
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mSipAndRecentKeyReceiver:Lcom/sec/android/app/shealth/settings/PinCodeActivity$SipAndRecentKeyReceiver;

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mSipAndRecentKeyReceiver:Lcom/sec/android/app/shealth/settings/PinCodeActivity$SipAndRecentKeyReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 244
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mSipAndRecentKeyReceiver:Lcom/sec/android/app/shealth/settings/PinCodeActivity$SipAndRecentKeyReceiver;

    .line 246
    :cond_0
    return-void
.end method

.method private updateTitleAndContinueButtonState(Landroid/text/Editable;)V
    .locals 6
    .param p1, "editable"    # Landroid/text/Editable;

    .prologue
    const/4 v3, 0x4

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 413
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    sget v1, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->NOT_ENTERED:I

    if-ne v0, v1, :cond_1

    .line 416
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinTitle:Landroid/widget/TextView;

    const v1, 0x7f090120

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 418
    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->setPasswordOK(Z)V

    .line 447
    :cond_0
    :goto_0
    return-void

    .line 420
    :cond_1
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    sget v1, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->MINIMUM_LENGTH:I

    if-ge v0, v1, :cond_2

    .line 422
    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->setPasswordOK(Z)V

    .line 423
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinTitle:Landroid/widget/TextView;

    const v1, 0x7f09088e

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 426
    :cond_2
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-lt v0, v3, :cond_0

    .line 428
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    sget v1, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->MAX_LENGTH:I

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mContainsLetterPattern:Ljava/util/regex/Pattern;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 430
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinTitle:Landroid/widget/TextView;

    const v1, 0x7f09088f

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    const/16 v3, 0x10

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 431
    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->setPasswordOK(Z)V

    goto :goto_0

    .line 435
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mContainsLetterPattern:Ljava/util/regex/Pattern;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-nez v0, :cond_4

    .line 437
    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->setPasswordOK(Z)V

    .line 438
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinTitle:Landroid/widget/TextView;

    const v1, 0x7f090892

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 442
    :cond_4
    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->setPasswordOK(Z)V

    .line 443
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinTitle:Landroid/widget/TextView;

    const v1, 0x7f090893

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 353
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 355
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f09011f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 356
    return-void
.end method

.method public hideHeightKeyboard()V
    .locals 3

    .prologue
    .line 454
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 455
    .local v0, "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    .line 457
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 459
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->clearFocus()V

    .line 460
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 654
    const/16 v1, 0x1c6d

    if-ne p1, v1, :cond_0

    .line 656
    const/4 v1, -0x1

    if-ne v1, p2, :cond_0

    .line 658
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->setResetStatus(Z)V

    .line 659
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->hideHeightKeyboard()V

    .line 660
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->finish()V

    .line 661
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 662
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->startActivity(Landroid/content/Intent;)V

    .line 665
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 638
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f08067f

    if-ne v0, v1, :cond_1

    .line 640
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->setResult(I)V

    .line 641
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->hideHeightKeyboard()V

    .line 642
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->finish()V

    .line 649
    :cond_0
    :goto_0
    return-void

    .line 644
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f080680

    if-ne v0, v1, :cond_0

    .line 646
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->setPinContinueMethod()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v7, 0x7f09020b

    const v6, 0x7f090047

    const v5, 0x7f09020a

    .line 112
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 113
    if-eqz p1, :cond_0

    .line 114
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mOutStateBundle:Landroid/os/Bundle;

    .line 116
    :cond_0
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/keyManager/KeyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->OldPassword:Ljava/lang/String;

    .line 118
    const v1, 0x7f030186

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->setContentView(I)V

    .line 119
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 121
    .local v0, "window":Landroid/view/Window;
    if-eqz v0, :cond_2

    .line 123
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x14

    if-gt v1, v2, :cond_1

    .line 124
    const/16 v1, 0x400

    const/16 v2, 0x500

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setFlags(II)V

    .line 129
    :cond_1
    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 133
    :cond_2
    const v1, 0x7f08067e

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinTitle:Landroid/widget/TextView;

    .line 134
    const v1, 0x7f080650

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinEditText:Landroid/widget/EditText;

    .line 135
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinEditText:Landroid/widget/EditText;

    new-instance v2, Lcom/sec/android/app/shealth/settings/PinCodeActivity$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity$1;-><init>(Lcom/sec/android/app/shealth/settings/PinCodeActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 148
    const v1, 0x7f080680

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mContinueButton:Landroid/widget/TextView;

    .line 149
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mContinueButton:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070082

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 150
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mContinueButton:Landroid/widget/TextView;

    const v2, 0x3ecccccd    # 0.4f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setAlpha(F)V

    .line 151
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mContinueButton:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09020d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 152
    const v1, 0x7f08067f

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mCancelBtn:Landroid/widget/TextView;

    .line 153
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mCancelBtn:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09020c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 154
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mCancelBtn:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    .line 156
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->setListenersAccordingToAction()V

    .line 158
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "state"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 160
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "state"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mIsSecurityDisable:Z

    .line 162
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "changePassword"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 164
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "changePassword"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mIsChangePassword:Z

    .line 166
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "initiatePasswordChange"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mIsInitiatePasswordChange:Z

    .line 168
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mIsSecurityDisable:Z

    if-nez v1, :cond_5

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mIsChangePassword:Z

    if-eqz v1, :cond_6

    .line 170
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->setPinContinueMethod()V

    .line 173
    :cond_6
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mIsSecurityDisable:Z

    if-eqz v1, :cond_7

    .line 174
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mContinueButton:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(I)V

    .line 175
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mContinueButton:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    :cond_7
    if-eqz p1, :cond_8

    .line 182
    const-string v1, "keyboard"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mKeyBoardStatus:Z

    .line 187
    :cond_8
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->registerSipAndRecentKeyReceiver()V

    .line 189
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 252
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->unregisterSipAndRecentKeyReceiver()V

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 261
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    .line 262
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 266
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mOutStateBundle:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mOutStateBundle:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    .line 269
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mOutStateBundle:Landroid/os/Bundle;

    .line 272
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mOutStateBundle:Landroid/os/Bundle;

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mOutStateBundle:Landroid/os/Bundle;

    const-string v1, "keyboard"

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mIsKeyBoardShown:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 277
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->unregisterSipAndRecentKeyReceiver()V

    .line 279
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mIsRecentKeyPressed:Z

    .line 281
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPause()V

    .line 282
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/16 v2, 0x14

    .line 295
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mOutStateBundle:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    .line 297
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mOutStateBundle:Landroid/os/Bundle;

    const-string v1, "keyboard"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mKeyBoardStatus:Z

    .line 298
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mKeyBoardStatus:Z

    if-nez v0, :cond_0

    .line 300
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 313
    :goto_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 319
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->toggleKeyBoard()V

    .line 322
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->registerSipAndRecentKeyReceiver()V

    .line 323
    return-void

    .line 304
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 305
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    goto :goto_0

    .line 310
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 311
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outStateTemp"    # Landroid/os/Bundle;

    .prologue
    .line 287
    const-string v0, "keyboard"

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mOutStateBundle:Landroid/os/Bundle;

    const-string v2, "keyboard"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 289
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 290
    return-void
.end method

.method public restoreCompleted(Z)V
    .locals 3
    .param p1, "restored"    # Z

    .prologue
    .line 669
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->finish()V

    .line 671
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 672
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->startActivity(Landroid/content/Intent;)V

    .line 673
    return-void
.end method
