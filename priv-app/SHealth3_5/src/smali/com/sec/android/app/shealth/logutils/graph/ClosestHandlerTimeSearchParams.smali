.class public abstract enum Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;
.super Ljava/lang/Enum;
.source "ClosestHandlerTimeSearchParams.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;

.field public static final enum SEARCH_AFTER_CURRENT_TIME:Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;

.field public static final enum SEARCH_BEFORE_CURRENT_TIME:Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;


# instance fields
.field private mPeriodTime:J

.field private mSortOrder:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams$1;

    const-string v1, "SEARCH_BEFORE_CURRENT_TIME"

    sget-object v3, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;->DESC:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;

    const-wide/high16 v4, -0x8000000000000000L

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams$1;-><init>(Ljava/lang/String;ILcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;J)V

    sput-object v0, Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;->SEARCH_BEFORE_CURRENT_TIME:Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;

    .line 30
    new-instance v3, Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams$2;

    const-string v4, "SEARCH_AFTER_CURRENT_TIME"

    sget-object v6, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;->ASC:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;

    const-wide v7, 0x7fffffffffffffffL

    move v5, v9

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams$2;-><init>(Ljava/lang/String;ILcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;J)V

    sput-object v3, Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;->SEARCH_AFTER_CURRENT_TIME:Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;

    .line 23
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;

    sget-object v1, Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;->SEARCH_BEFORE_CURRENT_TIME:Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;->SEARCH_AFTER_CURRENT_TIME:Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;

    aput-object v1, v0, v9

    sput-object v0, Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;->$VALUES:[Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;J)V
    .locals 0
    .param p3, "sortOrder"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;
    .param p4, "periodTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 46
    iput-object p3, p0, Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;->mSortOrder:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;

    .line 47
    iput-wide p4, p0, Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;->mPeriodTime:J

    .line 48
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;JLcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # I
    .param p3, "x2"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;
    .param p4, "x3"    # J
    .param p6, "x4"    # Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams$1;

    .prologue
    .line 23
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;-><init>(Ljava/lang/String;ILcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;J)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 23
    const-class v0, Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;->$VALUES:[Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;

    return-object v0
.end method


# virtual methods
.method public abstract getOptimalTime(JJ)J
.end method

.method public getPeriodEnd(J)J
    .locals 4
    .param p1, "selectedTime"    # J

    .prologue
    .line 69
    iget-wide v0, p0, Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;->mPeriodTime:J

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public getPeriodStart(J)J
    .locals 4
    .param p1, "selectedTime"    # J

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;->mPeriodTime:J

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public getSortOrder()Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;->mSortOrder:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;

    return-object v0
.end method
