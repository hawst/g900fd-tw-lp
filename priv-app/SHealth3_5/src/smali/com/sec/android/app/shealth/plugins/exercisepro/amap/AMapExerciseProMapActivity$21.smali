.class Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$21;
.super Ljava/lang/Object;
.source "AMapExerciseProMapActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)V
    .locals 0

    .prologue
    .line 2330
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$21;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 2333
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPrevLocation:Landroid/location/Location;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$21;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->mMap:Lcom/amap/api/maps2d/AMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Lcom/amap/api/maps2d/AMap;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2334
    new-instance v0, Lcom/amap/api/maps2d/model/LatLng;

    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreviousLatLng:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v1, v1, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreviousLatLng:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v3, v3, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    .line 2336
    .local v0, "mPreviousLatLng":Lcom/amap/api/maps2d/model/LatLng;
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$21;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->mMap:Lcom/amap/api/maps2d/AMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Lcom/amap/api/maps2d/AMap;

    move-result-object v1

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->mZoomLevel:F
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$2300()F

    move-result v2

    invoke-static {v0, v2}, Lcom/amap/api/maps2d/CameraUpdateFactory;->newLatLngZoom(Lcom/amap/api/maps2d/model/LatLng;F)Lcom/amap/api/maps2d/CameraUpdate;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/amap/api/maps2d/AMap;->moveCamera(Lcom/amap/api/maps2d/CameraUpdate;)V

    .line 2338
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$21;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->mAMapListener:Lcom/amap/api/maps2d/LocationSource$OnLocationChangedListener;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$2400(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Lcom/amap/api/maps2d/LocationSource$OnLocationChangedListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2339
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$21;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->mAMapListener:Lcom/amap/api/maps2d/LocationSource$OnLocationChangedListener;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$2400(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Lcom/amap/api/maps2d/LocationSource$OnLocationChangedListener;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPrevLocation:Landroid/location/Location;

    invoke-interface {v1, v2}, Lcom/amap/api/maps2d/LocationSource$OnLocationChangedListener;->onLocationChanged(Landroid/location/Location;)V

    .line 2341
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$21;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->mLocationHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$4400(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2342
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$21;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->mLocationHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$4400(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$21;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->mLocationRunnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$4500(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2351
    .end local v0    # "mPreviousLatLng":Lcom/amap/api/maps2d/model/LatLng;
    :cond_1
    :goto_0
    return-void

    .line 2344
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$21;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->mLocationHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$4400(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$21;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->mMap:Lcom/amap/api/maps2d/AMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Lcom/amap/api/maps2d/AMap;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2345
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$21;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->mLocationHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$4400(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$21;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->mLocationRunnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$4500(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Ljava/lang/Runnable;

    move-result-object v2

    const-wide/16 v3, 0x7d0

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 2347
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$21;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->mLocationHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$4400(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2348
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$21;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->mLocationHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$4400(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$21;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->mLocationRunnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$4500(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
