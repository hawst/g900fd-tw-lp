.class public interface abstract Lcom/sec/android/app/shealth/logutils/graph/GraphConstants;
.super Ljava/lang/Object;
.source "GraphConstants.java"


# static fields
.field public static final DAY_DEFAULT_MARK_COUNT:I = 0x7

.field public static final DAY_MAX_ZOOM_MARK_COUNT:I = 0x7

.field public static final DAY_MIN_ZOOM_MARK_COUNT:I = 0x1f

.field public static final DAY_TAB_BOTTOM_SEPARATOR_DATE_FORMAT_CONTENTS:[Ljava/lang/String;

.field public static final HOUR_DEFAULT_MARK_COUNT:I = 0xc

.field public static final HOUR_MAX_ZOOM_MARK_COUNT:I = 0x3

.field public static final HOUR_MIN_ZOOM_MARK_COUNT:I = 0x18

.field public static final HOUR_TAB_BOTTOM_SEPARATOR_DATE_FORMAT_CONTENTS:[Ljava/lang/String;

.field public static final MIN_MARK_INTERVAL:I = 0x1

.field public static final MONTH_DEFAULT_MARK_COUNT:I = 0xc

.field public static final MONTH_MAX_ZOOM_MARK_COUNT:I = 0x6

.field public static final MONTH_MIN_ZOOM_MARK_COUNT:I = 0x18

.field public static final MONTH_TAB_BOTTOM_SEPARATOR_DATE_FORMAT_CONTENTS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 64
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "yyyy"

    aput-object v1, v0, v2

    const-string v1, "MM"

    aput-object v1, v0, v3

    const-string v1, "dd"

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/shealth/logutils/graph/GraphConstants;->HOUR_TAB_BOTTOM_SEPARATOR_DATE_FORMAT_CONTENTS:[Ljava/lang/String;

    .line 68
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "yyyy"

    aput-object v1, v0, v2

    const-string v1, "MM"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/shealth/logutils/graph/GraphConstants;->DAY_TAB_BOTTOM_SEPARATOR_DATE_FORMAT_CONTENTS:[Ljava/lang/String;

    .line 72
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "yyyy"

    aput-object v1, v0, v2

    sput-object v0, Lcom/sec/android/app/shealth/logutils/graph/GraphConstants;->MONTH_TAB_BOTTOM_SEPARATOR_DATE_FORMAT_CONTENTS:[Ljava/lang/String;

    return-void
.end method
