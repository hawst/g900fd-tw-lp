.class public Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;
.super Ljava/lang/Object;
.source "PointDataDisplay.java"


# instance fields
.field private efficiency:F

.field private isFirst:Z

.field private isLast:Z

.field private isfalseOverlap:Z

.field private motionLess:J

.field private sleepId:J

.field private sleepRiseTime:J

.field private sleepStartTime:J

.field private totalTime:J


# direct methods
.method public constructor <init>(JJFJJZZJZ)V
    .locals 1
    .param p1, "totalTime"    # J
    .param p3, "sleepStartTime"    # J
    .param p5, "efficiency"    # F
    .param p6, "sleepRiseTime"    # J
    .param p8, "motionLess"    # J
    .param p10, "isFirst"    # Z
    .param p11, "isLast"    # Z
    .param p12, "sleepId"    # J
    .param p14, "isfOverlap"    # Z

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->isfalseOverlap:Z

    .line 15
    iput-wide p1, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->totalTime:J

    .line 16
    iput-wide p3, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->sleepStartTime:J

    .line 17
    iput p5, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->efficiency:F

    .line 18
    iput-wide p6, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->sleepRiseTime:J

    .line 19
    iput-wide p8, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->motionLess:J

    .line 20
    iput-boolean p10, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->isFirst:Z

    .line 21
    iput-boolean p11, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->isLast:Z

    .line 22
    iput-wide p12, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->sleepId:J

    .line 23
    iput-boolean p14, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->isfalseOverlap:Z

    .line 24
    return-void
.end method


# virtual methods
.method public getEfficiency()F
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->efficiency:F

    return v0
.end method

.method public getIsFirst()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->isFirst:Z

    return v0
.end method

.method public getIsLast()Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->isLast:Z

    return v0
.end method

.method public getMotionLess()J
    .locals 2

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->motionLess:J

    return-wide v0
.end method

.method public getSleepId()J
    .locals 2

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->sleepId:J

    return-wide v0
.end method

.method public getSleepRiseTime()J
    .locals 2

    .prologue
    .line 39
    iget-wide v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->sleepRiseTime:J

    return-wide v0
.end method

.method public getSleepStartTime()J
    .locals 2

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->sleepStartTime:J

    return-wide v0
.end method

.method public getTotalTime()J
    .locals 2

    .prologue
    .line 27
    iget-wide v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->totalTime:J

    return-wide v0
.end method

.method public isfalseOverlap()Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->isfalseOverlap:Z

    return v0
.end method
