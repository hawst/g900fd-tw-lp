.class public Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "ComfortZoneLevelActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IContentInitializatorGetter;


# static fields
.field private static final DISCARD_CHANGE_POPUP:Ljava/lang/String; = "discard"

.field private static final MAX_DIGIT_COUNT_IN_RANGE_INPUT_CELSIUS:I = 0x2

.field private static final MAX_DIGIT_COUNT_IN_RANGE_INPUT_FAHRENHEIT:I = 0x3

.field private static final SHORT_RANGE_POPUP:Ljava/lang/String; = "short_range"

.field public static TAG:Ljava/lang/String; = null

.field private static final WRONG_RANGE_POPUP:Ljava/lang/String; = "range"


# instance fields
.field private MAXIMAL_HUMIDITY:F

.field private MAXIMAL_TEMPETATURE:F

.field private MINIMAL_HUMIDIY:F

.field private MINIMAL_TEMPETATURE:F

.field private acceptableRangeHumidity:F

.field private acceptableRangeTemperature:F

.field cancel:Landroid/widget/Button;

.field private degreeName:Ljava/lang/String;

.field done:Landroid/widget/Button;

.field protected humidityFrom:Landroid/widget/EditText;

.field private humidityHigh:Ljava/lang/String;

.field private humidityLow:Ljava/lang/String;

.field protected humidityTo:Landroid/widget/EditText;

.field private min_value:I

.field private rangeView:Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;

.field resetBtn:Landroid/widget/Button;

.field private tempHigh:Ljava/lang/String;

.field private tempLow:Ljava/lang/String;

.field protected temperatureFrom:Landroid/widget/EditText;

.field protected temperatureTo:Landroid/widget/EditText;

.field private temperatuteUnitsFirst:Landroid/widget/TextView;

.field private temperatuteUnitsSecond:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const-class v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 53
    const/high16 v0, 0x40800000    # 4.0f

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->acceptableRangeTemperature:F

    .line 54
    const/high16 v0, 0x41800000    # 16.0f

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->acceptableRangeHumidity:F

    .line 69
    const/16 v0, -0x14

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->min_value:I

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->degreeName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->isDataChanged()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    .prologue
    .line 45
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->MINIMAL_HUMIDIY:F

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    .prologue
    .line 45
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->MAXIMAL_HUMIDITY:F

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;F)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;
    .param p1, "x1"    # F

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->setHumidityRangeMinimal(F)V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;F)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;
    .param p1, "x1"    # F

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->setHumidityRangeMaxial(F)V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    .prologue
    .line 45
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->acceptableRangeTemperature:F

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->showResetPopup()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->checkRangeAndSave()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;Landroid/text/Editable;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;
    .param p1, "x1"    # Landroid/text/Editable;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->removeSpace(Landroid/text/Editable;)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    .prologue
    .line 45
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->MINIMAL_TEMPETATURE:F

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    .prologue
    .line 45
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->MAXIMAL_TEMPETATURE:F

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->setButtonState()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;F)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;
    .param p1, "x1"    # F

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->setTemperatureRangeMinimal(F)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;F)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;
    .param p1, "x1"    # F

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->setTemperatureRangeMaximal(F)V

    return-void
.end method

.method private checkRangeAndSave()V
    .locals 7

    .prologue
    .line 465
    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityTo:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityFrom:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureTo:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureFrom:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureFrom:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureTo:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 467
    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureFrom:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    .line 468
    .local v2, "temptemperatureFrom":F
    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureTo:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    .line 469
    .local v3, "temptemperatureTo":F
    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityFrom:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    .line 470
    .local v0, "temphumidityFrom":F
    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityTo:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    .line 471
    .local v1, "temphumidityTo":F
    cmpl-float v4, v2, v3

    if-gez v4, :cond_0

    cmpl-float v4, v0, v1

    if-ltz v4, :cond_1

    .line 472
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->createInvalidDataPopup()V

    .line 500
    .end local v0    # "temphumidityFrom":F
    .end local v1    # "temphumidityTo":F
    .end local v2    # "temptemperatureFrom":F
    .end local v3    # "temptemperatureTo":F
    :goto_0
    return-void

    .line 473
    .restart local v0    # "temphumidityFrom":F
    .restart local v1    # "temphumidityTo":F
    .restart local v2    # "temptemperatureFrom":F
    .restart local v3    # "temptemperatureTo":F
    :cond_1
    invoke-direct {p0, v2, v3, v0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->isInputedDataCorrect(FFFF)Z

    move-result v4

    if-nez v4, :cond_2

    .line 474
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->createAndShowWrongRangePopup()V

    goto :goto_0

    .line 475
    :cond_2
    invoke-direct {p0, v2, v3, v0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->isInputedRangeCorrect(FFFF)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 477
    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->degreeName:Ljava/lang/String;

    const-string v5, "C"

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHUnitUtils;->convertTemperatureData(FLjava/lang/String;Ljava/lang/String;)F

    move-result v2

    .line 478
    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->degreeName:Ljava/lang/String;

    const-string v5, "C"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHUnitUtils;->convertTemperatureData(FLjava/lang/String;Ljava/lang/String;)F

    move-result v3

    .line 479
    invoke-static {v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setTemperatureMinimal(F)V

    .line 480
    invoke-static {v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setTemperatureMaximal(F)V

    .line 481
    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setHumidityMinimal(F)V

    .line 482
    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setHumidityMaximal(F)V

    .line 483
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->hideKeyboard()V

    .line 486
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "from_popup"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 487
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/SoundUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/common/utils/SoundUtils;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, p0, v5}, Lcom/sec/android/app/shealth/common/utils/SoundUtils;->playSoundResource(Landroid/content/Context;I)V

    .line 493
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->finish()V

    goto :goto_0

    .line 495
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->createComfortShortValuePopup()V

    goto :goto_0

    .line 498
    .end local v0    # "temphumidityFrom":F
    .end local v1    # "temphumidityTo":F
    .end local v2    # "temptemperatureFrom":F
    .end local v3    # "temptemperatureTo":F
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->createAndShowWrongRangePopup()V

    goto :goto_0
.end method

.method private createAndShowWrongRangePopup()V
    .locals 4

    .prologue
    .line 596
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v2, 0x0

    const v3, 0x7f0900e3

    invoke-direct {v1, p0, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    const v2, 0x7f030096

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setNeedRedraw(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    .line 599
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string/jumbo v3, "range"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 600
    return-void
.end method

.method private createComfortShortValuePopup()V
    .locals 4

    .prologue
    .line 603
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v2, 0x0

    const v3, 0x7f0900e3

    invoke-direct {v1, p0, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    const v2, 0x7f030098

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setNeedRedraw(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    .line 605
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string/jumbo v3, "short_range"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 606
    return-void
.end method

.method private createInvalidDataPopup()V
    .locals 5

    .prologue
    const v4, 0x7f090116

    .line 590
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v2, 0x0

    const v3, 0x7f0900e3

    invoke-direct {v1, p0, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    const v2, 0x7f030098

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setNeedRedraw(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    .line 592
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 593
    return-void
.end method

.method private static getTemperatureMatchingPattern()Ljava/lang/String;
    .locals 2

    .prologue
    .line 399
    const-string v0, "C"

    sget-object v1, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHCache;->infoTemperatureUnit:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 400
    const-string v0, "-?[0-9]{0,2}"

    .line 402
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "-?[0-9]{0,3}"

    goto :goto_0
.end method

.method private hideKeyboard()V
    .locals 3

    .prologue
    .line 738
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 739
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 741
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 743
    :cond_0
    return-void
.end method

.method private initTextWachers()V
    .locals 3

    .prologue
    .line 236
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureFrom:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/shealth/thermohygrometer/PatternTextWatcher;

    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->getTemperatureMatchingPattern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/thermohygrometer/PatternTextWatcher;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureTo:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/shealth/thermohygrometer/PatternTextWatcher;

    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->getTemperatureMatchingPattern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/thermohygrometer/PatternTextWatcher;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureFrom:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$3;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureTo:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$4;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityFrom:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$5;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 357
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityTo:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$6;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 396
    return-void
.end method

.method private initView(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 188
    const v0, 0x7f0802d1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatuteUnitsFirst:Landroid/widget/TextView;

    .line 189
    const v0, 0x7f0802d3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatuteUnitsSecond:Landroid/widget/TextView;

    .line 190
    const v0, 0x7f0802d0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureFrom:Landroid/widget/EditText;

    .line 191
    const v0, 0x7f0802d2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureTo:Landroid/widget/EditText;

    .line 192
    const v0, 0x7f0802d6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityFrom:Landroid/widget/EditText;

    .line 193
    const v0, 0x7f0802d8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityTo:Landroid/widget/EditText;

    .line 194
    const v0, 0x7f0802da

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->rangeView:Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;

    .line 195
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->refreshTemperatureUnits()V

    .line 196
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->initTextWachers()V

    .line 197
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->initEditText(Landroid/os/Bundle;)V

    .line 198
    return-void
.end method

.method private isDataChanged()Z
    .locals 8

    .prologue
    const/4 v6, 0x1

    .line 508
    iget-object v7, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureFrom:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 509
    .local v4, "temperatureF":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureTo:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 510
    .local v5, "temperatureT":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityFrom:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 511
    .local v0, "humidityF":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityTo:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 512
    .local v1, "humidityT":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    .line 514
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    .line 515
    .local v2, "tempHumidityFrom":F
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    .line 516
    .local v3, "tempHumidityTo":F
    iget-object v7, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->tempLow:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->tempHigh:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getHumidityMinimal()F

    move-result v7

    cmpl-float v7, v2, v7

    if-nez v7, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getHumidityMaximal()F

    move-result v7

    cmpl-float v7, v3, v7

    if-nez v7, :cond_0

    .line 518
    const/4 v6, 0x0

    .line 523
    .end local v2    # "tempHumidityFrom":F
    .end local v3    # "tempHumidityTo":F
    :cond_0
    return v6
.end method

.method private isInputedDataCorrect(FFFF)Z
    .locals 4
    .param p1, "tempTemperatureFrom"    # F
    .param p2, "tempTemperatureTo"    # F
    .param p3, "tempHumidityFrom"    # F
    .param p4, "tempHumidityTo"    # F

    .prologue
    const/4 v2, 0x0

    .line 536
    cmpg-float v3, p2, p1

    if-gez v3, :cond_0

    .line 537
    move v1, p2

    .line 538
    .local v1, "tempTemperature":F
    move p2, p1

    .line 539
    move p1, v1

    .line 541
    .end local v1    # "tempTemperature":F
    :cond_0
    cmpg-float v3, p4, p3

    if-gez v3, :cond_1

    .line 542
    move v0, p4

    .line 543
    .local v0, "tempHumidity":F
    move p4, p3

    .line 544
    move p3, v0

    .line 546
    .end local v0    # "tempHumidity":F
    :cond_1
    iget v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->MINIMAL_TEMPETATURE:F

    cmpg-float v3, p1, v3

    if-gez v3, :cond_2

    .line 547
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureFrom:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->requestFocus()Z

    .line 559
    :goto_0
    return v2

    .line 549
    :cond_2
    iget v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->MAXIMAL_TEMPETATURE:F

    cmpl-float v3, p2, v3

    if-lez v3, :cond_3

    .line 550
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureTo:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    .line 552
    :cond_3
    iget v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->MINIMAL_HUMIDIY:F

    cmpg-float v3, p3, v3

    if-gez v3, :cond_4

    .line 553
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityFrom:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    .line 555
    :cond_4
    iget v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->MAXIMAL_HUMIDITY:F

    cmpl-float v3, p4, v3

    if-lez v3, :cond_5

    .line 556
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityTo:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    .line 559
    :cond_5
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private isInputedRangeCorrect(FFFF)Z
    .locals 5
    .param p1, "temperatureFrom"    # F
    .param p2, "temperatureTo"    # F
    .param p3, "humidityFrom"    # F
    .param p4, "humidityTo"    # F

    .prologue
    const/4 v2, 0x0

    .line 566
    cmpg-float v3, p2, p1

    if-gez v3, :cond_0

    .line 567
    move v1, p2

    .line 568
    .local v1, "tempTemperature":F
    move p2, p1

    .line 569
    move p1, v1

    .line 571
    .end local v1    # "tempTemperature":F
    :cond_0
    cmpg-float v3, p4, p3

    if-gez v3, :cond_1

    .line 572
    move v0, p4

    .line 573
    .local v0, "tempHumidity":F
    move p4, p3

    .line 574
    move p3, v0

    .line 576
    .end local v0    # "tempHumidity":F
    :cond_1
    sub-float v3, p2, p1

    iget v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->acceptableRangeTemperature:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    .line 577
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureFrom:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->requestFocus()Z

    .line 584
    :goto_0
    return v2

    .line 580
    :cond_2
    sub-float v3, p4, p3

    iget v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->acceptableRangeHumidity:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_3

    .line 581
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityFrom:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    .line 584
    :cond_3
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private removeSpace(Landroid/text/Editable;)V
    .locals 3
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 407
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 408
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 409
    .local v0, "index":I
    add-int/lit8 v1, v0, 0x1

    invoke-interface {p1, v0, v1}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto :goto_0

    .line 411
    .end local v0    # "index":I
    :cond_0
    return-void
.end method

.method private setButtonState()V
    .locals 2

    .prologue
    .line 448
    const v0, 0x7f080a6e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->done:Landroid/widget/Button;

    .line 449
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->done:Landroid/widget/Button;

    if-eqz v0, :cond_1

    .line 450
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityTo:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityFrom:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureFrom:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureTo:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 451
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->done:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 455
    :cond_1
    :goto_0
    return-void

    .line 453
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->done:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method private setHumidityRangeMaxial(F)V
    .locals 1
    .param p1, "humidityTo"    # F

    .prologue
    .line 444
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->rangeView:Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->setTopHum(F)V

    .line 445
    return-void
.end method

.method private setHumidityRangeMinimal(F)V
    .locals 1
    .param p1, "humidityFrom"    # F

    .prologue
    .line 439
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->rangeView:Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->setLowHum(F)V

    .line 440
    return-void
.end method

.method private setTemperatureRangeMaximal(F)V
    .locals 1
    .param p1, "temperatureTo"    # F

    .prologue
    .line 434
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->rangeView:Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->setRightTemp(F)V

    .line 435
    return-void
.end method

.method private setTemperatureRangeMinimal(F)V
    .locals 1
    .param p1, "temperatureFrom"    # F

    .prologue
    .line 429
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->rangeView:Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->setLeftTemp(F)V

    .line 430
    return-void
.end method

.method private showResetPopup()V
    .locals 4

    .prologue
    .line 608
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v2, 0x2

    const v3, 0x7f090081

    invoke-direct {v1, p0, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    const v2, 0x7f030097

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setNeedRedraw(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$7;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$7;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    .line 616
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "discard"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 617
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 125
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 127
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$2;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)V

    .line 156
    .local v0, "actionBarButtonListener":Landroid/view/View$OnClickListener;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setHomeButtonVisible(Z)V

    .line 158
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v3, 0x7f090048

    invoke-direct {v1, v0, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(Landroid/view/View$OnClickListener;I)V

    .line 159
    .local v1, "cancelButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setISOKCancelType(Z)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 161
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    new-array v4, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 163
    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v3, 0x7f090044

    invoke-direct {v2, v0, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(Landroid/view/View$OnClickListener;I)V

    .line 164
    .local v2, "saveButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    invoke-virtual {v2, v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setISOKCancelType(Z)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 166
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    new-array v4, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 167
    return-void
.end method

.method public finish()V
    .locals 0

    .prologue
    .line 118
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->hideKeyboard()V

    .line 119
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->finish()V

    .line 120
    return-void
.end method

.method public getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 682
    const-string/jumbo v0, "short_range"

    invoke-virtual {v0, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 683
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$8;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)V

    .line 724
    :goto_0
    return-object v0

    .line 695
    :cond_0
    const-string/jumbo v0, "range"

    invoke-virtual {v0, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 696
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$9;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)V

    goto :goto_0

    .line 710
    :cond_1
    const-string v0, "discard"

    invoke-virtual {v0, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 711
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$10;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)V

    goto :goto_0

    .line 724
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected initConstants()V
    .locals 3

    .prologue
    .line 170
    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHCache;->infoTemperatureUnit:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->degreeName:Ljava/lang/String;

    .line 171
    const-string v0, "F"

    sget-object v1, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHCache;->infoTemperatureUnit:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    const-string v0, "F"

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->degreeName:Ljava/lang/String;

    .line 173
    const v0, 0x40e66666    # 7.2f

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->acceptableRangeTemperature:F

    .line 181
    :goto_0
    const/high16 v0, -0x3e600000    # -20.0f

    const-string v1, "C"

    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->degreeName:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHUnitUtils;->convertTemperatureData(FLjava/lang/String;Ljava/lang/String;)F

    move-result v0

    float-to-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->MINIMAL_TEMPETATURE:F

    .line 182
    const/high16 v0, 0x42700000    # 60.0f

    const-string v1, "C"

    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->degreeName:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHUnitUtils;->convertTemperatureData(FLjava/lang/String;Ljava/lang/String;)F

    move-result v0

    float-to-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->MAXIMAL_TEMPETATURE:F

    .line 183
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->MINIMAL_HUMIDIY:F

    .line 184
    const/high16 v0, 0x42c60000    # 99.0f

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->MAXIMAL_HUMIDITY:F

    .line 185
    return-void

    .line 175
    :cond_0
    const-string v0, "C"

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->degreeName:Ljava/lang/String;

    .line 176
    const/high16 v0, 0x40800000    # 4.0f

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->acceptableRangeTemperature:F

    goto :goto_0
.end method

.method protected initEditText(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 202
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->rangeView:Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getHumidityMaximal()F

    move-result v3

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getHumidityMinimal()F

    move-result v4

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getTemperatureMinimal()F

    move-result v5

    const-string v6, "C"

    iget-object v7, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->degreeName:Ljava/lang/String;

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHUnitUtils;->convertTemperatureData(FLjava/lang/String;Ljava/lang/String;)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    int-to-float v5, v5

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getTemperatureMaximal()F

    move-result v6

    const-string v7, "C"

    iget-object v8, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->degreeName:Ljava/lang/String;

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHUnitUtils;->convertTemperatureData(FLjava/lang/String;Ljava/lang/String;)F

    move-result v6

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->setData(FFFF)V

    .line 207
    if-nez p1, :cond_0

    .line 208
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getTemperatureFirst()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 209
    .local v1, "tempLowRound":I
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getTemperatureSecond()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 210
    .local v0, "tempHeightRound":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getTemperatureFirst()F

    move-result v3

    const-string v4, "C"

    iget-object v5, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->degreeName:Ljava/lang/String;

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHUnitUtils;->convertTemperatureData(FLjava/lang/String;Ljava/lang/String;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->tempLow:Ljava/lang/String;

    .line 212
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getTemperatureSecond()F

    move-result v3

    const-string v4, "C"

    iget-object v5, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->degreeName:Ljava/lang/String;

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHUnitUtils;->convertTemperatureData(FLjava/lang/String;Ljava/lang/String;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->tempHigh:Ljava/lang/String;

    .line 214
    int-to-float v2, v1

    invoke-static {v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setTemperatureMinimal(F)V

    .line 215
    int-to-float v2, v0

    invoke-static {v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setTemperatureMaximal(F)V

    .line 216
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getHumidityFirst()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityLow:Ljava/lang/String;

    .line 217
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getHumiditySecond()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityHigh:Ljava/lang/String;

    .line 224
    .end local v0    # "tempHeightRound":I
    .end local v1    # "tempLowRound":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureFrom:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->tempLow:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 225
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureTo:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->tempHigh:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 226
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityFrom:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityLow:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 227
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityTo:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityHigh:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 229
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureFrom:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureFrom:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setSelection(I)V

    .line 230
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureTo:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureTo:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setSelection(I)V

    .line 231
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityFrom:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityFrom:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setSelection(I)V

    .line 232
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityTo:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityTo:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setSelection(I)V

    .line 233
    return-void

    .line 219
    :cond_0
    const-string v2, "TEMP_LOW"

    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->tempLow:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->tempLow:Ljava/lang/String;

    .line 220
    const-string v2, "TEMP_HIGH"

    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->tempHigh:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->tempHigh:Ljava/lang/String;

    .line 221
    const-string v2, "HUMIDITY_LOW"

    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityLow:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityLow:Ljava/lang/String;

    .line 222
    const-string v2, "HUMIDITY_HIGH"

    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityHigh:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityHigh:Ljava/lang/String;

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 461
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->checkRangeAndSave()V

    .line 462
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 729
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 735
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->initConstants()V

    .line 75
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 76
    const v0, 0x7f030099

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->setContentView(I)V

    .line 77
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->initView(Landroid/os/Bundle;)V

    .line 78
    const v0, 0x7f0802db

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->resetBtn:Landroid/widget/Button;

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->resetBtn:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$1;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 629
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 657
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1

    .line 632
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureFrom:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    .line 633
    .local v0, "cursorPosition":I
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureFrom:Landroid/widget/EditText;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/high16 v3, 0x41a00000    # 20.0f

    const-string v4, "C"

    iget-object v5, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->degreeName:Ljava/lang/String;

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHUnitUtils;->convertTemperatureData(FLjava/lang/String;Ljava/lang/String;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 635
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureFrom:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 636
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureFrom:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureFrom:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelection(I)V

    .line 640
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureTo:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    .line 641
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureTo:Landroid/widget/EditText;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/high16 v3, 0x41d00000    # 26.0f

    const-string v4, "C"

    iget-object v5, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->degreeName:Ljava/lang/String;

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHUnitUtils;->convertTemperatureData(FLjava/lang/String;Ljava/lang/String;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 643
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureTo:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 644
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureTo:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureTo:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelection(I)V

    .line 648
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityFrom:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    .line 649
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityFrom:Landroid/widget/EditText;

    const-string v2, "30"

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 650
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityFrom:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 651
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityTo:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    .line 652
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityTo:Landroid/widget/EditText;

    const-string v2, "70"

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 653
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityTo:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    goto/16 :goto_0

    .line 638
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureFrom:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_1

    .line 646
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureTo:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_2

    .line 629
    :pswitch_data_0
    .packed-switch 0x7f0802db
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 663
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPause()V

    .line 664
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureFrom:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->tempLow:Ljava/lang/String;

    .line 665
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureTo:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->tempHigh:Ljava/lang/String;

    .line 666
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityFrom:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityLow:Ljava/lang/String;

    .line 667
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityTo:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityHigh:Ljava/lang/String;

    .line 668
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 621
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 624
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 113
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 114
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 672
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 674
    const-string v0, "TEMP_LOW"

    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureFrom:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 675
    const-string v0, "TEMP_HIGH"

    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureTo:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    const-string v0, "HUMIDITY_LOW"

    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityFrom:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 677
    const-string v0, "HUMIDITY_HIGH"

    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->humidityTo:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 678
    return-void
.end method

.method protected refreshTemperatureUnits()V
    .locals 5

    .prologue
    .line 415
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const-string v2, "C"

    sget-object v4, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHCache;->infoTemperatureUnit:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f0900cd

    :goto_0
    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 418
    .local v1, "units":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const-string v2, "C"

    sget-object v4, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHCache;->infoTemperatureUnit:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const v2, 0x7f0910a8

    :goto_1
    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 421
    .local v0, "discription":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatuteUnitsFirst:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 422
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatuteUnitsFirst:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 423
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatuteUnitsSecond:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 424
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatuteUnitsSecond:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 425
    return-void

    .line 415
    .end local v0    # "discription":Ljava/lang/String;
    .end local v1    # "units":Ljava/lang/String;
    :cond_0
    const v2, 0x7f0900ce

    goto :goto_0

    .line 418
    .restart local v1    # "units":Ljava/lang/String;
    :cond_1
    const v2, 0x7f0910a7

    goto :goto_1
.end method
