.class final enum Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;
.super Ljava/lang/Enum;
.source "GradationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "LocationRelativeToRange"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;

.field public static final enum AFTER:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;

.field public static final enum BEFORE:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;

.field public static final enum IN:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 769
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;

    const-string v1, "IN"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;->IN:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;

    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;

    const-string v1, "BEFORE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;->BEFORE:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;

    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;

    const-string v1, "AFTER"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;->AFTER:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;

    .line 768
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;

    sget-object v1, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;->IN:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;->BEFORE:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;->AFTER:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;->$VALUES:[Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 768
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 768
    const-class v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;
    .locals 1

    .prologue
    .line 768
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;->$VALUES:[Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;

    return-object v0
.end method
