.class public Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;
.super Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
.source "WalkingMateLogAdapter.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter$Tag;
    }
.end annotation


# static fields
.field private static mCrownStartTime:J

.field private static maxStep:I


# instance fields
.field private final TAG:Ljava/lang/String;

.field _dateFormatter:Ljava/text/DateFormat;

.field cal:Ljava/util/Calendar;

.field dateFormatter:Ljava/text/DateFormat;

.field dateFormatterNew:Ljava/text/SimpleDateFormat;

.field dateFormatterStep:Ljava/text/SimpleDateFormat;

.field dateFormatterTAG:Ljava/text/DateFormat;

.field debugLog:Z

.field mCommonSelection:Ljava/lang/StringBuilder;

.field private mContext:Landroid/content/Context;

.field private mPrivateSelection1:Ljava/lang/StringBuilder;

.field private mPrivateSelection2:Ljava/lang/StringBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 57
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mCrownStartTime:J

    .line 58
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->maxStep:I

    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;Landroid/content/Context;)V
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;-><init>(Landroid/database/Cursor;Landroid/content/Context;)V

    .line 46
    const-class v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->TAG:Ljava/lang/String;

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->debugLog:Z

    .line 50
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->cal:Ljava/util/Calendar;

    .line 51
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd/MM/yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->dateFormatter:Ljava/text/DateFormat;

    .line 52
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd/MM/yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->dateFormatterTAG:Ljava/text/DateFormat;

    .line 53
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MMMM dd EEEE"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->dateFormatterStep:Ljava/text/SimpleDateFormat;

    .line 54
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, " dd"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->dateFormatterNew:Ljava/text/SimpleDateFormat;

    .line 55
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->getLogTimeDateFormat()Ljava/text/SimpleDateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->_dateFormatter:Ljava/text/DateFormat;

    .line 67
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->debugLog:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->TAG:Ljava/lang/String;

    const-string v1, " "

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    :cond_0
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mCrownStartTime:J

    .line 69
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->maxStep:I

    .line 70
    iput-object p2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mCommonSelection:Ljava/lang/StringBuilder;

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mCommonSelection:Ljava/lang/StringBuilder;

    const-string v1, "SELECT _id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mCommonSelection:Ljava/lang/StringBuilder;

    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mCommonSelection:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "user_device__id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mCommonSelection:Ljava/lang/StringBuilder;

    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mCommonSelection:Ljava/lang/StringBuilder;

    const-string v1, "SUM(total_step) AS total_step"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mCommonSelection:Ljava/lang/StringBuilder;

    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mCommonSelection:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "start_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mCommonSelection:Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mCommonSelection:Ljava/lang/StringBuilder;

    const-string v1, "FROM walk_info"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mCommonSelection:Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mCommonSelection:Ljava/lang/StringBuilder;

    const-string v1, "WHERE total_step > 0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mPrivateSelection1:Ljava/lang/StringBuilder;

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mPrivateSelection1:Ljava/lang/StringBuilder;

    const-string v1, "SELECT w._id AS _id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mPrivateSelection1:Ljava/lang/StringBuilder;

    const-string v1, " , w.start_time AS start_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mPrivateSelection1:Ljava/lang/StringBuilder;

    const-string v1, " , w.total_step AS total_step"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mPrivateSelection1:Ljava/lang/StringBuilder;

    const-string v1, " , (SELECT value"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mPrivateSelection1:Ljava/lang/StringBuilder;

    const-string v1, " FROM goal WHERE goal_type=40001 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mPrivateSelection2:Ljava/lang/StringBuilder;

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mPrivateSelection2:Ljava/lang/StringBuilder;

    const-string v1, " AND set_time < w.eod"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mPrivateSelection2:Ljava/lang/StringBuilder;

    const-string v1, " ORDER BY set_time DESC LIMIT 1) as goal,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mPrivateSelection2:Ljava/lang/StringBuilder;

    const-string v1, " strftime(\'%d-%m-%Y\', w.start_time / 1000, \'unixepoch\') AS date"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mPrivateSelection2:Ljava/lang/StringBuilder;

    const-string v1, " FROM (SELECT _id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mPrivateSelection2:Ljava/lang/StringBuilder;

    const-string v1, " , SUM(total_step) AS total_step"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mPrivateSelection2:Ljava/lang/StringBuilder;

    const-string v1, " , start_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mPrivateSelection2:Ljava/lang/StringBuilder;

    const-string v1, " , strftime(\'%s\',datetime(datetime(start_time/1000, \'unixepoch\'), \'start of day\', \'+1 day\', \'-1 second\'))*1000 AS eod"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mPrivateSelection2:Ljava/lang/StringBuilder;

    const-string v1, " FROM walk_info"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mPrivateSelection2:Ljava/lang/StringBuilder;

    const-string v1, " WHERE total_step > 0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mPrivateSelection2:Ljava/lang/StringBuilder;

    const-string v1, " GROUP BY strftime(\"%d-%m-%Y\",([start_time]/1000),\'unixepoch\',\'localtime\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mPrivateSelection2:Ljava/lang/StringBuilder;

    const-string v1, " ORDER BY start_time DESC) AS w"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    return-void
.end method

.method private getLogTimeDateFormat()Ljava/text/SimpleDateFormat;
    .locals 4

    .prologue
    .line 531
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    check-cast v1, Ljava/text/SimpleDateFormat;

    invoke-virtual {v1}, Ljava/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object v0

    .line 533
    .local v0, "pattern":Ljava/lang/String;
    const-string v1, "dd"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 534
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "dd/MM"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 536
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "MM/dd"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto :goto_0
.end method


# virtual methods
.method protected bindChildView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 30
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "isLastChild"    # Z

    .prologue
    .line 138
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->debugLog:Z

    move/from16 v25, v0

    if-eqz v25, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->TAG:Ljava/lang/String;

    move-object/from16 v25, v0

    const-string v26, " "

    invoke-static/range {v25 .. v26}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    :cond_0
    if-nez p3, :cond_2

    .line 274
    :cond_1
    :goto_0
    return-void

    .line 143
    :cond_2
    const v25, 0x7f080129

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    .line 145
    .local v10, "divider":Landroid/view/View;
    if-eqz v10, :cond_1

    .line 146
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v25

    if-nez v25, :cond_7

    const/16 v25, 0x8

    :goto_1
    move/from16 v0, v25

    invoke-virtual {v10, v0}, Landroid/view/View;->setVisibility(I)V

    .line 153
    const v25, 0x7f080b86

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    .line 154
    .local v12, "first_icon":Landroid/widget/ImageView;
    const v25, 0x7f080b87

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/ImageView;

    .line 156
    .local v16, "second_icon":Landroid/widget/ImageView;
    const/4 v4, 0x0

    .line 157
    .local v4, "bMedal_crown":Z
    const/4 v5, 0x0

    .line 159
    .local v5, "bMedal_walking":Z
    const v25, 0x7f08016d

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/TextView;

    .line 161
    .local v23, "txtSteps":Landroid/widget/TextView;
    const-string/jumbo v25, "total_step"

    move-object/from16 v0, p3

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    move-object/from16 v0, p3

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 162
    .local v19, "steps":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v25

    move-object/from16 v0, v25

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v14

    .line 163
    .local v14, "language":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v25

    if-eqz v25, :cond_8

    .line 164
    sget-object v25, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f090b8d

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    const/16 v27, 0x1

    move/from16 v0, v27

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    aput-object v29, v27, v28

    invoke-static/range {v25 .. v27}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    .line 168
    .local v18, "stepStr":Ljava/lang/String;
    :goto_2
    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    const v25, 0x7f080171

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v24

    check-cast v24, Landroid/widget/TextView;

    .line 171
    .local v24, "txtTime":Landroid/widget/TextView;
    const-string/jumbo v25, "start_time"

    move-object/from16 v0, p3

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    move-object/from16 v0, p3

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v21

    .line 172
    .local v21, "time":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->cal:Ljava/util/Calendar;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-wide/from16 v1, v21

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 173
    move-object/from16 v0, p2

    move-wide/from16 v1, v21

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->getWeek_three_chars(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v11

    .line 174
    .local v11, "dname":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->_dateFormatter:Ljava/text/DateFormat;

    move-object/from16 v25, v0

    invoke-static/range {v21 .. v22}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 175
    .local v7, "date":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->dateFormatterStep:Ljava/text/SimpleDateFormat;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->cal:Ljava/util/Calendar;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    .line 176
    .local v8, "dateContentDesc":Ljava/lang/String;
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v25

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " ("

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ") "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    move-object/from16 v0, v24

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 179
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->TAG:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->dateFormatterTAG:Ljava/text/DateFormat;

    move-object/from16 v26, v0

    invoke-static/range {v21 .. v22}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v9

    .line 181
    .local v9, "deviceType":I
    const-string v15, " "

    .line 182
    .local v15, "mDeviceName":Ljava/lang/String;
    const/16 v25, 0x2719

    move/from16 v0, v25

    if-eq v9, v0, :cond_3

    .line 184
    const/16 v25, 0x2727

    move/from16 v0, v25

    if-ne v9, v0, :cond_9

    .line 185
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    const v26, 0x7f090bbd

    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 190
    :cond_3
    :goto_3
    new-instance v25, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter$Tag;

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->dateFormatterTAG:Ljava/text/DateFormat;

    move-object/from16 v27, v0

    invoke-static/range {v21 .. v22}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "_"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "_id"

    move-object/from16 v0, p3

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v27

    move-object/from16 v0, p3

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "_"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "_"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    move-object/from16 v2, v26

    move-object/from16 v3, v27

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter$Tag;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 193
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    move-result-object v25

    move-object/from16 v0, v25

    move-wide/from16 v1, v21

    move/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->isGoalAchived(JI)Z

    move-result v25

    if-eqz v25, :cond_a

    .line 194
    const/4 v5, 0x1

    .line 200
    :goto_4
    sget-wide v25, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mCrownStartTime:J

    cmp-long v25, v25, v21

    if-nez v25, :cond_b

    sget v25, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->maxStep:I

    const/16 v26, 0x2710

    move/from16 v0, v25

    move/from16 v1, v26

    if-lt v0, v1, :cond_b

    const/4 v4, 0x1

    .line 202
    :goto_5
    const v25, 0x7f080169

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/LinearLayout;

    .line 204
    .local v20, "teamHeader":Landroid/widget/LinearLayout;
    const/16 v25, 0x8

    move-object/from16 v0, v20

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 206
    if-eqz p4, :cond_c

    .line 207
    const v25, 0x7f08002d

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v25

    const/16 v26, 0x0

    invoke-virtual/range {v25 .. v26}, Landroid/view/View;->setVisibility(I)V

    .line 211
    :goto_6
    const v25, 0x7f080172

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/CheckBox;

    .line 212
    .local v6, "cb":Landroid/widget/CheckBox;
    move-object/from16 v0, p0

    invoke-virtual {v6, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 215
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    const v26, 0x106000d

    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getColor(I)I

    move-result v25

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 218
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->isMenuDeleteMode()Z

    move-result v25

    if-eqz v25, :cond_e

    .line 219
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-virtual {v6, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 220
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->isCheckAll()Z

    move-result v25

    if-nez v25, :cond_4

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->isLogSelected(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_d

    :cond_4
    const/16 v25, 0x1

    :goto_7
    move/from16 v0, v25

    invoke-virtual {v6, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 229
    :goto_8
    const/16 v17, 0x0

    .line 231
    .local v17, "states":I
    if-eqz v5, :cond_5

    .line 232
    or-int/lit8 v17, v17, 0x1

    .line 234
    :cond_5
    if-eqz v4, :cond_6

    .line 235
    or-int/lit8 v17, v17, 0x2

    .line 238
    :cond_6
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v25

    const/16 v26, 0x2719

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_f

    const/4 v13, 0x1

    .line 243
    .local v13, "isDeviceGoalAchieved":Z
    :goto_9
    packed-switch v17, :pswitch_data_0

    goto/16 :goto_0

    .line 245
    :pswitch_0
    const/16 v25, 0x8

    move/from16 v0, v25

    invoke-virtual {v12, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 246
    const/16 v25, 0x8

    move-object/from16 v0, v16

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 146
    .end local v4    # "bMedal_crown":Z
    .end local v5    # "bMedal_walking":Z
    .end local v6    # "cb":Landroid/widget/CheckBox;
    .end local v7    # "date":Ljava/lang/String;
    .end local v8    # "dateContentDesc":Ljava/lang/String;
    .end local v9    # "deviceType":I
    .end local v11    # "dname":Ljava/lang/String;
    .end local v12    # "first_icon":Landroid/widget/ImageView;
    .end local v13    # "isDeviceGoalAchieved":Z
    .end local v14    # "language":Ljava/lang/String;
    .end local v15    # "mDeviceName":Ljava/lang/String;
    .end local v16    # "second_icon":Landroid/widget/ImageView;
    .end local v17    # "states":I
    .end local v18    # "stepStr":Ljava/lang/String;
    .end local v19    # "steps":I
    .end local v20    # "teamHeader":Landroid/widget/LinearLayout;
    .end local v21    # "time":J
    .end local v23    # "txtSteps":Landroid/widget/TextView;
    .end local v24    # "txtTime":Landroid/widget/TextView;
    :cond_7
    const/16 v25, 0x0

    goto/16 :goto_1

    .line 166
    .restart local v4    # "bMedal_crown":Z
    .restart local v5    # "bMedal_walking":Z
    .restart local v12    # "first_icon":Landroid/widget/ImageView;
    .restart local v14    # "language":Ljava/lang/String;
    .restart local v16    # "second_icon":Landroid/widget/ImageView;
    .restart local v19    # "steps":I
    .restart local v23    # "txtSteps":Landroid/widget/TextView;
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    const v26, 0x7f090b8d

    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v25

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    aput-object v28, v26, v27

    invoke-static/range {v25 .. v26}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    .restart local v18    # "stepStr":Ljava/lang/String;
    goto/16 :goto_2

    .line 187
    .restart local v7    # "date":Ljava/lang/String;
    .restart local v8    # "dateContentDesc":Ljava/lang/String;
    .restart local v9    # "deviceType":I
    .restart local v11    # "dname":Ljava/lang/String;
    .restart local v15    # "mDeviceName":Ljava/lang/String;
    .restart local v21    # "time":J
    .restart local v24    # "txtTime":Landroid/widget/TextView;
    :cond_9
    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getGearDeviceNames(J)Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_3

    .line 196
    :cond_a
    const/4 v5, 0x0

    goto/16 :goto_4

    .line 200
    :cond_b
    const/4 v4, 0x0

    goto/16 :goto_5

    .line 209
    .restart local v20    # "teamHeader":Landroid/widget/LinearLayout;
    :cond_c
    const v25, 0x7f08002d

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v25

    const/16 v26, 0x8

    invoke-virtual/range {v25 .. v26}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_6

    .line 220
    .restart local v6    # "cb":Landroid/widget/CheckBox;
    :cond_d
    const/16 v25, 0x0

    goto :goto_7

    .line 222
    :cond_e
    const/16 v25, 0x8

    move/from16 v0, v25

    invoke-virtual {v6, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 223
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-virtual {v6, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 224
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->clearAllSelection()V

    goto/16 :goto_8

    .line 238
    .restart local v17    # "states":I
    :cond_f
    const/4 v13, 0x0

    goto :goto_9

    .line 249
    .restart local v13    # "isDeviceGoalAchieved":Z
    :pswitch_1
    const v25, 0x7f020593

    move-object/from16 v0, v16

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 250
    if-eqz v13, :cond_10

    const v25, 0x7f020604

    :goto_a
    move-object/from16 v0, v16

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 251
    sget-object v25, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f090b6b

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    const-string v27, ""

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    move-object/from16 v3, v27

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    const/16 v25, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 253
    const/16 v25, 0x8

    move/from16 v0, v25

    invoke-virtual {v12, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 250
    :cond_10
    const v25, 0x7f020607

    goto :goto_a

    .line 256
    :pswitch_2
    const v25, 0x7f0206cc

    move-object/from16 v0, v16

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 257
    sget-object v25, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f090b6c

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    const-string v27, ""

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    move-object/from16 v3, v27

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    const/16 v25, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 259
    const/16 v25, 0x8

    move/from16 v0, v25

    invoke-virtual {v12, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 262
    :pswitch_3
    const v25, 0x7f020593

    move-object/from16 v0, v16

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 263
    sget-object v25, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f090b6b

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    const-string v27, ""

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    move-object/from16 v3, v27

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    const/16 v25, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 265
    if-eqz v13, :cond_11

    const v25, 0x7f020604

    :goto_b
    move-object/from16 v0, v16

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 266
    const v25, 0x7f0206cc

    move/from16 v0, v25

    invoke-virtual {v12, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 267
    sget-object v25, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f090b6c

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    const-string v27, ""

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-static {v12, v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-virtual {v12, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 265
    :cond_11
    const v25, 0x7f020607

    goto :goto_b

    .line 243
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected bindGroupView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 27
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "isExpanded"    # Z

    .prologue
    .line 336
    if-nez p3, :cond_0

    .line 431
    :goto_0
    return-void

    .line 339
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->TAG:Ljava/lang/String;

    move-object/from16 v22, v0

    const-string v23, " "

    invoke-static/range {v22 .. v23}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    const v22, 0x7f080167

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    .line 341
    .local v20, "txtTime":Landroid/widget/TextView;
    if-nez v20, :cond_1

    .line 342
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->TAG:Ljava/lang/String;

    move-object/from16 v22, v0

    const-string v23, "It is failed to get text view."

    invoke-static/range {v22 .. v23}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 345
    :cond_1
    const-string/jumbo v22, "total_step"

    move-object/from16 v0, p3

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    move-object/from16 v0, p3

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 346
    .local v18, "totalSteps":I
    const-string/jumbo v22, "start_time"

    move-object/from16 v0, p3

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    move-object/from16 v0, p3

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 347
    .local v16, "time":J
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v22

    move-object/from16 v0, v22

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v10

    .line 350
    .local v10, "language":Ljava/lang/String;
    const-string v22, "ko"

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_2

    const-string/jumbo v22, "zh"

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_2

    const-string v22, "ja"

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_9

    .line 351
    :cond_2
    move-object/from16 v0, p2

    move-wide/from16 v1, v16

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->getKoreanMonthGroupDateString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v8

    .line 358
    .local v8, "dateSubString":Ljava/lang/String;
    :goto_1
    invoke-static/range {v16 .. v17}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v6

    .line 359
    .local v6, "calculatingMonth":J
    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getPassedDaysForMonthAverage(J)I

    move-result v9

    .line 360
    .local v9, "daysInMonth":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->debugLog:Z

    move/from16 v22, v0

    if-eqz v22, :cond_3

    .line 361
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->TAG:Ljava/lang/String;

    move-object/from16 v22, v0

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "daysInMonth : "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    :cond_3
    move/from16 v0, v18

    int-to-double v0, v0

    move-wide/from16 v22, v0

    int-to-double v0, v9

    move-wide/from16 v24, v0

    div-double v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->floor(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v5, v0

    .line 365
    .local v5, "avgerageSteps":I
    if-nez v5, :cond_4

    const/4 v5, 0x1

    .line 367
    :cond_4
    const/16 v22, 0x1

    move/from16 v0, v22

    if-ne v5, v0, :cond_b

    const v13, 0x7f090baa

    .line 369
    .local v13, "stepStr":I
    :goto_2
    const-string v22, "%d"

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 371
    .local v4, "avgSteps":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->debugLog:Z

    move/from16 v22, v0

    if-eqz v22, :cond_5

    .line 372
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->TAG:Ljava/lang/String;

    move-object/from16 v22, v0

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "average steps : "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->cal:Ljava/util/Calendar;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 375
    new-instance v22, Ljava/text/DateFormatSymbols;

    invoke-direct/range {v22 .. v22}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual/range {v22 .. v22}, Ljava/text/DateFormatSymbols;->getShortMonths()[Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->cal:Ljava/util/Calendar;

    move-object/from16 v23, v0

    const/16 v24, 0x2

    invoke-virtual/range {v23 .. v24}, Ljava/util/Calendar;->get(I)I

    move-result v23

    aget-object v11, v22, v23

    .line 376
    .local v11, "month":Ljava/lang/String;
    new-instance v22, Ljava/text/DateFormatSymbols;

    invoke-direct/range {v22 .. v22}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual/range {v22 .. v22}, Ljava/text/DateFormatSymbols;->getMonths()[Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->cal:Ljava/util/Calendar;

    move-object/from16 v23, v0

    const/16 v24, 0x2

    invoke-virtual/range {v23 .. v24}, Ljava/util/Calendar;->get(I)I

    move-result v23

    aget-object v12, v22, v23

    .line 377
    .local v12, "monthFullName":Ljava/lang/String;
    const/16 v21, 0x0

    .line 379
    .local v21, "yearMonthOrder":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->dateFormatter:Ljava/text/DateFormat;

    move-object/from16 v22, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x6

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v15

    .line 380
    .local v15, "tim":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->debugLog:Z

    move/from16 v22, v0

    if-eqz v22, :cond_6

    .line 381
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->TAG:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->dateFormatter:Ljava/text/DateFormat;

    move-object/from16 v23, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->debugLog:Z

    move/from16 v22, v0

    if-eqz v22, :cond_7

    .line 383
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->TAG:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-static {v0, v15}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    :cond_7
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    .line 386
    .local v19, "txt":Ljava/lang/StringBuilder;
    const-string v22, "ko"

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_8

    const-string/jumbo v22, "zh"

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_8

    const-string v22, "ja"

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_c

    .line 387
    :cond_8
    const/16 v22, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 388
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f090066

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 389
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " ("

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f090ae0

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 390
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f090b8d

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-static/range {v23 .. v24}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ")"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 391
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 392
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f090066

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 408
    :goto_3
    const/16 v22, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 409
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f090b9a

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 410
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 411
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f09021c

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412
    if-eqz p4, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0901ef

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    :goto_4
    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 414
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 416
    const v22, 0x7f080168

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 417
    .local v3, "arrow":Landroid/widget/ImageView;
    if-eqz p4, :cond_f

    .line 418
    sget-object v22, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f09021b

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    const-string v24, ""

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-static {v3, v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    const v22, 0x7f020845

    move/from16 v0, v22

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 430
    :goto_5
    move/from16 v0, p4

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setSelected(Z)V

    goto/16 :goto_0

    .line 352
    .end local v3    # "arrow":Landroid/widget/ImageView;
    .end local v4    # "avgSteps":Ljava/lang/String;
    .end local v5    # "avgerageSteps":I
    .end local v6    # "calculatingMonth":J
    .end local v8    # "dateSubString":Ljava/lang/String;
    .end local v9    # "daysInMonth":I
    .end local v11    # "month":Ljava/lang/String;
    .end local v12    # "monthFullName":Ljava/lang/String;
    .end local v13    # "stepStr":I
    .end local v15    # "tim":Ljava/lang/String;
    .end local v19    # "txt":Ljava/lang/StringBuilder;
    .end local v21    # "yearMonthOrder":Ljava/lang/String;
    :cond_9
    const-string v22, "fi"

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_a

    .line 353
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->getMonthGroupDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v22

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .restart local v8    # "dateSubString":Ljava/lang/String;
    goto/16 :goto_1

    .line 355
    .end local v8    # "dateSubString":Ljava/lang/String;
    :cond_a
    invoke-static/range {p2 .. p2}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->getMonthGroupDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v22

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .restart local v8    # "dateSubString":Ljava/lang/String;
    goto/16 :goto_1

    .line 367
    .restart local v5    # "avgerageSteps":I
    .restart local v6    # "calculatingMonth":J
    .restart local v9    # "daysInMonth":I
    :cond_b
    const v13, 0x7f0900c6

    goto/16 :goto_2

    .line 394
    .restart local v4    # "avgSteps":Ljava/lang/String;
    .restart local v11    # "month":Ljava/lang/String;
    .restart local v12    # "monthFullName":Ljava/lang/String;
    .restart local v13    # "stepStr":I
    .restart local v15    # "tim":Ljava/lang/String;
    .restart local v19    # "txt":Ljava/lang/StringBuilder;
    .restart local v21    # "yearMonthOrder":Ljava/lang/String;
    :cond_c
    const/16 v22, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 395
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " ("

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f090ae0

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 399
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v22

    if-eqz v22, :cond_d

    .line 400
    sget-object v22, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f090b8d

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-static/range {v22 .. v24}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    .line 404
    .local v14, "stepStr2":Ljava/lang/String;
    :goto_6
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ")"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 405
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 406
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    goto/16 :goto_3

    .line 402
    .end local v14    # "stepStr2":Ljava/lang/String;
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f090b8d

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    .restart local v14    # "stepStr2":Ljava/lang/String;
    goto :goto_6

    .line 412
    .end local v14    # "stepStr2":Ljava/lang/String;
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f090217

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    goto/16 :goto_4

    .line 424
    .restart local v3    # "arrow":Landroid/widget/ImageView;
    :cond_f
    const v22, 0x7f02084c

    move/from16 v0, v22

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 425
    sget-object v22, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f09021a

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    const-string v24, ""

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-static {v3, v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I
    .param p3, "isLastChild"    # Z
    .param p4, "convertView"    # Landroid/view/View;
    .param p5, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 326
    move-object v4, p4

    .line 327
    .local v4, "child":Landroid/view/View;
    if-eqz p4, :cond_0

    instance-of v0, p4, Lcom/sec/android/app/shealth/walkingmate/listview/LogRowLinearLayout;

    if-nez v0, :cond_0

    .line 328
    const/4 v4, 0x0

    :cond_0
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v5, p5

    .line 331
    invoke-super/range {v0 .. v5}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected getChildrenCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 11
    .param p1, "groupCursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v2, 0x0

    .line 435
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->TAG:Ljava/lang/String;

    const-string v1, " "

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    const/4 v6, 0x0

    .line 438
    .local v6, "cursor":Landroid/database/Cursor;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v7

    .line 439
    .local v7, "deviceType":I
    const-string/jumbo v0, "start_time"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 440
    .local v9, "time":J
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "group start time "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "group cursor count "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 445
    .local v8, "selection":Ljava/lang/StringBuilder;
    sget v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->SELECTED_FILTER_INDEX:I

    if-nez v0, :cond_1

    .line 447
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mCommonSelection:Ljava/lang/StringBuilder;

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 448
    const-string v0, " AND "

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 449
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "start_time > "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 450
    const-string v0, " AND "

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 451
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "start_time < "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfMonth(J)J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 452
    const-string v0, " AND "

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 453
    invoke-static {v7}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 454
    const-string v0, " "

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 455
    const-string v0, "GROUP BY (strftime(\"%d-%m-%Y\",([start_time]/1000),\'unixepoch\',\'localtime\'))"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 456
    const-string v0, " "

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 457
    const-string v0, "ORDER BY start_time DESC"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 458
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 484
    :cond_0
    :goto_0
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->setCrownStartTime(Landroid/database/Cursor;)V

    .line 485
    return-object v6

    .line 459
    :cond_1
    sget v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->SELECTED_FILTER_INDEX:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 461
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mPrivateSelection1:Ljava/lang/StringBuilder;

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 462
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v7}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 463
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mPrivateSelection2:Ljava/lang/StringBuilder;

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 464
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " WHERE w.start_time < strftime(\'%s\',datetime(datetime("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/1000, \'unixepoch\'), \'start of month\', \'+1 month\', \'-1 second\'))*1000"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 465
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " AND w.start_time > strftime(\'%s\',datetime(datetime("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/1000, \'unixepoch\'), \'start of month\'))*1000"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 466
    const-string v0, " AND w.total_step >= goal"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 467
    const-string v0, " ORDER BY start_time DESC;"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 468
    const-string v0, "PERFORMANCE"

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    goto/16 :goto_0

    .line 471
    :cond_2
    sget v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->SELECTED_FILTER_INDEX:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 473
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mPrivateSelection1:Ljava/lang/StringBuilder;

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 474
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v7}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 475
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mPrivateSelection2:Ljava/lang/StringBuilder;

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 476
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " WHERE w.start_time < strftime(\'%s\',datetime(datetime("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/1000, \'unixepoch\'), \'start of month\', \'+1 month\', \'-1 second\'))*1000"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 477
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " AND w.start_time > strftime(\'%s\',datetime(datetime("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/1000, \'unixepoch\'), \'start of month\'))*1000"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 478
    const-string v0, " AND w.total_step < goal"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 479
    const-string v0, " ORDER BY start_time DESC;"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 480
    const-string v0, "PERFORMANCE"

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    goto/16 :goto_0
.end method

.method protected getColumnNameForCreateTime()Ljava/lang/String;
    .locals 2

    .prologue
    .line 526
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->TAG:Ljava/lang/String;

    const-string v1, " "

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    const-string/jumbo v0, "start_time"

    return-object v0
.end method

.method protected getColumnNameForID()Ljava/lang/String;
    .locals 2

    .prologue
    .line 520
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->TAG:Ljava/lang/String;

    const-string v1, " "

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    const-string v0, "_id"

    return-object v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "groupPosition"    # I
    .param p2, "isExpanded"    # Z
    .param p3, "convertView"    # Landroid/view/View;
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 289
    move-object v3, p3

    .line 290
    .local v3, "group":Landroid/view/View;
    if-eqz p3, :cond_0

    instance-of v5, p3, Lcom/sec/android/app/shealth/walkingmate/listview/LogRowLinearLayout;

    if-eqz v5, :cond_0

    .line 291
    const/4 v3, 0x0

    .line 293
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->isGroupCollapsed(I)Z

    move-result v5

    invoke-super {p0, p1, v5, v3, p4}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .local v4, "groupView":Landroid/view/View;
    move-object v1, p4

    .line 295
    check-cast v1, Landroid/widget/ExpandableListView;

    .line 297
    .local v1, "expandableListView":Landroid/widget/ExpandableListView;
    invoke-virtual {v1, p1}, Landroid/widget/ExpandableListView;->isGroupExpanded(I)Z

    move-result v2

    .line 299
    .local v2, "expanded":Z
    const v5, 0x7f080168

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 300
    .local v0, "arrow":Landroid/widget/ImageView;
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Inside getGroupView"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "::"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "::"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    if-nez v0, :cond_1

    .line 320
    :goto_0
    return-object v4

    .line 304
    :cond_1
    if-eqz v2, :cond_2

    .line 305
    sget-object v5, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09021b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-static {v0, v5, v6, v7}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    const v5, 0x7f020845

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 317
    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_0

    .line 311
    :cond_2
    const v5, 0x7f02084c

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 312
    sget-object v5, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09021a

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-static {v0, v5, v6, v7}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected getTotalChildCount()I
    .locals 6

    .prologue
    .line 506
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->TAG:Ljava/lang/String;

    const-string v4, " "

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    const/4 v0, 0x0

    .line 509
    .local v0, "childCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->getGroupCount()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 510
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->getChildrenCount(I)I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 511
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "group "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "child "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    add-int/lit8 v0, v0, 0x1

    .line 510
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 509
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 515
    .end local v2    # "j":I
    :cond_1
    return v0
.end method

.method protected newChildView(Landroid/content/Context;Landroid/database/Cursor;ZLandroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "isLastChild"    # Z
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 490
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->TAG:Ljava/lang/String;

    const-string v3, " "

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 492
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f03028c

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 493
    .local v1, "v":Landroid/view/View;
    return-object v1
.end method

.method protected newGroupView(Landroid/content/Context;Landroid/database/Cursor;ZLandroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "isExpanded"    # Z
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 498
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->TAG:Ljava/lang/String;

    const-string v3, " "

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 500
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f03028b

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 501
    .local v1, "v":Landroid/view/View;
    return-object v1
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p1, "button"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 278
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;Landroid/widget/CompoundButton;Z)V

    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->post(Ljava/lang/Runnable;)Z

    .line 284
    return-void
.end method

.method public setCrownStartTime(Landroid/database/Cursor;)V
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 108
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->TAG:Ljava/lang/String;

    const-string v4, " "

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    if-nez p1, :cond_1

    .line 112
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->debugLog:Z

    if-eqz v3, :cond_0

    const-string v3, "cursor is null"

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-nez v3, :cond_2

    .line 116
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->debugLog:Z

    if-eqz v3, :cond_0

    const-string v3, "cursor is 0"

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    goto :goto_0

    .line 119
    :cond_2
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->debugLog:Z

    if-eqz v3, :cond_3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cursor size = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 121
    :cond_3
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 122
    const-string/jumbo v3, "total_step"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 123
    .local v0, "steps":I
    const-string/jumbo v3, "start_time"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    .line 124
    .local v1, "time":J
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->debugLog:Z

    if-eqz v3, :cond_4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Steps = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 125
    :cond_4
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->debugLog:Z

    if-eqz v3, :cond_5

    const-string v3, "Time = "

    invoke-static {v3, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->printDate(Ljava/lang/String;J)V

    .line 126
    :cond_5
    sget v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->maxStep:I

    if-ge v3, v0, :cond_3

    .line 127
    sput v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->maxStep:I

    .line 128
    sput-wide v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mCrownStartTime:J

    goto :goto_1

    .line 132
    .end local v0    # "steps":I
    .end local v1    # "time":J
    :cond_6
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->debugLog:Z

    if-eqz v3, :cond_7

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "top Steps = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->maxStep:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 133
    :cond_7
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->debugLog:Z

    if-eqz v3, :cond_0

    const-string v3, "mCrownStartTime"

    sget-wide v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogAdapter;->mCrownStartTime:J

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->printDate(Ljava/lang/String;J)V

    goto/16 :goto_0
.end method
