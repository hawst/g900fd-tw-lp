.class public Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;
.super Ljava/lang/Object;
.source "WeightUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils$ChartResponseData;,
        Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils$WEIGHT_TYPE;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final DAYS_IN_WEEK:B = 0x7t

.field public static final HOURS_IN_DAY:B = 0x18t

.field public static final KG_IN_LB:F = 0.45359236f

.field public static final LAST_DAY_IN_WEEK:B = 0x6t

.field public static final LAST_HOUR_IN_DAY:B = 0x17t

.field public static final LAST_MILLI_IN_SECOND:I = 0x3e7

.field public static final LAST_MINUTE_IN_HOUR:B = 0x3bt

.field public static final LAST_SECOND_IN_MINUTE:B = 0x3bt

.field public static final LB_IN_KG:F = 2.2046225f

.field public static final MILLIS_IN_DAY:I = 0x5265c00

.field public static final MILLIS_IN_HALFDAY:I = 0x2932e00

.field public static final MILLIS_IN_HOUR:I = 0x36ee80

.field public static final MILLIS_IN_SECOND:I = 0x3e8

.field public static final MILLIS_IN_WEEK:I = 0x240c8400

.field public static final MINUTES_IN_HOUR:B = 0x3ct

.field public static final MONTH_IN_YEAR:I = 0xc

.field public static final SECONDS_IN_MINUTE:B = 0x3ct

.field private static final tempCal:Ljava/util/Calendar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    return-void
.end method

.method public static clearWeightData(Landroid/content/Context;)I
    .locals 4
    .param p0, "mContext"    # Landroid/content/Context;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 572
    const/4 v0, 0x0

    .line 573
    .local v0, "rowsDeleted":I
    if-eqz p0, :cond_0

    .line 575
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 577
    :cond_0
    return v0
.end method

.method private static convertKgToLb(F)F
    .locals 1
    .param p0, "weightInKg"    # F
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 640
    const v0, 0x400d1889

    mul-float/2addr v0, p0

    return v0
.end method

.method private static convertLbToKg(F)F
    .locals 1
    .param p0, "weightInLb"    # F
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 634
    const v0, 0x3ee83d42

    mul-float/2addr v0, p0

    return v0
.end method

.method public static convertWeightData(FLjava/lang/String;Ljava/lang/String;)F
    .locals 3
    .param p0, "data"    # F
    .param p1, "inputUnit"    # Ljava/lang/String;
    .param p2, "outputUnit"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 598
    const-string v0, "kg"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "lb"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 600
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Weight unit should be kg or lb, but is: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 602
    :cond_0
    const-string v0, "kg"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "lb"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 604
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Weight unit should be kg or lb, but is: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 606
    :cond_1
    const-string v0, "kg"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 608
    const-string v0, "kg"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 628
    .end local p0    # "data":F
    :cond_2
    :goto_0
    return p0

    .line 612
    .restart local p0    # "data":F
    :cond_3
    const-string v0, "lb"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 614
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->convertLbToKg(F)F

    move-result p0

    goto :goto_0

    .line 617
    :cond_4
    const-string v0, "lb"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 619
    const-string v0, "kg"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 621
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->convertKgToLb(F)F

    move-result p0

    goto :goto_0

    .line 623
    :cond_5
    const-string v0, "lb"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 628
    :cond_6
    const/4 p0, 0x0

    goto :goto_0
.end method

.method private static cursorToWeight(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    .locals 27
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 137
    new-instance v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    const-string v3, "_id"

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const-string/jumbo v5, "user_device__id"

    move-object/from16 v0, p0

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p0

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "comment"

    move-object/from16 v0, p0

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p0

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "input_source_type"

    move-object/from16 v0, p0

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, p0

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    const-string/jumbo v8, "sample_time"

    move-object/from16 v0, p0

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, p0

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    const-string v10, "create_time"

    move-object/from16 v0, p0

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    move-object/from16 v0, p0

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    const-string/jumbo v12, "update_time"

    move-object/from16 v0, p0

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    move-object/from16 v0, p0

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    const-string/jumbo v14, "time_zone"

    move-object/from16 v0, p0

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p0

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    const-string v15, "body_fat"

    move-object/from16 v0, p0

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    move-object/from16 v0, p0

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getFloat(I)F

    move-result v15

    const-string v16, "height"

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v16

    const-string/jumbo v17, "weight"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v17

    const-string v18, "body_mass_index"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v18

    const-string v19, "basal_metabolic_rate"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v19

    const-string v20, "activity_metabolic_rate"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v20

    const-string v21, "body_age"

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v21

    const-string v22, "body_water"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v22

    const-string/jumbo v23, "visceral_fat"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v23

    const-string/jumbo v24, "muscle_mass"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v24

    const-string/jumbo v25, "skeletal_muscle"

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v25

    const-string v26, "bone_mass"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v26

    invoke-direct/range {v2 .. v26}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;-><init>(JLjava/lang/String;Ljava/lang/String;IJJJIFFFFFFFFFFFF)V

    .line 159
    .local v2, "weight":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    return-object v2
.end method

.method public static deleteWeightData(Landroid/content/Context;J)I
    .locals 6
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "date"    # J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 555
    const/4 v1, 0x0

    .line 556
    .local v1, "rowsDeleted":I
    const-string/jumbo v2, "sample_time>?"

    .line 557
    .local v2, "selectionClause":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v0, v3, [Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v0, v5

    .line 558
    .local v0, "mSelectionArgs":[Ljava/lang/String;
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->getEndOfDay(J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v5

    .line 559
    if-eqz p0, :cond_0

    .line 560
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v2, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 561
    :cond_0
    return v1
.end method

.method public static deleteWeightDataForDay(Landroid/content/Context;J)I
    .locals 7
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "date"    # J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 543
    const/4 v1, 0x0

    .line 544
    .local v1, "rowsDeleted":I
    const-string/jumbo v2, "sample_time>? AND sample_time<?"

    .line 545
    .local v2, "selectionClause":Ljava/lang/String;
    const/4 v3, 0x2

    new-array v0, v3, [Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v0, v5

    const-string v3, ""

    aput-object v3, v0, v6

    .line 546
    .local v0, "mSelectionArgs":[Ljava/lang/String;
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->getStartOfDay(J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v5

    .line 547
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->getEndOfDay(J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v6

    .line 548
    if-eqz p0, :cond_0

    .line 549
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v2, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 550
    :cond_0
    return v1
.end method

.method public static getEndOfDay(J)J
    .locals 4
    .param p0, "time"    # J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 344
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    monitor-enter v1

    .line 346
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 347
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xb

    const/16 v3, 0x17

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 348
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xc

    const/16 v3, 0x3b

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 349
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xd

    const/16 v3, 0x3b

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 350
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xe

    const/16 v3, 0x3e7

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 351
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    .line 352
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getEndOfMonth(J)J
    .locals 5
    .param p0, "time"    # J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 358
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    monitor-enter v1

    .line 360
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 361
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/4 v2, 0x5

    sget-object v3, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 362
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xb

    const/16 v3, 0x17

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 363
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xc

    const/16 v3, 0x3b

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 364
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xd

    const/16 v3, 0x3b

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 365
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xe

    const/16 v3, 0x3e7

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 366
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    .line 367
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getEndOfYear(J)J
    .locals 5
    .param p0, "time"    # J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 373
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    monitor-enter v1

    .line 375
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 376
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/4 v2, 0x2

    sget-object v3, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 377
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/4 v2, 0x5

    const/16 v3, 0x1f

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 378
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xb

    const/16 v3, 0x17

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 379
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xc

    const/16 v3, 0x3b

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 380
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xd

    const/16 v3, 0x3b

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 381
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xe

    const/16 v3, 0x3e7

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 382
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    .line 383
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getNextDateOfData(Landroid/content/Context;Ljava/util/Date;)Ljava/util/Date;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "date"    # Ljava/util/Date;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v5, 0x0

    .line 270
    new-array v2, v10, [Ljava/lang/String;

    const-string/jumbo v0, "sample_time"

    aput-object v0, v2, v5

    .line 271
    .local v2, "projection":[Ljava/lang/String;
    const-string/jumbo v3, "sample_time>? AND sample_time <? "

    .line 272
    .local v3, "selectionClause":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const-string v0, ""

    aput-object v0, v4, v5

    const-string v0, ""

    aput-object v0, v4, v10

    .line 273
    .local v4, "mSelectionArg":[Ljava/lang/String;
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->getEndOfDay(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 274
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->getEndOfDay(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v10

    .line 275
    const/4 v6, 0x0

    .line 278
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    const-string/jumbo v5, "sample_time ASC  LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 279
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 281
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 282
    const-string/jumbo v0, "sample_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 283
    .local v8, "time":J
    new-instance v7, Ljava/util/Date;

    invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 293
    .local v7, "foundDate":Ljava/util/Date;
    if-eqz v6, :cond_0

    .line 295
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v7    # "foundDate":Ljava/util/Date;
    .end local v8    # "time":J
    :cond_0
    :goto_0
    return-object v7

    .line 288
    :cond_1
    const/4 v7, 0x0

    .line 293
    if-eqz v6, :cond_0

    .line 295
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 293
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 295
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public static getPrevDateOfData(Landroid/content/Context;Ljava/util/Date;)Ljava/util/Date;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "date"    # Ljava/util/Date;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 238
    new-array v2, v1, [Ljava/lang/String;

    const-string/jumbo v0, "sample_time"

    aput-object v0, v2, v5

    .line 239
    .local v2, "projection":[Ljava/lang/String;
    const-string/jumbo v3, "sample_time<?"

    .line 240
    .local v3, "selectionClause":Ljava/lang/String;
    new-array v4, v1, [Ljava/lang/String;

    const-string v0, ""

    aput-object v0, v4, v5

    .line 241
    .local v4, "mSelectionArg":[Ljava/lang/String;
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->getStartOfDay(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 242
    const/4 v6, 0x0

    .line 245
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    const-string/jumbo v5, "sample_time DESC  LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 246
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 248
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 249
    const-string/jumbo v0, "sample_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 250
    .local v8, "time":J
    new-instance v7, Ljava/util/Date;

    invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 260
    .local v7, "foundDate":Ljava/util/Date;
    if-eqz v6, :cond_0

    .line 262
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v7    # "foundDate":Ljava/util/Date;
    .end local v8    # "time":J
    :cond_0
    :goto_0
    return-object v7

    .line 255
    :cond_1
    const/4 v7, 0x0

    .line 260
    if-eqz v6, :cond_0

    .line 262
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 260
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 262
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public static getRecentWeight(Landroid/content/Context;Ljava/util/Date;)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "date"    # Ljava/util/Date;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 204
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const-string/jumbo v0, "weight"

    aput-object v0, v2, v5

    const-string/jumbo v0, "sample_time"

    aput-object v0, v2, v8

    const-string v0, "body_mass_index"

    aput-object v0, v2, v9

    const/4 v0, 0x3

    const-string v1, "height"

    aput-object v1, v2, v0

    .line 205
    .local v2, "projection":[Ljava/lang/String;
    const-string/jumbo v3, "sample_time >?  AND sample_time <? "

    .line 206
    .local v3, "selectionClause":Ljava/lang/String;
    new-array v4, v9, [Ljava/lang/String;

    const-string v0, ""

    aput-object v0, v4, v5

    const-string v0, ""

    aput-object v0, v4, v8

    .line 207
    .local v4, "mSelectionArgs":[Ljava/lang/String;
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->getStartOfDay(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 208
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->getEndOfDay(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    .line 209
    const/4 v6, 0x0

    .line 212
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "_id DESC  LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 213
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 215
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 216
    new-instance v7, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    const-string/jumbo v0, "weight"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    const-string/jumbo v1, "sample_time"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-direct {v7, v0, v8, v9}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;-><init>(FJ)V

    .line 217
    .local v7, "weight":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    const-string v0, "body_mass_index"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    invoke-virtual {v7, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->setBodyMassIndex(F)V

    .line 218
    const-string v0, "height"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    invoke-virtual {v7, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->setHeight(F)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 228
    if-eqz v6, :cond_0

    .line 230
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v7    # "weight":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    :cond_0
    :goto_0
    return-object v7

    .line 223
    :cond_1
    const/4 v7, 0x0

    .line 228
    if-eqz v6, :cond_0

    .line 230
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 228
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 230
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public static getStartOfDay(J)J
    .locals 4
    .param p0, "time"    # J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 323
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    monitor-enter v1

    .line 325
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 326
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xb

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 327
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xc

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 328
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xd

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 329
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xe

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 330
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    .line 331
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getStartOfHour(J)J
    .locals 4
    .param p0, "time"    # J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 303
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    monitor-enter v1

    .line 305
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 306
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xc

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 307
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xd

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 308
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xe

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 309
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    .line 310
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getStartOfMonth(J)J
    .locals 4
    .param p0, "time"    # J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 389
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    monitor-enter v1

    .line 391
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 392
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 393
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xb

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 394
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xc

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 395
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xd

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 396
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xe

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 397
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    .line 398
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getStartOfWeekFromDay(JI)J
    .locals 4
    .param p0, "time"    # J
    .param p2, "day"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 412
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    monitor-enter v1

    .line 414
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 415
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xb

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 416
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xc

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 417
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xd

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 418
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xe

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 419
    :goto_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/4 v2, 0x7

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-eq v0, p2, :cond_0

    .line 421
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/4 v2, 0x6

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->add(II)V

    goto :goto_0

    .line 424
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 423
    :cond_0
    :try_start_1
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-wide v2
.end method

.method public static getStartOfYear(J)J
    .locals 4
    .param p0, "time"    # J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 437
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    monitor-enter v1

    .line 439
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 440
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xb

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 441
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xc

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 442
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xd

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 443
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xe

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 444
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    const/4 v2, 0x6

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 445
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    .line 446
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getWeightData(Landroid/content/Context;JJLcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$IOnDBDataFetchListener;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "start_time"    # J
    .param p3, "end_time"    # J
    .param p5, "operation"    # Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$Operation;
    .param p6, "periodType"    # Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;
    .param p7, "responseListener"    # Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$IOnDBDataFetchListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 460
    const-string v0, "WeightUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " start_time "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " end_time "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5, p3, p4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 461
    const/4 v1, 0x0

    .line 462
    .local v1, "projection":[Ljava/lang/String;
    const-string/jumbo v2, "sample_time>?  AND sample_time<? "

    .line 463
    .local v2, "selectionClause":Ljava/lang/String;
    const/4 v3, 0x0

    .line 464
    .local v3, "mSelectionArgs":[Ljava/lang/String;
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;->HOUR:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    if-ne p6, v0, :cond_1

    .line 466
    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    .end local v1    # "projection":[Ljava/lang/String;
    const/4 v0, 0x0

    const-string/jumbo v4, "weight"

    aput-object v4, v1, v0

    const/4 v0, 0x1

    const-string/jumbo v4, "sample_time"

    aput-object v4, v1, v0

    .line 467
    .restart local v1    # "projection":[Ljava/lang/String;
    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/String;

    .end local v3    # "mSelectionArgs":[Ljava/lang/String;
    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x1

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    .restart local v3    # "mSelectionArgs":[Ljava/lang/String;
    :cond_0
    :goto_0
    move-object v0, p0

    move-object v4, p7

    move-object v5, p6

    .line 481
    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->runQuery(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$IOnDBDataFetchListener;Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;)V

    .line 482
    return-void

    .line 469
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;->DAY:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    if-ne p6, v0, :cond_2

    .line 471
    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    .end local v1    # "projection":[Ljava/lang/String;
    const/4 v0, 0x0

    const-string/jumbo v4, "weight"

    aput-object v4, v1, v0

    const/4 v0, 0x1

    const-string/jumbo v4, "sample_time"

    aput-object v4, v1, v0

    .line 472
    .restart local v1    # "projection":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ") GROUP BY (strftime(\"%d-%m-%Y\",([Weight].[sample_time]/1000),\'unixepoch\')"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 473
    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/String;

    .end local v3    # "mSelectionArgs":[Ljava/lang/String;
    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x1

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    .restart local v3    # "mSelectionArgs":[Ljava/lang/String;
    goto :goto_0

    .line 475
    :cond_2
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;->MONTH:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    if-ne p6, v0, :cond_0

    .line 477
    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    .end local v1    # "projection":[Ljava/lang/String;
    const/4 v0, 0x0

    const-string v4, " AVG (weight ) AS AVGWEIGHT"

    aput-object v4, v1, v0

    const/4 v0, 0x1

    const-string/jumbo v4, "sample_time"

    aput-object v4, v1, v0

    .line 478
    .restart local v1    # "projection":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ") GROUP BY (strftime(\"%m-%Y\",([Weight].[sample_time]/1000),\'unixepoch\')"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 479
    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/String;

    .end local v3    # "mSelectionArgs":[Ljava/lang/String;
    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x1

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    .restart local v3    # "mSelectionArgs":[Ljava/lang/String;
    goto :goto_0
.end method

.method public static getWeights(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 3
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 117
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 119
    .local v1, "weightList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;>;"
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 120
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_0

    .line 122
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->cursorToWeight(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v0

    .line 123
    .local v0, "weight":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 126
    .end local v0    # "weight":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    :cond_0
    return-object v1
.end method

.method public static insertItem(Landroid/content/Context;IFFLjava/lang/String;JI)J
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "sensorId"    # I
    .param p2, "height"    # F
    .param p3, "value"    # F
    .param p4, "comment"    # Ljava/lang/String;
    .param p5, "sampleTime"    # J
    .param p7, "entryType"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 181
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 182
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "height"

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 183
    const-string/jumbo v2, "weight"

    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 184
    const-string v2, "comment"

    invoke-virtual {v1, v2, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    const-string/jumbo v2, "sample_time"

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 186
    const-string v2, "input_source_type"

    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 191
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 192
    .local v0, "rawContactUri":Landroid/net/Uri;
    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    return-wide v2
.end method

.method private static runQuery(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$IOnDBDataFetchListener;Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selectionClause"    # Ljava/lang/String;
    .param p3, "mSelectionArgs"    # [Ljava/lang/String;
    .param p4, "responseListener"    # Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$IOnDBDataFetchListener;
    .param p5, "periodType"    # Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 487
    const/4 v7, 0x0

    .line 488
    .local v7, "v":Ljava/lang/Void;
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p5

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils$1;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$IOnDBDataFetchListener;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Void;

    const/4 v2, 0x0

    aput-object v7, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 533
    return-void
.end method

.method public static updateWeightWithID(Landroid/content/Context;Ljava/lang/String;FLjava/lang/String;)I
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "wvalue"    # F
    .param p3, "comment"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 583
    const/4 v2, 0x0

    .line 584
    .local v2, "updatedRows":I
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 585
    .local v3, "values":Landroid/content/ContentValues;
    const-string/jumbo v4, "weight"

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 586
    const-string v4, "comment"

    invoke-virtual {v3, v4, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 587
    const-string v1, "_id=?"

    .line 588
    .local v1, "selectionClause":Ljava/lang/String;
    const/4 v4, 0x1

    new-array v0, v4, [Ljava/lang/String;

    const-string v4, ""

    aput-object v4, v0, v6

    .line 589
    .local v0, "mSelectionArgs":[Ljava/lang/String;
    aput-object p1, v0, v6

    .line 590
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v3, v1, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 592
    return v2
.end method
