.class public Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;
.super Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;
.source "FoodLogListConcreteDelegate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$4;,
        Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$SkippedCalculatorTask;,
        Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$TalkBackRunnable;,
        Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$ImageDrawingResultListener;,
        Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate",
        "<",
        "Lcom/sec/android/app/shealth/food/fooddao/MealDao;",
        ">;"
    }
.end annotation


# static fields
.field public static final DEFAULT_LOGLIST_IMAGE_RESOURCE:I = 0x7f0203b0

.field private static final GROUP_TEXT_FORMAT:Ljava/lang/String; = "%s (%s %s %s)"

.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private dateFormatterNew:Ljava/text/SimpleDateFormat;

.field private mCheckChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private mFilter:Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;

.field private mFoodDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

.field private mIsScrolling:Z

.field private mMaskDrawingCallbackProvider:Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/shealth/food/mask/CallbacksProvider",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;",
            ">;"
        }
    .end annotation
.end field

.field private mMaskedImageCollector:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

.field private mMealImageDao:Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;

.field private mMealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

.field private final timeFormat:Ljava/text/DateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 86
    const-class v0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 108
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;-><init>(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;)V

    .line 92
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, " dd"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->dateFormatterNew:Ljava/text/SimpleDateFormat;

    .line 507
    new-instance v0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$3;-><init>(Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mCheckChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 109
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mIsScrolling:Z

    .line 110
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/MealImageDaoImplDb;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/food/fooddao/MealImageDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mMealImageDao:Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;

    .line 111
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/MealItemDaoImplDb;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mMealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    .line 112
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mFoodDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    move-object v0, p1

    .line 113
    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/app/IFoodMaskedImageCollectorHolder;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/app/IFoodMaskedImageCollectorHolder;->getMaskedImageCollector()Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mMaskedImageCollector:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

    .line 114
    invoke-static {p1}, Lcom/sec/android/app/shealth/common/utils/DateFormatHelper;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->timeFormat:Ljava/text/DateFormat;

    .line 115
    new-instance v0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$1;-><init>(Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mMaskDrawingCallbackProvider:Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;

    .line 122
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;)Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mMaskedImageCollector:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;JJ)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;
    .param p1, "x1"    # J
    .param p3, "x2"    # J

    .prologue
    .line 85
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->countFoodSumForDay(JJ)I

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;)Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mMaskDrawingCallbackProvider:Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;)Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mFoodDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;)Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mMealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    return-object v0
.end method

.method private countFoodSumForDay(JJ)I
    .locals 6
    .param p1, "startOfDayTime"    # J
    .param p3, "endOfDayTime"    # J

    .prologue
    .line 450
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mDao:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;

    instance-of v4, v4, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;

    if-eqz v4, :cond_0

    .line 451
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mDao:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;

    check-cast v4, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;

    invoke-virtual {v4, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;->getDataForPeriodExclusive(JJ)Ljava/util/List;

    move-result-object v2

    .line 452
    .local v2, "mealDatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;>;"
    const/4 v3, 0x0

    .line 453
    .local v3, "sum":I
    if-eqz v2, :cond_1

    .line 454
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 455
    .local v1, "mealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    int-to-float v4, v3

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getKcal()F

    move-result v5

    add-float/2addr v4, v5

    float-to-int v3, v4

    .line 456
    goto :goto_0

    .line 461
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "mealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    .end local v2    # "mealDatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;>;"
    .end local v3    # "sum":I
    :cond_0
    const/4 v3, 0x0

    :cond_1
    return v3
.end method

.method private getContractColumnIndex(Landroid/database/Cursor;Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;)I
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "contract"    # Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    .prologue
    .line 466
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mDao:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;

    check-cast v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    invoke-interface {v0, p2}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->getContractColumnName(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private getGroupText(Landroid/content/Context;FJZ)Ljava/lang/String;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "monthAverageValue"    # F
    .param p3, "monthStartTime"    # J
    .param p5, "isFullname"    # Z

    .prologue
    .line 423
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget-object v3, v4, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 424
    .local v3, "systemLocale":Ljava/util/Locale;
    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 426
    .local v2, "strLanguage":Ljava/lang/String;
    if-eqz p5, :cond_0

    .line 427
    invoke-virtual {p0, p1, p3, p4}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->getMonthGroupDateString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    .line 428
    .local v1, "dateString":Ljava/lang/String;
    const v4, 0x7f090b9a

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 439
    .local v0, "averageString":Ljava/lang/String;
    :goto_0
    const-string v4, "%s (%s %s %s)"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    const/4 v6, 0x1

    aput-object v0, v5, v6

    const/4 v6, 0x2

    float-to-double v7, p2

    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/logutils/graph/GraphLogUtils;->convertCaloriesToString(D)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    const v7, 0x7f0900b9

    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 431
    .end local v0    # "averageString":Ljava/lang/String;
    .end local v1    # "dateString":Ljava/lang/String;
    :cond_0
    if-eqz v2, :cond_1

    const-string v4, "fi"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 432
    invoke-virtual {p0, p1, p3, p4}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->getMonthGroupDateString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    .line 437
    .restart local v1    # "dateString":Ljava/lang/String;
    :goto_1
    const v4, 0x7f090ae0

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "averageString":Ljava/lang/String;
    goto :goto_0

    .line 434
    .end local v0    # "averageString":Ljava/lang/String;
    .end local v1    # "dateString":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0, p1, p3, p4}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->getMonthGroupDateString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "dateString":Ljava/lang/String;
    goto :goto_1
.end method

.method private updateRowImage(Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;ZJ)V
    .locals 14
    .param p1, "rowView"    # Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;
    .param p2, "isRowChanged"    # Z
    .param p3, "mealId"    # J

    .prologue
    .line 285
    iget-object v10, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mMaskDrawingCallbackProvider:Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;->removeCallback(Ljava/lang/Object;)V

    .line 286
    iget-object v10, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mMealImageDao:Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;

    move-wide/from16 v0, p3

    invoke-interface {v10, v0, v1}, Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;->getImagesListByMealId(J)Ljava/util/List;

    move-result-object v6

    .line 287
    .local v6, "mealImageDatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;>;"
    new-instance v7, Ljava/util/ArrayList;

    new-instance v10, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$2;

    invoke-direct {v10, p0}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$2;-><init>(Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;)V

    invoke-static {v6, v10}, Lcom/sec/android/app/shealth/common/utils/Utils;->filterCollection(Ljava/util/Collection;Lcom/sec/android/app/shealth/common/utils/Filterable;)Ljava/util/Collection;

    move-result-object v10

    invoke-direct {v7, v10}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 295
    .end local v6    # "mealImageDatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;>;"
    .local v7, "mealImageDatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;>;"
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v5

    .line 300
    .local v5, "isImagesListEmpty":Z
    if-nez v5, :cond_3

    .line 302
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 303
    .local v2, "bitmapList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    const/4 v10, 0x4

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v11

    invoke-static {v10, v11}, Ljava/lang/Math;->min(II)I

    move-result v10

    if-ge v4, v10, :cond_0

    .line 304
    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;

    invoke-virtual {v10}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->getFilePath()Ljava/lang/String;

    move-result-object v8

    .line 305
    .local v8, "path":Ljava/lang/String;
    invoke-interface {v2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 303
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 307
    .end local v8    # "path":Ljava/lang/String;
    :cond_0
    new-instance v3, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$ImageDrawingResultListener;

    move-wide/from16 v0, p3

    invoke-direct {v3, p0, p1, v0, v1}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$ImageDrawingResultListener;-><init>(Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;J)V

    .line 308
    .local v3, "drawingResultListener":Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;
    iget-object v10, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mMaskDrawingCallbackProvider:Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v10, v11, v3}, Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;->addCallBack(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 309
    iget-object v10, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mMaskedImageCollector:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v11

    invoke-static {v11}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;->getIdBySquaresCount(I)I

    move-result v11

    invoke-virtual {v10, v11, v2, v3}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->registerTask(ILjava/util/List;Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;)V

    .line 313
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 314
    .local v9, "path":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v4, 0x0

    :goto_1
    const/4 v10, 0x4

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v11

    invoke-static {v10, v11}, Ljava/lang/Math;->min(II)I

    move-result v10

    if-ge v4, v10, :cond_1

    .line 316
    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;

    invoke-virtual {v10}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->getFilePath()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 314
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 318
    :cond_1
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v10

    if-lez v10, :cond_2

    .line 320
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getLeftImageView()Landroid/widget/ImageView;

    move-result-object v10

    sget-object v11, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_MULTI_PIC:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    invoke-static {v10, v11, v9}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/util/List;)V

    .line 328
    .end local v2    # "bitmapList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "drawingResultListener":Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;
    .end local v4    # "i":I
    .end local v9    # "path":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    :goto_2
    return-void

    .line 326
    :cond_3
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getLeftImageView()Landroid/widget/ImageView;

    move-result-object v10

    sget-object v11, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_NONE:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    const-string v12, ""

    const-string v13, ""

    invoke-static {v10, v11, v12, v13}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method


# virtual methods
.method public bindChildView(Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 23
    .param p1, "rowView"    # Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "isRowChanged"    # Z

    .prologue
    .line 192
    invoke-super/range {p0 .. p4}, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->bindChildView(Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;Landroid/content/Context;Landroid/database/Cursor;Z)V

    .line 193
    sget-object v3, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->SAMPLE_TIME:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->getContractColumnIndex(Landroid/database/Cursor;Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 194
    .local v7, "cursorTime":J
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->getIdColumnName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    .line 195
    .local v13, "mealId":J
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getLeftImageView()Landroid/widget/ImageView;

    move-result-object v3

    const v4, 0x7f0203b0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 197
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->isCursorSetOnFirstMeasureInDay(Landroid/database/Cursor;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 199
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v15

    .line 200
    .local v15, "calendar":Ljava/util/Calendar;
    invoke-virtual {v15, v7, v8}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 201
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->dateFormatterNew:Ljava/text/SimpleDateFormat;

    invoke-virtual {v15}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v19

    .line 202
    .local v19, "dateNew":Ljava/lang/String;
    new-instance v3, Ljava/text/DateFormatSymbols;

    invoke-direct {v3}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v3}, Ljava/text/DateFormatSymbols;->getMonths()[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v15, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    aget-object v18, v3, v4

    .line 203
    .local v18, "dMonth":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v20

    .line 204
    .local v20, "language":Ljava/lang/String;
    const-string v3, "ko"

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 205
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getLeftTeamHeaderTextView()Landroid/widget/TextView;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x2

    invoke-virtual {v15, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090212

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090213

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/text/DateFormatSymbols;

    invoke-direct {v5}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v5}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x7

    invoke-virtual {v15, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 217
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getLeftTeamHeaderTextView()Landroid/widget/TextView;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->getDayGroupDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v4

    invoke-virtual {v15}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 219
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->setTeamHeaderVisible(Z)V

    .line 220
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mIsScrolling:Z

    if-nez v3, :cond_0

    .line 221
    new-instance v3, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;

    move-object/from16 v4, p0

    move-object/from16 v5, p2

    move-object/from16 v6, p1

    move/from16 v9, p4

    invoke-direct/range {v3 .. v9}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;-><init>(Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;Landroid/content/Context;Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;JZ)V

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Void;

    const/4 v6, 0x0

    const/4 v4, 0x0

    check-cast v4, Ljava/lang/Void;

    aput-object v4, v5, v6

    invoke-virtual {v3, v5}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$CalorieCounterTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 229
    .end local v15    # "calendar":Ljava/util/Calendar;
    .end local v18    # "dMonth":Ljava/lang/String;
    .end local v19    # "dateNew":Ljava/lang/String;
    .end local v20    # "language":Ljava/lang/String;
    :cond_0
    :goto_1
    const v3, 0x7f080663

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/CheckBox;

    .line 230
    .local v16, "cb":Landroid/widget/CheckBox;
    if-eqz v16, :cond_1

    .line 232
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mCheckChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 238
    :cond_1
    const-string v3, "0"

    sget-object v4, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->TOTAL_KILO_CALORIE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->getContractColumnIndex(Landroid/database/Cursor;Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;)I

    move-result v4

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 241
    new-instance v9, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$SkippedCalculatorTask;

    sget-object v3, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->TOTAL_KILO_CALORIE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->getContractColumnIndex(Landroid/database/Cursor;Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v10, p0

    move-object/from16 v11, p1

    invoke-direct/range {v9 .. v14}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$SkippedCalculatorTask;-><init>(Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;Ljava/lang/String;J)V

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Void;

    const/4 v5, 0x0

    const/4 v3, 0x0

    check-cast v3, Ljava/lang/Void;

    aput-object v3, v4, v5

    invoke-virtual {v9, v4}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$SkippedCalculatorTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 258
    :goto_2
    sget-object v3, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->TYPE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->getContractColumnIndex(Landroid/database/Cursor;Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    .line 259
    .local v22, "mealType":I
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getBottomTextView()Landroid/widget/TextView;

    move-result-object v21

    .line 260
    .local v21, "mealTextView":Landroid/widget/TextView;
    invoke-static/range {v22 .. v22}, Lcom/sec/android/app/shealth/food/constants/MealType;->getMealTypeByMealTypeId(I)Lcom/sec/android/app/shealth/food/constants/MealType;

    move-result-object v3

    iget v3, v3, Lcom/sec/android/app/shealth/food/constants/MealType;->textResourceId:I

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 261
    const/4 v3, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 262
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getRightTextView()Landroid/widget/TextView;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->timeFormat:Ljava/text/DateFormat;

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 264
    sget-object v3, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->COMMENT:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->getContractColumnIndex(Landroid/database/Cursor;Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 265
    .local v17, "comment":Ljava/lang/String;
    if-eqz v17, :cond_7

    const-string v3, ""

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_7

    .line 267
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/common/utils/Utils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    const/4 v3, 0x1

    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->setMemoImageVisible(Z)V

    .line 268
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getMemoImageVisible()Landroid/widget/ImageView;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_USER_CUSTOM:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    const v5, 0x7f09007c

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-static {v3, v4, v5, v0}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    :goto_4
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->setLeftImageVisible(Z)V

    .line 276
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mIsScrolling:Z

    if-nez v3, :cond_2

    .line 277
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p4

    invoke-direct {v0, v1, v2, v13, v14}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->updateRowImage(Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;ZJ)V

    .line 282
    :cond_2
    return-void

    .line 214
    .end local v16    # "cb":Landroid/widget/CheckBox;
    .end local v17    # "comment":Ljava/lang/String;
    .end local v21    # "mealTextView":Landroid/widget/TextView;
    .end local v22    # "mealType":I
    .restart local v15    # "calendar":Ljava/util/Calendar;
    .restart local v18    # "dMonth":Ljava/lang/String;
    .restart local v19    # "dateNew":Ljava/lang/String;
    .restart local v20    # "language":Ljava/lang/String;
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getLeftTeamHeaderTextView()Landroid/widget/TextView;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/text/DateFormatSymbols;

    invoke-direct {v5}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v5}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x7

    invoke-virtual {v15, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 225
    .end local v15    # "calendar":Ljava/util/Calendar;
    .end local v18    # "dMonth":Ljava/lang/String;
    .end local v19    # "dateNew":Ljava/lang/String;
    .end local v20    # "language":Ljava/lang/String;
    :cond_4
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->setTeamHeaderVisible(Z)V

    goto/16 :goto_1

    .line 245
    .restart local v16    # "cb":Landroid/widget/CheckBox;
    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getLeftTextView()Landroid/widget/TextView;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->TOTAL_KILO_CALORIE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->getContractColumnIndex(Landroid/database/Cursor;Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;)I

    move-result v4

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 248
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getLeftTextView()Landroid/widget/TextView;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->TOTAL_KILO_CALORIE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->getContractColumnIndex(Landroid/database/Cursor;Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;)I

    move-result v4

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 251
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getCenterTextView()Landroid/widget/TextView;

    move-result-object v3

    const v4, 0x7f0900b9

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 252
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getCenterTextView()Landroid/widget/TextView;

    move-result-object v3

    const v4, 0x7f0900b9

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 254
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getCenterTextView()Landroid/widget/TextView;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 267
    .restart local v17    # "comment":Ljava/lang/String;
    .restart local v21    # "mealTextView":Landroid/widget/TextView;
    .restart local v22    # "mealType":I
    :cond_6
    const/4 v3, 0x0

    goto/16 :goto_3

    .line 272
    :cond_7
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->setMemoImageVisible(Z)V

    goto/16 :goto_4
.end method

.method public bindGroupView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 14
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "isExpanded"    # Z

    .prologue
    .line 381
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;

    move-object v8, v0

    .line 382
    .local v8, "groupHeaderView":Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;
    sget-object v1, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->SAMPLE_TIME:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    move-object/from16 v0, p3

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->getContractColumnIndex(Landroid/database/Cursor;Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;)I

    move-result v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 384
    .local v4, "monthStartTime":J
    sget-object v1, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->SUM_TOTAL_KILO_CALORIE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    move-object/from16 v0, p3

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->getContractColumnIndex(Landroid/database/Cursor;Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;)I

    move-result v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v10

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfMonth(J)J

    move-result-wide v12

    invoke-static {v10, v11, v12, v13}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getDataPresentCountInPeriod(JJ)I

    move-result v2

    int-to-float v2, v2

    div-float v3, v1, v2

    .line 387
    .local v3, "monthAverageValue":F
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;->getTextView()Landroid/widget/TextView;

    move-result-object v9

    .line 388
    .local v9, "groupText":Landroid/widget/TextView;
    const/4 v6, 0x0

    move-object v1, p0

    move-object/from16 v2, p2

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->getGroupText(Landroid/content/Context;FJZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 389
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x1

    move-object v1, p0

    move-object/from16 v2, p2

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->getGroupText(Landroid/content/Context;FJZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f09021c

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p4, :cond_0

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v6, 0x7f090217

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 404
    if-eqz p4, :cond_1

    .line 406
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;->getExpandButton()Landroid/widget/ImageButton;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v10, 0x7f09021a

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v10, ""

    invoke-static {v1, v2, v6, v10}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    :goto_1
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;->getExpandButton()Landroid/widget/ImageButton;

    move-result-object v1

    move/from16 v0, p4

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 417
    .end local v3    # "monthAverageValue":F
    .end local v4    # "monthStartTime":J
    .end local v8    # "groupHeaderView":Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;
    .end local v9    # "groupText":Landroid/widget/TextView;
    :goto_2
    return-void

    .line 389
    .restart local v3    # "monthAverageValue":F
    .restart local v4    # "monthStartTime":J
    .restart local v8    # "groupHeaderView":Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;
    .restart local v9    # "groupText":Landroid/widget/TextView;
    :cond_0
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v6, 0x7f0901ef

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 410
    :cond_1
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;->getExpandButton()Landroid/widget/ImageButton;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v10, 0x7f09021b

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v10, ""

    invoke-static {v1, v2, v6, v10}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 414
    .end local v3    # "monthAverageValue":F
    .end local v4    # "monthStartTime":J
    .end local v8    # "groupHeaderView":Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;
    .end local v9    # "groupText":Landroid/widget/TextView;
    :catch_0
    move-exception v7

    .line 415
    .local v7, "ex":Ljava/lang/ClassCastException;
    sget-object v1, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->LOG_TAG:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public getChildCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 24
    .param p1, "groupCursor"    # Landroid/database/Cursor;

    .prologue
    .line 159
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 160
    .local v8, "projection":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;>;"
    sget-object v2, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->_ID:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    invoke-interface {v8, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 161
    sget-object v2, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->SAMPLE_TIME:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    invoke-interface {v8, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 162
    sget-object v2, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->TOTAL_KILO_CALORIE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    invoke-interface {v8, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 163
    sget-object v2, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->TYPE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    invoke-interface {v8, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 164
    sget-object v2, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->COMMENT:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    invoke-interface {v8, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 165
    sget-object v2, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->SAMPLE_TIME:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->getContractColumnIndex(Landroid/database/Cursor;Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 166
    .local v22, "monthTime":J
    invoke-static/range {v22 .. v23}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v3

    .line 167
    .local v3, "periodStart":J
    invoke-static/range {v22 .. v23}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfMonth(J)J

    move-result-wide v5

    .line 169
    .local v5, "periodEnd":J
    sget-object v2, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$4;->$SwitchMap$com$sec$android$app$shealth$logutils$log$DefaultLogFilter:[I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mFilter:Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;->ordinal()I

    move-result v7

    aget v2, v2, v7

    packed-switch v2, :pswitch_data_0

    .line 180
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v7, "Unknown LogFilter value"

    invoke-direct {v2, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 171
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mDao:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;

    check-cast v2, Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    const/4 v7, 0x0

    const/4 v9, 0x0

    sget-object v10, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;->DESC:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;

    invoke-interface/range {v2 .. v10}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->getPeriodCursorForChartAndLogList(JJLcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;Ljava/util/Set;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Landroid/database/Cursor;

    move-result-object v2

    .line 182
    :goto_0
    return-object v2

    .line 174
    :pswitch_1
    const/16 v18, 0x1

    .line 182
    .local v18, "goalAchievedStatus":Z
    :goto_1
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mDao:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;

    check-cast v9, Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    const/4 v15, 0x0

    sget-object v16, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->SUM_TOTAL_KILO_CALORIE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    sget-object v17, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;->DESC:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;

    invoke-static {}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getDefaultIntakeCalorieGoal()I

    move-result v19

    const/16 v20, 0xa

    sget-object v21, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;->DAY:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    move-wide v10, v3

    move-wide v12, v5

    move-object v14, v8

    invoke-interface/range {v9 .. v21}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->getPeriodCursorWithGoalFilter(JJLjava/util/Set;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;ZIILcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;)Landroid/database/Cursor;

    move-result-object v2

    goto :goto_0

    .line 177
    .end local v18    # "goalAchievedStatus":Z
    :pswitch_2
    const/16 v18, 0x0

    .line 178
    .restart local v18    # "goalAchievedStatus":Z
    goto :goto_1

    .line 169
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getCommentColumnName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 481
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mDao:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;

    check-cast v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    sget-object v1, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->COMMENT:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->getContractColumnName(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 445
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$Meal;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method public getCreateTimeColumnName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 471
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mDao:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;

    check-cast v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    sget-object v1, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->SAMPLE_TIME:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->getContractColumnName(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGroupCursor(Lcom/sec/android/app/shealth/logutils/log/LogFilter;)Landroid/database/Cursor;
    .locals 14
    .param p1, "logFilter"    # Lcom/sec/android/app/shealth/logutils/log/LogFilter;

    .prologue
    .line 126
    instance-of v0, p1, Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;

    if-nez v0, :cond_0

    .line 127
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The filter supported only DefaultLogFilter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 129
    :cond_0
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 130
    .local v6, "projection":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;>;"
    sget-object v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->_ID:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 131
    sget-object v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->SAMPLE_TIME:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 132
    sget-object v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->SUM_TOTAL_KILO_CALORIE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 134
    check-cast p1, Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;

    .end local p1    # "logFilter":Lcom/sec/android/app/shealth/logutils/log/LogFilter;
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mFilter:Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;

    .line 135
    sget-object v0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$4;->$SwitchMap$com$sec$android$app$shealth$logutils$log$DefaultLogFilter:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mFilter:Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 146
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unknown LogFilter value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 137
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mDao:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;

    check-cast v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    const-wide/high16 v1, -0x8000000000000000L

    const-wide v3, 0x7fffffffffffffffL

    sget-object v5, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;->MONTH:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    const/4 v7, 0x0

    sget-object v8, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;->DESC:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;

    invoke-interface/range {v0 .. v8}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->getPeriodCursorForChartAndLogList(JJLcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;Ljava/util/Set;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Landroid/database/Cursor;

    move-result-object v0

    .line 151
    :goto_0
    return-object v0

    .line 140
    :pswitch_1
    const/4 v10, 0x1

    .line 148
    .local v10, "goalAchievedStatus":Z
    :goto_1
    sget-object v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->TYPE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 149
    sget-object v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->COMMENT:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 151
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mDao:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;

    check-cast v1, Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    const-wide/high16 v2, -0x8000000000000000L

    const-wide v4, 0x7fffffffffffffffL

    const/4 v7, 0x0

    sget-object v8, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->SUM_TOTAL_KILO_CALORIE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    sget-object v9, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;->DESC:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;

    invoke-static {}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getDefaultIntakeCalorieGoal()I

    move-result v11

    const/16 v12, 0xa

    sget-object v13, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;->MONTH:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    invoke-interface/range {v1 .. v13}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->getPeriodCursorWithGoalFilter(JJLjava/util/Set;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;ZIILcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 143
    .end local v10    # "goalAchievedStatus":Z
    :pswitch_2
    const/4 v10, 0x0

    .line 144
    .restart local v10    # "goalAchievedStatus":Z
    goto :goto_1

    .line 135
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getIdColumnName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 476
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mDao:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;

    check-cast v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    sget-object v1, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->_ID:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->getContractColumnName(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setScrollEnabled(Z)V
    .locals 0
    .param p1, "isScrolling"    # Z

    .prologue
    .line 541
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mIsScrolling:Z

    .line 542
    return-void
.end method
