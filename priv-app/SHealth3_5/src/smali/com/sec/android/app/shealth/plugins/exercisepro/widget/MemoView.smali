.class public Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;
.super Landroid/widget/LinearLayout;
.source "MemoView.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mTitleTxt:Landroid/widget/TextView;

.field private memoEditText:Landroid/widget/EditText;

.field private memoIcon:Landroid/widget/ImageView;

.field private memoMealInput:Landroid/widget/FrameLayout;

.field private memoTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->mContext:Landroid/content/Context;

    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->initLayout()V

    .line 42
    return-void
.end method

.method private initLayout()V
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->mContext:Landroid/content/Context;

    const v1, 0x7f030195

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 47
    const v0, 0x7f0803e6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->mTitleTxt:Landroid/widget/TextView;

    .line 48
    const v0, 0x7f0803e5

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->memoTextView:Landroid/widget/TextView;

    .line 49
    const v0, 0x7f0803e3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->memoEditText:Landroid/widget/EditText;

    .line 50
    const v0, 0x7f0803e4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->memoIcon:Landroid/widget/ImageView;

    .line 51
    const v0, 0x7f0803e2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->memoMealInput:Landroid/widget/FrameLayout;

    .line 53
    return-void
.end method


# virtual methods
.method public getEditText()Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->memoEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->memoEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setEditMode(Z)V
    .locals 3
    .param p1, "isEditMode"    # Z

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 56
    if-eqz p1, :cond_2

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->mTitleTxt:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->memoTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->memoIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->memoIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->memoEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->memoMealInput:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setFocusable(Z)V

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->memoEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 84
    :cond_1
    :goto_0
    return-void

    .line 75
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->mTitleTxt:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->memoEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->memoEditText:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->memoTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->memoMealInput:Landroid/widget/FrameLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setFocusable(Z)V

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->memoIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->memoIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setText(Ljava/lang/String;)V
    .locals 5
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 88
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 90
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->memoEditText:Landroid/widget/EditText;

    invoke-virtual {v1, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->memoEditText:Landroid/widget/EditText;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelection(I)V

    .line 92
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 93
    .local v0, "s":Landroid/text/SpannableString;
    new-instance v1, Landroid/text/style/LeadingMarginSpan$Standard;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a06e9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    invoke-direct {v1, v2, v4}, Landroid/text/style/LeadingMarginSpan$Standard;-><init>(II)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v4, v2, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 94
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/MemoView;->memoTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
