.class Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$1;
.super Ljava/lang/Object;
.source "HeartrateMoreTagActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/16 v6, 0x14

    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 62
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->access$000(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getCustomTags()Ljava/util/ArrayList;

    move-result-object v0

    .line 63
    .local v0, "customTags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v2, v6, :cond_0

    .line 64
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090f21

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v7}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 73
    :goto_0
    return-void

    .line 66
    :cond_0
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    const-class v3, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 67
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "from_more_activity"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 68
    const/high16 v2, 0x10000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 70
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    const/16 v3, 0x45c

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method
