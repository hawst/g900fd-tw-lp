.class Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;
.super Ljava/lang/Object;
.source "UnitSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/UnitSettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "UnitSettingsListItem"
.end annotation


# instance fields
.field mCategoryTitle:Ljava/lang/String;

.field mCurrentUnit:Ljava/lang/String;

.field mType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "categoryTitle"    # Ljava/lang/String;
    .param p2, "type"    # Ljava/lang/String;
    .param p3, "currentUnit"    # Ljava/lang/String;

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;->mCategoryTitle:Ljava/lang/String;

    .line 120
    iput-object p2, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;->mType:Ljava/lang/String;

    .line 121
    iput-object p3, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;->mCurrentUnit:Ljava/lang/String;

    .line 122
    return-void
.end method
