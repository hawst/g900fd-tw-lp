.class Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$FoodSubCategoryResultTaskWithDialog;
.super Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$TaskWithDialog;
.source "CategoryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FoodSubCategoryResultTaskWithDialog"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$TaskWithDialog",
        "<",
        "Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final listItem:Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;

.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;)V
    .locals 1
    .param p2, "listItem"    # Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;

    .prologue
    .line 449
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$FoodSubCategoryResultTaskWithDialog;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$TaskWithDialog;-><init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$1;)V

    .line 450
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$FoodSubCategoryResultTaskWithDialog;->listItem:Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;

    .line 451
    return-void
.end method


# virtual methods
.method protected doTask()Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 463
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodSubCategoryRequest;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$FoodSubCategoryResultTaskWithDialog;->listItem:Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;->getData()Lcom/sec/android/app/shealth/food/foodpick/search/TextOnlyData;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategory;

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodSubCategoryRequest;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategory;)V

    .line 465
    .local v0, "request":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodSubCategoryRequest;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$FoodSubCategoryResultTaskWithDialog;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$900(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;->performSubCategorySearch(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodSubCategoryRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    move-result-object v1

    return-object v1
.end method

.method protected getDisplayStage()Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;
    .locals 1

    .prologue
    .line 481
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;->SUB_CATEGORIES:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;

    return-object v0
.end method

.method protected onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;)V
    .locals 6
    .param p1, "result"    # Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;

    .prologue
    .line 470
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$FoodSubCategoryResultTaskWithDialog;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$FoodSubCategoryResultTaskWithDialog;->isAlreadyOnRequiredStage()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 477
    :cond_0
    :goto_0
    return-void

    .line 473
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;->getRequest()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodSubCategoryRequest;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodSubCategoryRequest;->getCategory()Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategory;->getName()Ljava/lang/String;

    move-result-object v3

    .line 474
    .local v3, "categoryName":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$FoodSubCategoryResultTaskWithDialog;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;->getSearchResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->createCategoryAdapter(Ljava/util/List;)Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;
    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$1000(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;Ljava/util/List;)Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;

    move-result-object v1

    .line 475
    .local v1, "categoryAdapter":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter<Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$FoodSubCategoryResultTaskWithDialog;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$FoodSubCategoryResultTaskWithDialog;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mOnSubCategoryClick:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$2200(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;

    move-result-object v2

    sget-object v4, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;->SUB_CATEGORIES:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;

    const/4 v5, 0x0

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->setDisplayedItems(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$OnScrollToEndListener;)V
    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$1300(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$OnScrollToEndListener;)V

    goto :goto_0
.end method

.method protected bridge synthetic onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;

    .prologue
    .line 445
    check-cast p1, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;

    .end local p1    # "x0":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$FoodSubCategoryResultTaskWithDialog;->onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;)V

    return-void
.end method

.method protected onTaskStarted()V
    .locals 1

    .prologue
    .line 455
    invoke-super {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$TaskWithDialog;->onTaskStarted()V

    .line 456
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$FoodSubCategoryResultTaskWithDialog;->isAlreadyOnRequiredStage()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 457
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$FoodSubCategoryResultTaskWithDialog;->onTaskCanceled()V

    .line 459
    :cond_0
    return-void
.end method
