.class public Lcom/sec/android/app/shealth/home/EditFavoritesActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "EditFavoritesActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/home/EditFavoritesActivity$7;,
        Lcom/sec/android/app/shealth/home/EditFavoritesActivity$RegistryContentObserver;,
        Lcom/sec/android/app/shealth/home/EditFavoritesActivity$DiscardDialogButtonController;
    }
.end annotation


# static fields
.field private static final ADD_FAV_REQUEST:I = 0x65

.field private static final DISCARD_CHANGES:Ljava/lang/String; = "DISCARD_CHANGES"

.field private static final MAX_APP_COUNT:I = 0x10

.field private static final TAG:Ljava/lang/String; = "EditFavorites"


# instance fields
.field private adapter:Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;

.field private gridView:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

.field private hasDataChanged:Z

.field private initialDataPLuginState:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;",
            ">;"
        }
    .end annotation
.end field

.field private initialSharedPrefState:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private initialTotalCount:I

.field private isChanged:Z

.field private isDoneClicked:Z

.field private isPaused:Z

.field private final mDialogControllerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;",
            ">;"
        }
    .end annotation
.end field

.field private mIconList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/home/data/IconData;",
            ">;"
        }
    .end annotation
.end field

.field private mOrderedFavlist:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;",
            ">;"
        }
    .end annotation
.end field

.field private registryContentObserver:Lcom/sec/android/app/shealth/home/EditFavoritesActivity$RegistryContentObserver;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->mOrderedFavlist:Ljava/util/ArrayList;

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->initialDataPLuginState:Ljava/util/ArrayList;

    .line 57
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->initialSharedPrefState:Ljava/util/HashMap;

    .line 58
    iput v1, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->initialTotalCount:I

    .line 59
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->isDoneClicked:Z

    .line 60
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->isChanged:Z

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->mIconList:Ljava/util/ArrayList;

    .line 68
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->isPaused:Z

    .line 69
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->hasDataChanged:Z

    .line 71
    new-instance v0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$1;-><init>(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->mDialogControllerMap:Ljava/util/Map;

    .line 569
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;)Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/EditFavoritesActivity;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->adapter:Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/EditFavoritesActivity;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->reInitGrid()V

    return-void
.end method

.method static synthetic access$1102(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/EditFavoritesActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->hasDataChanged:Z

    return p1
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/EditFavoritesActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->isChanged:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/EditFavoritesActivity;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->setFavOrder()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/EditFavoritesActivity;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->setDividers()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;)Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/EditFavoritesActivity;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->gridView:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/EditFavoritesActivity;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->revertChanges()V

    return-void
.end method

.method static synthetic access$702(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/EditFavoritesActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->isDoneClicked:Z

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/EditFavoritesActivity;

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->isPaused:Z

    return v0
.end method

.method private checkToShowCigna()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 105
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090029

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p0}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->getPluginRegistryDataFromName(Ljava/lang/String;Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    .line 106
    .local v1, "pluginlist":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 108
    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    .line 109
    .local v0, "appData":Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->isUseCigna()Z

    move-result v4

    if-nez v4, :cond_0

    .line 111
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->packagename:Ljava/lang/String;

    iget v6, v0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->pluginId:I

    invoke-static {v4, v5, v7, v6}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->updateAppFavoriteStatus(Landroid/content/Context;Ljava/lang/String;II)I

    .line 112
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, v0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->pluginId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->packagename:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 114
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, v0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->pluginId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->packagename:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->getOrder(Ljava/lang/String;)I

    move-result v2

    .line 115
    .local v2, "pos":I
    invoke-static {}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->getTotalFavoriteCount()I

    move-result v3

    .line 116
    .local v3, "total":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, v0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->pluginId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->packagename:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->remove(Ljava/lang/String;)V

    .line 117
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->updateOrderOnDelete(I)V

    .line 118
    add-int/lit8 v4, v3, -0x1

    invoke-static {v4}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->setTotalFavoriteCount(I)V

    .line 124
    .end local v0    # "appData":Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;
    .end local v2    # "pos":I
    .end local v3    # "total":I
    :cond_0
    return-void
.end method

.method private getandOrderFavoriteApps()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 273
    invoke-static {v8, p0}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->getAllAppRegistryDatas(ZLandroid/content/Context;)Ljava/util/List;

    move-result-object v4

    .line 275
    .local v4, "pluginlist":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    iget-object v6, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->mOrderedFavlist:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 278
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 279
    .local v2, "mFavPluginlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    if-eqz v4, :cond_1

    .line 281
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    .line 283
    .local v5, "temp":Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;
    iget v6, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->isFavorite:I

    if-ne v6, v8, :cond_0

    .line 285
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 286
    iget-object v6, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->mOrderedFavlist:Ljava/util/ArrayList;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 293
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v5    # "temp":Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v0, v6, :cond_3

    .line 295
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget v6, v6, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->pluginId:I

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v6, v6, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->packagename:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->getOrder(Ljava/lang/String;)I

    move-result v3

    .line 296
    .local v3, "order":I
    iget-object v6, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->mOrderedFavlist:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v3, v6, :cond_2

    if-ltz v3, :cond_2

    .line 297
    iget-object v6, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->mOrderedFavlist:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v3, v7}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 293
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 299
    :cond_2
    const-string v7, "EditFavorites"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Order Index Error for position="

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", plugin name= "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v6, v6, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 301
    .end local v3    # "order":I
    :cond_3
    return-void
.end method

.method private initialize()V
    .locals 1

    .prologue
    .line 90
    const v0, 0x7f080030

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->gridView:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    .line 91
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->checkToShowCigna()V

    .line 92
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->saveInitialState()V

    .line 93
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->getandOrderFavoriteApps()V

    .line 94
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->setIconsList()V

    .line 95
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->setIconsToGrid()V

    .line 96
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->setDividers()V

    .line 97
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->setDBListener()V

    .line 99
    return-void
.end method

.method private makeDiscardDialog(II)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 3
    .param p1, "title"    # I
    .param p2, "message"    # I

    .prologue
    .line 465
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v2, 0x2

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    .line 466
    .local v0, "dialog":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    return-object v1
.end method

.method private reInitGrid()V
    .locals 2

    .prologue
    .line 395
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->getandOrderFavoriteApps()V

    .line 396
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->setIconsList()V

    .line 397
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->adapter:Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->mIconList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->set(Ljava/util/List;)V

    .line 398
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->gridView:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->adapter:Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 399
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->setDividers()V

    .line 400
    return-void
.end method

.method private revertChanges()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 146
    iget-object v7, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->initialDataPLuginState:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 147
    .local v5, "size":I
    invoke-static {v11, p0}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->getAllAppRegistryDatas(ZLandroid/content/Context;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 148
    .local v0, "currentAppData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 150
    .local v6, "sizeCurrent":I
    const/4 v4, 0x0

    .local v4, "pluginId":I
    const/4 v1, 0x0

    .line 154
    .local v1, "favCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v5, :cond_3

    .line 156
    iget-object v7, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->initialDataPLuginState:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v3, v7, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->packagename:Ljava/lang/String;

    .line 157
    .local v3, "packageName":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->initialDataPLuginState:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget v4, v7, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->pluginId:I

    .line 158
    iget-object v7, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->initialDataPLuginState:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget v7, v7, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->isFavorite:I

    if-nez v7, :cond_1

    .line 160
    iget-object v7, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->initialDataPLuginState:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 162
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v3, v10, v4}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->updateAppFavoriteStatus(Landroid/content/Context;Ljava/lang/String;II)I

    .line 163
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 164
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->remove(Ljava/lang/String;)V

    .line 154
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 170
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->initialDataPLuginState:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 172
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v3, v11, v4}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->updateAppFavoriteStatus(Landroid/content/Context;Ljava/lang/String;II)I

    .line 173
    if-lt v6, v5, :cond_2

    .line 174
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->initialSharedPrefState:Ljava/util/HashMap;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-static {v8, v7}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->setOrder(Ljava/lang/String;I)V

    .line 177
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 176
    :cond_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v1}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->setOrder(Ljava/lang/String;I)V

    goto :goto_2

    .line 185
    .end local v3    # "packageName":Ljava/lang/String;
    :cond_3
    if-le v6, v5, :cond_5

    .line 186
    const/4 v2, 0x0

    :goto_3
    if-ge v2, v6, :cond_5

    .line 188
    iget-object v7, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->initialDataPLuginState:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 190
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v3, v7, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->packagename:Ljava/lang/String;

    .line 191
    .restart local v3    # "packageName":Ljava/lang/String;
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget v4, v7, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->pluginId:I

    .line 192
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v3, v10, v4}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->updateAppFavoriteStatus(Landroid/content/Context;Ljava/lang/String;II)I

    .line 193
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 194
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->remove(Ljava/lang/String;)V

    .line 186
    .end local v3    # "packageName":Ljava/lang/String;
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 197
    :cond_5
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->setTotalFavoriteCount(I)V

    .line 199
    return-void
.end method

.method private saveInitialState()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 128
    invoke-static {v8, p0}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->getAllAppRegistryDatas(ZLandroid/content/Context;)Ljava/util/List;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    iput-object v5, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->initialDataPLuginState:Ljava/util/ArrayList;

    .line 129
    iget-object v5, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->initialDataPLuginState:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 130
    .local v4, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_1

    .line 132
    iget-object v5, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->initialDataPLuginState:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->isFavorite:I

    if-ne v5, v8, :cond_0

    .line 134
    iget-object v5, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->initialDataPLuginState:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v2, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->packagename:Ljava/lang/String;

    .line 135
    .local v2, "packageName":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->initialDataPLuginState:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget v3, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->pluginId:I

    .line 136
    .local v3, "pluginId":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->getOrder(Ljava/lang/String;)I

    move-result v1

    .line 137
    .local v1, "order":I
    iget-object v5, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->initialSharedPrefState:Ljava/util/HashMap;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    .end local v1    # "order":I
    .end local v2    # "packageName":Ljava/lang/String;
    .end local v3    # "pluginId":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 140
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->getTotalFavoriteCount()I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->initialTotalCount:I

    .line 142
    return-void
.end method

.method private setDBListener()V
    .locals 4

    .prologue
    .line 203
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->registryContentObserver:Lcom/sec/android/app/shealth/home/EditFavoritesActivity$RegistryContentObserver;

    if-nez v0, :cond_0

    .line 205
    new-instance v0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$RegistryContentObserver;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$RegistryContentObserver;-><init>(Landroid/os/Handler;Lcom/sec/android/app/shealth/home/EditFavoritesActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->registryContentObserver:Lcom/sec/android/app/shealth/home/EditFavoritesActivity$RegistryContentObserver;

    .line 206
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/_private/BaseContract$AppRegistry;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->registryContentObserver:Lcom/sec/android/app/shealth/home/EditFavoritesActivity$RegistryContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 208
    :cond_0
    return-void
.end method

.method private setDividers()V
    .locals 7

    .prologue
    const v6, 0x7f08002e

    const v5, 0x7f08002d

    const v4, 0x7f08002c

    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 342
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->adapter:Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->getCount()I

    move-result v0

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    .line 344
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 345
    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 346
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 347
    const v0, 0x7f08002f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 391
    :goto_0
    return-void

    .line 350
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->adapter:Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->getCount()I

    move-result v0

    const/4 v1, 0x4

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->adapter:Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->getCount()I

    move-result v0

    const/16 v1, 0x9

    if-ge v0, v1, :cond_1

    .line 352
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 353
    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 354
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 355
    const v0, 0x7f08002f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 358
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->adapter:Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->getCount()I

    move-result v0

    if-le v0, v2, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->adapter:Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->getCount()I

    move-result v0

    const/16 v1, 0xd

    if-ge v0, v1, :cond_2

    .line 360
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 361
    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 362
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 363
    const v0, 0x7f08002f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 366
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->adapter:Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->getCount()I

    move-result v0

    const/16 v1, 0xc

    if-le v0, v1, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->adapter:Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->getCount()I

    move-result v0

    const/16 v1, 0x11

    if-ge v0, v1, :cond_3

    .line 368
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 369
    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 370
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 371
    const v0, 0x7f08002f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 384
    :cond_3
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 385
    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 386
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 387
    const v0, 0x7f08002f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method private setFavOrder()V
    .locals 5

    .prologue
    .line 556
    const-string v2, "EditFavorites"

    const-string/jumbo v3, "saving order in shared prefs now"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 557
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->adapter:Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->getItems()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 558
    .local v1, "iconList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/home/data/IconData;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 560
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/IconData;->getIconType()I

    move-result v2

    if-nez v2, :cond_0

    .line 562
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppPluginId()I

    move-result v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/IconData;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->setOrder(Ljava/lang/String;I)V

    .line 563
    const-string v3, "EditFavorites"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Order for pluginID:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "is "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 558
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 567
    :cond_1
    return-void
.end method

.method private setIconsList()V
    .locals 6

    .prologue
    .line 306
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->mIconList:Ljava/util/ArrayList;

    if-nez v3, :cond_1

    .line 338
    :cond_0
    :goto_0
    return-void

    .line 310
    :cond_1
    const-string v3, "EditFavorites"

    const-string v4, "Setting Icons List"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->mIconList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 313
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->mOrderedFavlist:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 315
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->mOrderedFavlist:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 317
    new-instance v2, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-direct {v2}, Lcom/sec/android/app/shealth/home/data/IconData;-><init>()V

    .line 318
    .local v2, "iconData":Lcom/sec/android/app/shealth/home/data/IconData;
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->mOrderedFavlist:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v3, v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/home/data/IconData;->setAppName(Ljava/lang/String;)V

    .line 319
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->mOrderedFavlist:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget v3, v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appType:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/home/data/IconData;->setAppType(I)V

    .line 320
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->mOrderedFavlist:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v3, v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->displayPlugInIcons:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/home/data/IconData;->setAppDisplayPlugInIcons(Ljava/lang/String;)V

    .line 321
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->mOrderedFavlist:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget v3, v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->pluginId:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/home/data/IconData;->setAppPluginId(I)V

    .line 322
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->mOrderedFavlist:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v3, v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->packagename:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/home/data/IconData;->setPackageName(Ljava/lang/String;)V

    .line 323
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/home/data/IconData;->setIconType(I)V

    .line 324
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->mIconList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 325
    const-string v3, "EditFavorites"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Icon data is : appname= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " |pluginID= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppPluginId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " |pluginICon="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppDisplayPlugInIcons()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " |packagename="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/home/data/IconData;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    .end local v2    # "iconData":Lcom/sec/android/app/shealth/home/data/IconData;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 331
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->mOrderedFavlist:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/16 v4, 0x10

    if-ge v3, v4, :cond_0

    .line 333
    new-instance v0, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/home/data/IconData;-><init>()V

    .line 334
    .local v0, "addIcon":Lcom/sec/android/app/shealth/home/data/IconData;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/home/data/IconData;->setIconType(I)V

    .line 335
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->mIconList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method private setIconsToGrid()V
    .locals 3

    .prologue
    .line 213
    new-instance v0, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->mIconList:Ljava/util/ArrayList;

    const/4 v2, 0x4

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->adapter:Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->gridView:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->adapter:Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->gridView:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    new-instance v1, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$2;-><init>(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->gridView:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    new-instance v1, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$3;-><init>(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->gridView:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    new-instance v1, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$4;-><init>(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->setOnDropListener(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$OnDropListener;)V

    .line 268
    return-void
.end method

.method private showDiscardDialog()V
    .locals 5

    .prologue
    .line 457
    const v1, 0x7f090081

    .line 458
    .local v1, "title":I
    const v0, 0x7f090082

    .line 459
    .local v0, "message":I
    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->makeDiscardDialog(II)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "DISCARD_CHANGES"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 460
    return-void
.end method

.method private unRegisterDBListener()V
    .locals 2

    .prologue
    .line 513
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->registryContentObserver:Lcom/sec/android/app/shealth/home/EditFavoritesActivity$RegistryContentObserver;

    if-eqz v0, :cond_0

    .line 515
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->registryContentObserver:Lcom/sec/android/app/shealth/home/EditFavoritesActivity$RegistryContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 516
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->registryContentObserver:Lcom/sec/android/app/shealth/home/EditFavoritesActivity$RegistryContentObserver;

    .line 518
    :cond_0
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 7

    .prologue
    .line 523
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 524
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    const v4, 0x7f090909

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 525
    new-instance v1, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$5;-><init>(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;)V

    .line 538
    .local v1, "doneClickListner":Landroid/view/View$OnClickListener;
    new-instance v2, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$6;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$6;-><init>(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;)V

    .line 548
    .local v2, "revertClickListner":Landroid/view/View$OnClickListener;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v3

    const v4, 0x7f080304

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 549
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v3, 0x7f090044

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(Landroid/view/View$OnClickListener;I)V

    .line 550
    .local v0, "doneButtonBuilder":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v5, 0x0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 551
    return-void
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 473
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->mDialogControllerMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 405
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 407
    const/16 v0, 0x65

    if-ne p1, v0, :cond_0

    .line 409
    invoke-static {}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->getTotalFavoriteCount()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->adapter:Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->getItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-eq v0, v1, :cond_0

    .line 411
    const-string v0, "EditFavorites"

    const-string/jumbo v1, "onActivityResult Called"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 412
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->isChanged:Z

    .line 413
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->reInitGrid()V

    .line 417
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 442
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->unRegisterDBListener()V

    .line 443
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->gridView:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->touchEventsEnded()V

    .line 445
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->isChanged:Z

    if-eqz v0, :cond_0

    .line 446
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->showDiscardDialog()V

    .line 449
    :goto_0
    return-void

    .line 448
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 82
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 83
    const v0, 0x7f030006

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->setContentView(I)V

    .line 84
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->initialize()V

    .line 85
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 505
    const-string v0, "EditFavorites"

    const-string/jumbo v1, "onDestroy Called"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 506
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->gridView:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 507
    iput-object v2, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->adapter:Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;

    .line 508
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    .line 509
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 434
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->isPaused:Z

    .line 435
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPause()V

    .line 436
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 422
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->isPaused:Z

    .line 423
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->hasDataChanged:Z

    if-eqz v0, :cond_0

    .line 425
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->reInitGrid()V

    .line 426
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->hasDataChanged:Z

    .line 428
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 429
    return-void
.end method
