.class Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$2;
.super Ljava/lang/Object;
.source "SelectedPanel.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->createAndInitTextView(Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;)V
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 188
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 189
    .local v3, "uniqueId":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mMealItemDataHolder:Ljava/util/Map;
    invoke-static {v4}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->access$000(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;)Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

    .line 190
    .local v2, "selectedPanelHolder":Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mOnSelectedItemClickListener:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$OnSelectedItemClickListener;
    invoke-static {v4}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->access$100(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;)Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$OnSelectedItemClickListener;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->isMyMeal()Z

    move-result v4

    if-nez v4, :cond_0

    .line 191
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->getMealItemData()Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    move-result-object v1

    .line 192
    .local v1, "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    new-instance v4, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getFoodInfoId()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v0

    .line 193
    .local v0, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mOnSelectedItemClickListener:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$OnSelectedItemClickListener;
    invoke-static {v4}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->access$100(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;)Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$OnSelectedItemClickListener;

    move-result-object v4

    invoke-interface {v4, v0, v2}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$OnSelectedItemClickListener;->onSelectedItemClick(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;)V

    .line 195
    .end local v0    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .end local v1    # "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    :cond_0
    return-void
.end method
