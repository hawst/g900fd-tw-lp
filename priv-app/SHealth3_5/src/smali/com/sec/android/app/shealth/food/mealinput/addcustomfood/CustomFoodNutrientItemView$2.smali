.class Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView$2;
.super Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;
.source "CustomFoodNutrientItemView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->setNutrientFilter(FF)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

.field final synthetic val$maxValue:F

.field final synthetic val$minValue:F


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;FFIFF)V
    .locals 0
    .param p2, "x0"    # F
    .param p3, "x1"    # F
    .param p4, "x2"    # I

    .prologue
    .line 138
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    iput p5, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView$2;->val$minValue:F

    iput p6, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView$2;->val$maxValue:F

    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;-><init>(FFI)V

    return-void
.end method


# virtual methods
.method public showAlertToast()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->mOutOfRangeToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->access$000(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;)Landroid/widget/Toast;

    move-result-object v0

    if-nez v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f09092d

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView$2;->val$minValue:F

    invoke-static {v5}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x1

    iget v6, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView$2;->val$maxValue:F

    invoke-static {v6}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v7}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->mOutOfRangeToast:Landroid/widget/Toast;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->access$002(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->mOutOfRangeToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->access$000(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 150
    return-void
.end method

.method public showInvalidInputToast()V
    .locals 4

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->mInvalidInputToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->access$100(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;)Landroid/widget/Toast;

    move-result-object v0

    if-nez v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f09092e

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->mInvalidInputToast:Landroid/widget/Toast;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->access$102(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->mInvalidInputToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->access$100(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 165
    :goto_0
    return-void

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->mInvalidInputToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->access$100(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
