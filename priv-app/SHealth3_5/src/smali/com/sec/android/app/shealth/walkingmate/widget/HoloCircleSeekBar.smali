.class public Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;
.super Landroid/view/View;
.source "HoloCircleSeekBar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$OnCircleSeekBarChangeListener;
    }
.end annotation


# static fields
.field private static final STATE_ANGLE:Ljava/lang/String; = "angle"

.field private static final STATE_PARENT:Ljava/lang/String; = "parent"

.field private static final TAG:Ljava/lang/String; = "HoloCircleSeekBar"


# instance fields
.field private isTimerTaskRunning:Z

.field private mAngle:F

.field private final mAnimationIntervalTime:I

.field private mArcColor:Landroid/graphics/Paint;

.field private mArcFinishRadians:I

.field private mAttr_pointer_halo_color_attr:Ljava/lang/String;

.field private mAttr_text_color_attr:Ljava/lang/String;

.field private mBackupValue:I

.field private mCircleTextColor:Landroid/graphics/Paint;

.field private mColorCenterHalo:Landroid/graphics/Paint;

.field private mColorCenterHaloRectangle:Landroid/graphics/RectF;

.field private mColorWheelPaint:Landroid/graphics/Paint;

.field private mColorWheelRadius:F

.field private mColorWheelRectangle:Landroid/graphics/RectF;

.field private mColor_init_position:I

.field private mColor_pointer_color:I

.field private mColor_pointer_halo_color:I

.field private mColor_text_color:I

.field private mColor_text_size:I

.field private mContext:Landroid/content/Context;

.field private mConversion:I

.field private mCurrentAniValue:I

.field private mCurrentValue:I

.field private mEndWheel:I

.field private mMax:I

.field private mPointerColor:Landroid/graphics/Paint;

.field private mPointerHaloPaint:Landroid/graphics/Paint;

.field private mPointerRadius:I

.field private mPressHaloPaint:I

.field private mStartArc:I

.field private mSweepGradient:Landroid/graphics/SweepGradient;

.field private mTextPaint:Landroid/graphics/Paint;

.field private mTimerTask:Ljava/util/TimerTask;

.field private mTranslationOffset:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 98
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 64
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColorWheelRectangle:Landroid/graphics/RectF;

    .line 69
    const/16 v0, 0x1e

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mAnimationIntervalTime:I

    .line 75
    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mConversion:I

    .line 76
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mMax:I

    .line 82
    const/16 v0, 0x168

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mArcFinishRadians:I

    .line 83
    const/16 v0, 0x10e

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mStartArc:I

    .line 86
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColorCenterHaloRectangle:Landroid/graphics/RectF;

    .line 99
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mContext:Landroid/content/Context;

    .line 100
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->init(Landroid/util/AttributeSet;I)V

    .line 101
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 104
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 64
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColorWheelRectangle:Landroid/graphics/RectF;

    .line 69
    const/16 v0, 0x1e

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mAnimationIntervalTime:I

    .line 75
    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mConversion:I

    .line 76
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mMax:I

    .line 82
    const/16 v0, 0x168

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mArcFinishRadians:I

    .line 83
    const/16 v0, 0x10e

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mStartArc:I

    .line 86
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColorCenterHaloRectangle:Landroid/graphics/RectF;

    .line 105
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mContext:Landroid/content/Context;

    .line 106
    invoke-direct {p0, p2, v1}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->init(Landroid/util/AttributeSet;I)V

    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 110
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 64
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColorWheelRectangle:Landroid/graphics/RectF;

    .line 69
    const/16 v0, 0x1e

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mAnimationIntervalTime:I

    .line 75
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mConversion:I

    .line 76
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mMax:I

    .line 82
    const/16 v0, 0x168

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mArcFinishRadians:I

    .line 83
    const/16 v0, 0x10e

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mStartArc:I

    .line 86
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColorCenterHaloRectangle:Landroid/graphics/RectF;

    .line 111
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mContext:Landroid/content/Context;

    .line 112
    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->init(Landroid/util/AttributeSet;I)V

    .line 113
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->isTimerTaskRunning:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mCurrentAniValue:I

    return v0
.end method

.method static synthetic access$112(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;
    .param p1, "x1"    # I

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mCurrentAniValue:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mCurrentAniValue:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mCurrentValue:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mBackupValue:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mArcFinishRadians:I

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;
    .param p1, "x1"    # I

    .prologue
    .line 52
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mArcFinishRadians:I

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mMax:I

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;
    .param p1, "x1"    # F

    .prologue
    .line 52
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mAngle:F

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;I)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;
    .param p1, "x1"    # I

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->calculateAngleFromRadians(I)F

    move-result v0

    return v0
.end method

.method private calculateAngleFromRadians(I)F
    .locals 4
    .param p1, "radians"    # I

    .prologue
    .line 287
    add-int/lit16 v0, p1, 0x10e

    int-to-double v0, v0

    const-wide v2, 0x401921fb54442d18L    # 6.283185307179586

    mul-double/2addr v0, v2

    const-wide v2, 0x4076800000000000L    # 360.0

    div-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method private calculateAngleFromText(I)D
    .locals 10
    .param p1, "position"    # I

    .prologue
    const-wide v0, 0x4056800000000000L    # 90.0

    .line 265
    if-eqz p1, :cond_0

    iget v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mMax:I

    if-lt p1, v6, :cond_1

    .line 272
    :cond_0
    :goto_0
    return-wide v0

    .line 268
    :cond_1
    iget v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mMax:I

    int-to-double v6, v6

    int-to-double v8, p1

    div-double v2, v6, v8

    .line 269
    .local v2, "f":D
    const-wide v6, 0x4076800000000000L    # 360.0

    div-double v4, v6, v2

    .line 270
    .local v4, "f_r":D
    add-double/2addr v0, v4

    .line 272
    .local v0, "ang":D
    goto :goto_0
.end method

.method private calculateRadiansFromAngle(F)I
    .locals 6
    .param p1, "angle"    # F

    .prologue
    .line 276
    float-to-double v2, p1

    const-wide v4, 0x401921fb54442d18L    # 6.283185307179586

    div-double/2addr v2, v4

    double-to-float v1, v2

    .line 277
    .local v1, "unit":F
    const/4 v2, 0x0

    cmpg-float v2, v1, v2

    if-gez v2, :cond_0

    .line 278
    const/high16 v2, 0x3f800000    # 1.0f

    add-float/2addr v1, v2

    .line 280
    :cond_0
    const/high16 v2, 0x43b40000    # 360.0f

    mul-float/2addr v2, v1

    const/high16 v3, 0x43870000    # 270.0f

    sub-float/2addr v2, v3

    float-to-int v0, v2

    .line 281
    .local v0, "radians":I
    if-gez v0, :cond_1

    .line 282
    add-int/lit16 v0, v0, 0x168

    .line 283
    :cond_1
    return v0
.end method

.method private calculateTextFromStartAngle(F)I
    .locals 4
    .param p1, "angle"    # F

    .prologue
    .line 259
    move v1, p1

    .line 260
    .local v1, "m":F
    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mEndWheel:I

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mStartArc:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    div-float v0, v2, v1

    .line 261
    .local v0, "f":F
    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mMax:I

    int-to-float v2, v2

    div-float/2addr v2, v0

    float-to-int v2, v2

    return v2
.end method

.method private init(Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1, "attrs"    # Landroid/util/AttributeSet;
    .param p2, "defStyle"    # I

    .prologue
    const/4 v5, 0x1

    .line 116
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/R$styleable;->workout_HoloCircleSeekBar:[I

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 119
    .local v0, "typedArray":Landroid/content/res/TypedArray;
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->initAttributes(Landroid/content/res/TypedArray;)V

    .line 121
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 123
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    .line 124
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mSweepGradient:Landroid/graphics/SweepGradient;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 125
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColor_pointer_color:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 126
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 127
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    const/high16 v3, 0x40e00000    # 7.0f

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->convertDpToPx(Landroid/content/Context;F)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 129
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColorCenterHalo:Landroid/graphics/Paint;

    .line 130
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColorCenterHalo:Landroid/graphics/Paint;

    const v2, -0xff0001

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 131
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColorCenterHalo:Landroid/graphics/Paint;

    const/16 v2, 0xcc

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 133
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mPointerHaloPaint:Landroid/graphics/Paint;

    .line 134
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mPointerHaloPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColor_pointer_halo_color:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 135
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mPointerHaloPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mPressHaloPaint:I

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x41a00000    # 20.0f

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->convertDpToPx(Landroid/content/Context;F)F

    move-result v3

    float-to-int v3, v3

    add-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 137
    new-instance v1, Landroid/graphics/Paint;

    const/16 v2, 0x41

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mTextPaint:Landroid/graphics/Paint;

    .line 138
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mTextPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColor_text_color:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 139
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mTextPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 140
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mTextPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 141
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mTextPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColor_text_size:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 143
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mPointerColor:Landroid/graphics/Paint;

    .line 144
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mPointerColor:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mPointerRadius:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 146
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mPointerColor:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColor_pointer_color:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 148
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mArcColor:Landroid/graphics/Paint;

    .line 149
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mArcColor:Landroid/graphics/Paint;

    const v2, -0xa041df

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 150
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mArcColor:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 151
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mArcColor:Landroid/graphics/Paint;

    const/high16 v2, 0x41b00000    # 22.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 153
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mCircleTextColor:Landroid/graphics/Paint;

    .line 154
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mCircleTextColor:Landroid/graphics/Paint;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 155
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mCircleTextColor:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 157
    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColor_init_position:I

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->calculateAngleFromText(I)D

    move-result-wide v1

    double-to-int v1, v1

    add-int/lit8 v1, v1, -0x5a

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mArcFinishRadians:I

    .line 159
    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mArcFinishRadians:I

    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mEndWheel:I

    if-le v1, v2, :cond_0

    .line 160
    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mEndWheel:I

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mArcFinishRadians:I

    .line 161
    :cond_0
    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mArcFinishRadians:I

    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mEndWheel:I

    if-le v1, v2, :cond_1

    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mEndWheel:I

    :goto_0
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->calculateAngleFromRadians(I)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mAngle:F

    .line 164
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->invalidate()V

    .line 165
    return-void

    .line 161
    :cond_1
    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mArcFinishRadians:I

    goto :goto_0
.end method

.method private initAttributes(Landroid/content/res/TypedArray;)V
    .locals 6
    .param p1, "a"    # Landroid/content/res/TypedArray;

    .prologue
    const/4 v5, 0x0

    const v4, -0xff0001

    const v3, -0x965400

    .line 168
    const-string v1, "HoloCircleSeekBar"

    const-string v2, "initAttributes"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x41200000    # 10.0f

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->convertDpToPx(Landroid/content/Context;F)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mPointerRadius:I

    .line 170
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x41600000    # 14.0f

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->convertDpToPx(Landroid/content/Context;F)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mPressHaloPaint:I

    .line 171
    const/16 v1, 0x11

    const/16 v2, 0x64

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mMax:I

    .line 173
    const/16 v1, 0x1b

    invoke-virtual {p1, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mAttr_pointer_halo_color_attr:Ljava/lang/String;

    .line 174
    const/16 v1, 0x1c

    invoke-virtual {p1, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mAttr_text_color_attr:Ljava/lang/String;

    .line 175
    const/16 v1, 0x15

    const/16 v2, 0x5f

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColor_text_size:I

    .line 176
    const/16 v1, 0x16

    invoke-virtual {p1, v1, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColor_init_position:I

    .line 178
    const/16 v1, 0x13

    invoke-virtual {p1, v1, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mStartArc:I

    .line 179
    const/16 v1, 0x14

    const/16 v2, 0x168

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mEndWheel:I

    .line 181
    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColor_init_position:I

    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mStartArc:I

    if-ge v1, v2, :cond_0

    .line 182
    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mStartArc:I

    int-to-float v1, v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->calculateTextFromStartAngle(F)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColor_init_position:I

    .line 184
    :cond_0
    const v1, -0xa041df

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColor_pointer_color:I

    .line 186
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mAttr_pointer_halo_color_attr:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 188
    const v1, -0x965400

    :try_start_0
    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColor_pointer_halo_color:I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 197
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mAttr_text_color_attr:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 199
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mAttr_text_color_attr:Ljava/lang/String;

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColor_text_color:I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 207
    :goto_1
    return-void

    .line 189
    :catch_0
    move-exception v0

    .line 190
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    iput v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColor_pointer_halo_color:I

    goto :goto_0

    .line 194
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_1
    iput v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColor_pointer_halo_color:I

    goto :goto_0

    .line 200
    :catch_1
    move-exception v0

    .line 201
    .restart local v0    # "e":Ljava/lang/IllegalArgumentException;
    iput v4, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColor_text_color:I

    goto :goto_1

    .line 204
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_2
    iput v4, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColor_text_color:I

    goto :goto_1
.end method


# virtual methods
.method public doTimerTask()V
    .locals 6

    .prologue
    .line 350
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 351
    .local v0, "t":Ljava/util/Timer;
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mCurrentAniValue:I

    .line 352
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mBackupValue:I

    .line 353
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->isTimerTaskRunning:Z

    .line 354
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    if-eqz v1, :cond_0

    .line 355
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    invoke-virtual {v1}, Ljava/util/TimerTask;->cancel()Z

    .line 356
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    .line 358
    :cond_0
    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    .line 364
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    const-wide/16 v2, 0x64

    const-wide/16 v4, 0x1e

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 366
    return-void
.end method

.method public doUiChanged()V
    .locals 2

    .prologue
    .line 369
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$2;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 391
    return-void
.end method

.method public getValue()I
    .locals 1

    .prologue
    .line 296
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mConversion:I

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 237
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mTranslationOffset:F

    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mTranslationOffset:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 238
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColorWheelRectangle:Landroid/graphics/RectF;

    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mStartArc:I

    add-int/lit16 v0, v0, 0x10e

    int-to-float v2, v0

    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mArcFinishRadians:I

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mEndWheel:I

    if-le v0, v3, :cond_0

    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mEndWheel:I

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mStartArc:I

    sub-int/2addr v0, v3

    int-to-float v3, v0

    :goto_0
    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 241
    return-void

    .line 238
    :cond_0
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mArcFinishRadians:I

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mStartArc:I

    sub-int/2addr v0, v3

    int-to-float v3, v0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 9
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    .line 245
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->getSuggestedMinimumHeight()I

    move-result v3

    invoke-static {v3, p2}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->getDefaultSize(II)I

    move-result v0

    .line 247
    .local v0, "height":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->getSuggestedMinimumWidth()I

    move-result v3

    invoke-static {v3, p1}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->getDefaultSize(II)I

    move-result v2

    .line 248
    .local v2, "width":I
    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 249
    .local v1, "min":I
    invoke-virtual {p0, v1, v1}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->setMeasuredDimension(II)V

    .line 251
    int-to-float v3, v1

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mTranslationOffset:F

    .line 252
    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mTranslationOffset:F

    iget v4, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mPressHaloPaint:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x411b3333    # 9.7f

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->convertDpToPx(Landroid/content/Context;F)F

    move-result v4

    add-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColorWheelRadius:F

    .line 253
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColorWheelRectangle:Landroid/graphics/RectF;

    iget v4, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColorWheelRadius:F

    neg-float v4, v4

    iget v5, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColorWheelRadius:F

    neg-float v5, v5

    iget v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColorWheelRadius:F

    iget v7, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColorWheelRadius:F

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 254
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColorCenterHaloRectangle:Landroid/graphics/RectF;

    iget v4, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColorWheelRadius:F

    neg-float v4, v4

    div-float/2addr v4, v8

    iget v5, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColorWheelRadius:F

    neg-float v5, v5

    div-float/2addr v5, v8

    iget v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColorWheelRadius:F

    div-float/2addr v6, v8

    iget v7, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColorWheelRadius:F

    div-float/2addr v7, v8

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 256
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 334
    move-object v0, p1

    check-cast v0, Landroid/os/Bundle;

    .line 336
    .local v0, "savedState":Landroid/os/Bundle;
    const-string/jumbo v2, "parent"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    .line 337
    .local v1, "superState":Landroid/os/Parcelable;
    invoke-super {p0, v1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 339
    const-string v2, "angle"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mAngle:F

    .line 340
    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mAngle:F

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->calculateRadiansFromAngle(F)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mArcFinishRadians:I

    .line 341
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .prologue
    .line 323
    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 325
    .local v1, "superState":Landroid/os/Parcelable;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 326
    .local v0, "state":Landroid/os/Bundle;
    const-string/jumbo v2, "parent"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 327
    const-string v2, "angle"

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mAngle:F

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 329
    return-object v0
.end method

.method public setMax(I)V
    .locals 1
    .param p1, "max"    # I

    .prologue
    .line 300
    add-int/lit8 v0, p1, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mMax:I

    .line 301
    return-void
.end method

.method public setPointerColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mPointerColor:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 219
    return-void
.end method

.method public setPressPointerColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 222
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mPointerHaloPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 223
    return-void
.end method

.method public setProgress(I)V
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 304
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->isTimerTaskRunning:Z

    if-eqz v0, :cond_0

    .line 305
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mBackupValue:I

    .line 311
    :goto_0
    return-void

    .line 308
    :cond_0
    int-to-float v0, p1

    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mMax:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    const/high16 v1, 0x43b40000    # 360.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mArcFinishRadians:I

    .line 309
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mArcFinishRadians:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->calculateAngleFromRadians(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mAngle:F

    .line 310
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->invalidate()V

    goto :goto_0
.end method

.method public setProgressBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mArcColor:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 211
    return-void
.end method

.method public setProgressColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 214
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 215
    return-void
.end method

.method public setProgressWithAnimation(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 314
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->isTimerTaskRunning:Z

    if-eqz v0, :cond_0

    .line 315
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->stopTask()V

    .line 317
    :cond_0
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mCurrentValue:I

    .line 318
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->doTimerTask()V

    .line 319
    return-void
.end method

.method public setWheelAlpha(Z)V
    .locals 2
    .param p1, "isStart"    # Z

    .prologue
    .line 226
    if-eqz p1, :cond_0

    const/16 v0, 0xff

    .line 227
    .local v0, "alpha":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 228
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->invalidate()V

    .line 229
    return-void

    .line 226
    .end local v0    # "alpha":I
    :cond_0
    const/16 v0, 0x66

    goto :goto_0
.end method

.method public stopTask()V
    .locals 1

    .prologue
    .line 395
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->isTimerTaskRunning:Z

    .line 396
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    if-eqz v0, :cond_0

    .line 397
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 398
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    .line 401
    :cond_0
    return-void
.end method
