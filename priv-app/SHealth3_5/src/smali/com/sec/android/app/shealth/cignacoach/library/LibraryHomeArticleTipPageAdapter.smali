.class public Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeArticleTipPageAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "LibraryHomeArticleTipPageAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    .local p2, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;>;"
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeArticleTipPageAdapter;->mContext:Landroid/content/Context;

    .line 28
    iput-object p2, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeArticleTipPageAdapter;->mItemList:Ljava/util/ArrayList;

    .line 29
    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "view"    # Ljava/lang/Object;

    .prologue
    .line 65
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    check-cast p3, Landroid/view/View;

    .end local p3    # "view":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    .line 66
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeArticleTipPageAdapter;->mItemList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeArticleTipPageAdapter;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 40
    const/4 v0, -0x2

    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 6
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 51
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryHomeArticleTipView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeArticleTipPageAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryHomeArticleTipView;-><init>(Landroid/content/Context;)V

    .line 52
    .local v0, "view":Lcom/sec/android/app/shealth/cignacoach/widget/LibraryHomeArticleTipView;
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeArticleTipPageAdapter;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryHomeArticleTipView;->setTitle(Ljava/lang/CharSequence;)V

    .line 53
    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryHomeArticleTipView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeArticleTipPageAdapter;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryHomeArticleTipView;->setTag(Ljava/lang/Object;)V

    .line 55
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeArticleTipPageAdapter;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->getTitleImg()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->getGoalImageResourceID(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryHomeArticleTipView;->setBackground(I)V

    .line 56
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeArticleTipPageAdapter;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->getTitle()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeArticleTipPageAdapter;->mContext:Landroid/content/Context;

    const v3, 0x7f090c8d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    add-int/lit8 v5, p2, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeArticleTipPageAdapter;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    invoke-virtual {p1, v0}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;)V

    .line 60
    return-object v0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 46
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 70
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    .line 71
    .local v0, "cignaLibraryArticleData":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeArticleTipPageAdapter;->mContext:Landroid/content/Context;

    const-class v3, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 72
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v2, 0x24000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 73
    const-string v2, "extra_name_article_id"

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->getId()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 74
    const-string v2, "article_resource_image"

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->getBackground()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 75
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeArticleTipPageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 76
    return-void
.end method
