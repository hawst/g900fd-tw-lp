.class public Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;
.super Ljava/lang/Object;
.source "WalkingMateStatisticServerTool.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$WalkForLifeView;,
        Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$WALK_TABLE;,
        Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;,
        Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$UserInfo;,
        Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicUser;,
        Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$TotalUserStatisticResponse;,
        Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;,
        Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;
    }
.end annotation


# static fields
.field private static final ACCOUNT_ID:Ljava/lang/String; = "accountId"

.field private static final ACTIVITY_TYPE:Ljava/lang/String; = "activity_type"

.field private static final AGE:Ljava/lang/String; = "age"

.field public static final ATTEMPTS_NUMBER:I = 0x1

.field private static BASIC_STATS_URI:Ljava/lang/String; = null

.field private static final BIRTH_DAY:Ljava/lang/String; = "birth_day"

.field private static final BIRTH_MONTH:Ljava/lang/String; = "birth_month"

.field private static final BIRTH_YEAR:Ljava/lang/String; = "birth_year"

.field private static BY_AGE_STATS_URI:Ljava/lang/String; = null

.field private static final COUNTRY:Ljava/lang/String; = "country"

.field private static final DEFAULT_NUMERIC_VALUE:Ljava/lang/String; = "0"

.field public static final ERROR_CODE:Ljava/lang/String; = "code"

.field private static final GENDER:Ljava/lang/String; = "gender"

.field private static final HEADER_ACCEPT:Ljava/lang/String; = "Accept"

.field private static final HEADER_ACCEPT_VALUE:Ljava/lang/String; = "application/json"

.field private static final HEADER_ACCESS_TOKEN:Ljava/lang/String; = "access_token"

.field private static final HEADER_ACCOUNT_ID:Ljava/lang/String; = "accountId"

.field private static final HEADER_APP_ID:Ljava/lang/String; = "reqAppId"

.field private static final HEADER_CONTENT_TYPE:Ljava/lang/String; = "Content-Type"

.field private static final HEADER_DEVICE_ID:Ljava/lang/String; = "deviceId"

.field private static final HEADER_HTX_ID:Ljava/lang/String; = "htxid"

.field private static final HEADER_USER_TOKEN:Ljava/lang/String; = "user_token"

.field private static final HEIGHT:Ljava/lang/String; = "height"

.field private static final HEIGHT_UNIT:Ljava/lang/String; = "height_unit"

.field public static final HTTP_STATUS_CODE:Ljava/lang/String; = "httpStatus"

.field private static final IMAGE_ENTITY_NAME:Ljava/lang/String; = "imgfile"

.field private static final IMG_URL:Ljava/lang/String; = "imgurl"

.field public static final INVALID_ACCESS_TOCKEN_MESSAGE:Ljava/lang/String; = "Invalid access tocken"

.field public static final INVALID_ACCESS_TOCKEN_STATUS:I = 0x191

.field public static final INVALID_SAMSUNG_ACCOUNT_ERROR_CODE:Ljava/lang/String; = "PHR_4301"

.field public static final INVALID_SAMSUNG_ACCOUNT_ERROR_TEMP_CODE:Ljava/lang/String; = "SHT_4703"

.field public static final INVALID_SAMSUNG_ACCOUNT_MESSAGE:Ljava/lang/String; = "Invalid Samsung account info"

.field public static final INVALID_SERVER_ERROR:Ljava/lang/String; = "Server Error"

.field private static MAIN_URL:Ljava/lang/String; = null

.field private static final MAX_COUNT_PARAMS:Ljava/lang/String; = "maxCount"

.field private static final NAME:Ljava/lang/String; = "name"

.field private static final NULL_STRING_VALUE:Ljava/lang/String; = "null"

.field public static final PROFILE_UPDATE_TIME:Ljava/lang/String; = "wm_init_time"

.field private static final RANKING:Ljava/lang/String; = "ranking"

.field private static final RANKING_LIST:Ljava/lang/String; = "rankingList"

.field private static final REQUEST_DATE:Ljava/lang/String; = "reqDate"

.field public static final SEARCH_TYPE_LEADER_BOARD:I = 0x1

.field public static final SEARCH_TYPE_YOUR_RANKING:I = 0x0

.field public static final STATUS_CODE_INVALID_ACCESS_TOCKEN:Ljava/lang/String; = "401"

.field public static final STATUS_CODE_SUCCESS:Ljava/lang/String; = "200"

.field private static final TAG:Ljava/lang/String;

.field private static TOP_TEN_STATS_URI:Ljava/lang/String; = null

.field private static final TOTAL_DISTANCE:Ljava/lang/String; = "totalDistance"

.field private static TOTAL_STATS_URI:Ljava/lang/String; = null

.field private static UPDATE_IMAGE_URI:Ljava/lang/String; = null

.field private static UPDATE_PROFILE_URI:Ljava/lang/String; = null

.field private static UPDATE_WALKINGMATE_URI:Ljava/lang/String; = null

.field private static USER_INFO_STATS_URI:Ljava/lang/String; = null

.field private static final USER_TOKEN:Ljava/lang/String; = "user_token"

.field private static WALKINGMATE_LEADER_BOARD_URI:Ljava/lang/String; = null

.field private static WALKINGMATE_YOUR_RANKING_URI:Ljava/lang/String; = null

.field private static final WEIGHT:Ljava/lang/String; = "weight"

.field private static final WEIGHT_UNIT:Ljava/lang/String; = "weight_unit"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 44
    const-class v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->TAG:Ljava/lang/String;

    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->HTTP_SERVER:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->MAIN_URL:Ljava/lang/String;

    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->MAIN_URL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "platform/reporting/getPersonalPedometerStats"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->BASIC_STATS_URI:Ljava/lang/String;

    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->BASIC_STATS_URI:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "byAge"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->BY_AGE_STATS_URI:Ljava/lang/String;

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->BASIC_STATS_URI:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "byAll"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->TOTAL_STATS_URI:Ljava/lang/String;

    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->MAIN_URL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "platform/reporting/getRankingList"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->TOP_TEN_STATS_URI:Ljava/lang/String;

    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->MAIN_URL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "platform/reporting/getUserInfo"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->USER_INFO_STATS_URI:Ljava/lang/String;

    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->MAIN_URL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "v2/shealth/phr/profileupdate"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->UPDATE_PROFILE_URI:Ljava/lang/String;

    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->MAIN_URL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "v2/shealth/phr/prfimgupdate"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->UPDATE_IMAGE_URI:Ljava/lang/String;

    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->MAIN_URL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "v2/shealth/stats/updateWalkingMate"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->UPDATE_WALKINGMATE_URI:Ljava/lang/String;

    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->MAIN_URL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "platform/reporting/getPersonalPedometerStats"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->WALKINGMATE_YOUR_RANKING_URI:Ljava/lang/String;

    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->MAIN_URL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "platform/reporting/getRankingList"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->WALKINGMATE_LEADER_BOARD_URI:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 721
    return-void
.end method

.method static synthetic access$000(Lorg/json/JSONObject;)V
    .locals 0
    .param p0, "x0"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;
        }
    .end annotation

    .prologue
    .line 42
    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->checkJson(Lorg/json/JSONObject;)V

    return-void
.end method

.method private static checkJson(Lorg/json/JSONObject;)V
    .locals 4
    .param p0, "basicResponse"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;
        }
    .end annotation

    .prologue
    .line 351
    const-string v2, "code"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 353
    :try_start_0
    const-string v2, "code"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 354
    .local v0, "code":Ljava/lang/String;
    const-string v2, "PHR_4301"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "SHT_4703"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 355
    :cond_0
    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;

    const-string v3, "Invalid Samsung account info"

    invoke-direct {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 357
    .end local v0    # "code":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 358
    .local v1, "e":Lorg/json/JSONException;
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->TAG:Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/common/utils/Utils;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 361
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_1
    return-void
.end method

.method public static getJsonObj(Lorg/apache/http/HttpResponse;)Lorg/json/JSONObject;
    .locals 6
    .param p0, "response"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;,
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 108
    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    .line 109
    .local v1, "ips":Ljava/io/InputStream;
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    const-string v5, "UTF-8"

    invoke-direct {v4, v1, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v0, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 110
    .local v0, "buf":Ljava/io/BufferedReader;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 113
    .local v3, "sb":Ljava/lang/StringBuilder;
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    .line 114
    .local v2, "s":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 118
    :cond_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    .line 119
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 121
    new-instance v4, Lorg/json/JSONObject;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    return-object v4

    .line 116
    :cond_1
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static getMyRanking(Lorg/json/JSONObject;)Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
    .locals 4
    .param p0, "json"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 741
    const-string/jumbo v1, "myRanking"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 743
    .local v0, "myRanking":Lorg/json/JSONObject;
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "myRanking : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 744
    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    const/4 v2, 0x1

    invoke-direct {v1, v0, v2}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;-><init>(Lorg/json/JSONObject;Z)V

    return-object v1
.end method

.method private static getServerResponse(Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/String;
    .locals 11
    .param p0, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 126
    .local p1, "headers":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v1, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v1}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 127
    .local v1, "client":Lorg/apache/http/client/HttpClient;
    new-instance v4, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v4}, Lorg/apache/http/client/methods/HttpGet;-><init>()V

    .line 128
    .local v4, "request":Lorg/apache/http/client/methods/HttpGet;
    new-instance v9, Ljava/net/URI;

    invoke-direct {v9, p0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v9}, Lorg/apache/http/client/methods/HttpGet;->setURI(Ljava/net/URI;)V

    .line 129
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    .line 130
    .local v7, "s":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v4, v9, v10}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 132
    .end local v7    # "s":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    invoke-interface {v1, v4}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v5

    .line 133
    .local v5, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v5}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v9

    invoke-interface {v9}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v3

    .line 134
    .local v3, "ips":Ljava/io/InputStream;
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v9, Ljava/io/InputStreamReader;

    const-string v10, "UTF-8"

    invoke-direct {v9, v3, v10}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v0, v9}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 135
    .local v0, "buf":Ljava/io/BufferedReader;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 138
    .local v8, "sb":Ljava/lang/StringBuilder;
    :goto_1
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    .line 139
    .local v6, "s":Ljava/lang/String;
    if-eqz v6, :cond_1

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    if-nez v9, :cond_2

    .line 143
    :cond_1
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    .line 144
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 145
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    return-object v9

    .line 141
    :cond_2
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private static getServerResponseWithWalkingMateRanking(Ljava/lang/String;Ljava/util/HashMap;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 20
    .param p0, "uri"    # Ljava/lang/String;
    .param p2, "postData"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lorg/json/JSONObject;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/URISyntaxException;,
            Ljava/io/IOException;,
            Ljava/lang/IllegalStateException;,
            Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;,
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 181
    .local p1, "headers":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v3, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 183
    .local v3, "client":Lorg/apache/http/impl/client/DefaultHttpClient;
    new-instance v11, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v11}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    .line 184
    .local v11, "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    invoke-static {}, Lorg/apache/http/conn/ssl/SSLSocketFactory;->getSocketFactory()Lorg/apache/http/conn/ssl/SSLSocketFactory;

    move-result-object v15

    .line 185
    .local v15, "socketFactory":Lorg/apache/http/conn/ssl/SSLSocketFactory;
    new-instance v17, Lorg/apache/http/conn/scheme/Scheme;

    const-string v18, "https"

    sget v19, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->HTTP_PORT:I

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v1, v15, v2}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 186
    new-instance v9, Lorg/apache/http/impl/conn/SingleClientConnManager;

    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v9, v0, v11}, Lorg/apache/http/impl/conn/SingleClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    .line 187
    .local v9, "mgr":Lorg/apache/http/impl/conn/SingleClientConnManager;
    new-instance v6, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v6, v9, v0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    .line 189
    .local v6, "httpClient":Lorg/apache/http/impl/client/DefaultHttpClient;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isChinaNetworkUsing()Z

    move-result v17

    if-eqz v17, :cond_0

    .line 190
    new-instance v10, Lorg/apache/http/HttpHost;

    sget-object v17, Lorg/apache/http/conn/params/ConnRouteParams;->NO_HOST:Lorg/apache/http/HttpHost;

    move-object/from16 v0, v17

    invoke-direct {v10, v0}, Lorg/apache/http/HttpHost;-><init>(Lorg/apache/http/HttpHost;)V

    .line 191
    .local v10, "proxy":Lorg/apache/http/HttpHost;
    invoke-virtual {v6}, Lorg/apache/http/impl/client/DefaultHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v17

    const-string v18, "http.route.default-proxy"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-interface {v0, v1, v10}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 195
    .end local v10    # "proxy":Lorg/apache/http/HttpHost;
    :cond_0
    sget-object v17, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "### setURI : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v19

    add-int/lit8 v19, v19, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    new-instance v12, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v12}, Lorg/apache/http/client/methods/HttpPost;-><init>()V

    .line 198
    .local v12, "request":Lorg/apache/http/client/methods/HttpPost;
    new-instance v17, Ljava/net/URI;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lorg/apache/http/client/methods/HttpPost;->setURI(Ljava/net/URI;)V

    .line 199
    if-eqz p2, :cond_1

    .line 200
    new-instance v17, Lorg/apache/http/entity/StringEntity;

    const-string v18, "UTF-8"

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 202
    :cond_1
    invoke-virtual/range {p1 .. p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/Map$Entry;

    .line 203
    .local v14, "s":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v14}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    invoke-interface {v14}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v12, v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 206
    .end local v14    # "s":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_2
    invoke-virtual {v6, v12}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v13

    .line 207
    .local v13, "response":Lorg/apache/http/HttpResponse;
    invoke-static {v13}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->getJsonObj(Lorg/apache/http/HttpResponse;)Lorg/json/JSONObject;

    move-result-object v8

    .line 208
    .local v8, "json":Lorg/json/JSONObject;
    if-eqz v8, :cond_3

    .line 209
    sget-object v17, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "HttpResponse json : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v8}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    :cond_3
    const-string v17, "httpStatus"

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 214
    sget-object v17, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "httpStatus : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "httpStatus"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    :try_start_0
    const-string v17, "httpStatus"

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 217
    .local v16, "status":Ljava/lang/String;
    const-string v17, "200"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_4

    .line 219
    const-string v17, "401"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 221
    new-instance v17, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;

    const-string v18, "Invalid access tocken"

    const/16 v19, 0x191

    invoke-direct/range {v17 .. v19}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;-><init>(Ljava/lang/String;I)V

    throw v17
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 229
    .end local v16    # "status":Ljava/lang/String;
    :catch_0
    move-exception v5

    .line 230
    .local v5, "e":Lorg/json/JSONException;
    sget-object v17, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->TAG:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-static {v0, v5}, Lcom/sec/android/app/shealth/common/utils/Utils;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 232
    const/4 v8, 0x0

    .line 249
    .end local v5    # "e":Lorg/json/JSONException;
    .end local v8    # "json":Lorg/json/JSONObject;
    :cond_4
    :goto_1
    return-object v8

    .line 224
    .restart local v8    # "json":Lorg/json/JSONObject;
    .restart local v16    # "status":Ljava/lang/String;
    :cond_5
    :try_start_1
    new-instance v17, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;

    const-string v18, "Server Error"

    invoke-direct/range {v17 .. v18}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;-><init>(Ljava/lang/String;)V

    throw v17
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 235
    .end local v16    # "status":Ljava/lang/String;
    :cond_6
    const-string v17, "code"

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 236
    sget-object v17, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "ERROR_CODE : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "code"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    :try_start_2
    const-string v17, "code"

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 239
    .local v4, "code":Ljava/lang/String;
    const-string v17, "PHR_4301"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_7

    const-string v17, "SHT_4703"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 240
    :cond_7
    new-instance v17, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;

    const-string v18, "Invalid Samsung account info"

    invoke-direct/range {v17 .. v18}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;-><init>(Ljava/lang/String;)V

    throw v17
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    .line 244
    .end local v4    # "code":Ljava/lang/String;
    :catch_1
    move-exception v5

    .line 245
    .restart local v5    # "e":Lorg/json/JSONException;
    sget-object v17, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->TAG:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-static {v0, v5}, Lcom/sec/android/app/shealth/common/utils/Utils;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 247
    const/4 v8, 0x0

    goto :goto_1

    .line 242
    .end local v5    # "e":Lorg/json/JSONException;
    .restart local v4    # "code":Ljava/lang/String;
    :cond_8
    :try_start_3
    new-instance v17, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;

    const-string v18, "Server Error"

    invoke-direct/range {v17 .. v18}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;-><init>(Ljava/lang/String;)V

    throw v17
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1
.end method

.method private static getStringContent(Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;)Ljava/lang/String;
    .locals 4
    .param p0, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 149
    .local p1, "params":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .local p2, "headers":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 150
    .local v2, "uriSB":Ljava/lang/StringBuilder;
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 153
    .local v1, "s":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    const-string v3, "&"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 158
    .end local v1    # "s":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->getServerResponse(Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static getTotalWalkingStats(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$TotalUserStatisticResponse;
    .locals 5
    .param p0, "accountId"    # Ljava/lang/String;
    .param p1, "userToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 538
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 539
    .local v1, "params":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 540
    .local v0, "headers":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "Accept"

    const-string v4, "application/json"

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 541
    const-string/jumbo v3, "user_token"

    invoke-virtual {v0, v3, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 542
    const-string v3, "accountId"

    invoke-virtual {v0, v3, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 544
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "https://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->HTTP_SERVER:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->MAIN_URL:Ljava/lang/String;

    .line 545
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->MAIN_URL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "platform/reporting/getPersonalPedometerStats"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->BASIC_STATS_URI:Ljava/lang/String;

    .line 546
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->BASIC_STATS_URI:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "byAll"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->TOTAL_STATS_URI:Ljava/lang/String;

    .line 548
    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->TOTAL_STATS_URI:Ljava/lang/String;

    invoke-static {v3, v1, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->getStringContent(Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;)Ljava/lang/String;

    move-result-object v2

    .line 549
    .local v2, "response":Ljava/lang/String;
    new-instance v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$TotalUserStatisticResponse;

    invoke-direct {v3, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$TotalUserStatisticResponse;-><init>(Ljava/lang/String;)V

    return-object v3
.end method

.method public static getUserInfo(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$UserInfo;
    .locals 5
    .param p0, "accountId"    # Ljava/lang/String;
    .param p1, "userToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 669
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 670
    .local v1, "params":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "accountId"

    invoke-virtual {v1, v3, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 671
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 672
    .local v0, "headers":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "Accept"

    const-string v4, "application/json"

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 673
    const-string/jumbo v3, "user_token"

    invoke-virtual {v0, v3, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 674
    const-string v3, "accountId"

    invoke-virtual {v0, v3, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 676
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "https://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->HTTP_SERVER:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->MAIN_URL:Ljava/lang/String;

    .line 677
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->MAIN_URL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "platform/reporting/getUserInfo"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->USER_INFO_STATS_URI:Ljava/lang/String;

    .line 679
    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->USER_INFO_STATS_URI:Ljava/lang/String;

    invoke-static {v3, v1, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->getStringContent(Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;)Ljava/lang/String;

    move-result-object v2

    .line 680
    .local v2, "response":Ljava/lang/String;
    new-instance v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$UserInfo;

    invoke-direct {v3, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$UserInfo;-><init>(Ljava/lang/String;)V

    return-object v3
.end method

.method public static getWalkingMateRanking(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 5
    .param p0, "searchType"    # I
    .param p1, "deviceId"    # Ljava/lang/String;
    .param p2, "accessToken"    # Ljava/lang/String;
    .param p3, "appid"    # Ljava/lang/String;
    .param p4, "postData"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/net/URISyntaxException;,
            Ljava/io/IOException;,
            Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;,
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 255
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 256
    .local v0, "headers":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "Accept"

    const-string v3, "application/json"

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    const-string v2, "deviceId"

    invoke-virtual {v0, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    const-string v2, "access_token"

    invoke-virtual {v0, v2, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    const-string/jumbo v2, "reqAppId"

    invoke-virtual {v0, v2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    const-string v2, "Content-Type"

    const-string v3, "application/json"

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "###postData : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    if-nez p0, :cond_0

    .line 265
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->WALKINGMATE_YOUR_RANKING_URI:Ljava/lang/String;

    .line 270
    .local v1, "uri":Ljava/lang/String;
    :goto_0
    invoke-static {v1, v0, p4}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->getServerResponseWithWalkingMateRanking(Ljava/lang/String;Ljava/util/HashMap;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    return-object v2

    .line 268
    .end local v1    # "uri":Ljava/lang/String;
    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->WALKINGMATE_LEADER_BOARD_URI:Ljava/lang/String;

    .restart local v1    # "uri":Ljava/lang/String;
    goto :goto_0
.end method

.method public static getWalkingStatsByAge(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;
    .locals 6
    .param p0, "accountId"    # Ljava/lang/String;
    .param p1, "requestDate"    # Ljava/lang/String;
    .param p2, "age"    # I
    .param p3, "userToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 488
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 489
    .local v1, "params":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v3, "reqDate"

    invoke-virtual {v1, v3, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 490
    const-string v3, "accountId"

    invoke-virtual {v1, v3, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 491
    const-string v3, "age"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 492
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 493
    .local v0, "headers":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "Accept"

    const-string v4, "application/json"

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 494
    const-string/jumbo v3, "user_token"

    invoke-virtual {v0, v3, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 495
    const-string v3, "accountId"

    invoke-virtual {v0, v3, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 497
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "https://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->HTTP_SERVER:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->MAIN_URL:Ljava/lang/String;

    .line 498
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->MAIN_URL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "platform/reporting/getPersonalPedometerStats"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->BASIC_STATS_URI:Ljava/lang/String;

    .line 499
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->BASIC_STATS_URI:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "byAge"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->BY_AGE_STATS_URI:Ljava/lang/String;

    .line 501
    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->BY_AGE_STATS_URI:Ljava/lang/String;

    invoke-static {v3, v1, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->getStringContent(Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;)Ljava/lang/String;

    move-result-object v2

    .line 502
    .local v2, "response":Ljava/lang/String;
    new-instance v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;

    invoke-direct {v3, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;-><init>(Ljava/lang/String;)V

    return-object v3
.end method

.method public static getWorldRanking(Lorg/json/JSONObject;)Ljava/util/ArrayList;
    .locals 7
    .param p0, "json"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 748
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 749
    .local v2, "rankingList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;>;"
    const-string/jumbo v4, "rankingList"

    invoke-virtual {p0, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 751
    .local v3, "worldRanking":Lorg/json/JSONArray;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 752
    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 754
    .local v1, "ranking":Lorg/json/JSONObject;
    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "i : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 755
    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 757
    new-instance v4, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    invoke-direct {v4, v1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;-><init>(Lorg/json/JSONObject;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 751
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 760
    .end local v1    # "ranking":Lorg/json/JSONObject;
    :cond_0
    return-object v2
.end method
