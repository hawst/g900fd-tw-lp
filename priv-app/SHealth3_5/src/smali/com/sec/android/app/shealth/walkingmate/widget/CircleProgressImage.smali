.class public Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;
.super Landroid/widget/ImageView;
.source "CircleProgressImage.java"


# static fields
.field private static final DEFAULT_FRAME_DURATION:I = 0x28

.field private static final DEGREES_IN_CIRCLE:I = 0x168

.field private static final DEGREES_IN_STEP:F = 0.25f

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isExcuting:Z

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mCanvas:Landroid/graphics/Canvas;

.field private mCenter:Landroid/graphics/Point;

.field private mColorTransparent:I

.field private mExecutor:Ljava/util/concurrent/ExecutorService;

.field private mGreenPaint:Landroid/graphics/Paint;

.field private mHeight:I

.field private mIsNeedInit:Z

.field private mMask:Landroid/graphics/Bitmap;

.field private mQueue:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mUpdateBitmap:Landroid/graphics/Bitmap;

.field private mUpdateCanvas:Landroid/graphics/Canvas;

.field mUpdateHandler:Landroid/os/Handler;

.field private mWhitePaint:Landroid/graphics/Paint;

.field private mWidth:I

.field private obj:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 29
    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mHeight:I

    .line 30
    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mWidth:I

    .line 40
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->isExcuting:Z

    .line 41
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->obj:Ljava/lang/Object;

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mIsNeedInit:Z

    .line 107
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$3;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mUpdateHandler:Landroid/os/Handler;

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    .line 53
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mHeight:I

    .line 30
    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mWidth:I

    .line 40
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->isExcuting:Z

    .line 41
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->obj:Ljava/lang/Object;

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mIsNeedInit:Z

    .line 107
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$3;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mUpdateHandler:Landroid/os/Handler;

    .line 54
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;
    .param p1, "x1"    # I

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->drawProgress(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)Ljava/util/concurrent/BlockingQueue;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mQueue:Ljava/util/concurrent/BlockingQueue;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)Landroid/graphics/Point;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mCenter:Landroid/graphics/Point;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mMask:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    .prologue
    .line 23
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mColorTransparent:I

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)Landroid/graphics/Paint;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mWhitePaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)Landroid/graphics/Paint;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mGreenPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)Ljava/util/concurrent/ExecutorService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mExecutor:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->obj:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->isExcuting:Z

    return v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;
    .param p1, "x1"    # Z

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->isExcuting:Z

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mUpdateBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mUpdateBitmap:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    .prologue
    .line 23
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mWidth:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    .prologue
    .line 23
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mHeight:I

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)Landroid/graphics/Canvas;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mUpdateCanvas:Landroid/graphics/Canvas;

    return-object v0
.end method

.method private drawProgress(I)V
    .locals 14
    .param p1, "end"    # I

    .prologue
    .line 166
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->obj:Ljava/lang/Object;

    monitor-enter v9

    .line 167
    :try_start_0
    iget-boolean v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->isExcuting:Z

    if-eqz v8, :cond_0

    .line 168
    monitor-exit v9

    .line 207
    :goto_0
    return-void

    .line 169
    :cond_0
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->obj:Ljava/lang/Object;

    monitor-enter v9

    .line 171
    const/4 v8, 0x1

    :try_start_1
    iput-boolean v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->isExcuting:Z

    .line 172
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 173
    const/4 v0, 0x0

    .local v0, "degree":F
    :goto_1
    const/high16 v8, 0x43b40000    # 360.0f

    cmpg-float v8, v0, v8

    if-gez v8, :cond_1

    .line 174
    const/4 v5, 0x0

    .line 175
    .local v5, "radius":I
    iget-boolean v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->isExcuting:Z

    if-nez v8, :cond_4

    .line 203
    .end local v5    # "radius":I
    :cond_1
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->obj:Ljava/lang/Object;

    monitor-enter v9

    .line 204
    const/4 v8, 0x0

    :try_start_2
    iput-boolean v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->isExcuting:Z

    .line 205
    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 206
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v8}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    goto :goto_0

    .line 169
    .end local v0    # "degree":F
    :catchall_0
    move-exception v8

    :try_start_3
    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v8

    .line 172
    :catchall_1
    move-exception v8

    :try_start_4
    monitor-exit v9
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v8

    .line 189
    .restart local v0    # "degree":F
    .local v1, "dx":I
    .local v2, "dy":I
    .restart local v5    # "radius":I
    .local v6, "x":I
    .local v7, "y":I
    :cond_2
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mMask:Landroid/graphics/Bitmap;

    invoke-virtual {v8, v6, v7}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v4

    .line 190
    .local v4, "pixel":I
    iget v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mColorTransparent:I

    if-eq v4, v8, :cond_3

    .line 191
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mCanvas:Landroid/graphics/Canvas;

    int-to-float v10, v6

    int-to-float v11, v7

    int-to-float v8, p1

    cmpg-float v8, v0, v8

    if-gez v8, :cond_7

    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mWhitePaint:Landroid/graphics/Paint;

    :goto_2
    invoke-virtual {v9, v10, v11, v8}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    .line 194
    :cond_3
    add-int/lit8 v5, v5, 0x1

    .line 178
    .end local v1    # "dx":I
    .end local v2    # "dy":I
    .end local v4    # "pixel":I
    .end local v6    # "x":I
    .end local v7    # "y":I
    :cond_4
    iget-boolean v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->isExcuting:Z

    if-nez v8, :cond_6

    .line 198
    :cond_5
    :goto_3
    :try_start_5
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mQueue:Ljava/util/concurrent/BlockingQueue;

    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mBitmap:Landroid/graphics/Bitmap;

    invoke-interface {v8, v9}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0

    .line 173
    :goto_4
    const/high16 v8, 0x3e800000    # 0.25f

    add-float/2addr v0, v8

    goto :goto_1

    .line 180
    :cond_6
    int-to-double v8, v5

    const-wide v10, 0x401921fb54442d18L    # 6.283185307179586

    float-to-double v12, v0

    mul-double/2addr v10, v12

    const-wide v12, 0x4076800000000000L    # 360.0

    div-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-int v2, v8

    .line 182
    .restart local v2    # "dy":I
    int-to-double v8, v5

    const-wide v10, 0x401921fb54442d18L    # 6.283185307179586

    float-to-double v12, v0

    mul-double/2addr v10, v12

    const-wide v12, 0x4076800000000000L    # 360.0

    div-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-int v1, v8

    .line 184
    .restart local v1    # "dx":I
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mCenter:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->x:I

    mul-int/lit8 v9, v1, -0x1

    sub-int v6, v8, v9

    .line 185
    .restart local v6    # "x":I
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mCenter:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->y:I

    sub-int v7, v8, v2

    .line 186
    .restart local v7    # "y":I
    if-ltz v6, :cond_5

    iget v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mWidth:I

    if-ge v6, v8, :cond_5

    if-ltz v7, :cond_5

    iget v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mHeight:I

    if-lt v7, v8, :cond_2

    goto :goto_3

    .line 191
    .restart local v4    # "pixel":I
    :cond_7
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mGreenPaint:Landroid/graphics/Paint;

    goto :goto_2

    .line 199
    .end local v1    # "dx":I
    .end local v2    # "dy":I
    .end local v4    # "pixel":I
    .end local v6    # "x":I
    .end local v7    # "y":I
    :catch_0
    move-exception v3

    .line 200
    .local v3, "e":Ljava/lang/InterruptedException;
    sget-object v8, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->TAG:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 205
    .end local v3    # "e":Ljava/lang/InterruptedException;
    .end local v5    # "radius":I
    :catchall_2
    move-exception v8

    :try_start_6
    monitor-exit v9
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v8
.end method

.method private initTools()V
    .locals 3

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0206ce

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mMask:Landroid/graphics/Bitmap;

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mMask:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mHeight:I

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mMask:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mWidth:I

    .line 60
    new-instance v0, Landroid/graphics/Point;

    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mHeight:I

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mWidth:I

    div-int/lit8 v2, v2, 0x2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mCenter:Landroid/graphics/Point;

    .line 61
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mWhitePaint:Landroid/graphics/Paint;

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mWhitePaint:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 63
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mGreenPaint:Landroid/graphics/Paint;

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mGreenPaint:Landroid/graphics/Paint;

    const v1, -0x669148ef

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mColorTransparent:I

    .line 66
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mQueue:Ljava/util/concurrent/BlockingQueue;

    .line 67
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mExecutor:Ljava/util/concurrent/ExecutorService;

    .line 68
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mQueue:Ljava/util/concurrent/BlockingQueue;

    .line 69
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mWidth:I

    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mHeight:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mBitmap:Landroid/graphics/Bitmap;

    .line 70
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mWidth:I

    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mHeight:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mUpdateBitmap:Landroid/graphics/Bitmap;

    .line 71
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mCanvas:Landroid/graphics/Canvas;

    .line 72
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mUpdateBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mUpdateCanvas:Landroid/graphics/Canvas;

    .line 73
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mIsNeedInit:Z

    .line 74
    return-void
.end method


# virtual methods
.method public initProgress(F)V
    .locals 3
    .param p1, "percent"    # F

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->initTools()V

    .line 84
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$1;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;F)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 90
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 91
    .local v0, "handler":Landroid/os/Handler;
    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$2;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 105
    return-void
.end method

.method public setProgress(F)V
    .locals 2
    .param p1, "percent"    # F

    .prologue
    .line 125
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mIsNeedInit:Z

    if-eqz v1, :cond_0

    .line 126
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->initTools()V

    .line 127
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mIsNeedInit:Z

    .line 129
    :cond_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$4;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$4;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;F)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 162
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 163
    return-void
.end method
