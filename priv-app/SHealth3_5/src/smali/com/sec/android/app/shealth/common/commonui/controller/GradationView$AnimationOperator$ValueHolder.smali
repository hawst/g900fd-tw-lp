.class Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator$ValueHolder;
.super Ljava/lang/Object;
.source "GradationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ValueHolder"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;)V
    .locals 0

    .prologue
    .line 706
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator$ValueHolder;->this$1:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$1;

    .prologue
    .line 706
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator$ValueHolder;-><init>(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;)V

    return-void
.end method


# virtual methods
.method public getAnimationValue()F
    .locals 1

    .prologue
    .line 714
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator$ValueHolder;->this$1:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mValue:F
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->access$1800(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)F

    move-result v0

    return v0
.end method

.method public setAnimationValue(F)V
    .locals 1
    .param p1, "value"    # F

    .prologue
    .line 709
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator$ValueHolder;->this$1:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->setValueWithSound(F)V
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->access$2300(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;F)V

    .line 710
    return-void
.end method
