.class Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$7;
.super Ljava/lang/Object;
.source "DataMigrationHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->startLocalMigration()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)V
    .locals 0

    .prologue
    .line 353
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 357
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationProgressMsg:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$1000(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationProgressMsg:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$1000(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090804

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 359
    :cond_0
    invoke-static {}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->getInstance()Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    iget-object v2, v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mLocalMigrationStatusListener:Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->migrateLocalDataFrom3xto35(Landroid/content/Context;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 360
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "MIGRATION - migrateLocalDataFrom3xto35 Failed"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mAppID:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$200(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "MI04 ERROR CODE : migrateLocalDataFrom3xto35() returned false"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    :cond_1
    return-void
.end method
