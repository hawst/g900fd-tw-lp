.class public Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;
.super Ljava/lang/Object;
.source "PeriodUtils.java"


# static fields
.field public static final DAYS_IN_WEEK:B = 0x7t

.field public static final DAY_IN_MONTH:B = 0x1ft

.field public static final HOURS_IN_DAY:B = 0x18t

.field public static final LAST_DAY_IN_WEEK:B = 0x6t

.field public static final LAST_HOUR_IN_DAY:B = 0x17t

.field public static final LAST_MILLI_IN_SECOND:I = 0x3e7

.field public static final LAST_MINUTE_IN_HOUR:B = 0x3bt

.field public static final LAST_SECOND_IN_MINUTE:B = 0x3bt

.field public static final MILLIS_IN_DAY:I = 0x5265c00

.field public static final MILLIS_IN_HALFDAY:I = 0x2932e00

.field public static final MILLIS_IN_HOUR:I = 0x36ee80

.field public static final MILLIS_IN_MINUTE:I = 0xea60

.field public static final MILLIS_IN_MONTH:I = -0x605adc00

.field public static final MILLIS_IN_SECOND:I = 0x3e8

.field public static final MILLIS_IN_WEEK:I = 0x240c8400

.field public static final MINUTES_IN_HOUR:B = 0x3ct

.field public static final MONTH_IN_YEAR:I = 0xc

.field public static final SECONDS_IN_MINUTE:B = 0x3ct

.field private static final TAG:Ljava/lang/String;

.field public static final WEEK_STARTS_FROM_SUNDAY:B

.field public static infoWeekFormat:I

.field public static mContext:Landroid/content/Context;

.field private static final tempCal:Ljava/util/Calendar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->infoWeekFormat:I

    .line 26
    const-class v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->TAG:Ljava/lang/String;

    .line 121
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static areTimesInSamePeriod(JJI)Z
    .locals 4
    .param p0, "time1"    # J
    .param p2, "time2"    # J
    .param p4, "periodType"    # I

    .prologue
    .line 923
    invoke-static {p0, p1, p4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfCurrentPeriod(JI)J

    move-result-wide v0

    invoke-static {p2, p3, p4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfCurrentPeriod(JI)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static convertHoursToMillis(I)I
    .locals 1
    .param p0, "hours"    # I

    .prologue
    .line 910
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->convertHoursToMinutes(I)I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->convertMinutesToMillis(I)I

    move-result v0

    return v0
.end method

.method public static convertHoursToMinutes(I)I
    .locals 1
    .param p0, "hours"    # I

    .prologue
    .line 870
    mul-int/lit8 v0, p0, 0x3c

    return v0
.end method

.method public static convertIntoDayTime(Ljava/util/Calendar;)F
    .locals 3
    .param p0, "calendar"    # Ljava/util/Calendar;

    .prologue
    .line 982
    const/16 v0, 0xb

    invoke-virtual {p0, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-float v0, v0

    const/16 v1, 0xc

    invoke-virtual {p0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x42700000    # 60.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public static convertMillisToHours(I)I
    .locals 1
    .param p0, "millis"    # I

    .prologue
    .line 830
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->convertMillisToMinutes(I)I

    move-result v0

    div-int/lit8 v0, v0, 0x3c

    return v0
.end method

.method public static convertMillisToMinutes(I)I
    .locals 1
    .param p0, "millis"    # I

    .prologue
    .line 819
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->convertMillisToSeconds(I)I

    move-result v0

    div-int/lit8 v0, v0, 0x3c

    return v0
.end method

.method public static convertMillisToSeconds(I)I
    .locals 1
    .param p0, "millis"    # I

    .prologue
    .line 809
    div-int/lit16 v0, p0, 0x3e8

    return v0
.end method

.method public static convertMinutesToHours(I)I
    .locals 1
    .param p0, "minutes"    # I

    .prologue
    .line 860
    div-int/lit8 v0, p0, 0x3c

    return v0
.end method

.method public static convertMinutesToMillis(I)I
    .locals 1
    .param p0, "minutes"    # I

    .prologue
    .line 900
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->convertMinutesToSeconds(I)I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->convertSecondsToMillis(I)I

    move-result v0

    return v0
.end method

.method public static convertMinutesToSeconds(I)I
    .locals 1
    .param p0, "minutes"    # I

    .prologue
    .line 880
    mul-int/lit8 v0, p0, 0x3c

    return v0
.end method

.method public static convertSecondsToHours(I)I
    .locals 1
    .param p0, "seconds"    # I

    .prologue
    .line 850
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->convertSecondsToMinutes(I)I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->convertMinutesToHours(I)I

    move-result v0

    return v0
.end method

.method public static convertSecondsToMillis(I)I
    .locals 1
    .param p0, "seconds"    # I

    .prologue
    .line 890
    mul-int/lit16 v0, p0, 0x3e8

    return v0
.end method

.method public static convertSecondsToMinutes(I)I
    .locals 1
    .param p0, "seconds"    # I

    .prologue
    .line 840
    div-int/lit8 v0, p0, 0x3c

    return v0
.end method

.method public static getCountDaysToNow(J)I
    .locals 8
    .param p0, "time"    # J

    .prologue
    .line 320
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfYear(J)J

    move-result-wide v0

    .line 322
    .local v0, "startOfYear":J
    sget-object v3, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    monitor-enter v3

    .line 324
    :try_start_0
    sget-object v2, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v2, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 325
    sget-object v2, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v4, 0xb

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 326
    sget-object v2, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v4, 0xc

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 327
    sget-object v2, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v4, 0xd

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 328
    sget-object v2, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v4, 0xe

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 330
    sget-object v2, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    sub-long/2addr v4, v0

    const-wide/32 v6, 0x5265c00

    div-long/2addr v4, v6

    long-to-int v2, v4

    monitor-exit v3

    return v2

    .line 331
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private static getCountMonthsFromStart(J)I
    .locals 3
    .param p0, "time"    # J

    .prologue
    .line 304
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    monitor-enter v1

    .line 306
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 308
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    monitor-exit v1

    return v0

    .line 309
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getCountUnitsFromStart(JI)I
    .locals 1
    .param p0, "time"    # J
    .param p2, "periodType"    # I

    .prologue
    .line 283
    packed-switch p2, :pswitch_data_0

    .line 292
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 289
    :pswitch_0
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getCountMonthsFromStart(J)I

    move-result v0

    goto :goto_0

    .line 283
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static getCurrentSystemDateFormatType()Lcom/sec/android/app/shealth/common/utils/calendar/DateFormatType;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 443
    sget-object v3, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v3

    check-cast v3, Ljava/text/SimpleDateFormat;

    invoke-virtual {v3}, Ljava/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object v2

    .line 447
    .local v2, "pattern":Ljava/lang/String;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->getDirectionality(C)B

    move-result v0

    .line 448
    .local v0, "directionality":I
    if-eq v0, v4, :cond_0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    :cond_0
    move v1, v4

    .line 450
    .local v1, "isRTL":Z
    :cond_1
    if-eqz v1, :cond_4

    .line 452
    const-string v3, "dd"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 454
    sget-object v3, Lcom/sec/android/app/shealth/common/utils/calendar/DateFormatType;->YYYYMMDD:Lcom/sec/android/app/shealth/common/utils/calendar/DateFormatType;

    .line 476
    :goto_0
    return-object v3

    .line 456
    :cond_2
    const-string v3, "MM"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 458
    sget-object v3, Lcom/sec/android/app/shealth/common/utils/calendar/DateFormatType;->YYYYDDMM:Lcom/sec/android/app/shealth/common/utils/calendar/DateFormatType;

    goto :goto_0

    .line 462
    :cond_3
    sget-object v3, Lcom/sec/android/app/shealth/common/utils/calendar/DateFormatType;->DDMMYYYY:Lcom/sec/android/app/shealth/common/utils/calendar/DateFormatType;

    goto :goto_0

    .line 466
    :cond_4
    const-string v3, "dd"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 468
    sget-object v3, Lcom/sec/android/app/shealth/common/utils/calendar/DateFormatType;->DDMMYYYY:Lcom/sec/android/app/shealth/common/utils/calendar/DateFormatType;

    goto :goto_0

    .line 470
    :cond_5
    const-string v3, "MM"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 472
    sget-object v3, Lcom/sec/android/app/shealth/common/utils/calendar/DateFormatType;->MMDDYYYY:Lcom/sec/android/app/shealth/common/utils/calendar/DateFormatType;

    goto :goto_0

    .line 476
    :cond_6
    sget-object v3, Lcom/sec/android/app/shealth/common/utils/calendar/DateFormatType;->YYYYMMDD:Lcom/sec/android/app/shealth/common/utils/calendar/DateFormatType;

    goto :goto_0
.end method

.method public static getCurrentSystemTimeFormatType()Ljava/text/SimpleDateFormat;
    .locals 3

    .prologue
    .line 489
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    check-cast v1, Ljava/text/SimpleDateFormat;

    invoke-virtual {v1}, Ljava/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object v0

    .line 490
    .local v0, "pattern":Ljava/lang/String;
    const-string v1, "a"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 492
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "hh:mm:ss a"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 496
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "hh:mm:ss"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getDateInAndroidFormat(J)Ljava/lang/String;
    .locals 3
    .param p0, "date"    # J

    .prologue
    .line 342
    sget-object v2, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    monitor-enter v2

    .line 345
    :try_start_0
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v1, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 346
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 347
    .local v0, "dateFormat":Ljava/text/DateFormat;
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 349
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    monitor-exit v2

    return-object v1

    .line 350
    .end local v0    # "dateFormat":Ljava/text/DateFormat;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static getDateInAndroidFormatWithoutYear(J)Ljava/lang/String;
    .locals 7
    .param p0, "date"    # J

    .prologue
    .line 361
    sget-object v4, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    monitor-enter v4

    .line 363
    :try_start_0
    sget-object v3, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v3, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 365
    sget-object v3, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 366
    .local v1, "dateFormat":Ljava/text/DateFormat;
    move-object v0, v1

    check-cast v0, Ljava/text/SimpleDateFormat;

    move-object v2, v0

    .line 367
    .local v2, "simpleDateFormat":Ljava/text/SimpleDateFormat;
    invoke-virtual {v2}, Ljava/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object v3

    const-string v5, "[^\\p{Alpha}]*y+[^\\p{Alpha}]*"

    const-string v6, ""

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 369
    sget-object v3, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    monitor-exit v4

    return-object v3

    .line 370
    .end local v1    # "dateFormat":Ljava/text/DateFormat;
    .end local v2    # "simpleDateFormat":Ljava/text/SimpleDateFormat;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public static getDateInAndroidFormatWithoutYear(JLjava/util/Locale;)Ljava/lang/String;
    .locals 7
    .param p0, "date"    # J
    .param p2, "loc"    # Ljava/util/Locale;

    .prologue
    .line 381
    sget-object v4, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    monitor-enter v4

    .line 383
    :try_start_0
    sget-object v3, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v3, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 385
    sget-object v3, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 386
    .local v1, "dateFormat":Ljava/text/DateFormat;
    move-object v0, v1

    check-cast v0, Ljava/text/SimpleDateFormat;

    move-object v2, v0

    .line 387
    .local v2, "simpleDateFormat":Ljava/text/SimpleDateFormat;
    invoke-virtual {v2}, Ljava/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object v3

    const-string v5, "[^\\p{Alpha}]*y+[^\\p{Alpha}]*"

    const-string v6, ""

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 388
    if-nez p2, :cond_0

    .line 389
    sget-object p2, Ljava/util/Locale;->US:Ljava/util/Locale;

    .line 390
    :cond_0
    invoke-static {p2}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->setNumberFormat(Ljava/text/NumberFormat;)V

    .line 392
    sget-object v3, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    monitor-exit v4

    return-object v3

    .line 393
    .end local v1    # "dateFormat":Ljava/text/DateFormat;
    .end local v2    # "simpleDateFormat":Ljava/text/SimpleDateFormat;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public static getDateInWorkOutDetailFormat(J)Ljava/lang/String;
    .locals 3
    .param p0, "date"    # J

    .prologue
    .line 1007
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "yyyy/MM/dd"

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1008
    .local v0, "dateFormatter":Ljava/text/SimpleDateFormat;
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1009
    .local v1, "detailDate":Ljava/lang/String;
    return-object v1
.end method

.method public static getDateInstance(J)Ljava/sql/Date;
    .locals 4
    .param p0, "time"    # J

    .prologue
    .line 658
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    monitor-enter v1

    .line 660
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 662
    new-instance v0, Ljava/sql/Date;

    sget-object v2, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/sql/Date;-><init>(J)V

    monitor-exit v1

    return-object v0

    .line 663
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getEndOfCurrentPeriod(JI)J
    .locals 2
    .param p0, "curTime"    # J
    .param p2, "periodType"    # I

    .prologue
    .line 676
    packed-switch p2, :pswitch_data_0

    .line 696
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    .line 679
    :pswitch_0
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v0

    goto :goto_0

    .line 682
    :pswitch_1
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfWeek(J)J

    move-result-wide v0

    goto :goto_0

    .line 685
    :pswitch_2
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfMonth(J)J

    move-result-wide v0

    goto :goto_0

    .line 688
    :pswitch_3
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfYear(J)J

    move-result-wide v0

    goto :goto_0

    .line 691
    :pswitch_4
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDecade(J)J

    move-result-wide v0

    goto :goto_0

    .line 694
    :pswitch_5
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0

    .line 676
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static getEndOfDay(J)J
    .locals 4
    .param p0, "time"    # J

    .prologue
    .line 217
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    monitor-enter v1

    .line 220
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 221
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xb

    const/16 v3, 0x17

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 222
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xc

    const/16 v3, 0x3b

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 223
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xd

    const/16 v3, 0x3b

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 224
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xe

    const/16 v3, 0x3e7

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 226
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    .line 227
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getEndOfDecade(J)J
    .locals 6
    .param p0, "time"    # J

    .prologue
    .line 260
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    monitor-enter v1

    .line 262
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 263
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xb

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 264
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xc

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 265
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xd

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 266
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xe

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 267
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/4 v2, 0x6

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 268
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/4 v2, 0x1

    sget-object v3, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    div-int/lit8 v3, v3, 0xa

    mul-int/lit8 v3, v3, 0xa

    add-int/lit8 v3, v3, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 270
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    monitor-exit v1

    return-wide v2

    .line 271
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getEndOfHour(J)J
    .locals 4
    .param p0, "time"    # J

    .prologue
    .line 239
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    monitor-enter v1

    .line 242
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 243
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xc

    const/16 v3, 0x3b

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 244
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xd

    const/16 v3, 0x3b

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 245
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xe

    const/16 v3, 0x3e7

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 247
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    .line 248
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getEndOfMonth(J)J
    .locals 4
    .param p0, "time"    # J

    .prologue
    .line 167
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    monitor-enter v1

    .line 169
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 170
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xb

    const/16 v3, 0x17

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 171
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xc

    const/16 v3, 0x3b

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 172
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xd

    const/16 v3, 0x3b

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 173
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xe

    const/16 v3, 0x3e7

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 174
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->roll(II)V

    .line 175
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 176
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/4 v2, 0x6

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->roll(II)V

    .line 178
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    .line 179
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getEndOfWeek(J)J
    .locals 3
    .param p0, "time"    # J

    .prologue
    .line 707
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->isWeekStartsFromSunday()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x7

    .line 709
    .local v0, "endOfWeek":I
    :goto_0
    invoke-static {p0, p1, v0}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfWeek(JI)J

    move-result-wide v1

    return-wide v1

    .line 707
    .end local v0    # "endOfWeek":I
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static getEndOfWeek(JI)J
    .locals 4
    .param p0, "time"    # J
    .param p2, "day"    # I

    .prologue
    .line 191
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    monitor-enter v1

    .line 193
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 194
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xb

    const/16 v3, 0x17

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 195
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xc

    const/16 v3, 0x3b

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 196
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xd

    const/16 v3, 0x3b

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 197
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xe

    const/16 v3, 0x3e7

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 199
    :goto_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/4 v2, 0x7

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-eq v0, p2, :cond_0

    .line 201
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/4 v2, 0x6

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->add(II)V

    goto :goto_0

    .line 205
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 204
    :cond_0
    :try_start_1
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-wide v2
.end method

.method public static getEndOfYear(J)J
    .locals 4
    .param p0, "time"    # J

    .prologue
    .line 143
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    monitor-enter v1

    .line 145
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 146
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xb

    const/16 v3, 0x17

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 147
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xc

    const/16 v3, 0x3b

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 148
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xd

    const/16 v3, 0x3b

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 149
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xe

    const/16 v3, 0x3e7

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 150
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/4 v2, 0x2

    const/16 v3, 0xb

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 151
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 152
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->roll(II)V

    .line 153
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/4 v2, 0x6

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->roll(II)V

    .line 155
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    .line 156
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getNextPeriod(I)I
    .locals 1
    .param p0, "periodType"    # I

    .prologue
    .line 756
    packed-switch p0, :pswitch_data_0

    .line 770
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 759
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 762
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 765
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 768
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 756
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getPreviousPeriod(I)I
    .locals 1
    .param p0, "periodType"    # I

    .prologue
    .line 785
    packed-switch p0, :pswitch_data_0

    .line 799
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 788
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 791
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 794
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 797
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 785
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getStartOfCurrentPeriod(JI)J
    .locals 2
    .param p0, "curTime"    # J
    .param p2, "periodType"    # I

    .prologue
    .line 722
    packed-switch p2, :pswitch_data_0

    .line 742
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    .line 725
    :pswitch_0
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v0

    goto :goto_0

    .line 731
    :pswitch_1
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v0

    goto :goto_0

    .line 734
    :pswitch_2
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfYear(J)J

    move-result-wide v0

    goto :goto_0

    .line 737
    :pswitch_3
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDecade(J)J

    move-result-wide v0

    goto :goto_0

    .line 740
    :pswitch_4
    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_0

    .line 722
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static getStartOfDay(J)J
    .locals 4
    .param p0, "time"    # J

    .prologue
    .line 555
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    monitor-enter v1

    .line 557
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 558
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xb

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 559
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xc

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 560
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xd

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 561
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xe

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 563
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    .line 564
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getStartOfDecade(J)J
    .locals 5
    .param p0, "time"    # J

    .prologue
    .line 637
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    monitor-enter v1

    .line 639
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 640
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xb

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 641
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xc

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 642
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xd

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 643
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xe

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 644
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/4 v2, 0x6

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 645
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/4 v2, 0x1

    sget-object v3, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    div-int/lit8 v3, v3, 0xa

    mul-int/lit8 v3, v3, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 647
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    .line 648
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getStartOfHour(J)J
    .locals 4
    .param p0, "time"    # J

    .prologue
    .line 575
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    monitor-enter v1

    .line 577
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 578
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xc

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 579
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xd

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 580
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xe

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 582
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    .line 583
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getStartOfMonth(J)J
    .locals 4
    .param p0, "time"    # J

    .prologue
    .line 616
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    monitor-enter v1

    .line 618
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 619
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xb

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 620
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xc

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 621
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xd

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 622
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xe

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 623
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 625
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    .line 626
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getStartOfWeekFromDay(JI)J
    .locals 4
    .param p0, "time"    # J
    .param p2, "day"    # I

    .prologue
    .line 531
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    monitor-enter v1

    .line 533
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 534
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xb

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 535
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xc

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 536
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xd

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 537
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xe

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 539
    :goto_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/4 v2, 0x7

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-eq v0, p2, :cond_0

    .line 541
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/4 v2, 0x6

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->add(II)V

    goto :goto_0

    .line 544
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 543
    :cond_0
    :try_start_1
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-wide v2
.end method

.method public static getStartOfYear(J)J
    .locals 4
    .param p0, "time"    # J

    .prologue
    .line 595
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    monitor-enter v1

    .line 597
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 598
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xb

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 599
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xc

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 600
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xd

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 601
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xe

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 602
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    const/4 v2, 0x6

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 604
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    .line 605
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getTimeInAndroidFormat(J)Ljava/lang/String;
    .locals 3
    .param p0, "time"    # J

    .prologue
    .line 404
    sget-object v2, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    monitor-enter v2

    .line 406
    :try_start_0
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v1, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 407
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 408
    .local v0, "timeFormat":Ljava/text/DateFormat;
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 410
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    monitor-exit v2

    return-object v1

    .line 411
    .end local v0    # "timeFormat":Ljava/text/DateFormat;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static getTimeInAndroidFormat(JLjava/util/Locale;)Ljava/lang/String;
    .locals 3
    .param p0, "time"    # J
    .param p2, "loc"    # Ljava/util/Locale;

    .prologue
    .line 422
    sget-object v2, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    monitor-enter v2

    .line 424
    :try_start_0
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v1, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 425
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 426
    .local v0, "timeFormat":Ljava/text/DateFormat;
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 427
    if-nez p2, :cond_0

    .line 428
    sget-object p2, Ljava/util/Locale;->US:Ljava/util/Locale;

    .line 429
    :cond_0
    invoke-static {p2}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setNumberFormat(Ljava/text/NumberFormat;)V

    .line 431
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    monitor-exit v2

    return-object v1

    .line 432
    .end local v0    # "timeFormat":Ljava/text/DateFormat;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static getTimeInApplicationFormat(J)Ljava/lang/String;
    .locals 8
    .param p0, "date"    # J

    .prologue
    const/16 v7, 0xa

    .line 509
    const-string v4, ""

    .line 513
    .local v4, "timeToShow":Ljava/lang/String;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 514
    .local v0, "cal":Ljava/util/Calendar;
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 515
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    add-int/lit8 v3, v5, 0x1

    .line 516
    .local v3, "month":I
    const/4 v5, 0x5

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 517
    .local v1, "day":I
    const-string v5, " E"

    invoke-static {v5, p0, p1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v2

    .line 518
    .local v2, "dayLetters":Ljava/lang/CharSequence;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    if-ge v3, v7, :cond_0

    const-string v5, "0"

    :goto_0
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-ge v1, v7, :cond_1

    const-string v5, "0"

    :goto_1
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 520
    return-object v4

    .line 518
    :cond_0
    const-string v5, ""

    goto :goto_0

    :cond_1
    const-string v5, ""

    goto :goto_1
.end method

.method public static getTimeWithoutSeconds(J)J
    .locals 3
    .param p0, "time"    # J

    .prologue
    const/4 v2, 0x0

    .line 1020
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1021
    .local v0, "cal":Ljava/util/Calendar;
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1022
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 1023
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 1024
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    return-wide v1
.end method

.method public static isDateToday(Ljava/util/Date;)Z
    .locals 3
    .param p0, "date"    # Ljava/util/Date;

    .prologue
    .line 995
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v0

    .line 996
    .local v0, "today":Ljava/util/Date;
    invoke-virtual {p0}, Ljava/util/Date;->getYear()I

    move-result v1

    invoke-virtual {v0}, Ljava/util/Date;->getYear()I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Ljava/util/Date;->getMonth()I

    move-result v1

    invoke-virtual {v0}, Ljava/util/Date;->getMonth()I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Ljava/util/Date;->getDate()I

    move-result v1

    invoke-virtual {v0}, Ljava/util/Date;->getDate()I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isWeekStartsFromSunday()Z
    .locals 1

    .prologue
    .line 972
    sget v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->infoWeekFormat:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static makeItBold(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 933
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<b><font color=#34470b>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</font></b>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static makeItBoldTransparentDateBar(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 953
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<b><font color=#ffffff>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</font></b>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static makeItGreen(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 943
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<font color=#34470b>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</font>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static makeItWhiteDateBar(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 963
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<b><font color=#ffffff>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</font></b>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static setFixedTimeZone(Ljava/lang/String;)V
    .locals 3
    .param p0, "timeZoneId"    # Ljava/lang/String;

    .prologue
    .line 129
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    monitor-enter v1

    .line 131
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->tempCal:Ljava/util/Calendar;

    invoke-static {p0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 132
    monitor-exit v1

    .line 133
    return-void

    .line 132
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
