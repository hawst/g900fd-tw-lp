.class Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;
.super Ljava/io/FilterOutputStream;
.source "Base64Util.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OutputStream"
.end annotation


# instance fields
.field private b4:[B

.field private breakLines:Z

.field private buffer:[B

.field private bufferLength:I

.field private encode:Z

.field private lineLength:I

.field private position:I

.field private suspendEncoding:Z


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;

    .prologue
    .line 277
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;-><init>(Ljava/io/OutputStream;I)V

    .line 278
    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;I)V
    .locals 5
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "options"    # I

    .prologue
    const/4 v3, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 281
    invoke-direct {p0, p1}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 282
    and-int/lit8 v0, p2, 0x8

    const/16 v4, 0x8

    if-eq v0, v4, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->breakLines:Z

    .line 283
    and-int/lit8 v0, p2, 0x1

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->encode:Z

    .line 284
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->encode:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    :goto_2
    iput v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->bufferLength:I

    .line 285
    iget v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->bufferLength:I

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->buffer:[B

    .line 286
    iput v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->position:I

    .line 287
    iput v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->lineLength:I

    .line 288
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->suspendEncoding:Z

    .line 289
    new-array v0, v3, [B

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->b4:[B

    .line 290
    return-void

    :cond_0
    move v0, v2

    .line 282
    goto :goto_0

    :cond_1
    move v1, v2

    .line 283
    goto :goto_1

    :cond_2
    move v0, v3

    .line 284
    goto :goto_2
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 346
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->flushBase64()V

    .line 347
    invoke-super {p0}, Ljava/io/FilterOutputStream;->close()V

    .line 348
    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->buffer:[B

    .line 349
    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->out:Ljava/io/OutputStream;

    .line 350
    return-void
.end method

.method public flushBase64()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 335
    iget v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->position:I

    if-lez v0, :cond_0

    .line 336
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->encode:Z

    if-eqz v0, :cond_1

    .line 337
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->out:Ljava/io/OutputStream;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->b4:[B

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->buffer:[B

    iget v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->position:I

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->encode3to4([B[BI)[B
    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->access$000([B[BI)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 338
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->position:I

    .line 343
    :cond_0
    return-void

    .line 340
    :cond_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Base64Util input not properly padded."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public write(I)V
    .locals 6
    .param p1, "theByte"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, -0x5

    const/4 v5, 0x0

    .line 293
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->suspendEncoding:Z

    if-eqz v1, :cond_1

    .line 294
    iget-object v1, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v1, p1}, Ljava/io/OutputStream;->write(I)V

    .line 321
    :cond_0
    :goto_0
    return-void

    .line 297
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->encode:Z

    if-eqz v1, :cond_3

    .line 298
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->buffer:[B

    iget v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->position:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->position:I

    int-to-byte v3, p1

    aput-byte v3, v1, v2

    .line 299
    iget v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->position:I

    iget v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->bufferLength:I

    if-lt v1, v2, :cond_0

    .line 301
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->out:Ljava/io/OutputStream;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->b4:[B

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->buffer:[B

    iget v4, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->bufferLength:I

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->encode3to4([B[BI)[B
    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->access$000([B[BI)[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write([B)V

    .line 302
    iget v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->lineLength:I

    add-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->lineLength:I

    .line 303
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->breakLines:Z

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->lineLength:I

    const/16 v2, 0x4c

    if-lt v1, v2, :cond_2

    .line 304
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->out:Ljava/io/OutputStream;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write(I)V

    .line 305
    iput v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->lineLength:I

    .line 307
    :cond_2
    iput v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->position:I

    goto :goto_0

    .line 310
    :cond_3
    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->DECODABET:[B
    invoke-static {}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->access$100()[B

    move-result-object v1

    and-int/lit8 v2, p1, 0x7f

    aget-byte v1, v1, v2

    if-le v1, v3, :cond_4

    .line 311
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->buffer:[B

    iget v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->position:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->position:I

    int-to-byte v3, p1

    aput-byte v3, v1, v2

    .line 312
    iget v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->position:I

    iget v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->bufferLength:I

    if-lt v1, v2, :cond_0

    .line 313
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->buffer:[B

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->b4:[B

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->decode4to3([BI[BI)I
    invoke-static {v1, v5, v2, v5}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->access$200([BI[BI)I

    move-result v0

    .line 314
    .local v0, "len":I
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->out:Ljava/io/OutputStream;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->b4:[B

    invoke-virtual {v1, v2, v5, v0}, Ljava/io/OutputStream;->write([BII)V

    .line 315
    iput v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->position:I

    goto :goto_0

    .line 317
    .end local v0    # "len":I
    :cond_4
    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->DECODABET:[B
    invoke-static {}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->access$100()[B

    move-result-object v1

    and-int/lit8 v2, p1, 0x7f

    aget-byte v1, v1, v2

    if-eq v1, v3, :cond_0

    .line 318
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Invalid character in Base64Util data."

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public write([BII)V
    .locals 2
    .param p1, "theBytes"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 325
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->suspendEncoding:Z

    if-eqz v1, :cond_1

    .line 326
    iget-object v1, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v1, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 332
    :cond_0
    return-void

    .line 329
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p3, :cond_0

    .line 330
    add-int v1, p2, v0

    aget-byte v1, p1, v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;->write(I)V

    .line 329
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
