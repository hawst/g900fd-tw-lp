.class public Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
.source "EveryoneRankingsFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingAsync$RankingAsyncListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ResourceAsColor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$keyblockhandler;
    }
.end annotation


# static fields
.field private static final BROADCAST_MESSAGE:I = 0x0

.field private static final CLIENT_ID:Ljava/lang/String; = "1y90e30264"

.field private static final CLIENT_SECRET:Ljava/lang/String; = "80E7ECD9D301CB7888C73703639302E5"

.field private static final DISMISSDIALOG:I = 0x3

.field private static final LIST_UPDATE_MESSAGE:I = 0x1

.field private static final PERIOD_DAILY:I = 0x2

.field private static final PERIOD_LASTWEEK:I = 0x1

.field private static final PERIOD_MAX:I = 0x3

.field private static final PERIOD_THISWEEK:I = 0x0

.field private static final REGISTER_NEW_SAMSUNG_TOKEN_MESSAGE:I = 0x4

.field private static final UPDATE_MESSAGE_EPIC_FAIL:I = 0x2

.field private static mIsBlocked:Z


# instance fields
.field private TAG:Ljava/lang/String;

.field private loadingDialog:Lcom/sec/android/app/shealth/walkingmate/utils/Utils$LoadingDialog;

.field private mActivity:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

.field private mAdapter:Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;

.field private mContent:Landroid/view/View;

.field private mDefaultListProfileIcon:[Landroid/graphics/drawable/BitmapDrawable;

.field private mImageLoadingTasksList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/os/AsyncTask;",
            ">;"
        }
    .end annotation
.end field

.field private mIsWalkersUpdated:Z

.field mItemClickListener:Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter$OnItemClickListener;

.field private mNoNetworkLayout:Landroid/widget/RelativeLayout;

.field private mOnNetworkLayout:Landroid/widget/LinearLayout;

.field private mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

.field private mShouldSaveWalkersDataLocally:Z

.field private mTV_dropdown_day_seleter:Landroid/widget/TextView;

.field private mTimeFilter:Landroid/view/View;

.field private mTopWalkersListView:Landroid/widget/ListView;

.field private mTopWalkersUpdater:Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;

.field private mUpdateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

.field private mUpdateTextView:Landroid/widget/TextView;

.field private mWalk_my_zero:Landroid/view/View;

.field private myHeaderView:Landroid/view/View;

.field private myValuesHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

.field private period_select_idx:I

.field private topWalkersList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 124
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mIsBlocked:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 740
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 70
    const-class v0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    .line 77
    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->period_select_idx:I

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->topWalkersList:Ljava/util/ArrayList;

    .line 101
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mIsWalkersUpdated:Z

    .line 103
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mShouldSaveWalkersDataLocally:Z

    .line 105
    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mImageLoadingTasksList:Ljava/util/List;

    .line 116
    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mContent:Landroid/view/View;

    .line 122
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->DEFAULT:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    .line 131
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mUpdateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    .line 814
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$8;-><init>(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mItemClickListener:Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter$OnItemClickListener;

    .line 741
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->setRetainInstance(Z)V

    .line 742
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->sendRequestForNewUserToken()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->sendRequestForUserToken()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->myValuesHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;)Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->myValuesHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)[Landroid/graphics/drawable/BitmapDrawable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mDefaultListProfileIcon:[Landroid/graphics/drawable/BitmapDrawable;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mUpdateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->visibleZeroRankingLayout()V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->refreshFragmentFocusables()V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->showUpdateFail()V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->refreshFragmentFocusables()V

    return-void
.end method

.method static synthetic access$1700()Z
    .locals 1

    .prologue
    .line 68
    sget-boolean v0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mIsBlocked:Z

    return v0
.end method

.method static synthetic access$1702(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 68
    sput-boolean p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mIsBlocked:Z

    return p0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->blockManyClick()V

    return-void
.end method

.method static synthetic access$1900(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    .prologue
    .line 68
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->period_select_idx:I

    return v0
.end method

.method static synthetic access$1902(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;
    .param p1, "x1"    # I

    .prologue
    .line 68
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->period_select_idx:I

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->topWalkersList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;
    .param p1, "x1"    # I

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getExtraStringPeriod(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->topWalkersList:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mIsWalkersUpdated:Z

    return v0
.end method

.method static synthetic access$2102(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mIsWalkersUpdated:Z

    return p1
.end method

.method static synthetic access$2200(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTopWalkersUpdater:Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;)Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTopWalkersUpdater:Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;Landroid/content/Intent;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;
    .param p1, "x1"    # Landroid/content/Intent;
    .param p2, "x2"    # Z

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->requestTopWalkersListUpdate(Landroid/content/Intent;Z)V

    return-void
.end method

.method static synthetic access$2400(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->loadOfflineData()V

    return-void
.end method

.method static synthetic access$2500(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTV_dropdown_day_seleter:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->showLoadingDialog()V

    return-void
.end method

.method static synthetic access$2700(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->updateValues()V

    return-void
.end method

.method static synthetic access$2800(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    return-object v0
.end method

.method static synthetic access$2802(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;)Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    return-object p1
.end method

.method static synthetic access$2902(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mShouldSaveWalkersDataLocally:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->loadWalkersListItemsIconsImages()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->checkMyRanking()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTimeFilter:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTopWalkersListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mActivity:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mUpdateTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mAdapter:Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;)Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mAdapter:Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;

    return-object p1
.end method

.method private addFootMyRanking()V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v9, -0x1

    const/4 v8, 0x0

    .line 214
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030287

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->myHeaderView:Landroid/view/View;

    .line 215
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->myHeaderView:Landroid/view/View;

    const v2, 0x7f080b7b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    .line 216
    .local v6, "container":Landroid/view/ViewGroup;
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;-><init>(Landroid/content/Context;)V

    .line 217
    .local v0, "myRanking":Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->myValuesHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->myValuesHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mDefaultListProfileIcon:[Landroid/graphics/drawable/BitmapDrawable;

    aget-object v4, v4, v8

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mDefaultListProfileIcon:[Landroid/graphics/drawable/BitmapDrawable;

    aget-object v5, v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->setValuesHolder(Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;ZLandroid/graphics/drawable/BitmapDrawable;Landroid/graphics/drawable/BitmapDrawable;)V

    .line 219
    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->leader_list_item_layout:Landroid/widget/RelativeLayout;

    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$2;-><init>(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 234
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v7, v9, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 235
    .local v7, "params":Landroid/widget/RelativeLayout$LayoutParams;
    iput v9, v7, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 236
    iput v8, v7, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 237
    iput v8, v7, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 238
    invoke-virtual {v6, v0, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 239
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTopWalkersListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->myHeaderView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 240
    return-void
.end method

.method private blockManyClick()V
    .locals 4

    .prologue
    .line 930
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mIsBlocked:Z

    .line 931
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 933
    .local v0, "h":Landroid/os/Handler;
    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$keyblockhandler;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$keyblockhandler;-><init>(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 934
    return-void
.end method

.method private cancelLoadingListItemsImages()V
    .locals 5

    .prologue
    .line 621
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " - cancelLoadingListItemsImages"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mImageLoadingTasksList:Ljava/util/List;

    if-eqz v2, :cond_1

    .line 623
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mImageLoadingTasksList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/AsyncTask;

    .line 624
    .local v1, "imageLoadingTask":Landroid/os/AsyncTask;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->cancel(Z)Z

    goto :goto_0

    .line 626
    .end local v1    # "imageLoadingTask":Landroid/os/AsyncTask;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mImageLoadingTasksList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 628
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    return-void
.end method

.method private cancelUpdatingTasks()V
    .locals 3

    .prologue
    .line 631
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - cancelUpdatingTasks"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTopWalkersUpdater:Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTopWalkersUpdater:Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->cancel(Z)Z

    .line 633
    :cond_0
    return-void
.end method

.method private checkMyRanking()Z
    .locals 2

    .prologue
    .line 200
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTopWalkersListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTopWalkersListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->myHeaderView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeHeaderView(Landroid/view/View;)Z

    .line 203
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->myValuesHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    if-eqz v0, :cond_2

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->myValuesHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getRanking()I

    move-result v0

    const/16 v1, 0xa

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->myValuesHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getRanking()I

    move-result v0

    if-gez v0, :cond_2

    .line 205
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->addFootMyRanking()V

    .line 208
    :cond_2
    const/4 v0, 0x1

    return v0
.end method

.method private getExtraStringPeriod(I)Ljava/lang/String;
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 559
    packed-switch p1, :pswitch_data_0

    .line 573
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 561
    :pswitch_0
    const-string v0, "PERIOD_DAILY"

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 562
    const-string v0, "daily"

    goto :goto_0

    .line 565
    :pswitch_1
    const-string v0, "PERIOD_LASTWEEK"

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 566
    const-string v0, "lastWeek"

    goto :goto_0

    .line 569
    :pswitch_2
    const-string v0, "PERIOD_THISWEEK"

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 570
    const-string/jumbo v0, "thisWeek"

    goto :goto_0

    .line 559
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getFirstListItemPosition()I
    .locals 2

    .prologue
    .line 716
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTopWalkersListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTopWalkersListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method private init(Landroid/view/View;)V
    .locals 3
    .param p1, "contentView"    # Landroid/view/View;

    .prologue
    const/4 v0, 0x0

    .line 360
    sput-boolean v0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mIsBlocked:Z

    .line 361
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mIsWalkersUpdated:Z

    .line 362
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->initTopWalkersList(Landroid/view/View;)V

    .line 364
    const v0, 0x7f080b83

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mUpdateTextView:Landroid/widget/TextView;

    .line 365
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mUpdateTextView:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 367
    const v0, 0x7f080b84

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTimeFilter:Landroid/view/View;

    .line 368
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTimeFilter:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 369
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTimeFilter:Landroid/view/View;

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;-><init>(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 415
    const v0, 0x7f080be2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mWalk_my_zero:Landroid/view/View;

    .line 417
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - init"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    return-void
.end method

.method private initSamsungAccountInfo()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 452
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - initSamsungAccountInfo - 0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/loaders/WalkingMateRankingHelper;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 455
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - initSamsungAccountInfo - 1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/loaders/WalkingMateRankingHelper;->getSamsungAccountInfoIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0, v3}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->requestTopWalkersListUpdate(Landroid/content/Intent;Z)V

    .line 462
    :cond_0
    :goto_0
    return-void

    .line 457
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mUpdateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    if-eqz v0, :cond_0

    .line 458
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - initSamsungAccountInfo - 2"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mUpdateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method private initTopWalkersList(Landroid/view/View;)V
    .locals 1
    .param p1, "contentView"    # Landroid/view/View;

    .prologue
    .line 423
    const v0, 0x7f080801

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTopWalkersListView:Landroid/widget/ListView;

    .line 424
    return-void
.end method

.method private isNetworkConnted()Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 779
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const-string v6, "connectivity"

    invoke-virtual {v5, v6}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 780
    .local v0, "manager":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 781
    .local v1, "mobile":Landroid/net/NetworkInfo;
    invoke-virtual {v0, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 783
    .local v2, "wifi":Landroid/net/NetworkInfo;
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    move v3, v4

    .line 786
    :cond_1
    return v3
.end method

.method private loadOfflineData()V
    .locals 3

    .prologue
    .line 352
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->updateWalkersList()V

    .line 353
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - loadOfflineData"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    return-void
.end method

.method private loadWalkersListItemsIconsImages()V
    .locals 7

    .prologue
    .line 577
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " - loadWalkersListItemsIconsImages"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mImageLoadingTasksList:Ljava/util/List;

    .line 579
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->topWalkersList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    .line 580
    .local v0, "holder":Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$7;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$7;-><init>(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;)V

    .line 610
    .local v2, "imageLoadingTask":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/String;Ljava/lang/Void;Landroid/graphics/drawable/Drawable;>;"
    sget-object v3, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getIconUrl()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 611
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mImageLoadingTasksList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 613
    .end local v0    # "holder":Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
    .end local v2    # "imageLoadingTask":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/String;Ljava/lang/Void;Landroid/graphics/drawable/Drawable;>;"
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " - loadWalkersListItemsIconsImages end"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    return-void
.end method

.method private maskingProfileImage(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 13
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v12, 0x0

    const v9, 0x7f0a0a99

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 668
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    .line 670
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 671
    .local v7, "width":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 673
    .local v1, "height":I
    invoke-static {p1, v7, v1, v11}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 674
    .local v2, "mainImage":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f020616

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 675
    .local v3, "mask":Landroid/graphics/Bitmap;
    invoke-static {v3, v7, v1, v11}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 676
    .local v5, "resizedBitmap":Landroid/graphics/Bitmap;
    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v1, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 678
    .local v6, "result":Landroid/graphics/Bitmap;
    invoke-virtual {v0, v6}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 679
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 680
    .local v4, "paint":Landroid/graphics/Paint;
    invoke-virtual {v4, v11}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 681
    const/4 v8, 0x0

    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 683
    invoke-virtual {v0, v2, v10, v10, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 684
    new-instance v8, Landroid/graphics/PorterDuffXfermode;

    sget-object v9, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v8, v9}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 685
    invoke-virtual {v0, v5, v10, v10, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 686
    invoke-virtual {v4, v12}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 688
    return-object v6
.end method

.method private requestTopWalkersListUpdate(Landroid/content/Intent;Z)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "isLatestIntent"    # Z

    .prologue
    .line 475
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "requestTopWalkersListUpdate"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 476
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTopWalkersUpdater:Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;

    if-nez v0, :cond_0

    .line 477
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6;

    invoke-direct {v0, p0, p2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6;-><init>(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;Z)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTopWalkersUpdater:Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;

    .line 531
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTopWalkersUpdater:Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/content/Intent;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 534
    :cond_0
    return-void
.end method

.method private saveTheLatestReceivedTopWalkersListData()V
    .locals 1

    .prologue
    .line 843
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->myValuesHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->topWalkersList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 844
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setEveryoneRankingTotalTopWalkersLisDataAvailable(Z)V

    .line 845
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->topWalkersList:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingStatisticsServerResponseDecoder;->encodeTopWalkersListIntoStringSet(Ljava/util/List;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setTopWalkersList(Ljava/util/Set;)V

    .line 846
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->myValuesHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getRanking()I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setTopWalkersListUserPosition(I)V

    .line 848
    :cond_0
    return-void
.end method

.method private sendRequestForNewUserToken()V
    .locals 3

    .prologue
    .line 299
    invoke-static {}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->getInstance()Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    move-result-object v1

    .line 301
    .local v1, "userTokenManager":Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;
    :try_start_0
    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$4;-><init>(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->sendRequestForNewUserToken(Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 348
    :goto_0
    return-void

    .line 345
    :catch_0
    move-exception v0

    .line 346
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private sendRequestForUserToken()V
    .locals 6

    .prologue
    .line 244
    invoke-static {}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->getInstance()Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    move-result-object v1

    .line 247
    .local v1, "userTokenManager":Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-string v3, "1y90e30264"

    const-string v4, "80E7ECD9D301CB7888C73703639302E5"

    new-instance v5, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$3;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$3;-><init>(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->sendRequestForUserToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;)V

    .line 291
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    const-string v3, "call sendRequestForUserToken"

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 295
    :goto_0
    return-void

    .line 292
    :catch_0
    move-exception v0

    .line 293
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private showLoadingDialog()V
    .locals 4

    .prologue
    .line 867
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 868
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - showLoadingDialog"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 869
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->dismissLoadingDialog()V

    .line 870
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$LoadingDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$LoadingDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->loadingDialog:Lcom/sec/android/app/shealth/walkingmate/utils/Utils$LoadingDialog;

    .line 871
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->loadingDialog:Lcom/sec/android/app/shealth/walkingmate/utils/Utils$LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$LoadingDialog;->show()V

    .line 873
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mUpdateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    if-eqz v0, :cond_0

    .line 874
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mUpdateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    const/4 v1, 0x2

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 876
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->loadingDialog:Lcom/sec/android/app/shealth/walkingmate/utils/Utils$LoadingDialog;

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$9;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$9;-><init>(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$LoadingDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 885
    :cond_1
    return-void
.end method

.method private showUpdateFail()V
    .locals 5

    .prologue
    const v4, 0x7f090b75

    .line 889
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - showUpdateFail"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 890
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v2, 0x7f0900d9

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$11;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$11;-><init>(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$10;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$10;-><init>(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    .line 908
    .local v0, "dialogBuilder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->CONNECTION_ERROR:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    if-ne v1, v2, :cond_0

    .line 909
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090bd3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 917
    :goto_0
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v3, "vskrutie pokazalo: pacient spal"

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 918
    return-void

    .line 910
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->SERVER_ERROR:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    if-ne v1, v2, :cond_1

    .line 911
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    goto :goto_0

    .line 912
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->FAILED_TO_GET_TOKEN:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    if-ne v1, v2, :cond_2

    .line 913
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090ba5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    goto :goto_0

    .line 915
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    goto :goto_0
.end method

.method private updateValues()V
    .locals 3

    .prologue
    .line 432
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - updateValues"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mIsWalkersUpdated:Z

    if-nez v0, :cond_2

    .line 435
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->isNetworkConnted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 436
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->showLoadingDialog()V

    .line 438
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->initSamsungAccountInfo()V

    .line 445
    :cond_1
    :goto_0
    return-void

    .line 441
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mIsWalkersUpdated:Z

    if-eqz v0, :cond_1

    .line 442
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->updateWalkersList()V

    goto :goto_0
.end method

.method private updateWalkersList()V
    .locals 4

    .prologue
    .line 542
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - updateWalkersList"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 544
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTopWalkersListView:Landroid/widget/ListView;

    if-nez v1, :cond_0

    .line 545
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mContent:Landroid/view/View;

    const v2, 0x7f080801

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTopWalkersListView:Landroid/widget/ListView;

    .line 547
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTopWalkersListView:Landroid/widget/ListView;

    if-eqz v1, :cond_1

    .line 548
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTopWalkersListView:Landroid/widget/ListView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 551
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/loaders/WalkingMateRankingHelper;->getSamsungAccountInfoIntent()Landroid/content/Intent;

    move-result-object v0

    .line 552
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_2

    .line 553
    const-string/jumbo v1, "type"

    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->period_select_idx:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getExtraStringPeriod(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 554
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->requestTopWalkersListUpdate(Landroid/content/Intent;Z)V

    .line 556
    :cond_2
    return-void
.end method

.method private visibleZeroRankingLayout()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 192
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTimeFilter:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 193
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mActivity:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->displayLastUpdatedTime(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v0

    .line 194
    .local v0, "str":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mUpdateTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mWalk_my_zero:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 196
    return-void
.end method


# virtual methods
.method public dismissLoadingDialog()V
    .locals 3

    .prologue
    .line 922
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - dismissLoadingDialog"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 923
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->loadingDialog:Lcom/sec/android/app/shealth/walkingmate/utils/Utils$LoadingDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->loadingDialog:Lcom/sec/android/app/shealth/walkingmate/utils/Utils$LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$LoadingDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 924
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->loadingDialog:Lcom/sec/android/app/shealth/walkingmate/utils/Utils$LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$LoadingDialog;->dismiss()V

    .line 925
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->loadingDialog:Lcom/sec/android/app/shealth/walkingmate/utils/Utils$LoadingDialog;

    .line 927
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 721
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onAttach(Landroid/app/Activity;)V

    .line 722
    check-cast p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mActivity:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    .line 724
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mUpdateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    if-eqz v0, :cond_0

    .line 725
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mUpdateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->resume()V

    .line 726
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 746
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 748
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 752
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f03028a

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mContent:Landroid/view/View;

    .line 753
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->getDefaultBitmaps(Landroid/content/Context;)[Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mDefaultListProfileIcon:[Landroid/graphics/drawable/BitmapDrawable;

    .line 755
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - onCreateView"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 756
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mContent:Landroid/view/View;

    const v1, 0x7f080b84

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTV_dropdown_day_seleter:Landroid/widget/TextView;

    .line 757
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTV_dropdown_day_seleter:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mActivity:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTV_dropdown_day_seleter:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0901ec

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 761
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mContent:Landroid/view/View;

    const v1, 0x7f080b82

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mOnNetworkLayout:Landroid/widget/LinearLayout;

    .line 762
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mContent:Landroid/view/View;

    const v1, 0x7f080b85

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mNoNetworkLayout:Landroid/widget/RelativeLayout;

    .line 763
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mContent:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->init(Landroid/view/View;)V

    .line 764
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->isNetworkConnted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 765
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mNoNetworkLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 766
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mOnNetworkLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 767
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->removeShareViaButton()V

    .line 775
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mContent:Landroid/view/View;

    return-object v0

    .line 769
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->updateValues()V

    .line 770
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mNoNetworkLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 771
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mOnNetworkLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 772
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->addShareViaButton()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 802
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mShouldSaveWalkersDataLocally:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->saveTheLatestReceivedTopWalkersListData()V

    .line 803
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->cancelLoadingListItemsImages()V

    .line 804
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->cancelUpdatingTasks()V

    .line 806
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onDestroy()V

    .line 807
    return-void
.end method

.method public onDetach()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 730
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onDetach()V

    .line 731
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mUpdateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    if-eqz v0, :cond_0

    .line 733
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mUpdateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->pause()V

    .line 734
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mUpdateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 735
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mUpdateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    .line 737
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 792
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onPause()V

    .line 793
    return-void
.end method

.method public onPostExecuteRankingAsync(Lorg/json/JSONObject;)V
    .locals 1
    .param p1, "jsonObj"    # Lorg/json/JSONObject;

    .prologue
    .line 859
    :try_start_0
    invoke-static {p1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->getMyRanking(Lorg/json/JSONObject;)Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    .line 860
    invoke-static {p1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->getWorldRanking(Lorg/json/JSONObject;)Ljava/util/ArrayList;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 864
    :goto_0
    return-void

    .line 861
    :catch_0
    move-exception v0

    .line 862
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public onPreExecuteRankingAsync()V
    .locals 0

    .prologue
    .line 853
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 797
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onResume()V

    .line 798
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 811
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 812
    return-void
.end method

.method public setImageMasking(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 5
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 658
    move-object v1, p1

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .line 659
    .local v1, "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 661
    .local v3, "mainImage":Landroid/graphics/Bitmap;
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->maskingProfileImage(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 662
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v2, v4, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 663
    .local v2, "cirCleImg":Landroid/graphics/drawable/Drawable;
    return-object v2
.end method

.method public updateRankingListItem(I)V
    .locals 7
    .param p1, "rankingPosition"    # I

    .prologue
    .line 698
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " - updateRankingListItem - position : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 699
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getFirstListItemPosition()I

    move-result v4

    sub-int v3, p1, v4

    .line 700
    .local v3, "realPosition":I
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTopWalkersListView:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->getChildCount()I

    move-result v0

    .line 701
    .local v0, "childCnt":I
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "- updateRankingListItem rank: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v6, p1, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", real Pos : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", Child Cnt : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    if-ltz v3, :cond_2

    if-ge v3, v0, :cond_2

    .line 704
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTopWalkersListView:Landroid/widget/ListView;

    invoke-virtual {v4, v3}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;

    .line 705
    .local v2, "itemToUpdate":Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;
    if-eqz v2, :cond_0

    .line 706
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->updateIconDrawable()V

    .line 708
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " - updateRankingListItem updateIconDrawable realP: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 713
    .end local v2    # "itemToUpdate":Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;
    :cond_1
    :goto_0
    return-void

    .line 709
    :cond_2
    const/16 v4, 0x8

    if-lt v3, v4, :cond_1

    .line 710
    rem-int/lit8 v1, p1, 0x8

    .line 711
    .local v1, "idx":I
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " - Wait until scroll down idx: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
