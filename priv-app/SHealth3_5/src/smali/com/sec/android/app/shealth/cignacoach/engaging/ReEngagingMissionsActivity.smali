.class public Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;
.super Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;
.source "ReEngagingMissionsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private TAG:Ljava/lang/String;

.field private mAssessment_fifth:Landroid/widget/LinearLayout;

.field private mEngagingFirst:Landroid/widget/RelativeLayout;

.field private mEngagingFourth:Landroid/widget/RelativeLayout;

.field private mEngagingSecond:Landroid/widget/RelativeLayout;

.field private mEngagingThird:Landroid/widget/RelativeLayout;

.field private mGoalId:I

.field private mMissionId:I

.field private mMissionSequenceId:I

.field private state_id:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;-><init>()V

    .line 22
    const-string v0, "ReEngagingMissionsActivity"

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->TAG:Ljava/lang/String;

    .line 25
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->state_id:I

    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 4

    .prologue
    const v2, 0x7f090351

    const v3, 0x7f090350

    .line 144
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->customizeActionBar()V

    .line 147
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(II)V

    .line 148
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f080304

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f09020b

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 151
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 152
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 76
    const/4 v2, 0x0

    .line 78
    .local v2, "reasonMessage":Ljava/lang/String;
    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->mMissionSequenceId:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 81
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->mMissionSequenceId:I

    .line 82
    .local v1, "missionSequenceId":I
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 109
    :goto_0
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->getApplication()Landroid/app/Application;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 110
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 111
    const-string/jumbo v3, "state_id"

    iget v4, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->state_id:I

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 112
    const-string/jumbo v3, "reason"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 113
    const-string v3, "EXTRA_NAME_GOAL_ID"

    iget v4, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->mGoalId:I

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 114
    const-string v3, "EXTRA_NAME_MISSION_ID"

    iget v4, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->mMissionId:I

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 115
    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 116
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->startActivity(Landroid/content/Intent;)V

    .line 117
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->finish()V

    .line 120
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "missionSequenceId":I
    :cond_0
    return-void

    .line 84
    .restart local v1    # "missionSequenceId":I
    :pswitch_1
    const/4 v3, 0x1

    iput v3, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->state_id:I

    .line 85
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v3

    sget-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;->TOO_BUSY:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    invoke-virtual {v3, v1, v4}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->setEngagingReasonCode(ILcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;)Ljava/lang/String;

    move-result-object v2

    .line 86
    goto :goto_0

    .line 88
    :pswitch_2
    const/4 v3, 0x2

    iput v3, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->state_id:I

    .line 89
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v3

    sget-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;->FORGOT:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    invoke-virtual {v3, v1, v4}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->setEngagingReasonCode(ILcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;)Ljava/lang/String;

    move-result-object v2

    .line 90
    goto :goto_0

    .line 92
    :pswitch_3
    const/4 v3, 0x3

    iput v3, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->state_id:I

    .line 93
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v3

    sget-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;->TOO_HARD:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    invoke-virtual {v3, v1, v4}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->setEngagingReasonCode(ILcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;)Ljava/lang/String;

    move-result-object v2

    .line 94
    goto :goto_0

    .line 96
    :pswitch_4
    const/4 v3, 0x4

    iput v3, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->state_id:I

    .line 97
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v3

    sget-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;->TOO_EASY:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    invoke-virtual {v3, v1, v4}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->setEngagingReasonCode(ILcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;)Ljava/lang/String;

    move-result-object v2

    .line 98
    goto :goto_0

    .line 100
    :pswitch_5
    const/4 v3, 0x5

    iput v3, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->state_id:I

    .line 101
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v3

    sget-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;->NONE_OF_THESE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    invoke-virtual {v3, v1, v4}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->setEngagingReasonCode(ILcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;)Ljava/lang/String;

    move-result-object v2

    .line 102
    goto :goto_0

    .line 82
    :pswitch_data_0
    .packed-switch 0x7f080228
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, -0x1

    .line 32
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 34
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 35
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_0

    .line 36
    const-string v2, "EXTRA_NAME_MISSION_SEQUENCE_ID"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->mMissionSequenceId:I

    .line 37
    const-string v2, "EXTRA_NAME_GOAL_ID"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->mGoalId:I

    .line 38
    const-string v2, "EXTRA_NAME_MISSION_ID"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->mMissionId:I

    .line 40
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ReEngagingMissionsActivity mMissionSequenceId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->mMissionSequenceId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "mGoalId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->mGoalId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "mMissionId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->mMissionId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    :cond_0
    const v2, 0x7f030073

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->setContentView(I)V

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->customizeActionBar()V

    .line 54
    const v2, 0x7f080226

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 55
    .local v0, "engageMsg":Landroid/widget/TextView;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getEngagingMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    const v2, 0x7f080228

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->mEngagingFirst:Landroid/widget/RelativeLayout;

    .line 58
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->mEngagingFirst:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    const v2, 0x7f08022a

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->mEngagingSecond:Landroid/widget/RelativeLayout;

    .line 61
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->mEngagingSecond:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    const v2, 0x7f08022c

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->mEngagingThird:Landroid/widget/RelativeLayout;

    .line 64
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->mEngagingThird:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    const v2, 0x7f08022e

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->mEngagingFourth:Landroid/widget/RelativeLayout;

    .line 67
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->mEngagingFourth:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    const v2, 0x7f080230

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->mAssessment_fifth:Landroid/widget/LinearLayout;

    .line 70
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->mAssessment_fifth:Landroid/widget/LinearLayout;

    invoke-virtual {v2, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 132
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 133
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsActivity;->setIntent(Landroid/content/Intent;)V

    .line 134
    return-void
.end method

.method protected onRestart()V
    .locals 0

    .prologue
    .line 139
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onRestart()V

    .line 140
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 125
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onResume()V

    .line 127
    return-void
.end method
