.class public Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
.super Ljava/lang/Object;
.source "HeartrateDatabaseHelper.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field public static final TAG_DEPRECATED:Ljava/lang/String; = "tag"

.field public static final TAG_INDEX_DEPRECATED:Ljava/lang/String; = "tag_index"

.field private static instance:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;


# instance fields
.field private delHeartRate:I

.field private delHeartRateData:I

.field private delTag:I

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const-class v0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->delHeartRate:I

    .line 57
    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->delHeartRateData:I

    .line 58
    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->delTag:I

    .line 74
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    .line 76
    return-void
.end method

.method private checkData(J[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;
    .locals 13
    .param p1, "selectedTime"    # J
    .param p3, "projection"    # [Ljava/lang/String;
    .param p4, "selection"    # Ljava/lang/String;
    .param p5, "selectionArgs"    # [Ljava/lang/String;
    .param p6, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 410
    const/4 v7, 0x0

    .line 411
    .local v7, "ret":Ljava/util/Date;
    const/4 v6, 0x0

    .line 413
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRate;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 414
    if-eqz v6, :cond_1

    .line 415
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    .line 417
    .local v11, "todayTime":J
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_4

    .line 418
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 419
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 420
    .local v9, "searchTime":J
    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v0

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v0

    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    .line 421
    new-instance v8, Ljava/util/Date;

    invoke-direct {v8, v11, v12}, Ljava/util/Date;-><init>(J)V

    .end local v7    # "ret":Ljava/util/Date;
    .local v8, "ret":Ljava/util/Date;
    move-object v7, v8

    .line 430
    .end local v8    # "ret":Ljava/util/Date;
    .end local v9    # "searchTime":J
    .restart local v7    # "ret":Ljava/util/Date;
    :cond_0
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 433
    .end local v11    # "todayTime":J
    :cond_1
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 434
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 438
    :cond_2
    return-object v7

    .line 423
    .restart local v9    # "searchTime":J
    .restart local v11    # "todayTime":J
    :cond_3
    :try_start_1
    new-instance v8, Ljava/util/Date;

    invoke-direct {v8, v9, v10}, Ljava/util/Date;-><init>(J)V

    .end local v7    # "ret":Ljava/util/Date;
    .restart local v8    # "ret":Ljava/util/Date;
    move-object v7, v8

    .end local v8    # "ret":Ljava/util/Date;
    .restart local v7    # "ret":Ljava/util/Date;
    goto :goto_0

    .line 426
    .end local v9    # "searchTime":J
    :cond_4
    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v0

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 427
    new-instance v8, Ljava/util/Date;

    invoke-direct {v8, v11, v12}, Ljava/util/Date;-><init>(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v7    # "ret":Ljava/util/Date;
    .restart local v8    # "ret":Ljava/util/Date;
    move-object v7, v8

    .end local v8    # "ret":Ljava/util/Date;
    .restart local v7    # "ret":Ljava/util/Date;
    goto :goto_0

    .line 433
    .end local v11    # "todayTime":J
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_5

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_5

    .line 434
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
.end method

.method private getChildQuery(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 11
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 219
    const-string/jumbo v0, "start_time>? AND start_time<?"

    .line 220
    .local v0, "clause":Ljava/lang/String;
    const-string/jumbo v6, "start_time"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 221
    .local v4, "time":J
    new-array v2, v10, [Ljava/lang/String;

    invoke-static {v9, v4, v5}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getMonthToStringByMillisecond(ZJ)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v8

    invoke-static {v8, v4, v5}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getMonthToStringByMillisecond(ZJ)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v9

    .line 222
    .local v2, "selectionArgs":[Ljava/lang/String;
    sget v6, Lcom/sec/android/app/shealth/heartrate/common/HeartrateConstants;->SELECTED_FILTER_INDEX:I

    if-nez v6, :cond_0

    .line 223
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "0==0"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 226
    :cond_0
    const-string v6, "AND"

    invoke-virtual {v0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    aget-object v3, v6, v10

    .line 228
    .local v3, "selectionClause":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SELECT A.[_id], A.[user_device__id], A.[start_time], A.[comment], B.[value], B.[sample_time], B.[value] as AvgHeartRate, F.AvgMonth, U.[custom_name]  FROM heart_rate A, user_device U, heart_rate_data B  LEFT OUTER JOIN (SELECT E.[_id], AVG(E.[value]) AS AvgMonth FROM (SELECT * FROM heart_rate_data ORDER BY  heart_rate_data.[sample_time]) E GROUP BY STRFTIME(\"%d-%m-%Y\", (E.[sample_time]/1000),  \'unixepoch\', \'localtime\')) F ON B.[_id] = F.[_id] WHERE A.[_id] = B.[heart_rate__id] AND A.[start_time] >= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v2, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND A.["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "start_time"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] <= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v2, v9

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND A.user_device__id = U._id "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " GROUP BY A.["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_id"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] ORDER BY A.["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "start_time"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " DESC"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 264
    .local v1, "query":Ljava/lang/String;
    return-object v1
.end method

.method private getChildQueryByTag(Landroid/database/Cursor;I)Ljava/lang/String;
    .locals 11
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "tagId"    # I

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 270
    const-string/jumbo v0, "start_time>? AND start_time<?"

    .line 271
    .local v0, "clause":Ljava/lang/String;
    const-string/jumbo v6, "start_time"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 272
    .local v4, "time":J
    new-array v2, v10, [Ljava/lang/String;

    invoke-static {v9, v4, v5}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getMonthToStringByMillisecond(ZJ)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v8

    invoke-static {v8, v4, v5}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getMonthToStringByMillisecond(ZJ)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v9

    .line 273
    .local v2, "selectionArgs":[Ljava/lang/String;
    sget v6, Lcom/sec/android/app/shealth/heartrate/common/HeartrateConstants;->SELECTED_FILTER_INDEX:I

    if-nez v6, :cond_0

    .line 274
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "0==0"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 277
    :cond_0
    const-string v6, "AND"

    invoke-virtual {v0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    aget-object v3, v6, v10

    .line 279
    .local v3, "selectionClause":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SELECT A.[_id], A.[user_device__id], A.[start_time], A.[comment], TG.[tag_index], B.[value], B.[sample_time], D.AvgHeartRate, F.AvgMonth, U.[custom_name]  FROM heart_rate A, user_device U,(select * from tag order by update_time desc) TG, heart_rate_data B LEFT OUTER JOIN (SELECT C.[_id], C.[heart_rate__id], AVG(C.[value]) AS AvgHeartRate FROM heart_rate_data C GROUP BY C.[heart_rate__id]) D ON B.[_id] = D.[_id] LEFT OUTER JOIN ( SELECT E.[_id], AVG(E.[value]) AS AvgMonth FROM (SELECT * FROM heart_rate_data ORDER BY  heart_rate_data.[sample_time]) E INNER JOIN (select * from tag order by update_time desc) TG1 ON  E.[heart_rate__id] = TG1.[table_record_id] where TG1.[tag_index] ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " GROUP BY STRFTIME( "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\"%d-%m-%Y\", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " (E.[sample_time]/1000),  \'unixepoch\', \'localtime\'))"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "F ON B.["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_id"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] = F.["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_id"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] WHERE A.["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_id"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] = B.["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "heart_rate__id"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] AND A.["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_id"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] = TG.["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "table_record_id"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]  AND A.["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "start_time"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] >= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v2, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND A.["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "start_time"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] <= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v2, v9

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND A.user_device__id = U._id "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "AND TG.[tag_index] ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " GROUP BY A.["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_id"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] ORDER BY A.["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "start_time"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " DESC"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 325
    .local v1, "query":Ljava/lang/String;
    return-object v1
.end method

.method private getHeartrateData(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 79
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;-><init>()V

    .line 80
    .local v0, "heartrateData":Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;
    if-eqz p1, :cond_0

    .line 81
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 82
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-eqz v1, :cond_0

    .line 83
    const-string v1, "_id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->setId(J)V

    .line 84
    const-string/jumbo v1, "user_device__id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->setUserDeviceId(Ljava/lang/String;)V

    .line 85
    const-string/jumbo v1, "value"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->setMeanHeartRate(F)V

    .line 86
    const-string/jumbo v1, "start_time"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->setStartTime(J)V

    .line 87
    const-string v1, "comment"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->setComment(Ljava/lang/String;)V

    .line 88
    const-string v1, "AvgHeartRate"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->setAverage(F)V

    .line 89
    const-string v1, "custom_name"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->setAccessoryName(Ljava/lang/String;)V

    .line 92
    :cond_0
    return-object v0
.end method

.method private getHeartrateDatas(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 5
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 97
    .local v0, "hearteRateDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;>;"
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 98
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_1

    .line 99
    new-instance v1, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;-><init>()V

    .line 100
    .local v1, "heartrateData":Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;
    const-string v2, "_id"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->setId(J)V

    .line 101
    const-string/jumbo v2, "start_time"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->setStartTime(J)V

    .line 102
    const-string/jumbo v2, "value"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->setMeanHeartRate(F)V

    .line 103
    const-string/jumbo v2, "sample_time"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->setSampleTime(J)V

    .line 104
    const-string v2, "AvgHeartRate"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->setAverage(F)V

    .line 106
    const-string v2, "MinHeartRate"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v4, :cond_0

    const-string v2, "MaxHeartRate"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v4, :cond_0

    .line 107
    const-string v2, "MaxHeartRate"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->setMaxData(F)V

    .line 108
    const-string v2, "MinHeartRate"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->setMinData(F)V

    .line 110
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 111
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 114
    .end local v1    # "heartrateData":Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;
    :cond_1
    return-object v0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 67
    const-class v1, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->instance:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    if-nez v0, :cond_0

    .line 68
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->instance:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    .line 70
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->instance:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 67
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getShareData(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 7
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const v6, 0x7f090d14

    .line 532
    const-string v2, ""

    .line 533
    .local v2, "shareData":Ljava/lang/String;
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 534
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_0

    .line 535
    const-string/jumbo v3, "sample_time"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 536
    .local v0, "sampleTime":J
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    const v5, 0x7f090022

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "AvgHeartRate"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getFloat(I)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    const v5, 0x7f0900d2

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    const v5, 0x7f090135

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getDateTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 538
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    goto/16 :goto_0

    .line 540
    .end local v0    # "sampleTime":J
    :cond_0
    return-object v2
.end method

.method private returnStringQuery(J)Ljava/lang/String;
    .locals 3
    .param p1, "timeMills"    # J

    .prologue
    .line 773
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SELECT A.*, B.[value], B.[sample_time], D.AvgHeartRate FROM heart_rate A, heart_rate_data B , (SELECT C.[_id], C.[heart_rate__id], AVG(C.[value]) AS AvgHeartRate from heart_rate_data C GROUP BY C.[heart_rate__id]) D WHERE B.[_id] = D.[_id] AND A.[_id] = B.[heart_rate__id] AND B.[sample_time] <= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " GROUP BY A.["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]  ORDER BY  B.["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "sample_time"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 794
    .local v0, "query":Ljava/lang/String;
    return-object v0
.end method


# virtual methods
.method public addTagToHeartRateLog(JI)V
    .locals 6
    .param p1, "heartRateRecordId"    # J
    .param p3, "tagId"    # I

    .prologue
    .line 904
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 905
    .local v2, "values":Landroid/content/ContentValues;
    const-string/jumbo v3, "table_name_type"

    const v4, 0x55733

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 906
    const-string/jumbo v3, "table_record_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 907
    const-string/jumbo v3, "tag_index"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 908
    const-string v3, "input_source_type"

    const v4, 0x1adb2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 910
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$Tag;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    .line 911
    .local v1, "uri":Landroid/net/Uri;
    sget-object v3, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Tag(id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") is added to HR record(id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_0

    .line 916
    .end local v1    # "uri":Landroid/net/Uri;
    :goto_0
    return-void

    .line 912
    :catch_0
    move-exception v0

    .line 913
    .local v0, "e":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->printStackTrace()V

    goto :goto_0
.end method

.method public createCustomTag(ILjava/lang/String;)Landroid/net/Uri;
    .locals 6
    .param p1, "tagIconId"    # I
    .param p2, "tagName"    # Ljava/lang/String;

    .prologue
    .line 806
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 807
    .local v2, "values":Landroid/content/ContentValues;
    const-string/jumbo v3, "table_name_type"

    const v4, 0x55733

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 808
    const-string/jumbo v3, "tag_index"

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->generateCustomTagId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 809
    const-string v3, "custom_icon_index"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 810
    const-string v3, "custom_tag_name"

    invoke-virtual {v2, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 811
    const-string v3, "input_source_type"

    const v4, 0x1adb1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 812
    const/4 v1, 0x0

    .line 814
    .local v1, "uri":Landroid/net/Uri;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$Tag;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    .line 815
    if-eqz v1, :cond_0

    .line 816
    sget-object v3, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "New custom tag(iconId: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", name: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") is added - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_0

    .line 822
    :cond_0
    :goto_0
    return-object v1

    .line 818
    :catch_0
    move-exception v0

    .line 819
    .local v0, "e":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->printStackTrace()V

    goto :goto_0
.end method

.method public deleteAllData()Z
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v7, 0x0

    const/4 v4, 0x0

    .line 333
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 334
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v5, "heartrate_warning_checked"

    invoke-interface {v0, v5, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 335
    const-string v5, "first_r_tag"

    invoke-interface {v0, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 336
    const-string/jumbo v5, "second_r_tag"

    invoke-interface {v0, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 337
    const-string/jumbo v5, "third_r_tag"

    invoke-interface {v0, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 338
    sget-object v5, Lcom/sec/android/app/shealth/heartrate/common/HeartrateConstants;->ARE_DEFAULT_CUSTOM_TAGS_ADDED:Ljava/lang/String;

    invoke-interface {v0, v5, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 339
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 340
    new-array v2, v3, [Ljava/lang/String;

    const v5, 0x55733

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    .line 341
    .local v2, "selectionArgs":[Ljava/lang/String;
    const-string/jumbo v1, "table_name_type=?"

    .line 342
    .local v1, "selection":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRate;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6, v7, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->delHeartRate:I

    .line 343
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRateData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6, v7, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->delHeartRateData:I

    .line 344
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/health/content/ShealthContract$Tag;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6, v1, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->delTag:I

    .line 346
    iget v5, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->delHeartRate:I

    iget v6, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->delHeartRateData:I

    add-int/2addr v5, v6

    iget v6, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->delTag:I

    add-int/2addr v5, v6

    if-lez v5, :cond_0

    :goto_0
    return v3

    :cond_0
    move v3, v4

    goto :goto_0
.end method

.method public deleteCustomTag(Ljava/lang/String;)V
    .locals 6
    .param p1, "tagName"    # Ljava/lang/String;

    .prologue
    .line 893
    const-string/jumbo v1, "table_record_id IS  NULL  AND custom_tag_name=?"

    .line 897
    .local v1, "selection":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v2, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    .line 899
    .local v2, "selectionArgs":[Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$Tag;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v1, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 900
    .local v0, "count":I
    sget-object v3, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Custom tag(name"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") is deleted - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 901
    return-void
.end method

.method public deleteDailyDataByTime(J)Z
    .locals 7
    .param p1, "timeMillis"    # J

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 360
    const-string/jumbo v1, "start_time>? AND start_time<?"

    .line 361
    .local v1, "selection":Ljava/lang/String;
    const-string/jumbo v0, "sample_time>? AND sample_time<?"

    .line 362
    .local v0, "dataSelection":Ljava/lang/String;
    const/4 v5, 0x2

    new-array v2, v5, [Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getEndDayToMillis(J)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v3

    .line 364
    .local v2, "selectionArgs":[Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRate;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6, v1, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->delHeartRate:I

    .line 365
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRateData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6, v0, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->delHeartRateData:I

    .line 366
    iget v5, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->delHeartRate:I

    iget v6, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->delHeartRateData:I

    add-int/2addr v5, v6

    if-lez v5, :cond_0

    :goto_0
    return v3

    :cond_0
    move v3, v4

    goto :goto_0
.end method

.method public deleteRowById(Ljava/lang/String;)Z
    .locals 7
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 350
    const-string v1, "_id=?"

    .line 351
    .local v1, "selection":Ljava/lang/String;
    const-string v0, "heart_rate__id=?"

    .line 352
    .local v0, "dataSelection":Ljava/lang/String;
    new-array v2, v3, [Ljava/lang/String;

    aput-object p1, v2, v4

    .line 354
    .local v2, "selectionArgs":[Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRate;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6, v1, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->delHeartRate:I

    .line 355
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRateData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6, v0, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->delHeartRateData:I

    .line 356
    iget v5, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->delHeartRate:I

    iget v6, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->delHeartRateData:I

    add-int/2addr v5, v6

    if-lez v5, :cond_0

    :goto_0
    return v3

    :cond_0
    move v3, v4

    goto :goto_0
.end method

.method public executeGroupByTagQuery(I)Landroid/database/Cursor;
    .locals 6
    .param p1, "tagId"    # I

    .prologue
    const/4 v2, 0x0

    .line 150
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT AVG(E.[value]) AS AvgMonth, MIN(E.[value]) AS MinMonth, MAX(E.[value]) AS MaxMonth FROM heart_rate_data E ,(select * from tag order by update_time desc) TG, heart_rate A where  A.[_id] =E.[heart_rate__id]and  A.[_id] =TG.[table_record_id] and TG.[tag_index] = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 161
    .local v3, "query":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public executeQuery()Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 118
    const-string v3, "SELECT A.[_id], A.[user_device__id], A.[start_time], A.[comment], B.[value] , F.AvgMonth FROM heart_rate A, heart_rate_data B ,(SELECT E.[_id], AVG(E.[value]) AS AvgMonth FROM heart_rate_data E GROUP BY STRFTIME(\"%m-%Y\", (E.[sample_time]/1000),  \'unixepoch\', \'localtime\')) F WHERE B.[_id] = F.[_id] AND A.[_id] = B.[heart_rate__id] GROUP BY STRFTIME(\"%m-%Y\", (A.[start_time]/1000),  \'unixepoch\', \'localtime\') ORDER BY (A.[start_time]/1000) DESC"

    .line 146
    .local v3, "query":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public executeTagQuery(I)Landroid/database/Cursor;
    .locals 7
    .param p1, "tagId"    # I

    .prologue
    const/4 v2, 0x0

    .line 175
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT E.[_id], AVG(E.value) AS AvgMonth FROM heart_rate_data E, (SELECT HR.[_id] FROM heart_rate HR JOIN tag TG ON (HR._id = TG.table_record_id) AND (TG.tag_index = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 185
    .local v6, "innerQuery":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") H WHERE  H._id = E.heart_rate__id GROUP BY STRFTIME(\"%m-%Y\", (E.sample_time/1000),  \'unixepoch\', \'localtime\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 191
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT A.[_id], A.[user_device__id], A.[start_time], A.[comment], B.[value], F.AvgMonth FROM heart_rate A, heart_rate_data B, ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") F"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE A.["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] = B.["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "heart_rate__id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND B.["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] = F._id "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " GROUP BY STRFTIME("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\"%m-%Y\", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "(A.["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "start_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]/1000), "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " \'unixepoch\', \'localtime\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ORDER BY (A.["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "start_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]/1000)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " DESC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 211
    .local v3, "query":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public generateCustomTagId()I
    .locals 8

    .prologue
    .line 1033
    const-string v3, "SELECT max(tag_index) AS latest_custom_created_id FROM tag WHERE tag_index>=30000 AND table_name_type=350003"

    .line 1034
    .local v3, "query":Ljava/lang/String;
    const/4 v7, 0x0

    .line 1036
    .local v7, "newIndexId":I
    const/4 v6, 0x0

    .line 1038
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1039
    if-eqz v6, :cond_1

    .line 1040
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1041
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1042
    const-string v0, "latest_custom_created_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 1043
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Max custom index : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1045
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1048
    :cond_1
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1049
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1052
    :cond_2
    const/16 v0, 0x7530

    if-ge v7, v0, :cond_3

    .line 1053
    const/16 v7, 0x7530

    .line 1054
    :cond_3
    add-int/lit8 v7, v7, 0x1

    .line 1055
    return v7

    .line 1048
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_4

    .line 1049
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public getCalendarDatas(JJ)Ljava/util/ArrayList;
    .locals 11
    .param p1, "startMonthTime"    # J
    .param p3, "endMonthTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 721
    new-array v2, v10, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v1

    const-string/jumbo v0, "start_time"

    aput-object v0, v2, v5

    .line 722
    .local v2, "projection":[Ljava/lang/String;
    const-string/jumbo v3, "start_time>? AND start_time<?"

    .line 723
    .local v3, "selection":Ljava/lang/String;
    new-array v4, v10, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 724
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 725
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 727
    .local v8, "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;>;"
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRate;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 728
    if-eqz v6, :cond_2

    .line 729
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 730
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 731
    .end local v8    # "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;>;"
    .local v9, "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;>;"
    :goto_0
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_1

    .line 732
    new-instance v7, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-direct {v7}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;-><init>()V

    .line 733
    .local v7, "heartrateData":Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v7, v0, v1}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->setId(J)V

    .line 734
    const-string/jumbo v0, "start_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v7, v0, v1}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->setStartTime(J)V

    .line 736
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 737
    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 742
    .end local v7    # "heartrateData":Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;
    :catchall_0
    move-exception v0

    move-object v8, v9

    .end local v9    # "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;>;"
    .restart local v8    # "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;>;"
    :goto_1
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 743
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 739
    .end local v8    # "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;>;"
    .restart local v9    # "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;>;"
    :cond_1
    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v8, v9

    .line 742
    .end local v9    # "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;>;"
    .restart local v8    # "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;>;"
    :cond_2
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_3

    .line 743
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 747
    :cond_3
    return-object v8

    .line 742
    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public getChildrenCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 6
    .param p1, "groupCursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v2, 0x0

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getChildQuery(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getChildrenCursorByTag(Landroid/database/Cursor;I)Landroid/database/Cursor;
    .locals 6
    .param p1, "groupCursor"    # Landroid/database/Cursor;
    .param p2, "tag"    # I

    .prologue
    const/4 v2, 0x0

    .line 798
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getChildQueryByTag(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v3

    .line 800
    .local v3, "query":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getCustomTags()Ljava/util/ArrayList;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v12, 0x0

    .line 826
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 828
    .local v7, "customTags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;>;"
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const-string/jumbo v0, "tag_index"

    aput-object v0, v2, v12

    const-string v0, "custom_icon_index"

    aput-object v0, v2, v5

    const/4 v0, 0x2

    const-string v1, "custom_tag_name"

    aput-object v1, v2, v0

    .line 829
    .local v2, "projection":[Ljava/lang/String;
    const-string/jumbo v3, "table_record_id IS  NULL  AND input_source_type=?"

    .line 832
    .local v3, "selection":Ljava/lang/String;
    new-array v4, v5, [Ljava/lang/String;

    const v0, 0x1adb1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v12

    .line 834
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 836
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Tag;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 837
    if-eqz v6, :cond_2

    .line 838
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 839
    const-string/jumbo v0, "tag_index"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 840
    .local v10, "tagId":I
    const-string v0, "custom_icon_index"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 841
    .local v9, "tagIconId":I
    const-string v0, "custom_tag_name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 842
    .local v11, "tagName":Ljava/lang/String;
    invoke-static {v10, v9, v11}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getTag(IILjava/lang/String;)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v8

    .line 843
    .local v8, "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 848
    .end local v8    # "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    .end local v9    # "tagIconId":I
    .end local v10    # "tagId":I
    .end local v11    # "tagName":Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 849
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 845
    :cond_1
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 848
    :cond_2
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_3

    .line 849
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 853
    :cond_3
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Total "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz v7, :cond_4

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_1
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "custom tags are retrieved."

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 855
    return-object v7

    :cond_4
    move v0, v12

    .line 853
    goto :goto_1
.end method

.method public getDataById(Ljava/lang/String;)Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;
    .locals 8
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 443
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT A.*, B.[value], B.[sample_time], D.AvgHeartRate, U.[custom_name] FROM heart_rate A, user_device U, heart_rate_data B , (SELECT C.[_id], C.[heart_rate__id], AVG(C.[value]) AS AvgHeartRate FROM heart_rate_data C GROUP BY C.[heart_rate__id]) D WHERE B.[_id] = D.[_id] AND D.AvgHeartRate != 0 AND A.[_id] = B.[heart_rate__id] AND A.[_id] = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND A.user_device__id = U._id "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 464
    .local v3, "query":Ljava/lang/String;
    const/4 v7, 0x0

    .line 465
    .local v7, "ret":Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;
    const/4 v6, 0x0

    .line 467
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 468
    if-eqz v6, :cond_0

    .line 469
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 470
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getHeartrateData(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    move-result-object v7

    .line 471
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 474
    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 475
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 478
    :cond_1
    return-object v7

    .line 474
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_2

    .line 475
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public getDataCount()I
    .locals 8

    .prologue
    .line 665
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    .line 666
    .local v2, "projection":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 667
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 669
    .local v7, "ret":I
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRate;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 670
    if-eqz v6, :cond_0

    .line 671
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v7

    .line 672
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 675
    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 676
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 680
    :cond_1
    return v7

    .line 675
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_2

    .line 676
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public getDataCountToShowTag()I
    .locals 9

    .prologue
    .line 685
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    .line 686
    .local v2, "projection":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 687
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 689
    .local v7, "ret":I
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRate;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 690
    if-eqz v6, :cond_1

    .line 691
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v7

    .line 692
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 693
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getDistinctTagsToShow()Landroid/database/Cursor;

    move-result-object v6

    .line 694
    if-lez v7, :cond_1

    if-eqz v6, :cond_1

    .line 695
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 696
    const/4 v7, 0x0

    .line 697
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_0

    .line 698
    const-string/jumbo v0, "tag_index"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 699
    .local v8, "tagIndex":I
    const/16 v0, 0x4e20

    if-eq v8, v0, :cond_3

    .line 700
    const/4 v7, 0x1

    .line 705
    .end local v8    # "tagIndex":I
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 710
    :cond_1
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 711
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 717
    :cond_2
    return v7

    .line 703
    .restart local v8    # "tagIndex":I
    :cond_3
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 710
    .end local v8    # "tagIndex":I
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_4

    .line 711
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public getDistinctTags()Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 165
    const-string/jumbo v3, "select distinct tag_index from tag"

    .line 166
    .local v3, "query":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getDistinctTagsToShow()Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 170
    const-string/jumbo v3, "select distinct TG.[tag_index] from tag TG,heart_rate A where A.[_id] =TG.[table_record_id]"

    .line 171
    .local v3, "query":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getHearteRateGraphDatasByType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/util/ArrayList;
    .locals 9
    .param p1, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 594
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_2

    const-string v8, "STRFTIME(\"%d-%m-%Y\", C.[sample_time]/1000, \'unixepoch\', \'localtime\')"

    .line 596
    .local v8, "timeCondition":Ljava/lang/String;
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT A.[_id], A.[start_time], A.[comment], D.[value], D.[sample_time], B.AvgHeartRate, B.MinHeartRate, B.MaxHeartRate FROM heart_rate A, heart_rate_data D LEFT OUTER JOIN (SELECT C.[_id], C.[value] , C.[sample_time], AVG(C.[value]) AS AvgHeartRate, MIN(C.[value]) AS MinHeartRate, MAX(C.[value]) AS MaxHeartRate FROM heart_rate_data AS C GROUP BY "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") AS B ON D.["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] = B.["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] WHERE A.["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] = D.["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "heart_rate__id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "B."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "AvgHeartRate"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " != 0 GROUP BY  A.["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] ORDER BY A.["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "start_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ASC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 632
    .local v3, "query":Ljava/lang/String;
    const/4 v6, 0x0

    .line 633
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 635
    .local v7, "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;>;"
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 636
    if-eqz v6, :cond_0

    .line 637
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getHeartrateDatas(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v7

    .line 638
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 641
    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 642
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 646
    :cond_1
    return-object v7

    .line 594
    .end local v3    # "query":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;>;"
    .end local v8    # "timeCondition":Ljava/lang/String;
    :cond_2
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_3

    const-string v8, "STRFTIME(\"%m-%Y\", C.[sample_time]/1000, \'unixepoch\', \'localtime\')"

    goto/16 :goto_0

    :cond_3
    const-string v8, "(C.[sample_time]/(1000 * 60 *5))"

    goto/16 :goto_0

    .line 641
    .restart local v3    # "query":Ljava/lang/String;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v7    # "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;>;"
    .restart local v8    # "timeCondition":Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_4

    .line 642
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public getHearteRateGraphDatasHourType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/util/ArrayList;
    .locals 9
    .param p1, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 546
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_2

    const-string v8, "STRFTIME(\"%d-%m-%Y\", C.[sample_time]/1000, \'unixepoch\', \'localtime\')"

    .line 548
    .local v8, "timeCondition":Ljava/lang/String;
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT A.[_id], A.[start_time], A.[comment], D.[value], D.[sample_time], B.AvgHeartRate FROM heart_rate A, heart_rate_data D LEFT OUTER JOIN (SELECT C.[_id], C.[value] , C.[sample_time], AVG(C.[value]) AS AvgHeartRate FROM heart_rate_data AS C GROUP BY "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") AS B ON D.["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] = B.["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] WHERE A.["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] = D.["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "heart_rate__id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "B."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "AvgHeartRate"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " != 0 GROUP BY  A.["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] ORDER BY A.["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "start_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ASC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 574
    .local v3, "query":Ljava/lang/String;
    const/4 v6, 0x0

    .line 575
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 577
    .local v7, "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;>;"
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 578
    if-eqz v6, :cond_0

    .line 579
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getHeartrateDatas(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v7

    .line 580
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 583
    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 584
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 588
    :cond_1
    return-object v7

    .line 546
    .end local v3    # "query":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;>;"
    .end local v8    # "timeCondition":Ljava/lang/String;
    :cond_2
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_3

    const-string v8, "STRFTIME(\"%m-%Y\", C.[sample_time]/1000, \'unixepoch\', \'localtime\')"

    goto/16 :goto_0

    :cond_3
    const-string v8, "(C.[sample_time]/(1000 * 60 *5))"

    goto/16 :goto_0

    .line 583
    .restart local v3    # "query":Ljava/lang/String;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v7    # "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;>;"
    .restart local v8    # "timeCondition":Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_4

    .line 584
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public getLastTwoDataByTime(J)Ljava/util/ArrayList;
    .locals 8
    .param p1, "timeMills"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 752
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->returnStringQuery(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " DESC LIMIT 2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 754
    .local v3, "query":Ljava/lang/String;
    const/4 v6, 0x0

    .line 755
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 757
    .local v7, "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;>;"
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 758
    if-eqz v6, :cond_0

    .line 759
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getHeartrateDatas(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v7

    .line 760
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 763
    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 764
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 768
    :cond_1
    return-object v7

    .line 763
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_2

    .line 764
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public getNextDateOfData(J)Ljava/util/Date;
    .locals 8
    .param p1, "selectedTime"    # J

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 398
    new-array v3, v7, [Ljava/lang/String;

    const-string/jumbo v0, "start_time"

    aput-object v0, v3, v2

    .line 399
    .local v3, "projection":[Ljava/lang/String;
    const-string/jumbo v4, "start_time>? AND start_time<?"

    .line 400
    .local v4, "selection":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v5, v0, [Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getEndDayToMillis(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getEndDayToMillis(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    .line 401
    .local v5, "selectionArgs":[Ljava/lang/String;
    const-string/jumbo v6, "start_time ASC LIMIT 1"

    .local v6, "sortOrder":Ljava/lang/String;
    move-object v0, p0

    move-wide v1, p1

    .line 403
    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->checkData(J[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getPreviousDateOfData(J)Ljava/util/Date;
    .locals 7
    .param p1, "selectedTime"    # J

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 388
    new-array v3, v1, [Ljava/lang/String;

    const-string/jumbo v0, "start_time"

    aput-object v0, v3, v2

    .line 389
    .local v3, "projection":[Ljava/lang/String;
    const-string/jumbo v4, "start_time<?"

    .line 390
    .local v4, "selection":Ljava/lang/String;
    new-array v5, v1, [Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v2

    .line 391
    .local v5, "selectionArgs":[Ljava/lang/String;
    const-string/jumbo v6, "start_time DESC LIMIT 1"

    .local v6, "sortOrder":Ljava/lang/String;
    move-object v0, p0

    move-wide v1, p1

    .line 393
    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->checkData(J[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getShareData(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 482
    .local p1, "checkedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    const v2, 0x7f090022

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 483
    .local v8, "ret":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_3

    .line 484
    const-string v9, ""

    .line 485
    .local v9, "selection":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v10

    .line 486
    .local v10, "size":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-ge v7, v10, :cond_1

    .line 487
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "_"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    aget-object v0, v0, v2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    add-int/lit8 v0, v10, -0x1

    if-ne v7, v0, :cond_0

    const-string v0, ""

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 486
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 487
    :cond_0
    const-string v0, ","

    goto :goto_1

    .line 490
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT A.*, B.[value], B.[sample_time], D.AvgHeartRate FROM heart_rate A, heart_rate_data B LEFT OUTER JOIN (SELECT C.[_id], C.[heart_rate__id], AVG(C.[value]) AS AvgHeartRate FROM heart_rate_data C GROUP BY C.[heart_rate__id]) D ON B.[_id] = D.[_id] WHERE A.[_id] = B.[heart_rate__id] AND B.[heart_rate__id] IN ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") GROUP BY A.["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] ORDER BY B.["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "sample_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " DESC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 513
    .local v3, "query":Ljava/lang/String;
    const/4 v6, 0x0

    .line 515
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 516
    if-eqz v6, :cond_2

    .line 517
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getShareData(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v8

    .line 518
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 521
    :cond_2
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_3

    .line 522
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 527
    .end local v3    # "query":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "i":I
    .end local v9    # "selection":Ljava/lang/String;
    .end local v10    # "size":I
    :cond_3
    return-object v8

    .line 521
    .restart local v3    # "query":Ljava/lang/String;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v7    # "i":I
    .restart local v9    # "selection":Ljava/lang/String;
    .restart local v10    # "size":I
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_4

    .line 522
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public getTag(J)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    .locals 13
    .param p1, "heartRateRecordId"    # J

    .prologue
    .line 919
    const/4 v7, 0x0

    .line 921
    .local v7, "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "input_source_type"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string/jumbo v1, "tag_index"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "custom_icon_index"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "custom_tag_name"

    aput-object v1, v2, v0

    .line 922
    .local v2, "projection":[Ljava/lang/String;
    const-string/jumbo v3, "table_record_id=?"

    .line 923
    .local v3, "selection":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 925
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 927
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Tag;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 928
    if-eqz v6, :cond_2

    .line 929
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 930
    sget-object v11, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    .line 932
    .local v11, "tagType":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;
    const-string v0, "input_source_type"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 933
    .local v12, "type":I
    const v0, 0x1adb1

    if-ne v12, v0, :cond_0

    .line 934
    sget-object v11, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_USER_ADDED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    .line 937
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    if-ne v11, v0, :cond_4

    .line 938
    const-string/jumbo v0, "tag_index"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 939
    .local v9, "tagId":I
    invoke-static {v9}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getTag(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v7

    .line 949
    .end local v9    # "tagId":I
    .end local v11    # "tagType":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;
    .end local v12    # "type":I
    :cond_1
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 952
    :cond_2
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_3

    .line 953
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 957
    :cond_3
    return-object v7

    .line 940
    .restart local v11    # "tagType":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;
    .restart local v12    # "type":I
    :cond_4
    :try_start_1
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_USER_ADDED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    if-ne v11, v0, :cond_1

    .line 941
    const-string/jumbo v0, "tag_index"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 942
    .restart local v9    # "tagId":I
    const-string v0, "custom_icon_index"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 943
    .local v8, "tagIconId":I
    const-string v0, "custom_tag_name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 944
    .local v10, "tagName":Ljava/lang/String;
    invoke-static {v9, v8, v10}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getTag(IILjava/lang/String;)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v7

    goto :goto_0

    .line 952
    .end local v8    # "tagIconId":I
    .end local v9    # "tagId":I
    .end local v10    # "tagName":Ljava/lang/String;
    .end local v11    # "tagType":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;
    .end local v12    # "type":I
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_5

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_5

    .line 953
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
.end method

.method public getTagByIndex(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    .locals 11
    .param p1, "tagIndex"    # I

    .prologue
    const/4 v10, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 859
    const/4 v7, 0x0

    .line 860
    .local v7, "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    const/16 v0, 0x5208

    if-lt p1, v0, :cond_1

    const/16 v0, 0x533c

    if-gt p1, v0, :cond_1

    .line 862
    invoke-static {p1}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getTag(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v7

    .line 889
    :cond_0
    :goto_0
    return-object v7

    .line 866
    :cond_1
    new-array v2, v10, [Ljava/lang/String;

    const-string v0, "custom_icon_index"

    aput-object v0, v2, v1

    const-string v0, "custom_tag_name"

    aput-object v0, v2, v5

    .line 867
    .local v2, "projection":[Ljava/lang/String;
    const-string/jumbo v3, "tag_index=? AND input_source_type=?"

    .line 870
    .local v3, "selection":Ljava/lang/String;
    new-array v4, v10, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    const v0, 0x1adb1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 872
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 874
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Tag;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 875
    if-eqz v6, :cond_3

    .line 876
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 877
    const-string v0, "custom_icon_index"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 878
    .local v8, "tagIconId":I
    const-string v0, "custom_tag_name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 879
    .local v9, "tagName":Ljava/lang/String;
    invoke-static {p1, v8, v9}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getTag(IILjava/lang/String;)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v7

    .line 881
    .end local v8    # "tagIconId":I
    .end local v9    # "tagName":Ljava/lang/String;
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 884
    :cond_3
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 885
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 884
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_4

    .line 885
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public getTagsByTime(J)Ljava/lang/String;
    .locals 18
    .param p1, "time"    # J

    .prologue
    .line 961
    const/4 v15, 0x0

    .line 963
    .local v15, "tagIconResourceIds":Ljava/lang/String;
    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "start_time"

    aput-object v2, v3, v1

    const/4 v1, 0x1

    const-string v2, "_id"

    aput-object v2, v3, v1

    .line 964
    .local v3, "projection":[Ljava/lang/String;
    const-string/jumbo v4, "start_time>? AND start_time<?"

    .line 965
    .local v4, "selection":Ljava/lang/String;
    const/4 v1, 0x2

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static/range {p1 .. p2}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    const/4 v1, 0x1

    invoke-static/range {p1 .. p2}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getEndDayToMillis(J)J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    .line 967
    .local v5, "selectionArgs":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 969
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRate;->CONTENT_URI:Landroid/net/Uri;

    const-string/jumbo v6, "start_time desc"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 970
    if-eqz v8, :cond_2

    .line 971
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 972
    const-string/jumbo v1, "start_time"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    .line 973
    .local v12, "startTime":J
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    .line 974
    .local v7, "calendar":Ljava/util/Calendar;
    invoke-virtual {v7, v12, v13}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 975
    const/16 v1, 0xd

    const/4 v2, 0x0

    invoke-virtual {v7, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 976
    const/16 v1, 0xe

    const/4 v2, 0x0

    invoke-virtual {v7, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 977
    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v12

    .line 980
    cmp-long v1, v12, p1

    if-nez v1, :cond_0

    .line 981
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v11

    .line 982
    .local v11, "mHeartrateDatabaseHelper":Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    const-string v1, "_id"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v9

    .line 983
    .local v9, "heartrateRecordId":J
    invoke-virtual {v11, v9, v10}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getTag(J)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v14

    .line 985
    .local v14, "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    if-nez v14, :cond_4

    .line 986
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "No tag is assigned for the log id("

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ")."

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 999
    .end local v7    # "calendar":Ljava/util/Calendar;
    .end local v9    # "heartrateRecordId":J
    .end local v11    # "mHeartrateDatabaseHelper":Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    .end local v12    # "startTime":J
    .end local v14    # "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    :cond_1
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1002
    :cond_2
    if-eqz v8, :cond_3

    invoke-interface {v8}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1003
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1006
    :cond_3
    return-object v15

    .line 988
    .restart local v7    # "calendar":Ljava/util/Calendar;
    .restart local v9    # "heartrateRecordId":J
    .restart local v11    # "mHeartrateDatabaseHelper":Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    .restart local v12    # "startTime":J
    .restart local v14    # "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    :cond_4
    :try_start_1
    iget v1, v14, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mIconId:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v15

    .line 989
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 990
    iget-object v1, v14, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mType:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    sget-object v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    if-ne v1, v2, :cond_5

    .line 991
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v6, v14, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mNameId:I

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    goto :goto_0

    .line 993
    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v14, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v15

    .line 996
    goto :goto_0

    .line 1002
    .end local v7    # "calendar":Ljava/util/Calendar;
    .end local v9    # "heartrateRecordId":J
    .end local v11    # "mHeartrateDatabaseHelper":Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    .end local v12    # "startTime":J
    .end local v14    # "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    :catchall_0
    move-exception v1

    if-eqz v8, :cond_6

    invoke-interface {v8}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_6

    .line 1003
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v1
.end method

.method public updateCommentById(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "comment"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 379
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 380
    .local v2, "values":Landroid/content/ContentValues;
    const-string v5, "comment"

    invoke-virtual {v2, v5, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    const-string v0, "_id=?"

    .line 383
    .local v0, "selection":Ljava/lang/String;
    new-array v1, v3, [Ljava/lang/String;

    aput-object p1, v1, v4

    .line 384
    .local v1, "selectionArgs":[Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRate;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6, v2, v0, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    if-lez v5, :cond_0

    :goto_0
    return v3

    :cond_0
    move v3, v4

    goto :goto_0
.end method

.method public updateCustomTagByName(ILjava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "mTagIconId"    # I
    .param p2, "mTagText"    # Ljava/lang/String;
    .param p3, "mLastTagName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1061
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1062
    .local v2, "values":Landroid/content/ContentValues;
    const-string/jumbo v5, "table_name_type"

    const v6, 0x55733

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1063
    const-string v5, "custom_icon_index"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1064
    const-string v5, "custom_tag_name"

    invoke-virtual {v2, v5, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1065
    const-string v0, "custom_tag_name=?"

    .line 1066
    .local v0, "selection":Ljava/lang/String;
    new-array v1, v3, [Ljava/lang/String;

    aput-object p3, v1, v4

    .line 1067
    .local v1, "selectionArgs":[Ljava/lang/String;
    sget-object v5, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "New custom tag(iconId: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", name: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") is modified - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1068
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/health/content/ShealthContract$Tag;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6, v2, v0, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    if-lez v5, :cond_0

    :goto_0
    return v3

    :cond_0
    move v3, v4

    goto :goto_0
.end method

.method public updateTagFromHeartRateLogById(JI)Z
    .locals 7
    .param p1, "heartRateRecordId"    # J
    .param p3, "tagId"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1010
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1011
    .local v2, "values":Landroid/content/ContentValues;
    const-string/jumbo v5, "tag_index"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1012
    const-string v5, "input_source_type"

    const v6, 0x1adb2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1013
    const-string/jumbo v0, "table_record_id=?"

    .line 1014
    .local v0, "selection":Ljava/lang/String;
    new-array v1, v3, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    .line 1015
    .local v1, "selectionArgs":[Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/health/content/ShealthContract$Tag;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6, v2, v0, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    if-lez v5, :cond_0

    :goto_0
    return v3

    :cond_0
    move v3, v4

    goto :goto_0
.end method

.method public updateTagFromHeartRateLogById(JILjava/lang/String;I)Z
    .locals 7
    .param p1, "heartRateRecordId"    # J
    .param p3, "tagId"    # I
    .param p4, "tagName"    # Ljava/lang/String;
    .param p5, "tagIconId"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1020
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1021
    .local v2, "values":Landroid/content/ContentValues;
    const-string/jumbo v5, "tag_index"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1022
    const-string v5, "custom_icon_index"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1023
    const-string v5, "custom_tag_name"

    invoke-virtual {v2, v5, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1024
    const-string v5, "input_source_type"

    const v6, 0x1adb1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1025
    const-string/jumbo v0, "table_record_id=?"

    .line 1026
    .local v0, "selection":Ljava/lang/String;
    new-array v1, v3, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    .line 1027
    .local v1, "selectionArgs":[Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/health/content/ShealthContract$Tag;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6, v2, v0, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    if-lez v5, :cond_0

    :goto_0
    return v3

    :cond_0
    move v3, v4

    goto :goto_0
.end method
