.class Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$ScannedDeviceListAdapter;
.super Landroid/widget/BaseAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ScannedDeviceListAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$ScannedDeviceListAdapter;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 13

    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v3, 0x0

    const/16 v0, 0xa

    new-array v1, v0, [I

    const/4 v0, 0x5

    const/4 v2, 0x6

    const/4 v4, 0x7

    const/16 v5, 0x8

    const/16 v6, 0x9

    const/16 v7, -0x5c

    aput v7, v1, v6

    const/16 v6, -0xd

    aput v6, v1, v5

    const/16 v5, 0x4a76

    aput v5, v1, v4

    const/16 v4, -0x3dc4

    aput v4, v1, v2

    const/16 v2, -0x50

    aput v2, v1, v0

    const/16 v0, 0x5b58

    aput v0, v1, v12

    const/16 v0, 0x7508

    aput v0, v1, v11

    const/16 v0, 0x3e17

    aput v0, v1, v10

    const/16 v0, -0x21a9

    aput v0, v1, v9

    const/16 v0, -0x4e

    aput v0, v1, v3

    const/16 v0, 0xa

    new-array v0, v0, [I

    const/4 v2, 0x5

    const/4 v4, 0x6

    const/4 v5, 0x7

    const/16 v6, 0x8

    const/16 v7, 0x9

    const/16 v8, -0x3f

    aput v8, v0, v7

    const/16 v7, -0x70

    aput v7, v0, v6

    const/16 v6, 0x4a1f

    aput v6, v0, v5

    const/16 v5, -0x3db6

    aput v5, v0, v4

    const/16 v4, -0x3e

    aput v4, v0, v2

    const/16 v2, 0x5b3d

    aput v2, v0, v12

    const/16 v2, 0x755b

    aput v2, v0, v11

    const/16 v2, 0x3e75

    aput v2, v0, v10

    const/16 v2, -0x21c2

    aput v2, v0, v9

    const/16 v2, -0x22

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v4, v0

    if-lt v2, v4, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    move v2, v3

    :goto_1
    array-length v4, v0

    if-lt v2, v4, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x7

    new-array v1, v0, [I

    const/4 v0, 0x5

    const/4 v2, 0x6

    const/16 v6, 0x271b

    aput v6, v1, v2

    const/16 v2, 0x6d1d

    aput v2, v1, v0

    const/16 v0, 0x5819

    aput v0, v1, v12

    const/16 v0, 0x4d36

    aput v0, v1, v11

    const/16 v0, -0x6c8

    aput v0, v1, v10

    const/16 v0, -0x6a

    aput v0, v1, v9

    const/16 v0, -0x51

    aput v0, v1, v3

    const/4 v0, 0x7

    new-array v0, v0, [I

    const/4 v2, 0x5

    const/4 v6, 0x6

    const/16 v7, 0x273b

    aput v7, v0, v6

    const/16 v6, 0x6d27

    aput v6, v0, v2

    const/16 v2, 0x586d

    aput v2, v0, v12

    const/16 v2, 0x4d58

    aput v2, v0, v11

    const/16 v2, -0x6b3

    aput v2, v0, v10

    const/4 v2, -0x7

    aput v2, v0, v9

    const/16 v2, -0x34

    aput v2, v0, v3

    move v2, v3

    :goto_2
    array-length v6, v0

    if-lt v2, v6, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    :goto_3
    array-length v2, v0

    if-lt v3, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$ScannedDeviceListAdapter;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScannedDevicesList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1600(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$ScannedDeviceListAdapter;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScannedDevicesList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1600(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0

    :cond_0
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_1
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    :cond_2
    aget v6, v0, v2

    aget v7, v1, v2

    xor-int/2addr v6, v7

    aput v6, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 11

    const-wide v9, 0x5239f9bde556bec1L    # 1.29182464986668E88

    const/4 v8, 0x0

    const/16 v7, 0x20

    const/4 v0, 0x2

    new-array v2, v0, [J

    const/4 v0, 0x1

    const-wide/16 v3, 0x1

    aput-wide v3, v2, v0

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v8}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p1

    shl-long/2addr v0, v7

    ushr-long v3, v0, v7

    aget-wide v0, v2, v8

    const-wide/16 v5, 0x0

    cmp-long v5, v0, v5

    if-eqz v5, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v3

    xor-long/2addr v0, v9

    aput-wide v0, v2, v8

    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 12

    const-wide v10, 0x1632260c63196b81L    # 9.261614005361919E-202

    const-wide/16 v8, 0x0

    const/4 v7, 0x0

    const/16 v6, 0x20

    const/4 v0, 0x2

    new-array v2, v0, [J

    const/4 v0, 0x1

    const-wide/16 v3, 0x1

    aput-wide v3, v2, v0

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v7}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p1

    shl-long/2addr v0, v6

    ushr-long v3, v0, v6

    aget-wide v0, v2, v7

    cmp-long v5, v0, v8

    if-eqz v5, :cond_1

    xor-long/2addr v0, v10

    :cond_1
    ushr-long/2addr v0, v6

    shl-long/2addr v0, v6

    xor-long/2addr v0, v3

    xor-long/2addr v0, v10

    aput-wide v0, v2, v7

    return-wide v8
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 21

    const/4 v1, 0x2

    new-array v4, v1, [J

    const/4 v1, 0x1

    const-wide/16 v2, 0x1

    aput-wide v2, v4, v1

    const/4 v1, 0x0

    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v4, v2

    long-to-int v2, v2

    if-gtz v2, :cond_0

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v3, 0x0

    move/from16 v0, p1

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    ushr-long v5, v1, v5

    aget-wide v1, v4, v3

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_1

    const-wide v7, -0x52c850b999375e02L    # -7.26689055408849E-91

    xor-long/2addr v1, v7

    :cond_1
    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    const/16 v7, 0x20

    shl-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, -0x52c850b999375e02L    # -7.26689055408849E-91

    xor-long/2addr v1, v5

    aput-wide v1, v4, v3

    :try_start_0
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$ScannedDeviceListAdapter;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->device_name:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDevicename:Landroid/widget/TextView;
    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5802(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Landroid/widget/TextView;)Landroid/widget/TextView;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->paired_devices_divider:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->device_connected_status:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->device_image_name:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$ScannedDeviceListAdapter;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScannedDevicesList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1600(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/ArrayList;

    move-result-object v3

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-gtz v1, :cond_6

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x5f

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x6f

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :catch_0
    move-exception v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$ScannedDeviceListAdapter;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const/16 v1, 0xf

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, -0x61

    aput v19, v2, v18

    const/16 v18, -0xf

    aput v18, v2, v17

    const/16 v17, 0x7c56

    aput v17, v2, v16

    const/16 v16, 0x7e1d

    aput v16, v2, v15

    const/16 v15, 0x6212

    aput v15, v2, v14

    const/16 v14, -0x4bfc

    aput v14, v2, v13

    const/16 v13, -0x26

    aput v13, v2, v12

    const/16 v12, -0x66b5

    aput v12, v2, v11

    const/16 v11, -0x3a

    aput v11, v2, v10

    const/16 v10, -0x18

    aput v10, v2, v9

    const/16 v9, 0x1824

    aput v9, v2, v8

    const/16 v8, 0x4877

    aput v8, v2, v7

    const/16 v7, 0x4631

    aput v7, v2, v6

    const/16 v6, -0x33d9

    aput v6, v2, v3

    const/16 v3, -0x60

    aput v3, v2, v1

    const/16 v1, 0xf

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, -0x13

    aput v20, v1, v19

    const/16 v19, -0x6c

    aput v19, v1, v18

    const/16 v18, 0x7c22

    aput v18, v1, v17

    const/16 v17, 0x7e7c

    aput v17, v1, v16

    const/16 v16, 0x627e

    aput v16, v1, v15

    const/16 v15, -0x4b9e

    aput v15, v1, v14

    const/16 v14, -0x4c

    aput v14, v1, v13

    const/16 v13, -0x66de

    aput v13, v1, v12

    const/16 v12, -0x67

    aput v12, v1, v11

    const/16 v11, -0x64

    aput v11, v1, v10

    const/16 v10, 0x1851

    aput v10, v1, v9

    const/16 v9, 0x4818

    aput v9, v1, v8

    const/16 v8, 0x4648

    aput v8, v1, v7

    const/16 v7, -0x33ba

    aput v7, v1, v6

    const/16 v6, -0x34

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_3
    array-length v6, v1

    if-lt v3, v6, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_4
    array-length v6, v1

    if-lt v3, v6, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$layout;->accessory_item:I

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto/16 :goto_0

    :cond_2
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_3
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_4
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_5
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_6
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v4, 0x0

    cmp-long v4, v1, v4

    if-eqz v4, :cond_7

    const-wide v4, -0x52c850b999375e02L    # -7.26689055408849E-91

    xor-long/2addr v1, v4

    :cond_7
    const/16 v4, 0x20

    shl-long/2addr v1, v4

    const/16 v4, 0x20

    shr-long/2addr v1, v4

    long-to-int v1, v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;->deviceName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$ScannedDeviceListAdapter;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mDevicename:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5800(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p2
.end method
