.class Lcom/sec/android/app/shealth/food/foodpick/loader/LifoBlockingDeque;
.super Ljava/util/concurrent/LinkedBlockingDeque;
.source "LifoBlockingDeque.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/LinkedBlockingDeque",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x43605f7457214ab7L


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 22
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/loader/LifoBlockingDeque;, "Lcom/sec/android/app/shealth/food/foodpick/loader/LifoBlockingDeque<TE;>;"
    invoke-direct {p0}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>()V

    return-void
.end method


# virtual methods
.method public add(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 40
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/loader/LifoBlockingDeque;, "Lcom/sec/android/app/shealth/food/foodpick/loader/LifoBlockingDeque<TE;>;"
    .local p1, "e":Ljava/lang/Object;, "TE;"
    invoke-super {p0, p1}, Ljava/util/concurrent/LinkedBlockingDeque;->offerFirst(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public offer(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 28
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/loader/LifoBlockingDeque;, "Lcom/sec/android/app/shealth/food/foodpick/loader/LifoBlockingDeque<TE;>;"
    .local p1, "e":Ljava/lang/Object;, "TE;"
    invoke-super {p0, p1}, Ljava/util/concurrent/LinkedBlockingDeque;->offerFirst(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public offer(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z
    .locals 1
    .param p2, "timeout"    # J
    .param p4, "unit"    # Ljava/util/concurrent/TimeUnit;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;J",
            "Ljava/util/concurrent/TimeUnit;",
            ")Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 34
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/loader/LifoBlockingDeque;, "Lcom/sec/android/app/shealth/food/foodpick/loader/LifoBlockingDeque<TE;>;"
    .local p1, "e":Ljava/lang/Object;, "TE;"
    invoke-super {p0, p1, p2, p3, p4}, Ljava/util/concurrent/LinkedBlockingDeque;->offerFirst(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    return v0
.end method

.method public put(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 46
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/loader/LifoBlockingDeque;, "Lcom/sec/android/app/shealth/food/foodpick/loader/LifoBlockingDeque<TE;>;"
    .local p1, "e":Ljava/lang/Object;, "TE;"
    invoke-super {p0, p1}, Ljava/util/concurrent/LinkedBlockingDeque;->putFirst(Ljava/lang/Object;)V

    .line 47
    return-void
.end method
