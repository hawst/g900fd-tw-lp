.class final Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder$1;
.super Ljava/lang/Object;
.source "TopWalkersHolder.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
    .locals 10
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 234
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 235
    .local v1, "lName":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 236
    .local v2, "lGender":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 237
    .local v3, "lAge":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 238
    .local v4, "iconUrl":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v5

    .line 239
    .local v5, "lDistance":J
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v7

    .line 240
    .local v7, "lPosition":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    .line 241
    .local v8, "lStep":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v9

    .line 243
    .local v9, "lCalorie":I
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "___DEFAULT___"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v1, 0x0

    .line 244
    :cond_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "___DEFAULT___"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v2, 0x0

    .line 245
    :cond_3
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "___DEFAULT___"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    const/4 v4, 0x0

    .line 247
    :cond_5
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    invoke-direct/range {v0 .. v9}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JIII)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 225
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder$1;->createFromParcel(Landroid/os/Parcel;)Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 229
    new-array v0, p1, [Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 225
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder$1;->newArray(I)[Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    move-result-object v0

    return-object v0
.end method
