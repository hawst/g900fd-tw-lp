.class public Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;
.super Ljava/lang/Object;
.source "SharedPrefManager.java"


# static fields
.field private static final PREF_TYPE_BOOLEAN:I = 0x4

.field private static final PREF_TYPE_FLOAT:I = 0x3

.field private static final PREF_TYPE_INT:I = 0x5

.field private static final PREF_TYPE_LONG:I = 0x1

.field private static final PREF_TYPE_STRING:I = 0x2

.field private static final SHAREDPREF_FILE_BACKUP_RESTORE:Ljava/lang/String; = "com.sec.android.app.shealth_preferences_backup_restore"

.field private static final TAG:Ljava/lang/String;

.field private static instance:Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;


# instance fields
.field private mBackupPref:Landroid/content/SharedPreferences;

.field private mContext:Landroid/content/Context;

.field private mIsUpgradeFromKies:Z

.field private mPref:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->TAG:Ljava/lang/String;

    .line 34
    new-instance v0, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->instance:Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->mIsUpgradeFromKies:Z

    .line 48
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "shared pref manager created"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;
    .locals 2

    .prologue
    .line 63
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->TAG:Ljava/lang/String;

    const-string v1, "SharedPrefManager getInstance"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->instance:Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;

    return-object v0
.end method


# virtual methods
.method public getBackupPref()Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->mBackupPref:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method public getValue(Ljava/lang/String;)Ljava/lang/Object;
    .locals 12
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 68
    const/4 v0, 0x0

    .line 71
    .local v0, "sqlDB":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->getInstance()Lcom/sec/android/app/shealth/framework/repository/database/DBManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteCantOpenDatabaseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 79
    const-string v1, "configuration"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "key"

    aput-object v3, v2, v5

    const-string/jumbo v3, "value"

    aput-object v3, v2, v6

    const/4 v3, 0x2

    const-string/jumbo v5, "type"

    aput-object v5, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "key = \'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 83
    .local v8, "cursor":Landroid/database/Cursor;
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 87
    :try_start_1
    const-string/jumbo v1, "type"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v10

    .line 93
    .local v10, "type":I
    const/4 v11, 0x0

    .line 98
    .local v11, "value":Ljava/lang/Object;
    packed-switch v10, :pswitch_data_0

    .line 123
    :try_start_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 129
    :catchall_0
    move-exception v1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v1

    .line 73
    .end local v8    # "cursor":Landroid/database/Cursor;
    .end local v10    # "type":I
    .end local v11    # "value":Ljava/lang/Object;
    :catch_0
    move-exception v9

    .line 75
    .local v9, "e":Landroid/database/sqlite/SQLiteCantOpenDatabaseException;
    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteCantOpenDatabaseException;->printStackTrace()V

    .line 132
    .end local v9    # "e":Landroid/database/sqlite/SQLiteCantOpenDatabaseException;
    :goto_0
    return-object v4

    .line 88
    .restart local v8    # "cursor":Landroid/database/Cursor;
    :catch_1
    move-exception v9

    .line 89
    .local v9, "e":Landroid/database/CursorIndexOutOfBoundsException;
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 100
    .end local v9    # "e":Landroid/database/CursorIndexOutOfBoundsException;
    .restart local v10    # "type":I
    .restart local v11    # "value":Ljava/lang/Object;
    :pswitch_0
    :try_start_3
    const-string v1, "1"

    const-string/jumbo v2, "value"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 102
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v11

    .line 129
    .end local v11    # "value":Ljava/lang/Object;
    :goto_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move-object v4, v11

    .line 132
    goto :goto_0

    .line 104
    .restart local v11    # "value":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    :try_start_4
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    .line 106
    .local v11, "value":Ljava/lang/Boolean;
    goto :goto_1

    .line 108
    .local v11, "value":Ljava/lang/Object;
    :pswitch_1
    const-string/jumbo v1, "value"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    .line 110
    .local v11, "value":Ljava/lang/Integer;
    goto :goto_1

    .line 112
    .local v11, "value":Ljava/lang/Object;
    :pswitch_2
    const-string/jumbo v1, "value"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    .line 114
    .local v11, "value":Ljava/lang/Float;
    goto :goto_1

    .line 116
    .local v11, "value":Ljava/lang/Object;
    :pswitch_3
    const-string/jumbo v1, "value"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 117
    .local v11, "value":Ljava/lang/String;
    goto :goto_1

    .line 119
    .local v11, "value":Ljava/lang/Object;
    :pswitch_4
    const-string/jumbo v1, "value"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v11

    .line 121
    .local v11, "value":Ljava/lang/Long;
    goto :goto_1

    .line 98
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public initialize(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->TAG:Ljava/lang/String;

    const-string v1, "initializing SharedPrefManager"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->mContext:Landroid/content/Context;

    .line 42
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->mPref:Landroid/content/SharedPreferences;

    .line 43
    const-string v0, "com.sec.android.app.shealth_preferences_backup_restore"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->mBackupPref:Landroid/content/SharedPreferences;

    .line 44
    return-void
.end method

.method public onUpgrade(II)Z
    .locals 4
    .param p1, "oldVersion"    # I
    .param p2, "newVersion"    # I

    .prologue
    .line 52
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onUpgrade"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    new-instance v0, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->mPref:Landroid/content/SharedPreferences;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->mContext:Landroid/content/Context;

    iget-boolean v3, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->mIsUpgradeFromKies:Z

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;-><init>(Landroid/content/SharedPreferences;Landroid/content/Context;Z)V

    .line 54
    .local v0, "upgradeManager":Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;
    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/shealth/framework/repository/configuration/UpgradeManager;->upgrade(II)Z

    move-result v1

    return v1
.end method

.method public setUpgradeMode(Z)V
    .locals 2
    .param p1, "isUpgradeFromKies"    # Z

    .prologue
    .line 185
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "setUpgradeMode to kies"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->mIsUpgradeFromKies:Z

    .line 187
    return-void
.end method

.method public setValue(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 7
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    const/4 v6, 0x1

    .line 136
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->getInstance()Lcom/sec/android/app/shealth/framework/repository/database/DBManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 139
    .local v1, "sqlDB":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 140
    .local v2, "values":Landroid/content/ContentValues;
    instance-of v3, p2, Ljava/lang/Boolean;

    if-eqz v3, :cond_2

    .line 142
    const-string/jumbo v3, "type"

    const/4 v4, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 143
    const-string/jumbo v3, "value"

    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {v2, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 163
    :cond_0
    :goto_0
    :try_start_0
    const-string v3, "configuration"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "key = \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1

    .line 165
    const-string v3, "key"

    invoke-virtual {v2, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const-string v3, "configuration"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    :cond_1
    :goto_1
    return v6

    .line 144
    .restart local p2    # "value":Ljava/lang/Object;
    :cond_2
    instance-of v3, p2, Ljava/lang/Float;

    if-eqz v3, :cond_3

    .line 146
    const-string/jumbo v3, "type"

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 147
    const-string/jumbo v3, "value"

    check-cast p2, Ljava/lang/Float;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {v2, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    goto :goto_0

    .line 148
    .restart local p2    # "value":Ljava/lang/Object;
    :cond_3
    instance-of v3, p2, Ljava/lang/Integer;

    if-eqz v3, :cond_4

    .line 150
    const-string/jumbo v3, "type"

    const/4 v4, 0x5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 151
    const-string/jumbo v3, "value"

    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {v2, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    .line 152
    .restart local p2    # "value":Ljava/lang/Object;
    :cond_4
    instance-of v3, p2, Ljava/lang/Long;

    if-eqz v3, :cond_5

    .line 154
    const-string/jumbo v3, "type"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 155
    const-string/jumbo v3, "value"

    check-cast p2, Ljava/lang/Long;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {v2, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_0

    .line 156
    .restart local p2    # "value":Ljava/lang/Object;
    :cond_5
    instance-of v3, p2, Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 158
    const-string/jumbo v3, "type"

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 159
    const-string/jumbo v3, "value"

    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {v2, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 169
    :catch_0
    move-exception v0

    .line 170
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_1
.end method
