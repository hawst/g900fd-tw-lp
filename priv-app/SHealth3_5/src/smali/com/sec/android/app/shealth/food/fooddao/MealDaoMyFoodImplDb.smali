.class public Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFoodImplDb;
.super Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;
.source "MealDaoMyFoodImplDb.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFood;


# instance fields
.field private final mFilterQueryBuilder:Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 29
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    new-instance v1, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string v2, "category"

    new-instance v3, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-direct {v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;-><init>()V

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFoodImplDb;->getDefaultCategory(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;)Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;->getConstantRepresentation()Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "="

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;-><init>(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFoodImplDb;->mFilterQueryBuilder:Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    .line 40
    return-void
.end method


# virtual methods
.method protected getCommonMethodsFilter()Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFoodImplDb;->mFilterQueryBuilder:Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    return-object v0
.end method

.method protected getDefaultCategory(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;)Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;
    .locals 1
    .param p1, "item"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .prologue
    .line 44
    sget-object v0, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;->MY_FOOD:Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

    return-object v0
.end method
