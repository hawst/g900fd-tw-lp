.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$7;
.super Ljava/lang/Object;
.source "ExerciseProWorkoutInfoActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->customizeActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)V
    .locals 0

    .prologue
    .line 492
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    .line 496
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mEndMode:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->access$602(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;Z)Z

    .line 497
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 498
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "pick_result"

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseRowId:J
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 499
    const-string v1, "data_type"

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->dataType:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->access$900(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 500
    const-string v1, "edit"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 501
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    const/16 v2, 0x99

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 502
    return-void
.end method
