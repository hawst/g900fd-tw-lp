.class Lcom/sec/android/app/shealth/home/AddFavoritesActivity$4;
.super Ljava/lang/Object;
.source "AddFavoritesActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->addSelectSpinner()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)V
    .locals 0

    .prologue
    .line 517
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$4;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 10
    .param p2, "selectedItemView"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parentView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const v9, 0x7f090dfa

    const/16 v8, 0x10

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 522
    if-lez p3, :cond_9

    .line 524
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$4;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSHealthList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$000(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 525
    .local v2, "sHealthPluginCount":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$4;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPartnersList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$400(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 526
    .local v1, "partnerPluginCount":I
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$4;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090071

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 528
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_2

    .line 530
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$4;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSHealthList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$000(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/home/data/IconData;->getIsSelected()Z

    move-result v3

    if-nez v3, :cond_0

    .line 532
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$4;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mFavoritesCount:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$100(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)I

    move-result v3

    if-le v8, v3, :cond_1

    .line 534
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$4;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSHealthList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$000(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v3, v7}, Lcom/sec/android/app/shealth/home/data/IconData;->setIsSelected(Z)V

    .line 535
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$4;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # operator++ for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mFavoritesCount:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$108(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)I

    .line 528
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 538
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$4;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    invoke-static {v3, v9, v6}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 544
    :cond_2
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v1, :cond_8

    .line 546
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$4;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPartnersList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$400(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/home/data/IconData;->getIsSelected()Z

    move-result v3

    if-nez v3, :cond_3

    .line 548
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$4;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mFavoritesCount:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$100(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)I

    move-result v3

    if-le v8, v3, :cond_4

    .line 550
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$4;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPartnersList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$400(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v3, v7}, Lcom/sec/android/app/shealth/home/data/IconData;->setIsSelected(Z)V

    .line 551
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$4;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # operator++ for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mFavoritesCount:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$108(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)I

    .line 544
    :cond_3
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 554
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$4;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    invoke-static {v3, v9, v6}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_3

    .line 559
    .end local v0    # "i":I
    :cond_5
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$4;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090073

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 561
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_4
    if-ge v0, v2, :cond_6

    .line 563
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$4;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSHealthList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$000(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/shealth/home/data/IconData;->setIsSelected(Z)V

    .line 561
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 566
    :cond_6
    const/4 v0, 0x0

    :goto_5
    if-ge v0, v1, :cond_7

    .line 568
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$4;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPartnersList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$400(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/shealth/home/data/IconData;->setIsSelected(Z)V

    .line 566
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 570
    :cond_7
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$4;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    invoke-static {}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->getTotalFavoriteCount()I

    move-result v4

    # setter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mFavoritesCount:I
    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$102(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;I)I

    .line 573
    .end local v0    # "i":I
    :cond_8
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$4;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mCareAdapter:Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;
    invoke-static {v3}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$500(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->notifyDataSetChanged()V

    .line 574
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$4;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mFitnessAdapter:Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;
    invoke-static {v3}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$1200(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->notifyDataSetChanged()V

    .line 575
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$4;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # invokes: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->setActionBarSpinner()V
    invoke-static {v3}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$200(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)V

    .line 579
    .end local v1    # "partnerPluginCount":I
    .end local v2    # "sHealthPluginCount":I
    :cond_9
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 585
    .local p1, "parentView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
