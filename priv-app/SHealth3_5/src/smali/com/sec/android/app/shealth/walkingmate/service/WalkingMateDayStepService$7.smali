.class Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;
.super Landroid/content/BroadcastReceiver;
.source "WalkingMateDayStepService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V
    .locals 0

    .prologue
    .line 1151
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 21
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1154
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 1155
    .local v4, "action":Ljava/lang/String;
    const-string v17, "WalkingMateDayStepService"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "action = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1157
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 1158
    .local v6, "currentTimeStamp":J
    const-string v17, "android.intent.action.DATE_CHANGED"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_0

    const-string v17, "android.intent.action.TIME_TICK"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 1159
    :cond_0
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v7}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->dateChangeCheck(J)Z

    move-result v17

    if-nez v17, :cond_2

    .line 1312
    :cond_1
    :goto_0
    return-void

    .line 1162
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-object/from16 v17, v0

    # invokes: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->dateChanged()V
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$1200(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V

    goto :goto_0

    .line 1163
    :cond_3
    const-string v17, "com.sec.android.app.shealth.command.resettotalstep"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 1164
    const-string v17, "WalkingMateDayStepService"

    const-string/jumbo v18, "resettotalstep"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1165
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->resetData(Z)V

    .line 1167
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mUpdateRunnable:Ljava/lang/Runnable;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$1300(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)Ljava/lang/Runnable;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1168
    const-string v17, "WalkingMateDayStepService"

    const-string/jumbo v18, "notUsedNotify change false 5"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1170
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isWalkingStarted()Z

    move-result v17

    if-eqz v17, :cond_4

    .line 1171
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->onStop(Z)V

    .line 1172
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-object/from16 v17, v0

    # invokes: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->resetWithNotification()V
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$1400(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V

    goto :goto_0

    .line 1174
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-object/from16 v17, v0

    # invokes: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->resetWithNotification()V
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$1400(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V

    goto :goto_0

    .line 1176
    :cond_5
    const-string v17, "com.sec.android.app.shealth.command.deletetotalstep"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 1177
    const-wide/16 v14, 0x0

    .line 1178
    .local v14, "intentTime":J
    const-string/jumbo v17, "selectedDate"

    const-wide/16 v18, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move-wide/from16 v2, v18

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v14

    .line 1179
    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v10

    .line 1180
    .local v10, "endDayOfCurrentTime":J
    invoke-static {v14, v15}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v12

    .line 1181
    .local v12, "endDayofIntentTime":J
    const-string v17, "WalkingMateDayStepService"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "endDayOfCurrentTime:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", endDayOfIntentTime:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1182
    cmp-long v17, v10, v12

    if-nez v17, :cond_1

    .line 1183
    const-string v17, "WalkingMateDayStepService"

    const-string v18, "deletetotalstep"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->resetData(Z)V

    .line 1186
    const-string v17, "WalkingMateDayStepService"

    const-string/jumbo v18, "notUsedNotify change false 6"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1187
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mUpdateRunnable:Ljava/lang/Runnable;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$1300(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)Ljava/lang/Runnable;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1188
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isWalkingStarted()Z

    move-result v17

    if-eqz v17, :cond_6

    .line 1189
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->onStop(Z)V

    .line 1190
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-object/from16 v17, v0

    # invokes: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->resetWithNotification()V
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$1400(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V

    goto/16 :goto_0

    .line 1192
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-object/from16 v17, v0

    # invokes: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->resetWithNotification()V
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$1400(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V

    goto/16 :goto_0

    .line 1195
    .end local v10    # "endDayOfCurrentTime":J
    .end local v12    # "endDayofIntentTime":J
    .end local v14    # "intentTime":J
    :cond_7
    const-string v17, "com.sec.android.app.shealth.command.pedometer.goal.refresh"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 1196
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->checkNotiStatus()V

    goto/16 :goto_0

    .line 1197
    :cond_8
    const-string v17, "android.intent.action.TIME_SET"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_9

    const-string v17, "android.intent.action.TIMEZONE_CHANGED"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_d

    .line 1198
    :cond_9
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v17

    const-wide/16 v18, 0x0

    invoke-virtual/range {v17 .. v19}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->setLastUpdateTime(J)V

    .line 1200
    const-string v17, "android.intent.action.TIMEZONE_CHANGED"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 1201
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->setFixedTimeZone(Ljava/lang/String;)V

    .line 1203
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->currentEndofDate:J
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$1500(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)J

    move-result-wide v17

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v19

    cmp-long v17, v17, v19

    if-eqz v17, :cond_c

    .line 1204
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v17

    if-eqz v17, :cond_b

    .line 1205
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-object/from16 v17, v0

    # invokes: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->resetWithoutNotification()V
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$800(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V

    .line 1206
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->onStop(Z)V

    .line 1208
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-object/from16 v17, v0

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v18

    # setter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->currentEndofDate:J
    invoke-static/range {v17 .. v19}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$1502(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;J)J

    .line 1210
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-object/from16 v17, v0

    # invokes: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->resetWithoutNotification()V
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$800(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V

    .line 1211
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v17

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->setRefreshingNotiState(Z)V

    .line 1212
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->unsetInactiveMonitor()V

    .line 1213
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v17

    const/16 v18, 0x2

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->setInactiveMonitor(I)V

    .line 1214
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v17

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->setRefreshingNotiState(Z)V

    goto/16 :goto_0

    .line 1218
    :cond_d
    const-string v17, "com.samsung.android.sdk.health.sensor.action.DATA_UPDATED"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_10

    .line 1219
    const-string v17, "WalkingMateDayStepService"

    const-string v18, "[**] ACTION_HEALTH_DATA_UPDATED"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mSyncTimeoutRunnable:Ljava/lang/Runnable;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$1600(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)Ljava/lang/Runnable;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1221
    const-string v17, "WalkingMateDayStepService"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "mSyncing = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mSyncing:Z
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$1700()Z

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1223
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getConnectedDeviceType(Landroid/content/Context;)I

    move-result v16

    .line 1224
    .local v16, "viewStepCount":I
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->allowDeviceConnectionSwitch(Landroid/content/Context;)Z

    move-result v17

    if-eqz v17, :cond_e

    .line 1226
    const-string v17, "WalkingMateDayStepService"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "[FIRSTCONNECTION]onReceived : ACTION_HEALTH_DATA_UPDATED - viewStepCount:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1227
    invoke-static/range {v16 .. v16}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->saveViewStepCount(I)V

    .line 1228
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v17

    sput v17, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mCurrentDevice:I

    .line 1231
    :cond_e
    const-string v17, "com.samsung.android.sdk.health.sensor.extra.CONNECTION_TYPE"

    const/16 v18, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 1232
    .local v5, "connectionType":I
    const-string v17, "com.samsung.android.sdk.health.sensor.extra.DEVICE_TYPE"

    const/16 v18, -0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 1234
    .local v8, "deviceType":I
    const/16 v17, 0x7

    move/from16 v0, v17

    if-ne v5, v0, :cond_f

    .line 1235
    packed-switch v8, :pswitch_data_0

    .line 1264
    :pswitch_0
    const-string v17, "WalkingMateDayStepService"

    const-string v18, "Not registered wearable device"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1269
    :cond_f
    :goto_1
    const/16 v17, 0x0

    # setter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mSyncing:Z
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$1702(Z)Z

    .line 1270
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->refreshBestStep()V

    .line 1271
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v17

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->setRefreshingNotiState(Z)V

    .line 1272
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->refreshTodayGoalFromDB()V

    .line 1273
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v17

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->setRefreshingNotiState(Z)V

    .line 1275
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->refreshBestRecordInfo()V

    .line 1276
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->refreshFirstDate()V

    .line 1277
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->updateFromDB()V

    .line 1278
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-object/from16 v17, v0

    # invokes: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->callLoadLatestData()V
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$1800(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V

    .line 1280
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v17

    const/16 v18, -0x1

    const-string v19, "com.samsung.android.sdk.health.sensor.extra.DEVICE_TYPE"

    const/16 v20, -0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v19

    invoke-virtual/range {v17 .. v19}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->sendResponseForSync(II)V

    .line 1281
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->refreshGearGoal(Landroid/content/Context;)V

    .line 1283
    new-instance v17, Ljava/lang/Thread;

    new-instance v18, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$SyncWearableRunner;

    const/16 v19, 0x0

    invoke-direct/range {v18 .. v19}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$SyncWearableRunner;-><init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$1;)V

    invoke-direct/range {v17 .. v18}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    .line 1237
    :pswitch_1
    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->saveUpdateTimeOfGear(J)V

    .line 1238
    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->saveUpdateTimeOfAllGears(J)V

    goto/16 :goto_1

    .line 1241
    :pswitch_2
    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->saveUpdateTimeOfTizenGear(J)V

    .line 1242
    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->saveUpdateTimeOfAllGears(J)V

    goto/16 :goto_1

    .line 1245
    :pswitch_3
    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->saveUpdateTimeOfGear2(J)V

    .line 1246
    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->saveUpdateTimeOfAllGears(J)V

    goto/16 :goto_1

    .line 1249
    :pswitch_4
    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->saveUpdateTimeOfGear3(J)V

    .line 1250
    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->saveUpdateTimeOfAllGears(J)V

    goto/16 :goto_1

    .line 1253
    :pswitch_5
    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->saveUpdateTimeOfGearO(J)V

    .line 1254
    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->saveUpdateTimeOfAllGears(J)V

    goto/16 :goto_1

    .line 1257
    :pswitch_6
    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->saveUpdateTimeOfGearFit(J)V

    .line 1258
    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->saveUpdateTimeOfAllGears(J)V

    goto/16 :goto_1

    .line 1261
    :pswitch_7
    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->saveUpdateTimeOfActivityTracker(J)V

    goto/16 :goto_1

    .line 1285
    .end local v5    # "connectionType":I
    .end local v8    # "deviceType":I
    .end local v16    # "viewStepCount":I
    :cond_10
    const-string v17, "com.sec.android.app.shealth.pedometer.viewstepcount"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_11

    .line 1286
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v17

    sput v17, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mCurrentDevice:I

    .line 1287
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-object/from16 v17, v0

    # invokes: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->callLoadLatestData()V
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$1800(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V

    goto/16 :goto_0

    .line 1288
    :cond_11
    const-string v17, "com.sec.android.app.shealth.RESTORE_START"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_12

    .line 1289
    const-string v17, "WalkingMateDayStepService"

    const-string v18, "[**] RESTORE_START"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1290
    :cond_12
    const-string v17, "com.sec.android.app.shealth.RESTORE_END"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_13

    .line 1291
    const-string v17, "WalkingMateDayStepService"

    const-string v18, "[**] RESTORE_END"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1292
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-object/from16 v17, v0

    # invokes: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->callLoadLatestData()V
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$1800(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V

    goto/16 :goto_0

    .line 1293
    :cond_13
    const-string v17, "android.intent.action.SCREEN_ON"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_14

    .line 1294
    const-string v17, "WalkingMateDayStepService"

    const-string v18, "Intent.ACTION_SCREEN_ON is received"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1296
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v17

    # invokes: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->updateLockScreenWidget(Landroid/content/Context;)V
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$1900(Landroid/content/Context;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1297
    :catch_0
    move-exception v9

    .line 1298
    .local v9, "e":Landroid/os/RemoteException;
    invoke-virtual {v9}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 1300
    .end local v9    # "e":Landroid/os/RemoteException;
    :cond_14
    const-string v17, "android.intent.action.WEARABLE_DEVICE_SYNC"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 1301
    const-string v17, "WalkingMateDayStepService"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "[**] SYNC_START - mSyncing="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mSyncing:Z
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$1700()Z

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", mConnectAccessory="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    sget v19, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mCurrentDevice:I

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1303
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mSyncing:Z
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$1700()Z

    move-result v17

    if-nez v17, :cond_1

    sget v17, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mCurrentDevice:I

    const/16 v18, 0x2719

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_1

    .line 1304
    const-string v17, "WalkingMateDayStepService"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "1.action is "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, "mSyncing = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mSyncing:Z
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$1700()Z

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1305
    const/16 v17, 0x1

    # setter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mSyncing:Z
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$1702(Z)Z

    .line 1306
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v17

    new-instance v18, Landroid/content/Intent;

    sget v19, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mCurrentDevice:I

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->getWearableSyncIntent(I)Landroid/content/Intent;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1307
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->updateFromDB()V

    .line 1308
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-object/from16 v17, v0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->updateWidgets(Landroid/content/Context;)V

    .line 1309
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mSyncTimeoutRunnable:Ljava/lang/Runnable;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$1600(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)Ljava/lang/Runnable;

    move-result-object v18

    const-wide/32 v19, 0x88b8

    invoke-virtual/range {v17 .. v20}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 1235
    nop

    :pswitch_data_0
    .packed-switch 0x2723
        :pswitch_6
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_7
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method
