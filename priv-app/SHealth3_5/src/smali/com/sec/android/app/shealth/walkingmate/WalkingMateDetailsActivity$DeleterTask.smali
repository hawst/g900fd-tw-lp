.class Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$DeleterTask;
.super Landroid/os/AsyncTask;
.source "WalkingMateDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DeleterTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private mTime:J

.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;)V
    .locals 0

    .prologue
    .line 224
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$DeleterTask;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 224
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$DeleterTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 11
    .param p1, "deleteableId"    # [Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    .line 231
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 232
    .local v3, "deleteableTagList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$DeleterTask;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->mTag:Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->access$100(Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 234
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$DeleterTask;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;

    invoke-static {v8, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getDeletableItemList(Landroid/content/Context;Ljava/util/ArrayList;)Lcom/sec/android/app/shealth/walkingmate/utils/DeleteableData;

    move-result-object v2

    .line 235
    .local v2, "deleteableDataObj":Lcom/sec/android/app/shealth/walkingmate/utils/DeleteableData;
    iget-object v0, v2, Lcom/sec/android/app/shealth/walkingmate/utils/DeleteableData;->deleteableTagList:Ljava/util/ArrayList;

    .line 237
    .local v0, "deleteClause":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v4, v2, Lcom/sec/android/app/shealth/walkingmate/utils/DeleteableData;->deleteableEntryTime:[J

    .line 239
    .local v4, "deleteableTime":[J
    if-eqz v4, :cond_0

    array-length v8, v4

    if-lez v8, :cond_0

    .line 240
    const/4 v8, 0x0

    aget-wide v8, v4, v8

    iput-wide v8, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$DeleterTask;->mTime:J

    .line 243
    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 244
    .local v7, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lez v8, :cond_1

    .line 245
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 246
    .local v1, "deleteItem":Ljava/lang/String;
    sget-object v8, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v8}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v8

    invoke-virtual {v8, v1, v10}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 250
    .end local v1    # "deleteItem":Ljava/lang/String;
    .end local v6    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$DeleterTask;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "com.sec.android.app.shealth.cp.HealthContentProvider"

    invoke-virtual {v8, v9, v7}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 257
    :goto_1
    if-eqz v4, :cond_2

    .line 258
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v8

    invoke-virtual {v8, v4}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->deleteListbyTime([J)V

    .line 259
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->deleteExercise([J)V

    .line 262
    :cond_2
    return-object v10

    .line 251
    :catch_0
    move-exception v5

    .line 252
    .local v5, "e":Landroid/os/RemoteException;
    invoke-virtual {v5}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 253
    .end local v5    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v5

    .line 254
    .local v5, "e":Landroid/content/OperationApplicationException;
    invoke-virtual {v5}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 224
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$DeleterTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 4
    .param p1, "aVoid"    # Ljava/lang/Void;

    .prologue
    .line 267
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 269
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 270
    .local v0, "resultIntent":Landroid/content/Intent;
    const-string/jumbo v1, "time"

    iget-wide v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$DeleterTask;->mTime:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 272
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$DeleterTask;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;

    const/16 v2, 0x65

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->setResult(ILandroid/content/Intent;)V

    .line 273
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity$DeleterTask;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateDetailsActivity;->finish()V

    .line 275
    return-void
.end method
