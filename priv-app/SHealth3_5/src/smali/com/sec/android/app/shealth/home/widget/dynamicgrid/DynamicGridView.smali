.class public Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;
.super Landroid/widget/GridView;
.source "DynamicGridView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$OnDropListener;
    }
.end annotation


# static fields
.field private static final COLOUMN_COUNT:I = 0x4

.field private static final INVALID_ID:I = -0x1

.field private static final MOVE_DURATION:I = 0x12c


# instance fields
.field private idList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mActivePointerId:I

.field private mCellIsMobile:Z

.field private mDownX:I

.field private mDownY:I

.field private mDropListener:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$OnDropListener;

.field private mHoverAnimation:Z

.field private mHoverCell:Landroid/graphics/drawable/BitmapDrawable;

.field private mHoverCellCurrentBounds:Landroid/graphics/Rect;

.field private mHoverCellOriginalBounds:Landroid/graphics/Rect;

.field private mLastEventX:I

.field private mLastEventY:I

.field private mLocalItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mLocalLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

.field private mMobileItemId:J

.field private mReorderAnimation:Z

.field private mTotalOffsetX:I

.field private mTotalOffsetY:I

.field private mUserItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mUserLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 123
    invoke-direct {p0, p1}, Landroid/widget/GridView;-><init>(Landroid/content/Context;)V

    .line 45
    iput v3, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mTotalOffsetY:I

    .line 46
    iput v3, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mTotalOffsetX:I

    .line 48
    iput v2, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mDownX:I

    .line 49
    iput v2, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mDownY:I

    .line 50
    iput v2, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mLastEventY:I

    .line 51
    iput v2, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mLastEventX:I

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->idList:Ljava/util/List;

    .line 55
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mMobileItemId:J

    .line 57
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mCellIsMobile:Z

    .line 58
    iput v2, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mActivePointerId:I

    .line 65
    new-instance v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$1;-><init>(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mLocalLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 110
    new-instance v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$2;-><init>(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mLocalItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 125
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 128
    invoke-direct {p0, p1, p2}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    iput v3, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mTotalOffsetY:I

    .line 46
    iput v3, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mTotalOffsetX:I

    .line 48
    iput v2, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mDownX:I

    .line 49
    iput v2, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mDownY:I

    .line 50
    iput v2, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mLastEventY:I

    .line 51
    iput v2, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mLastEventX:I

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->idList:Ljava/util/List;

    .line 55
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mMobileItemId:J

    .line 57
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mCellIsMobile:Z

    .line 58
    iput v2, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mActivePointerId:I

    .line 65
    new-instance v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$1;-><init>(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mLocalLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 110
    new-instance v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$2;-><init>(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mLocalItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 130
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 133
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    iput v3, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mTotalOffsetY:I

    .line 46
    iput v3, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mTotalOffsetX:I

    .line 48
    iput v2, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mDownX:I

    .line 49
    iput v2, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mDownY:I

    .line 50
    iput v2, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mLastEventY:I

    .line 51
    iput v2, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mLastEventX:I

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->idList:Ljava/util/List;

    .line 55
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mMobileItemId:J

    .line 57
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mCellIsMobile:Z

    .line 58
    iput v2, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mActivePointerId:I

    .line 65
    new-instance v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$1;-><init>(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mLocalLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 110
    new-instance v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$2;-><init>(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mLocalItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 135
    return-void
.end method

.method private above(Landroid/graphics/Point;Landroid/graphics/Point;)Z
    .locals 2
    .param p1, "targetColumnRowPair"    # Landroid/graphics/Point;
    .param p2, "mobileColumnRowPair"    # Landroid/graphics/Point;

    .prologue
    .line 605
    iget v0, p1, Landroid/graphics/Point;->y:I

    iget v1, p2, Landroid/graphics/Point;->y:I

    if-ge v0, v1, :cond_0

    iget v0, p1, Landroid/graphics/Point;->x:I

    iget v1, p2, Landroid/graphics/Point;->x:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aboveLeft(Landroid/graphics/Point;Landroid/graphics/Point;)Z
    .locals 2
    .param p1, "targetColumnRowPair"    # Landroid/graphics/Point;
    .param p2, "mobileColumnRowPair"    # Landroid/graphics/Point;

    .prologue
    .line 597
    iget v0, p1, Landroid/graphics/Point;->y:I

    iget v1, p2, Landroid/graphics/Point;->y:I

    if-ge v0, v1, :cond_0

    iget v0, p1, Landroid/graphics/Point;->x:I

    iget v1, p2, Landroid/graphics/Point;->x:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aboveRight(Landroid/graphics/Point;Landroid/graphics/Point;)Z
    .locals 2
    .param p1, "targetColumnRowPair"    # Landroid/graphics/Point;
    .param p2, "mobileColumnRowPair"    # Landroid/graphics/Point;

    .prologue
    .line 601
    iget v0, p1, Landroid/graphics/Point;->y:I

    iget v1, p2, Landroid/graphics/Point;->y:I

    if-ge v0, v1, :cond_0

    iget v0, p1, Landroid/graphics/Point;->x:I

    iget v1, p2, Landroid/graphics/Point;->x:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;
    .param p1, "x1"    # I

    .prologue
    .line 35
    iput p1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mTotalOffsetY:I

    return p1
.end method

.method static synthetic access$012(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;
    .param p1, "x1"    # I

    .prologue
    .line 35
    iget v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mTotalOffsetY:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mTotalOffsetY:I

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;
    .param p1, "x1"    # I

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->playTalkBack(I)V

    return-void
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;
    .param p1, "x1"    # I

    .prologue
    .line 35
    iput p1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mTotalOffsetX:I

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;)Landroid/widget/AdapterView$OnItemLongClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mUserLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    return-object v0
.end method

.method static synthetic access$112(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;
    .param p1, "x1"    # I

    .prologue
    .line 35
    iget v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mTotalOffsetX:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mTotalOffsetX:I

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;)Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mUserItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;
    .param p1, "x1"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mHoverAnimation:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->updateEnableState()V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->reset(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->animateReorder(II)V

    return-void
.end method

.method static synthetic access$1702(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;
    .param p1, "x1"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mReorderAnimation:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    .prologue
    .line 35
    iget v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mDownX:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    .prologue
    .line 35
    iget v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mDownY:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    .prologue
    .line 35
    iget-wide v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mMobileItemId:J

    return-wide v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;J)J
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;
    .param p1, "x1"    # J

    .prologue
    .line 35
    iput-wide p1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mMobileItemId:J

    return-wide p1
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;Landroid/graphics/drawable/BitmapDrawable;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;
    .param p1, "x1"    # Landroid/graphics/drawable/BitmapDrawable;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mHoverCell:Landroid/graphics/drawable/BitmapDrawable;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;Landroid/view/View;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getAndAddHoverView(Landroid/view/View;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->isPostHoneycomb()Z

    move-result v0

    return v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;
    .param p1, "x1"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mCellIsMobile:Z

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;J)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;
    .param p1, "x1"    # J

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->updateNeighborViewsForId(J)V

    return-void
.end method

.method private adjustOpacity(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "opacity"    # I

    .prologue
    .line 214
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v2, p1

    .line 217
    .local v2, "mutableBitmap":Landroid/graphics/Bitmap;
    :goto_0
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 218
    .local v0, "canvas":Landroid/graphics/Canvas;
    and-int/lit16 v3, p2, 0xff

    shl-int/lit8 v1, v3, 0x18

    .line 219
    .local v1, "colour":I
    sget-object v3, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 220
    return-object v2

    .line 214
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v1    # "colour":I
    .end local v2    # "mutableBitmap":Landroid/graphics/Bitmap;
    :cond_0
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v4, 0x1

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_0
.end method

.method private animateBounds(Landroid/view/View;)V
    .locals 7
    .param p1, "mobileView"    # Landroid/view/View;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 432
    new-instance v1, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$3;-><init>(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;)V

    .line 446
    .local v1, "sBoundEvaluator":Landroid/animation/TypeEvaluator;, "Landroid/animation/TypeEvaluator<Landroid/graphics/Rect;>;"
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mHoverCell:Landroid/graphics/drawable/BitmapDrawable;

    const-string v3, "bounds"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mHoverCellCurrentBounds:Landroid/graphics/Rect;

    aput-object v6, v4, v5

    invoke-static {v2, v3, v1, v4}, Landroid/animation/ObjectAnimator;->ofObject(Ljava/lang/Object;Ljava/lang/String;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 448
    .local v0, "hoverViewAnimator":Landroid/animation/ObjectAnimator;
    new-instance v2, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$4;-><init>(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;)V

    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 454
    new-instance v2, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$5;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$5;-><init>(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;Landroid/view/View;)V

    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 468
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 469
    return-void
.end method

.method private animateReorder(II)V
    .locals 10
    .param p1, "oldPosition"    # I
    .param p2, "newPosition"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 640
    if-le p2, p1, :cond_0

    const/4 v6, 0x1

    .line 641
    .local v6, "isForward":Z
    :goto_0
    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V

    .line 642
    .local v8, "resultList":Ljava/util/List;, "Ljava/util/List<Landroid/animation/Animator;>;"
    if-eqz v6, :cond_2

    .line 643
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result v7

    .local v7, "pos":I
    :goto_1
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    if-ge v7, v0, :cond_4

    .line 644
    invoke-direct {p0, v7}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getId(I)J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getViewForId(J)Landroid/view/View;

    move-result-object v1

    .line 645
    .local v1, "view":Landroid/view/View;
    add-int/lit8 v0, v7, 0x1

    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getColumnCount()I

    move-result v2

    rem-int/2addr v0, v2

    if-nez v0, :cond_1

    .line 646
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v0

    neg-int v0, v0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getColumnCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    mul-int/2addr v0, v2

    int-to-float v2, v0

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v4, v0

    move-object v0, p0

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->createTranslationAnimations(Landroid/view/View;FFFF)Landroid/animation/AnimatorSet;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 643
    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 640
    .end local v1    # "view":Landroid/view/View;
    .end local v6    # "isForward":Z
    .end local v7    # "pos":I
    .end local v8    # "resultList":Ljava/util/List;, "Ljava/util/List<Landroid/animation/Animator;>;"
    :cond_0
    const/4 v6, 0x0

    goto :goto_0

    .line 648
    .restart local v1    # "view":Landroid/view/View;
    .restart local v6    # "isForward":Z
    .restart local v7    # "pos":I
    .restart local v8    # "resultList":Ljava/util/List;, "Ljava/util/List<Landroid/animation/Animator;>;"
    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v2, v0

    move-object v0, p0

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->createTranslationAnimations(Landroid/view/View;FFFF)Landroid/animation/AnimatorSet;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 652
    .end local v1    # "view":Landroid/view/View;
    .end local v7    # "pos":I
    :cond_2
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v7

    .restart local v7    # "pos":I
    :goto_3
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    if-le v7, v0, :cond_4

    .line 653
    invoke-direct {p0, v7}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getId(I)J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getViewForId(J)Landroid/view/View;

    move-result-object v1

    .line 654
    .restart local v1    # "view":Landroid/view/View;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getColumnCount()I

    move-result v0

    add-int/2addr v0, v7

    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getColumnCount()I

    move-result v2

    rem-int/2addr v0, v2

    if-nez v0, :cond_3

    .line 655
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getColumnCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    mul-int/2addr v0, v2

    int-to-float v2, v0

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v4, v0

    move-object v0, p0

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->createTranslationAnimations(Landroid/view/View;FFFF)Landroid/animation/AnimatorSet;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 652
    :goto_4
    add-int/lit8 v7, v7, -0x1

    goto :goto_3

    .line 657
    :cond_3
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v0

    neg-int v0, v0

    int-to-float v2, v0

    move-object v0, p0

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->createTranslationAnimations(Landroid/view/View;FFFF)Landroid/animation/AnimatorSet;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 662
    .end local v1    # "view":Landroid/view/View;
    :cond_4
    new-instance v9, Landroid/animation/AnimatorSet;

    invoke-direct {v9}, Landroid/animation/AnimatorSet;-><init>()V

    .line 663
    .local v9, "resultSet":Landroid/animation/AnimatorSet;
    invoke-virtual {v9, v8}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 664
    const-wide/16 v2, 0x12c

    invoke-virtual {v9, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 665
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v9, v0}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 666
    new-instance v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$7;-><init>(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;)V

    invoke-virtual {v9, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 679
    invoke-virtual {v9}, Landroid/animation/AnimatorSet;->start()V

    .line 680
    return-void
.end method

.method private below(Landroid/graphics/Point;Landroid/graphics/Point;)Z
    .locals 2
    .param p1, "targetColumnRowPair"    # Landroid/graphics/Point;
    .param p2, "mobileColumnRowPair"    # Landroid/graphics/Point;

    .prologue
    .line 609
    iget v0, p1, Landroid/graphics/Point;->y:I

    iget v1, p2, Landroid/graphics/Point;->y:I

    if-le v0, v1, :cond_0

    iget v0, p1, Landroid/graphics/Point;->x:I

    iget v1, p2, Landroid/graphics/Point;->x:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private belowLeft(Landroid/graphics/Point;Landroid/graphics/Point;)Z
    .locals 2
    .param p1, "targetColumnRowPair"    # Landroid/graphics/Point;
    .param p2, "mobileColumnRowPair"    # Landroid/graphics/Point;

    .prologue
    .line 589
    iget v0, p1, Landroid/graphics/Point;->y:I

    iget v1, p2, Landroid/graphics/Point;->y:I

    if-le v0, v1, :cond_0

    iget v0, p1, Landroid/graphics/Point;->x:I

    iget v1, p2, Landroid/graphics/Point;->x:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private belowRight(Landroid/graphics/Point;Landroid/graphics/Point;)Z
    .locals 2
    .param p1, "targetColumnRowPair"    # Landroid/graphics/Point;
    .param p2, "mobileColumnRowPair"    # Landroid/graphics/Point;

    .prologue
    .line 593
    iget v0, p1, Landroid/graphics/Point;->y:I

    iget v1, p2, Landroid/graphics/Point;->y:I

    if-le v0, v1, :cond_0

    iget v0, p1, Landroid/graphics/Point;->x:I

    iget v1, p2, Landroid/graphics/Point;->x:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private createTranslationAnimations(Landroid/view/View;FFFF)Landroid/animation/AnimatorSet;
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "startX"    # F
    .param p3, "endX"    # F
    .param p4, "startY"    # F
    .param p5, "endY"    # F
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 694
    const-string/jumbo v3, "translationX"

    new-array v4, v7, [F

    aput p2, v4, v5

    aput p3, v4, v6

    invoke-static {p1, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 695
    .local v1, "animX":Landroid/animation/ObjectAnimator;
    const-string/jumbo v3, "translationY"

    new-array v4, v7, [F

    aput p4, v4, v5

    aput p5, v4, v6

    invoke-static {p1, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 696
    .local v2, "animY":Landroid/animation/ObjectAnimator;
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 697
    .local v0, "animSetXY":Landroid/animation/AnimatorSet;
    new-array v3, v7, [Landroid/animation/Animator;

    aput-object v1, v3, v5

    aput-object v2, v3, v6

    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 698
    return-object v0
.end method

.method private getAdapterInterface()Lcom/sec/android/app/shealth/home/widget/dynamicgrid/AbstractDynamicGridAdapter;
    .locals 1

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/AbstractDynamicGridAdapter;

    return-object v0
.end method

.method private getAndAddHoverView(Landroid/view/View;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 184
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v6

    .line 185
    .local v6, "w":I
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    .line 186
    .local v3, "h":I
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v5

    .line 187
    .local v5, "top":I
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v4

    .line 189
    .local v4, "left":I
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getBitmapFromView(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 190
    .local v1, "b1":Landroid/graphics/Bitmap;
    const/16 v7, 0x66

    invoke-direct {p0, v1, v7}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->adjustOpacity(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 202
    .local v0, "b":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-direct {v2, v7, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 204
    .local v2, "drawable":Landroid/graphics/drawable/BitmapDrawable;
    new-instance v7, Landroid/graphics/Rect;

    add-int v8, v4, v6

    add-int v9, v5, v3

    invoke-direct {v7, v4, v5, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v7, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mHoverCellOriginalBounds:Landroid/graphics/Rect;

    .line 205
    new-instance v7, Landroid/graphics/Rect;

    iget-object v8, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mHoverCellOriginalBounds:Landroid/graphics/Rect;

    invoke-direct {v7, v8}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v7, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mHoverCellCurrentBounds:Landroid/graphics/Rect;

    .line 207
    iget-object v7, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mHoverCellCurrentBounds:Landroid/graphics/Rect;

    invoke-virtual {v2, v7}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 209
    return-object v2
.end method

.method private getBitmapFromView(Landroid/view/View;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 277
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 278
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 279
    .local v1, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {p1, v1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 280
    return-object v0
.end method

.method private getColumnAndRowForView(Landroid/view/View;)Landroid/graphics/Point;
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 621
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getPositionForView(Landroid/view/View;)I

    move-result v2

    .line 622
    .local v2, "pos":I
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getColumnCount()I

    move-result v1

    .line 623
    .local v1, "columns":I
    rem-int v0, v2, v1

    .line 624
    .local v0, "column":I
    div-int v3, v2, v1

    .line 625
    .local v3, "row":I
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4, v0, v3}, Landroid/graphics/Point;-><init>(II)V

    return-object v4
.end method

.method private getColumnCount()I
    .locals 1

    .prologue
    .line 170
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getAdapterInterface()Lcom/sec/android/app/shealth/home/widget/dynamicgrid/AbstractDynamicGridAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/AbstractDynamicGridAdapter;->getColumnCount()I

    move-result v0

    return v0
.end method

.method private getId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 629
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method private handleCellSwitch()V
    .locals 23

    .prologue
    .line 509
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mLastEventY:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mDownY:I

    sub-int v5, v2, v3

    .line 510
    .local v5, "deltaY":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mLastEventX:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mDownX:I

    sub-int v6, v2, v3

    .line 511
    .local v6, "deltaX":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mHoverCellOriginalBounds:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mTotalOffsetY:I

    add-int/2addr v2, v3

    add-int v10, v2, v5

    .line 512
    .local v10, "deltaYTotal":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mHoverCellOriginalBounds:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mTotalOffsetX:I

    add-int/2addr v2, v3

    add-int v9, v2, v6

    .line 513
    .local v9, "deltaXTotal":I
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mMobileItemId:J

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getViewForId(J)Landroid/view/View;

    move-result-object v14

    .line 514
    .local v14, "mobileView":Landroid/view/View;
    const/16 v17, 0x0

    .line 515
    .local v17, "targetView":Landroid/view/View;
    const/16 v18, 0x0

    .line 516
    .local v18, "vX":F
    const/16 v19, 0x0

    .line 517
    .local v19, "vY":F
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getColumnAndRowForView(Landroid/view/View;)Landroid/graphics/Point;

    move-result-object v13

    .line 518
    .local v13, "mobileColumnRowPair":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->idList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Long;

    .line 519
    .local v12, "id":Ljava/lang/Long;
    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getViewForId(J)Landroid/view/View;

    move-result-object v20

    .line 520
    .local v20, "view":Landroid/view/View;
    if-eqz v20, :cond_0

    .line 521
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getColumnAndRowForView(Landroid/view/View;)Landroid/graphics/Point;

    move-result-object v15

    .line 522
    .local v15, "targetColumnRowPair":Landroid/graphics/Point;
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v13}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->aboveRight(Landroid/graphics/Point;Landroid/graphics/Point;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getBottom()I

    move-result v2

    if-ge v10, v2, :cond_1

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getLeft()I

    move-result v2

    if-gt v9, v2, :cond_8

    :cond_1
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v13}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->aboveLeft(Landroid/graphics/Point;Landroid/graphics/Point;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getBottom()I

    move-result v2

    if-ge v10, v2, :cond_2

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getRight()I

    move-result v2

    if-lt v9, v2, :cond_8

    :cond_2
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v13}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->belowRight(Landroid/graphics/Point;Landroid/graphics/Point;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getTop()I

    move-result v2

    if-le v10, v2, :cond_3

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getLeft()I

    move-result v2

    if-gt v9, v2, :cond_8

    :cond_3
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v13}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->belowLeft(Landroid/graphics/Point;Landroid/graphics/Point;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getTop()I

    move-result v2

    if-le v10, v2, :cond_4

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getRight()I

    move-result v2

    if-lt v9, v2, :cond_8

    :cond_4
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v13}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->above(Landroid/graphics/Point;Landroid/graphics/Point;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getBottom()I

    move-result v2

    if-lt v10, v2, :cond_8

    :cond_5
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v13}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->below(Landroid/graphics/Point;Landroid/graphics/Point;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getTop()I

    move-result v2

    if-gt v10, v2, :cond_8

    :cond_6
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v13}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->right(Landroid/graphics/Point;Landroid/graphics/Point;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getLeft()I

    move-result v2

    if-gt v9, v2, :cond_8

    :cond_7
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v13}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->left(Landroid/graphics/Point;Landroid/graphics/Point;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getRight()I

    move-result v2

    if-ge v9, v2, :cond_0

    .line 534
    :cond_8
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridUtils;->getViewX(Landroid/view/View;)F

    move-result v2

    invoke-static {v14}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridUtils;->getViewX(Landroid/view/View;)F

    move-result v3

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v21

    .line 535
    .local v21, "xDiff":F
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridUtils;->getViewY(Landroid/view/View;)F

    move-result v2

    invoke-static {v14}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridUtils;->getViewY(Landroid/view/View;)F

    move-result v3

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v22

    .line 536
    .local v22, "yDiff":F
    cmpl-float v2, v21, v18

    if-ltz v2, :cond_0

    cmpl-float v2, v22, v19

    if-ltz v2, :cond_0

    .line 537
    move/from16 v18, v21

    .line 538
    move/from16 v19, v22

    .line 539
    move-object/from16 v17, v20

    goto/16 :goto_0

    .line 544
    .end local v12    # "id":Ljava/lang/Long;
    .end local v15    # "targetColumnRowPair":Landroid/graphics/Point;
    .end local v20    # "view":Landroid/view/View;
    .end local v21    # "xDiff":F
    .end local v22    # "yDiff":F
    :cond_9
    if-eqz v17, :cond_a

    .line 545
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getPositionForView(Landroid/view/View;)I

    move-result v7

    .line 546
    .local v7, "originalPosition":I
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getPositionForView(Landroid/view/View;)I

    move-result v16

    .line 548
    .local v16, "targetPosition":I
    const/4 v2, -0x1

    move/from16 v0, v16

    if-ne v0, v2, :cond_b

    .line 549
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mMobileItemId:J

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->updateNeighborViewsForId(J)V

    .line 586
    .end local v7    # "originalPosition":I
    .end local v16    # "targetPosition":I
    :cond_a
    :goto_1
    return-void

    .line 552
    .restart local v7    # "originalPosition":I
    .restart local v16    # "targetPosition":I
    :cond_b
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v7, v1}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->reorderElements(II)V

    .line 555
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mLastEventY:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mDownY:I

    .line 556
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mLastEventX:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mDownX:I

    .line 557
    const/4 v2, 0x0

    invoke-virtual {v14, v2}, Landroid/view/View;->setVisibility(I)V

    .line 559
    const v2, 0x7f0804f2

    invoke-virtual {v14, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 567
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mMobileItemId:J

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->updateNeighborViewsForId(J)V

    .line 568
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v4

    .line 569
    .local v4, "observer":Landroid/view/ViewTreeObserver;
    move/from16 v8, v16

    .line 570
    .local v8, "finalTargetPosition":I
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->isPostHoneycomb()Z

    move-result v2

    if-eqz v2, :cond_c

    if-eqz v4, :cond_c

    .line 571
    new-instance v2, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$6;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v8}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$6;-><init>(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;Landroid/view/ViewTreeObserver;IIII)V

    invoke-virtual {v4, v2}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    goto :goto_1

    .line 582
    :cond_c
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mTotalOffsetY:I

    add-int/2addr v2, v5

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mTotalOffsetY:I

    .line 583
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mTotalOffsetX:I

    add-int/2addr v2, v6

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mTotalOffsetX:I

    goto :goto_1
.end method

.method private isPostHoneycomb()Z
    .locals 2

    .prologue
    .line 491
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private left(Landroid/graphics/Point;Landroid/graphics/Point;)Z
    .locals 2
    .param p1, "targetColumnRowPair"    # Landroid/graphics/Point;
    .param p2, "mobileColumnRowPair"    # Landroid/graphics/Point;

    .prologue
    .line 617
    iget v0, p1, Landroid/graphics/Point;->y:I

    iget v1, p2, Landroid/graphics/Point;->y:I

    if-ne v0, v1, :cond_0

    iget v0, p1, Landroid/graphics/Point;->x:I

    iget v1, p2, Landroid/graphics/Point;->x:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private playTalkBack(I)V
    .locals 10
    .param p1, "targetPosition"    # I

    .prologue
    .line 226
    rem-int/lit8 v4, p1, 0x4

    .line 227
    .local v4, "x":I
    div-int/lit8 v5, p1, 0x4

    .line 228
    .local v5, "y":I
    add-int/lit8 v0, v4, 0x1

    .line 229
    .local v0, "column":I
    add-int/lit8 v2, v5, 0x1

    .line 230
    .local v2, "row":I
    iget-object v6, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 231
    .local v1, "res":Landroid/content/res/Resources;
    const v6, 0x7f090cf0

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 234
    .local v3, "text":Ljava/lang/String;
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 235
    return-void
.end method

.method private reorderElements(II)V
    .locals 1
    .param p1, "originalPosition"    # I
    .param p2, "targetPosition"    # I

    .prologue
    .line 165
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getAdapterInterface()Lcom/sec/android/app/shealth/home/widget/dynamicgrid/AbstractDynamicGridAdapter;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/AbstractDynamicGridAdapter;->reorderItems(II)V

    .line 166
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->playTalkBack(I)V

    .line 167
    return-void
.end method

.method private reset(Landroid/view/View;)V
    .locals 2
    .param p1, "mobileView"    # Landroid/view/View;

    .prologue
    .line 472
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->idList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 473
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mMobileItemId:J

    .line 474
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 475
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mHoverCell:Landroid/graphics/drawable/BitmapDrawable;

    .line 477
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->invalidate()V

    .line 478
    return-void
.end method

.method private right(Landroid/graphics/Point;Landroid/graphics/Point;)Z
    .locals 2
    .param p1, "targetColumnRowPair"    # Landroid/graphics/Point;
    .param p2, "mobileColumnRowPair"    # Landroid/graphics/Point;

    .prologue
    .line 613
    iget v0, p1, Landroid/graphics/Point;->y:I

    iget v1, p2, Landroid/graphics/Point;->y:I

    if-ne v0, v1, :cond_0

    iget v0, p1, Landroid/graphics/Point;->x:I

    iget v1, p2, Landroid/graphics/Point;->x:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private touchEventsCancelled()V
    .locals 3

    .prologue
    .line 495
    iget-wide v1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mMobileItemId:J

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getViewForId(J)Landroid/view/View;

    move-result-object v0

    .line 496
    .local v0, "mobileView":Landroid/view/View;
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mCellIsMobile:Z

    if-eqz v1, :cond_0

    .line 497
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->reset(Landroid/view/View;)V

    .line 499
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mCellIsMobile:Z

    .line 500
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mActivePointerId:I

    .line 502
    return-void
.end method

.method private updateEnableState()V
    .locals 1

    .prologue
    .line 481
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mHoverAnimation:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mReorderAnimation:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->setEnabled(Z)V

    .line 482
    return-void

    .line 481
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateNeighborViewsForId(J)V
    .locals 6
    .param p1, "itemId"    # J

    .prologue
    .line 287
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getPositionForID(J)I

    move-result v0

    .line 288
    .local v0, "draggedPos":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getFirstVisiblePosition()I

    move-result v2

    .local v2, "pos":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getLastVisiblePosition()I

    move-result v3

    if-gt v2, v3, :cond_1

    .line 289
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    invoke-interface {v3, v2}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/data/IconData;

    .line 290
    .local v1, "icondata":Lcom/sec/android/app/shealth/home/data/IconData;
    if-eqz v1, :cond_0

    if-eq v0, v2, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/IconData;->getIconType()I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 291
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->idList:Ljava/util/List;

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getId(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 288
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 294
    .end local v1    # "icondata":Lcom/sec/android/app/shealth/home/data/IconData;
    :cond_1
    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 703
    invoke-super {p0, p1}, Landroid/widget/GridView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 704
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mHoverCell:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    .line 705
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mHoverCell:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 707
    :cond_0
    return-void
.end method

.method public getPositionForID(J)I
    .locals 2
    .param p1, "itemId"    # J

    .prologue
    .line 300
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getViewForId(J)Landroid/view/View;

    move-result-object v0

    .line 301
    .local v0, "v":Landroid/view/View;
    if-nez v0, :cond_0

    .line 302
    const/4 v1, -0x1

    .line 304
    :goto_0
    return v1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getPositionForView(Landroid/view/View;)I

    move-result v1

    goto :goto_0
.end method

.method public getViewForId(J)Landroid/view/View;
    .locals 8
    .param p1, "itemId"    # J

    .prologue
    .line 309
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getFirstVisiblePosition()I

    move-result v1

    .line 310
    .local v1, "firstVisiblePosition":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/AbstractDynamicGridAdapter;

    .line 311
    .local v0, "adapter":Lcom/sec/android/app/shealth/home/widget/dynamicgrid/AbstractDynamicGridAdapter;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getChildCount()I

    move-result v7

    if-ge v2, v7, :cond_1

    .line 312
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 313
    .local v6, "v":Landroid/view/View;
    add-int v5, v1, v2

    .line 314
    .local v5, "position":I
    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/AbstractDynamicGridAdapter;->getItemId(I)J

    move-result-wide v3

    .line 315
    .local v3, "id":J
    cmp-long v7, v3, p1

    if-nez v7, :cond_0

    .line 319
    .end local v3    # "id":J
    .end local v5    # "position":I
    .end local v6    # "v":Landroid/view/View;
    :goto_1
    return-object v6

    .line 311
    .restart local v3    # "id":J
    .restart local v5    # "position":I
    .restart local v6    # "v":Landroid/view/View;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 319
    .end local v3    # "id":J
    .end local v5    # "position":I
    .end local v6    # "v":Landroid/view/View;
    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    .line 324
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    and-int/lit16 v5, v5, 0xff

    packed-switch v5, :pswitch_data_0

    .line 395
    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/widget/GridView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    :goto_1
    return v4

    .line 326
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    float-to-int v5, v5

    iput v5, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mDownX:I

    .line 327
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    iput v5, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mDownY:I

    .line 328
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mActivePointerId:I

    .line 330
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->isEnabled()Z

    move-result v5

    if-nez v5, :cond_0

    goto :goto_1

    .line 336
    :pswitch_2
    iget v5, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mActivePointerId:I

    const/4 v6, -0x1

    if-eq v5, v6, :cond_0

    .line 340
    iget v5, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mActivePointerId:I

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v3

    .line 342
    .local v3, "pointerIndex":I
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v5

    float-to-int v5, v5

    iput v5, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mLastEventY:I

    .line 343
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v5

    float-to-int v5, v5

    iput v5, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mLastEventX:I

    .line 344
    iget v5, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mLastEventY:I

    iget v6, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mDownY:I

    sub-int v1, v5, v6

    .line 345
    .local v1, "deltaY":I
    iget v5, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mLastEventX:I

    iget v6, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mDownX:I

    sub-int v0, v5, v6

    .line 347
    .local v0, "deltaX":I
    iget-boolean v5, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mCellIsMobile:Z

    if-eqz v5, :cond_0

    .line 348
    iget-object v5, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mHoverCellCurrentBounds:Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mHoverCellOriginalBounds:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    add-int/2addr v6, v0

    iget v7, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mTotalOffsetX:I

    add-int/2addr v6, v7

    iget-object v7, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mHoverCellOriginalBounds:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    add-int/2addr v7, v1

    iget v8, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mTotalOffsetY:I

    add-int/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 350
    iget-object v5, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mHoverCell:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v6, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mHoverCellCurrentBounds:Landroid/graphics/Rect;

    invoke-virtual {v5, v6}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 351
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->invalidate()V

    .line 352
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->handleCellSwitch()V

    goto :goto_1

    .line 360
    .end local v0    # "deltaX":I
    .end local v1    # "deltaY":I
    .end local v3    # "pointerIndex":I
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->touchEventsEnded()V

    .line 364
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mHoverCell:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v4, :cond_0

    .line 365
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mDropListener:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$OnDropListener;

    if-eqz v4, :cond_0

    .line 366
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mDropListener:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$OnDropListener;

    invoke-interface {v4}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$OnDropListener;->onActionDrop()V

    goto :goto_0

    .line 371
    :pswitch_4
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->touchEventsCancelled()V

    .line 373
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mHoverCell:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v4, :cond_0

    .line 374
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mDropListener:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$OnDropListener;

    if-eqz v4, :cond_0

    .line 375
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mDropListener:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$OnDropListener;

    invoke-interface {v4}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$OnDropListener;->onActionDrop()V

    goto/16 :goto_0

    .line 384
    :pswitch_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    const v5, 0xff00

    and-int/2addr v4, v5

    shr-int/lit8 v3, v4, 0x8

    .line 386
    .restart local v3    # "pointerIndex":I
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v2

    .line 387
    .local v2, "pointerId":I
    iget v4, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mActivePointerId:I

    if-ne v2, v4, :cond_0

    .line 388
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->touchEventsEnded()V

    goto/16 :goto_0

    .line 324
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0
    .param p1, "x0"    # Landroid/widget/Adapter;

    .prologue
    .line 35
    check-cast p1, Landroid/widget/ListAdapter;

    .end local p1    # "x0":Landroid/widget/Adapter;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 0
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    .line 402
    invoke-super {p0, p1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 403
    return-void
.end method

.method public setOnDropListener(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$OnDropListener;)V
    .locals 0
    .param p1, "dropListener"    # Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$OnDropListener;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mDropListener:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$OnDropListener;

    .line 143
    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/widget/AdapterView$OnItemClickListener;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mUserItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mLocalItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-super {p0, v0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 156
    return-void
.end method

.method public setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/widget/AdapterView$OnItemLongClickListener;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mUserLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mLocalLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    invoke-super {p0, v0}, Landroid/widget/GridView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 150
    return-void
.end method

.method public touchEventsEnded()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 406
    iget-wide v1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mMobileItemId:J

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getViewForId(J)Landroid/view/View;

    move-result-object v0

    .line 407
    .local v0, "mobileView":Landroid/view/View;
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mCellIsMobile:Z

    if-eqz v1, :cond_1

    .line 408
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mCellIsMobile:Z

    .line 410
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mActivePointerId:I

    .line 413
    const v1, 0x7f0804f2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 416
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mHoverCellCurrentBounds:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 418
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-le v1, v2, :cond_0

    .line 419
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->animateBounds(Landroid/view/View;)V

    .line 428
    :goto_0
    return-void

    .line 421
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mHoverCell:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mHoverCellCurrentBounds:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 422
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->invalidate()V

    .line 423
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->reset(Landroid/view/View;)V

    goto :goto_0

    .line 426
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->touchEventsCancelled()V

    goto :goto_0
.end method
