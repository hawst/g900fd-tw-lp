.class Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$4;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$4;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 2

    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$4;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->settingsSensorDeviceDetails:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->access$300(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$4;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->rename()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$4;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$4;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mRename:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->access$500(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->accessary_name:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->access$402(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$4;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mNameView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->access$600(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$4;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->accessary_name:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->access$400(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$4;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->hideSIP()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->access$700(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)V

    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$4;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->hideSIP()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->access$700(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)V

    :cond_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
