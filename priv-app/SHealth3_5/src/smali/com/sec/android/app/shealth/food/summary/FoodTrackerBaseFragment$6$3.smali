.class Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$3;
.super Ljava/lang/Object;
.source "FoodTrackerBaseFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/ListPopup$OnItemClickedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->showMealTypeSelectorPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;)V
    .locals 0

    .prologue
    .line 1076
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$3;->this$1:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClicked(ILjava/lang/String;Landroid/widget/PopupWindow;)V
    .locals 4
    .param p1, "itemIndex"    # I
    .param p2, "itemContent"    # Ljava/lang/String;
    .param p3, "popupWindow"    # Landroid/widget/PopupWindow;

    .prologue
    .line 1081
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$3;->this$1:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$3;->this$1:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1082
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$3;->this$1:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$1700(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->dismiss()V

    .line 1083
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$3;->this$1:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealTypeSelector:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$1100(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1085
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$3;->this$1:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealTypeSelectorLL:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$1200(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$3;->this$1:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;

    iget-object v1, v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealTypeSelector:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$1100(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$3;->this$1:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;

    iget-object v2, v2, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901ec

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 1086
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$3;->this$1:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->TYPES_MAP:[I
    invoke-static {}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$1900()[I

    move-result-object v1

    aget v1, v1, p1

    # setter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealType:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$802(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;I)I

    .line 1087
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$3;->this$1:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$1700(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setCurrentItem(I)V

    .line 1088
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$3;->this$1:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mQuickInputPanel:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$3;->this$1:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;

    iget-object v1, v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealType:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$800(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->updateCalorieValues(I)V

    .line 1089
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$3;->this$1:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$100(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_CHANGE_MEAL_TYPE:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 1091
    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$400()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "meal type chosen: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1094
    :cond_0
    return-void
.end method
