.class Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$3;
.super Ljava/lang/Object;
.source "UvSkinTypeDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->initLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$3;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 184
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$3;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinSettings:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->access$000(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    .line 186
    .local v1, "extra":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$3;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->access$400(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 187
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v2, "skin_type_checked"

    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$3;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinSettings:Landroid/widget/CheckBox;
    invoke-static {v4}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->access$000(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)Landroid/widget/CheckBox;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 188
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 189
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$3;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinSettings:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->access$000(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-nez v2, :cond_1

    .line 190
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$3;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    iget-object v2, v2, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;

    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$3;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    iget-object v4, v4, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->getSelectedSkinType()Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINTYPE;->getValue()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$3;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    iget-object v5, v5, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->getSelectedSkinOption()Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->getValue()I

    move-result v5

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;->insertData(II)V

    .line 195
    :goto_1
    new-instance v2, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$BroadcastSendingTask;

    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$3;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    invoke-direct {v2, v4}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$BroadcastSendingTask;-><init>(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)V

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$BroadcastSendingTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 196
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$3;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->dismiss()V

    .line 197
    return-void

    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "extra":Ljava/lang/String;
    :cond_0
    move v2, v3

    .line 184
    goto :goto_0

    .line 193
    .restart local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .restart local v1    # "extra":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$3;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    iget-object v2, v2, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;->clearData()Z

    goto :goto_1
.end method
