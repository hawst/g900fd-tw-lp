.class public Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;
.super Ljava/lang/Object;
.source "CompleteGoalMissionInfoData.java"


# instance fields
.field private mGoalCompeleteCount:I

.field private mMissionCompleteCount:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getGoalCompeleteCount()I
    .locals 1

    .prologue
    .line 9
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;->mGoalCompeleteCount:I

    return v0
.end method

.method public getMissionCompleteCount()I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;->mMissionCompleteCount:I

    return v0
.end method

.method public setGoalCompeleteCount(I)V
    .locals 0
    .param p1, "goalCompeleteCount"    # I

    .prologue
    .line 13
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;->mGoalCompeleteCount:I

    .line 14
    return-void
.end method

.method public setMissionCompleteCount(I)V
    .locals 0
    .param p1, "missionCompleteCount"    # I

    .prologue
    .line 21
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;->mMissionCompleteCount:I

    .line 22
    return-void
.end method
