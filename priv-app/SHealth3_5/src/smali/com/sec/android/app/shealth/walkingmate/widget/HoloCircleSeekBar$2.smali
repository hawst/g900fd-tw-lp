.class Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$2;
.super Ljava/lang/Object;
.source "HoloCircleSeekBar.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->doUiChanged()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;)V
    .locals 0

    .prologue
    .line 369
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/high16 v3, 0x43b40000    # 360.0f

    .line 371
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->isTimerTaskRunning:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->access$000(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 389
    :goto_0
    return-void

    .line 373
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mCurrentAniValue:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->access$100(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mCurrentValue:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->access$200(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;)I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 374
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mBackupValue:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->access$300(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;)I

    move-result v0

    if-ltz v0, :cond_1

    .line 375
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mBackupValue:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->access$300(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->access$500(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v3

    float-to-int v1, v1

    # setter for: Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mArcFinishRadians:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->access$402(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;I)I

    .line 379
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mArcFinishRadians:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->access$400(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;)I

    move-result v2

    # invokes: Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->calculateAngleFromRadians(I)F
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->access$700(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;I)F

    move-result v1

    # setter for: Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mAngle:F
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->access$602(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;F)F

    .line 380
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->invalidate()V

    .line 381
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->stopTask()V

    goto :goto_0

    .line 377
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mCurrentValue:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->access$200(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->access$500(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v3

    float-to-int v1, v1

    # setter for: Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mArcFinishRadians:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->access$402(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;I)I

    goto :goto_1

    .line 384
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    const/4 v1, 0x2

    # += operator for: Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mCurrentAniValue:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->access$112(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;I)I

    .line 385
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mCurrentAniValue:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->access$100(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->access$500(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v3

    float-to-int v1, v1

    # setter for: Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mArcFinishRadians:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->access$402(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;I)I

    .line 386
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mArcFinishRadians:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->access$400(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;)I

    move-result v2

    # invokes: Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->calculateAngleFromRadians(I)F
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->access$700(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;I)F

    move-result v1

    # setter for: Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->mAngle:F
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->access$602(Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;F)F

    .line 387
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/widget/HoloCircleSeekBar;->invalidate()V

    goto/16 :goto_0
.end method
