.class public Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;
.super Ljava/lang/Object;


# instance fields
.field private mIsGoalAchieved:Z

.field private mMedalResourceId:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;->mIsGoalAchieved:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;->mMedalResourceId:I

    return-void
.end method


# virtual methods
.method public getMedalResourceId()I
    .locals 1

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;->mMedalResourceId:I

    return v0
.end method

.method public isGoal()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;->mIsGoalAchieved:Z

    return v0
.end method

.method public setGoalAchieved(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;->mIsGoalAchieved:Z

    return-void
.end method

.method public setMedalResourceId(I)V
    .locals 0

    iput p1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;->mMedalResourceId:I

    return-void
.end method
