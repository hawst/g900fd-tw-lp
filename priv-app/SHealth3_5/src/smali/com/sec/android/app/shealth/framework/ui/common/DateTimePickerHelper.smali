.class public Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;
.super Ljava/lang/Object;


# static fields
.field public static final DATE_PICKER_DIALOG:Ljava/lang/String; = "date_picker_dialog"

.field public static final TIME_PICKER_DIALOG:Ljava/lang/String; = "time_picker_dialog"


# instance fields
.field private context:Landroid/content/Context;

.field private datePicker:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;

.field private parentView:Landroid/view/View;

.field private timePicker:Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->btn_date:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-direct {v1, v0}, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;-><init>(Landroid/widget/Button;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->datePicker:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->btn_time:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-direct {v1, v0}, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;-><init>(Landroid/widget/Button;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->timePicker:Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->context:Landroid/content/Context;

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->parentView:Landroid/view/View;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->init()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->hideKeyboard()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;)Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->timePicker:Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;)Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->datePicker:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;

    return-object v0
.end method

.method private hideKeyboard()V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->context:Landroid/content/Context;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->parentView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    :cond_0
    return-void
.end method

.method private init()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->datePicker:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->setOnBeforeClickListener(Lcom/sec/android/app/shealth/framework/ui/common/BasePicker$OnBeforeClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->timePicker:Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper$2;-><init>(Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;->setOnBeforeClickListener(Lcom/sec/android/app/shealth/framework/ui/common/BasePicker$OnBeforeClickListener;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->refreshDateTime()V

    return-void
.end method


# virtual methods
.method public getDateDialog()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->datePicker:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->getDialog()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public getMeasureTimeInMillis()J
    .locals 5

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->datePicker:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->timePicker:Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v4, 0x5

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/Calendar;->set(III)V

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTimeDialog()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->timePicker:Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;->getDialog()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->datePicker:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->onRestoreInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->timePicker:Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;->onRestoreInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->datePicker:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->timePicker:Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public refreshDateTime()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->datePicker:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->refresh()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->timePicker:Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;->refresh()V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->datePicker:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->setEnabled(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->timePicker:Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;->setEnabled(Z)V

    return-void
.end method

.method public setMeasureDateWithErrorCheck(III)V
    .locals 4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->getMeasureTimeInMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->set(II)V

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p3}, Ljava/util/Calendar;->set(II)V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->datePicker:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->setMeasureTimeWithErrorCheck(J)Z

    return-void
.end method

.method public setMeasureDateWithErrorCheck(Ljava/util/Calendar;III)V
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p1, v0, p2}, Ljava/util/Calendar;->set(II)V

    const/4 v0, 0x2

    invoke-virtual {p1, v0, p3}, Ljava/util/Calendar;->set(II)V

    const/4 v0, 0x5

    invoke-virtual {p1, v0, p4}, Ljava/util/Calendar;->set(II)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->datePicker:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->setMeasureTimeWithErrorCheck(J)Z

    return-void
.end method

.method public setMeasureTime(J)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->datePicker:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->setTimeInMillis(J)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->timePicker:Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;->setTimeInMillis(J)V

    return-void
.end method

.method public setMeasureTimeWithErrorCheck(II)V
    .locals 4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->getMeasureTimeInMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/16 v1, 0xb

    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xc

    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->timePicker:Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;->setMeasureTimeWithErrorCheck(J)Z

    return-void
.end method

.method public setMeasureTimeWithErrorCheck(Ljava/util/Calendar;II)V
    .locals 3

    const/16 v0, 0xb

    invoke-virtual {p1, v0, p2}, Ljava/util/Calendar;->set(II)V

    const/16 v0, 0xc

    invoke-virtual {p1, v0, p3}, Ljava/util/Calendar;->set(II)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->timePicker:Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;->setMeasureTimeWithErrorCheck(J)Z

    return-void
.end method
