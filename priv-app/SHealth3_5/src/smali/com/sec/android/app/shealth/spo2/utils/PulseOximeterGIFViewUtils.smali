.class public Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;
.super Landroid/view/View;
.source "PulseOximeterGIFViewUtils.java"


# static fields
.field private static final CACHE_SIZE:I = 0x100000

.field private static final DEFAULT_MOVIEW_DURATION:I = 0x3e8

.field private static final MOVIE_TAG:Ljava/lang/String; = "movie"

.field private static sMovieCache:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Movie;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCurrentAnimationTime:I

.field private mLeft:F

.field private mMeasuredMovieHeight:I

.field private mMeasuredMovieWidth:I

.field private mMovie:Landroid/graphics/Movie;

.field private mMovieResourceId:I

.field private mMovieStart:J

.field private volatile mPaused:Z

.field private volatile mRestart:Z

.field private mScale:F

.field private mTop:F

.field private mVisible:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 48
    new-instance v0, Landroid/support/v4/util/LruCache;

    const/high16 v1, 0x100000

    invoke-direct {v0, v1}, Landroid/support/v4/util/LruCache;-><init>(I)V

    sput-object v0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->sMovieCache:Landroid/support/v4/util/LruCache;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 62
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    iput v1, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mCurrentAnimationTime:I

    .line 49
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mPaused:Z

    .line 50
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mRestart:Z

    .line 51
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mVisible:Z

    .line 66
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->setViewAttributes(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    :goto_0
    return-void

    .line 68
    :catch_0
    move-exception v0

    .line 71
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private drawMovieFrame(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 299
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mMovie:Landroid/graphics/Movie;

    iget v1, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mCurrentAnimationTime:I

    invoke-virtual {v0, v1}, Landroid/graphics/Movie;->setTime(I)Z

    .line 301
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->save(I)I

    .line 302
    iget v0, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mScale:F

    iget v1, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mScale:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mMovie:Landroid/graphics/Movie;

    iget v1, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mLeft:F

    iget v2, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mScale:F

    div-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mTop:F

    iget v3, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mScale:F

    div-float/2addr v2, v3

    invoke-virtual {v0, p1, v1, v2}, Landroid/graphics/Movie;->draw(Landroid/graphics/Canvas;FF)V

    .line 304
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 305
    return-void
.end method

.method private invalidateView()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 265
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mVisible:Z

    if-eqz v0, :cond_0

    .line 266
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    .line 267
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->postInvalidateOnAnimation()V

    .line 272
    :cond_0
    :goto_0
    return-void

    .line 269
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->invalidate()V

    goto :goto_0
.end method

.method private setViewAttributes(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_0

    .line 83
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->setLayerType(ILandroid/graphics/Paint;)V

    .line 87
    :cond_0
    iget v2, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mMovieResourceId:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    .line 88
    sget-object v2, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->sMovieCache:Landroid/support/v4/util/LruCache;

    const-string/jumbo v3, "movie"

    invoke-virtual {v2, v3}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_2

    .line 90
    const/4 v1, 0x0

    .line 93
    .local v1, "in":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020757

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 94
    invoke-static {v1}, Landroid/graphics/Movie;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Movie;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mMovie:Landroid/graphics/Movie;

    .line 95
    sget-object v2, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->sMovieCache:Landroid/support/v4/util/LruCache;

    const-string/jumbo v3, "movie"

    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mMovie:Landroid/graphics/Movie;

    invoke-virtual {v2, v3, v4}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 117
    .end local v1    # "in":Ljava/io/InputStream;
    :cond_1
    :goto_0
    return-void

    .line 104
    .restart local v1    # "in":Ljava/io/InputStream;
    :catch_0
    move-exception v0

    .line 107
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 100
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    .line 102
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 108
    :goto_1
    throw v2

    .line 104
    :catch_1
    move-exception v0

    .line 107
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 113
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "in":Ljava/io/InputStream;
    :cond_2
    sget-object v2, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->sMovieCache:Landroid/support/v4/util/LruCache;

    const-string/jumbo v3, "movie"

    invoke-virtual {v2, v3}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Movie;

    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mMovie:Landroid/graphics/Movie;

    goto :goto_0
.end method

.method private updateAnimationTime()V
    .locals 7

    .prologue
    .line 278
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    .line 280
    .local v1, "now":J
    iget-wide v3, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mMovieStart:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-nez v3, :cond_0

    .line 281
    iput-wide v1, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mMovieStart:J

    .line 284
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mMovie:Landroid/graphics/Movie;

    invoke-virtual {v3}, Landroid/graphics/Movie;->duration()I

    move-result v0

    .line 286
    .local v0, "dur":I
    if-nez v0, :cond_1

    .line 287
    const/16 v0, 0x3e8

    .line 290
    :cond_1
    iget-wide v3, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mMovieStart:J

    sub-long v3, v1, v3

    int-to-long v5, v0

    rem-long/2addr v3, v5

    long-to-int v3, v3

    iput v3, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mCurrentAnimationTime:I

    .line 291
    return-void
.end method


# virtual methods
.method public getMovie()Landroid/graphics/Movie;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mMovie:Landroid/graphics/Movie;

    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v1, 0x0

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mMovie:Landroid/graphics/Movie;

    if-eqz v0, :cond_0

    .line 240
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mRestart:Z

    if-eqz v0, :cond_1

    .line 241
    iput v1, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mCurrentAnimationTime:I

    .line 242
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->invalidateView()V

    .line 243
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mRestart:Z

    .line 255
    :cond_0
    :goto_0
    return-void

    .line 247
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mPaused:Z

    if-nez v0, :cond_2

    .line 248
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->updateAnimationTime()V

    .line 249
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->drawMovieFrame(Landroid/graphics/Canvas;)V

    .line 250
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->invalidateView()V

    goto :goto_0

    .line 252
    :cond_2
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->drawMovieFrame(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 225
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 230
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mMeasuredMovieWidth:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr v0, v2

    iput v0, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mLeft:F

    .line 231
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->getHeight()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mMeasuredMovieHeight:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr v0, v2

    iput v0, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mTop:F

    .line 233
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mVisible:Z

    .line 234
    return-void

    .line 233
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 10
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 175
    iget-object v8, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mMovie:Landroid/graphics/Movie;

    if-eqz v8, :cond_2

    .line 176
    iget-object v8, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mMovie:Landroid/graphics/Movie;

    invoke-virtual {v8}, Landroid/graphics/Movie;->width()I

    move-result v5

    .line 177
    .local v5, "movieWidth":I
    iget-object v8, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mMovie:Landroid/graphics/Movie;

    invoke-virtual {v8}, Landroid/graphics/Movie;->height()I

    move-result v4

    .line 182
    .local v4, "movieHeight":I
    const/high16 v6, 0x3f800000    # 1.0f

    .line 183
    .local v6, "scaleH":F
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 185
    .local v3, "measureModeWidth":I
    if-eqz v3, :cond_0

    .line 186
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 187
    .local v1, "maximumWidth":I
    if-le v5, v1, :cond_0

    .line 188
    int-to-float v8, v5

    int-to-float v9, v1

    div-float v6, v8, v9

    .line 195
    .end local v1    # "maximumWidth":I
    :cond_0
    const/high16 v7, 0x3f800000    # 1.0f

    .line 196
    .local v7, "scaleW":F
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 198
    .local v2, "measureModeHeight":I
    if-eqz v2, :cond_1

    .line 199
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 200
    .local v0, "maximumHeight":I
    if-le v4, v0, :cond_1

    .line 201
    int-to-float v8, v4

    int-to-float v9, v0

    div-float v7, v8, v9

    .line 208
    .end local v0    # "maximumHeight":I
    :cond_1
    const/high16 v8, 0x3f800000    # 1.0f

    invoke-static {v6, v7}, Ljava/lang/Math;->max(FF)F

    move-result v9

    div-float/2addr v8, v9

    iput v8, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mScale:F

    .line 210
    int-to-float v8, v5

    iget v9, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mScale:F

    mul-float/2addr v8, v9

    float-to-int v8, v8

    iput v8, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mMeasuredMovieWidth:I

    .line 211
    int-to-float v8, v4

    iget v9, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mScale:F

    mul-float/2addr v8, v9

    float-to-int v8, v8

    iput v8, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mMeasuredMovieHeight:I

    .line 213
    iget v8, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mMeasuredMovieWidth:I

    iget v9, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mMeasuredMovieHeight:I

    invoke-virtual {p0, v8, v9}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->setMeasuredDimension(II)V

    .line 221
    .end local v2    # "measureModeHeight":I
    .end local v3    # "measureModeWidth":I
    .end local v4    # "movieHeight":I
    .end local v5    # "movieWidth":I
    .end local v6    # "scaleH":F
    .end local v7    # "scaleW":F
    :goto_0
    return-void

    .line 219
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->getSuggestedMinimumWidth()I

    move-result v8

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->getSuggestedMinimumHeight()I

    move-result v9

    invoke-virtual {p0, v8, v9}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public onScreenStateChanged(I)V
    .locals 1
    .param p1, "screenState"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 310
    invoke-super {p0, p1}, Landroid/view/View;->onScreenStateChanged(I)V

    .line 311
    if-ne p1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mVisible:Z

    .line 312
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->invalidateView()V

    .line 313
    return-void

    .line 311
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 1
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 318
    invoke-super {p0, p1, p2}, Landroid/view/View;->onVisibilityChanged(Landroid/view/View;I)V

    .line 319
    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mVisible:Z

    .line 320
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->invalidateView()V

    .line 321
    return-void

    .line 319
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 325
    invoke-super {p0, p1}, Landroid/view/View;->onWindowVisibilityChanged(I)V

    .line 326
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mVisible:Z

    .line 327
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->invalidateView()V

    .line 328
    return-void

    .line 326
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public restartAnimation()V
    .locals 1

    .prologue
    .line 331
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mRestart:Z

    .line 332
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->invalidate()V

    .line 333
    return-void
.end method

.method public setMovie(Landroid/graphics/Movie;)V
    .locals 0
    .param p1, "movie"    # Landroid/graphics/Movie;

    .prologue
    .line 151
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mMovie:Landroid/graphics/Movie;

    .line 152
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->requestLayout()V

    .line 153
    return-void
.end method

.method public setMovieResource(I)V
    .locals 4
    .param p1, "movieResId"    # I

    .prologue
    .line 124
    iput p1, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mMovieResourceId:I

    .line 125
    const/4 v1, 0x0

    .line 128
    .local v1, "in":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mMovieResourceId:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 129
    invoke-static {v1}, Landroid/graphics/Movie;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Movie;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mMovie:Landroid/graphics/Movie;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 143
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->requestLayout()V

    .line 144
    return-void

    .line 138
    :catch_0
    move-exception v0

    .line 140
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 134
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    .line 136
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 141
    :goto_1
    throw v2

    .line 138
    :catch_1
    move-exception v0

    .line 140
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public setMovieTime(I)V
    .locals 0
    .param p1, "time"    # I

    .prologue
    .line 168
    iput p1, p0, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->mCurrentAnimationTime:I

    .line 169
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->invalidate()V

    .line 170
    return-void
.end method
