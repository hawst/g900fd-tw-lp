.class Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$5;
.super Ljava/lang/Object;
.source "RealtimeHealthService.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V
    .locals 0

    .prologue
    .line 762
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$5;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeviceFound(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V
    .locals 2
    .param p1, "device"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .prologue
    .line 766
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$5;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHRMListListener:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 767
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$5;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHRMListListener:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;->onDeviceFound(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V

    .line 768
    :cond_0
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "mHRMScanListener - onDeviceFound"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 769
    return-void
.end method

.method public onStarted(I)V
    .locals 2
    .param p1, "error"    # I

    .prologue
    .line 773
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$5;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHRMListListener:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 774
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$5;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHRMListListener:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;->onStarted(I)V

    .line 775
    :cond_0
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "mHRMScanListener - onStarted"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 776
    return-void
.end method

.method public onStopped(I)V
    .locals 2
    .param p1, "error"    # I

    .prologue
    .line 780
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$5;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHRMListListener:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 781
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$5;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHRMListListener:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;->onStopped(I)V

    .line 782
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$5;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mHRMListListener:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$1302(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;)Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$realtimeHRMListener;

    .line 784
    :cond_0
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "mHRMScanListener - onStopped"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 785
    return-void
.end method
