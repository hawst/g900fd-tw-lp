.class public Lcom/sec/android/app/shealth/stress/log/StressInputActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;
.source "StressInputActivity.java"


# instance fields
.field private mComment:Ljava/lang/String;

.field private mId:J

.field private mStressScore:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getContentView()Landroid/view/View;
    .locals 5

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/log/StressInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "HEART_RATE_ID_KEY"

    const-wide/16 v3, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/android/app/shealth/stress/log/StressInputActivity;->mId:J

    .line 63
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/log/StressInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "MODE_KEY"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/log/StressInputActivity;->mMode:Ljava/lang/String;

    .line 64
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/log/StressInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "COMMENT_KEY"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/log/StressInputActivity;->mComment:Ljava/lang/String;

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/log/StressInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "SCORE_KEY"

    const-wide/16 v3, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/android/app/shealth/stress/log/StressInputActivity;->mStressScore:D

    .line 66
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030231

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 67
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f0809d0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/stress/custom/StressStateDetailBar;

    iget-wide v2, p0, Lcom/sec/android/app/shealth/stress/log/StressInputActivity;->mStressScore:D

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/stress/custom/StressStateDetailBar;->moveToPolygon(D)V

    .line 68
    const v1, 0x7f0809cf

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/log/StressInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "TIME_DATE_KEY"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    return-object v0
.end method

.method protected isInputChanged()Z
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/log/StressInputActivity;->mComment:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/log/StressInputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 28
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->onCreate(Landroid/os/Bundle;)V

    .line 30
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v1

    if-nez v1, :cond_0

    .line 32
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.shealth.SplashScreenActivity.restart"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 34
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "temps"

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->getRandomKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 35
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 36
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 37
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 38
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/stress/log/StressInputActivity;->startActivity(Landroid/content/Intent;)V

    .line 40
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-static {v1}, Landroid/os/Process;->killProcess(I)V

    .line 43
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    const v1, 0x7f0803e8

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/stress/log/StressInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 44
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/log/StressInputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/log/StressInputActivity;->mComment:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setText(Ljava/lang/CharSequence;)V

    .line 45
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/log/StressInputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    new-instance v2, Lcom/sec/android/app/shealth/stress/log/StressInputActivity$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/stress/log/StressInputActivity$1;-><init>(Lcom/sec/android/app/shealth/stress/log/StressInputActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 57
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/log/StressInputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/log/StressInputActivity;->mComment:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setSelection(I)V

    .line 58
    return-void
.end method

.method protected onSaveButtonSelect(Z)V
    .locals 4
    .param p1, "isAdd"    # Z

    .prologue
    .line 75
    invoke-static {p0}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/app/shealth/stress/log/StressInputActivity;->mId:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/log/StressInputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->updateCommentById(Ljava/lang/String;Ljava/lang/String;)Z

    .line 76
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/log/StressInputActivity;->isInputChanged()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/log/StressInputActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/log/StressInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090835

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 79
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 80
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 81
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/stress/log/StressInputActivity;->startActivity(Landroid/content/Intent;)V

    .line 82
    return-void
.end method
