.class public abstract enum Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;
.super Ljava/lang/Enum;
.source "DartVaderSpaceShipView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4409
    name = "DartVaderState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

.field public static final enum NO_STATE:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

.field public static final enum STATE_1:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

.field public static final enum STATE_2:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

.field public static final enum STATE_3:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

.field public static final enum STATE_4:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

.field public static final enum STATE_5:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 118
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState$1;

    const-string v1, "STATE_1"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState$1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->STATE_1:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    .line 129
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState$2;

    const-string v1, "STATE_2"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState$2;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->STATE_2:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    .line 140
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState$3;

    const-string v1, "STATE_3"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState$3;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->STATE_3:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    .line 151
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState$4;

    const-string v1, "STATE_4"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState$4;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->STATE_4:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    .line 162
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState$5;

    const-string v1, "STATE_5"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState$5;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->STATE_5:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    .line 173
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState$6;

    const-string v1, "NO_STATE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState$6;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->NO_STATE:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    .line 117
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->STATE_1:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->STATE_2:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->STATE_3:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->STATE_4:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->STATE_5:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->NO_STATE:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->$VALUES:[Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 117
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # I
    .param p3, "x2"    # Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$1;

    .prologue
    .line 117
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getDartVaderState(I)Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;
    .locals 1
    .param p0, "position"    # I

    .prologue
    .line 209
    packed-switch p0, :pswitch_data_0

    .line 221
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->NO_STATE:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    :goto_0
    return-object v0

    .line 211
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->STATE_1:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    goto :goto_0

    .line 213
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->STATE_2:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    goto :goto_0

    .line 215
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->STATE_3:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    goto :goto_0

    .line 217
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->STATE_4:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    goto :goto_0

    .line 219
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->STATE_5:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    goto :goto_0

    .line 209
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static getRandomState()Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;
    .locals 5

    .prologue
    .line 196
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v1

    double-to-float v0, v1

    .line 197
    .local v0, "r":F
    float-to-double v1, v0

    const-wide v3, 0x3fe999999999999aL    # 0.8

    cmpl-double v1, v1, v3

    if-lez v1, :cond_0

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->STATE_1:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    .line 201
    :goto_0
    return-object v1

    .line 198
    :cond_0
    float-to-double v1, v0

    const-wide v3, 0x3fe3333333333333L    # 0.6

    cmpl-double v1, v1, v3

    if-lez v1, :cond_1

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->STATE_2:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    goto :goto_0

    .line 199
    :cond_1
    float-to-double v1, v0

    const-wide v3, 0x3fd999999999999aL    # 0.4

    cmpl-double v1, v1, v3

    if-lez v1, :cond_2

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->STATE_3:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    goto :goto_0

    .line 200
    :cond_2
    float-to-double v1, v0

    const-wide v3, 0x3fc999999999999aL    # 0.2

    cmpl-double v1, v1, v3

    if-lez v1, :cond_3

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->STATE_4:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    goto :goto_0

    .line 201
    :cond_3
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->STATE_5:Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 117
    const-class v0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;
    .locals 1

    .prologue
    .line 117
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->$VALUES:[Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;

    return-object v0
.end method


# virtual methods
.method public abstract getIndex()I
.end method

.method public abstract next()Lcom/sec/android/app/shealth/walkingmate/tools/DartVaderSpaceShipView$DartVaderState;
.end method
