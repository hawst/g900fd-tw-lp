.class Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector$2;
.super Ljava/lang/Object;
.source "SpO2DeviceConnector.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;)V
    .locals 0

    .prologue
    .line 360
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(I)V
    .locals 2
    .param p1, "error"    # I

    .prologue
    .line 393
    # getter for: Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Binding is done - Service connected"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    # invokes: Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->startDevice()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->access$400(Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;)V

    .line 395
    return-void
.end method

.method public onServiceDisconnected(I)V
    .locals 3
    .param p1, "error"    # I

    .prologue
    .line 363
    # getter for: Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Binding - Service disconnected"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->access$100(Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 366
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->access$200(Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3

    move-result v1

    if-eqz v1, :cond_0

    .line 368
    :try_start_1
    # getter for: Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "stopReceivingData_3"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->access$100(Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->stopReceivingData()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_3

    .line 377
    :cond_0
    :goto_0
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->access$202(Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;Z)Z

    .line 378
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->access$100(Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->leave()V

    .line 379
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->access$102(Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_2 .. :try_end_2} :catch_3

    .line 387
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->access$300(Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 388
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector$2;->this$0:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->access$300(Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->close()V

    .line 389
    :cond_2
    return-void

    .line 370
    :catch_0
    move-exception v0

    .line 371
    .local v0, "e":Ljava/lang/IllegalStateException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    .line 381
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 382
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 372
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_2
    move-exception v0

    .line 374
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :try_start_4
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_0

    .line 383
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :catch_3
    move-exception v0

    .line 384
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_1
.end method
