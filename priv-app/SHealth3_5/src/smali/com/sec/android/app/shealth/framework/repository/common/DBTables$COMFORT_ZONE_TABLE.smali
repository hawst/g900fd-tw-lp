.class public Lcom/sec/android/app/shealth/framework/repository/common/DBTables$COMFORT_ZONE_TABLE;
.super Ljava/lang/Object;
.source "DBTables.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/repository/common/DBTables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "COMFORT_ZONE_TABLE"
.end annotation


# static fields
.field public static final KEY_CREATE_TIME:Ljava/lang/String; = "create_time"

.field public static final KEY_DATA_HUMIDITY:Ljava/lang/String; = "data_humidity"

.field public static final KEY_DATA_TEMPERATURE:Ljava/lang/String; = "data_temperature"

.field public static final KEY_PERIOD_DAY_START:Ljava/lang/String; = "day_st"

.field public static final KEY_PERIOD_MONTH_START:Ljava/lang/String; = "month_st"

.field public static final KEY_PERIOD_WEEK_START_MON:Ljava/lang/String; = "week_st_mon"

.field public static final KEY_PERIOD_WEEK_START_SUN:Ljava/lang/String; = "week_st_sun"

.field public static final KEY_PERIOD_YEAR_START:Ljava/lang/String; = "year_st"

.field public static final KEY_ROWID:Ljava/lang/String; = "_id"

.field public static final TABLE_NAME:Ljava/lang/String; = "comfort_zone"

.field public static final TRIGGER_INSERT_NAME:Ljava/lang/String; = "create_comfort_zone_trigger"

.field public static final TRIGGER_UPDATE_NAME:Ljava/lang/String; = "update_comfort_zone_trigger"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
