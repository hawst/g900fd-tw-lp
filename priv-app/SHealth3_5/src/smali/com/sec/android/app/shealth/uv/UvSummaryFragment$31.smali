.class Lcom/sec/android/app/shealth/uv/UvSummaryFragment$31;
.super Ljava/lang/Object;
.source "UvSummaryFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->showRecommendedDialog(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

.field final synthetic val$isChinaModel:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;Z)V
    .locals 0

    .prologue
    .line 2840
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$31;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iput-boolean p2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$31;->val$isChinaModel:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 7
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    const/4 v6, 0x1

    .line 2847
    invoke-virtual {p5, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;->setEnabled(Z)V

    .line 2848
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$31;->val$isChinaModel:Z

    if-nez v1, :cond_0

    .line 2849
    const v1, 0x7f080aa6

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2851
    .local v0, "message":Landroid/widget/TextView;
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$31;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$900(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090de0

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/16 v5, 0xa

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2858
    .end local v0    # "message":Landroid/widget/TextView;
    :cond_0
    return-void
.end method
