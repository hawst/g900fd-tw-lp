.class public Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelectorImpl;
.super Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;
.source "PageSelectorImpl.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;-><init>(Landroid/content/Context;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/PageSelector;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method


# virtual methods
.method protected getMarginBetweenIndicators(Landroid/content/res/Resources;)I
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 48
    const v0, 0x7f0a0713

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method protected getPageIndicatorHeight(Landroid/content/res/Resources;)I
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 43
    const v0, 0x7f0a0712

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method protected getPageIndicatorImageResource()I
    .locals 1

    .prologue
    .line 53
    const v0, 0x7f02019d

    return v0
.end method

.method protected getPageIndicatorWidth(Landroid/content/res/Resources;)I
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 38
    const v0, 0x7f0a0711

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method
