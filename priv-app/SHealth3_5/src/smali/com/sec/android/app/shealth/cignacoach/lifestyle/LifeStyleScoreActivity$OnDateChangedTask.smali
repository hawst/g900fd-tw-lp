.class Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;
.super Landroid/os/AsyncTask;
.source "LifeStyleScoreActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "OnDateChangedTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/cigna/coach/apiobjects/LifeStyleScores;",
        ">;>;"
    }
.end annotation


# instance fields
.field private mTempIsTimeToReassess:Z

.field private mTempReassessMssage:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;)V
    .locals 1

    .prologue
    .line 394
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 395
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;->mTempIsTimeToReassess:Z

    .line 396
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;->mTempReassessMssage:Ljava/lang/String;

    return-void
.end method

.method private updateListData(Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;)Ljava/util/List;
    .locals 8
    .param p1, "periodType"    # Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/LifeStyleScores;",
            ">;"
        }
    .end annotation

    .prologue
    .line 400
    const/4 v3, 0x0

    .line 402
    .local v3, "lss":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/LifeStyleScores;>;"
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getScoreStatus(Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;)Lcom/cigna/coach/apiobjects/CoachResponse;

    move-result-object v0

    .line 403
    .local v0, "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/ScoreInfo;>;"
    if-eqz v0, :cond_2

    .line 404
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cigna/coach/apiobjects/ScoreInfo;

    invoke-virtual {v5}, Lcom/cigna/coach/apiobjects/ScoreInfo;->isTimeToReassess()Z

    move-result v5

    iput-boolean v5, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;->mTempIsTimeToReassess:Z

    .line 405
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cigna/coach/apiobjects/ScoreInfo;

    invoke-virtual {v5}, Lcom/cigna/coach/apiobjects/ScoreInfo;->getLssList()Ljava/util/List;

    move-result-object v3

    .line 407
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 409
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    .line 410
    .local v4, "msgListSize":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v4, :cond_2

    .line 411
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v5}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageType()Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-result-object v5

    sget-object v6, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->REASSESS_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    if-ne v5, v6, :cond_0

    .line 412
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v5}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageDescription()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;->mTempReassessMssage:Ljava/lang/String;

    .line 410
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 416
    .end local v2    # "i":I
    .end local v4    # "msgListSize":I
    :cond_1
    # getter for: Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->access$000()Ljava/lang/String;

    move-result-object v5

    const-string v6, "coach msg is null"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 423
    .end local v0    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/ScoreInfo;>;"
    :cond_2
    :goto_1
    # getter for: Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->access$000()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "mTempIsTimeToReassess : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;->mTempIsTimeToReassess:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    return-object v3

    .line 419
    :catch_0
    move-exception v1

    .line 421
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 394
    check-cast p1, [Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;->doInBackground([Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;)Ljava/util/List;
    .locals 1
    .param p1, "periodType"    # [Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/LifeStyleScores;",
            ">;"
        }
    .end annotation

    .prologue
    .line 430
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 431
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;->updateListData(Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;)Ljava/util/List;

    move-result-object v0

    .line 433
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 394
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/LifeStyleScores;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 439
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/LifeStyleScores;>;"
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 440
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 441
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mInitializeLifeStyleScore:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->access$102(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;Z)Z

    .line 442
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;->mTempIsTimeToReassess:Z

    # setter for: Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mIsTimeToReassess:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->access$202(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;Z)Z

    .line 444
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;->mTempReassessMssage:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 445
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;->mTempReassessMssage:Ljava/lang/String;

    # setter for: Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mReassessMssage:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->access$302(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 447
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->showLifestyleFragment(Ljava/util/List;)V
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->access$400(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;Ljava/util/List;)V

    .line 449
    :cond_1
    return-void
.end method
