.class public Lcom/sec/android/app/shealth/award/medaldata/AwardStepsData;
.super Ljava/lang/Object;
.source "AwardStepsData.java"


# instance fields
.field private sampleTime:Ljava/lang/String;

.field private value:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method


# virtual methods
.method public getSampleTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/medaldata/AwardStepsData;->sampleTime:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/sec/android/app/shealth/award/medaldata/AwardStepsData;->value:I

    return v0
.end method

.method public setSampleTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "sampleTime"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/android/app/shealth/award/medaldata/AwardStepsData;->sampleTime:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public setValue(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 36
    iput p1, p0, Lcom/sec/android/app/shealth/award/medaldata/AwardStepsData;->value:I

    .line 37
    return-void
.end method
