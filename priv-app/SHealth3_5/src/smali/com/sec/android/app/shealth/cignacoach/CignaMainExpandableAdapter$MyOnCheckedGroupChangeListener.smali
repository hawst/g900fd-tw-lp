.class Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedGroupChangeListener;
.super Ljava/lang/Object;
.source "CignaMainExpandableAdapter.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MyOnCheckedGroupChangeListener"
.end annotation


# instance fields
.field goalMissionDelete:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;

.field private mGoalData:Lcom/sec/android/app/shealth/cignacoach/data/GoalData;

.field private mGroupPosition:I

.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;Lcom/sec/android/app/shealth/cignacoach/data/GoalData;I)V
    .locals 2
    .param p2, "goalData"    # Lcom/sec/android/app/shealth/cignacoach/data/GoalData;
    .param p3, "groupPosition"    # I

    .prologue
    .line 460
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedGroupChangeListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 458
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedGroupChangeListener;->mGroupPosition:I

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedGroupChangeListener;->goalMissionDelete:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;

    .line 461
    iput-object p2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedGroupChangeListener;->mGoalData:Lcom/sec/android/app/shealth/cignacoach/data/GoalData;

    .line 462
    iput p3, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedGroupChangeListener;->mGroupPosition:I

    .line 463
    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 469
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedGroupChangeListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mMainType:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->access$200(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;)Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;->DELETE:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    if-ne v0, v1, :cond_0

    .line 471
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedGroupChangeListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mGoalMissionDelete:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->access$300(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;)Ljava/util/HashSet;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 474
    if-eqz p2, :cond_3

    .line 476
    const-string v0, "Goal Selection"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Selected Goal"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedGroupChangeListener;->mGroupPosition:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedGroupChangeListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mGoalMissionDelete:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->access$300(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedGroupChangeListener;->goalMissionDelete:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 493
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedGroupChangeListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mGroupCheckBoxTouch:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->access$100(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 494
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedGroupChangeListener;->mGoalData:Lcom/sec/android/app/shealth/cignacoach/data/GoalData;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->setChecked(Z)V

    .line 495
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedGroupChangeListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedGroupChangeListener;->mGroupPosition:I

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->allChildItemCheckOfGroup(IZ)V

    .line 496
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedGroupChangeListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mGroupCheckBoxTouch:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->access$102(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;Z)Z

    .line 501
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedGroupChangeListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mCheckedCountListener:Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->access$400(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;)Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 502
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedGroupChangeListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mCheckedCountListener:Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->access$400(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;)Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedGroupChangeListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getCheckedItemCount()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;->checkedCheckBoxCountChanged(I)V

    .line 504
    :cond_2
    return-void

    .line 481
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedGroupChangeListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mGoalMissionDelete:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->access$300(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedGroupChangeListener;->goalMissionDelete:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 483
    const-string v0, "Goal Deselection"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Deselected Goal"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedGroupChangeListener;->mGroupPosition:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 484
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedGroupChangeListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->mGoalMissionDelete:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->access$300(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MyOnCheckedGroupChangeListener;->goalMissionDelete:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
