.class public Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;
.super Landroid/widget/RelativeLayout;
.source "IndicatorViewPager.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$2;,
        Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$OnDisableInterceptTouchEventListener;,
        Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$TYPE;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final INDICATOR_NUMBER_HEIGHT:I

.field private final INDICATOR_NUMBER_LEFT_MARGIN:I

.field private final INDICATOR_NUMBER_LINE_HEIGHT:I

.field private final INDICATOR_NUMBER_LINE_LEFT_MARGIN:I

.field private final INDICATOR_NUMBER_LINE_RIGHT_MARGIN:I

.field private final INDICATOR_NUMBER_LINE_WIDTH:I

.field private final INDICATOR_NUMBER_TOP_MARGIN:I

.field private final INDICATOR_NUMBER_WIDTH:I

.field private final INDICATOR_RECTANGLE_HEIGHT:I

.field private final INDICATOR_RECTANGLE_TOP_MARGIN:I

.field private final INDICATOR_RECTANGLE_WIDTH:I

.field private final NUMBER_CHECKED_RES_IDS:I

.field private final NUMBER_TYPE_RES_IDS:[[I

.field private isPagingEnabled:Z

.field private mIndicatorType:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$TYPE;

.field private mNumberIndicatorContainer:Landroid/widget/LinearLayout;

.field private mOnDisableInceptTouchEventListener:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$OnDisableInterceptTouchEventListener;

.field private mOnPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

.field private mRectangleIndicatorContainer:Landroid/widget/LinearLayout;

.field private mTouchDownX:F

.field private mTouchSlop:I

.field private mViewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x2

    .line 70
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 35
    const v0, 0x7f0205f1

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->NUMBER_CHECKED_RES_IDS:I

    .line 36
    const/4 v0, 0x7

    new-array v0, v0, [[I

    const/4 v1, 0x0

    new-array v2, v3, [I

    fill-array-data v2, :array_0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-array v2, v3, [I

    fill-array-data v2, :array_1

    aput-object v2, v0, v1

    new-array v1, v3, [I

    fill-array-data v1, :array_2

    aput-object v1, v0, v3

    const/4 v1, 0x3

    new-array v2, v3, [I

    fill-array-data v2, :array_3

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v3, [I

    fill-array-data v2, :array_4

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v3, [I

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [I

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->NUMBER_TYPE_RES_IDS:[[I

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0228

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_RECTANGLE_WIDTH:I

    .line 47
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0229

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_RECTANGLE_HEIGHT:I

    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0227

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_RECTANGLE_TOP_MARGIN:I

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a022e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_NUMBER_WIDTH:I

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a022f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_NUMBER_HEIGHT:I

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a022a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_NUMBER_TOP_MARGIN:I

    .line 53
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a022b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_NUMBER_LEFT_MARGIN:I

    .line 54
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a022c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_NUMBER_LINE_LEFT_MARGIN:I

    .line 55
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a022d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_NUMBER_LINE_RIGHT_MARGIN:I

    .line 56
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0230

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_NUMBER_LINE_WIDTH:I

    .line 57
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0231

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_NUMBER_LINE_HEIGHT:I

    .line 71
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->init()V

    .line 72
    return-void

    .line 36
    :array_0
    .array-data 4
        0x7f0205f2
        0x7f0205f9
    .end array-data

    :array_1
    .array-data 4
        0x7f0205f3
        0x7f0205fa
    .end array-data

    :array_2
    .array-data 4
        0x7f0205f4
        0x7f0205fb
    .end array-data

    :array_3
    .array-data 4
        0x7f0205f5
        0x7f0205fc
    .end array-data

    :array_4
    .array-data 4
        0x7f0205f6
        0x7f0205fd
    .end array-data

    :array_5
    .array-data 4
        0x7f0205f7
        0x7f0205fe
    .end array-data

    :array_6
    .array-data 4
        0x7f0205f8
        0x7f0205ff
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x2

    .line 75
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    const v0, 0x7f0205f1

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->NUMBER_CHECKED_RES_IDS:I

    .line 36
    const/4 v0, 0x7

    new-array v0, v0, [[I

    const/4 v1, 0x0

    new-array v2, v3, [I

    fill-array-data v2, :array_0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-array v2, v3, [I

    fill-array-data v2, :array_1

    aput-object v2, v0, v1

    new-array v1, v3, [I

    fill-array-data v1, :array_2

    aput-object v1, v0, v3

    const/4 v1, 0x3

    new-array v2, v3, [I

    fill-array-data v2, :array_3

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v3, [I

    fill-array-data v2, :array_4

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v3, [I

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [I

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->NUMBER_TYPE_RES_IDS:[[I

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0228

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_RECTANGLE_WIDTH:I

    .line 47
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0229

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_RECTANGLE_HEIGHT:I

    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0227

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_RECTANGLE_TOP_MARGIN:I

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a022e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_NUMBER_WIDTH:I

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a022f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_NUMBER_HEIGHT:I

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a022a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_NUMBER_TOP_MARGIN:I

    .line 53
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a022b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_NUMBER_LEFT_MARGIN:I

    .line 54
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a022c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_NUMBER_LINE_LEFT_MARGIN:I

    .line 55
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a022d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_NUMBER_LINE_RIGHT_MARGIN:I

    .line 56
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0230

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_NUMBER_LINE_WIDTH:I

    .line 57
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0231

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_NUMBER_LINE_HEIGHT:I

    .line 76
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->init()V

    .line 77
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mTouchSlop:I

    .line 78
    return-void

    .line 36
    :array_0
    .array-data 4
        0x7f0205f2
        0x7f0205f9
    .end array-data

    :array_1
    .array-data 4
        0x7f0205f3
        0x7f0205fa
    .end array-data

    :array_2
    .array-data 4
        0x7f0205f4
        0x7f0205fb
    .end array-data

    :array_3
    .array-data 4
        0x7f0205f5
        0x7f0205fc
    .end array-data

    :array_4
    .array-data 4
        0x7f0205f6
        0x7f0205fd
    .end array-data

    :array_5
    .array-data 4
        0x7f0205f7
        0x7f0205fe
    .end array-data

    :array_6
    .array-data 4
        0x7f0205f8
        0x7f0205ff
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->isPagingEnabled:Z

    return v0
.end method

.method private getViewCount()I
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method private init()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 81
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->isPagingEnabled:Z

    .line 83
    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$1;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 103
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 105
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 106
    .local v0, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 107
    return-void
.end method

.method private initNumberIndicatorContainer()V
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mNumberIndicatorContainer:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mNumberIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->removeView(Landroid/view/View;)V

    .line 196
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mNumberIndicatorContainer:Landroid/widget/LinearLayout;

    .line 200
    :goto_0
    return-void

    .line 198
    :cond_0
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mNumberIndicatorContainer:Landroid/widget/LinearLayout;

    goto :goto_0
.end method

.method private initRectangleIndicatorContainer()V
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mRectangleIndicatorContainer:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    .line 186
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mRectangleIndicatorContainer:Landroid/widget/LinearLayout;

    .line 191
    :goto_0
    return-void

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mRectangleIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->removeView(Landroid/view/View;)V

    .line 189
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mRectangleIndicatorContainer:Landroid/widget/LinearLayout;

    goto :goto_0
.end method

.method private onPageSelectedNumber(I)V
    .locals 5
    .param p1, "position"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 376
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mNumberIndicatorContainer:Landroid/widget/LinearLayout;

    mul-int/lit8 v2, p1, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 377
    .local v0, "indicatorView":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 378
    new-array v1, v4, [I

    const v2, 0x10100a1

    aput v2, v1, v3

    invoke-virtual {v0, v1, v3}, Landroid/widget/ImageView;->setImageState([IZ)V

    .line 381
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mNumberIndicatorContainer:Landroid/widget/LinearLayout;

    add-int/lit8 v2, p1, 0x1

    mul-int/lit8 v2, v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "indicatorView":Landroid/widget/ImageView;
    check-cast v0, Landroid/widget/ImageView;

    .line 382
    .restart local v0    # "indicatorView":Landroid/widget/ImageView;
    if-eqz v0, :cond_1

    .line 383
    new-array v1, v4, [I

    const v2, 0x10100a9

    aput v2, v1, v3

    invoke-virtual {v0, v1, v3}, Landroid/widget/ImageView;->setImageState([IZ)V

    .line 386
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mNumberIndicatorContainer:Landroid/widget/LinearLayout;

    add-int/lit8 v2, p1, -0x1

    mul-int/lit8 v2, v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "indicatorView":Landroid/widget/ImageView;
    check-cast v0, Landroid/widget/ImageView;

    .line 387
    .restart local v0    # "indicatorView":Landroid/widget/ImageView;
    if-eqz v0, :cond_2

    .line 388
    new-array v1, v4, [I

    const v2, 0x10100a0

    aput v2, v1, v3

    invoke-virtual {v0, v1, v3}, Landroid/widget/ImageView;->setImageState([IZ)V

    .line 390
    :cond_2
    return-void
.end method

.method private onPageSelectedRectangle(I)V
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 363
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mRectangleIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    .line 364
    .local v1, "indicatorSize":I
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 365
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mRectangleIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 366
    .local v2, "indicatorView":Landroid/widget/ImageView;
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 364
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 369
    .end local v2    # "indicatorView":Landroid/widget/ImageView;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mRectangleIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 370
    .restart local v2    # "indicatorView":Landroid/widget/ImageView;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 371
    return-void
.end method

.method private setAssessmentPageSelectedNumber(I)V
    .locals 8
    .param p1, "position"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 314
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->getAssessmentAndQAIndex(I)[I

    move-result-object v0

    .line 316
    .local v0, "assessmentAndQAIndex":[I
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mNumberIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aget v4, v0, v6

    if-eq v3, v4, :cond_4

    .line 318
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;

    aget v4, v0, v6

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->getAssessmentData(I)Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

    move-result-object v1

    .line 322
    .local v1, "assessmentData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    if-eqz v1, :cond_2

    .line 324
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getContext()Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->changeActionBar(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V

    .line 335
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v3

    sget-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    if-ne v3, v4, :cond_0

    .line 336
    sget-object v4, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$TYPE;->NUMBER:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$TYPE;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;

    aget v5, v0, v6

    invoke-virtual {v3, v5}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->getQACountOfAssessmentData(I)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    aget v5, v0, v6

    invoke-virtual {p0, v4, v3, v5}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->setIndicatorType(Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$TYPE;II)V

    .line 341
    :goto_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    aget v3, v0, v7

    if-gt v2, v3, :cond_1

    .line 342
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->onPageSelectedNumber(I)V

    .line 341
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 338
    .end local v2    # "i":I
    :cond_0
    sget-object v4, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$TYPE;->NUMBER:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$TYPE;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;

    aget v5, v0, v6

    invoke-virtual {v3, v5}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->getQACountOfAssessmentData(I)I

    move-result v3

    aget v5, v0, v6

    invoke-virtual {p0, v4, v3, v5}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->setIndicatorType(Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$TYPE;II)V

    goto :goto_0

    .line 346
    .restart local v2    # "i":I
    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v3

    sget-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    if-ne v3, v4, :cond_3

    .line 347
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->setIndicatorVisibility(Z)V

    .line 358
    .end local v1    # "assessmentData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    .end local v2    # "i":I
    :cond_2
    :goto_2
    return-void

    .line 349
    .restart local v1    # "assessmentData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    .restart local v2    # "i":I
    :cond_3
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->setIndicatorVisibility(Z)V

    goto :goto_2

    .line 356
    .end local v1    # "assessmentData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    .end local v2    # "i":I
    :cond_4
    aget v3, v0, v7

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->onPageSelectedNumber(I)V

    goto :goto_2
.end method

.method private setNumberIndicator(I)V
    .locals 12
    .param p1, "viewCount"    # I

    .prologue
    const/16 v11, 0x8

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 242
    if-nez p1, :cond_1

    .line 243
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mNumberIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 271
    :cond_0
    return-void

    .line 247
    :cond_1
    move v0, p1

    .line 249
    .local v0, "dataLength":I
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    if-ge v1, v0, :cond_0

    if-ge v0, v11, :cond_0

    .line 250
    new-instance v5, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v5}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 251
    .local v5, "stateList":Landroid/graphics/drawable/StateListDrawable;
    new-array v6, v8, [I

    const v9, 0x10100a0

    aput v9, v6, v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0205f1

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 252
    new-array v6, v8, [I

    const v9, 0x10100a1

    aput v9, v6, v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->NUMBER_TYPE_RES_IDS:[[I

    aget-object v10, v10, v1

    aget v10, v10, v8

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 253
    sget-object v6, Landroid/util/StateSet;->NOTHING:[I

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->NUMBER_TYPE_RES_IDS:[[I

    aget-object v10, v10, v1

    aget v10, v10, v7

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 255
    new-instance v3, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v3, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 256
    .local v3, "indicatorView":Landroid/widget/ImageView;
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    iget v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_NUMBER_WIDTH:I

    iget v9, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_NUMBER_HEIGHT:I

    invoke-direct {v4, v6, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 257
    .local v4, "lp":Landroid/widget/LinearLayout$LayoutParams;
    if-nez v1, :cond_3

    move v6, v7

    :goto_1
    invoke-virtual {v4, v6, v7, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 258
    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 259
    if-nez v1, :cond_4

    move v6, v8

    :goto_2
    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 260
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mNumberIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 262
    add-int/lit8 v6, v0, -0x1

    if-ge v1, v6, :cond_2

    .line 263
    new-instance v2, Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v2, v6}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 264
    .local v2, "indicatorLine":Landroid/view/View;
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    .end local v4    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_NUMBER_LINE_WIDTH:I

    iget v9, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_NUMBER_LINE_HEIGHT:I

    invoke-direct {v4, v6, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 265
    .restart local v4    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_NUMBER_LINE_LEFT_MARGIN:I

    iget v9, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_NUMBER_LINE_RIGHT_MARGIN:I

    invoke-virtual {v4, v6, v7, v9, v7}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 266
    const/16 v6, 0x10

    iput v6, v4, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 267
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v9, 0x7f07013c

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v2, v6}, Landroid/view/View;->setBackgroundColor(I)V

    .line 268
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mNumberIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v2, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 249
    .end local v2    # "indicatorLine":Landroid/view/View;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 257
    :cond_3
    iget v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_NUMBER_LEFT_MARGIN:I

    goto :goto_1

    :cond_4
    move v6, v7

    .line 259
    goto :goto_2
.end method

.method private setNumberIndicatorContainer()V
    .locals 3

    .prologue
    const/4 v1, -0x2

    const/4 v2, 0x0

    .line 232
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 234
    .local v0, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_NUMBER_TOP_MARGIN:I

    invoke-virtual {v0, v2, v1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 235
    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 236
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mNumberIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 238
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mNumberIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->bringToFront()V

    .line 239
    return-void
.end method

.method private setRectangleIndicator(I)V
    .locals 8
    .param p1, "viewCount"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 214
    if-ne p1, v5, :cond_1

    .line 215
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mRectangleIndicatorContainer:Landroid/widget/LinearLayout;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 229
    :cond_0
    return-void

    .line 219
    :cond_1
    move v0, p1

    .line 221
    .local v0, "dataLength":I
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 222
    new-instance v2, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 223
    .local v2, "indicatorView":Landroid/widget/ImageView;
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_RECTANGLE_WIDTH:I

    iget v7, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_RECTANGLE_HEIGHT:I

    invoke-direct {v3, v4, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 224
    .local v3, "lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v3, v6, v6, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 225
    const v4, 0x7f0200a4

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 226
    if-nez v1, :cond_2

    move v4, v5

    :goto_1
    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 227
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mRectangleIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 221
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    move v4, v6

    .line 226
    goto :goto_1
.end method

.method private setRectangleIndicatorContainer()V
    .locals 3

    .prologue
    const/4 v1, -0x2

    const/4 v2, 0x0

    .line 204
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 206
    .local v0, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->INDICATOR_RECTANGLE_TOP_MARGIN:I

    invoke-virtual {v0, v2, v1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 207
    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 208
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mRectangleIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 210
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mRectangleIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->bringToFront()V

    .line 211
    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 441
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 454
    :cond_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 451
    :pswitch_1
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;->getAnswerCount()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    if-ge v0, v1, :cond_0

    .line 452
    const/4 v0, 0x0

    goto :goto_0

    .line 441
    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getCurrentItem()I
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 124
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x0

    .line 403
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->isPagingEnabled:Z

    if-eqz v1, :cond_1

    .line 405
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 420
    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 423
    :cond_1
    return v0

    .line 407
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mTouchDownX:F

    .line 408
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mOnDisableInceptTouchEventListener:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$OnDisableInterceptTouchEventListener;

    if-eqz v1, :cond_0

    .line 409
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mOnDisableInceptTouchEventListener:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$OnDisableInterceptTouchEventListener;

    invoke-interface {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$OnDisableInterceptTouchEventListener;->disableInterceptTouchEvent(Z)V

    goto :goto_0

    .line 413
    :pswitch_2
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mTouchDownX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mTouchSlop:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 414
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mOnDisableInceptTouchEventListener:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$OnDisableInterceptTouchEventListener;

    if-eqz v0, :cond_0

    .line 415
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mOnDisableInceptTouchEventListener:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$OnDisableInterceptTouchEventListener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$OnDisableInterceptTouchEventListener;->disableInterceptTouchEvent(Z)V

    goto :goto_0

    .line 405
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onPageScrollStateChanged(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 275
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mOnPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_0

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mOnPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrollStateChanged(I)V

    .line 278
    :cond_0
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 282
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mOnPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mOnPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1, p2, p3}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrolled(IFI)V

    .line 285
    :cond_0
    return-void
.end method

.method public onPageSelected(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 289
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$2;->$SwitchMap$com$sec$android$app$shealth$cignacoach$widget$IndicatorViewPager$TYPE:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mIndicatorType:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$TYPE;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$TYPE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 307
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mOnPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mOnPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageSelected(I)V

    .line 310
    :cond_0
    return-void

    .line 291
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->onPageSelectedRectangle(I)V

    goto :goto_0

    .line 295
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;

    if-eqz v0, :cond_1

    .line 297
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->setAssessmentPageSelectedNumber(I)V

    goto :goto_0

    .line 301
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->onPageSelectedNumber(I)V

    goto :goto_0

    .line 289
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 394
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->isPagingEnabled:Z

    if-eqz v0, :cond_0

    .line 395
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 398
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAdapter(Landroid/support/v4/view/PagerAdapter;)V
    .locals 3
    .param p1, "adapter"    # Landroid/support/v4/view/PagerAdapter;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mIndicatorType:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$TYPE;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mIndicatorType:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$TYPE;

    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getViewCount()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->setIndicatorType(Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$TYPE;II)V

    .line 114
    :cond_0
    return-void

    .line 112
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$TYPE;->RECTANGLE:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$TYPE;

    goto :goto_0
.end method

.method public setCurrentItem(IZ)V
    .locals 1
    .param p1, "item"    # I
    .param p2, "smoothScroll"    # Z

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 132
    :cond_0
    return-void
.end method

.method public setIndicatorType(Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$TYPE;II)V
    .locals 2
    .param p1, "indicatorType"    # Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$TYPE;
    .param p2, "viewCount"    # I
    .param p3, "dataIndex"    # I

    .prologue
    .line 144
    if-nez p1, :cond_0

    sget-object p1, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$TYPE;->RECTANGLE:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$TYPE;

    .line 146
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mIndicatorType:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$TYPE;

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    if-nez v0, :cond_2

    .line 182
    :cond_1
    :goto_0
    return-void

    .line 150
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->initRectangleIndicatorContainer()V

    .line 151
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->initNumberIndicatorContainer()V

    .line 153
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$2;->$SwitchMap$com$sec$android$app$shealth$cignacoach$widget$IndicatorViewPager$TYPE:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mIndicatorType:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$TYPE;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$TYPE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 156
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->setRectangleIndicatorContainer()V

    .line 157
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->setRectangleIndicator(I)V

    goto :goto_0

    .line 162
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->setNumberIndicatorContainer()V

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mNumberIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;

    if-eqz v0, :cond_3

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;

    invoke-virtual {v0, p3}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->getAssessmentData(I)Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 178
    :cond_3
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->setNumberIndicator(I)V

    goto :goto_0

    .line 153
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setIndicatorVisibility(Z)V
    .locals 4
    .param p1, "isVisible"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mRectangleIndicatorContainer:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mRectangleIndicatorContainer:Landroid/widget/LinearLayout;

    if-eqz p1, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mNumberIndicatorContainer:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mNumberIndicatorContainer:Landroid/widget/LinearLayout;

    if-eqz p1, :cond_3

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 141
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 139
    goto :goto_0

    :cond_3
    move v1, v2

    .line 140
    goto :goto_1
.end method

.method public setOnDisableInceptTouchEventListener(Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$OnDisableInterceptTouchEventListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$OnDisableInterceptTouchEventListener;

    .prologue
    .line 431
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mOnDisableInceptTouchEventListener:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$OnDisableInterceptTouchEventListener;

    .line 432
    return-void
.end method

.method public setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->mOnPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .line 118
    return-void
.end method

.method public setPagingEnabled(Z)V
    .locals 0
    .param p1, "isPagingEnabled"    # Z

    .prologue
    .line 427
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->isPagingEnabled:Z

    .line 428
    return-void
.end method
