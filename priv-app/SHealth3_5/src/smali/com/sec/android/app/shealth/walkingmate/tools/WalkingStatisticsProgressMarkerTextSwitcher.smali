.class public Lcom/sec/android/app/shealth/walkingmate/tools/WalkingStatisticsProgressMarkerTextSwitcher;
.super Landroid/widget/LinearLayout;
.source "WalkingStatisticsProgressMarkerTextSwitcher.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 20
    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingStatisticsProgressMarkerTextSwitcher;->init(I)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "gravity"    # I

    .prologue
    .line 14
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 15
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingStatisticsProgressMarkerTextSwitcher;->init(I)V

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingStatisticsProgressMarkerTextSwitcher;->init(I)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingStatisticsProgressMarkerTextSwitcher;->init(I)V

    .line 31
    return-void
.end method

.method private init(I)V
    .locals 3
    .param p1, "gravity"    # I

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingStatisticsProgressMarkerTextSwitcher;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0302aa

    invoke-static {v1, v2, p0}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingStatisticsProgressMarkerTextSwitcher;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 35
    const v1, 0x7f080bf5

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingStatisticsProgressMarkerTextSwitcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 36
    .local v0, "textArea":Landroid/widget/LinearLayout;
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 39
    return-void
.end method


# virtual methods
.method public getTextWidth()F
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 56
    invoke-static {v0, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public setTextString(Ljava/lang/String;)V
    .locals 0
    .param p1, "textString"    # Ljava/lang/String;

    .prologue
    .line 47
    return-void
.end method

.method public setTextVisible(Z)V
    .locals 0
    .param p1, "isVisible"    # Z

    .prologue
    .line 51
    return-void
.end method

.method public setValueString(Ljava/lang/String;)V
    .locals 0
    .param p1, "valueString"    # Ljava/lang/String;

    .prologue
    .line 43
    return-void
.end method
