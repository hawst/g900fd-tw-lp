.class Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$1;
.super Ljava/lang/Object;
.source "InitSetTermsOfUse.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$1;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const v5, 0x7f080a51

    const v1, 0x7f080a3e

    const v4, 0x7f080a0f

    const v3, 0x7f080a0b

    const/4 v2, 0x0

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$1;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->TC_STEP:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$000(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$1;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isAustralia:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$100(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$1;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->w:Ljava/lang/ref/WeakReference;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$200(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$1;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->w:Ljava/lang/ref/WeakReference;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$200(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$1;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # operator++ for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->TC_STEP:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$008(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)I

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$1;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$300(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v1, 0x64

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 106
    :cond_0
    :goto_0
    return-void

    .line 100
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$1;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->TC_STEP:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$000(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$1;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isAustralia:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$100(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$1;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->w:Ljava/lang/ref/WeakReference;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$200(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$1;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->w:Ljava/lang/ref/WeakReference;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$200(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$1;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # operator++ for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->TC_STEP:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$008(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)I

    goto :goto_0
.end method
