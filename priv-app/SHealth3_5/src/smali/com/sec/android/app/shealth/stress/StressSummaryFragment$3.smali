.class Lcom/sec/android/app/shealth/stress/StressSummaryFragment$3;
.super Ljava/lang/Object;
.source "StressSummaryFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V
    .locals 0

    .prologue
    .line 724
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x1

    const/16 v7, -0x3e9

    const/16 v6, -0x7d1

    const/4 v5, 0x0

    .line 728
    sget-object v2, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "mHeartrateOnClcickListener"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 729
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 770
    :goto_0
    return-void

    .line 731
    :sswitch_0
    sget-object v2, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "mHeartrateOnClcickListener event : ib_summary_third_graph"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 732
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # invokes: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->showGraphFragment()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$1500(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V

    goto :goto_0

    .line 735
    :sswitch_1
    sget-object v2, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "mHeartrateOnClcickListener event : ll_summary_third_previous"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 736
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$1600(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 739
    :sswitch_2
    sget-object v2, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "mHeartrateOnClcickListener event : bt_summary_second_retry"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 740
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    sget-object v3, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->effectAudio:[I

    aget v3, v3, v5

    # invokes: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->playSound(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$1700(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;I)V

    .line 742
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdStartDiscard:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$600(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 744
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mState:Lcom/sec/android/app/shealth/stress/state/StressState;

    invoke-virtual {v2, v7, v6}, Lcom/sec/android/app/shealth/stress/state/StressState;->updateNextState(II)V

    .line 746
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v2, v8}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->updateBpmDataView(Z)V

    goto :goto_0

    .line 750
    :sswitch_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;
    invoke-static {v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$300(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->selectedDate:Ljava/util/Date;
    invoke-static {v3}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$1800(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->getLastTwoDataByTime(J)Ljava/util/ArrayList;

    move-result-object v1

    .line 751
    .local v1, "stressLastDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/stress/data/StressData;>;"
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 752
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/stress/data/StressData;

    .line 753
    .local v0, "msressLatestData":Lcom/sec/android/app/shealth/stress/data/StressData;
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/stress/data/StressData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/stress/data/StressData;->getId()J

    .line 754
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;
    invoke-static {v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$300(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/data/StressData;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->deleteRowById(Ljava/lang/String;)Z

    .line 757
    .end local v0    # "msressLatestData":Lcom/sec/android/app/shealth/stress/data/StressData;
    :cond_0
    sget-object v2, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "mHeartrateOnClcickListener event : bt_summary_second_discard"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 758
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    sget-object v3, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->effectAudio:[I

    const/4 v4, 0x2

    aget v3, v3, v4

    # invokes: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->playSound(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$1700(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;I)V

    .line 760
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdStartDiscard:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$600(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 762
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mState:Lcom/sec/android/app/shealth/stress/state/StressState;

    invoke-virtual {v2, v7, v6}, Lcom/sec/android/app/shealth/stress/state/StressState;->updateNextState(II)V

    .line 764
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v2, v8}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->updateBpmDataView(Z)V

    .line 765
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$1600(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    const v4, 0x7f090c3b

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v5}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 729
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0805e5 -> :sswitch_1
        0x7f0805ef -> :sswitch_0
        0x7f080a03 -> :sswitch_3
        0x7f080a04 -> :sswitch_2
    .end sparse-switch
.end method
