.class interface abstract Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem;
.super Ljava/lang/Object;
.source "IListItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;
    }
.end annotation


# virtual methods
.method public abstract createSelectedPanelHolder()Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;
.end method

.method public abstract getExpandableIndicator()Landroid/view/View;
.end method

.method public abstract getListItemId()J
.end method

.method public abstract getListItemType()Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;
.end method

.method public abstract getView(Landroid/view/LayoutInflater;Landroid/view/View;Landroid/view/ViewGroup;Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;)Landroid/view/View;
.end method

.method public abstract removeFromMyCategory()V
.end method
