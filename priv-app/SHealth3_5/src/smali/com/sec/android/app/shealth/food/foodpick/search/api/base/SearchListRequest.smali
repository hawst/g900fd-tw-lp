.class public Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;
.super Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;
.source "SearchListRequest.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mPageNumber:I

.field private mPerPageNumber:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;-><init>(Landroid/os/Parcel;)V

    .line 53
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->mPageNumber:I

    .line 54
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->mPerPageNumber:I

    .line 55
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p1, "requestString"    # Ljava/lang/String;
    .param p2, "pageNumber"    # I
    .param p3, "perPageNumber"    # I

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;-><init>(Ljava/lang/String;)V

    .line 32
    iput p2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->mPageNumber:I

    .line 33
    iput p3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->mPerPageNumber:I

    .line 34
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;)V
    .locals 2
    .param p1, "requestString"    # Ljava/lang/String;
    .param p2, "foodSearchApi"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    .prologue
    .line 43
    invoke-interface {p2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;->getIndexOfFirstPage()I

    move-result v0

    invoke-interface {p2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;->getPerPageCount()I

    move-result v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;-><init>(Ljava/lang/String;II)V

    .line 44
    return-void
.end method


# virtual methods
.method public getPageNumber()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->mPageNumber:I

    return v0
.end method

.method public getPerPageNumber()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->mPerPageNumber:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "i"    # I

    .prologue
    .line 84
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;->writeToParcel(Landroid/os/Parcel;I)V

    .line 85
    iget v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->mPageNumber:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 86
    iget v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->mPerPageNumber:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 87
    return-void
.end method
