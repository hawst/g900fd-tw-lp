.class public Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopupImpl;
.super Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;
.source "ListPopupImpl.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;ILcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "items"    # [Ljava/lang/String;
    .param p3, "caller"    # Landroid/view/View;
    .param p4, "width"    # I
    .param p5, "uniquizeViewIdHelper"    # Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;

    .prologue
    .line 30
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;ILcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;ILcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;Z)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "items"    # [Ljava/lang/String;
    .param p3, "caller"    # Landroid/view/View;
    .param p4, "width"    # I
    .param p5, "uniquizeViewIdHelper"    # Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;
    .param p6, "focusable"    # Z

    .prologue
    .line 36
    invoke-direct/range {p0 .. p6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;ILcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;Z)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;I[Ljava/lang/String;Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "items"    # [Ljava/lang/String;
    .param p3, "caller"    # Landroid/view/View;
    .param p4, "width"    # I
    .param p5, "additionalItems"    # [Ljava/lang/String;
    .param p6, "uniquizeViewIdHelper"    # Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;

    .prologue
    .line 18
    invoke-direct/range {p0 .. p6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;I[Ljava/lang/String;Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "items"    # [Ljava/lang/String;
    .param p3, "caller"    # Landroid/view/View;
    .param p4, "uniquizeViewIdHelper"    # Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;

    .prologue
    .line 13
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;)V

    .line 14
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;Z)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "items"    # [Ljava/lang/String;
    .param p3, "caller"    # Landroid/view/View;
    .param p4, "uniquizeViewIdHelper"    # Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;
    .param p5, "focusable"    # Z

    .prologue
    .line 42
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;Z)V

    .line 43
    return-void
.end method


# virtual methods
.method protected getAdditionalUnitViewId()I
    .locals 1

    .prologue
    .line 108
    const v0, 0x7f0807ff

    return v0
.end method

.method protected getBackgroundEmptySpaceHeight()I
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopupImpl;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a08e6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    return v0
.end method

.method protected getBackgroundResource()I
    .locals 1

    .prologue
    .line 83
    const v0, 0x7f02086f

    return v0
.end method

.method protected getBelowTextViewId()I
    .locals 1

    .prologue
    .line 98
    const v0, 0x7f0807fe

    return v0
.end method

.method protected getBottomItemDrawableId()I
    .locals 1

    .prologue
    .line 78
    const v0, 0x7f020258

    return v0
.end method

.method protected getClickableViewId()I
    .locals 1

    .prologue
    .line 129
    const v0, 0x7f0807fc

    return v0
.end method

.method protected getDefaultItemColorId()I
    .locals 1

    .prologue
    .line 63
    const v0, 0x7f07007a

    return v0
.end method

.method protected getDividerDrawable()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopupImpl;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020756

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method protected getDividerHeight()I
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopupImpl;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00e9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method protected getGreyItemColorId()I
    .locals 1

    .prologue
    .line 119
    const v0, 0x7f07009e

    return v0
.end method

.method protected getLayoutId()I
    .locals 1

    .prologue
    .line 88
    const v0, 0x7f0301ce

    return v0
.end method

.method protected getListPopupItemHeight()I
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopupImpl;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00e6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method protected getMiddleItemDrawableId()I
    .locals 1

    .prologue
    .line 73
    const v0, 0x7f020258

    return v0
.end method

.method protected getRecetTextViewId()I
    .locals 1

    .prologue
    .line 124
    const v0, 0x7f0807fb

    return v0
.end method

.method protected getSelectedItemColorId()I
    .locals 1

    .prologue
    .line 58
    const v0, 0x7f070079

    return v0
.end method

.method protected getTextViewId()I
    .locals 1

    .prologue
    .line 93
    const v0, 0x7f08033a

    return v0
.end method

.method protected getTopItemDrawableId()I
    .locals 1

    .prologue
    .line 68
    const v0, 0x7f020258

    return v0
.end method
