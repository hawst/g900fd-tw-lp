.class public final Lcom/sec/android/app/shealth/common/commonui/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commonui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final cal_month_data_etc:I = 0x7f020037

.field public static final checkbox_selector:I = 0x7f020056

.field public static final checkbox_selector_for_textview:I = 0x7f020057

.field public static final common_et_bg:I = 0x7f02011a

.field public static final custom_cursor:I = 0x7f02013d

.field public static final default_dropdown_list_item_selector:I = 0x7f020146

.field public static final default_dropdown_list_selector:I = 0x7f020147

.field public static final horizontal_input_module_back_btn_selector:I = 0x7f0201eb

.field public static final horizontal_input_module_controller_view_background_green_pin:I = 0x7f0201ec

.field public static final horizontal_input_module_controller_view_background_orange_pin:I = 0x7f0201ed

.field public static final horizontal_input_module_next_btn_selector:I = 0x7f0201ee

.field public static final ic_launcher:I = 0x7f02020b

.field public static final input_common_edit_text_background_selector:I = 0x7f02021a

.field public static final input_edit_text_background_selector:I = 0x7f02021b

.field public static final list_selector:I = 0x7f020222

.field public static final menu_selector:I = 0x7f020236

.field public static final popup_bottom_button_selector_bg:I = 0x7f02024d

.field public static final popup_button_selector_bg:I = 0x7f02024e

.field public static final profile_edit_text_background_selector:I = 0x7f020281

.field public static final s_health_action_bar_focus:I = 0x7f020298

.field public static final s_health_blood_glucose_list_dot:I = 0x7f0202a2

.field public static final s_health_blood_glucose_mask:I = 0x7f0202a3

.field public static final s_health_blood_glucose_orange:I = 0x7f0202a4

.field public static final s_health_blood_glucose_setgoal:I = 0x7f0202a5

.field public static final s_health_blood_glucose_setgoal_pin_green:I = 0x7f0202a6

.field public static final s_health_blood_glucose_setgoal_pin_orange:I = 0x7f0202a7

.field public static final s_health_bottom_btn_icon:I = 0x7f0202a8

.field public static final s_health_input_exercise_icon_back_dim:I = 0x7f020563

.field public static final s_health_input_exercise_icon_back_dimfocus:I = 0x7f020564

.field public static final s_health_input_exercise_icon_back_focus:I = 0x7f020565

.field public static final s_health_input_exercise_icon_back_nor:I = 0x7f020566

.field public static final s_health_input_exercise_icon_back_press:I = 0x7f020567

.field public static final s_health_input_exercise_icon_down_dim:I = 0x7f020568

.field public static final s_health_input_exercise_icon_down_dimfocus:I = 0x7f020569

.field public static final s_health_input_exercise_icon_down_focus:I = 0x7f02056a

.field public static final s_health_input_exercise_icon_down_nor:I = 0x7f02056b

.field public static final s_health_input_exercise_icon_down_press:I = 0x7f02056c

.field public static final s_health_input_exercise_icon_next_dim:I = 0x7f02056d

.field public static final s_health_input_exercise_icon_next_dimfocus:I = 0x7f02056e

.field public static final s_health_input_exercise_icon_next_focus:I = 0x7f02056f

.field public static final s_health_input_exercise_icon_next_nor:I = 0x7f020570

.field public static final s_health_input_exercise_icon_next_press:I = 0x7f020571

.field public static final s_health_input_exercise_icon_up_dim:I = 0x7f020572

.field public static final s_health_input_exercise_icon_up_dimpress:I = 0x7f020573

.field public static final s_health_input_exercise_icon_up_focus:I = 0x7f020574

.field public static final s_health_input_exercise_icon_up_nor:I = 0x7f020575

.field public static final s_health_input_exercise_icon_up_press:I = 0x7f020576

.field public static final s_health_medication_setgoal:I = 0x7f0205a8

.field public static final s_health_medication_setgoal_pin_green:I = 0x7f0205a9

.field public static final s_health_medication_setgoal_pin_orange:I = 0x7f0205aa

.field public static final s_health_quickpanel_icon_02:I = 0x7f020605

.field public static final s_health_summary_accessary:I = 0x7f020654

.field public static final s_health_summary_button_dim:I = 0x7f020664

.field public static final s_health_summary_button_disabled:I = 0x7f020665

.field public static final s_health_summary_button_discard:I = 0x7f020666

.field public static final s_health_summary_button_focus:I = 0x7f020667

.field public static final s_health_summary_button_normal:I = 0x7f020669

.field public static final s_health_summary_button_pause_dim:I = 0x7f02066b

.field public static final s_health_summary_button_plus:I = 0x7f02066c

.field public static final s_health_summary_button_plus_dim:I = 0x7f02066d

.field public static final s_health_summary_button_press:I = 0x7f02066e

.field public static final s_health_summary_button_pressselect:I = 0x7f02066f

.field public static final s_health_summary_button_refresh:I = 0x7f020672

.field public static final s_health_summary_button_select:I = 0x7f020673

.field public static final s_health_summary_pageturn:I = 0x7f02069c

.field public static final s_health_summary_plus:I = 0x7f0206d9

.field public static final s_health_summary_s_accessary:I = 0x7f0206dd

.field public static final share_item_bg_color:I = 0x7f02073f

.field public static final share_popup_item_selector:I = 0x7f020740

.field public static final spinner_divider:I = 0x7f020756

.field public static final summary_button_left_drawable_selector:I = 0x7f020787

.field public static final summary_button_selector:I = 0x7f020788

.field public static final summary_button_text_color_selector:I = 0x7f020789

.field public static final tw_btn_check_off_disabled_focused_holo_light:I = 0x7f0207ed

.field public static final tw_btn_check_off_disabled_holo_light:I = 0x7f0207ee

.field public static final tw_btn_check_off_focused_holo_light:I = 0x7f0207ef

.field public static final tw_btn_check_off_holo_light:I = 0x7f0207f0

.field public static final tw_btn_check_off_pressed_holo_light:I = 0x7f0207f1

.field public static final tw_btn_check_on_disabled_focused_holo_light:I = 0x7f0207f2

.field public static final tw_btn_check_on_disabled_holo_light:I = 0x7f0207f3

.field public static final tw_btn_check_on_focused_holo_light:I = 0x7f0207f4

.field public static final tw_btn_check_on_holo_light:I = 0x7f0207f5

.field public static final tw_btn_check_on_pressed_holo_light:I = 0x7f0207f6

.field public static final tw_btn_radio_off_disabled_focused_holo_light:I = 0x7f02080a

.field public static final tw_btn_radio_off_disabled_holo_light:I = 0x7f02080b

.field public static final tw_btn_radio_off_focused_holo_light:I = 0x7f02080c

.field public static final tw_btn_radio_off_holo_light:I = 0x7f02080d

.field public static final tw_btn_radio_off_pressed_holo_light:I = 0x7f02080e

.field public static final tw_btn_radio_on_disabled_focused_holo_light:I = 0x7f02080f

.field public static final tw_btn_radio_on_disabled_holo_light:I = 0x7f020810

.field public static final tw_btn_radio_on_focused_holo_light:I = 0x7f020811

.field public static final tw_btn_radio_on_holo_light:I = 0x7f020812

.field public static final tw_btn_radio_on_pressed_holo_light:I = 0x7f020813

.field public static final tw_btn_radio_selector:I = 0x7f020814

.field public static final tw_buttonbarbutton_selector_default_holo_light:I = 0x7f02081d

.field public static final tw_buttonbarbutton_selector_disabled_focused_holo_light:I = 0x7f020820

.field public static final tw_buttonbarbutton_selector_disabled_holo_light:I = 0x7f020823

.field public static final tw_buttonbarbutton_selector_focused_holo_light:I = 0x7f020826

.field public static final tw_buttonbarbutton_selector_pressed_holo_light:I = 0x7f020829

.field public static final tw_buttonbarbutton_selector_selected_holo_light:I = 0x7f02082c

.field public static final tw_dialog_bottom_holo_light:I = 0x7f020830

.field public static final tw_dialog_bottom_medium_holo_light:I = 0x7f020831

.field public static final tw_dialog_full_holo_light:I = 0x7f020832

.field public static final tw_dialog_middle_holo_light:I = 0x7f020833

.field public static final tw_dialog_middle_list_line:I = 0x7f020834

.field public static final tw_dialog_title_holo_light:I = 0x7f020835

.field public static final tw_dialog_top_holo_light:I = 0x7f020836

.field public static final tw_dialog_top_medium_holo_light:I = 0x7f020837

.field public static final tw_divider_option_popup_holo_light:I = 0x7f020839

.field public static final tw_divider_popup_vertical_holo_light:I = 0x7f02083a

.field public static final tw_list_disabled_focused_holo_light:I = 0x7f02085a

.field public static final tw_list_disabled_holo_light:I = 0x7f02085b

.field public static final tw_list_divider_holo_light:I = 0x7f02085c

.field public static final tw_list_focused_holo_light:I = 0x7f02085d

.field public static final tw_list_longpressed_holo_light:I = 0x7f020864

.field public static final tw_list_pressed_holo_light:I = 0x7f020865

.field public static final tw_list_selected_holo_light:I = 0x7f02086c

.field public static final tw_list_selector_disabled_holo_light:I = 0x7f02086d

.field public static final tw_menu_dropdown_panel_holo_light:I = 0x7f02086f

.field public static final tw_no_item_popup_bg_holo_light:I = 0x7f020872

.field public static final tw_select_all_bg_holo_light:I = 0x7f020879

.field public static final tw_spinner_default_holo_light:I = 0x7f020880

.field public static final tw_spinner_disabled_holo_light:I = 0x7f020882

.field public static final tw_spinner_focused_holo_light:I = 0x7f020884

.field public static final tw_spinner_list_focused_holo_light:I = 0x7f020886

.field public static final tw_spinner_pressed_holo_light:I = 0x7f020888

.field public static final tw_textfield_activated_holo_light:I = 0x7f0208a0

.field public static final tw_textfield_default_holo_light:I = 0x7f0208a1

.field public static final tw_textfield_disabled_focused_holo_light:I = 0x7f0208a2

.field public static final tw_textfield_disabled_holo_light:I = 0x7f0208a3

.field public static final tw_textfield_focused_holo_light:I = 0x7f0208a4

.field public static final tw_textfield_pressed_holo_light:I = 0x7f0208ac

.field public static final tw_textfield_selected_holo_light:I = 0x7f0208b1

.field public static final tw_toast_frame_holo_light:I = 0x7f0208b2

.field public static final tw_widget_progressbar:I = 0x7f0208b3

.field public static final tw_widget_progressbar_holo_light:I = 0x7f0208b4

.field public static final tw_widget_progressbar_holo_light_02:I = 0x7f0208b5

.field public static final vertical_input_module_controller_view_background_green_pin:I = 0x7f0208ce

.field public static final vertical_input_module_controller_view_background_orange_pin:I = 0x7f0208cf

.field public static final vertical_input_module_down_btn_selector:I = 0x7f0208d0

.field public static final vertical_input_module_up_btn_selector:I = 0x7f0208d1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 307
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
