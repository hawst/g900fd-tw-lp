.class public final Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;
.super Ljava/lang/Object;
.source "CursorWrapperWithValidation.java"


# static fields
.field private static final EMPTY:I = -0x1

.field private static final EMPTY_STRING:Ljava/lang/String; = ""


# instance fields
.field private final mCursor:Landroid/database/Cursor;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    if-nez p1, :cond_0

    .line 37
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "cursor should not be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->mCursor:Landroid/database/Cursor;

    .line 40
    return-void
.end method


# virtual methods
.method public getBoolean(Ljava/lang/String;)Z
    .locals 3
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 53
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->mCursor:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getInt(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDouble(Ljava/lang/String;)D
    .locals 3
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 109
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 110
    .local v0, "columnIndex":I
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 111
    const-wide/high16 v1, -0x4010000000000000L    # -1.0

    .line 113
    :goto_0
    return-wide v1

    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v1

    goto :goto_0
.end method

.method public getFloat(Ljava/lang/String;)F
    .locals 2
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 94
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 95
    .local v0, "columnIndex":I
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 96
    const/high16 v1, -0x40800000    # -1.0f

    .line 98
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    goto :goto_0
.end method

.method public getInt(Ljava/lang/String;)I
    .locals 2
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 64
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 65
    .local v0, "columnIndex":I
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    const/4 v1, -0x1

    .line 68
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    goto :goto_0
.end method

.method public getLong(Ljava/lang/String;)J
    .locals 3
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 79
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 80
    .local v0, "columnIndex":I
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 81
    const-wide/16 v1, -0x1

    .line 83
    :goto_0
    return-wide v1

    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    goto :goto_0
.end method

.method public getString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 124
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 125
    .local v0, "columnIndex":I
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 126
    const-string v1, ""

    .line 128
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
