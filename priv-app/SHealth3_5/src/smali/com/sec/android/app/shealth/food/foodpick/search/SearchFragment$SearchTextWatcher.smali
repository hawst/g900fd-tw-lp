.class Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$SearchTextWatcher;
.super Lcom/sec/android/app/shealth/common/utils/TextWatcherStub;
.source "SearchFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SearchTextWatcher"
.end annotation


# instance fields
.field private mIsEnabled:Z

.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V
    .locals 1

    .prologue
    .line 1096
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$SearchTextWatcher;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/TextWatcherStub;-><init>()V

    .line 1097
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$SearchTextWatcher;->mIsEnabled:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$1;

    .prologue
    .line 1096
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$SearchTextWatcher;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3
    .param p1, "editable"    # Landroid/text/Editable;

    .prologue
    .line 1101
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$SearchTextWatcher;->mIsEnabled:Z

    if-nez v1, :cond_1

    .line 1113
    :cond_0
    :goto_0
    return-void

    .line 1104
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1105
    .local v0, "text":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$SearchTextWatcher;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$2300(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;->isAutoCompleteSupported()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$SearchTextWatcher;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$2300(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;->getAutoCompleteMinLength()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 1106
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$SearchTextWatcher;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->cancelAutoCompleteSearch()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$1900(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V

    .line 1107
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$SearchTextWatcher;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompletePopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$1100(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$SearchTextWatcher;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompletePopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$1100(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1108
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$SearchTextWatcher;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompletePopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$1100(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->dismiss()V

    goto :goto_0

    .line 1112
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$SearchTextWatcher;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->performAutoCompleteSearch(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$2900(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Ljava/lang/String;)V

    goto :goto_0
.end method

.method setEnabled(Z)V
    .locals 0
    .param p1, "isEnabled"    # Z

    .prologue
    .line 1116
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$SearchTextWatcher;->mIsEnabled:Z

    .line 1117
    return-void
.end method
