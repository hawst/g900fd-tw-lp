.class Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$13;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->settingInfo(Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

.field final synthetic val$settingDevice:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$13;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$13;->val$settingDevice:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 10

    const/4 v9, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$13;->val$settingDevice:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$13;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$13;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->IsAutoScanRequired:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1902(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$13;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->scanbutton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$3200(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$13;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->disconnectToastReq:Z
    invoke-static {v0, v9}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$2002(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$13;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$13;->val$settingDevice:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->unpairedSensorDeviceDetails:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$2202(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$13;->val$settingDevice:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->isFromPairedDB()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/utils/ScanningFragmentHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/utils/ScanningFragmentHelper;

    move-result-object v4

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$13;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$13;->val$settingDevice:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceType()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    new-array v1, v9, [I

    const/16 v0, -0x58b2

    aput v0, v1, v3

    new-array v0, v9, [I

    const/16 v2, -0x58ef

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v7, v0

    if-lt v2, v7, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    :goto_1
    array-length v2, v0

    if-lt v3, v2, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$13;->val$settingDevice:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Lcom/sec/android/app/shealth/framework/ui/utils/ScanningFragmentHelper;->deletePairedDeviceInDb(Landroid/content/Context;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$13;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$13;->val$settingDevice:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->leaveDevice(Ljava/lang/String;Z)V
    invoke-static {v0, v1, v9}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$4700(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Ljava/lang/String;Z)V

    :goto_2
    return-void

    :cond_0
    aget v7, v0, v2

    aget v8, v1, v2

    xor-int/2addr v7, v8

    aput v7, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$13;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$13;->val$settingDevice:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->leaveDevice(Ljava/lang/String;Z)V
    invoke-static {v0, v1, v9}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$4700(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Ljava/lang/String;Z)V

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_2
.end method
