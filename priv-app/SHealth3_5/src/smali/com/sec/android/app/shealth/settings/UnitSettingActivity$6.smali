.class Lcom/sec/android/app/shealth/settings/UnitSettingActivity$6;
.super Ljava/lang/Object;
.source "UnitSettingActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V
    .locals 0

    .prologue
    .line 550
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSave(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 554
    if-nez p1, :cond_0

    .line 555
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900cf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 559
    .local v0, "glucoseUnit":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$700(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putGlucoseUnit(Ljava/lang/String;)V

    .line 560
    return-void

    .line 557
    .end local v0    # "glucoseUnit":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900d0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "glucoseUnit":Ljava/lang/String;
    goto :goto_0
.end method
