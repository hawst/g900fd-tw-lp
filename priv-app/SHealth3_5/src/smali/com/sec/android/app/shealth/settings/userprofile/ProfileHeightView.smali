.class public Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;
.super Landroid/widget/RelativeLayout;
.source "ProfileHeightView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextFocusHandler;,
        Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;
    }
.end annotation


# instance fields
.field private final CM:I

.field private isChangeFocus:Z

.field private isChangeUnit:Z

.field private isChangeValue:Z

.field private mContext:Landroid/content/Context;

.field private mCustomSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

.field private mEditTextFocusHandler:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextFocusHandler;

.field private mEditTextWatcher:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;

.field private mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

.field private mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

.field private mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

.field private mHeightUnit:I

.field private mHeightValue:F

.field private mInputFilters:[Landroid/text/InputFilter;

.field private mInputSecondFilters:[Landroid/text/InputFilter;

.field private mListener:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;

.field private mMaxValue:F

.field private mMinValue:F

.field private mPrimeTextView:[Landroid/widget/TextView;

.field private mSystemNumberSeparator:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 76
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 80
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v3, 0x0

    const/high16 v2, 0x43160000    # 150.0f

    const/4 v1, 0x0

    .line 84
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mMinValue:F

    .line 57
    const v0, 0x4479f99a    # 999.9f

    iput v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mMaxValue:F

    .line 58
    iput v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightValue:F

    .line 60
    iput v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->CM:I

    .line 61
    const v0, 0x249f1

    iput v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightUnit:I

    .line 68
    new-instance v0, Ljava/text/DecimalFormat;

    invoke-direct {v0}, Ljava/text/DecimalFormat;-><init>()V

    invoke-virtual {v0}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object v0

    invoke-virtual {v0}, Ljava/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mSystemNumberSeparator:Ljava/lang/String;

    .line 71
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->isChangeFocus:Z

    .line 72
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->isChangeValue:Z

    .line 73
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->isChangeUnit:Z

    .line 85
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mContext:Landroid/content/Context;

    .line 86
    new-instance v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;

    invoke-direct {v0, p0, v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mEditTextWatcher:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;

    .line 87
    new-instance v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextFocusHandler;

    invoke-direct {v0, p0, v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextFocusHandler;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mEditTextFocusHandler:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextFocusHandler;

    .line 89
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->initLayout()V

    .line 91
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->setValue(F)V

    .line 92
    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mListener:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->setDatalimit(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->isChangeFocus:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->isChangeFocus:Z

    return p1
.end method

.method static synthetic access$1302(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->isChangeValue:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    .prologue
    .line 46
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mMinValue:F

    return v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->isHeightValidValue()F

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    .prologue
    .line 46
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightUnit:I

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;
    .param p1, "x1"    # I

    .prologue
    .line 46
    iput p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightUnit:I

    return p1
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->isChangeUnit:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    .prologue
    .line 46
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightValue:F

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->isHeighUnitCM()Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->getHeightTextValue()F

    move-result v0

    return v0
.end method

.method private convertComma(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 248
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mSystemNumberSeparator:Ljava/lang/String;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mSystemNumberSeparator:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mSystemNumberSeparator:Ljava/lang/String;

    const-string v1, "."

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 252
    :cond_0
    return-object p1
.end method

.method public static getFeetandInch(F)Ljava/util/HashMap;
    .locals 5
    .param p0, "value"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 307
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 308
    .local v1, "feetInch":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertInchToCm(F)F

    move-result v3

    float-to-int v2, v3

    .line 309
    .local v2, "inch":I
    div-int/lit8 v0, v2, 0xc

    .line 310
    .local v0, "feet":I
    rem-int/lit8 v2, v2, 0xc

    .line 311
    if-nez v0, :cond_0

    const/16 v3, 0x8

    if-ge v2, v3, :cond_0

    .line 312
    const/16 v2, 0x8

    .line 313
    :cond_0
    mul-int/lit8 v3, v2, 0xa

    int-to-float v3, v3

    const/high16 v4, 0x3f000000    # 0.5f

    add-float/2addr v3, v4

    float-to-int v3, v3

    div-int/lit8 v2, v3, 0xa

    .line 314
    const/16 v3, 0xc

    if-ne v2, v3, :cond_1

    .line 315
    const/4 v2, 0x0

    .line 316
    add-int/lit8 v0, v0, 0x1

    .line 318
    :cond_1
    const-string v3, "feet"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 319
    const-string v3, "inch"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    return-object v1
.end method

.method private getHeightTextValue()F
    .locals 7

    .prologue
    const/16 v6, 0x9

    const/high16 v3, -0x40800000    # -1.0f

    .line 561
    iget v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightUnit:I

    const v5, 0x249f1

    if-ne v4, v5, :cond_2

    .line 563
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 565
    .local v2, "value":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "."

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 589
    .end local v2    # "value":Ljava/lang/String;
    :cond_0
    :goto_0
    return v3

    .line 568
    .restart local v2    # "value":Ljava/lang/String;
    :cond_1
    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    goto :goto_0

    .line 573
    .end local v2    # "value":Ljava/lang/String;
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 574
    .local v0, "ftString":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 576
    .local v1, "inchString":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    .line 580
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_3

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/16 v5, 0x8

    if-lt v4, v5, :cond_0

    .line 582
    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    if-ne v4, v6, :cond_4

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/16 v5, 0xa

    if-gt v4, v5, :cond_0

    .line 584
    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    if-gt v4, v6, :cond_0

    .line 586
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/16 v5, 0xc

    if-ge v4, v5, :cond_0

    .line 589
    iget v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightValue:F

    goto :goto_0
.end method

.method private initLayout()V
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 100
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mContext:Landroid/content/Context;

    const v3, 0x7f0301e9

    invoke-static {v2, v3, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 102
    .local v1, "view":Landroid/view/View;
    new-array v2, v4, [Landroid/text/InputFilter;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mInputFilters:[Landroid/text/InputFilter;

    .line 103
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mInputFilters:[Landroid/text/InputFilter;

    sget-object v3, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->inputFilter:Landroid/text/InputFilter;

    aput-object v3, v2, v5

    .line 105
    new-array v2, v6, [Landroid/text/InputFilter;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mInputSecondFilters:[Landroid/text/InputFilter;

    .line 106
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mInputSecondFilters:[Landroid/text/InputFilter;

    sget-object v3, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->inputFilter:Landroid/text/InputFilter;

    aput-object v3, v2, v5

    .line 107
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mInputSecondFilters:[Landroid/text/InputFilter;

    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v3, v6}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v2, v4

    .line 109
    const v2, 0x7f08088b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    .line 110
    const v2, 0x7f08088d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    .line 111
    const v2, 0x7f08088c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    .line 113
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mInputFilters:[Landroid/text/InputFilter;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setFilters([Landroid/text/InputFilter;)V

    .line 114
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mInputSecondFilters:[Landroid/text/InputFilter;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setFilters([Landroid/text/InputFilter;)V

    .line 115
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mInputFilters:[Landroid/text/InputFilter;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setFilters([Landroid/text/InputFilter;)V

    .line 117
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mEditTextWatcher:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 118
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mEditTextWatcher:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 119
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mEditTextWatcher:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextHandler;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 121
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mEditTextFocusHandler:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextFocusHandler;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 122
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mEditTextFocusHandler:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextFocusHandler;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 123
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mEditTextFocusHandler:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextFocusHandler;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 124
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2, v7}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setImeOptions(I)V

    .line 125
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2, v7}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setImeOptions(I)V

    .line 126
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2, v7}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setImeOptions(I)V

    .line 127
    new-array v2, v6, [Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mPrimeTextView:[Landroid/widget/TextView;

    .line 128
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mPrimeTextView:[Landroid/widget/TextView;

    const v2, 0x7f0801a8

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v5

    .line 129
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mPrimeTextView:[Landroid/widget/TextView;

    const v2, 0x7f0801aa

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    .line 131
    new-array v0, v6, [Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mContext:Landroid/content/Context;

    const v3, 0x7f0900bc

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v5

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mContext:Landroid/content/Context;

    const v3, 0x7f0900bd

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    .line 132
    .local v0, "heightUnits":[Ljava/lang/String;
    const v2, 0x7f08088e

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

    .line 133
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

    const v3, 0x7f02086f

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;->setPopupBackgroundResource(I)V

    .line 134
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;->setDropDownVerticalOffset(I)V

    .line 135
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

    new-instance v3, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomBaseSpinnerAdapter;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

    const v6, 0x7f0301f1

    invoke-direct {v3, v4, v5, v6, v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomBaseSpinnerAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;I[Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 136
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

    new-instance v3, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$1;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 171
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

    new-instance v3, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$2;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 198
    return-void
.end method

.method private isHeighUnitCM()Z
    .locals 2

    .prologue
    .line 538
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightUnit:I

    const v1, 0x249f1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isHeightValidValue()F
    .locals 5

    .prologue
    const/high16 v2, -0x40800000    # -1.0f

    .line 543
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 544
    .local v0, "ftString":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 546
    .local v1, "inchString":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 554
    :cond_0
    :goto_0
    return v2

    .line 550
    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_2

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/16 v4, 0x8

    if-lt v3, v4, :cond_0

    .line 554
    :cond_2
    iget v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightValue:F

    goto :goto_0
.end method

.method private setDatalimit(Ljava/lang/String;I)V
    .locals 4
    .param p1, "preData"    # Ljava/lang/String;
    .param p2, "prePos"    # I

    .prologue
    .line 380
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->isHeighUnitCM()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 383
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 384
    :cond_0
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mMinValue:F

    .line 410
    :cond_1
    :goto_0
    return-void

    .line 386
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->convertComma(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    .line 388
    .local v0, "data":F
    iget v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mMaxValue:F

    cmpl-float v2, v0, v2

    if-lez v2, :cond_1

    .line 389
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 390
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2, p2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setSelection(I)V

    goto :goto_0

    .line 398
    .end local v0    # "data":F
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 399
    :cond_4
    const/4 v1, 0x0

    .line 404
    .local v1, "ftData":F
    :goto_1
    const/high16 v2, 0x42c60000    # 99.0f

    cmpl-float v2, v1, v2

    if-lez v2, :cond_1

    .line 405
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 406
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2, p2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setSelection(I)V

    goto :goto_0

    .line 401
    .end local v1    # "ftData":F
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    int-to-float v1, v2

    .restart local v1    # "ftData":F
    goto :goto_1
.end method

.method private setValueText(F)V
    .locals 2
    .param p1, "value"    # F

    .prologue
    const/high16 v1, 0x43960000    # 300.0f

    .line 217
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mMinValue:F

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    .line 218
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mMinValue:F

    iput v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightValue:F

    .line 224
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->heightValueConvert()V

    .line 225
    return-void

    .line 219
    :cond_0
    cmpl-float v0, p1, v1

    if-ltz v0, :cond_1

    .line 220
    iput v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightValue:F

    goto :goto_0

    .line 222
    :cond_1
    iput p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightValue:F

    goto :goto_0
.end method


# virtual methods
.method public checkChangeUnit()Z
    .locals 1

    .prologue
    .line 681
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->isChangeUnit:Z

    return v0
.end method

.method public checkChangeValue()Z
    .locals 1

    .prologue
    .line 677
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->isChangeValue:Z

    return v0
.end method

.method public checkValueText()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 596
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->getHeightTextValue()F

    move-result v0

    .line 598
    .local v0, "value":F
    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v2, v0, v2

    if-nez v2, :cond_1

    .line 608
    :cond_0
    :goto_0
    return v1

    .line 602
    :cond_1
    const/high16 v2, 0x41a00000    # 20.0f

    cmpg-float v2, v0, v2

    if-ltz v2, :cond_0

    const/high16 v2, 0x43960000    # 300.0f

    cmpl-float v2, v0, v2

    if-gtz v2, :cond_0

    .line 608
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getHeightUnit()I
    .locals 1

    .prologue
    .line 324
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightUnit:I

    return v0
.end method

.method public getHeightValue()F
    .locals 1

    .prologue
    .line 243
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightValue:F

    return v0
.end method

.method public getListValue()I
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;->getSelectedItemPosition()I

    move-result v0

    return v0
.end method

.method public heightValueConvert()V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 256
    iget v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightUnit:I

    const v6, 0x249f1

    if-ne v5, v6, :cond_3

    .line 257
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->getHeightValue()F

    move-result v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->convertDecimalFormat(F)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 258
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5, v8}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setVisibility(I)V

    .line 259
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5, v7}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setFocusable(Z)V

    .line 260
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5, v7}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setFocusableInTouchMode(Z)V

    .line 261
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->isFocused()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->isFocused()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 262
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->requestFocus()Z

    .line 264
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5, v9}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setVisibility(I)V

    .line 265
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5, v8}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setFocusable(Z)V

    .line 266
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5, v8}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setFocusableInTouchMode(Z)V

    .line 267
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5, v9}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setVisibility(I)V

    .line 268
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5, v8}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setFocusable(Z)V

    .line 269
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5, v8}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setFocusableInTouchMode(Z)V

    .line 270
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;->getId()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setNextFocusRightId(I)V

    .line 271
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getId()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;->setNextFocusLeftId(I)V

    .line 272
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a0590

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setMinWidth(I)V

    .line 273
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mPrimeTextView:[Landroid/widget/TextView;

    .local v0, "arr$":[Landroid/widget/TextView;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v4, v0, v2

    .line 274
    .local v4, "tv":Landroid/widget/TextView;
    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 273
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 276
    .end local v4    # "tv":Landroid/widget/TextView;
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->selectAll()V

    .line 304
    :goto_1
    return-void

    .line 278
    .end local v0    # "arr$":[Landroid/widget/TextView;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->getHeightValue()F

    move-result v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->getFeetandInch(F)Ljava/util/HashMap;

    move-result-object v1

    .line 279
    .local v1, "feetInch":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    const-string v6, "feet"

    invoke-virtual {v1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 280
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    const-string v6, "inch"

    invoke-virtual {v1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 281
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5, v8}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setVisibility(I)V

    .line 282
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5, v8}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setVisibility(I)V

    .line 283
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5, v7}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setFocusable(Z)V

    .line 284
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5, v7}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setFocusableInTouchMode(Z)V

    .line 285
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5, v7}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setFocusable(Z)V

    .line 286
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5, v7}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setFocusableInTouchMode(Z)V

    .line 287
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->isFocused()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 288
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->requestFocus()Z

    .line 290
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5, v9}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setVisibility(I)V

    .line 291
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5, v8}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setFocusable(Z)V

    .line 292
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5, v8}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setFocusableInTouchMode(Z)V

    .line 293
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getId()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setNextFocusRightId(I)V

    .line 294
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getId()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;->setNextFocusLeftId(I)V

    .line 295
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a058e

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setMinWidth(I)V

    .line 296
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mPrimeTextView:[Landroid/widget/TextView;

    .restart local v0    # "arr$":[Landroid/widget/TextView;
    array-length v3, v0

    .restart local v3    # "len$":I
    const/4 v2, 0x0

    .restart local v2    # "i$":I
    :goto_2
    if-ge v2, v3, :cond_5

    aget-object v4, v0, v2

    .line 297
    .restart local v4    # "tv":Landroid/widget/TextView;
    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 296
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 299
    .end local v4    # "tv":Landroid/widget/TextView;
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->selectAll()V

    goto/16 :goto_1
.end method

.method public hideHeightKeyboard()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 201
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mContext:Landroid/content/Context;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 202
    .local v0, "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    .line 203
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 204
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 205
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 207
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->clearFocus()V

    .line 208
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->clearFocus()V

    .line 209
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->clearFocus()V

    .line 210
    return-void
.end method

.method public setHeightProfileData(FI)V
    .locals 2
    .param p1, "value"    # F
    .param p2, "unit"    # I

    .prologue
    .line 227
    const/4 v0, 0x0

    invoke-static {p1, v0}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-eqz v0, :cond_0

    .line 228
    iput p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightValue:F

    .line 231
    :cond_0
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 232
    iput p2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightUnit:I

    .line 234
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightUnit:I

    const v1, 0x249f1

    if-ne v0, v1, :cond_2

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;->setSelection(I)V

    .line 240
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->heightValueConvert()V

    .line 241
    return-void

    .line 237
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;->setSelection(I)V

    goto :goto_0
.end method

.method public setHeightUnit(I)V
    .locals 0
    .param p1, "unit"    # I

    .prologue
    .line 328
    iput p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightUnit:I

    .line 329
    return-void
.end method

.method public setHeightValue()V
    .locals 6

    .prologue
    .line 341
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->isHeighUnitCM()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 343
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_2

    .line 344
    :cond_0
    const/high16 v0, 0x41a00000    # 20.0f

    .line 349
    .local v0, "data":F
    :goto_0
    iget v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightValue:F

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->floorToDigit(FI)F

    move-result v4

    cmpl-float v4, v4, v0

    if-eqz v4, :cond_1

    .line 351
    iput v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightValue:F

    .line 377
    .end local v0    # "data":F
    :cond_1
    :goto_1
    return-void

    .line 346
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->convertComma(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    .restart local v0    # "data":F
    goto :goto_0

    .line 358
    .end local v0    # "data":F
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_7

    .line 359
    :cond_4
    const/4 v1, 0x0

    .line 364
    .local v1, "feet":I
    :goto_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_8

    .line 365
    :cond_5
    const/16 v3, 0x8

    .line 370
    .local v3, "inch":I
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->getHeightValue()F

    move-result v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->getFeetandInch(F)Ljava/util/HashMap;

    move-result-object v2

    .line 371
    .local v2, "feetInch":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    const-string v4, "inch"

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v4, v3, :cond_6

    const-string v4, "feet"

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eq v4, v1, :cond_1

    .line 373
    :cond_6
    int-to-float v4, v1

    invoke-static {v4}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertCmToFeet(F)F

    move-result v4

    int-to-float v5, v3

    invoke-static {v5}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertCmToInch(F)F

    move-result v5

    add-float/2addr v4, v5

    iput v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightValue:F

    .line 374
    iget v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightValue:F

    const/4 v5, 0x5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->ceilToDigit(FI)F

    move-result v4

    iput v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightValue:F

    goto/16 :goto_1

    .line 361
    .end local v1    # "feet":I
    .end local v2    # "feetInch":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v3    # "inch":I
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .restart local v1    # "feet":I
    goto :goto_2

    .line 367
    :cond_8
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .restart local v3    # "inch":I
    goto :goto_3
.end method

.method public setListValue(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 336
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;->setSelection(I)V

    .line 337
    return-void
.end method

.method public setMinValueText()V
    .locals 9

    .prologue
    const/16 v8, 0xa

    const/4 v7, 0x0

    const/high16 v6, 0x41100000    # 9.0f

    .line 613
    iget v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightUnit:I

    const v5, 0x249f1

    if-ne v4, v5, :cond_3

    .line 617
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 618
    :cond_0
    iget v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mMinValue:F

    .line 623
    .local v3, "value":F
    :goto_0
    iget v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mMinValue:F

    cmpg-float v4, v3, v4

    if-gtz v4, :cond_2

    .line 624
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mMinValue:F

    invoke-static {v5}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 674
    .end local v3    # "value":F
    :goto_1
    return-void

    .line 620
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    .restart local v3    # "value":F
    goto :goto_0

    .line 626
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    const/high16 v5, 0x43960000    # 300.0f

    invoke-static {v5}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 634
    .end local v3    # "value":F
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 635
    :cond_4
    const/4 v1, 0x0

    .line 640
    .local v1, "feet":F
    :goto_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 641
    :cond_5
    const/high16 v2, 0x40e00000    # 7.0f

    .line 646
    .local v2, "inch":F
    :goto_3
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertCmToFeet(F)F

    move-result v4

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertCmToInch(F)F

    move-result v5

    add-float v0, v4, v5

    .line 648
    .local v0, "checkValue":F
    iget v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mMinValue:F

    cmpg-float v4, v0, v4

    if-gtz v4, :cond_9

    .line 649
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 650
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    const/16 v5, 0x8

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 669
    :cond_6
    :goto_4
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setSelection(I)V

    .line 670
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setSelection(I)V

    goto/16 :goto_1

    .line 637
    .end local v0    # "checkValue":F
    .end local v1    # "feet":F
    .end local v2    # "inch":F
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    .restart local v1    # "feet":F
    goto :goto_2

    .line 643
    :cond_8
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    .restart local v2    # "inch":F
    goto :goto_3

    .line 652
    .restart local v0    # "checkValue":F
    :cond_9
    cmpl-float v4, v1, v6

    if-nez v4, :cond_a

    const/high16 v4, 0x41200000    # 10.0f

    cmpl-float v4, v2, v4

    if-lez v4, :cond_a

    .line 654
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 656
    :cond_a
    cmpl-float v4, v1, v6

    if-lez v4, :cond_b

    .line 658
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    const/16 v5, 0x9

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 659
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 661
    :cond_b
    cmpg-float v4, v1, v6

    if-gez v4, :cond_c

    const/high16 v4, 0x41300000    # 11.0f

    cmpl-float v4, v2, v4

    if-lez v4, :cond_c

    .line 663
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    const/16 v5, 0xb

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 665
    :cond_c
    const/4 v4, 0x0

    cmpl-float v4, v1, v4

    if-nez v4, :cond_6

    .line 667
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4
.end method

.method public setOnProfileDialogListener(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mListener:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;

    .line 96
    return-void
.end method

.method public setValue(F)V
    .locals 0
    .param p1, "value"    # F

    .prologue
    .line 213
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->setValueText(F)V

    .line 214
    return-void
.end method
