.class Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;
.super Ljava/lang/Object;
.source "RestoreHelper2.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->startRestore()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field denominatorValue:I

.field endValue:I

.field startValue:I

.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

.field final synthetic val$restoreAfterBackup:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Z)V
    .locals 1

    .prologue
    .line 577
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    iput-boolean p2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->val$restoreAfterBackup:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 578
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->startValue:I

    .line 579
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->denominatorValue:I

    .line 580
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->endValue:I

    return-void
.end method


# virtual methods
.method public onFinished(II)V
    .locals 7
    .param p1, "dataSyncType"    # I
    .param p2, "error"    # I

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x32

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 629
    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "3.0 Restore onFinished. Type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 631
    if-nez p2, :cond_2

    .line 633
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->cignaBackupRestoreEnable:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$900(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 634
    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Calling cigna restore, 3.0 Restore onFinished."

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 635
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    const-string v1, "RESTORE"

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->cignaBackupRestore(Ljava/lang/String;)Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$1000(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Ljava/lang/String;)Z

    .line 692
    :goto_0
    return-void

    .line 637
    :cond_0
    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "not calling cigna restore as cigna is disabled, 3.0 Restore onFinished.[success]"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 638
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->updateProgressPopupValue(I)V

    .line 640
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mMigrationState:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$400(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)I

    move-result v0

    if-eqz v0, :cond_1

    .line 642
    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "3.0 Restore: Restore was completed after upgrade"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 643
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->showDelayedProgressPopupAndRestart(I)V

    goto :goto_0

    .line 647
    :cond_1
    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "3.0 Restore: Restore was completed, start find 2.5 import data"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 648
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->checkImportData()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    goto :goto_0

    .line 654
    :cond_2
    const/16 v0, 0x8

    if-ne p2, v0, :cond_4

    .line 656
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->stopBackupOrRestore()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$1100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    .line 658
    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "3.0 Restore error received : ShealthContentManager.ActivityListener.ERROR_PROFILE_NOT_EXIST"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 659
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->noData1:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$1202(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Z)Z

    .line 661
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mMigrationState:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$400(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)I

    move-result v0

    if-eqz v0, :cond_3

    .line 663
    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "it is not needed to import 2.x data, due to upgrade process."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 664
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->noData2:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$1302(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Z)Z

    .line 667
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setRestoreOngoing(Landroid/content/Context;Z)V

    .line 669
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setInitializationNeeded(Landroid/content/Context;Z)V

    .line 671
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setRestoreAgainHomePopup(Landroid/content/Context;Z)V

    .line 673
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->finish()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$1400(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    goto/16 :goto_0

    .line 677
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->checkImportData()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    goto/16 :goto_0

    .line 680
    :cond_4
    const/16 v0, 0x10

    if-ne p2, v0, :cond_5

    .line 682
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->STORAGE_LOW:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->ERROR_CODE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$702(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 683
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->onException(Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 687
    :cond_5
    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "3.0 Restore error received : else case"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 688
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->NETWORK_FAILURE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->ERROR_CODE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$702(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 689
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->onException(Ljava/lang/Exception;)V

    goto/16 :goto_0
.end method

.method public onProgress(II)V
    .locals 5
    .param p1, "dataSyncType"    # I
    .param p2, "percent"    # I

    .prologue
    .line 617
    const/4 v0, 0x0

    .line 618
    .local v0, "percentage":I
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->val$restoreAfterBackup:Z

    if-eqz v1, :cond_0

    .line 619
    const-wide v1, 0x3fe999999999999aL    # 0.8

    int-to-double v3, p2

    mul-double/2addr v1, v3

    iget v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->denominatorValue:I

    int-to-double v3, v3

    div-double/2addr v1, v3

    double-to-int v0, v1

    .line 623
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    iget v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->startValue:I

    add-int/2addr v2, v0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->updateProgressPopupValue(I)V

    .line 624
    return-void

    .line 621
    :cond_0
    const-wide v1, 0x3feccccccccccccdL    # 0.9

    int-to-double v3, p2

    mul-double/2addr v1, v3

    iget v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->denominatorValue:I

    int-to-double v3, v3

    div-double/2addr v1, v3

    double-to-int v0, v1

    goto :goto_0
.end method

.method public onStarted(IILjava/lang/String;)V
    .locals 4
    .param p1, "dataSyncType"    # I
    .param p2, "error"    # I
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 585
    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Restore Started. Type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 587
    if-nez p2, :cond_1

    .line 589
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->val$restoreAfterBackup:Z

    if-eqz v0, :cond_0

    .line 591
    const/16 v0, 0x19

    iput v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->startValue:I

    .line 592
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->denominatorValue:I

    .line 611
    :cond_0
    :goto_0
    return-void

    .line 594
    :cond_1
    const/16 v0, 0xb

    if-ne p2, v0, :cond_2

    .line 595
    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Restore, Samsung account issue occured"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->requestCheckValication(Landroid/content/Context;)V

    goto :goto_0

    .line 598
    :cond_2
    const/16 v0, 0x10

    if-ne p2, v0, :cond_3

    .line 600
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->STORAGE_LOW:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->ERROR_CODE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$702(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 601
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->onException(Ljava/lang/Exception;)V

    goto :goto_0

    .line 602
    :cond_3
    const/16 v0, 0x12

    if-ne p2, v0, :cond_4

    .line 603
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->SERVER_MAINTENANCE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->ERROR_CODE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$702(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 604
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->SERVER_ERROR_MESSAGE:Ljava/lang/String;
    invoke-static {v0, p3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$802(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Ljava/lang/String;)Ljava/lang/String;

    .line 605
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->onException(Ljava/lang/Exception;)V

    goto :goto_0

    .line 607
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->NETWORK_FAILURE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->ERROR_CODE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$702(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 608
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->onException(Ljava/lang/Exception;)V

    goto :goto_0
.end method
