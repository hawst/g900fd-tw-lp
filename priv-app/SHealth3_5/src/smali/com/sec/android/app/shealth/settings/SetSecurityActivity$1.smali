.class Lcom/sec/android/app/shealth/settings/SetSecurityActivity$1;
.super Ljava/lang/Object;
.source "SetSecurityActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/SetSecurityActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/SetSecurityActivity;)V
    .locals 0

    .prologue
    .line 274
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/SetSecurityActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 4
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 278
    sget-object v2, Lcom/sec/android/app/shealth/settings/SetSecurityActivity$4;->$SwitchMap$com$sec$android$app$shealth$common$commonui$dialog$DialogButtonType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 295
    :goto_0
    return-void

    .line 282
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.service.health.cp.serversync.syncadapter.authenticator.SHEALTH_ACCOUNT"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 283
    .local v0, "accountIntent":Landroid/content/Intent;
    const-string/jumbo v2, "packageName"

    const-string v3, "com.sec.android.app.shealth"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 284
    const/high16 v2, 0x4000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 285
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/SetSecurityActivity;

    const/16 v3, 0x3e7

    invoke-virtual {v2, v0, v3}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 288
    .end local v0    # "accountIntent":Landroid/content/Intent;
    :pswitch_1
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/SetSecurityActivity;

    const-class v3, Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 289
    .local v1, "pincodeIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/SetSecurityActivity;

    const/4 v3, 0x1

    # invokes: Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->setChangePinEnabled(Z)V
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->access$000(Lcom/sec/android/app/shealth/settings/SetSecurityActivity;Z)V

    .line 290
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/SetSecurityActivity;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 278
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
