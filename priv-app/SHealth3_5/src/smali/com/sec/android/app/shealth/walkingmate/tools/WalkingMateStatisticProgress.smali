.class public Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;
.super Landroid/widget/RelativeLayout;
.source "WalkingMateStatisticProgress.java"


# static fields
.field private static final DELAY:I = 0xa


# instance fields
.field private ValueYou:J

.field private Valueaverage:J

.field private mAverageProgress:Landroid/widget/ProgressBar;

.field private mAverageTextView:Landroid/widget/TextView;

.field private mBestProgress:Landroid/widget/ProgressBar;

.field private mBestTextView:Landroid/widget/TextView;

.field private mRunnable:Ljava/lang/Runnable;

.field private mStartValueBest:J

.field private mStartValueYou:J

.field private mStartValueaverage:J

.field private mYouProgress:Landroid/widget/ProgressBar;

.field private mYouTextView:Landroid/widget/TextView;

.field private maxValue:J

.field private syncObj:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v0, 0x0

    .line 49
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 40
    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueYou:J

    .line 42
    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueaverage:J

    .line 44
    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueBest:J

    .line 46
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->syncObj:Ljava/lang/Object;

    .line 50
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->init(Landroid/content/Context;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const-wide/16 v0, 0x0

    .line 54
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueYou:J

    .line 42
    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueaverage:J

    .line 44
    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueBest:J

    .line 46
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->syncObj:Ljava/lang/Object;

    .line 55
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->init(Landroid/content/Context;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const-wide/16 v0, 0x0

    .line 59
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueYou:J

    .line 42
    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueaverage:J

    .line 44
    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueBest:J

    .line 46
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->syncObj:Ljava/lang/Object;

    .line 60
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->init(Landroid/content/Context;)V

    .line 61
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->syncObj:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    .prologue
    .line 16
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->maxValue:J

    return-wide v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    .prologue
    .line 16
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->ValueYou:J

    return-wide v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    .prologue
    .line 16
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueYou:J

    return-wide v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;J)J
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;
    .param p1, "x1"    # J

    .prologue
    .line 16
    iput-wide p1, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueYou:J

    return-wide p1
.end method

.method static synthetic access$314(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;J)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;
    .param p1, "x1"    # J

    .prologue
    .line 16
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueYou:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueYou:J

    return-wide v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    .prologue
    .line 16
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->Valueaverage:J

    return-wide v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    .prologue
    .line 16
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueaverage:J

    return-wide v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;J)J
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;
    .param p1, "x1"    # J

    .prologue
    .line 16
    iput-wide p1, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueaverage:J

    return-wide p1
.end method

.method static synthetic access$514(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;J)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;
    .param p1, "x1"    # J

    .prologue
    .line 16
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueaverage:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueaverage:J

    return-wide v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    .prologue
    .line 16
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueBest:J

    return-wide v0
.end method

.method static synthetic access$614(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;J)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;
    .param p1, "x1"    # J

    .prologue
    .line 16
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueBest:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueBest:J

    return-wide v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mYouProgress:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mAverageProgress:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mBestProgress:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method private init(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v1, 0x0

    .line 64
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mContext:Landroid/content/Context;

    move-object v0, p0

    move-wide v3, v1

    move-wide v5, v1

    .line 65
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->setValues(JJJ)V

    .line 66
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->initProgressBar()V

    .line 67
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->initProgressValue(Landroid/content/Context;)V

    .line 68
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->setTextValue(Landroid/content/Context;)V

    .line 69
    return-void
.end method

.method private initProgressBar()V
    .locals 4

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f03028f

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 79
    .local v0, "progressBar":Landroid/view/View;
    const v1, 0x7f080b93

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mYouProgress:Landroid/widget/ProgressBar;

    .line 80
    const v1, 0x7f080b96

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mAverageProgress:Landroid/widget/ProgressBar;

    .line 81
    const v1, 0x7f080b99

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mBestProgress:Landroid/widget/ProgressBar;

    .line 87
    const v1, 0x7f080b94

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mYouTextView:Landroid/widget/TextView;

    .line 88
    const v1, 0x7f080b97

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mAverageTextView:Landroid/widget/TextView;

    .line 89
    const v1, 0x7f080b9a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mBestTextView:Landroid/widget/TextView;

    .line 90
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->addView(Landroid/view/View;)V

    .line 91
    return-void
.end method

.method private initProgressValue(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mYouProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mAverageProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mBestProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 105
    return-void
.end method

.method private setTextValue(Landroid/content/Context;)V
    .locals 14
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v12, 0x1

    const v11, 0x7f090baa

    const v10, 0x7f090b8d

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 112
    iget-wide v3, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->ValueYou:J

    cmp-long v3, v3, v12

    if-nez v3, :cond_0

    .line 113
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mYouTextView:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "1 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    :goto_0
    iget-wide v3, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->Valueaverage:J

    cmp-long v3, v3, v12

    if-nez v3, :cond_2

    .line 124
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mAverageTextView:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "1 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    :goto_1
    iget-wide v3, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->maxValue:J

    cmp-long v3, v3, v12

    if-nez v3, :cond_4

    .line 135
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mBestTextView:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "1 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    :goto_2
    return-void

    .line 115
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 116
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v9, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->ValueYou:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 121
    .local v2, "yourStepText":Ljava/lang/String;
    :goto_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mYouTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 118
    .end local v2    # "yourStepText":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v9, [Ljava/lang/Object;

    iget-wide v5, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->ValueYou:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "yourStepText":Ljava/lang/String;
    goto :goto_3

    .line 126
    .end local v2    # "yourStepText":Ljava/lang/String;
    :cond_2
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 127
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v9, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->Valueaverage:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 132
    .local v0, "averageStepText":Ljava/lang/String;
    :goto_4
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mAverageTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 129
    .end local v0    # "averageStepText":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v9, [Ljava/lang/Object;

    iget-wide v5, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->Valueaverage:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "averageStepText":Ljava/lang/String;
    goto :goto_4

    .line 137
    .end local v0    # "averageStepText":Ljava/lang/String;
    :cond_4
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 138
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v9, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->maxValue:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 143
    .local v1, "bestStepText":Ljava/lang/String;
    :goto_5
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mBestTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 140
    .end local v1    # "bestStepText":Ljava/lang/String;
    :cond_5
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v9, [Ljava/lang/Object;

    iget-wide v5, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->maxValue:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "bestStepText":Ljava/lang/String;
    goto :goto_5
.end method


# virtual methods
.method public AnimationStart()V
    .locals 3

    .prologue
    const-wide/16 v0, 0x0

    .line 148
    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueYou:J

    .line 149
    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueaverage:J

    .line 150
    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mStartValueBest:J

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mRunnable:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 153
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mRunnable:Ljava/lang/Runnable;

    .line 185
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->mRunnable:Ljava/lang/Runnable;

    const-wide/16 v1, 0xa

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 186
    return-void
.end method

.method public animateMe()V
    .locals 4

    .prologue
    .line 95
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->maxValue:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->AnimationStart()V

    .line 98
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->setTextValue(Landroid/content/Context;)V

    .line 99
    return-void
.end method

.method public setValues(JJJ)V
    .locals 0
    .param p1, "currentValue"    # J
    .param p3, "averageValue"    # J
    .param p5, "maxValue"    # J

    .prologue
    .line 72
    iput-wide p1, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->ValueYou:J

    .line 73
    iput-wide p3, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->Valueaverage:J

    .line 74
    iput-wide p5, p0, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingMateStatisticProgress;->maxValue:J

    .line 75
    return-void
.end method
