.class Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$8;
.super Ljava/lang/Object;
.source "SearchFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->findViews(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V
    .locals 0

    .prologue
    .line 393
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$8;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 399
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$8;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const v6, 0x7f0900ba

    invoke-virtual {v5, v6}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 400
    .local v1, "calories":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$8;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    iget-object v6, v6, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->searchEditText:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "UTF-8"

    invoke-static {v5, v6}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 407
    .local v4, "value":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isChinaModel()Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "http://www.baidu.com/s?word="

    :goto_0
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 408
    .local v3, "finalUrl":Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-direct {v0, v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 409
    .local v0, "browserIntent":Landroid/content/Intent;
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$8;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-virtual {v5, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->startActivity(Landroid/content/Intent;)V

    .line 410
    .end local v0    # "browserIntent":Landroid/content/Intent;
    .end local v1    # "calories":Ljava/lang/String;
    .end local v3    # "finalUrl":Ljava/lang/String;
    .end local v4    # "value":Ljava/lang/String;
    :goto_1
    return-void

    .line 401
    :catch_0
    move-exception v2

    .line 402
    .local v2, "e":Ljava/io/UnsupportedEncodingException;
    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$1300()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 407
    .end local v2    # "e":Ljava/io/UnsupportedEncodingException;
    .restart local v1    # "calories":Ljava/lang/String;
    .restart local v4    # "value":Ljava/lang/String;
    :cond_0
    const-string v5, "https://www.google.com/?#q="

    goto :goto_0
.end method
