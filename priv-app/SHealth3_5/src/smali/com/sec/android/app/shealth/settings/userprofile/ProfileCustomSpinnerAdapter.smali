.class public abstract Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinnerAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ProfileCustomSpinnerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;I[Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "spinner"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;
    .param p3, "resource"    # I
    .param p4, "objects"    # [Ljava/lang/String;

    .prologue
    .line 38
    invoke-direct {p0, p1, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 40
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinnerAdapter;->mContext:Landroid/content/Context;

    .line 41
    iput-object p2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinnerAdapter;->mSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

    .line 42
    return-void
.end method


# virtual methods
.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v3, 0x7f0700e3

    const/4 v4, 0x0

    .line 56
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinnerAdapter;->getSpinnerChildView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 58
    .local v1, "view":Landroid/view/View;
    const v2, 0x7f0808a4

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 59
    .local v0, "unitTxt":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinnerAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 61
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinnerAdapter;->mSpinner:Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinner;->getSelectedItemPosition()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 62
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinnerAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07007d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 63
    const-string/jumbo v2, "sans-serif"

    invoke-static {v2, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 70
    :goto_0
    return-object v1

    .line 66
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinnerAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 67
    const-string/jumbo v2, "sec-roboto-light"

    invoke-static {v2, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0
.end method

.method protected getSpinnerChildView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 92
    if-nez p2, :cond_0

    .line 93
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinnerAdapter;->mContext:Landroid/content/Context;

    const v3, 0x7f0301f0

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 96
    :cond_0
    const v2, 0x7f0808a4

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 97
    .local v1, "unitTxt":Landroid/widget/TextView;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 99
    .local v0, "unitString":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 101
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 103
    :cond_1
    return-object p2
.end method

.method protected getSpinnerGroupView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 75
    if-nez p2, :cond_0

    .line 76
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinnerAdapter;->mContext:Landroid/content/Context;

    const v3, 0x7f0301f1

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 79
    :cond_0
    const v2, 0x7f0808a6

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 80
    .local v1, "unitTxt":Landroid/widget/TextView;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 82
    .local v0, "unitString":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 84
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 87
    :cond_1
    return-object p2
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 48
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileCustomSpinnerAdapter;->getSpinnerGroupView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 50
    .local v0, "view":Landroid/view/View;
    return-object v0
.end method
