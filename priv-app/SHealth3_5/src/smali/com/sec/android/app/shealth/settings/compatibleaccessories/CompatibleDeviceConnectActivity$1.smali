.class Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$1;
.super Ljava/lang/Object;
.source "CompatibleDeviceConnectActivity.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$1;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 122
    const-string v0, "CompatibleDeviceConnectActivity"

    const-string v1, "Binding is done - Service connected"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    return-void
.end method

.method public onServiceDisconnected(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 128
    const-string v0, "CompatibleDeviceConnectActivity"

    const-string v1, "Service disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    return-void
.end method
