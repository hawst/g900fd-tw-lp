.class public Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;
.super Landroid/widget/LinearLayout;
.source "LogListChildRow.java"


# instance fields
.field private mBottomTextView:Landroid/widget/TextView;

.field private mCenterTextView:Landroid/widget/TextView;

.field private mDataRow:Landroid/view/View;

.field private mDeleteCheckBox:Landroid/widget/CheckBox;

.field private mItemDivider:Landroid/view/View;

.field private mLeftImageView:Landroid/widget/ImageView;

.field private mLeftTeamHeaderTextView:Landroid/widget/TextView;

.field private mLeftTextView:Landroid/widget/TextView;

.field private mMedalImageView:Landroid/widget/ImageView;

.field private mRightTeamHeaderTextView:Landroid/widget/TextView;

.field private mRightTextView:Landroid/widget/TextView;

.field private mTeamHeader:Landroid/view/View;

.field private mTopRightTextView:Landroid/widget/TextView;

.field private memoImageView:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    return-void
.end method

.method private getTeamHeader()Landroid/view/View;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mTeamHeader:Landroid/view/View;

    if-nez v0, :cond_0

    .line 201
    sget v0, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_team_header:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mTeamHeader:Landroid/view/View;

    .line 203
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mTeamHeader:Landroid/view/View;

    return-object v0
.end method

.method private setViewVisibility(Landroid/view/View;I)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "visible"    # I

    .prologue
    .line 381
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 382
    return-void
.end method

.method private setViewVisibility(Landroid/view/View;Z)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "visible"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 322
    if-eqz p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 323
    return-void

    .line 322
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method


# virtual methods
.method public getBottomTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mBottomTextView:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 125
    sget v0, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_bottom_text:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mBottomTextView:Landroid/widget/TextView;

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mBottomTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method public getCenterTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mCenterTextView:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 89
    sget v0, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_center_text:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mCenterTextView:Landroid/widget/TextView;

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mCenterTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method public getDeleteCheckBox()Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mDeleteCheckBox:Landroid/widget/CheckBox;

    if-nez v0, :cond_0

    .line 137
    sget v0, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_delete_check_box:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mDeleteCheckBox:Landroid/widget/CheckBox;

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mDeleteCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method public getDividerView()Landroid/view/View;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mItemDivider:Landroid/view/View;

    if-nez v0, :cond_0

    .line 173
    sget v0, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_item_divider:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mItemDivider:Landroid/view/View;

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mItemDivider:Landroid/view/View;

    return-object v0
.end method

.method public getLeftImageView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mLeftImageView:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 65
    sget v0, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_row_left_image:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mLeftImageView:Landroid/widget/ImageView;

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mLeftImageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getLeftTeamHeaderTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mLeftTeamHeaderTextView:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 149
    sget v0, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_team_header_left_text_view:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mLeftTeamHeaderTextView:Landroid/widget/TextView;

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mLeftTeamHeaderTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method public getLeftTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mLeftTextView:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 77
    sget v0, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_left_text:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mLeftTextView:Landroid/widget/TextView;

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mLeftTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method public getMemoImageVisible()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->memoImageView:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 265
    sget v0, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_memo_image:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->memoImageView:Landroid/widget/ImageView;

    .line 267
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->memoImageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getRightTeamHeaderTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mRightTeamHeaderTextView:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 161
    sget v0, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_team_header_right_text_view:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mRightTeamHeaderTextView:Landroid/widget/TextView;

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mRightTeamHeaderTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method public getRightTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mRightTextView:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 101
    sget v0, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_right_text:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mRightTextView:Landroid/widget/TextView;

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mRightTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method public getTopRightTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mTopRightTextView:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 113
    sget v0, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_top_right_text:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mTopRightTextView:Landroid/widget/TextView;

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mTopRightTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method getViewWithoutTeamHeader()Landroid/view/View;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mDataRow:Landroid/view/View;

    if-nez v0, :cond_0

    .line 211
    sget v0, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_data_row:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mDataRow:Landroid/view/View;

    .line 213
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mDataRow:Landroid/view/View;

    return-object v0
.end method

.method public initMedalImageView(IZ)Landroid/widget/ImageView;
    .locals 3
    .param p1, "imageResourceId"    # I
    .param p2, "isTeamHeaderMedal"    # Z

    .prologue
    .line 226
    if-eqz p2, :cond_2

    sget v0, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_team_header_medal_image:I

    .line 228
    .local v0, "medalViewId":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mMedalImageView:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mMedalImageView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getId()I

    move-result v1

    if-eq v1, v0, :cond_1

    .line 229
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mMedalImageView:Landroid/widget/ImageView;

    .line 231
    :cond_1
    const/4 v1, -0x1

    if-ne p1, v1, :cond_3

    .line 232
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mMedalImageView:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 236
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mMedalImageView:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 237
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mMedalImageView:Landroid/widget/ImageView;

    return-object v1

    .line 226
    .end local v0    # "medalViewId":I
    :cond_2
    sget v0, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_data_row_medal_image:I

    goto :goto_0

    .line 234
    .restart local v0    # "medalViewId":I
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->mMedalImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method

.method public setAccessoryImageVisible(I)V
    .locals 1
    .param p1, "visible"    # I

    .prologue
    .line 331
    sget v0, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_accessory_image:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->setViewVisibility(Landroid/view/View;I)V

    .line 332
    return-void
.end method

.method public setAccessoryImageVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 248
    sget v0, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_accessory_image:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->setViewVisibility(Landroid/view/View;Z)V

    .line 249
    return-void
.end method

.method public setBottomTextViewVisible(I)V
    .locals 6
    .param p1, "visible"    # I

    .prologue
    const/4 v5, 0x0

    .line 358
    sget v3, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_bottom_text:I

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-direct {p0, v3, p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->setViewVisibility(Landroid/view/View;I)V

    .line 360
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    if-nez p1, :cond_1

    sget v3, Lcom/sec/android/app/shealth/logutils/R$dimen;->log_list_row_left_text_with_icon_margin_left:I

    :goto_0
    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 363
    .local v2, "textMarginLeft":I
    sget v3, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_row_left_text_container:I

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 364
    .local v1, "rowTextContainer":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object v0, v3

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 365
    .local v0, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    if-eq v2, v3, :cond_0

    .line 366
    invoke-virtual {v0, v2, v5, v5, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 367
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 369
    :cond_0
    return-void

    .line 360
    .end local v0    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v1    # "rowTextContainer":Landroid/view/View;
    .end local v2    # "textMarginLeft":I
    :cond_1
    sget v3, Lcom/sec/android/app/shealth/logutils/R$dimen;->log_list_row_left_text_margin_left:I

    goto :goto_0
.end method

.method public setBottomTextViewVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 299
    sget v0, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_bottom_text:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->setViewVisibility(Landroid/view/View;Z)V

    .line 300
    return-void
.end method

.method public setDividerVisible(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 196
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getDividerView()Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 197
    return-void

    .line 196
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setHeadGoalVisibility(I)V
    .locals 1
    .param p1, "visible"    # I

    .prologue
    .line 377
    sget v0, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_team_header_medal_image:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->setViewVisibility(Landroid/view/View;I)V

    .line 378
    return-void
.end method

.method public setHeadGoalVisibility(Z)V
    .locals 1
    .param p1, "visible"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 310
    sget v0, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_team_header_medal_image:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->setViewVisibility(Landroid/view/View;Z)V

    .line 311
    return-void
.end method

.method public setLeftImageVisible(I)V
    .locals 1
    .param p1, "visible"    # I

    .prologue
    .line 349
    sget v0, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_row_left_image:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->setViewVisibility(Landroid/view/View;I)V

    .line 350
    return-void
.end method

.method public setLeftImageVisible(Z)V
    .locals 6
    .param p1, "visible"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 278
    sget v3, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_row_left_image:I

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-direct {p0, v3, p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->setViewVisibility(Landroid/view/View;Z)V

    .line 280
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    if-eqz p1, :cond_1

    sget v3, Lcom/sec/android/app/shealth/logutils/R$dimen;->log_list_row_left_text_with_icon_margin_left:I

    :goto_0
    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 283
    .local v2, "textMarginLeft":I
    sget v3, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_row_left_text_container:I

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 284
    .local v1, "rowTextContainer":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object v0, v3

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 285
    .local v0, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    if-eq v2, v3, :cond_0

    .line 286
    invoke-virtual {v0, v2, v5, v5, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 287
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 289
    :cond_0
    return-void

    .line 280
    .end local v0    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v1    # "rowTextContainer":Landroid/view/View;
    .end local v2    # "textMarginLeft":I
    :cond_1
    sget v3, Lcom/sec/android/app/shealth/logutils/R$dimen;->log_list_row_left_text_margin_left:I

    goto :goto_0
.end method

.method public setLeftTextViewMarginForText()V
    .locals 5

    .prologue
    .line 385
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/sec/android/app/shealth/logutils/R$dimen;->log_list_data_row_height_weight:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 386
    .local v0, "containerHeight":I
    sget v3, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_data_row:I

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 387
    .local v2, "rowContainer":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object v1, v3

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 388
    .local v1, "paramsC":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    if-eq v0, v3, :cond_0

    .line 389
    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 390
    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 392
    :cond_0
    return-void
.end method

.method public setMemoImageVisible(I)V
    .locals 1
    .param p1, "visible"    # I

    .prologue
    .line 340
    sget v0, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_memo_image:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->setViewVisibility(Landroid/view/View;I)V

    .line 341
    return-void
.end method

.method public setMemoImageVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 259
    sget v0, Lcom/sec/android/app/shealth/logutils/R$id;->log_list_memo_image:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->setViewVisibility(Landroid/view/View;Z)V

    .line 260
    return-void
.end method

.method public setTeamHeaderVisible(Z)V
    .locals 4
    .param p1, "visible"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 186
    invoke-direct {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getTeamHeader()Landroid/view/View;

    move-result-object v3

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 187
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getDividerView()Landroid/view/View;

    move-result-object v0

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 188
    return-void

    :cond_0
    move v0, v2

    .line 186
    goto :goto_0

    :cond_1
    move v2, v1

    .line 187
    goto :goto_1
.end method
