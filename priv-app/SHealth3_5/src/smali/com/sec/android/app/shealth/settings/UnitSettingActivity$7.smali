.class Lcom/sec/android/app/shealth/settings/UnitSettingActivity$7;
.super Ljava/lang/Object;
.source "UnitSettingActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V
    .locals 0

    .prologue
    .line 565
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSave(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 569
    # getter for: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$600()Lcom/samsung/android/sdk/health/content/ShealthProfile;

    move-result-object v1

    if-nez v1, :cond_0

    .line 570
    new-instance v1, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$602(Lcom/samsung/android/sdk/health/content/ShealthProfile;)Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 573
    :cond_0
    if-nez p1, :cond_1

    .line 574
    const-string v0, "C"

    .line 575
    .local v0, "temperatureUnit":Ljava/lang/String;
    # getter for: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$600()Lcom/samsung/android/sdk/health/content/ShealthProfile;

    move-result-object v1

    const v2, 0x27101

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setTemperatureUnit(I)V

    .line 580
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$700(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->putTemperatureUnit(Ljava/lang/String;)V

    .line 581
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->saveShealthProfile()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$800(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V

    .line 582
    return-void

    .line 577
    .end local v0    # "temperatureUnit":Ljava/lang/String;
    :cond_1
    const-string v0, "F"

    .line 578
    .restart local v0    # "temperatureUnit":Ljava/lang/String;
    # getter for: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$600()Lcom/samsung/android/sdk/health/content/ShealthProfile;

    move-result-object v1

    const v2, 0x27102

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setTemperatureUnit(I)V

    goto :goto_0
.end method
