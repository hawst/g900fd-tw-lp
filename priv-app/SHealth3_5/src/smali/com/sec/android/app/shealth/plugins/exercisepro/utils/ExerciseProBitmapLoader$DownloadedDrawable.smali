.class Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$DownloadedDrawable;
.super Landroid/graphics/drawable/ColorDrawable;
.source "ExerciseProBitmapLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DownloadedDrawable"
.end annotation


# instance fields
.field private final bitmapDownloaderTaskReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;)V
    .locals 1
    .param p1, "bitmapDownloaderTask"    # Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;

    .prologue
    .line 137
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 138
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$DownloadedDrawable;->bitmapDownloaderTaskReference:Ljava/lang/ref/WeakReference;

    .line 139
    return-void
.end method


# virtual methods
.method public getBitmapDownloaderTask()Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$DownloadedDrawable;->bitmapDownloaderTaskReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$BitmapDownloaderTask;

    return-object v0
.end method
