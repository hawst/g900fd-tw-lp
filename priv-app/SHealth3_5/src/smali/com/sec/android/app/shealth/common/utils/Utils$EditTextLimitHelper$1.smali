.class Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelper$1;
.super Landroid/text/InputFilter$LengthFilter;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelper;->addLimit(ILandroid/widget/EditText;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelper;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelper;I)V
    .locals 0
    .param p2, "x0"    # I

    .prologue
    .line 270
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelper$1;->this$0:Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelper;

    invoke-direct {p0, p2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 3
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    .line 273
    invoke-super/range {p0 .. p6}, Landroid/text/InputFilter$LengthFilter;->filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 274
    .local v0, "res":Ljava/lang/CharSequence;
    if-eqz v0, :cond_0

    .line 275
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelper$1;->this$0:Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelper;->onLimitExceeded()V

    .line 277
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelper$1;->this$0:Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelper;

    iget v1, v1, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelper;->xmlLengthFilterPosition:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    const/4 v0, 0x0

    .end local v0    # "res":Ljava/lang/CharSequence;
    :cond_1
    return-object v0
.end method
