.class Lcom/sec/android/app/shealth/home/HomeFragment$RegistryContentObserver$1;
.super Ljava/lang/Object;
.source "HomeFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/home/HomeFragment$RegistryContentObserver;->onChange(ZLandroid/net/Uri;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/home/HomeFragment$RegistryContentObserver;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/HomeFragment$RegistryContentObserver;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/HomeFragment$RegistryContentObserver$1;->this$1:Lcom/sec/android/app/shealth/home/HomeFragment$RegistryContentObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 210
    # getter for: Lcom/sec/android/app/shealth/home/HomeFragment;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/home/HomeFragment;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RegistryContentObserver "

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeFragment$RegistryContentObserver$1;->this$1:Lcom/sec/android/app/shealth/home/HomeFragment$RegistryContentObserver;

    iget-object v0, v0, Lcom/sec/android/app/shealth/home/HomeFragment$RegistryContentObserver;->this$0:Lcom/sec/android/app/shealth/home/HomeFragment;

    # getter for: Lcom/sec/android/app/shealth/home/HomeFragment;->homeshortcuts:Lcom/sec/android/app/shealth/home/HomeShortcutManager;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/HomeFragment;->access$200(Lcom/sec/android/app/shealth/home/HomeFragment;)Lcom/sec/android/app/shealth/home/HomeShortcutManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeFragment$RegistryContentObserver$1;->this$1:Lcom/sec/android/app/shealth/home/HomeFragment$RegistryContentObserver;

    iget-object v0, v0, Lcom/sec/android/app/shealth/home/HomeFragment$RegistryContentObserver;->this$0:Lcom/sec/android/app/shealth/home/HomeFragment;

    # invokes: Lcom/sec/android/app/shealth/home/HomeFragment;->isGearDevice()Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/HomeFragment;->access$300(Lcom/sec/android/app/shealth/home/HomeFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeFragment$RegistryContentObserver$1;->this$1:Lcom/sec/android/app/shealth/home/HomeFragment$RegistryContentObserver;

    iget-object v0, v0, Lcom/sec/android/app/shealth/home/HomeFragment$RegistryContentObserver;->this$0:Lcom/sec/android/app/shealth/home/HomeFragment;

    # getter for: Lcom/sec/android/app/shealth/home/HomeFragment;->homeshortcuts:Lcom/sec/android/app/shealth/home/HomeShortcutManager;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/HomeFragment;->access$200(Lcom/sec/android/app/shealth/home/HomeFragment;)Lcom/sec/android/app/shealth/home/HomeShortcutManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->refresh()V

    .line 214
    :cond_0
    return-void
.end method
