.class final Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog$2;
.super Ljava/lang/Object;
.source "ShareViaDialog.java"

# interfaces
.implements Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->showShareViaDialog(Landroid/content/Context;Landroid/graphics/Bitmap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field private msc:Landroid/media/MediaScannerConnection;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$filePath:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 157
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog$2;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog$2;->val$filePath:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog$2;->msc:Landroid/media/MediaScannerConnection;

    .line 161
    new-instance v1, Landroid/media/MediaScannerConnection;

    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog$2;->val$context:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-direct {v1, v0, p0}, Landroid/media/MediaScannerConnection;-><init>(Landroid/content/Context;Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog$2;->msc:Landroid/media/MediaScannerConnection;

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog$2;->msc:Landroid/media/MediaScannerConnection;

    invoke-virtual {v0}, Landroid/media/MediaScannerConnection;->connect()V

    .line 163
    return-void
.end method


# virtual methods
.method public onMediaScannerConnected()V
    .locals 3

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog$2;->msc:Landroid/media/MediaScannerConnection;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog$2;->val$filePath:Ljava/lang/String;

    const-string v2, "image/jpeg"

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaScannerConnection;->scanFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    return-void
.end method

.method public onScanCompleted(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog$2;->val$context:Landroid/content/Context;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->createAndShowShareViaPopup(Landroid/content/Context;)V
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->access$200(Landroid/content/Context;)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog$2;->msc:Landroid/media/MediaScannerConnection;

    invoke-virtual {v0}, Landroid/media/MediaScannerConnection;->disconnect()V

    .line 176
    return-void
.end method
