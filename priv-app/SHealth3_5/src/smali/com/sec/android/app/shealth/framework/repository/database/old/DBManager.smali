.class public Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;
.super Ljava/lang/Object;
.source "DBManager.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static database:Landroid/database/sqlite/SQLiteDatabase;

.field private static instance:Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;

.field public static mAppName:Ljava/lang/String;

.field private static volatile mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;->TAG:Ljava/lang/String;

    .line 20
    const-string v0, "SHealth2"

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;->mAppName:Ljava/lang/String;

    .line 21
    new-instance v0, Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;->instance:Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;

    .line 22
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;->database:Landroid/database/sqlite/SQLiteDatabase;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;->TAG:Ljava/lang/String;

    const-string v1, "created"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;
    .locals 2

    .prologue
    .line 57
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;->TAG:Ljava/lang/String;

    const-string v1, "DBManager getInstance"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;->instance:Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;

    return-object v0
.end method

.method public static initSQLiteDB()V
    .locals 16

    .prologue
    .line 64
    sget-object v14, Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;->TAG:Ljava/lang/String;

    const-string v15, "initSQLiteDB"

    invoke-static {v14, v15}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    sget-object v14, Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v11

    .line 67
    .local v11, "manager":Landroid/content/res/AssetManager;
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->checkDataBasePath()V

    .line 70
    :try_start_0
    new-instance v7, Ljava/io/File;

    const-string v14, "/data/data/com.sec.android.app.shealth/databases/"

    invoke-direct {v7, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 71
    .local v7, "folder":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_0

    .line 72
    invoke-virtual {v7}, Ljava/io/File;->mkdir()Z

    .line 75
    :cond_0
    const-string v6, "/data/data/com.sec.android.app.shealth/databases/secure_shealth2.db"

    .line 76
    .local v6, "filePath":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 77
    .local v5, "file":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v14

    if-eqz v14, :cond_2

    .line 78
    sget-object v14, Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;->TAG:Ljava/lang/String;

    const-string/jumbo v15, "secure exist"

    invoke-static {v14, v15}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->checkDataBasePath()V

    .line 143
    .end local v5    # "file":Ljava/io/File;
    .end local v6    # "filePath":Ljava/lang/String;
    .end local v7    # "folder":Ljava/io/File;
    :cond_1
    :goto_0
    return-void

    .line 82
    .restart local v5    # "file":Ljava/io/File;
    .restart local v6    # "filePath":Ljava/lang/String;
    .restart local v7    # "folder":Ljava/io/File;
    :cond_2
    sget-object v14, Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;->TAG:Ljava/lang/String;

    const-string/jumbo v15, "secure not exist"

    invoke-static {v14, v15}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    const-string v6, "/data/data/com.sec.android.app.shealth/databases/shealth2.db"

    .line 85
    new-instance v5, Ljava/io/File;

    .end local v5    # "file":Ljava/io/File;
    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 86
    .restart local v5    # "file":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v14

    if-eqz v14, :cond_3

    .line 87
    sget-object v14, Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;->TAG:Ljava/lang/String;

    const-string v15, "db exist"

    invoke-static {v14, v15}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 138
    .end local v5    # "file":Ljava/io/File;
    .end local v6    # "filePath":Ljava/lang/String;
    .end local v7    # "folder":Ljava/io/File;
    :catch_0
    move-exception v4

    .line 140
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 141
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v14

    invoke-static {v14}, Landroid/os/Process;->killProcess(I)V

    goto :goto_0

    .line 96
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v5    # "file":Ljava/io/File;
    .restart local v6    # "filePath":Ljava/lang/String;
    .restart local v7    # "folder":Ljava/io/File;
    :cond_3
    :try_start_1
    sget-object v14, Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;->TAG:Ljava/lang/String;

    const-string v15, "file not exists"

    invoke-static {v14, v15}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 97
    const/4 v8, 0x0

    .line 98
    .local v8, "fos":Ljava/io/FileOutputStream;
    const/4 v10, 0x0

    .line 99
    .local v10, "is":Ljava/io/InputStream;
    const/4 v0, 0x0

    .line 101
    .local v0, "bis":Ljava/io/BufferedInputStream;
    :try_start_2
    const-string/jumbo v14, "shealth2.db"

    invoke-virtual {v11, v14}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v10

    .line 102
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, v10}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_b
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 103
    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .local v1, "bis":Ljava/io/BufferedInputStream;
    :try_start_3
    const-string v13, "/data/data/com.sec.android.app.shealth/databases/shealth2.db"

    .line 104
    .local v13, "outFilePath":Ljava/lang/String;
    new-instance v12, Ljava/io/File;

    invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 105
    .local v12, "outFile":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->createNewFile()Z

    .line 106
    new-instance v9, Ljava/io/FileOutputStream;

    invoke-direct {v9, v12}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_c
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 108
    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .local v9, "fos":Ljava/io/FileOutputStream;
    const/16 v14, 0x400

    :try_start_4
    new-array v2, v14, [B

    .line 109
    .local v2, "buffer":[B
    :goto_1
    invoke-virtual {v1, v2}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v3

    .local v3, "count":I
    const/4 v14, -0x1

    if-eq v3, v14, :cond_6

    .line 110
    const/4 v14, 0x0

    invoke-virtual {v9, v2, v14, v3}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_1

    .line 115
    .end local v2    # "buffer":[B
    .end local v3    # "count":I
    :catch_1
    move-exception v4

    move-object v0, v1

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    move-object v8, v9

    .line 116
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .end local v12    # "outFile":Ljava/io/File;
    .end local v13    # "outFilePath":Ljava/lang/String;
    .local v4, "e":Ljava/io/IOException;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    :goto_2
    :try_start_5
    sget-object v14, Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;->TAG:Ljava/lang/String;

    const-string v15, "Can\'t copy database."

    invoke-static {v14, v15, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 118
    if-eqz v8, :cond_4

    .line 120
    :try_start_6
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    .line 124
    :cond_4
    :goto_3
    if-eqz v0, :cond_5

    .line 126
    :try_start_7
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    .line 130
    :cond_5
    :goto_4
    if-eqz v10, :cond_1

    .line 132
    :try_start_8
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    goto :goto_0

    .line 133
    :catch_2
    move-exception v14

    goto :goto_0

    .line 112
    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .end local v4    # "e":Ljava/io/IOException;
    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v2    # "buffer":[B
    .restart local v3    # "count":I
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v12    # "outFile":Ljava/io/File;
    .restart local v13    # "outFilePath":Ljava/lang/String;
    :cond_6
    :try_start_9
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->flush()V

    .line 113
    sget-object v14, Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;->TAG:Ljava/lang/String;

    const-string v15, "created db"

    invoke-static {v14, v15}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 118
    if-eqz v9, :cond_7

    .line 120
    :try_start_a
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0

    .line 124
    :cond_7
    :goto_5
    if-eqz v1, :cond_8

    .line 126
    :try_start_b
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_5
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_0

    .line 130
    :cond_8
    :goto_6
    if-eqz v10, :cond_c

    .line 132
    :try_start_c
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_3
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_0

    move-object v0, v1

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    move-object v8, v9

    .line 134
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_0

    .line 133
    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v14

    move-object v0, v1

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    move-object v8, v9

    .line 134
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_0

    .line 118
    .end local v2    # "buffer":[B
    .end local v3    # "count":I
    .end local v12    # "outFile":Ljava/io/File;
    .end local v13    # "outFilePath":Ljava/lang/String;
    :catchall_0
    move-exception v14

    :goto_7
    if-eqz v8, :cond_9

    .line 120
    :try_start_d
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_8
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_0

    .line 124
    :cond_9
    :goto_8
    if-eqz v0, :cond_a

    .line 126
    :try_start_e
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_9
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_0

    .line 130
    :cond_a
    :goto_9
    if-eqz v10, :cond_b

    .line 132
    :try_start_f
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_a
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_0

    .line 134
    :cond_b
    :goto_a
    :try_start_10
    throw v14
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_0

    .line 121
    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v2    # "buffer":[B
    .restart local v3    # "count":I
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v12    # "outFile":Ljava/io/File;
    .restart local v13    # "outFilePath":Ljava/lang/String;
    :catch_4
    move-exception v14

    goto :goto_5

    .line 127
    :catch_5
    move-exception v14

    goto :goto_6

    .line 121
    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .end local v2    # "buffer":[B
    .end local v3    # "count":I
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .end local v12    # "outFile":Ljava/io/File;
    .end local v13    # "outFilePath":Ljava/lang/String;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    .restart local v4    # "e":Ljava/io/IOException;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v14

    goto :goto_3

    .line 127
    :catch_7
    move-exception v14

    goto :goto_4

    .line 121
    .end local v4    # "e":Ljava/io/IOException;
    :catch_8
    move-exception v15

    goto :goto_8

    .line 127
    :catch_9
    move-exception v15

    goto :goto_9

    .line 133
    :catch_a
    move-exception v15

    goto :goto_a

    .line 118
    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    :catchall_1
    move-exception v14

    move-object v0, v1

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    goto :goto_7

    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v12    # "outFile":Ljava/io/File;
    .restart local v13    # "outFilePath":Ljava/lang/String;
    :catchall_2
    move-exception v14

    move-object v0, v1

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    move-object v8, v9

    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    goto :goto_7

    .line 115
    .end local v12    # "outFile":Ljava/io/File;
    .end local v13    # "outFilePath":Ljava/lang/String;
    :catch_b
    move-exception v4

    goto :goto_2

    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    :catch_c
    move-exception v4

    move-object v0, v1

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    goto :goto_2

    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v2    # "buffer":[B
    .restart local v3    # "count":I
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v12    # "outFile":Ljava/io/File;
    .restart local v13    # "outFilePath":Ljava/lang/String;
    :cond_c
    move-object v0, v1

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    move-object v8, v9

    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_0
.end method


# virtual methods
.method public getAppContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public declared-synchronized getDataBase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 2

    .prologue
    .line 151
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;->database:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v0, :cond_0

    .line 152
    new-instance v0, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;

    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/repository/database/old/DatabaseHelper;->openDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;->database:Landroid/database/sqlite/SQLiteDatabase;

    .line 154
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;->database:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 151
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public initialize(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appName"    # Ljava/lang/String;

    .prologue
    .line 36
    sput-object p1, Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;->mContext:Landroid/content/Context;

    .line 37
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;->initSQLiteDB()V

    .line 39
    sput-object p2, Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;->mAppName:Ljava/lang/String;

    .line 40
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 158
    sput-object p1, Lcom/sec/android/app/shealth/framework/repository/database/old/DBManager;->mContext:Landroid/content/Context;

    .line 160
    return-void
.end method
