.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$8$1;
.super Ljava/lang/Object;
.source "ExerciseProWorkoutInfoActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$8;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$8;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$8;)V
    .locals 0

    .prologue
    .line 511
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$8$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 5
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 514
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$8$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$8;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$8$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$8;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->mExerciseRowId:J
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;)J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;->deleteItem(Landroid/content/Context;J)J

    move-result-wide v0

    .line 515
    .local v0, "result":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 516
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$8$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$8;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->finish()V

    .line 517
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$8$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$8;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->setResult(I)V

    .line 520
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$8$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$8;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;->finish()V

    .line 521
    return-void
.end method
