.class public Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
.super Ljava/lang/Object;
.source "AssessmentData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData$1;
    }
.end annotation


# static fields
.field public static final NO_DATA:I = -0x80000000


# instance fields
.field private mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

.field private mIconResID:I

.field private mQADataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;",
            ">;"
        }
    .end annotation
.end field

.field private mQuestionsAnswersGroupID:I

.field private mQuestionsAnswersGroupKey:I


# direct methods
.method public constructor <init>(ILcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)V
    .locals 1
    .param p1, "icon"    # I
    .param p2, "categoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    .prologue
    const/high16 v0, -0x80000000

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->mQuestionsAnswersGroupKey:I

    .line 16
    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->mQuestionsAnswersGroupID:I

    .line 20
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->mIconResID:I

    .line 21
    iput-object p2, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->mQADataList:Ljava/util/ArrayList;

    .line 23
    return-void
.end method


# virtual methods
.method public addQAData(Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;)V
    .locals 1
    .param p1, "assessmentQAData"    # Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->mQADataList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 66
    return-void
.end method

.method public getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .locals 2

    .prologue
    .line 30
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData$1;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryTypeRequest:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    invoke-virtual {v1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 45
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 32
    :pswitch_0
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->NUTRITION:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    goto :goto_0

    .line 34
    :pswitch_1
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->PHYSICAL_ACTIVITY:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    goto :goto_0

    .line 36
    :pswitch_2
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->SLEEP:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    goto :goto_0

    .line 38
    :pswitch_3
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->STRESS:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    goto :goto_0

    .line 40
    :pswitch_4
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    goto :goto_0

    .line 30
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public getQAData(I)Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->mQADataList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

    return-object v0
.end method

.method public getQADataCount()I
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->mQADataList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public setGroupID(I)V
    .locals 0
    .param p1, "questionsAnswersGroupID"    # I

    .prologue
    .line 57
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->mQuestionsAnswersGroupID:I

    .line 58
    return-void
.end method
