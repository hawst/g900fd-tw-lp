.class Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$3;
.super Ljava/lang/Object;
.source "CompatibleDeviceConnectActivity.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;)V
    .locals 0

    .prologue
    .line 377
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeviceFound(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V
    .locals 4
    .param p1, "device"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .prologue
    .line 381
    if-eqz p1, :cond_2

    .line 383
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v1

    .line 385
    .local v1, "name":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->mAccessoryDetails:Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->getModelName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    const-string v2, "Samsung HRM"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 387
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    const/4 v3, 0x1

    # setter for: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->isDeviceFound:Z
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$702(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;Z)Z

    .line 388
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->foundDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v2, p1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$802(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 389
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->isScanning:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$200(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 391
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->stopHealthSersorScan()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$400(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;)V

    .line 398
    :cond_1
    if-eqz p1, :cond_2

    .line 399
    :try_start_0
    new-instance v2, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$WrapperEventListener;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    invoke-direct {v2, v3, p1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$WrapperEventListener;-><init>(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V

    invoke-virtual {p1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->join(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_4

    .line 418
    .end local v1    # "name":Ljava/lang/String;
    :cond_2
    :goto_0
    return-void

    .line 401
    .restart local v1    # "name":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 403
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 405
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 407
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 408
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v0

    .line 409
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 410
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v0

    .line 411
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 412
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_4
    move-exception v0

    .line 413
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0
.end method

.method public onStarted(I)V
    .locals 2
    .param p1, "error"    # I

    .prologue
    .line 423
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->isScanning:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$202(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;Z)Z

    .line 424
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->isDeviceFound:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$702(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;Z)Z

    .line 425
    return-void
.end method

.method public onStopped(I)V
    .locals 5
    .param p1, "error"    # I

    .prologue
    .line 430
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->isScanning:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$202(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;Z)Z

    .line 431
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->isDeviceFound:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$700(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 433
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->foundDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$800(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 437
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->foundDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$800(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$WrapperEventListener;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->foundDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$800(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$WrapperEventListener;-><init>(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->join(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_4

    .line 458
    :cond_0
    :goto_0
    return-void

    .line 439
    :catch_0
    move-exception v0

    .line 441
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 443
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 445
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 446
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v0

    .line 447
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 448
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v0

    .line 449
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 450
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_4
    move-exception v0

    .line 451
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0

    .line 455
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0907a6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->showToast(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$600(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;Ljava/lang/String;)V

    .line 456
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->dismissDialog()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;->access$000(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;)V

    goto :goto_0
.end method
