.class Lcom/sec/android/app/shealth/moreapps/service/Log;
.super Ljava/lang/Object;
.source "Util.java"


# static fields
.field public static showLog:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/shealth/moreapps/service/Log;->showLog:Z

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 77
    sget-boolean v0, Lcom/sec/android/app/shealth/moreapps/service/Log;->showLog:Z

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    :cond_0
    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 86
    sget-boolean v0, Lcom/sec/android/app/shealth/moreapps/service/Log;->showLog:Z

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    :cond_0
    return-void
.end method

.method public static i(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 80
    sget-boolean v0, Lcom/sec/android/app/shealth/moreapps/service/Log;->showLog:Z

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    :cond_0
    return-void
.end method

.method public static v(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 74
    sget-boolean v0, Lcom/sec/android/app/shealth/moreapps/service/Log;->showLog:Z

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    :cond_0
    return-void
.end method

.method public static w(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 83
    sget-boolean v0, Lcom/sec/android/app/shealth/moreapps/service/Log;->showLog:Z

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    :cond_0
    return-void
.end method
