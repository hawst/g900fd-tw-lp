.class Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText$2;
.super Lcom/sec/android/app/shealth/common/utils/TextWatcherStub;
.source "SearchEditText.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;

.field final synthetic val$cancelIcon:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;

    iput-object p2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText$2;->val$cancelIcon:Landroid/view/View;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/TextWatcherStub;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 5
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 71
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v2

    if-nez v2, :cond_0

    const/4 v0, 0x1

    .line 73
    .local v0, "isEmpty":Z
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText$2;->val$cancelIcon:Landroid/view/View;

    if-eqz v0, :cond_1

    move v2, v3

    :goto_1
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 75
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->hintText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->access$100(Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;)Landroid/widget/TextView;

    move-result-object v4

    if-eqz v0, :cond_2

    move v2, v1

    :goto_2
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 77
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->mSearchIcon:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->access$200(Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;)Landroid/widget/ImageView;

    move-result-object v2

    if-eqz v0, :cond_3

    :goto_3
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 78
    return-void

    .end local v0    # "isEmpty":Z
    :cond_0
    move v0, v1

    .line 71
    goto :goto_0

    .restart local v0    # "isEmpty":Z
    :cond_1
    move v2, v1

    .line 73
    goto :goto_1

    :cond_2
    move v2, v3

    .line 75
    goto :goto_2

    :cond_3
    move v1, v3

    .line 77
    goto :goto_3
.end method
