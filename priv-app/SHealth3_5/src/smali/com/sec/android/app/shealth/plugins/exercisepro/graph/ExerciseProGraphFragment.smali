.class public abstract Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
.source "ExerciseProGraphFragment.java"


# static fields
.field private static isPaceMode:Z


# instance fields
.field private mChangePeriodListener:Landroid/view/View$OnClickListener;

.field private mGenerRelativeLayout:Landroid/widget/RelativeLayout;

.field protected mGeneralViewContainer:Landroid/widget/LinearLayout;

.field private mGraphViewHeader:Landroid/view/View;

.field private mIfGraphModified:Z

.field protected mInformationArea:Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;

.field private mInformationContainer:Landroid/widget/LinearLayout;

.field private mLegendContainer:Landroid/widget/LinearLayout;

.field private mNoDataContainer:Landroid/widget/LinearLayout;

.field mOnLayoutResizeListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewSizeChangedObserver$OnResizeObserverListener;

.field private mPeriodChangeButtonsArray:[Landroid/view/View;

.field private mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

.field private mRootView:Landroid/view/View;

.field private mSicGraphView:Landroid/view/View;

.field private sizeChangedObserver:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewSizeChangedObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->isPaceMode:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 39
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 88
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mOnLayoutResizeListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewSizeChangedObserver$OnResizeObserverListener;

    .line 48
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "PaceMode"    # Z

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 39
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 88
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mOnLayoutResizeListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewSizeChangedObserver$OnResizeObserverListener;

    .line 52
    sput-boolean p1, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->isPaceMode:Z

    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mSicGraphView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->setPeroidType(Landroid/view/View;)V

    return-void
.end method

.method private changePeriod(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 1
    .param p1, "newPeriodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 236
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mIfGraphModified:Z

    .line 237
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 238
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->initGeneralView()V

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mInformationArea:Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;->dimInformationAreaView()V

    .line 241
    return-void
.end method

.method public static getPaceMode()Z
    .locals 1

    .prologue
    .line 282
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->isPaceMode:Z

    return v0
.end method

.method private initDateBar()V
    .locals 11

    .prologue
    const v10, 0x7f09020a

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 134
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mGraphViewHeader:Landroid/view/View;

    const v4, 0x7f0804d1

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 135
    .local v1, "changePeriodButtonHour":Landroid/view/View;
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mGraphViewHeader:Landroid/view/View;

    const v4, 0x7f0804d2

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 136
    .local v0, "changePeriodButtonDay":Landroid/view/View;
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mGraphViewHeader:Landroid/view/View;

    const v4, 0x7f0804d3

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 137
    .local v2, "changePeriodButtonWeek":Landroid/view/View;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f090212

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f09020b

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 138
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f090213

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f09020b

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 139
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f090211

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f09020b

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 141
    new-array v3, v9, [Landroid/view/View;

    aput-object v1, v3, v6

    aput-object v0, v3, v7

    aput-object v2, v3, v8

    iput-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mPeriodChangeButtonsArray:[Landroid/view/View;

    .line 142
    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mChangePeriodListener:Landroid/view/View$OnClickListener;

    .line 152
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mChangePeriodListener:Landroid/view/View$OnClickListener;

    new-array v4, v9, [Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mPeriodChangeButtonsArray:[Landroid/view/View;

    aget-object v5, v5, v6

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mPeriodChangeButtonsArray:[Landroid/view/View;

    aget-object v5, v5, v7

    aput-object v5, v4, v7

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mPeriodChangeButtonsArray:[Landroid/view/View;

    aget-object v5, v5, v8

    aput-object v5, v4, v8

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->setCommonOnClickListenerForViews(Landroid/view/View$OnClickListener;[Landroid/view/View;)V

    .line 153
    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment$3;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;)V

    new-array v4, v9, [Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mPeriodChangeButtonsArray:[Landroid/view/View;

    aget-object v5, v5, v6

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mPeriodChangeButtonsArray:[Landroid/view/View;

    aget-object v5, v5, v7

    aput-object v5, v4, v7

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mPeriodChangeButtonsArray:[Landroid/view/View;

    aget-object v5, v5, v8

    aput-object v5, v4, v8

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->setCommonFocusChangeListenerForViews(Landroid/view/View$OnFocusChangeListener;[Landroid/view/View;)V

    .line 164
    return-void
.end method

.method private initGeneralView()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 205
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mGenerRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 206
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->initGraphView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mSicGraphView:Landroid/view/View;

    .line 207
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 208
    .local v0, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->ordinal()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->selectPeriodButton(I)V

    .line 209
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mGenerRelativeLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mSicGraphView:Landroid/view/View;

    invoke-virtual {v1, v2, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 210
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mGenerRelativeLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->sizeChangedObserver:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewSizeChangedObserver;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;I)V

    .line 211
    return-void
.end method

.method private initInformationArea()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 215
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mInformationContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 216
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->initInformationArea(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mInformationArea:Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;

    .line 217
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mInformationArea:Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;

    check-cast v1, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setFirstInformationVisible(Z)V

    .line 218
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 219
    .local v0, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mInformationArea:Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;

    if-eqz v1, :cond_0

    .line 220
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mInformationContainer:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mInformationArea:Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;

    invoke-virtual {v1, v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 222
    :cond_0
    return-void
.end method

.method private initLegendArea()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 226
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mLegendContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 227
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->initGraphLegendArea()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 228
    .local v0, "legendArea":Landroid/widget/LinearLayout;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 229
    .local v1, "lp":Landroid/widget/LinearLayout$LayoutParams;
    if-eqz v0, :cond_0

    .line 230
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mLegendContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 232
    :cond_0
    return-void
.end method

.method private selectPeriodButton(I)V
    .locals 3
    .param p1, "newPeriodType"    # I

    .prologue
    .line 245
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mPeriodChangeButtonsArray:[Landroid/view/View;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 247
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mPeriodChangeButtonsArray:[Landroid/view/View;

    aget-object v2, v1, v0

    if-ne v0, p1, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v2, v1}, Landroid/view/View;->setSelected(Z)V

    .line 245
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 247
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 249
    :cond_1
    return-void
.end method

.method private varargs setCommonFocusChangeListenerForViews(Landroid/view/View$OnFocusChangeListener;[Landroid/view/View;)V
    .locals 4
    .param p1, "commonFocusChangeListener"    # Landroid/view/View$OnFocusChangeListener;
    .param p2, "viewsWhichWillGetCommonFocusChangeListener"    # [Landroid/view/View;

    .prologue
    .line 176
    move-object v0, p2

    .local v0, "arr$":[Landroid/view/View;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 178
    .local v3, "view":Landroid/view/View;
    invoke-virtual {v3, p1}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 176
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 180
    .end local v3    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method

.method private varargs setCommonOnClickListenerForViews(Landroid/view/View$OnClickListener;[Landroid/view/View;)V
    .locals 4
    .param p1, "commonOnClickListener"    # Landroid/view/View$OnClickListener;
    .param p2, "viewsWhichWillGetCommonOnClickListener"    # [Landroid/view/View;

    .prologue
    .line 168
    move-object v0, p2

    .local v0, "arr$":[Landroid/view/View;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 170
    .local v3, "view":Landroid/view/View;
    invoke-virtual {v3, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 172
    .end local v3    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public static setPaceMode(Z)V
    .locals 0
    .param p0, "paceMode"    # Z

    .prologue
    .line 286
    sput-boolean p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->isPaceMode:Z

    .line 287
    return-void
.end method

.method private setPeroidType(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 184
    .local v0, "newPeriodType":Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f0804d1

    if-ne v1, v2, :cond_2

    .line 186
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 196
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-eq v1, v0, :cond_1

    .line 198
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->changePeriod(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 200
    :cond_1
    return-void

    .line 188
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f0804d2

    if-ne v1, v2, :cond_3

    .line 190
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    goto :goto_0

    .line 192
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f0804d3

    if-ne v1, v2, :cond_0

    .line 194
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    goto :goto_0
.end method


# virtual methods
.method protected abstract initGraphLegendArea()Landroid/view/View;
.end method

.method protected abstract initGraphView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
.end method

.method protected abstract initInformationArea(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v5, 0x7f0804cf

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 63
    const v1, 0x7f030124

    invoke-virtual {p1, v1, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mRootView:Landroid/view/View;

    .line 64
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mRootView:Landroid/view/View;

    const v2, 0x7f08036d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mGeneralViewContainer:Landroid/widget/LinearLayout;

    .line 66
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mGeneralViewContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 67
    .local v0, "params":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mRootView:Landroid/view/View;

    const v2, 0x7f08036c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mInformationContainer:Landroid/widget/LinearLayout;

    .line 68
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mRootView:Landroid/view/View;

    const v2, 0x7f080371

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mLegendContainer:Landroid/widget/LinearLayout;

    .line 69
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mRootView:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mGraphViewHeader:Landroid/view/View;

    .line 70
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mRootView:Landroid/view/View;

    const v2, 0x7f08036e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mNoDataContainer:Landroid/widget/LinearLayout;

    .line 71
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mRootView:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mGraphViewHeader:Landroid/view/View;

    .line 72
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mGraphViewHeader:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 73
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewSizeChangedObserver;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewSizeChangedObserver;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->sizeChangedObserver:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewSizeChangedObserver;

    .line 74
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->sizeChangedObserver:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewSizeChangedObserver;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mOnLayoutResizeListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewSizeChangedObserver$OnResizeObserverListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewSizeChangedObserver;->SetOnResizeListener(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewSizeChangedObserver$OnResizeObserverListener;)V

    .line 75
    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 76
    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 77
    new-instance v1, Landroid/widget/RelativeLayout;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mGenerRelativeLayout:Landroid/widget/RelativeLayout;

    .line 78
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mGeneralViewContainer:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mGenerRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 79
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->sizeChangedObserver:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewSizeChangedObserver;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewSizeChangedObserver;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 80
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mInformationContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 81
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->initDateBar()V

    .line 82
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->initGeneralView()V

    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->initInformationArea()V

    .line 84
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->initLegendArea()V

    .line 85
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mRootView:Landroid/view/View;

    return-object v1
.end method

.method protected setSchartZoomMap(Lcom/samsung/android/sdk/chart/view/SchartZoomMap;)V
    .locals 1
    .param p1, "map"    # Lcom/samsung/android/sdk/chart/view/SchartZoomMap;

    .prologue
    .line 290
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mGeneralViewContainer:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->mGeneralViewContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 293
    :cond_0
    return-void
.end method

.method public updateGraphFragment()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->initGeneralView()V

    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->initInformationArea()V

    .line 58
    return-void
.end method
