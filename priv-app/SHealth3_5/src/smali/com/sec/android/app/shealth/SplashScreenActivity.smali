.class public Lcom/sec/android/app/shealth/SplashScreenActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "SplashScreenActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IBackPressControllerProvider;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IContentInitializatorGetter;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/SplashScreenActivity$13;,
        Lcom/sec/android/app/shealth/SplashScreenActivity$ProfileFinishedReceiver;,
        Lcom/sec/android/app/shealth/SplashScreenActivity$SHealthUpgradeHandler;,
        Lcom/sec/android/app/shealth/SplashScreenActivity$SystemCloseDialogReceiver;,
        Lcom/sec/android/app/shealth/SplashScreenActivity$UpdateServiceBackPressController;,
        Lcom/sec/android/app/shealth/SplashScreenActivity$UpdateServiceDialogButtonController;
    }
.end annotation


# static fields
.field private static final DATA_CONNECTION_DIALOG:Ljava/lang/String; = "data_connection_dialog"

.field public static final INITIALIZATION_STATUSRECEIVER_INTENT:Ljava/lang/String; = "com.sec.android.service.health.cp.PLATFORM_INITIALIZATION_STATUSRECEIVER_INTENT"

.field public static final INITILIZATION_GETSTATUS_INTENT:Ljava/lang/String; = "com.sec.android.service.health.cp.PLATFORM_INITILIZATION_GETSTATUS_INTENT"

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final POPUP_MOBILE:I = 0x6

.field private static final POPUP_WIFI:I = 0x8

.field private static final SYSTEM_DIALOG_REASON_HOME_KEY:Ljava/lang/String; = "homekey"

.field private static final SYSTEM_DIALOG_REASON_KEY:Ljava/lang/String; = "reason"

.field private static final TAG:Ljava/lang/String;

.field private static final UPDATE_SERVICE:Ljava/lang/String; = "update service"


# instance fields
.field backPressListenernew:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;

.field private connectionBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

.field private connectionWifiBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

.field private doNotShowCheckBox:Landroid/widget/CheckBox;

.field private fadeInAniListener:Landroid/view/animation/Animation$AnimationListener;

.field private fadeOutAniListener:Landroid/view/animation/Animation$AnimationListener;

.field private isNext:Z

.field private isPopupOkDone:Z

.field isSamsungAccountLinked:Z

.field private mBStateLossOnsaveInstance:Z

.field private final mBackPressMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/IBackPressController;",
            ">;"
        }
    .end annotation
.end field

.field private mBackgroundLayout:Landroid/widget/RelativeLayout;

.field private mBottomLayout:Landroid/widget/LinearLayout;

.field private mCenterLayout:Landroid/widget/RelativeLayout;

.field private mContentUpAnime:Landroid/view/animation/Animation;

.field private final mDialogControllerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;",
            ">;"
        }
    .end annotation
.end field

.field private mFadeInAnime:Landroid/view/animation/Animation;

.field private mFadeOutAnime:Landroid/view/animation/Animation;

.field private mNextBtnLayout:Landroid/widget/RelativeLayout;

.field private mProfileFinishedReceiver:Lcom/sec/android/app/shealth/SplashScreenActivity$ProfileFinishedReceiver;

.field private mScaleExAnime:Landroid/view/animation/Animation;

.field private mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

.field private mSipIntentFilter:Landroid/content/IntentFilter;

.field private mWelcomeLayout:Landroid/widget/RelativeLayout;

.field private networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

.field private nextIcon:Landroid/view/View;

.field private nextText:Landroid/widget/TextView;

.field private popupMode:I

.field run:Ljava/lang/Runnable;

.field startClickListener:Landroid/view/View$OnClickListener;

.field private systemCloseDialogReceiver:Lcom/sec/android/app/shealth/SplashScreenActivity$SystemCloseDialogReceiver;

.field private welcomeText:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 105
    const-class v0, Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/SplashScreenActivity;->LOG_TAG:Ljava/lang/String;

    .line 106
    const-class v0, Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 104
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 130
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->isNext:Z

    .line 140
    new-instance v0, Lcom/sec/android/app/shealth/SplashScreenActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/SplashScreenActivity$1;-><init>(Lcom/sec/android/app/shealth/SplashScreenActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mDialogControllerMap:Ljava/util/Map;

    .line 146
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->isSamsungAccountLinked:Z

    .line 153
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mBStateLossOnsaveInstance:Z

    .line 160
    new-instance v0, Lcom/sec/android/app/shealth/SplashScreenActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/SplashScreenActivity$2;-><init>(Lcom/sec/android/app/shealth/SplashScreenActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mBackPressMap:Ljava/util/Map;

    .line 604
    new-instance v0, Lcom/sec/android/app/shealth/SplashScreenActivity$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/SplashScreenActivity$5;-><init>(Lcom/sec/android/app/shealth/SplashScreenActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->startClickListener:Landroid/view/View$OnClickListener;

    .line 620
    new-instance v0, Lcom/sec/android/app/shealth/SplashScreenActivity$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/SplashScreenActivity$6;-><init>(Lcom/sec/android/app/shealth/SplashScreenActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->fadeInAniListener:Landroid/view/animation/Animation$AnimationListener;

    .line 638
    new-instance v0, Lcom/sec/android/app/shealth/SplashScreenActivity$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/SplashScreenActivity$7;-><init>(Lcom/sec/android/app/shealth/SplashScreenActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->fadeOutAniListener:Landroid/view/animation/Animation$AnimationListener;

    .line 662
    new-instance v0, Lcom/sec/android/app/shealth/SplashScreenActivity$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/SplashScreenActivity$8;-><init>(Lcom/sec/android/app/shealth/SplashScreenActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->run:Ljava/lang/Runnable;

    .line 898
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->isPopupOkDone:Z

    .line 955
    new-instance v0, Lcom/sec/android/app/shealth/SplashScreenActivity$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/SplashScreenActivity$11;-><init>(Lcom/sec/android/app/shealth/SplashScreenActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->backPressListenernew:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;

    .line 1144
    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/SplashScreenActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SplashScreenActivity;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->doNotShowCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/shealth/SplashScreenActivity;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SplashScreenActivity;
    .param p1, "x1"    # Landroid/widget/CheckBox;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->doNotShowCheckBox:Landroid/widget/CheckBox;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/SplashScreenActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SplashScreenActivity;

    .prologue
    .line 104
    iget v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->popupMode:I

    return v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/shealth/SplashScreenActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SplashScreenActivity;
    .param p1, "x1"    # I

    .prologue
    .line 104
    iput p1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->popupMode:I

    return p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/SplashScreenActivity;)Lcom/sec/android/app/shealth/common/utils/NetworkUtils;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SplashScreenActivity;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/SplashScreenActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SplashScreenActivity;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->connectionWifiBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/sec/android/app/shealth/SplashScreenActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SplashScreenActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 104
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->isPopupOkDone:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/SplashScreenActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SplashScreenActivity;

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->launchHomeActivity()V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/SplashScreenActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SplashScreenActivity;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->connectionBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/SplashScreenActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SplashScreenActivity;

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->isNext:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/SplashScreenActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SplashScreenActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 104
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->isNext:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/SplashScreenActivity;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SplashScreenActivity;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mNextBtnLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/SplashScreenActivity;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SplashScreenActivity;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mFadeOutAnime:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/SplashScreenActivity;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SplashScreenActivity;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mCenterLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/SplashScreenActivity;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SplashScreenActivity;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mBottomLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/SplashScreenActivity;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SplashScreenActivity;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mBackgroundLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/SplashScreenActivity;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SplashScreenActivity;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mScaleExAnime:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/SplashScreenActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SplashScreenActivity;

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->launchHomeIfPossible()V

    return-void
.end method

.method private createDiaglog()V
    .locals 5

    .prologue
    const v4, 0x7f090047

    const v3, 0x7f0300a3

    const/4 v2, 0x2

    .line 949
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f0907a1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->backPressListenernew:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->connectionBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 951
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f0907a2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->backPressListenernew:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->connectionWifiBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 953
    return-void
.end method

.method private init()V
    .locals 15

    .prologue
    .line 297
    sget-object v12, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string v13, "[PERF] init() - START"

    invoke-static {v12, v13}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    sget-object v12, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "[PERF] KeyStatus #1 : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/service/health/keyManager/KeyManager;->getKeyMansgerState()Z

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v4

    .line 303
    .local v4, "initializationNeeded":Z
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {v12}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->getMigrationState(Landroid/content/Context;)I

    move-result v9

    .line 304
    .local v9, "migrationState":I
    if-nez v4, :cond_0

    if-eqz v9, :cond_0

    .line 305
    sget-object v12, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "MIGRATION Legacy isInitializationNeeded == false but migrationState is available("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ")"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    const/4 v12, 0x1

    invoke-static {p0, v12}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setInitializationNeeded(Landroid/content/Context;Z)V

    .line 307
    const/4 v4, 0x1

    .line 308
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    move-result-object v12

    const-string/jumbo v13, "request_password"

    const/4 v14, 0x0

    invoke-virtual {v12, p0, v13, v14}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->saveBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 309
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    move-result-object v12

    const-string/jumbo v13, "security_pin_enabled"

    const/4 v14, 0x0

    invoke-virtual {v12, p0, v13, v14}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->saveBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 311
    const/4 v12, 0x0

    invoke-static {p0, v12}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setRestoreOngoing(Landroid/content/Context;Z)V

    .line 314
    :cond_0
    if-eqz v4, :cond_1

    .line 316
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {v12, v9}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setUpgradeStatus(Landroid/content/Context;I)V

    .line 319
    :cond_1
    sget-object v12, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "[PERF] KeyStatus #2 : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/service/health/keyManager/KeyManager;->isSecureStorageSupported()Z

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/service/health/keyManager/KeyManager;->isDefaultPassword()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/service/health/keyManager/KeyManager;->isSecureStorageSupported()Z

    move-result v12

    if-nez v12, :cond_8

    .line 322
    sget-boolean v12, Lcom/sec/android/service/health/keyManager/KeyManager;->DEBUG:Z

    if-eqz v12, :cond_2

    sget-object v12, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "KeyManager SplashScreenActivity ======= "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    :cond_2
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v12

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Lcom/sec/android/service/health/keyManager/KeyManager;->initialization(Landroid/content/Context;I)V

    .line 325
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/service/health/keyManager/KeyManager;->isDefaultPassword()I

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_6

    .line 326
    const/4 v11, 0x0

    .line 328
    .local v11, "temp":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v12

    const-string/jumbo v13, "temps"

    invoke-virtual {v12, v13}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v11

    .line 332
    :goto_0
    if-eqz v11, :cond_4

    .line 333
    sget-boolean v12, Lcom/sec/android/service/health/keyManager/KeyManager;->DEBUG:Z

    if-eqz v12, :cond_3

    sget-object v12, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "KeyManager SplashScreenActivity.temp = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    :cond_3
    sget-object v12, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string v13, "KeyStatus #2-1 : "

    invoke-static {v12, v13}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v12

    invoke-virtual {v12, v11}, Lcom/sec/android/service/health/keyManager/KeyManager;->restoreKeyFromTemporaryStore(Ljava/lang/String;)V

    .line 336
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lcom/sec/android/service/health/keyManager/KeyManager;->setApplicationPassword(Z)V

    .line 342
    .end local v11    # "temp":Ljava/lang/String;
    :cond_4
    :goto_1
    sget-object v12, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "[PERF] KeyStatus #3 : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/service/health/keyManager/KeyManager;->getApplicationPassword()Z

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInputPincodeBeforeOOBE()Z

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/service/health/keyManager/KeyManager;->getApplicationPassword()Z

    move-result v12

    if-eqz v12, :cond_7

    .line 345
    sget-object v12, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "KeyManager +KeyManagerReceiver.onReceive.KeyManager_Password ======= settingPassword"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/service/health/keyManager/KeyManager;->getApplicationPassword()Z

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInputPincodeFlag()Z

    move-result v12

    if-nez v12, :cond_5

    .line 347
    new-instance v5, Landroid/content/Intent;

    const-class v12, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;

    invoke-direct {v5, p0, v12}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 348
    .local v5, "intent":Landroid/content/Intent;
    const-string v12, "SetPasswordActivity"

    const/4 v13, 0x1

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 349
    const/high16 v12, 0x34000000

    invoke-virtual {v5, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 350
    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/SplashScreenActivity;->startActivity(Landroid/content/Intent;)V

    .line 361
    .end local v5    # "intent":Landroid/content/Intent;
    :cond_5
    :goto_2
    sget-object v12, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "[PERF] KeyStatus #4 : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/service/health/keyManager/KeyManager;->getKeyMansgerState()Z

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    if-nez v4, :cond_8

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/service/health/keyManager/KeyManager;->getKeyMansgerState()Z

    move-result v12

    if-nez v12, :cond_8

    .line 367
    sget-object v12, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string v13, "MIGRATION: KeyService not ready - delay activity running"

    invoke-static {v12, v13}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 370
    .restart local v5    # "intent":Landroid/content/Intent;
    const-string v12, "com.sec.android.app.shealth"

    const-string v13, "com.sec.android.app.shealth.SplashScreenActivity"

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 371
    const/high16 v12, 0x34000000

    invoke-virtual {v5, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 372
    const-string v12, "ClassName"

    const-string v13, "SplashScreenActivity"

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 373
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInputPincodeFlag()Z

    move-result v12

    if-nez v12, :cond_8

    .line 374
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v12

    invoke-virtual {v12, v5}, Lcom/sec/android/service/health/keyManager/KeyManager;->addCallBackActivityIntent(Landroid/content/Intent;)V

    .line 375
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->finish()V

    .line 491
    .end local v5    # "intent":Landroid/content/Intent;
    :goto_3
    return-void

    .line 329
    .restart local v11    # "temp":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 330
    .local v2, "e":Ljava/lang/Exception;
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 338
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v11    # "temp":Ljava/lang/String;
    :cond_6
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/service/health/keyManager/KeyManager;->isDefaultPassword()I

    move-result v12

    if-nez v12, :cond_4

    .line 339
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getApplication()Landroid/app/Application;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/SHealthApplication;

    invoke-static {v12}, Lcom/sec/android/app/shealth/KeyManagerReceiver;->setApplication(Lcom/sec/android/app/shealth/SHealthApplication;)V

    .line 340
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/service/health/keyManager/KeyManager;->generateDBkey()Ljava/lang/String;

    goto/16 :goto_1

    .line 353
    :cond_7
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInputPincodeBeforeOOBE()Z

    move-result v12

    if-eqz v12, :cond_5

    .line 354
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInputPincodeFlag()Z

    move-result v12

    if-nez v12, :cond_5

    .line 355
    new-instance v5, Landroid/content/Intent;

    const-class v12, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;

    invoke-direct {v5, p0, v12}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 356
    .restart local v5    # "intent":Landroid/content/Intent;
    const-string v12, "SetPasswordActivity"

    const/4 v13, 0x1

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 357
    const/high16 v12, 0x34000000

    invoke-virtual {v5, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 358
    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/SplashScreenActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 380
    .end local v5    # "intent":Landroid/content/Intent;
    :cond_8
    sget-object v12, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "[PERF] KeyStatus #5 : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/service/health/keyManager/KeyManager;->getKeyMansgerState()Z

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    if-nez v9, :cond_9

    .line 382
    invoke-static {}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->getInstance()Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    move-result-object v12

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    new-instance v14, Lcom/sec/android/app/shealth/SplashScreenActivity$3;

    invoke-direct {v14, p0}, Lcom/sec/android/app/shealth/SplashScreenActivity$3;-><init>(Lcom/sec/android/app/shealth/SplashScreenActivity;)V

    invoke-virtual {v12, v13, v14}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->doNormalInitialization(Landroid/content/Context;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)Z

    .line 403
    :cond_9
    sget-boolean v12, Lcom/sec/android/service/health/keyManager/KeyManager;->DEBUG:Z

    if-eqz v12, :cond_a

    sget-object v12, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "KeyManager -SplashScreenActivity ======= "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    :cond_a
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->registerSystemCloseDialogReceiver()V

    .line 407
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getWindow()Landroid/view/Window;

    move-result-object v12

    invoke-virtual {v12}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v8

    .line 408
    .local v8, "lp":Landroid/view/WindowManager$LayoutParams;
    sget v12, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v13, 0x14

    if-gt v12, v13, :cond_f

    .line 409
    iget v12, v8, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit16 v12, v12, 0x2000

    iput v12, v8, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 414
    :goto_4
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getWindow()Landroid/view/Window;

    move-result-object v12

    invoke-virtual {v12, v8}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 426
    sget-boolean v12, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->mIsInitialized:Z

    if-nez v12, :cond_b

    .line 427
    new-instance v12, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;

    invoke-direct {v12, p0}, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;-><init>(Landroid/content/Context;)V

    const/4 v13, 0x0

    new-array v13, v13, [Ljava/lang/Void;

    invoke-virtual {v12, v13}, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 441
    :cond_b
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->getInstance()Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;

    move-result-object v12

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->setInitialized(Z)V

    .line 442
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {v12}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->initialize(Landroid/content/Context;)V

    .line 443
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CoreUtil;->initialize(Landroid/content/Context;)V

    .line 444
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v12

    if-eqz v12, :cond_10

    .line 445
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    const-string v13, "com.sec.android.app.shealth"

    const-string v14, "IS01"

    invoke-static {v12, v13, v14}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    const v12, 0x7f030167

    invoke-virtual {p0, v12}, Lcom/sec/android/app/shealth/SplashScreenActivity;->setContentView(I)V

    .line 447
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->initLayout()V

    .line 449
    new-instance v12, Landroid/content/Intent;

    const-string v13, "com.sec.shealth.action.SHEALTH_STARTED"

    invoke-direct {v12, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v12}, Lcom/sec/android/app/shealth/SplashScreenActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 456
    :goto_5
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->isRestoreInProgress()Z

    move-result v12

    if-eqz v12, :cond_11

    .line 457
    sget-object v12, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string v13, "disable launch Home: Migration is ongoing"

    invoke-static {v12, v13}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    const v12, 0x7f0907f5

    invoke-virtual {p0, v12}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x1

    invoke-static {p0, v12, v13}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/Toast;->show()V

    .line 459
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->finish()V

    .line 463
    :goto_6
    sget-object v12, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string v13, "[PERF] In Init() After launchHomeActivity"

    invoke-static {v12, v13}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v12

    const-string v13, "VER"

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v10

    .line 467
    .local v10, "pref":Landroid/content/SharedPreferences;
    :try_start_1
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 468
    .local v3, "edit":Landroid/content/SharedPreferences$Editor;
    const-string v12, "isFirstRun"

    const/4 v13, 0x1

    invoke-interface {v3, v12, v13}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 469
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 473
    .end local v3    # "edit":Landroid/content/SharedPreferences$Editor;
    :goto_7
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v12

    if-eqz v12, :cond_d

    .line 475
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {v12}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    .line 476
    .local v1, "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    new-instance v12, Landroid/content/ComponentName;

    const-class v13, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;

    invoke-direct {v12, p0, v13}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v12}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    .line 477
    .local v0, "appWidgetIds":[I
    array-length v12, v0

    if-lez v12, :cond_c

    .line 478
    new-instance v12, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;

    invoke-direct {v12}, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;-><init>()V

    invoke-virtual {v12, p0, v1, v0}, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 481
    :cond_c
    const/4 v12, 0x0

    invoke-static {p0, v12}, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->updateHomeWidgets(Landroid/content/Context;Z)V

    .line 484
    .end local v0    # "appWidgetIds":[I
    .end local v1    # "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    :cond_d
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v12

    const-string v13, "launchWidget"

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    .line 485
    .local v7, "isLaunchedFromWidget":Z
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v12

    const-string v13, "COACH_WIDGET"

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    .line 486
    .local v6, "isCoachWidgetIntent":Z
    if-eqz v7, :cond_e

    if-eqz v6, :cond_e

    .line 487
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    const-string v13, "com.sec.android.app.shealth.cignacoach"

    const-string v14, "0019"

    invoke-static {v12, v13, v14}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    :cond_e
    sget-object v12, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string v13, "[PERF] init() - END"

    invoke-static {v12, v13}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 412
    .end local v6    # "isCoachWidgetIntent":Z
    .end local v7    # "isLaunchedFromWidget":Z
    .end local v10    # "pref":Landroid/content/SharedPreferences;
    :cond_f
    iget v12, v8, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v12, v12, 0x2

    iput v12, v8, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    goto/16 :goto_4

    .line 451
    :cond_10
    new-instance v12, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-direct {v12, p0}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;-><init>(Landroid/content/Context;)V

    iput-object v12, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    .line 452
    iget-object v12, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->setInitialSettingOngoing(Z)V

    goto/16 :goto_5

    .line 461
    :cond_11
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->launchHomeActivity()V

    goto/16 :goto_6

    .line 470
    .restart local v10    # "pref":Landroid/content/SharedPreferences;
    :catch_1
    move-exception v12

    goto :goto_7
.end method

.method private initLayout()V
    .locals 5

    .prologue
    const/16 v1, 0x8

    const/4 v4, 0x0

    .line 571
    const v0, 0x7f08061a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mWelcomeLayout:Landroid/widget/RelativeLayout;

    .line 572
    const v0, 0x7f08061c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mBottomLayout:Landroid/widget/LinearLayout;

    .line 573
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mBottomLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 574
    const v0, 0x7f08061b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mBackgroundLayout:Landroid/widget/RelativeLayout;

    .line 575
    const v0, 0x7f08061d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mCenterLayout:Landroid/widget/RelativeLayout;

    .line 576
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mCenterLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 577
    const v0, 0x7f08062a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mNextBtnLayout:Landroid/widget/RelativeLayout;

    .line 578
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mNextBtnLayout:Landroid/widget/RelativeLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901f6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09020b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09020a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 579
    const v0, 0x7f040011

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mContentUpAnime:Landroid/view/animation/Animation;

    .line 580
    const v0, 0x7f040012

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mFadeInAnime:Landroid/view/animation/Animation;

    .line 581
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mFadeInAnime:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->fadeInAniListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 582
    const v0, 0x7f040013

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mFadeOutAnime:Landroid/view/animation/Animation;

    .line 583
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mFadeOutAnime:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->fadeOutAniListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 584
    const v0, 0x7f040014

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mScaleExAnime:Landroid/view/animation/Animation;

    .line 585
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mNextBtnLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->startClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 587
    const v0, 0x7f08039d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->nextText:Landroid/widget/TextView;

    .line 588
    const v0, 0x7f080614

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->nextIcon:Landroid/view/View;

    .line 589
    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/SplashScreenActivity;->setNextButtonEnable(Z)V

    .line 591
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mCenterLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 592
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mBottomLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 593
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mCenterLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mFadeInAnime:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 594
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mBottomLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mContentUpAnime:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 596
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mBottomLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f080629

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->welcomeText:Landroid/widget/TextView;

    .line 597
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isRtlLanguage()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->welcomeText:Landroid/widget/TextView;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 599
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->welcomeText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "\u200f"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0908ca

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 602
    :cond_0
    return-void
.end method

.method private isMYXJAppInstalled()Z
    .locals 4

    .prologue
    .line 964
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.meitu.meiyancamera"

    const/16 v3, 0x80

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 968
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 965
    :catch_0
    move-exception v0

    .line 966
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private launchHomeActivity()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 768
    sget-object v5, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string v6, "launchHomeActivity: ++"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 770
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isChinaModel()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 771
    new-instance v5, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    .line 772
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->disableBa()V

    .line 774
    iget-boolean v5, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->isPopupOkDone:Z

    if-nez v5, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "launchWidget"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 779
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->isMYXJAppInstalled()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 780
    new-instance v1, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    const-string v6, "MYXJ"

    invoke-direct {v1, v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 783
    .local v1, "dir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_1

    .line 784
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    .line 788
    .end local v1    # "dir":Ljava/io/File;
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->createDiaglog()V

    .line 789
    iget-object v5, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isWifiEnabled()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isLaunchWifiPopupWasShown(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 792
    const/16 v5, 0x8

    iput v5, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->popupMode:I

    .line 793
    iget-object v5, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->connectionWifiBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    const-string v7, "data_connection_dialog"

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 853
    :goto_0
    return-void

    .line 797
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isWifiEnabled()Z

    move-result v5

    if-nez v5, :cond_3

    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isLaunchMobilePopupWasShown(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 800
    const/4 v5, 0x6

    iput v5, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->popupMode:I

    .line 801
    iget-object v5, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->connectionBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    const-string v7, "data_connection_dialog"

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 808
    :cond_3
    iget-boolean v5, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->isPopupOkDone:Z

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isWifiEnabled()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isLaunchWifiPopupWasShown(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_5

    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isWifiEnabled()Z

    move-result v5

    if-nez v5, :cond_6

    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isLaunchMobilePopupWasShown(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 811
    :cond_5
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->enableBa()V

    .line 815
    :cond_6
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v5

    if-ne v5, v8, :cond_9

    .line 816
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isRestoreOngoing(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 817
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->moveToNextStepAfterRestore()V

    goto :goto_0

    .line 820
    :cond_7
    invoke-direct {p0, v8}, Lcom/sec/android/app/shealth/SplashScreenActivity;->setNextButtonEnable(Z)V

    .line 821
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isChinaModel()Z

    move-result v5

    if-nez v5, :cond_8

    .line 822
    sget-object v5, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "startSHealthService"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 823
    new-instance v2, Landroid/content/Intent;

    const-class v5, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-direct {v2, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 824
    .local v2, "i":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/SplashScreenActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 852
    .end local v2    # "i":Landroid/content/Intent;
    :cond_8
    :goto_1
    sget-object v5, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string v6, "launchHomeActivity: --"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 828
    :cond_9
    sget-object v5, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string v6, "[PERF] launchHomeActivity() else clause Intent - START"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 829
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 830
    .local v4, "intentParam":Landroid/content/Intent;
    if-eqz v4, :cond_a

    .line 831
    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 832
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_a

    .line 833
    const-string v5, "com.sec.android.intent.action.WEARABLE_DEVICE_SYNC_AND_START"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 834
    new-instance v3, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-class v6, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;

    invoke-direct {v3, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 835
    .local v3, "intentLaunch":Landroid/content/Intent;
    const-string v5, "com.sec.android.intent.action.WEARABLE_DEVICE_SYNC_AND_START"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 836
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/SplashScreenActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 837
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->finish()V

    goto/16 :goto_0

    .line 842
    .end local v0    # "action":Ljava/lang/String;
    .end local v3    # "intentLaunch":Landroid/content/Intent;
    :cond_a
    sget-object v5, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string v6, "[PERF] launchHomeActivity() else clause Intent - START"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 843
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isRestoreOngoing(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 844
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->moveToNextStepAfterRestore()V

    goto/16 :goto_0

    .line 848
    :cond_b
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->launchHomeIfPossible()V

    goto :goto_1
.end method

.method private launchHomeIfPossible()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 857
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 858
    sget-object v2, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string v3, "launchHomeIfPossible: CP is initialized. Launching Home"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 860
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 861
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "widgetActivityAction"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "widgetActivityPackage"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 862
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 863
    const-string v2, "launchWidget"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 864
    const-string/jumbo v2, "widget"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 870
    :goto_0
    sget-object v2, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string v3, "[PERF] launchHomeIfPossible() calling HomeActivity - START"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 871
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/SplashScreenActivity;->startActivity(Landroid/content/Intent;)V

    .line 872
    sget-object v2, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string v3, "[PERF] launchHomeIfPossible() HomeActivity is called - END"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 873
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->finish()V

    .line 888
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_1
    return-void

    .line 865
    .restart local v1    # "intent":Landroid/content/Intent;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "IS_UPGRADE_DONE"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 866
    const-string v2, "IS_UPGRADE_DONE"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0

    .line 868
    :cond_1
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_0

    .line 877
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_2
    sget-object v2, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string v3, "launchHomeIfPossible: CP not initialized yet. Waiting for 100 MS"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 878
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 879
    .local v0, "handler":Landroid/os/Handler;
    new-instance v2, Lcom/sec/android/app/shealth/SplashScreenActivity$9;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/SplashScreenActivity$9;-><init>(Lcom/sec/android/app/shealth/SplashScreenActivity;)V

    const-wide/16 v3, 0x64

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method

.method private registerProfileFinishedReceiver()V
    .locals 2

    .prologue
    .line 1161
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/SplashScreenActivity$ProfileFinishedReceiver;

    if-nez v0, :cond_0

    .line 1163
    new-instance v0, Lcom/sec/android/app/shealth/SplashScreenActivity$ProfileFinishedReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/SplashScreenActivity$ProfileFinishedReceiver;-><init>(Lcom/sec/android/app/shealth/SplashScreenActivity;Lcom/sec/android/app/shealth/SplashScreenActivity$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/SplashScreenActivity$ProfileFinishedReceiver;

    .line 1164
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mSipIntentFilter:Landroid/content/IntentFilter;

    .line 1165
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mSipIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.shealth.action.FINISHED_PROFILE_SETUP"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1166
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/SplashScreenActivity$ProfileFinishedReceiver;

    iget-object v1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mSipIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/SplashScreenActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1169
    :cond_0
    return-void
.end method

.method private registerSystemCloseDialogReceiver()V
    .locals 3

    .prologue
    .line 1104
    iget-object v1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->systemCloseDialogReceiver:Lcom/sec/android/app/shealth/SplashScreenActivity$SystemCloseDialogReceiver;

    if-nez v1, :cond_0

    .line 1106
    new-instance v1, Lcom/sec/android/app/shealth/SplashScreenActivity$SystemCloseDialogReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/SplashScreenActivity$SystemCloseDialogReceiver;-><init>(Lcom/sec/android/app/shealth/SplashScreenActivity$1;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->systemCloseDialogReceiver:Lcom/sec/android/app/shealth/SplashScreenActivity$SystemCloseDialogReceiver;

    .line 1107
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1108
    .local v0, "dialogFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1109
    iget-object v1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->systemCloseDialogReceiver:Lcom/sec/android/app/shealth/SplashScreenActivity$SystemCloseDialogReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1112
    .end local v0    # "dialogFilter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private setNextButtonEnable(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 672
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mNextBtnLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 674
    if-eqz p1, :cond_0

    .line 675
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->nextText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 676
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->nextIcon:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 681
    :goto_0
    return-void

    .line 678
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->nextText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 679
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->nextIcon:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0
.end method

.method private static showToast(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 972
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 973
    return-void
.end method

.method private unregisterProfileFinishedReceiver()V
    .locals 1

    .prologue
    .line 1176
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/SplashScreenActivity$ProfileFinishedReceiver;

    if-eqz v0, :cond_0

    .line 1178
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/SplashScreenActivity$ProfileFinishedReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1179
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/SplashScreenActivity$ProfileFinishedReceiver;

    .line 1181
    :cond_0
    return-void
.end method

.method private unregisterSystemCloseDialogReceiver()V
    .locals 1

    .prologue
    .line 1119
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->systemCloseDialogReceiver:Lcom/sec/android/app/shealth/SplashScreenActivity$SystemCloseDialogReceiver;

    if-eqz v0, :cond_0

    .line 1121
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->systemCloseDialogReceiver:Lcom/sec/android/app/shealth/SplashScreenActivity$SystemCloseDialogReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1122
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->systemCloseDialogReceiver:Lcom/sec/android/app/shealth/SplashScreenActivity$SystemCloseDialogReceiver;

    .line 1124
    :cond_0
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 4

    .prologue
    .line 684
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x400

    const/16 v3, 0x500

    invoke-virtual {v1, v2, v3}, Landroid/view/Window;->setFlags(II)V

    .line 685
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 686
    .local v0, "actionBar":Landroid/app/ActionBar;
    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    .line 687
    return-void
.end method

.method finishApplication()V
    .locals 4

    .prologue
    .line 506
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->finish()V

    .line 507
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 508
    .local v0, "handler":Landroid/os/Handler;
    new-instance v1, Lcom/sec/android/app/shealth/SplashScreenActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/SplashScreenActivity$4;-><init>(Lcom/sec/android/app/shealth/SplashScreenActivity;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 519
    return-void
.end method

.method public getBackPressController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IBackPressController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 1056
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mBackPressMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/IBackPressController;

    return-object v0
.end method

.method public getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    .locals 2
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 904
    const-string v1, "data_connection_dialog"

    invoke-virtual {v1, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 905
    new-instance v0, Lcom/sec/android/app/shealth/SplashScreenActivity$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/SplashScreenActivity$10;-><init>(Lcom/sec/android/app/shealth/SplashScreenActivity;)V

    .line 945
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 977
    const-string v0, "data_connection_dialog"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 978
    new-instance v0, Lcom/sec/android/app/shealth/SplashScreenActivity$12;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/SplashScreenActivity$12;-><init>(Lcom/sec/android/app/shealth/SplashScreenActivity;)V

    .line 1019
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 271
    sget-object v0, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string v1, "[PERF] handleMessage() Message Received - START"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 294
    :goto_0
    return-void

    .line 275
    :sswitch_0
    sget-object v0, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string v1, "handleMessage() : SHealthUpgradeConstants.MESSAGE_UPGRADE_NOT_REQUIRED"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->init()V

    goto :goto_0

    .line 280
    :sswitch_1
    sget-object v0, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string v1, "handleMessage() : SHealthUpgradeConstants.MESSAGE_UPGRADE_HEALTH_SERVICE_REQUIRED"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    sget-object v0, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string v1, "App Type : FULL cannot reach this code block. something wrong."

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 273
    nop

    :sswitch_data_0
    .sparse-switch
        0xff -> :sswitch_0
        0x163 -> :sswitch_1
    .end sparse-switch
.end method

.method public moveToNextStepAfterRestore()V
    .locals 3

    .prologue
    .line 497
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 498
    .local v0, "intent":Landroid/content/Intent;
    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 499
    const-string/jumbo v1, "profileRestored"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 500
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->startActivity(Landroid/content/Intent;)V

    .line 502
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->finish()V

    .line 503
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v4, -0x1

    .line 693
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 695
    sget-object v1, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SplashScreenActivity onActivityResult MESSAGE "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " resultCode "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 698
    const/16 v1, 0x3e7

    if-ne p1, v1, :cond_2

    .line 700
    if-ne p2, v4, :cond_1

    .line 702
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 703
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "isAgreeMode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 704
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->startActivity(Landroid/content/Intent;)V

    .line 706
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->finish()V

    .line 722
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 708
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090d53

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 712
    :cond_2
    if-ne p2, v4, :cond_3

    .line 714
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->launchHomeActivity()V

    .line 715
    sget-object v1, Lcom/sec/android/app/shealth/SplashScreenActivity;->LOG_TAG:Ljava/lang/String;

    const-string v2, "InitializeCPReceiver only log"

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 717
    :cond_3
    if-nez p2, :cond_0

    .line 719
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->finish()V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 562
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mBStateLossOnsaveInstance:Z

    if-eqz v0, :cond_0

    .line 567
    :goto_0
    return-void

    .line 566
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->moveTaskToBack(Z)Z

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    const/4 v11, 0x1

    .line 168
    sget-object v8, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string v9, "[PERF] SplashScreenActivity.onCreate() - START"

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    const-string v7, ""

    .line 171
    .local v7, "version":Ljava/lang/String;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->registerProfileFinishedReceiver()V

    .line 173
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getPackageName()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    .line 174
    .local v4, "pInfo":Landroid/content/pm/PackageInfo;
    iget-object v7, v4, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 176
    const-string v8, "SHEALTH VERSION"

    invoke-static {v8}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 177
    .local v0, "beforeVersion":Ljava/lang/Object;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 178
    const-string v8, "SHEALTH VERSION"

    invoke-static {v8, v7}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setValue(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 184
    .end local v0    # "beforeVersion":Ljava/lang/Object;
    .end local v4    # "pInfo":Landroid/content/pm/PackageInfo;
    :cond_0
    :goto_0
    sget-object v8, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v9, "onCreate"

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    const-string v8, "VerificationLog"

    const-string/jumbo v9, "onCreate"

    invoke-static {v8, v9}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "com.sec.android.app.shealth"

    const-string v10, "0001"

    invoke-static {v8, v9, v10, v7}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    sget-object v8, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "SplashScreenActivity onCreate"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 195
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->isTaskRoot()Z

    move-result v8

    if-nez v8, :cond_1

    .line 196
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 197
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 198
    .local v3, "intentAction":Ljava/lang/String;
    const-string v8, "android.intent.category.LAUNCHER"

    invoke-virtual {v2, v8}, Landroid/content/Intent;->hasCategory(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    if-eqz v3, :cond_1

    const-string v8, "android.intent.action.MAIN"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 199
    sget-object v8, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string v9, "SplashScreenActivity is not the root.  Finishing SplashScreenActivity"

    invoke-static {v8, v9}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->finish()V

    .line 268
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v3    # "intentAction":Ljava/lang/String;
    :goto_1
    return-void

    .line 180
    :catch_0
    move-exception v1

    .line 181
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v8, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 205
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/MemoryStatusUtils;->isNotEnoughMemory()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 206
    const v8, 0x7f09078b

    invoke-static {p0, v8, v11}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    .line 207
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->finish()V

    goto :goto_1

    .line 224
    :cond_2
    sget-boolean v8, Lcom/sec/android/app/shealth/SHealthApplication;->gIsHealthServiceUpgradedNeeded:Z

    if-ne v8, v11, :cond_4

    .line 225
    sget-object v8, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string v9, "HEALTH SERVICE NEEDS TO UPGRADE."

    invoke-static {v8, v9}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->getUpgradeStatus(Landroid/content/Context;)Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    .line 232
    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeHelper;->isHealthServiceUpgradeNeeded(Landroid/content/Context;)Z

    move-result v8

    if-ne v8, v11, :cond_3

    .line 233
    sget-object v8, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string v9, "LAUNCH SDKExceptionActivity To Show Popup !!"

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    new-instance v5, Landroid/content/Intent;

    const-class v8, Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity;

    invoke-direct {v5, p0, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 235
    .local v5, "sdkException":Landroid/content/Intent;
    const/high16 v8, 0x14000000

    invoke-virtual {v5, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 236
    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/SplashScreenActivity;->startActivity(Landroid/content/Intent;)V

    .line 238
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->finishAffinity()V

    .line 239
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v8

    invoke-static {v8}, Landroid/os/Process;->killProcess(I)V

    goto :goto_1

    .line 244
    .end local v5    # "sdkException":Landroid/content/Intent;
    :cond_3
    sget-object v8, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string v9, "JUST KILL. SplashScreenActivity WILL BE LAUNCHED AGAIN !!"

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v8

    invoke-static {v8}, Landroid/os/Process;->killProcess(I)V

    goto :goto_1

    .line 256
    :cond_4
    :try_start_1
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier;->blockRestore()V

    .line 257
    new-instance v6, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;

    invoke-direct {v6, p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;-><init>(Landroid/content/Context;)V

    .line 258
    .local v6, "upgradeManager":Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;
    sget-object v8, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string v9, "[PERF] SplashScreenActivity.onCreate() calling start upgrade - START"

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->getUpgradeStatus(Landroid/content/Context;)Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    move-result-object v8

    new-instance v9, Lcom/sec/android/app/shealth/SplashScreenActivity$SHealthUpgradeHandler;

    invoke-direct {v9, p0}, Lcom/sec/android/app/shealth/SplashScreenActivity$SHealthUpgradeHandler;-><init>(Lcom/sec/android/app/shealth/SplashScreenActivity;)V

    invoke-virtual {v6, v8, v9}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;->startUpgrade(Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;Landroid/os/Handler;)V
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_2

    .line 267
    .end local v6    # "upgradeManager":Lcom/sec/android/service/health/upgrade/SHealthUpgradeManager;
    :goto_2
    sget-object v8, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string v9, "[PERF] SplashScreenActivity.onCreate() - END"

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 260
    :catch_1
    move-exception v1

    .line 261
    .local v1, "e":Ljava/lang/SecurityException;
    invoke-virtual {v1}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_2

    .line 262
    .end local v1    # "e":Ljava/lang/SecurityException;
    :catch_2
    move-exception v1

    .line 263
    .local v1, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v1}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_2
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 746
    sget-object v1, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "ondestroy() splashscreen"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 748
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->unregisterProfileFinishedReceiver()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 752
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->unregisterSystemCloseDialogReceiver()V

    .line 753
    const-wide/16 v1, 0x0

    sput-wide v1, Lcom/sec/android/app/shealth/receiver/LaunchWidgetReceiver;->prevWidgetClickTime:J

    .line 754
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mBStateLossOnsaveInstance:Z

    .line 757
    iget-object v1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mWelcomeLayout:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    .line 758
    iget-object v1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mWelcomeLayout:Landroid/widget/RelativeLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 761
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onDestroy()V

    .line 765
    return-void

    .line 749
    :catch_0
    move-exception v0

    .line 750
    .local v0, "iae":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/sec/android/app/shealth/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string v2, "Very rare and exceptional case where Android claims that mProfileFinishedReceiver was never registered in the first place."

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 523
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    .line 524
    const-string v0, "VerificationLog"

    const-string/jumbo v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 525
    const-string v0, "VerificationLog"

    const-string v1, "Executed"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 548
    :cond_0
    :goto_0
    return-void

    .line 532
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mBStateLossOnsaveInstance:Z

    .line 534
    sget-boolean v0, Lcom/sec/android/app/shealth/SHealthApplication;->unSupported:Z

    if-eq v0, v2, :cond_0

    .line 535
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v2, :cond_0

    .line 536
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mBackgroundLayout:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_2

    .line 537
    const v0, 0x7f030167

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->setContentView(I)V

    .line 538
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->initLayout()V

    .line 540
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mBackgroundLayout:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 541
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mBottomLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07011f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 543
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mNextBtnLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->requestFocus()Z

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 552
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity;->mBStateLossOnsaveInstance:Z

    .line 553
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 554
    return-void
.end method
