.class public Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
.source "LifeStyleNoScoreFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment$2;
    }
.end annotation


# instance fields
.field private mRemainCategory:[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 103
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;)[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;->mRemainCategory:[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    return-object v0
.end method

.method private findRemainCategory()[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    .locals 12

    .prologue
    .line 72
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 73
    .local v5, "remainCategory":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;>;"
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v9

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/cigna/coach/factory/CoachFactory;->getLifeStyle(Landroid/content/Context;)Lcom/cigna/coach/interfaces/ILifeStyle;

    move-result-object v3

    .line 76
    .local v3, "ils":Lcom/cigna/coach/interfaces/ILifeStyle;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v8

    .line 77
    .local v8, "userId":Ljava/lang/String;
    sget-object v9, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->ALL:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v10

    const/4 v11, 0x0

    invoke-interface {v3, v8, v9, v10, v11}, Lcom/cigna/coach/interfaces/ILifeStyle;->getScores(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;Ljava/util/Calendar;Z)Lcom/cigna/coach/apiobjects/Scores;

    move-result-object v7

    .line 79
    .local v7, "scores":Lcom/cigna/coach/apiobjects/Scores;
    invoke-virtual {v7}, Lcom/cigna/coach/apiobjects/Scores;->getCategoryInfo()Ljava/util/Hashtable;

    move-result-object v6

    .line 81
    .local v6, "score":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/Integer;>;"
    invoke-virtual {v6}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v1

    .line 83
    .local v1, "ect":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 84
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 85
    .local v4, "key":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    invoke-virtual {v6, v4}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 86
    .local v2, "i":Ljava/lang/Integer;
    const/4 v0, 0x0

    .line 87
    .local v0, "category":Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v9

    if-gez v9, :cond_0

    .line 88
    sget-object v9, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment$2;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    invoke-virtual {v4}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 105
    :goto_1
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 108
    .end local v0    # "category":Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    .end local v1    # "ect":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;>;"
    .end local v2    # "i":Ljava/lang/Integer;
    .end local v4    # "key":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .end local v6    # "score":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/Integer;>;"
    .end local v7    # "scores":Lcom/cigna/coach/apiobjects/Scores;
    .end local v8    # "userId":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 110
    :cond_1
    invoke-static {v5}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 111
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v9

    new-array v9, v9, [Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    return-object v9

    .line 90
    .restart local v0    # "category":Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    .restart local v1    # "ect":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;>;"
    .restart local v2    # "i":Ljava/lang/Integer;
    .restart local v4    # "key":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .restart local v6    # "score":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/Integer;>;"
    .restart local v7    # "scores":Lcom/cigna/coach/apiobjects/Scores;
    .restart local v8    # "userId":Ljava/lang/String;
    :pswitch_0
    :try_start_1
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->FOOD:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 91
    goto :goto_1

    .line 93
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->EXERCISE:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 94
    goto :goto_1

    .line 96
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->SLEEP:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 97
    goto :goto_1

    .line 99
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->STRESS:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 100
    goto :goto_1

    .line 102
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->WEIGHT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 88
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 39
    const v4, 0x7f030068

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 44
    .local v1, "rootView":Landroid/view/View;
    const v4, 0x7f0801ef

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 45
    .local v3, "scoreCoachMessage":Landroid/widget/TextView;
    const v4, 0x7f0801f0

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 46
    .local v2, "scoreCoachDescription":Landroid/widget/TextView;
    const v4, 0x7f0801f2

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 48
    .local v0, "remainStartBtn":Landroid/widget/Button;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;->findRemainCategory()[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;->mRemainCategory:[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 51
    const v4, 0x7f090343

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->getScoreCoachDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    new-instance v4, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleNoScoreFragment;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    return-object v1
.end method
