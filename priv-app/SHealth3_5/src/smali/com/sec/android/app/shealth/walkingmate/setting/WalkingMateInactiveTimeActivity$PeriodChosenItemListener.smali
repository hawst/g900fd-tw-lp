.class Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity$PeriodChosenItemListener;
.super Ljava/lang/Object;
.source "WalkingMateInactiveTimeActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PeriodChosenItemListener"
.end annotation


# instance fields
.field private mWalkingMateInactiveTimeActivity:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;

.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;)V
    .locals 1
    .param p2, "walkingMateInactiveTimeActivity"    # Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity$PeriodChosenItemListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity$PeriodChosenItemListener;->mWalkingMateInactiveTimeActivity:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;

    .line 50
    iput-object p2, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity$PeriodChosenItemListener;->mWalkingMateInactiveTimeActivity:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;

    .line 51
    return-void
.end method


# virtual methods
.method public onSave(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 54
    const-string v0, "WalkingMateInactiveTimeActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Selected index is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getInactiveTimeTrackingPeriod()I

    move-result v0

    add-int/lit8 v1, p1, 0x1

    mul-int/lit8 v1, v1, 0x1e

    if-eq v0, v1, :cond_0

    .line 56
    add-int/lit8 v0, p1, 0x1

    mul-int/lit8 v0, v0, 0x1e

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setInactiveTimeTrackingPeriod(I)V

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity$PeriodChosenItemListener;->mWalkingMateInactiveTimeActivity:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->setInactiveMonitor(I)V

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity$PeriodChosenItemListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;->mWalkingMateInactiveTimeFragment:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;->access$000(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;)Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->setCurrentInactiveConfig()V

    .line 60
    :cond_0
    return-void
.end method
