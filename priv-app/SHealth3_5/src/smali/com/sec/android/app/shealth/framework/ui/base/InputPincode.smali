.class public Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;
.super Landroid/support/v4/app/FragmentActivity;

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$3;,
        Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$SamsungAccountDialogButtonController;,
        Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$SipAndRecentKeyReceiver;
    }
.end annotation


# static fields
.field private static ACTION_CLOSE_SYSTEM_DIALOGS_REASON:Ljava/lang/String;

.field private static INPUT_PINCODE_POPUP:Ljava/lang/String;

.field private static IS_VISIBLE_WINDOW:Ljava/lang/String;

.field private static RECENT_KEY:Ljava/lang/String;

.field private static RESPONSE_AXT9INFO:Ljava/lang/String;


# instance fields
.field private ServiceName:Ljava/lang/String;

.field private TAG:Ljava/lang/String;

.field private bgImage:Landroid/widget/ImageView;

.field private isEnteredWithCorrectPasscode:Z

.field private isSetPasswordActivity:Z

.field private isToHomeActivity:Z

.field private final mDialogControllerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;",
            ">;"
        }
    .end annotation
.end field

.field private mEditTextPinCode:Landroid/widget/EditText;

.field private mHandler:Landroid/os/Handler;

.field private mIsDialogShown:Z

.field private mIsKeyBoardShown:Z

.field private mIsRecentKeyPressed:Z

.field private mKeyBoardStatus:Z

.field private mOutStateBundle:Landroid/os/Bundle;

.field private mParentLayout:Landroid/view/View;

.field private mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

.field private mSipAndRecentKeyReceiver:Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$SipAndRecentKeyReceiver;

.field private mSipIntentFilter:Landroid/content/IntentFilter;

.field private mStatusMessageText:Landroid/widget/TextView;

.field private screenHeight:I

.field private wrongPasswordcount:I


# direct methods
.method static constructor <clinit>()V
    .locals 26

    const/16 v0, 0xd

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, -0x46d4

    aput v14, v1, v13

    const/16 v13, -0x34

    aput v13, v1, v12

    const/16 v12, -0x58ee

    aput v12, v1, v11

    const/16 v11, -0x38

    aput v11, v1, v10

    const/16 v10, 0x3f15

    aput v10, v1, v9

    const/16 v9, -0x4fa0

    aput v9, v1, v8

    const/16 v8, -0x22

    aput v8, v1, v7

    const/16 v7, -0x69

    aput v7, v1, v6

    const/16 v6, -0x1a

    aput v6, v1, v5

    const/16 v5, -0x6e87

    aput v5, v1, v4

    const/16 v4, -0xa

    aput v4, v1, v3

    const/16 v3, -0x4dd3

    aput v3, v1, v2

    const/16 v2, -0x3f

    aput v2, v1, v0

    const/16 v0, 0xd

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, -0x46a4

    aput v15, v0, v14

    const/16 v14, -0x47

    aput v14, v0, v13

    const/16 v13, -0x589e

    aput v13, v0, v12

    const/16 v12, -0x59

    aput v12, v0, v11

    const/16 v11, 0x3f65

    aput v11, v0, v10

    const/16 v10, -0x4fc1

    aput v10, v0, v9

    const/16 v9, -0x50

    aput v9, v0, v8

    const/4 v8, -0x2

    aput v8, v0, v7

    const/16 v7, -0x47

    aput v7, v0, v6

    const/16 v6, -0x6ee9

    aput v6, v0, v5

    const/16 v5, -0x6f

    aput v5, v0, v4

    const/16 v4, -0x4dbc

    aput v4, v0, v3

    const/16 v3, -0x4e

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->INPUT_PINCODE_POPUP:Ljava/lang/String;

    const/4 v0, 0x6

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/16 v7, 0x4814

    aput v7, v1, v6

    const/16 v6, -0x67d9

    aput v6, v1, v5

    const/16 v5, -0x15

    aput v5, v1, v4

    const/16 v4, 0x1930

    aput v4, v1, v3

    const/16 v3, -0x3684

    aput v3, v1, v2

    const/16 v2, -0x45

    aput v2, v1, v0

    const/4 v0, 0x6

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/16 v8, 0x487a    # 2.6E-41f

    aput v8, v0, v7

    const/16 v7, -0x67b8

    aput v7, v0, v6

    const/16 v6, -0x68

    aput v6, v0, v5

    const/16 v5, 0x1951

    aput v5, v0, v4

    const/16 v4, -0x36e7

    aput v4, v0, v3

    const/16 v3, -0x37

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_2
    array-length v3, v0

    if-lt v2, v3, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_3
    array-length v3, v0

    if-lt v2, v3, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->ACTION_CLOSE_SYSTEM_DIALOGS_REASON:Ljava/lang/String;

    const/16 v0, 0x17

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, 0x552e

    aput v24, v1, v23

    const/16 v23, -0x62c6

    aput v23, v1, v22

    const/16 v22, -0x7

    aput v22, v1, v21

    const/16 v21, -0x68

    aput v21, v1, v20

    const/16 v20, -0x2dc1

    aput v20, v1, v19

    const/16 v19, -0x7b

    aput v19, v1, v18

    const/16 v18, 0x572a

    aput v18, v1, v17

    const/16 v17, -0x40c5

    aput v17, v1, v16

    const/16 v16, -0x23

    aput v16, v1, v15

    const/16 v15, -0x43

    aput v15, v1, v14

    const/16 v14, -0x31

    aput v14, v1, v13

    const/16 v13, -0x38

    aput v13, v1, v12

    const/16 v12, 0x4217

    aput v12, v1, v11

    const/16 v11, 0x6a31

    aput v11, v1, v10

    const/16 v10, 0x7403

    aput v10, v1, v9

    const/16 v9, 0x65a

    aput v9, v1, v8

    const/16 v8, 0x4f43

    aput v8, v1, v7

    const/16 v7, 0x5302

    aput v7, v1, v6

    const/16 v6, 0x3d1a

    aput v6, v1, v5

    const/16 v5, 0x2f04

    aput v5, v1, v4

    const/16 v4, 0x4e7b

    aput v4, v1, v3

    const/16 v3, -0x12ca

    aput v3, v1, v2

    const/16 v2, -0x54

    aput v2, v1, v0

    const/16 v0, 0x17

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x5559

    aput v25, v0, v24

    const/16 v24, -0x62ab

    aput v24, v0, v23

    const/16 v23, -0x63

    aput v23, v0, v22

    const/16 v22, -0xa

    aput v22, v0, v21

    const/16 v21, -0x2daa

    aput v21, v0, v20

    const/16 v20, -0x2e

    aput v20, v0, v19

    const/16 v19, 0x574f

    aput v19, v0, v18

    const/16 v18, -0x40a9

    aput v18, v0, v17

    const/16 v17, -0x41

    aput v17, v0, v16

    const/16 v16, -0x2c

    aput v16, v0, v15

    const/16 v15, -0x44

    aput v15, v0, v14

    const/16 v14, -0x5f

    aput v14, v0, v13

    const/16 v13, 0x4241

    aput v13, v0, v12

    const/16 v12, 0x6a42

    aput v12, v0, v11

    const/16 v11, 0x746a

    aput v11, v0, v10

    const/16 v10, 0x674

    aput v10, v0, v9

    const/16 v9, 0x4f06

    aput v9, v0, v8

    const/16 v8, 0x534f

    aput v8, v0, v7

    const/16 v7, 0x3d53

    aput v7, v0, v6

    const/16 v6, 0x2f3d

    aput v6, v0, v5

    const/16 v5, 0x4e2f

    aput v5, v0, v4

    const/16 v4, -0x12b2

    aput v4, v0, v3

    const/16 v3, -0x13

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_4
    array-length v3, v0

    if-lt v2, v3, :cond_4

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_5
    array-length v3, v0

    if-lt v2, v3, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->IS_VISIBLE_WINDOW:Ljava/lang/String;

    const/16 v0, 0xa

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, -0x59b

    aput v11, v1, v10

    const/16 v10, -0x76

    aput v10, v1, v9

    const/16 v9, -0x68d5

    aput v9, v1, v8

    const/16 v8, -0xa

    aput v8, v1, v7

    const/16 v7, -0x38

    aput v7, v1, v6

    const/16 v6, -0x28b8

    aput v6, v1, v5

    const/16 v5, -0x4e

    aput v5, v1, v4

    const/16 v4, -0xb

    aput v4, v1, v3

    const/16 v3, 0x4b04

    aput v3, v1, v2

    const/16 v2, -0xc7

    aput v2, v1, v0

    const/16 v0, 0xa

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x5ea

    aput v12, v0, v11

    const/4 v11, -0x6

    aput v11, v0, v10

    const/16 v10, -0x68a5

    aput v10, v0, v9

    const/16 v9, -0x69

    aput v9, v0, v8

    const/16 v8, -0x44

    aput v8, v0, v7

    const/16 v7, -0x28da

    aput v7, v0, v6

    const/16 v6, -0x29

    aput v6, v0, v5

    const/16 v5, -0x6a

    aput v5, v0, v4

    const/16 v4, 0x4b61

    aput v4, v0, v3

    const/16 v3, -0xb5

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_6
    array-length v3, v0

    if-lt v2, v3, :cond_6

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_7
    array-length v3, v0

    if-lt v2, v3, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->RECENT_KEY:Ljava/lang/String;

    const/16 v0, 0x10

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x1e54

    aput v17, v1, v16

    const/16 v16, 0x6578

    aput v16, v1, v15

    const/16 v15, -0x74f5

    aput v15, v1, v14

    const/16 v14, -0x3e

    aput v14, v1, v13

    const/16 v13, 0x295a

    aput v13, v1, v12

    const/16 v12, 0x167d

    aput v12, v1, v11

    const/16 v11, -0x4292

    aput v11, v1, v10

    const/4 v10, -0x4

    aput v10, v1, v9

    const/16 v9, -0x24fd

    aput v9, v1, v8

    const/16 v8, -0x58

    aput v8, v1, v7

    const/16 v7, -0x74

    aput v7, v1, v6

    const/16 v6, -0x65b8

    aput v6, v1, v5

    const/16 v5, -0x16

    aput v5, v1, v4

    const/4 v4, -0x3

    aput v4, v1, v3

    const/16 v3, -0x678a

    aput v3, v1, v2

    const/16 v2, -0x36

    aput v2, v1, v0

    const/16 v0, 0x10

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x1e3b

    aput v18, v0, v17

    const/16 v17, 0x651e

    aput v17, v0, v16

    const/16 v16, -0x749b

    aput v16, v0, v15

    const/16 v15, -0x75

    aput v15, v0, v14

    const/16 v14, 0x2963

    aput v14, v0, v13

    const/16 v13, 0x1629

    aput v13, v0, v12

    const/16 v12, -0x42ea

    aput v12, v0, v11

    const/16 v11, -0x43

    aput v11, v0, v10

    const/16 v10, -0x249a

    aput v10, v0, v9

    const/16 v9, -0x25

    aput v9, v0, v8

    const/16 v8, -0x1e

    aput v8, v0, v7

    const/16 v7, -0x65d9

    aput v7, v0, v6

    const/16 v6, -0x66

    aput v6, v0, v5

    const/16 v5, -0x72

    aput v5, v0, v4

    const/16 v4, -0x67ed

    aput v4, v0, v3

    const/16 v3, -0x68

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_8
    array-length v3, v0

    if-lt v2, v3, :cond_8

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_9
    array-length v3, v0

    if-lt v2, v3, :cond_9

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->RESPONSE_AXT9INFO:Ljava/lang/String;

    return-void

    :cond_0
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_1
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    :cond_2
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    :cond_3
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_3

    :cond_4
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_4

    :cond_5
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_5

    :cond_6
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_6

    :cond_7
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_7

    :cond_8
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    :cond_9
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_9
.end method

.method public constructor <init>()V
    .locals 17

    invoke-direct/range {p0 .. p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->wrongPasswordcount:I

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->isEnteredWithCorrectPasscode:Z

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, -0x1192

    aput v10, v2, v9

    const/16 v9, -0x66

    aput v9, v2, v8

    const/16 v8, -0x2790

    aput v8, v2, v7

    const/16 v7, -0x47

    aput v7, v2, v6

    const/16 v6, -0x32

    aput v6, v2, v5

    const/16 v5, -0x7a

    aput v5, v2, v4

    const/16 v4, 0x7c51

    aput v4, v2, v3

    const/16 v3, 0x782f

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, -0x11fa

    aput v11, v1, v10

    const/16 v10, -0x12

    aput v10, v1, v9

    const/16 v9, -0x27e4

    aput v9, v1, v8

    const/16 v8, -0x28

    aput v8, v1, v7

    const/16 v7, -0x55

    aput v7, v1, v6

    const/16 v6, -0x32

    aput v6, v1, v5

    const/16 v5, 0x7c71

    aput v5, v1, v4

    const/16 v4, 0x787c

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->TAG:Ljava/lang/String;

    const/16 v1, 0xd

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, -0x5c

    aput v15, v2, v14

    const/4 v14, -0x8

    aput v14, v2, v13

    const/16 v13, -0x31

    aput v13, v2, v12

    const/16 v12, -0x74f0

    aput v12, v2, v11

    const/16 v11, -0x12

    aput v11, v2, v10

    const/16 v10, -0x21

    aput v10, v2, v9

    const/16 v9, 0x3f1d

    aput v9, v2, v8

    const/16 v8, 0x6851

    aput v8, v2, v7

    const/16 v7, 0x5109

    aput v7, v2, v6

    const/16 v6, -0x34e4

    aput v6, v2, v5

    const/16 v5, -0x4e

    aput v5, v2, v4

    const/16 v4, -0x67d0

    aput v4, v2, v3

    const/16 v3, -0x2d

    aput v3, v2, v1

    const/16 v1, 0xd

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, -0x7c

    aput v16, v1, v15

    const/16 v15, -0x3e

    aput v15, v1, v14

    const/16 v14, -0x11

    aput v14, v1, v13

    const/16 v13, -0x749e

    aput v13, v1, v12

    const/16 v12, -0x75

    aput v12, v1, v11

    const/16 v11, -0x48

    aput v11, v1, v10

    const/16 v10, 0x3f7c

    aput v10, v1, v9

    const/16 v9, 0x683f

    aput v9, v1, v8

    const/16 v8, 0x5168

    aput v8, v1, v7

    const/16 v7, -0x34af

    aput v7, v1, v6

    const/16 v6, -0x35

    aput v6, v1, v5

    const/16 v5, -0x67ab

    aput v5, v1, v4

    const/16 v4, -0x68

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v4, v1

    if-lt v3, v4, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v4, v1

    if-lt v3, v4, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->ServiceName:Ljava/lang/String;

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mOutStateBundle:Landroid/os/Bundle;

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mIsKeyBoardShown:Z

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mKeyBoardStatus:Z

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mIsRecentKeyPressed:Z

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mIsDialogShown:Z

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->isSetPasswordActivity:Z

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->isToHomeActivity:Z

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$1;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mDialogControllerMap:Ljava/util/Map;

    return-void

    :cond_0
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mIsRecentKeyPressed:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mIsRecentKeyPressed:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mIsDialogShown:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mIsDialogShown:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mIsKeyBoardShown:Z

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mIsKeyBoardShown:Z

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;)Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mOutStateBundle:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mKeyBoardStatus:Z

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mKeyBoardStatus:Z

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mEditTextPinCode:Landroid/widget/EditText;

    return-object v0
.end method

.method private isCorrecPassword()Z
    .locals 33

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mEditTextPinCode:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    const/4 v1, 0x0

    :goto_1
    return v1

    :cond_0
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/service/health/keyManager/KeyManager;->isCompletePassword(Ljava/lang/String;)Z

    move-result v4

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    sget-boolean v1, Lcom/sec/android/service/health/keyManager/KeyManager;->DEBUG:Z

    if-eqz v1, :cond_1

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/4 v11, -0x8

    aput v11, v2, v10

    const/16 v10, 0x7112

    aput v10, v2, v9

    const/16 v9, 0x491d

    aput v9, v2, v8

    const/16 v8, 0x2a28

    aput v8, v2, v7

    const/16 v7, -0x62b1

    aput v7, v2, v6

    const/16 v6, -0x2b

    aput v6, v2, v5

    const/16 v5, -0x6bc6

    aput v5, v2, v3

    const/16 v3, -0x39

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, -0x70

    aput v12, v1, v11

    const/16 v11, 0x7166

    aput v11, v1, v10

    const/16 v10, 0x4971

    aput v10, v1, v9

    const/16 v9, 0x2a49

    aput v9, v1, v8

    const/16 v8, -0x62d6

    aput v8, v1, v7

    const/16 v7, -0x63

    aput v7, v1, v6

    const/16 v6, -0x6be6

    aput v6, v1, v5

    const/16 v5, -0x6c

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_3

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x1a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, -0x20

    aput v31, v2, v30

    const/16 v30, -0x8

    aput v30, v2, v29

    const/16 v29, 0x5430

    aput v29, v2, v28

    const/16 v28, -0x6ad0

    aput v28, v2, v27

    const/16 v27, -0x10

    aput v27, v2, v26

    const/16 v26, -0x7e

    aput v26, v2, v25

    const/16 v25, 0x779

    aput v25, v2, v24

    const/16 v24, 0x3a64

    aput v24, v2, v23

    const/16 v23, -0x5b1

    aput v23, v2, v22

    const/16 v22, -0x57

    aput v22, v2, v21

    const/16 v21, -0x3c

    aput v21, v2, v20

    const/16 v20, -0x72

    aput v20, v2, v19

    const/16 v19, -0x1c

    aput v19, v2, v18

    const/16 v18, -0x34

    aput v18, v2, v17

    const/16 v17, -0x6b

    aput v17, v2, v16

    const/16 v16, -0x1bb5

    aput v16, v2, v15

    const/16 v15, -0x6a

    aput v15, v2, v14

    const/16 v14, -0x51

    aput v14, v2, v13

    const/16 v13, -0x32

    aput v13, v2, v12

    const/16 v12, 0x6d28

    aput v12, v2, v11

    const/16 v11, -0x6ffd

    aput v11, v2, v10

    const/16 v10, -0xf

    aput v10, v2, v9

    const/16 v9, 0x6531

    aput v9, v2, v8

    const/16 v8, -0x43e4

    aput v8, v2, v7

    const/16 v7, -0x27

    aput v7, v2, v3

    const/16 v3, -0x7e

    aput v3, v2, v1

    const/16 v1, 0x1a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x11

    const/16 v24, 0x12

    const/16 v25, 0x13

    const/16 v26, 0x14

    const/16 v27, 0x15

    const/16 v28, 0x16

    const/16 v29, 0x17

    const/16 v30, 0x18

    const/16 v31, 0x19

    const/16 v32, -0x40

    aput v32, v1, v31

    const/16 v31, -0x3e

    aput v31, v1, v30

    const/16 v30, 0x5410

    aput v30, v1, v29

    const/16 v29, -0x6aac

    aput v29, v1, v28

    const/16 v28, -0x6b

    aput v28, v1, v27

    const/16 v27, -0x19

    aput v27, v1, v26

    const/16 v26, 0x71a

    aput v26, v1, v25

    const/16 v25, 0x3a07

    aput v25, v1, v24

    const/16 v24, -0x5c6

    aput v24, v1, v23

    const/16 v23, -0x6

    aput v23, v1, v22

    const/16 v22, -0x49

    aput v22, v1, v21

    const/16 v21, -0x19

    aput v21, v1, v20

    const/16 v20, -0x36

    aput v20, v1, v19

    const/16 v19, -0x14

    aput v19, v1, v18

    const/16 v18, -0x51

    aput v18, v1, v17

    const/16 v17, -0x1b95

    aput v17, v1, v16

    const/16 v16, -0x1c

    aput v16, v1, v15

    const/16 v15, -0x36

    aput v15, v1, v14

    const/16 v14, -0x57

    aput v14, v1, v13

    const/16 v13, 0x6d49

    aput v13, v1, v12

    const/16 v12, -0x6f93

    aput v12, v1, v11

    const/16 v11, -0x70

    aput v11, v1, v10

    const/16 v10, 0x657c

    aput v10, v1, v9

    const/16 v9, -0x439b

    aput v9, v1, v8

    const/16 v8, -0x44

    aput v8, v1, v7

    const/16 v7, -0x37

    aput v7, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v7, v1

    if-lt v3, v7, :cond_5

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v7, v1

    if-lt v3, v7, :cond_6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mEditTextPinCode:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->setPasswordByUser(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    iget-boolean v1, v1, Lcom/sec/android/service/health/keyManager/KeyManager;->setting_password:Z

    if-nez v1, :cond_2

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mEditTextPinCode:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/service/health/keyManager/KeyManager;->getDBKeyFromKeyStore(Ljava/lang/String;I)Ljava/lang/String;

    :cond_2
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/sec/android/service/health/keyManager/KeyManager;->setting_password:Z

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->setPasswordActivity(Z)V

    move v1, v4

    goto/16 :goto_1

    :cond_3
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_4
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_5
    aget v7, v1, v3

    aget v8, v2, v3

    xor-int/2addr v7, v8

    aput v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_6
    aget v7, v2, v3

    int-to-char v7, v7

    aput-char v7, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :catch_0
    move-exception v1

    goto/16 :goto_0
.end method

.method private isPinCorrect()Z
    .locals 51

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    const/16 v1, 0x16

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, -0x5dd1

    aput v25, v2, v24

    const/16 v24, -0x30

    aput v24, v2, v23

    const/16 v23, -0x67

    aput v23, v2, v22

    const/16 v22, -0x72

    aput v22, v2, v21

    const/16 v21, -0x48

    aput v21, v2, v20

    const/16 v20, -0x2b

    aput v20, v2, v19

    const/16 v19, -0x5eea

    aput v19, v2, v18

    const/16 v18, -0x2f

    aput v18, v2, v17

    const/16 v17, -0x3f

    aput v17, v2, v16

    const/16 v16, 0x4a1f

    aput v16, v2, v15

    const/16 v15, 0x4629

    aput v15, v2, v14

    const/16 v14, 0x1229

    aput v14, v2, v13

    const/16 v13, 0x3d7e

    aput v13, v2, v12

    const/16 v12, 0x1862

    aput v12, v2, v11

    const/16 v11, 0x5561

    aput v11, v2, v10

    const/16 v10, -0x1edf

    aput v10, v2, v9

    const/16 v9, -0x78

    aput v9, v2, v8

    const/16 v8, -0x5a

    aput v8, v2, v7

    const/16 v7, -0x53

    aput v7, v2, v6

    const/16 v6, 0x3e09

    aput v6, v2, v5

    const/16 v5, -0x55a5

    aput v5, v2, v3

    const/16 v3, -0x27

    aput v3, v2, v1

    const/16 v1, 0x16

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, -0x5db5

    aput v26, v1, v25

    const/16 v25, -0x5e

    aput v25, v1, v24

    const/16 v24, -0xa

    aput v24, v1, v23

    const/16 v23, -0x7

    aput v23, v1, v22

    const/16 v22, -0x35

    aput v22, v1, v21

    const/16 v21, -0x5a

    aput v21, v1, v20

    const/16 v20, -0x5e89

    aput v20, v1, v19

    const/16 v19, -0x5f

    aput v19, v1, v18

    const/16 v18, -0x62

    aput v18, v1, v17

    const/16 v17, 0x4a74

    aput v17, v1, v16

    const/16 v16, 0x464a

    aput v16, v1, v15

    const/16 v15, 0x1246

    aput v15, v1, v14

    const/16 v14, 0x3d12

    aput v14, v1, v13

    const/16 v13, 0x183d

    aput v13, v1, v12

    const/16 v12, 0x5518

    aput v12, v1, v11

    const/16 v11, -0x1eab

    aput v11, v1, v10

    const/16 v10, -0x1f

    aput v10, v1, v9

    const/16 v9, -0x2c

    aput v9, v1, v8

    const/16 v8, -0x28

    aput v8, v1, v7

    const/16 v7, 0x3e6a

    aput v7, v1, v6

    const/16 v6, -0x55c2

    aput v6, v1, v5

    const/16 v5, -0x56

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_1

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getStringValueForConfigKey(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mEditTextPinCode:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2, v1}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->doesPinCodeMatch(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x6714

    aput v10, v2, v9

    const/16 v9, 0x113

    aput v9, v2, v8

    const/16 v8, 0x746d

    aput v8, v2, v7

    const/16 v7, -0x3eeb

    aput v7, v2, v6

    const/16 v6, -0x5c

    aput v6, v2, v5

    const/16 v5, -0x31ce

    aput v5, v2, v4

    const/16 v4, -0x12

    aput v4, v2, v3

    const/16 v3, -0x23

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x677c

    aput v11, v1, v10

    const/16 v10, 0x167

    aput v10, v1, v9

    const/16 v9, 0x7401

    aput v9, v1, v8

    const/16 v8, -0x3e8c

    aput v8, v1, v7

    const/16 v7, -0x3f

    aput v7, v1, v6

    const/16 v6, -0x3186

    aput v6, v1, v5

    const/16 v5, -0x32

    aput v5, v1, v4

    const/16 v4, -0x72

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v4, v1

    if-lt v3, v4, :cond_3

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v4, v1

    if-lt v3, v4, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x2e

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, 0x27

    const/16 v43, 0x28

    const/16 v44, 0x29

    const/16 v45, 0x2a

    const/16 v46, 0x2b

    const/16 v47, 0x2c

    const/16 v48, 0x2d

    const/16 v49, -0x708f

    aput v49, v2, v48

    const/16 v48, -0x3

    aput v48, v2, v47

    const/16 v47, -0x44c8

    aput v47, v2, v46

    const/16 v46, -0x34

    aput v46, v2, v45

    const/16 v45, -0x6f

    aput v45, v2, v44

    const/16 v44, 0xd54

    aput v44, v2, v43

    const/16 v43, -0x3c94

    aput v43, v2, v42

    const/16 v42, -0x4d

    aput v42, v2, v41

    const/16 v41, -0x77

    aput v41, v2, v40

    const/16 v40, -0x3c

    aput v40, v2, v39

    const/16 v39, -0x598b

    aput v39, v2, v38

    const/16 v38, -0x39

    aput v38, v2, v37

    const/16 v37, -0x2fd5

    aput v37, v2, v36

    const/16 v36, -0x10

    aput v36, v2, v35

    const/16 v35, -0x62a0

    aput v35, v2, v34

    const/16 v34, -0x46

    aput v34, v2, v33

    const/16 v33, -0x44

    aput v33, v2, v32

    const/16 v32, -0x2791

    aput v32, v2, v31

    const/16 v31, -0x8

    aput v31, v2, v30

    const/16 v30, 0x4c0d

    aput v30, v2, v29

    const/16 v29, -0x1994

    aput v29, v2, v28

    const/16 v28, -0x7d

    aput v28, v2, v27

    const/16 v27, -0x10

    aput v27, v2, v26

    const/16 v26, -0x8

    aput v26, v2, v25

    const/16 v25, 0x6d4b

    aput v25, v2, v24

    const/16 v24, 0x7f03

    aput v24, v2, v23

    const/16 v23, -0x24ea

    aput v23, v2, v22

    const/16 v22, -0x75

    aput v22, v2, v21

    const/16 v21, -0x13

    aput v21, v2, v20

    const/16 v20, -0x7c

    aput v20, v2, v19

    const/16 v19, 0x6e47

    aput v19, v2, v18

    const/16 v18, -0x6500

    aput v18, v2, v17

    const/16 v17, -0x2e

    aput v17, v2, v16

    const/16 v16, -0xad8

    aput v16, v2, v15

    const/16 v15, -0x31

    aput v15, v2, v14

    const/16 v14, -0x21

    aput v14, v2, v13

    const/16 v13, -0x11

    aput v13, v2, v12

    const/4 v12, -0x1

    aput v12, v2, v11

    const/16 v11, 0x704d

    aput v11, v2, v10

    const/16 v10, -0x74ef

    aput v10, v2, v9

    const/16 v9, -0x1b

    aput v9, v2, v8

    const/16 v8, -0x5c

    aput v8, v2, v7

    const/16 v7, -0x108c

    aput v7, v2, v6

    const/16 v6, -0x6a

    aput v6, v2, v5

    const/16 v5, 0x5a7d

    aput v5, v2, v3

    const/16 v3, 0x811

    aput v3, v2, v1

    const/16 v1, 0x2e

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, 0x26

    const/16 v43, 0x27

    const/16 v44, 0x28

    const/16 v45, 0x29

    const/16 v46, 0x2a

    const/16 v47, 0x2b

    const/16 v48, 0x2c

    const/16 v49, 0x2d

    const/16 v50, -0x70eb

    aput v50, v1, v49

    const/16 v49, -0x71

    aput v49, v1, v48

    const/16 v48, -0x44a9

    aput v48, v1, v47

    const/16 v47, -0x45

    aput v47, v1, v46

    const/16 v46, -0x1e

    aput v46, v1, v45

    const/16 v45, 0xd27

    aput v45, v1, v44

    const/16 v44, -0x3cf3

    aput v44, v1, v43

    const/16 v43, -0x3d

    aput v43, v1, v42

    const/16 v42, -0x57

    aput v42, v1, v41

    const/16 v41, -0x5f

    aput v41, v1, v40

    const/16 v40, -0x59e8

    aput v40, v1, v39

    const/16 v39, -0x5a

    aput v39, v1, v38

    const/16 v38, -0x2fa8

    aput v38, v1, v37

    const/16 v37, -0x30

    aput v37, v1, v36

    const/16 v36, -0x62ed

    aput v36, v1, v35

    const/16 v35, -0x63

    aput v35, v1, v34

    const/16 v34, -0x38

    aput v34, v1, v33

    const/16 v33, -0x27da

    aput v33, v1, v32

    const/16 v32, -0x28

    aput v32, v1, v31

    const/16 v31, 0x4c37

    aput v31, v1, v30

    const/16 v30, -0x19b4

    aput v30, v1, v29

    const/16 v29, -0x1a

    aput v29, v1, v28

    const/16 v28, -0x6c

    aput v28, v1, v27

    const/16 v27, -0x69

    aput v27, v1, v26

    const/16 v26, 0x6d28

    aput v26, v1, v25

    const/16 v25, 0x7f6d

    aput v25, v1, v24

    const/16 v24, -0x2481

    aput v24, v1, v23

    const/16 v23, -0x25

    aput v23, v1, v22

    const/16 v22, -0x67

    aput v22, v1, v21

    const/16 v21, -0xf

    aput v21, v1, v20

    const/16 v20, 0x6e37

    aput v20, v1, v19

    const/16 v19, -0x6492

    aput v19, v1, v18

    const/16 v18, -0x65

    aput v18, v1, v17

    const/16 v17, -0xaf8

    aput v17, v1, v16

    const/16 v16, -0xb

    aput v16, v1, v15

    const/4 v15, -0x1

    aput v15, v1, v14

    const/16 v14, -0x63

    aput v14, v1, v13

    const/16 v13, -0x66

    aput v13, v1, v12

    const/16 v12, 0x702a

    aput v12, v1, v11

    const/16 v11, -0x7490

    aput v11, v1, v10

    const/16 v10, -0x75

    aput v10, v1, v9

    const/16 v9, -0x3b

    aput v9, v1, v8

    const/16 v8, -0x10c7

    aput v8, v1, v7

    const/16 v7, -0x11

    aput v7, v1, v6

    const/16 v6, 0x5a18

    aput v6, v1, v5

    const/16 v5, 0x85a

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_5

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mEditTextPinCode:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->changeDbKeyIfNeeded(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mEditTextPinCode:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->setPasswordByUser(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    iget-boolean v1, v1, Lcom/sec/android/service/health/keyManager/KeyManager;->setting_password:Z

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mEditTextPinCode:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/service/health/keyManager/KeyManager;->getDBKeyFromKeyStore(Ljava/lang/String;I)Ljava/lang/String;

    :cond_0
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/sec/android/service/health/keyManager/KeyManager;->setting_password:Z

    const/4 v1, 0x1

    :goto_6
    return v1

    :cond_1
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_2
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_3
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_4
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_5
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_6
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_7
    const/4 v1, 0x0

    goto :goto_6
.end method

.method public static rQYC()Ljava/lang/String;
    .locals 64

    const/16 v0, 0x3d

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, 0x17

    const/16 v25, 0x18

    const/16 v26, 0x19

    const/16 v27, 0x1a

    const/16 v28, 0x1b

    const/16 v29, 0x1c

    const/16 v30, 0x1d

    const/16 v31, 0x1e

    const/16 v32, 0x1f

    const/16 v33, 0x20

    const/16 v34, 0x21

    const/16 v35, 0x22

    const/16 v36, 0x23

    const/16 v37, 0x24

    const/16 v38, 0x25

    const/16 v39, 0x26

    const/16 v40, 0x27

    const/16 v41, 0x28

    const/16 v42, 0x29

    const/16 v43, 0x2a

    const/16 v44, 0x2b

    const/16 v45, 0x2c

    const/16 v46, 0x2d

    const/16 v47, 0x2e

    const/16 v48, 0x2f

    const/16 v49, 0x30

    const/16 v50, 0x31

    const/16 v51, 0x32

    const/16 v52, 0x33

    const/16 v53, 0x34

    const/16 v54, 0x35

    const/16 v55, 0x36

    const/16 v56, 0x37

    const/16 v57, 0x38

    const/16 v58, 0x39

    const/16 v59, 0x3a

    const/16 v60, 0x3b

    const/16 v61, 0x3c

    const/16 v62, -0x40

    aput v62, v1, v61

    const/16 v61, -0x54

    aput v61, v1, v60

    const/16 v60, -0x8

    aput v60, v1, v59

    const/16 v59, -0x5b

    aput v59, v1, v58

    const/16 v58, -0x11

    aput v58, v1, v57

    const/16 v57, -0x79

    aput v57, v1, v56

    const/16 v56, -0x7894

    aput v56, v1, v55

    const/16 v55, -0xd

    aput v55, v1, v54

    const/16 v54, 0x1f32

    aput v54, v1, v53

    const/16 v53, 0x607a

    aput v53, v1, v52

    const/16 v52, 0x1413

    aput v52, v1, v51

    const/16 v51, -0x64cc

    aput v51, v1, v50

    const/16 v50, -0x17

    aput v50, v1, v49

    const/16 v49, -0x52b5

    aput v49, v1, v48

    const/16 v48, -0x25

    aput v48, v1, v47

    const/16 v47, -0x7faf

    aput v47, v1, v46

    const/16 v46, -0x1b

    aput v46, v1, v45

    const/16 v45, -0x29

    aput v45, v1, v44

    const/16 v44, 0x2014

    aput v44, v1, v43

    const/16 v43, 0x3272

    aput v43, v1, v42

    const/16 v42, -0x48c0

    aput v42, v1, v41

    const/16 v41, -0x2e

    aput v41, v1, v40

    const/16 v40, -0x1fe6

    aput v40, v1, v39

    const/16 v39, -0x7f

    aput v39, v1, v38

    const/16 v38, -0x44

    aput v38, v1, v37

    const/16 v37, -0x5e

    aput v37, v1, v36

    const/16 v36, -0x558a

    aput v36, v1, v35

    const/16 v35, -0x2d

    aput v35, v1, v34

    const/16 v34, -0xa

    aput v34, v1, v33

    const/16 v33, -0x13

    aput v33, v1, v32

    const/16 v32, -0x10

    aput v32, v1, v31

    const/16 v31, -0x108c

    aput v31, v1, v30

    const/16 v30, -0x65

    aput v30, v1, v29

    const/16 v29, -0x15

    aput v29, v1, v28

    const/16 v28, 0x4c33

    aput v28, v1, v27

    const/16 v27, 0x6129

    aput v27, v1, v26

    const/16 v26, 0x7409

    aput v26, v1, v25

    const/16 v25, 0x4e5a

    aput v25, v1, v24

    const/16 v24, 0x2d2b

    aput v24, v1, v23

    const/16 v23, -0x50b2

    aput v23, v1, v22

    const/16 v22, -0x3a

    aput v22, v1, v21

    const/16 v21, -0x58

    aput v21, v1, v20

    const/16 v20, -0x80

    aput v20, v1, v19

    const/16 v19, -0x20e0

    aput v19, v1, v18

    const/16 v18, -0x54

    aput v18, v1, v17

    const/16 v17, -0x34

    aput v17, v1, v16

    const/16 v16, -0x67

    aput v16, v1, v15

    const/16 v15, -0x6c

    aput v15, v1, v14

    const/16 v14, -0x739f

    aput v14, v1, v13

    const/4 v13, -0x2

    aput v13, v1, v12

    const/16 v12, -0x67

    aput v12, v1, v11

    const/16 v11, -0x5e

    aput v11, v1, v10

    const/16 v10, -0x1f

    aput v10, v1, v9

    const/16 v9, 0x6949

    aput v9, v1, v8

    const/16 v8, 0x7b0a

    aput v8, v1, v7

    const/16 v7, -0x25e2

    aput v7, v1, v6

    const/16 v6, -0x57

    aput v6, v1, v5

    const/4 v5, -0x6

    aput v5, v1, v4

    const/16 v4, -0x23ca

    aput v4, v1, v3

    const/16 v3, -0x4d

    aput v3, v1, v2

    const/16 v2, 0x3e3d

    aput v2, v1, v0

    const/16 v0, 0x3d

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, 0x1e

    const/16 v33, 0x1f

    const/16 v34, 0x20

    const/16 v35, 0x21

    const/16 v36, 0x22

    const/16 v37, 0x23

    const/16 v38, 0x24

    const/16 v39, 0x25

    const/16 v40, 0x26

    const/16 v41, 0x27

    const/16 v42, 0x28

    const/16 v43, 0x29

    const/16 v44, 0x2a

    const/16 v45, 0x2b

    const/16 v46, 0x2c

    const/16 v47, 0x2d

    const/16 v48, 0x2e

    const/16 v49, 0x2f

    const/16 v50, 0x30

    const/16 v51, 0x31

    const/16 v52, 0x32

    const/16 v53, 0x33

    const/16 v54, 0x34

    const/16 v55, 0x35

    const/16 v56, 0x36

    const/16 v57, 0x37

    const/16 v58, 0x38

    const/16 v59, 0x39

    const/16 v60, 0x3a

    const/16 v61, 0x3b

    const/16 v62, 0x3c

    const/16 v63, -0x6e

    aput v63, v0, v62

    const/16 v62, -0x12

    aput v62, v0, v61

    const/16 v61, -0x28

    aput v61, v0, v60

    const/16 v60, -0x3c

    aput v60, v0, v59

    const/16 v59, -0x7a

    aput v59, v0, v58

    const/16 v58, -0xf

    aput v58, v0, v57

    const/16 v57, -0x78b4

    aput v57, v0, v56

    const/16 v56, -0x79

    aput v56, v0, v55

    const/16 v55, 0x1f5c

    aput v55, v0, v54

    const/16 v54, 0x601f

    aput v54, v0, v53

    const/16 v53, 0x1460

    aput v53, v0, v52

    const/16 v52, -0x64ec

    aput v52, v0, v51

    const/16 v51, -0x65

    aput v51, v0, v50

    const/16 v50, -0x52d2

    aput v50, v0, v49

    const/16 v49, -0x53

    aput v49, v0, v48

    const/16 v48, -0x7fc8

    aput v48, v0, v47

    const/16 v47, -0x80

    aput v47, v0, v46

    const/16 v46, -0x4c

    aput v46, v0, v45

    const/16 v45, 0x2071

    aput v45, v0, v44

    const/16 v44, 0x3220

    aput v44, v0, v43

    const/16 v43, -0x48ce

    aput v43, v0, v42

    const/16 v42, -0x49

    aput v42, v0, v41

    const/16 v41, -0x1f83

    aput v41, v0, v40

    const/16 v40, -0x20

    aput v40, v0, v39

    const/16 v39, -0x2e

    aput v39, v0, v38

    const/16 v38, -0x3d

    aput v38, v0, v37

    const/16 v37, -0x55c5

    aput v37, v0, v36

    const/16 v36, -0x56

    aput v36, v0, v35

    const/16 v35, -0x6d

    aput v35, v0, v34

    const/16 v34, -0x5a

    aput v34, v0, v33

    const/16 v33, -0x22

    aput v33, v0, v32

    const/16 v32, -0x10e4

    aput v32, v0, v31

    const/16 v31, -0x11

    aput v31, v0, v30

    const/16 v30, -0x79

    aput v30, v0, v29

    const/16 v29, 0x4c52

    aput v29, v0, v28

    const/16 v28, 0x614c

    aput v28, v0, v27

    const/16 v27, 0x7461

    aput v27, v0, v26

    const/16 v26, 0x4e74

    aput v26, v0, v25

    const/16 v25, 0x2d4e

    aput v25, v0, v24

    const/16 v24, -0x50d3

    aput v24, v0, v23

    const/16 v23, -0x51

    aput v23, v0, v22

    const/16 v22, -0x22

    aput v22, v0, v21

    const/16 v21, -0xe

    aput v21, v0, v20

    const/16 v20, -0x20bb

    aput v20, v0, v19

    const/16 v19, -0x21

    aput v19, v0, v18

    const/16 v18, -0x1e

    aput v18, v0, v17

    const/16 v17, -0x3

    aput v17, v0, v16

    const/16 v16, -0x3

    aput v16, v0, v15

    const/16 v15, -0x73f2

    aput v15, v0, v14

    const/16 v14, -0x74

    aput v14, v0, v13

    const/4 v13, -0x3

    aput v13, v0, v12

    const/16 v12, -0x34

    aput v12, v0, v11

    const/16 v11, -0x80

    aput v11, v0, v10

    const/16 v10, 0x6967

    aput v10, v0, v9

    const/16 v9, 0x7b69

    aput v9, v0, v8

    const/16 v8, -0x2585

    aput v8, v0, v7

    const/16 v7, -0x26

    aput v7, v0, v6

    const/16 v6, -0x2c

    aput v6, v0, v5

    const/16 v5, -0x23a5

    aput v5, v0, v4

    const/16 v4, -0x24

    aput v4, v0, v3

    const/16 v3, 0x3e5e

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private registerSipAndRecentKeyReceiver()V
    .locals 47

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mSipAndRecentKeyReceiver:Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$SipAndRecentKeyReceiver;

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$SipAndRecentKeyReceiver;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$SipAndRecentKeyReceiver;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$1;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mSipAndRecentKeyReceiver:Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$SipAndRecentKeyReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mSipIntentFilter:Landroid/content/IntentFilter;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mSipIntentFilter:Landroid/content/IntentFilter;

    const/16 v1, 0x10

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, -0x21a4

    aput v19, v2, v18

    const/16 v18, -0x48

    aput v18, v2, v17

    const/16 v17, -0x77

    aput v17, v2, v16

    const/16 v16, -0x44

    aput v16, v2, v15

    const/16 v15, -0x2d

    aput v15, v2, v14

    const/16 v14, -0x65d5

    aput v14, v2, v13

    const/16 v13, -0x1e

    aput v13, v2, v12

    const/16 v12, 0x5161

    aput v12, v2, v11

    const/16 v11, -0xecc

    aput v11, v2, v10

    const/16 v10, -0x7e

    aput v10, v2, v9

    const/16 v9, -0x79

    aput v9, v2, v8

    const/16 v8, -0x68

    aput v8, v2, v7

    const/16 v7, -0x45d3

    aput v7, v2, v6

    const/16 v6, -0x37

    aput v6, v2, v5

    const/16 v5, -0x5a

    aput v5, v2, v3

    const/16 v3, -0xca7

    aput v3, v2, v1

    const/16 v1, 0x10

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, -0x21cd

    aput v20, v1, v19

    const/16 v19, -0x22

    aput v19, v1, v18

    const/16 v18, -0x19

    aput v18, v1, v17

    const/16 v17, -0xb

    aput v17, v1, v16

    const/16 v16, -0x16

    aput v16, v1, v15

    const/16 v15, -0x6581

    aput v15, v1, v14

    const/16 v14, -0x66

    aput v14, v1, v13

    const/16 v13, 0x5120

    aput v13, v1, v12

    const/16 v12, -0xeaf

    aput v12, v1, v11

    const/16 v11, -0xf

    aput v11, v1, v10

    const/16 v10, -0x17

    aput v10, v1, v9

    const/16 v9, -0x9

    aput v9, v1, v8

    const/16 v8, -0x45a3

    aput v8, v1, v7

    const/16 v7, -0x46

    aput v7, v1, v6

    const/16 v6, -0x3d

    aput v6, v1, v5

    const/16 v5, -0xcf5

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mSipIntentFilter:Landroid/content/IntentFilter;

    const/16 v1, 0x2a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, 0x27

    const/16 v43, 0x28

    const/16 v44, 0x29

    const/16 v45, 0x1b70

    aput v45, v2, v44

    const/16 v44, 0x455c

    aput v44, v2, v43

    const/16 v43, 0x1a0a

    aput v43, v2, v42

    const/16 v42, -0x2baa

    aput v42, v2, v41

    const/16 v41, -0x6b

    aput v41, v2, v40

    const/16 v40, 0x340e

    aput v40, v2, v39

    const/16 v39, 0xc70

    aput v39, v2, v38

    const/16 v38, -0x29ad

    aput v38, v2, v37

    const/16 v37, -0x65

    aput v37, v2, v36

    const/16 v36, -0x2b

    aput v36, v2, v35

    const/16 v35, -0x4a

    aput v35, v2, v34

    const/16 v34, -0x7a8d

    aput v34, v2, v33

    const/16 v33, -0x24

    aput v33, v2, v32

    const/16 v32, -0x2fbb

    aput v32, v2, v31

    const/16 v31, -0x71

    aput v31, v2, v30

    const/16 v30, -0x52

    aput v30, v2, v29

    const/16 v29, -0x4d

    aput v29, v2, v28

    const/16 v28, -0x1

    aput v28, v2, v27

    const/16 v27, -0x4bcc

    aput v27, v2, v26

    const/16 v26, -0x9

    aput v26, v2, v25

    const/16 v25, 0x792a

    aput v25, v2, v24

    const/16 v24, 0x1f17

    aput v24, v2, v23

    const/16 v23, -0x3d90

    aput v23, v2, v22

    const/16 v22, -0x55

    aput v22, v2, v21

    const/16 v21, -0x28

    aput v21, v2, v20

    const/16 v20, -0x13b4

    aput v20, v2, v19

    const/16 v19, -0x73

    aput v19, v2, v18

    const/16 v18, -0x2e

    aput v18, v2, v17

    const/16 v17, -0x2a

    aput v17, v2, v16

    const/16 v16, -0x70

    aput v16, v2, v15

    const/16 v15, -0x4d

    aput v15, v2, v14

    const/16 v14, -0x6c8

    aput v14, v2, v13

    const/16 v13, -0x69

    aput v13, v2, v12

    const/16 v12, 0x4824

    aput v12, v2, v11

    const/16 v11, -0x6b9a

    aput v11, v2, v10

    const/16 v10, -0x10

    aput v10, v2, v9

    const/16 v9, -0x62

    aput v9, v2, v8

    const/16 v8, -0x73

    aput v8, v2, v7

    const/16 v7, 0x480a

    aput v7, v2, v6

    const/16 v6, -0x3fd4

    aput v6, v2, v5

    const/16 v5, -0x52

    aput v5, v2, v3

    const/16 v3, -0x78

    aput v3, v2, v1

    const/16 v1, 0x2a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, 0x26

    const/16 v43, 0x27

    const/16 v44, 0x28

    const/16 v45, 0x29

    const/16 v46, 0x1b23

    aput v46, v1, v45

    const/16 v45, 0x451b

    aput v45, v1, v44

    const/16 v44, 0x1a45

    aput v44, v1, v43

    const/16 v43, -0x2be6

    aput v43, v1, v42

    const/16 v42, -0x2c

    aput v42, v1, v41

    const/16 v41, 0x3447

    aput v41, v1, v40

    const/16 v40, 0xc34

    aput v40, v1, v39

    const/16 v39, -0x29f4

    aput v39, v1, v38

    const/16 v38, -0x2a

    aput v38, v1, v37

    const/16 v37, -0x70

    aput v37, v1, v36

    const/16 v36, -0x1e

    aput v36, v1, v35

    const/16 v35, -0x7ae0

    aput v35, v1, v34

    const/16 v34, -0x7b

    aput v34, v1, v33

    const/16 v33, -0x2fea

    aput v33, v1, v32

    const/16 v32, -0x30

    aput v32, v1, v31

    const/16 v31, -0x15

    aput v31, v1, v30

    const/16 v30, -0x20

    aput v30, v1, v29

    const/16 v29, -0x50

    aput v29, v1, v28

    const/16 v28, -0x4b88

    aput v28, v1, v27

    const/16 v27, -0x4c

    aput v27, v1, v26

    const/16 v26, 0x7904

    aput v26, v1, v25

    const/16 v25, 0x1f79

    aput v25, v1, v24

    const/16 v24, -0x3de1

    aput v24, v1, v23

    const/16 v23, -0x3e

    aput v23, v1, v22

    const/16 v22, -0x54

    aput v22, v1, v21

    const/16 v21, -0x13d1

    aput v21, v1, v20

    const/16 v20, -0x14

    aput v20, v1, v19

    const/16 v19, -0x4

    aput v19, v1, v18

    const/16 v18, -0x5e

    aput v18, v1, v17

    const/16 v17, -0x2

    aput v17, v1, v16

    const/16 v16, -0x2a

    aput v16, v1, v15

    const/16 v15, -0x6b4

    aput v15, v1, v14

    const/4 v14, -0x7

    aput v14, v1, v13

    const/16 v13, 0x484d

    aput v13, v1, v12

    const/16 v12, -0x6bb8

    aput v12, v1, v11

    const/16 v11, -0x6c

    aput v11, v1, v10

    const/16 v10, -0x9

    aput v10, v1, v9

    const/16 v9, -0x1e

    aput v9, v1, v8

    const/16 v8, 0x4878

    aput v8, v1, v7

    const/16 v7, -0x3fb8

    aput v7, v1, v6

    const/16 v6, -0x40

    aput v6, v1, v5

    const/16 v5, -0x17

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mSipAndRecentKeyReceiver:Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$SipAndRecentKeyReceiver;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mSipIntentFilter:Landroid/content/IntentFilter;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto/16 :goto_0

    :cond_0
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_1
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4
.end method

.method private showHomeScreen()V
    .locals 25

    const/4 v1, 0x1

    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->setRequestPassword(Z)V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const/16 v1, 0x14

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x7818

    aput v23, v2, v22

    const/16 v22, -0x7cf4

    aput v22, v2, v21

    const/16 v21, -0x16

    aput v21, v2, v20

    const/16 v20, -0x7b

    aput v20, v2, v19

    const/16 v19, -0xbf0

    aput v19, v2, v18

    const/16 v18, -0x80

    aput v18, v2, v17

    const/16 v17, -0x2d

    aput v17, v2, v16

    const/16 v16, 0x6f5c

    aput v16, v2, v15

    const/16 v15, 0x3601

    aput v15, v2, v14

    const/16 v14, -0x27ad

    aput v14, v2, v13

    const/16 v13, -0x43

    aput v13, v2, v12

    const/16 v12, 0x2468

    aput v12, v2, v11

    const/16 v11, 0x4a47

    aput v11, v2, v10

    const/16 v10, 0x5c19

    aput v10, v2, v9

    const/16 v9, -0xccc

    aput v9, v2, v8

    const/16 v8, -0x80

    aput v8, v2, v7

    const/16 v7, 0x4a44

    aput v7, v2, v6

    const/16 v6, -0x5eda

    aput v6, v2, v5

    const/16 v5, -0x2f

    aput v5, v2, v3

    const/16 v3, -0x48ea

    aput v3, v2, v1

    const/16 v1, 0x14

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x7861

    aput v24, v1, v23

    const/16 v23, -0x7c88

    aput v23, v1, v22

    const/16 v22, -0x7d

    aput v22, v1, v21

    const/16 v21, -0xd

    aput v21, v1, v20

    const/16 v20, -0xb87

    aput v20, v1, v19

    const/16 v19, -0xc

    aput v19, v1, v18

    const/16 v18, -0x50

    aput v18, v1, v17

    const/16 v17, 0x6f1d

    aput v17, v1, v16

    const/16 v16, 0x366f

    aput v16, v1, v15

    const/16 v15, -0x27ca

    aput v15, v1, v14

    const/16 v14, -0x28

    aput v14, v1, v13

    const/16 v13, 0x241a

    aput v13, v1, v12

    const/16 v12, 0x4a24

    aput v12, v1, v11

    const/16 v11, 0x5c4a

    aput v11, v1, v10

    const/16 v10, -0xca4

    aput v10, v1, v9

    const/16 v9, -0xd

    aput v9, v1, v8

    const/16 v8, 0x4a25

    aput v8, v1, v7

    const/16 v7, -0x5eb6

    aput v7, v1, v6

    const/16 v6, -0x5f

    aput v6, v1, v5

    const/16 v5, -0x48bb

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->setResult(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->finish()V

    :goto_2
    return-void

    :cond_0
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->moveTaskToBack(Z)Z

    goto :goto_2
.end method

.method private showSamsungAccountPopup()V
    .locals 18

    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->forgot_password:I

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/keyManager/KeyManager;->isKeyAlloc()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->forgot_password_message_d:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    :goto_0
    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->reset_password:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->retry:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const/16 v0, 0xd

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, -0x6cf

    aput v16, v1, v15

    const/16 v15, -0x74

    aput v15, v1, v14

    const/16 v14, -0x5b8c

    aput v14, v1, v13

    const/16 v13, -0x35

    aput v13, v1, v12

    const/16 v12, -0x6d

    aput v12, v1, v11

    const/16 v11, -0x7f

    aput v11, v1, v10

    const/16 v10, 0x434a

    aput v10, v1, v9

    const/16 v9, 0x492a

    aput v9, v1, v8

    const/16 v8, 0x2816

    aput v8, v1, v7

    const/16 v7, 0x1246

    aput v7, v1, v6

    const/16 v6, 0x7775

    aput v6, v1, v5

    const/16 v5, 0x541e

    aput v5, v1, v2

    const/16 v2, 0x4a27

    aput v2, v1, v0

    const/16 v0, 0xd

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, -0x6bf

    aput v17, v0, v16

    const/16 v16, -0x7

    aput v16, v0, v15

    const/16 v15, -0x5bfc

    aput v15, v0, v14

    const/16 v14, -0x5c

    aput v14, v0, v13

    const/16 v13, -0x1d

    aput v13, v0, v12

    const/16 v12, -0x22

    aput v12, v0, v11

    const/16 v11, 0x4324

    aput v11, v0, v10

    const/16 v10, 0x4943

    aput v10, v0, v9

    const/16 v9, 0x2849

    aput v9, v0, v8

    const/16 v8, 0x1228

    aput v8, v0, v7

    const/16 v7, 0x7712

    aput v7, v0, v6

    const/16 v6, 0x5477

    aput v6, v0, v5

    const/16 v5, 0x4a54

    aput v5, v0, v2

    const/4 v2, 0x0

    :goto_1
    array-length v5, v0

    if-lt v2, v5, :cond_1

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_2
    array-length v5, v0

    if-lt v2, v5, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->incorrect_password_5_times:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    goto/16 :goto_0

    :cond_1
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    aget v5, v1, v2

    int-to-char v5, v5

    aput-char v5, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method private toggleKeyBoard()V
    .locals 4

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$2;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public static u6iG()Ljava/lang/String;
    .locals 66

    const/16 v0, 0x3f

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, 0x17

    const/16 v25, 0x18

    const/16 v26, 0x19

    const/16 v27, 0x1a

    const/16 v28, 0x1b

    const/16 v29, 0x1c

    const/16 v30, 0x1d

    const/16 v31, 0x1e

    const/16 v32, 0x1f

    const/16 v33, 0x20

    const/16 v34, 0x21

    const/16 v35, 0x22

    const/16 v36, 0x23

    const/16 v37, 0x24

    const/16 v38, 0x25

    const/16 v39, 0x26

    const/16 v40, 0x27

    const/16 v41, 0x28

    const/16 v42, 0x29

    const/16 v43, 0x2a

    const/16 v44, 0x2b

    const/16 v45, 0x2c

    const/16 v46, 0x2d

    const/16 v47, 0x2e

    const/16 v48, 0x2f

    const/16 v49, 0x30

    const/16 v50, 0x31

    const/16 v51, 0x32

    const/16 v52, 0x33

    const/16 v53, 0x34

    const/16 v54, 0x35

    const/16 v55, 0x36

    const/16 v56, 0x37

    const/16 v57, 0x38

    const/16 v58, 0x39

    const/16 v59, 0x3a

    const/16 v60, 0x3b

    const/16 v61, 0x3c

    const/16 v62, 0x3d

    const/16 v63, 0x3e

    const/16 v64, -0x40

    aput v64, v1, v63

    const/16 v63, 0x411b

    aput v63, v1, v62

    const/16 v62, -0x7798

    aput v62, v1, v61

    const/16 v61, -0xf

    aput v61, v1, v60

    const/16 v60, 0x5057

    aput v60, v1, v59

    const/16 v59, 0x6039

    aput v59, v1, v58

    const/16 v58, -0xeea

    aput v58, v1, v57

    const/16 v57, -0x68

    aput v57, v1, v56

    const/16 v56, -0x66a7

    aput v56, v1, v55

    const/16 v55, -0x6

    aput v55, v1, v54

    const/16 v54, 0x7b4c

    aput v54, v1, v53

    const/16 v53, -0x6ce1

    aput v53, v1, v52

    const/16 v52, -0x1f

    aput v52, v1, v51

    const/16 v51, -0x49

    aput v51, v1, v50

    const/16 v50, -0xeb1

    aput v50, v1, v49

    const/16 v49, -0x7e

    aput v49, v1, v48

    const/16 v48, -0x5497

    aput v48, v1, v47

    const/16 v47, -0x36

    aput v47, v1, v46

    const/16 v46, -0x1

    aput v46, v1, v45

    const/16 v45, -0x16

    aput v45, v1, v44

    const/16 v44, 0x480f

    aput v44, v1, v43

    const/16 v43, 0x431b

    aput v43, v1, v42

    const/16 v42, 0x1e30

    aput v42, v1, v41

    const/16 v41, 0x1477

    aput v41, v1, v40

    const/16 v40, 0x4634

    aput v40, v1, v39

    const/16 v39, -0x58a0

    aput v39, v1, v38

    const/16 v38, -0x7f

    aput v38, v1, v37

    const/16 v37, -0x1c

    aput v37, v1, v36

    const/16 v36, -0x5c

    aput v36, v1, v35

    const/16 v35, 0x7778

    aput v35, v1, v34

    const/16 v34, -0xbed

    aput v34, v1, v33

    const/16 v33, -0x7a

    aput v33, v1, v32

    const/16 v32, -0x6dfc

    aput v32, v1, v31

    const/16 v31, -0x1b

    aput v31, v1, v30

    const/16 v30, 0x440d

    aput v30, v1, v29

    const/16 v29, -0x6dc9

    aput v29, v1, v28

    const/16 v28, -0xd

    aput v28, v1, v27

    const/16 v27, -0x7

    aput v27, v1, v26

    const/16 v26, -0x66

    aput v26, v1, v25

    const/16 v25, -0x40

    aput v25, v1, v24

    const/16 v24, 0x1540

    aput v24, v1, v23

    const/16 v23, -0x5699

    aput v23, v1, v22

    const/16 v22, -0x3a

    aput v22, v1, v21

    const/16 v21, -0x3d

    aput v21, v1, v20

    const/16 v20, -0x47

    aput v20, v1, v19

    const/16 v19, -0x1e

    aput v19, v1, v18

    const/16 v18, -0x5b

    aput v18, v1, v17

    const/16 v17, -0x61

    aput v17, v1, v16

    const/16 v16, -0x44ad

    aput v16, v1, v15

    const/16 v15, -0x2e

    aput v15, v1, v14

    const/16 v14, 0x6b67

    aput v14, v1, v13

    const/16 v13, -0x18f2

    aput v13, v1, v12

    const/16 v12, -0x6c

    aput v12, v1, v11

    const/16 v11, -0x55

    aput v11, v1, v10

    const/16 v10, -0xade

    aput v10, v1, v9

    const/16 v9, -0x2b

    aput v9, v1, v8

    const/16 v8, -0x19

    aput v8, v1, v7

    const/16 v7, 0x760f

    aput v7, v1, v6

    const/16 v6, 0x7056

    aput v6, v1, v5

    const/16 v5, 0x515

    aput v5, v1, v4

    const/16 v4, 0x1368

    aput v4, v1, v3

    const/16 v3, 0x437c

    aput v3, v1, v2

    const/16 v2, 0x7920

    aput v2, v1, v0

    const/16 v0, 0x3f

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, 0x1e

    const/16 v33, 0x1f

    const/16 v34, 0x20

    const/16 v35, 0x21

    const/16 v36, 0x22

    const/16 v37, 0x23

    const/16 v38, 0x24

    const/16 v39, 0x25

    const/16 v40, 0x26

    const/16 v41, 0x27

    const/16 v42, 0x28

    const/16 v43, 0x29

    const/16 v44, 0x2a

    const/16 v45, 0x2b

    const/16 v46, 0x2c

    const/16 v47, 0x2d

    const/16 v48, 0x2e

    const/16 v49, 0x2f

    const/16 v50, 0x30

    const/16 v51, 0x31

    const/16 v52, 0x32

    const/16 v53, 0x33

    const/16 v54, 0x34

    const/16 v55, 0x35

    const/16 v56, 0x36

    const/16 v57, 0x37

    const/16 v58, 0x38

    const/16 v59, 0x39

    const/16 v60, 0x3a

    const/16 v61, 0x3b

    const/16 v62, 0x3c

    const/16 v63, 0x3d

    const/16 v64, 0x3e

    const/16 v65, -0x45

    aput v65, v0, v64

    const/16 v64, 0x413b

    aput v64, v0, v63

    const/16 v63, -0x77bf

    aput v63, v0, v62

    const/16 v62, -0x78

    aput v62, v0, v61

    const/16 v61, 0x5023

    aput v61, v0, v60

    const/16 v60, 0x6050

    aput v60, v0, v59

    const/16 v59, -0xea0

    aput v59, v0, v58

    const/16 v58, -0xf

    aput v58, v0, v57

    const/16 v57, -0x66d3

    aput v57, v0, v56

    const/16 v56, -0x67

    aput v56, v0, v55

    const/16 v55, 0x7b0d

    aput v55, v0, v54

    const/16 v54, -0x6c85

    aput v54, v0, v53

    const/16 v53, -0x6d

    aput v53, v0, v52

    const/16 v52, -0x28

    aput v52, v0, v51

    const/16 v51, -0xec8

    aput v51, v0, v50

    const/16 v50, -0xf

    aput v50, v0, v49

    const/16 v49, -0x54e6

    aput v49, v0, v48

    const/16 v48, -0x55

    aput v48, v0, v47

    const/16 v47, -0x51

    aput v47, v0, v46

    const/16 v46, -0x62

    aput v46, v0, v45

    const/16 v45, 0x486a

    aput v45, v0, v44

    const/16 v44, 0x4348

    aput v44, v0, v43

    const/16 v43, 0x1e43

    aput v43, v0, v42

    const/16 v42, 0x141e

    aput v42, v0, v41

    const/16 v41, 0x4614

    aput v41, v0, v40

    const/16 v40, -0x58ba

    aput v40, v0, v39

    const/16 v39, -0x59

    aput v39, v0, v38

    const/16 v38, -0x3c

    aput v38, v0, v37

    const/16 v37, -0x73

    aput v37, v0, v36

    const/16 v36, 0x7750

    aput v36, v0, v35

    const/16 v35, -0xb89

    aput v35, v0, v34

    const/16 v34, -0xc

    aput v34, v0, v33

    const/16 v33, -0x6d95

    aput v33, v0, v32

    const/16 v32, -0x6e

    aput v32, v0, v31

    const/16 v31, 0x447e

    aput v31, v0, v30

    const/16 v30, -0x6dbc

    aput v30, v0, v29

    const/16 v29, -0x6e

    aput v29, v0, v28

    const/16 v28, -0x57

    aput v28, v0, v27

    const/16 v27, -0x7

    aput v27, v0, v26

    const/16 v26, -0x5b

    aput v26, v0, v25

    const/16 v25, 0x1532

    aput v25, v0, v24

    const/16 v24, -0x56eb

    aput v24, v0, v23

    const/16 v23, -0x57

    aput v23, v0, v22

    const/16 v22, -0x80

    aput v22, v0, v21

    const/16 v21, -0x36

    aput v21, v0, v20

    const/16 v20, -0x75

    aput v20, v0, v19

    const/16 v19, -0x73

    aput v19, v0, v18

    const/16 v18, -0x41

    aput v18, v0, v17

    const/16 v17, -0x44cb

    aput v17, v0, v16

    const/16 v16, -0x45

    aput v16, v0, v15

    const/16 v15, 0x6b47

    aput v15, v0, v14

    const/16 v14, -0x1895

    aput v14, v0, v13

    const/16 v13, -0x19

    aput v13, v0, v12

    const/16 v12, -0x39

    aput v12, v0, v11

    const/16 v11, -0xab9

    aput v11, v0, v10

    const/16 v10, -0xb

    aput v10, v0, v9

    const/16 v9, -0x78

    aput v9, v0, v8

    const/16 v8, 0x767b

    aput v8, v0, v7

    const/16 v7, 0x7076

    aput v7, v0, v6

    const/16 v6, 0x570

    aput v6, v0, v5

    const/16 v5, 0x1305

    aput v5, v0, v4

    const/16 v4, 0x4313

    aput v4, v0, v3

    const/16 v3, 0x7943

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private unregisterSipAndRecentKeyReceiver()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mSipAndRecentKeyReceiver:Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$SipAndRecentKeyReceiver;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mSipAndRecentKeyReceiver:Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$SipAndRecentKeyReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mSipAndRecentKeyReceiver:Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$SipAndRecentKeyReceiver;

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static xLCy676O()Ljava/lang/String;
    .locals 56

    const/16 v0, 0x35

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, 0x17

    const/16 v25, 0x18

    const/16 v26, 0x19

    const/16 v27, 0x1a

    const/16 v28, 0x1b

    const/16 v29, 0x1c

    const/16 v30, 0x1d

    const/16 v31, 0x1e

    const/16 v32, 0x1f

    const/16 v33, 0x20

    const/16 v34, 0x21

    const/16 v35, 0x22

    const/16 v36, 0x23

    const/16 v37, 0x24

    const/16 v38, 0x25

    const/16 v39, 0x26

    const/16 v40, 0x27

    const/16 v41, 0x28

    const/16 v42, 0x29

    const/16 v43, 0x2a

    const/16 v44, 0x2b

    const/16 v45, 0x2c

    const/16 v46, 0x2d

    const/16 v47, 0x2e

    const/16 v48, 0x2f

    const/16 v49, 0x30

    const/16 v50, 0x31

    const/16 v51, 0x32

    const/16 v52, 0x33

    const/16 v53, 0x34

    const/16 v54, -0x4e

    aput v54, v1, v53

    const/16 v53, -0x1d

    aput v53, v1, v52

    const/16 v52, -0x23

    aput v52, v1, v51

    const/16 v51, -0x7

    aput v51, v1, v50

    const/16 v50, -0x4490

    aput v50, v1, v49

    const/16 v49, -0x2e

    aput v49, v1, v48

    const/16 v48, -0x5dc3

    aput v48, v1, v47

    const/16 v47, -0x3f

    aput v47, v1, v46

    const/16 v46, -0x26

    aput v46, v1, v45

    const/16 v45, -0x34

    aput v45, v1, v44

    const/16 v44, 0x4c63

    aput v44, v1, v43

    const/16 v43, -0x30dd

    aput v43, v1, v42

    const/16 v42, -0x48

    aput v42, v1, v41

    const/16 v41, -0x3d

    aput v41, v1, v40

    const/16 v40, -0x33f7

    aput v40, v1, v39

    const/16 v39, -0x53

    aput v39, v1, v38

    const/16 v38, -0x46

    aput v38, v1, v37

    const/16 v37, 0x582c

    aput v37, v1, v36

    const/16 v36, -0x39c3

    aput v36, v1, v35

    const/16 v35, -0x6b

    aput v35, v1, v34

    const/16 v34, -0x6f

    aput v34, v1, v33

    const/16 v33, -0x7cd7

    aput v33, v1, v32

    const/16 v32, -0x5e

    aput v32, v1, v31

    const/16 v31, -0x709b

    aput v31, v1, v30

    const/16 v30, -0x57

    aput v30, v1, v29

    const/16 v29, 0x271

    aput v29, v1, v28

    const/16 v28, 0x4122

    aput v28, v1, v27

    const/16 v27, 0x6b68

    aput v27, v1, v26

    const/16 v26, 0x2443

    aput v26, v1, v25

    const/16 v25, -0x2fb0

    aput v25, v1, v24

    const/16 v24, -0x4d

    aput v24, v1, v23

    const/16 v23, -0x35be

    aput v23, v1, v22

    const/16 v22, -0x48

    aput v22, v1, v21

    const/16 v21, 0x3d18

    aput v21, v1, v20

    const/16 v20, -0x7bae

    aput v20, v1, v19

    const/16 v19, -0x39

    aput v19, v1, v18

    const/16 v18, 0x195c

    aput v18, v1, v17

    const/16 v17, 0x870

    aput v17, v1, v16

    const/16 v16, 0x1858

    aput v16, v1, v15

    const/16 v15, 0x566b    # 3.1001E-41f

    aput v15, v1, v14

    const/16 v14, 0xb3f

    aput v14, v1, v13

    const/16 v13, 0x6623

    aput v13, v1, v12

    const/16 v12, 0x546

    aput v12, v1, v11

    const/16 v11, -0x4c9d

    aput v11, v1, v10

    const/16 v10, -0x26

    aput v10, v1, v9

    const/16 v9, -0x6d

    aput v9, v1, v8

    const/16 v8, -0x5bc2

    aput v8, v1, v7

    const/16 v7, -0x30

    aput v7, v1, v6

    const/16 v6, 0x773d

    aput v6, v1, v5

    const/16 v5, -0x6aee

    aput v5, v1, v4

    const/4 v4, -0x8

    aput v4, v1, v3

    const/16 v3, -0x23

    aput v3, v1, v2

    const/16 v2, -0xb

    aput v2, v1, v0

    const/16 v0, 0x35

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, 0x1e

    const/16 v33, 0x1f

    const/16 v34, 0x20

    const/16 v35, 0x21

    const/16 v36, 0x22

    const/16 v37, 0x23

    const/16 v38, 0x24

    const/16 v39, 0x25

    const/16 v40, 0x26

    const/16 v41, 0x27

    const/16 v42, 0x28

    const/16 v43, 0x29

    const/16 v44, 0x2a

    const/16 v45, 0x2b

    const/16 v46, 0x2c

    const/16 v47, 0x2d

    const/16 v48, 0x2e

    const/16 v49, 0x2f

    const/16 v50, 0x30

    const/16 v51, 0x31

    const/16 v52, 0x32

    const/16 v53, 0x33

    const/16 v54, 0x34

    const/16 v55, -0x65

    aput v55, v0, v54

    const/16 v54, -0x66

    aput v54, v0, v53

    const/16 v53, -0x57

    aput v53, v0, v52

    const/16 v52, -0x70

    aput v52, v0, v51

    const/16 v51, -0x44fa

    aput v51, v0, v50

    const/16 v50, -0x45

    aput v50, v0, v49

    const/16 v49, -0x5db7

    aput v49, v0, v48

    const/16 v48, -0x5e

    aput v48, v0, v47

    const/16 v47, -0x65

    aput v47, v0, v46

    const/16 v46, -0x58

    aput v46, v0, v45

    const/16 v45, 0x4c11

    aput v45, v0, v44

    const/16 v44, -0x30b4

    aput v44, v0, v43

    const/16 v43, -0x31

    aput v43, v0, v42

    const/16 v42, -0x50

    aput v42, v0, v41

    const/16 v41, -0x3386

    aput v41, v0, v40

    const/16 v40, -0x34

    aput v40, v0, v39

    const/16 v39, -0x16

    aput v39, v0, v38

    const/16 v38, 0x5858

    aput v38, v0, v37

    const/16 v37, -0x39a8

    aput v37, v0, v36

    const/16 v36, -0x3a

    aput v36, v0, v35

    const/16 v35, -0x1e

    aput v35, v0, v34

    const/16 v34, -0x7cc0

    aput v34, v0, v33

    const/16 v33, -0x7d

    aput v33, v0, v32

    const/16 v32, -0x70bb

    aput v32, v0, v31

    const/16 v31, -0x71

    aput v31, v0, v30

    const/16 v30, 0x257

    aput v30, v0, v29

    const/16 v29, 0x4102

    aput v29, v0, v28

    const/16 v28, 0x6b41

    aput v28, v0, v27

    const/16 v27, 0x246b

    aput v27, v0, v26

    const/16 v26, -0x2fdc

    aput v26, v0, v25

    const/16 v25, -0x30

    aput v25, v0, v24

    const/16 v24, -0x35d9

    aput v24, v0, v23

    const/16 v23, -0x36

    aput v23, v0, v22

    const/16 v22, 0x3d6a

    aput v22, v0, v21

    const/16 v21, -0x7bc3

    aput v21, v0, v20

    const/16 v20, -0x7c

    aput v20, v0, v19

    const/16 v19, 0x1932

    aput v19, v0, v18

    const/16 v18, 0x819

    aput v18, v0, v17

    const/16 v17, 0x1808

    aput v17, v0, v16

    const/16 v16, 0x5618

    aput v16, v0, v15

    const/16 v15, 0xb56

    aput v15, v0, v14

    const/16 v14, 0x660b

    aput v14, v0, v13

    const/16 v13, 0x566

    aput v13, v0, v12

    const/16 v12, -0x4cfb

    aput v12, v0, v11

    const/16 v11, -0x4d

    aput v11, v0, v10

    const/16 v10, -0x4d

    aput v10, v0, v9

    const/16 v9, -0x5baf

    aput v9, v0, v8

    const/16 v8, -0x5c

    aput v8, v0, v7

    const/16 v7, 0x771d

    aput v7, v0, v6

    const/16 v6, -0x6a89

    aput v6, v0, v5

    const/16 v5, -0x6b

    aput v5, v0, v4

    const/16 v4, -0x4e

    aput v4, v0, v3

    const/16 v3, -0x6a

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method


# virtual methods
.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mDialogControllerMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    return-object v0
.end method

.method public hideHeightKeyboard()V
    .locals 15

    const/16 v0, 0xc

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, -0xb88

    aput v13, v1, v12

    const/16 v12, -0x65

    aput v12, v1, v11

    const/16 v11, 0x5152

    aput v11, v1, v10

    const/16 v10, -0x28db

    aput v10, v1, v9

    const/16 v9, -0x4e

    aput v9, v1, v8

    const/16 v8, -0x6d

    aput v8, v1, v7

    const/16 v7, -0x13

    aput v7, v1, v6

    const/16 v6, -0x44

    aput v6, v1, v5

    const/16 v5, -0x13

    aput v5, v1, v4

    const/16 v4, -0x3f

    aput v4, v1, v3

    const/16 v3, 0x5403

    aput v3, v1, v2

    const/16 v2, 0x613d

    aput v2, v1, v0

    const/16 v0, 0xc

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, -0xbe4

    aput v14, v0, v13

    const/16 v13, -0xc

    aput v13, v0, v12

    const/16 v12, 0x513a

    aput v12, v0, v11

    const/16 v11, -0x28af

    aput v11, v0, v10

    const/16 v10, -0x29

    aput v10, v0, v9

    const/4 v9, -0x2

    aput v9, v0, v8

    const/16 v8, -0x4e

    aput v8, v0, v7

    const/16 v7, -0x38

    aput v7, v0, v6

    const/16 v6, -0x68

    aput v6, v0, v5

    const/16 v5, -0x4f

    aput v5, v0, v4

    const/16 v4, 0x546d

    aput v4, v0, v3

    const/16 v3, 0x6154

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mEditTextPinCode:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mEditTextPinCode:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    return-void

    :cond_0
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 37

    const/4 v1, 0x2

    new-array v3, v1, [J

    const/4 v1, 0x1

    const-wide/16 v4, 0x2

    aput-wide v4, v3, v1

    const/4 v1, 0x0

    array-length v2, v3

    add-int/lit8 v2, v2, -0x1

    aget-wide v4, v3, v2

    long-to-int v2, v4

    if-gtz v2, :cond_0

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v4, 0x0

    move/from16 v0, p1

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    ushr-long v5, v1, v5

    aget-wide v1, v3, v4

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_1

    const-wide v7, 0x5fa9563a146630c8L    # 6.63498566539745E152

    xor-long/2addr v1, v7

    :cond_1
    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    const/16 v7, 0x20

    shl-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, 0x5fa9563a146630c8L    # 6.63498566539745E152

    xor-long/2addr v1, v5

    aput-wide v1, v3, v4

    const/4 v1, 0x1

    array-length v2, v3

    add-int/lit8 v2, v2, -0x1

    aget-wide v4, v3, v2

    long-to-int v2, v4

    if-lt v1, v2, :cond_2

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    const/4 v4, 0x0

    move/from16 v0, p2

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long v5, v1, v5

    aget-wide v1, v3, v4

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_3

    const-wide v7, 0x5fa9563a146630c8L    # 6.63498566539745E152

    xor-long/2addr v1, v7

    :cond_3
    const/16 v7, 0x20

    shl-long/2addr v1, v7

    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, 0x5fa9563a146630c8L    # 6.63498566539745E152

    xor-long/2addr v1, v5

    aput-wide v1, v3, v4

    array-length v1, v3

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v3, v1

    long-to-int v1, v1

    if-gtz v1, :cond_6

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x76

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x46

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_4
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_5
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_6
    const/4 v1, 0x0

    aget-wide v1, v3, v1

    const-wide/16 v4, 0x0

    cmp-long v4, v1, v4

    if-eqz v4, :cond_7

    const-wide v4, 0x5fa9563a146630c8L    # 6.63498566539745E152

    xor-long/2addr v1, v4

    :cond_7
    const/16 v4, 0x20

    shl-long/2addr v1, v4

    const/16 v4, 0x20

    shr-long/2addr v1, v4

    long-to-int v1, v1

    const/16 v2, 0x1c6d

    if-ne v1, v2, :cond_c

    const/4 v4, -0x1

    array-length v1, v3

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v3, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_a

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x2b94

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x2ba3

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_8

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_8
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_9
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_a
    const/4 v1, 0x0

    aget-wide v1, v3, v1

    const-wide/16 v5, 0x0

    cmp-long v3, v1, v5

    if-eqz v3, :cond_b

    const-wide v5, 0x5fa9563a146630c8L    # 6.63498566539745E152

    xor-long/2addr v1, v5

    :cond_b
    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    if-ne v4, v1, :cond_c

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->finish()V

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->setResetStatus(Z)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    const/16 v1, 0x10

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, -0x6e

    aput v19, v2, v18

    const/16 v18, 0x7c02

    aput v18, v2, v17

    const/16 v17, -0x64ed

    aput v17, v2, v16

    const/16 v16, -0x14

    aput v16, v2, v15

    const/16 v15, -0x19

    aput v15, v2, v14

    const/16 v14, -0x78

    aput v14, v2, v13

    const/16 v13, -0x48

    aput v13, v2, v12

    const/16 v12, -0x4b

    aput v12, v2, v11

    const/16 v11, 0x1a3b

    aput v11, v2, v10

    const/16 v10, -0x1192

    aput v10, v2, v9

    const/16 v9, -0x63

    aput v9, v2, v8

    const/16 v8, -0x5e

    aput v8, v2, v7

    const/16 v7, -0x39

    aput v7, v2, v6

    const/16 v6, -0x63

    aput v6, v2, v5

    const/16 v5, 0x166

    aput v5, v2, v3

    const/16 v3, -0x158d

    aput v3, v2, v1

    const/16 v1, 0x10

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, -0xa

    aput v20, v1, v19

    const/16 v19, 0x7c70

    aput v19, v1, v18

    const/16 v18, -0x6484

    aput v18, v1, v17

    const/16 v17, -0x65

    aput v17, v1, v16

    const/16 v16, -0x6c

    aput v16, v1, v15

    const/4 v15, -0x5

    aput v15, v1, v14

    const/16 v14, -0x27

    aput v14, v1, v13

    const/16 v13, -0x3b

    aput v13, v1, v12

    const/16 v12, 0x1a64

    aput v12, v1, v11

    const/16 v11, -0x11e6

    aput v11, v1, v10

    const/16 v10, -0x12

    aput v10, v1, v9

    const/16 v9, -0x39

    aput v9, v1, v8

    const/16 v8, -0x4e

    aput v8, v1, v7

    const/16 v7, -0x14

    aput v7, v1, v6

    const/16 v6, 0x103

    aput v6, v1, v5

    const/16 v5, -0x15ff

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_d

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v4, v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->saveBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    const/16 v1, 0x14

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x670c

    aput v23, v2, v22

    const/16 v22, 0x7e02

    aput v22, v2, v21

    const/16 v21, -0x35ee

    aput v21, v2, v20

    const/16 v20, -0x58

    aput v20, v2, v19

    const/16 v19, 0x116d

    aput v19, v2, v18

    const/16 v18, -0x1781

    aput v18, v2, v17

    const/16 v17, -0x73

    aput v17, v2, v16

    const/16 v16, -0x4ec6

    aput v16, v2, v15

    const/16 v15, -0x21

    aput v15, v2, v14

    const/16 v14, -0x1f

    aput v14, v2, v13

    const/16 v13, -0x3d

    aput v13, v2, v12

    const/16 v12, 0xe31

    aput v12, v2, v11

    const/16 v11, -0x3889

    aput v11, v2, v10

    const/16 v10, -0x4d

    aput v10, v2, v9

    const/16 v9, -0x4a7

    aput v9, v2, v8

    const/16 v8, -0x77

    aput v8, v2, v7

    const/16 v7, -0x7efc

    aput v7, v2, v6

    const/16 v6, -0x1e

    aput v6, v2, v5

    const/16 v5, 0x1460

    aput v5, v2, v3

    const/16 v3, 0x1967

    aput v3, v2, v1

    const/16 v1, 0x14

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x6768

    aput v24, v1, v23

    const/16 v23, 0x7e67

    aput v23, v1, v22

    const/16 v22, -0x3582

    aput v22, v1, v21

    const/16 v21, -0x36

    aput v21, v1, v20

    const/16 v20, 0x110c

    aput v20, v1, v19

    const/16 v19, -0x17ef

    aput v19, v1, v18

    const/16 v18, -0x18

    aput v18, v1, v17

    const/16 v17, -0x4e9b

    aput v17, v1, v16

    const/16 v16, -0x4f

    aput v16, v1, v15

    const/16 v15, -0x78

    aput v15, v1, v14

    const/16 v14, -0x4d

    aput v14, v1, v13

    const/16 v13, 0xe6e

    aput v13, v1, v12

    const/16 v12, -0x38f2

    aput v12, v1, v11

    const/16 v11, -0x39

    aput v11, v1, v10

    const/16 v10, -0x4d0

    aput v10, v1, v9

    const/4 v9, -0x5

    aput v9, v1, v8

    const/16 v8, -0x7e8f

    aput v8, v1, v7

    const/16 v7, -0x7f

    aput v7, v1, v6

    const/16 v6, 0x1405

    aput v6, v1, v5

    const/16 v5, 0x1914

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v5, v1

    if-lt v3, v5, :cond_f

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_10

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v4, v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->saveBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;Z)V

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    const/16 v1, 0x1b

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, -0x21

    aput v30, v2, v29

    const/16 v29, -0x23

    aput v29, v2, v28

    const/16 v28, -0x1f99

    aput v28, v2, v27

    const/16 v27, -0x7f

    aput v27, v2, v26

    const/16 v26, -0x30

    aput v26, v2, v25

    const/16 v25, 0x1f58

    aput v25, v2, v24

    const/16 v24, -0x3794

    aput v24, v2, v23

    const/16 v23, -0x1a

    aput v23, v2, v22

    const/16 v22, -0xcd8

    aput v22, v2, v21

    const/16 v21, -0x7d

    aput v21, v2, v20

    const/16 v20, 0x1c4a

    aput v20, v2, v19

    const/16 v19, 0xf32

    aput v19, v2, v18

    const/16 v18, 0x7c6b

    aput v18, v2, v17

    const/16 v17, 0x7115

    aput v17, v2, v16

    const/16 v16, -0x30e2

    aput v16, v2, v15

    const/16 v15, -0x43

    aput v15, v2, v14

    const/16 v14, -0x2b8e

    aput v14, v2, v13

    const/16 v13, -0x46

    aput v13, v2, v12

    const/16 v12, -0x2e92

    aput v12, v2, v11

    const/4 v11, -0x1

    aput v11, v2, v10

    const/16 v10, -0x7f

    aput v10, v2, v9

    const/16 v9, -0x19a6

    aput v9, v2, v8

    const/16 v8, -0x6b

    aput v8, v2, v7

    const/16 v7, 0x3952

    aput v7, v2, v6

    const/16 v6, 0x2a54

    aput v6, v2, v5

    const/16 v5, 0x2b45

    aput v5, v2, v3

    const/16 v3, -0x12b8

    aput v3, v2, v1

    const/16 v1, 0x1b

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, -0x49

    aput v31, v1, v30

    const/16 v30, -0x57

    aput v30, v1, v29

    const/16 v29, -0x1ff5

    aput v29, v1, v28

    const/16 v28, -0x20

    aput v28, v1, v27

    const/16 v27, -0x4b

    aput v27, v1, v26

    const/16 v26, 0x1f30

    aput v26, v1, v25

    const/16 v25, -0x37e1

    aput v25, v1, v24

    const/16 v24, -0x38

    aput v24, v1, v23

    const/16 v23, -0xca8

    aput v23, v1, v22

    const/16 v22, -0xd

    aput v22, v1, v21

    const/16 v21, 0x1c2b

    aput v21, v1, v20

    const/16 v20, 0xf1c

    aput v20, v1, v19

    const/16 v19, 0x7c0f

    aput v19, v1, v18

    const/16 v18, 0x717c

    aput v18, v1, v17

    const/16 v17, -0x308f

    aput v17, v1, v16

    const/16 v16, -0x31

    aput v16, v1, v15

    const/16 v15, -0x2bea

    aput v15, v1, v14

    const/16 v14, -0x2c

    aput v14, v1, v13

    const/16 v13, -0x2ef1

    aput v13, v1, v12

    const/16 v12, -0x2f

    aput v12, v1, v11

    const/16 v11, -0x1e

    aput v11, v1, v10

    const/16 v10, -0x19c1

    aput v10, v1, v9

    const/16 v9, -0x1a

    aput v9, v1, v8

    const/16 v8, 0x397c

    aput v8, v1, v7

    const/16 v7, 0x2a39

    aput v7, v1, v6

    const/16 v6, 0x2b2a

    aput v6, v1, v5

    const/16 v5, -0x12d5

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_8
    array-length v5, v1

    if-lt v3, v5, :cond_11

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_9
    array-length v5, v1

    if-lt v3, v5, :cond_12

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const/16 v1, 0x20

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, -0x1a

    aput v35, v2, v34

    const/16 v34, -0x6f

    aput v34, v2, v33

    const/16 v33, -0x14

    aput v33, v2, v32

    const/16 v32, -0x63

    aput v32, v2, v31

    const/16 v31, 0x2c5d

    aput v31, v2, v30

    const/16 v30, 0x6178

    aput v30, v2, v29

    const/16 v29, 0x1622

    aput v29, v2, v28

    const/16 v28, -0x3ca9

    aput v28, v2, v27

    const/16 v27, -0x64

    aput v27, v2, v26

    const/16 v26, -0x3f

    aput v26, v2, v25

    const/16 v25, 0x662e

    aput v25, v2, v24

    const/16 v24, 0x7029

    aput v24, v2, v23

    const/16 v23, -0x7dcd

    aput v23, v2, v22

    const/16 v22, -0x34

    aput v22, v2, v21

    const/16 v21, -0x2

    aput v21, v2, v20

    const/16 v20, -0x6aac

    aput v20, v2, v19

    const/16 v19, -0x45

    aput v19, v2, v18

    const/16 v18, 0x2541

    aput v18, v2, v17

    const/16 v17, -0x1baf

    aput v17, v2, v16

    const/16 v16, -0x78

    aput v16, v2, v15

    const/16 v15, -0x5aeb

    aput v15, v2, v14

    const/16 v14, -0x40

    aput v14, v2, v13

    const/16 v13, -0x2b

    aput v13, v2, v12

    const/16 v12, -0x5f

    aput v12, v2, v11

    const/16 v11, 0x3a29

    aput v11, v2, v10

    const/16 v10, -0x27a7

    aput v10, v2, v9

    const/16 v9, -0x43

    aput v9, v2, v8

    const/16 v8, -0x27cf

    aput v8, v2, v7

    const/16 v7, -0xa

    aput v7, v2, v6

    const/16 v6, -0x6d

    aput v6, v2, v5

    const/4 v5, -0x1

    aput v5, v2, v3

    const/16 v3, -0x75

    aput v3, v2, v1

    const/16 v1, 0x20

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, -0x41

    aput v36, v1, v35

    const/16 v35, -0x3b

    aput v35, v1, v34

    const/16 v34, -0x5b

    aput v34, v1, v33

    const/16 v33, -0x35

    aput v33, v1, v32

    const/16 v32, 0x2c14

    aput v32, v1, v31

    const/16 v31, 0x612c

    aput v31, v1, v30

    const/16 v30, 0x1661

    aput v30, v1, v29

    const/16 v29, -0x3cea

    aput v29, v1, v28

    const/16 v28, -0x3d

    aput v28, v1, v27

    const/16 v27, -0x7c

    aput v27, v1, v26

    const/16 v26, 0x666a

    aput v26, v1, v25

    const/16 v25, 0x7066

    aput v25, v1, v24

    const/16 v24, -0x7d90

    aput v24, v1, v23

    const/16 v23, -0x7e

    aput v23, v1, v22

    const/16 v22, -0x49

    aput v22, v1, v21

    const/16 v21, -0x6afc

    aput v21, v1, v20

    const/16 v20, -0x6b

    aput v20, v1, v19

    const/16 v19, 0x2529

    aput v19, v1, v18

    const/16 v18, -0x1bdb

    aput v18, v1, v17

    const/16 v17, -0x1c

    aput v17, v1, v16

    const/16 v16, -0x5a8c

    aput v16, v1, v15

    const/16 v15, -0x5b

    aput v15, v1, v14

    const/16 v14, -0x43

    aput v14, v1, v13

    const/16 v13, -0x2e

    aput v13, v1, v12

    const/16 v12, 0x3a07

    aput v12, v1, v11

    const/16 v11, -0x27c6

    aput v11, v1, v10

    const/16 v10, -0x28

    aput v10, v1, v9

    const/16 v9, -0x27be

    aput v9, v1, v8

    const/16 v8, -0x28

    aput v8, v1, v7

    const/4 v7, -0x2

    aput v7, v1, v6

    const/16 v6, -0x70

    aput v6, v1, v5

    const/16 v5, -0x18

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_a
    array-length v5, v1

    if-lt v3, v5, :cond_13

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_b
    array-length v5, v1

    if-lt v3, v5, :cond_14

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->startActivity(Landroid/content/Intent;)V

    :cond_c
    return-void

    :cond_d
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_e
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_f
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_10
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :cond_11
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_8

    :cond_12
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_9

    :cond_13
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    :cond_14
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_b
.end method

.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mEditTextPinCode:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->clear()V

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->showHomeScreen()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 57

    const/16 v2, 0xc

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/4 v15, -0x6

    aput v15, v3, v14

    const/16 v14, -0x42

    aput v14, v3, v13

    const/16 v13, 0x6d6d

    aput v13, v3, v12

    const/16 v12, 0x5b0e

    aput v12, v3, v11

    const/16 v11, -0x12cb

    aput v11, v3, v10

    const/16 v10, -0x7c

    aput v10, v3, v9

    const/16 v9, -0x13

    aput v9, v3, v8

    const/16 v8, 0x3f2c

    aput v8, v3, v7

    const/16 v7, 0x1b4a

    aput v7, v3, v6

    const/16 v6, -0x595

    aput v6, v3, v5

    const/16 v5, -0x6c

    aput v5, v3, v4

    const/16 v4, 0x245b

    aput v4, v3, v2

    const/16 v2, 0xc

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, -0x61

    aput v16, v2, v15

    const/16 v15, -0x26

    aput v15, v2, v14

    const/16 v14, 0x6d02

    aput v14, v2, v13

    const/16 v13, 0x5b6d

    aput v13, v2, v12

    const/16 v12, -0x12a5

    aput v12, v2, v11

    const/16 v11, -0x13

    aput v11, v2, v10

    const/16 v10, -0x43

    aput v10, v2, v9

    const/16 v9, 0x3f58

    aput v9, v2, v8

    const/16 v8, 0x1b3f

    aput v8, v2, v7

    const/16 v7, -0x5e5

    aput v7, v2, v6

    const/4 v6, -0x6

    aput v6, v2, v5

    const/16 v5, 0x2412

    aput v5, v2, v4

    const/4 v4, 0x0

    :goto_0
    array-length v5, v2

    if-lt v4, v5, :cond_2

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_1
    array-length v5, v2

    if-lt v4, v5, :cond_3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x15

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, -0x608a

    aput v26, v3, v25

    const/16 v25, -0x15

    aput v25, v3, v24

    const/16 v24, -0x16

    aput v24, v3, v23

    const/16 v23, 0x7716

    aput v23, v3, v22

    const/16 v22, -0x4fb

    aput v22, v3, v21

    const/16 v21, -0x48

    aput v21, v3, v20

    const/16 v20, -0xd

    aput v20, v3, v19

    const/16 v19, -0x71

    aput v19, v3, v18

    const/16 v18, -0x5b8d

    aput v18, v3, v17

    const/16 v17, -0x3f

    aput v17, v3, v16

    const/16 v16, -0x25

    aput v16, v3, v15

    const/16 v15, 0x4770

    aput v15, v3, v14

    const/16 v14, 0x7e24

    aput v14, v3, v13

    const/16 v13, 0x2b10

    aput v13, v3, v12

    const/16 v12, 0x4642

    aput v12, v3, v11

    const/16 v11, -0x70ea

    aput v11, v3, v10

    const/4 v10, -0x5

    aput v10, v3, v9

    const/16 v9, -0x10

    aput v9, v3, v8

    const/16 v8, -0x6ad0

    aput v8, v3, v7

    const/4 v7, -0x5

    aput v7, v3, v4

    const/16 v4, 0x4d4d

    aput v4, v3, v2

    const/16 v2, 0x15

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x11

    const/16 v24, 0x12

    const/16 v25, 0x13

    const/16 v26, 0x14

    const/16 v27, -0x60ed

    aput v27, v2, v26

    const/16 v26, -0x61

    aput v26, v2, v25

    const/16 v25, -0x75

    aput v25, v2, v24

    const/16 v24, 0x7773

    aput v24, v2, v23

    const/16 v23, -0x489

    aput v23, v2, v22

    const/16 v22, -0x5

    aput v22, v2, v21

    const/16 v21, -0x63

    aput v21, v2, v20

    const/16 v20, -0x20

    aput v20, v2, v19

    const/16 v19, -0x5bad

    aput v19, v2, v18

    const/16 v18, -0x5c

    aput v18, v2, v17

    const/16 v17, -0x41

    aput v17, v2, v16

    const/16 v16, 0x471f

    aput v16, v2, v15

    const/16 v15, 0x7e47

    aput v15, v2, v14

    const/16 v14, 0x2b7e

    aput v14, v2, v13

    const/16 v13, 0x462b

    aput v13, v2, v12

    const/16 v12, -0x70ba

    aput v12, v2, v11

    const/16 v11, -0x71

    aput v11, v2, v10

    const/16 v10, -0x7b

    aput v10, v2, v9

    const/16 v9, -0x6ac0

    aput v9, v2, v8

    const/16 v8, -0x6b

    aput v8, v2, v7

    const/16 v7, 0x4d04

    aput v7, v2, v4

    const/4 v4, 0x0

    :goto_2
    array-length v7, v2

    if-lt v4, v7, :cond_4

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_3
    array-length v7, v2

    if-lt v4, v7, :cond_5

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super/range {p0 .. p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const/16 v2, 0x13

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, -0x6db5

    aput v23, v3, v22

    const/16 v22, -0x1a

    aput v22, v3, v21

    const/16 v21, -0x67

    aput v21, v3, v20

    const/16 v20, -0x50

    aput v20, v3, v19

    const/16 v19, -0x76d3

    aput v19, v3, v18

    const/16 v18, -0x3

    aput v18, v3, v17

    const/16 v17, -0x42

    aput v17, v3, v16

    const/16 v16, -0x65

    aput v16, v3, v15

    const/16 v15, 0x325b

    aput v15, v3, v14

    const/16 v14, 0x4740

    aput v14, v3, v13

    const/16 v13, -0x68d8

    aput v13, v3, v12

    const/16 v12, -0x20

    aput v12, v3, v11

    const/16 v11, 0x3d1f

    aput v11, v3, v10

    const/16 v10, 0x24e

    aput v10, v3, v9

    const/16 v9, 0x5363

    aput v9, v3, v8

    const/16 v8, 0x5903

    aput v8, v3, v7

    const/16 v7, -0x14d3

    aput v7, v3, v6

    const/16 v6, -0x72

    aput v6, v3, v4

    const/16 v4, 0x1752

    aput v4, v3, v2

    const/16 v2, 0x13

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, -0x6dce

    aput v24, v2, v23

    const/16 v23, -0x6e

    aput v23, v2, v22

    const/16 v22, -0x10

    aput v22, v2, v21

    const/16 v21, -0x3a

    aput v21, v2, v20

    const/16 v20, -0x76bc

    aput v20, v2, v19

    const/16 v19, -0x77

    aput v19, v2, v18

    const/16 v18, -0x23

    aput v18, v2, v17

    const/16 v17, -0x26

    aput v17, v2, v16

    const/16 v16, 0x323f

    aput v16, v2, v15

    const/16 v15, 0x4732

    aput v15, v2, v14

    const/16 v14, -0x68b9

    aput v14, v2, v13

    const/16 v13, -0x69

    aput v13, v2, v12

    const/16 v12, 0x3d6c

    aput v12, v2, v11

    const/16 v11, 0x23d

    aput v11, v2, v10

    const/16 v10, 0x5302

    aput v10, v2, v9

    const/16 v9, 0x5953

    aput v9, v2, v8

    const/16 v8, -0x14a7

    aput v8, v2, v7

    const/16 v7, -0x15

    aput v7, v2, v6

    const/16 v6, 0x1701

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_4
    array-length v6, v2

    if-lt v4, v6, :cond_6

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_5
    array-length v6, v2

    if-lt v4, v6, :cond_7

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v5, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->isSetPasswordActivity:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_6
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->isSetPasswordActivity:Z

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/service/health/keyManager/KeyManager;->setInputPincodeFlag(Z)V

    :cond_0
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const/16 v2, 0x10

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, -0x3bbe

    aput v20, v3, v19

    const/16 v19, -0x50

    aput v19, v3, v18

    const/16 v18, -0x139d

    aput v18, v3, v17

    const/16 v17, -0x66

    aput v17, v3, v16

    const/16 v16, -0x52

    aput v16, v3, v15

    const/16 v15, -0x4c

    aput v15, v3, v14

    const/16 v14, -0x3d93

    aput v14, v3, v13

    const/16 v13, -0x5d

    aput v13, v3, v12

    const/16 v12, 0x7d63

    aput v12, v3, v11

    const/16 v11, -0x70e8

    aput v11, v3, v10

    const/16 v10, -0x1e

    aput v10, v3, v9

    const/16 v9, 0x414e

    aput v9, v3, v8

    const/16 v8, 0x6d29

    aput v8, v3, v7

    const/16 v7, -0x1ce

    aput v7, v3, v6

    const/16 v6, -0x6f

    aput v6, v3, v4

    const/16 v4, -0x11

    aput v4, v3, v2

    const/16 v2, 0x10

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, -0x3bc5

    aput v21, v2, v20

    const/16 v20, -0x3c

    aput v20, v2, v19

    const/16 v19, -0x13f6

    aput v19, v2, v18

    const/16 v18, -0x14

    aput v18, v2, v17

    const/16 v17, -0x39

    aput v17, v2, v16

    const/16 v16, -0x40

    aput v16, v2, v15

    const/16 v15, -0x3df2

    aput v15, v2, v14

    const/16 v14, -0x3e

    aput v14, v2, v13

    const/16 v13, 0x7d3c

    aput v13, v2, v12

    const/16 v12, -0x7083

    aput v12, v2, v11

    const/16 v11, -0x71

    aput v11, v2, v10

    const/16 v10, 0x4121

    aput v10, v2, v9

    const/16 v9, 0x6d41

    aput v9, v2, v8

    const/16 v8, -0x193

    aput v8, v2, v7

    const/4 v7, -0x2

    aput v7, v2, v6

    const/16 v6, -0x65

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_7
    array-length v6, v2

    if-lt v4, v6, :cond_8

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_8
    array-length v6, v2

    if-lt v4, v6, :cond_9

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v5, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->isToHomeActivity:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_9
    const/16 v2, 0x8

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, -0x75

    aput v11, v3, v10

    const/16 v10, -0x19f6

    aput v10, v3, v9

    const/16 v9, -0x76

    aput v9, v3, v8

    const/16 v8, -0x2c

    aput v8, v3, v7

    const/16 v7, -0x37

    aput v7, v3, v6

    const/16 v6, -0x3f

    aput v6, v3, v5

    const/16 v5, -0x6b

    aput v5, v3, v4

    const/16 v4, 0x6106

    aput v4, v3, v2

    const/16 v2, 0x8

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, -0x1d

    aput v12, v2, v11

    const/16 v11, -0x1982

    aput v11, v2, v10

    const/16 v10, -0x1a

    aput v10, v2, v9

    const/16 v9, -0x4b

    aput v9, v2, v8

    const/16 v8, -0x54

    aput v8, v2, v7

    const/16 v7, -0x77

    aput v7, v2, v6

    const/16 v6, -0x4b

    aput v6, v2, v5

    const/16 v5, 0x6155

    aput v5, v2, v4

    const/4 v4, 0x0

    :goto_a
    array-length v5, v2

    if-lt v4, v5, :cond_a

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_b
    array-length v5, v2

    if-lt v4, v5, :cond_b

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x32

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, 0x1a

    const/16 v32, 0x1b

    const/16 v33, 0x1c

    const/16 v34, 0x1d

    const/16 v35, 0x1e

    const/16 v36, 0x1f

    const/16 v37, 0x20

    const/16 v38, 0x21

    const/16 v39, 0x22

    const/16 v40, 0x23

    const/16 v41, 0x24

    const/16 v42, 0x25

    const/16 v43, 0x26

    const/16 v44, 0x27

    const/16 v45, 0x28

    const/16 v46, 0x29

    const/16 v47, 0x2a

    const/16 v48, 0x2b

    const/16 v49, 0x2c

    const/16 v50, 0x2d

    const/16 v51, 0x2e

    const/16 v52, 0x2f

    const/16 v53, 0x30

    const/16 v54, 0x31

    const/16 v55, 0x2675

    aput v55, v3, v54

    const/16 v54, -0x7ce5

    aput v54, v3, v53

    const/16 v53, -0x5d

    aput v53, v3, v52

    const/16 v52, -0x28

    aput v52, v3, v51

    const/16 v51, 0x4777

    aput v51, v3, v50

    const/16 v50, -0x4ed2

    aput v50, v3, v49

    const/16 v49, -0x39

    aput v49, v3, v48

    const/16 v48, 0x1423

    aput v48, v3, v47

    const/16 v47, 0x3c60

    aput v47, v3, v46

    const/16 v46, -0x13a1

    aput v46, v3, v45

    const/16 v45, -0x53

    aput v45, v3, v44

    const/16 v44, -0x63

    aput v44, v3, v43

    const/16 v43, -0x73d5

    aput v43, v3, v42

    const/16 v42, -0x1d

    aput v42, v3, v41

    const/16 v41, -0x72

    aput v41, v3, v40

    const/16 v40, -0x38

    aput v40, v3, v39

    const/16 v39, -0x74

    aput v39, v3, v38

    const/16 v38, -0x1184

    aput v38, v3, v37

    const/16 v37, -0x42

    aput v37, v3, v36

    const/16 v36, -0x29

    aput v36, v3, v35

    const/16 v35, -0xff5

    aput v35, v3, v34

    const/16 v34, -0x5d

    aput v34, v3, v33

    const/16 v33, -0xcc4

    aput v33, v3, v32

    const/16 v32, -0x37

    aput v32, v3, v31

    const/16 v31, -0x7f

    aput v31, v3, v30

    const/16 v30, -0x4cef

    aput v30, v3, v29

    const/16 v29, -0x2a

    aput v29, v3, v28

    const/16 v28, 0x4933

    aput v28, v3, v27

    const/16 v27, -0x74d8

    aput v27, v3, v26

    const/16 v26, -0x1b

    aput v26, v3, v25

    const/16 v25, -0x57f1

    aput v25, v3, v24

    const/16 v24, -0x1b

    aput v24, v3, v23

    const/16 v23, -0x23

    aput v23, v3, v22

    const/16 v22, 0x1327

    aput v22, v3, v21

    const/16 v21, 0x4f58

    aput v21, v3, v20

    const/16 v20, -0x1391

    aput v20, v3, v19

    const/16 v19, -0x3f

    aput v19, v3, v18

    const/16 v18, -0x1b

    aput v18, v3, v17

    const/16 v17, -0x35

    aput v17, v3, v16

    const/16 v16, 0xa67

    aput v16, v3, v15

    const/16 v15, -0x7c9b

    aput v15, v3, v14

    const/16 v14, -0x20

    aput v14, v3, v13

    const/16 v13, 0x487a    # 2.6E-41f

    aput v13, v3, v12

    const/16 v12, 0x1721

    aput v12, v3, v11

    const/16 v11, 0x5647

    aput v11, v3, v10

    const/16 v10, -0x27de

    aput v10, v3, v9

    const/16 v9, -0x53

    aput v9, v3, v8

    const/16 v8, 0x537

    aput v8, v3, v7

    const/16 v7, -0x6695

    aput v7, v3, v4

    const/16 v4, -0x30

    aput v4, v3, v2

    const/16 v2, 0x32

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x11

    const/16 v24, 0x12

    const/16 v25, 0x13

    const/16 v26, 0x14

    const/16 v27, 0x15

    const/16 v28, 0x16

    const/16 v29, 0x17

    const/16 v30, 0x18

    const/16 v31, 0x19

    const/16 v32, 0x1a

    const/16 v33, 0x1b

    const/16 v34, 0x1c

    const/16 v35, 0x1d

    const/16 v36, 0x1e

    const/16 v37, 0x1f

    const/16 v38, 0x20

    const/16 v39, 0x21

    const/16 v40, 0x22

    const/16 v41, 0x23

    const/16 v42, 0x24

    const/16 v43, 0x25

    const/16 v44, 0x26

    const/16 v45, 0x27

    const/16 v46, 0x28

    const/16 v47, 0x29

    const/16 v48, 0x2a

    const/16 v49, 0x2b

    const/16 v50, 0x2c

    const/16 v51, 0x2d

    const/16 v52, 0x2e

    const/16 v53, 0x2f

    const/16 v54, 0x30

    const/16 v55, 0x31

    const/16 v56, 0x2655

    aput v56, v2, v55

    const/16 v55, -0x7cda

    aput v55, v2, v54

    const/16 v54, -0x7d

    aput v54, v2, v53

    const/16 v53, -0x5f

    aput v53, v2, v52

    const/16 v52, 0x4703

    aput v52, v2, v51

    const/16 v51, -0x4eb9

    aput v51, v2, v50

    const/16 v50, -0x4f

    aput v50, v2, v49

    const/16 v49, 0x144a

    aput v49, v2, v48

    const/16 v48, 0x3c14

    aput v48, v2, v47

    const/16 v47, -0x13c4

    aput v47, v2, v46

    const/16 v46, -0x14

    aput v46, v2, v45

    const/16 v45, -0x7

    aput v45, v2, v44

    const/16 v44, -0x73a7

    aput v44, v2, v43

    const/16 v43, -0x74

    aput v43, v2, v42

    const/16 v42, -0x7

    aput v42, v2, v41

    const/16 v41, -0x45

    aput v41, v2, v40

    const/16 v40, -0x1

    aput v40, v2, v39

    const/16 v39, -0x11e3

    aput v39, v2, v38

    const/16 v38, -0x12

    aput v38, v2, v37

    const/16 v37, -0x5d

    aput v37, v2, v36

    const/16 v36, -0xf92

    aput v36, v2, v35

    const/16 v35, -0x10

    aput v35, v2, v34

    const/16 v34, -0xce4

    aput v34, v2, v33

    const/16 v33, -0xd

    aput v33, v2, v32

    const/16 v32, -0x5f

    aput v32, v2, v31

    const/16 v31, -0x4c9d

    aput v31, v2, v30

    const/16 v30, -0x4d

    aput v30, v2, v29

    const/16 v29, 0x4954

    aput v29, v2, v28

    const/16 v28, -0x74b7

    aput v28, v2, v27

    const/16 v27, -0x75

    aput v27, v2, v26

    const/16 v26, -0x5792

    aput v26, v2, v25

    const/16 v25, -0x58

    aput v25, v2, v24

    const/16 v24, -0x5c

    aput v24, v2, v23

    const/16 v23, 0x1342

    aput v23, v2, v22

    const/16 v22, 0x4f13

    aput v22, v2, v21

    const/16 v21, -0x13b1

    aput v21, v2, v20

    const/16 v20, -0x14

    aput v20, v2, v19

    const/16 v19, -0x3b

    aput v19, v2, v18

    const/16 v18, -0x52

    aput v18, v2, v17

    const/16 v17, 0xa03

    aput v17, v2, v16

    const/16 v16, -0x7cf6

    aput v16, v2, v15

    const/16 v15, -0x7d

    aput v15, v2, v14

    const/16 v14, 0x4814

    aput v14, v2, v13

    const/16 v13, 0x1748

    aput v13, v2, v12

    const/16 v12, 0x5617

    aput v12, v2, v11

    const/16 v11, -0x27aa

    aput v11, v2, v10

    const/16 v10, -0x28

    aput v10, v2, v9

    const/16 v9, 0x547

    aput v9, v2, v8

    const/16 v8, -0x66fb

    aput v8, v2, v7

    const/16 v7, -0x67

    aput v7, v2, v4

    const/4 v4, 0x0

    :goto_c
    array-length v7, v2

    if-lt v4, v7, :cond_c

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_d
    array-length v7, v2

    if-lt v4, v7, :cond_d

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->isSetPasswordActivity:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_2
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mOutStateBundle:Landroid/os/Bundle;

    :goto_e
    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$layout;->lock_screen_activity:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->setContentView(I)V

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->parentLayout:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mParentLayout:Landroid/view/View;

    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->screenHeight:I

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->bg_image:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->bgImage:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->bgImage:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->screenHeight:I

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->bgImage:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->pin_code_edit_text:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mEditTextPinCode:Landroid/widget/EditText;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->text_below_pin_code:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mStatusMessageText:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mStatusMessageText:Landroid/widget/TextView;

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->enter_password:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mEditTextPinCode:Landroid/widget/EditText;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->getPincode()Ljava/lang/String;

    move-result-object v5

    const/16 v2, 0x8

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x6001

    aput v12, v3, v11

    const/16 v11, 0x4514

    aput v11, v3, v10

    const/16 v10, 0x6b29

    aput v10, v3, v9

    const/16 v9, -0x3cf6

    aput v9, v3, v8

    const/16 v8, -0x5a

    aput v8, v3, v7

    const/4 v7, -0x2

    aput v7, v3, v6

    const/16 v6, 0x3179

    aput v6, v3, v4

    const/16 v4, -0x559e

    aput v4, v3, v2

    const/16 v2, 0x8

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x6069

    aput v13, v2, v12

    const/16 v12, 0x4560

    aput v12, v2, v11

    const/16 v11, 0x6b45

    aput v11, v2, v10

    const/16 v10, -0x3c95

    aput v10, v2, v9

    const/16 v9, -0x3d

    aput v9, v2, v8

    const/16 v8, -0x4a

    aput v8, v2, v7

    const/16 v7, 0x3159

    aput v7, v2, v6

    const/16 v6, -0x55cf

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_f
    array-length v6, v2

    if-lt v4, v6, :cond_e

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_10
    array-length v6, v2

    if-lt v4, v6, :cond_f

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x28

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x11

    const/16 v24, 0x12

    const/16 v25, 0x13

    const/16 v26, 0x14

    const/16 v27, 0x15

    const/16 v28, 0x16

    const/16 v29, 0x17

    const/16 v30, 0x18

    const/16 v31, 0x19

    const/16 v32, 0x1a

    const/16 v33, 0x1b

    const/16 v34, 0x1c

    const/16 v35, 0x1d

    const/16 v36, 0x1e

    const/16 v37, 0x1f

    const/16 v38, 0x20

    const/16 v39, 0x21

    const/16 v40, 0x22

    const/16 v41, 0x23

    const/16 v42, 0x24

    const/16 v43, 0x25

    const/16 v44, 0x26

    const/16 v45, 0x27

    const/16 v46, -0x7ca4

    aput v46, v3, v45

    const/16 v45, -0x47

    aput v45, v3, v44

    const/16 v44, -0x10

    aput v44, v3, v43

    const/16 v43, -0x8

    aput v43, v3, v42

    const/16 v42, -0x1d

    aput v42, v3, v41

    const/16 v41, -0x2d

    aput v41, v3, v40

    const/16 v40, -0x37

    aput v40, v3, v39

    const/16 v39, -0x4db2

    aput v39, v3, v38

    const/16 v38, -0x25

    aput v38, v3, v37

    const/16 v37, 0x2d6d

    aput v37, v3, v36

    const/16 v36, -0x4aa7

    aput v36, v3, v35

    const/16 v35, -0x30

    aput v35, v3, v34

    const/16 v34, -0x6498

    aput v34, v3, v33

    const/16 v33, -0x4b

    aput v33, v3, v32

    const/16 v32, -0x32

    aput v32, v3, v31

    const/16 v31, -0x2cbf

    aput v31, v3, v30

    const/16 v30, -0x44

    aput v30, v3, v29

    const/16 v29, 0x1476

    aput v29, v3, v28

    const/16 v28, -0x5a86

    aput v28, v3, v27

    const/16 v27, -0x34

    aput v27, v3, v26

    const/16 v26, 0x6d3c

    aput v26, v3, v25

    const/16 v25, -0xbe7

    aput v25, v3, v24

    const/16 v24, -0x7f

    aput v24, v3, v23

    const/16 v23, -0x68

    aput v23, v3, v22

    const/16 v22, 0x4067

    aput v22, v3, v21

    const/16 v21, -0x2cf7

    aput v21, v3, v20

    const/16 v20, -0x3

    aput v20, v3, v19

    const/16 v19, -0x3997

    aput v19, v3, v18

    const/16 v18, -0x4

    aput v18, v3, v17

    const/16 v17, 0x3204

    aput v17, v3, v16

    const/16 v16, 0x2540

    aput v16, v3, v15

    const/16 v15, -0x29c0

    aput v15, v3, v14

    const/16 v14, -0x4f

    aput v14, v3, v13

    const/16 v13, -0x7ab5

    aput v13, v3, v12

    const/16 v12, -0x15

    aput v12, v3, v11

    const/16 v11, -0x45

    aput v11, v3, v10

    const/16 v10, -0x5a

    aput v10, v3, v9

    const/16 v9, -0x27

    aput v9, v3, v8

    const/16 v8, 0x155

    aput v8, v3, v4

    const/16 v4, -0x6ab6

    aput v4, v3, v2

    const/16 v2, 0x28

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v11, 0x4

    const/4 v12, 0x5

    const/4 v13, 0x6

    const/4 v14, 0x7

    const/16 v15, 0x8

    const/16 v16, 0x9

    const/16 v17, 0xa

    const/16 v18, 0xb

    const/16 v19, 0xc

    const/16 v20, 0xd

    const/16 v21, 0xe

    const/16 v22, 0xf

    const/16 v23, 0x10

    const/16 v24, 0x11

    const/16 v25, 0x12

    const/16 v26, 0x13

    const/16 v27, 0x14

    const/16 v28, 0x15

    const/16 v29, 0x16

    const/16 v30, 0x17

    const/16 v31, 0x18

    const/16 v32, 0x19

    const/16 v33, 0x1a

    const/16 v34, 0x1b

    const/16 v35, 0x1c

    const/16 v36, 0x1d

    const/16 v37, 0x1e

    const/16 v38, 0x1f

    const/16 v39, 0x20

    const/16 v40, 0x21

    const/16 v41, 0x22

    const/16 v42, 0x23

    const/16 v43, 0x24

    const/16 v44, 0x25

    const/16 v45, 0x26

    const/16 v46, 0x27

    const/16 v47, -0x7c84

    aput v47, v2, v46

    const/16 v46, -0x7d

    aput v46, v2, v45

    const/16 v45, -0x30

    aput v45, v2, v44

    const/16 v44, -0x63

    aput v44, v2, v43

    const/16 v43, -0x79

    aput v43, v2, v42

    const/16 v42, -0x44

    aput v42, v2, v41

    const/16 v41, -0x56

    aput v41, v2, v40

    const/16 v40, -0x4de0

    aput v40, v2, v39

    const/16 v39, -0x4e

    aput v39, v2, v38

    const/16 v38, 0x2d3d

    aput v38, v2, v37

    const/16 v37, -0x4ad3

    aput v37, v2, v36

    const/16 v36, -0x4b

    aput v36, v2, v35

    const/16 v35, -0x64f1

    aput v35, v2, v34

    const/16 v34, -0x65

    aput v34, v2, v33

    const/16 v33, -0x55

    aput v33, v2, v32

    const/16 v32, -0x2cdb

    aput v32, v2, v31

    const/16 v31, -0x2d

    aput v31, v2, v30

    const/16 v30, 0x1415

    aput v30, v2, v29

    const/16 v29, -0x5aec

    aput v29, v2, v28

    const/16 v28, -0x5b

    aput v28, v2, v27

    const/16 v27, 0x6d6c

    aput v27, v2, v26

    const/16 v26, -0xb93

    aput v26, v2, v25

    const/16 v25, -0xc

    aput v25, v2, v24

    const/16 v24, -0x18

    aput v24, v2, v23

    const/16 v23, 0x4009

    aput v23, v2, v22

    const/16 v22, -0x2cc0

    aput v22, v2, v21

    const/16 v21, -0x2d

    aput v21, v2, v20

    const/16 v20, -0x39b7

    aput v20, v2, v19

    const/16 v19, -0x3a

    aput v19, v2, v18

    const/16 v18, 0x3224

    aput v18, v2, v17

    const/16 v17, 0x2532

    aput v17, v2, v16

    const/16 v16, -0x29db

    aput v16, v2, v15

    const/16 v15, -0x2a

    aput v15, v2, v14

    const/16 v14, -0x7ad6

    aput v14, v2, v13

    const/16 v13, -0x7b

    aput v13, v2, v12

    const/16 v12, -0x26

    aput v12, v2, v11

    const/16 v11, -0x15

    aput v11, v2, v10

    const/16 v10, -0x60

    aput v10, v2, v9

    const/16 v9, 0x130

    aput v9, v2, v8

    const/16 v8, -0x6aff

    aput v8, v2, v4

    const/4 v4, 0x0

    :goto_11
    array-length v8, v2

    if-lt v4, v8, :cond_10

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_12
    array-length v8, v2

    if-lt v4, v8, :cond_11

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v6, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_3
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_3

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->isEnteredWithCorrectPasscode:Z

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const/16 v2, 0x14

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, -0x41bf

    aput v24, v3, v23

    const/16 v23, -0x36

    aput v23, v3, v22

    const/16 v22, -0x4c

    aput v22, v3, v21

    const/16 v21, -0x57f3

    aput v21, v3, v20

    const/16 v20, -0x3f

    aput v20, v3, v19

    const/16 v19, -0x2d

    aput v19, v3, v18

    const/16 v18, 0x7473

    aput v18, v3, v17

    const/16 v17, -0x3bcb

    aput v17, v3, v16

    const/16 v16, -0x56

    aput v16, v3, v15

    const/16 v15, -0x25

    aput v15, v3, v14

    const/16 v14, -0x28

    aput v14, v3, v13

    const/16 v13, 0x552

    aput v13, v3, v12

    const/16 v12, -0x5a9a

    aput v12, v3, v11

    const/16 v11, -0xa

    aput v11, v3, v10

    const/16 v10, -0x10ff

    aput v10, v3, v9

    const/16 v9, -0x64

    aput v9, v3, v8

    const/16 v8, 0x1328

    aput v8, v3, v7

    const/16 v7, 0x5c7f

    aput v7, v3, v6

    const/16 v6, -0x2cd4

    aput v6, v3, v4

    const/16 v4, -0x80

    aput v4, v3, v2

    const/16 v2, 0x14

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, -0x41c8

    aput v25, v2, v24

    const/16 v24, -0x42

    aput v24, v2, v23

    const/16 v23, -0x23

    aput v23, v2, v22

    const/16 v22, -0x5785

    aput v22, v2, v21

    const/16 v21, -0x58

    aput v21, v2, v20

    const/16 v20, -0x59

    aput v20, v2, v19

    const/16 v19, 0x7410

    aput v19, v2, v18

    const/16 v18, -0x3b8c

    aput v18, v2, v17

    const/16 v17, -0x3c

    aput v17, v2, v16

    const/16 v16, -0x42

    aput v16, v2, v15

    const/16 v15, -0x43

    aput v15, v2, v14

    const/16 v14, 0x520

    aput v14, v2, v13

    const/16 v13, -0x5afb

    aput v13, v2, v12

    const/16 v12, -0x5b

    aput v12, v2, v11

    const/16 v11, -0x1097

    aput v11, v2, v10

    const/16 v10, -0x11

    aput v10, v2, v9

    const/16 v9, 0x1349

    aput v9, v2, v8

    const/16 v8, 0x5c13

    aput v8, v2, v7

    const/16 v7, -0x2ca4

    aput v7, v2, v6

    const/16 v6, -0x2d

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_13
    array-length v6, v2

    if-lt v4, v6, :cond_12

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_14
    array-length v6, v2

    if-lt v4, v6, :cond_13

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->setResult(I)V

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->finish()V

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->setRequestPassword(Z)V

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    move-result-object v5

    const/16 v2, 0x10

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x740c

    aput v20, v3, v19

    const/16 v19, 0x3906

    aput v19, v3, v18

    const/16 v18, -0x5faa

    aput v18, v3, v17

    const/16 v17, -0x29

    aput v17, v3, v16

    const/16 v16, -0x58

    aput v16, v3, v15

    const/16 v15, -0x27

    aput v15, v3, v14

    const/16 v14, -0x29

    aput v14, v3, v13

    const/16 v13, -0x1a

    aput v13, v3, v12

    const/4 v12, -0x6

    aput v12, v3, v11

    const/16 v11, -0x5d

    aput v11, v3, v10

    const/16 v10, -0x28fd

    aput v10, v3, v9

    const/16 v9, -0x4e

    aput v9, v3, v8

    const/16 v8, 0x3360

    aput v8, v3, v7

    const/16 v7, 0xd42

    aput v7, v3, v6

    const/16 v6, 0x468

    aput v6, v3, v4

    const/16 v4, 0x4376

    aput v4, v3, v2

    const/16 v2, 0x10

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x7468

    aput v21, v2, v20

    const/16 v20, 0x3974

    aput v20, v2, v19

    const/16 v19, -0x5fc7

    aput v19, v2, v18

    const/16 v18, -0x60

    aput v18, v2, v17

    const/16 v17, -0x25

    aput v17, v2, v16

    const/16 v16, -0x56

    aput v16, v2, v15

    const/16 v15, -0x4a

    aput v15, v2, v14

    const/16 v14, -0x6a

    aput v14, v2, v13

    const/16 v13, -0x5b

    aput v13, v2, v12

    const/16 v12, -0x29

    aput v12, v2, v11

    const/16 v11, -0x2890

    aput v11, v2, v10

    const/16 v10, -0x29

    aput v10, v2, v9

    const/16 v9, 0x3315

    aput v9, v2, v8

    const/16 v8, 0xd33

    aput v8, v2, v7

    const/16 v7, 0x40d

    aput v7, v2, v6

    const/16 v6, 0x4304

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_15
    array-length v6, v2

    if-lt v4, v6, :cond_14

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_16
    array-length v6, v2

    if-lt v4, v6, :cond_15

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v5, v0, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->saveBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;Z)V

    :goto_17
    :try_start_4
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_4

    const/16 v2, 0x8

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x6a12

    aput v11, v3, v10

    const/16 v10, -0x20e8

    aput v10, v3, v9

    const/16 v9, -0x42

    aput v9, v3, v8

    const/16 v8, -0x33

    aput v8, v3, v7

    const/16 v7, 0x7b79

    aput v7, v3, v6

    const/16 v6, -0x2ffe

    aput v6, v3, v5

    const/16 v5, -0x4b

    aput v5, v3, v4

    const/16 v4, -0x5a

    aput v4, v3, v2

    const/16 v2, 0x8

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x6a76

    aput v12, v2, v11

    const/16 v11, -0x2096

    aput v11, v2, v10

    const/16 v10, -0x21

    aput v10, v2, v9

    const/16 v9, -0x5e

    aput v9, v2, v8

    const/16 v8, 0x7b1b

    aput v8, v2, v7

    const/16 v7, -0x2f85

    aput v7, v2, v6

    const/16 v6, -0x30

    aput v6, v2, v5

    const/16 v5, -0x33

    aput v5, v2, v4

    const/4 v4, 0x0

    :goto_18
    array-length v5, v2

    if-lt v4, v5, :cond_16

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_19
    array-length v5, v2

    if-lt v4, v5, :cond_17

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mKeyBoardStatus:Z

    :goto_1a
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->registerSipAndRecentKeyReceiver()V

    return-void

    :cond_2
    aget v5, v2, v4

    aget v6, v3, v4

    xor-int/2addr v5, v6

    aput v5, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    :cond_3
    aget v5, v3, v4

    int-to-char v5, v5

    aput-char v5, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    :cond_4
    aget v7, v2, v4

    aget v8, v3, v4

    xor-int/2addr v7, v8

    aput v7, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2

    :cond_5
    aget v7, v3, v4

    int-to-char v7, v7

    aput-char v7, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_3

    :cond_6
    :try_start_5
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_4

    :cond_7
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_5

    :catch_0
    move-exception v2

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->isSetPasswordActivity:Z

    goto/16 :goto_6

    :cond_8
    :try_start_6
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_7

    :cond_9
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_8

    :catch_1
    move-exception v2

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->isToHomeActivity:Z

    goto/16 :goto_9

    :cond_a
    aget v5, v2, v4

    aget v6, v3, v4

    xor-int/2addr v5, v6

    aput v5, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_a

    :cond_b
    aget v5, v3, v4

    int-to-char v5, v5

    aput-char v5, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_b

    :cond_c
    aget v7, v2, v4

    aget v8, v3, v4

    xor-int/2addr v7, v8

    aput v7, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_c

    :cond_d
    aget v7, v3, v4

    int-to-char v7, v7

    aput-char v7, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_d

    :cond_e
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_f

    :cond_f
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_10

    :cond_10
    aget v8, v2, v4

    aget v9, v3, v4

    xor-int/2addr v8, v9

    aput v8, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_11

    :cond_11
    aget v8, v3, v4

    int-to-char v8, v8

    aput-char v8, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_12

    :cond_12
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_13

    :cond_13
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_14

    :cond_14
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_15

    :cond_15
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_16

    :cond_16
    aget v5, v2, v4

    aget v6, v3, v4

    xor-int/2addr v5, v6

    aput v5, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_18

    :cond_17
    aget v5, v3, v4

    int-to-char v5, v5

    aput-char v5, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_19

    :catch_2
    move-exception v2

    goto/16 :goto_e

    :catch_3
    move-exception v2

    goto/16 :goto_17

    :catch_4
    move-exception v2

    goto/16 :goto_1a
.end method

.method protected onDestroy()V
    .locals 2

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->unregisterSipAndRecentKeyReceiver()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mHandler:Landroid/os/Handler;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :goto_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onDestroy()V

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 41

    const/4 v1, 0x2

    new-array v3, v1, [J

    const/4 v1, 0x1

    const-wide/16 v4, 0x1

    aput-wide v4, v3, v1

    const/4 v1, 0x0

    array-length v2, v3

    add-int/lit8 v2, v2, -0x1

    aget-wide v4, v3, v2

    long-to-int v2, v4

    if-gtz v2, :cond_0

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v4, 0x0

    move/from16 v0, p2

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    ushr-long v5, v1, v5

    aget-wide v1, v3, v4

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_1

    const-wide v7, 0x1a75c1f077fac470L

    xor-long/2addr v1, v7

    :cond_1
    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    const/16 v7, 0x20

    shl-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, 0x1a75c1f077fac470L

    xor-long/2addr v1, v5

    aput-wide v1, v3, v4

    invoke-virtual/range {p1 .. p1}, Landroid/widget/TextView;->getId()I

    move-result v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->pin_code_edit_text:I

    if-ne v1, v2, :cond_37

    array-length v1, v3

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v3, v1

    long-to-int v1, v1

    if-gtz v1, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x28e6

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x28d6

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    aget-wide v1, v3, v1

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-eqz v3, :cond_5

    const-wide v3, 0x1a75c1f077fac470L

    xor-long/2addr v1, v3

    :cond_5
    const/16 v3, 0x20

    shl-long/2addr v1, v3

    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    const/4 v2, 0x6

    if-eq v1, v2, :cond_6

    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x42

    if-eq v1, v2, :cond_6

    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x17

    if-ne v1, v2, :cond_37

    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->isPinCorrect()Z

    move-result v1

    if-eqz v1, :cond_25

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->isSetPasswordActivity:Z

    if-nez v1, :cond_25

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, -0x6e

    aput v10, v2, v9

    const/16 v9, -0x5d

    aput v9, v2, v8

    const/16 v8, -0x39

    aput v8, v2, v7

    const/16 v7, -0x6aca

    aput v7, v2, v6

    const/16 v6, -0x10

    aput v6, v2, v5

    const/16 v5, -0x39

    aput v5, v2, v4

    const/16 v4, 0x3879

    aput v4, v2, v3

    const/16 v3, -0x4995

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/4 v11, -0x6

    aput v11, v1, v10

    const/16 v10, -0x29

    aput v10, v1, v9

    const/16 v9, -0x55

    aput v9, v1, v8

    const/16 v8, -0x6aa9

    aput v8, v1, v7

    const/16 v7, -0x6b

    aput v7, v1, v6

    const/16 v6, -0x71

    aput v6, v1, v5

    const/16 v5, 0x3859

    aput v5, v1, v4

    const/16 v4, -0x49c8

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v4, v1

    if-lt v3, v4, :cond_9

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v4, v1

    if-lt v3, v4, :cond_a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->xLCy676O()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->isEnteredWithCorrectPasscode:Z

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const/16 v1, 0x14

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, -0x75b6

    aput v23, v2, v22

    const/16 v22, -0x2

    aput v22, v2, v21

    const/16 v21, -0x4b

    aput v21, v2, v20

    const/16 v20, -0x2b9b

    aput v20, v2, v19

    const/16 v19, -0x43

    aput v19, v2, v18

    const/16 v18, -0x28

    aput v18, v2, v17

    const/16 v17, -0x51

    aput v17, v2, v16

    const/16 v16, -0x4fac

    aput v16, v2, v15

    const/16 v15, -0x22

    aput v15, v2, v14

    const/16 v14, -0x61

    aput v14, v2, v13

    const/16 v13, 0x3c7e

    aput v13, v2, v12

    const/16 v12, 0x4e

    aput v12, v2, v11

    const/16 v11, -0x7b9d

    aput v11, v2, v10

    const/16 v10, -0x29

    aput v10, v2, v9

    const/16 v9, -0x2c

    aput v9, v2, v8

    const/16 v8, -0x1e

    aput v8, v2, v7

    const/16 v7, -0x58

    aput v7, v2, v6

    const/16 v6, -0x54

    aput v6, v2, v5

    const/16 v5, 0x7230

    aput v5, v2, v3

    const/16 v3, 0xa21

    aput v3, v2, v1

    const/16 v1, 0x14

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, -0x75cd

    aput v24, v1, v23

    const/16 v23, -0x76

    aput v23, v1, v22

    const/16 v22, -0x24

    aput v22, v1, v21

    const/16 v21, -0x2bed

    aput v21, v1, v20

    const/16 v20, -0x2c

    aput v20, v1, v19

    const/16 v19, -0x54

    aput v19, v1, v18

    const/16 v18, -0x34

    aput v18, v1, v17

    const/16 v17, -0x4feb

    aput v17, v1, v16

    const/16 v16, -0x50

    aput v16, v1, v15

    const/4 v15, -0x6

    aput v15, v1, v14

    const/16 v14, 0x3c1b

    aput v14, v1, v13

    const/16 v13, 0x3c

    aput v13, v1, v12

    const/16 v12, -0x7c00

    aput v12, v1, v11

    const/16 v11, -0x7c

    aput v11, v1, v10

    const/16 v10, -0x44

    aput v10, v1, v9

    const/16 v9, -0x6f

    aput v9, v1, v8

    const/16 v8, -0x37

    aput v8, v1, v7

    const/16 v7, -0x40

    aput v7, v1, v6

    const/16 v6, 0x7240

    aput v6, v1, v5

    const/16 v5, 0xa72

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_b

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v1, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->setResult(I)V

    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->finish()V

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->setRequestPassword(Z)V

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    move-result-object v4

    const/16 v1, 0x10

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, -0x1c

    aput v19, v2, v18

    const/16 v18, -0x1a

    aput v18, v2, v17

    const/16 v17, 0x6634

    aput v17, v2, v16

    const/16 v16, 0x2811

    aput v16, v2, v15

    const/16 v15, 0x765b

    aput v15, v2, v14

    const/16 v14, -0x2ffb

    aput v14, v2, v13

    const/16 v13, -0x4f

    aput v13, v2, v12

    const/16 v12, -0x74

    aput v12, v2, v11

    const/16 v11, -0x35

    aput v11, v2, v10

    const/16 v10, -0x45fd

    aput v10, v2, v9

    const/16 v9, -0x37

    aput v9, v2, v8

    const/16 v8, -0x41

    aput v8, v2, v7

    const/16 v7, -0x1bed

    aput v7, v2, v6

    const/16 v6, -0x6b

    aput v6, v2, v5

    const/16 v5, -0x6591

    aput v5, v2, v3

    const/16 v3, -0x18

    aput v3, v2, v1

    const/16 v1, 0x10

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, -0x80

    aput v20, v1, v19

    const/16 v19, -0x6c

    aput v19, v1, v18

    const/16 v18, 0x665b

    aput v18, v1, v17

    const/16 v17, 0x2866

    aput v17, v1, v16

    const/16 v16, 0x7628

    aput v16, v1, v15

    const/16 v15, -0x2f8a

    aput v15, v1, v14

    const/16 v14, -0x30

    aput v14, v1, v13

    const/4 v13, -0x4

    aput v13, v1, v12

    const/16 v12, -0x6c

    aput v12, v1, v11

    const/16 v11, -0x4589

    aput v11, v1, v10

    const/16 v10, -0x46

    aput v10, v1, v9

    const/16 v9, -0x26

    aput v9, v1, v8

    const/16 v8, -0x1b9a

    aput v8, v1, v7

    const/16 v7, -0x1c

    aput v7, v1, v6

    const/16 v6, -0x65f6

    aput v6, v1, v5

    const/16 v5, -0x66

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v5, v1

    if-lt v3, v5, :cond_d

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v4, v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->saveBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;Z)V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const/16 v1, 0x14

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, -0x49

    aput v23, v2, v22

    const/16 v22, -0x41

    aput v22, v2, v21

    const/16 v21, -0x2ce6

    aput v21, v2, v20

    const/16 v20, -0x59

    aput v20, v2, v19

    const/16 v19, -0x58

    aput v19, v2, v18

    const/16 v18, -0x32

    aput v18, v2, v17

    const/16 v17, 0x1802

    aput v17, v2, v16

    const/16 v16, -0x1a94

    aput v16, v2, v15

    const/16 v15, -0x74

    aput v15, v2, v14

    const/4 v14, -0x8

    aput v14, v2, v13

    const/16 v13, -0x2f

    aput v13, v2, v12

    const/16 v12, 0x796e

    aput v12, v2, v11

    const/16 v11, -0x8e6

    aput v11, v2, v10

    const/16 v10, -0x4a

    aput v10, v2, v9

    const/16 v9, -0x35

    aput v9, v2, v8

    const/16 v8, -0x2c

    aput v8, v2, v7

    const/16 v7, -0x78a3

    aput v7, v2, v6

    const/16 v6, -0x1d

    aput v6, v2, v5

    const/16 v5, -0x6e

    aput v5, v2, v3

    const/4 v3, -0x6

    aput v3, v2, v1

    const/16 v1, 0x14

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, -0x27

    aput v24, v1, v23

    const/16 v23, -0x30

    aput v23, v1, v22

    const/16 v22, -0x2c8d

    aput v22, v1, v21

    const/16 v21, -0x2d

    aput v21, v1, v20

    const/16 v20, -0x35

    aput v20, v1, v19

    const/16 v19, -0x71

    aput v19, v1, v18

    const/16 v18, 0x187b

    aput v18, v1, v17

    const/16 v17, -0x1ae8

    aput v17, v1, v16

    const/16 v16, -0x1b

    aput v16, v1, v15

    const/16 v15, -0x72

    aput v15, v1, v14

    const/16 v14, -0x48

    aput v14, v1, v13

    const/16 v13, 0x791a

    aput v13, v1, v12

    const/16 v12, -0x887

    aput v12, v1, v11

    const/16 v11, -0x9

    aput v11, v1, v10

    const/16 v10, -0x41

    aput v10, v1, v9

    const/16 v9, -0x4f

    aput v9, v1, v8

    const/16 v8, -0x78c6

    aput v8, v1, v7

    const/16 v7, -0x79

    aput v7, v1, v6

    const/4 v6, -0x5

    aput v6, v1, v5

    const/16 v5, -0x73

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_8
    array-length v5, v1

    if-lt v3, v5, :cond_f

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_9
    array-length v5, v1

    if-lt v3, v5, :cond_10

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    const/16 v1, 0x14

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, -0xd

    aput v24, v2, v23

    const/16 v23, -0x54fb

    aput v23, v2, v22

    const/16 v22, -0x3e

    aput v22, v2, v21

    const/16 v21, -0x33

    aput v21, v2, v20

    const/16 v20, -0x3f

    aput v20, v2, v19

    const/16 v19, -0x43

    aput v19, v2, v18

    const/16 v18, -0xa

    aput v18, v2, v17

    const/16 v17, -0x4baf

    aput v17, v2, v16

    const/16 v16, -0x23

    aput v16, v2, v15

    const/16 v15, -0x21

    aput v15, v2, v14

    const/16 v14, -0x18

    aput v14, v2, v13

    const/16 v13, -0x31e5

    aput v13, v2, v12

    const/16 v12, -0x53

    aput v12, v2, v11

    const/16 v11, -0x56

    aput v11, v2, v10

    const/16 v10, 0x6b05

    aput v10, v2, v9

    const/16 v9, 0x3c0e

    aput v9, v2, v8

    const/16 v8, -0x28a5

    aput v8, v2, v7

    const/16 v7, -0x4d

    aput v7, v2, v6

    const/16 v6, -0x16

    aput v6, v2, v3

    const/16 v3, -0x26

    aput v3, v2, v1

    const/16 v1, 0x14

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, -0x63

    aput v25, v1, v24

    const/16 v24, -0x5496

    aput v24, v1, v23

    const/16 v23, -0x55

    aput v23, v1, v22

    const/16 v22, -0x47

    aput v22, v1, v21

    const/16 v21, -0x5e

    aput v21, v1, v20

    const/16 v20, -0x4

    aput v20, v1, v19

    const/16 v19, -0x71

    aput v19, v1, v18

    const/16 v18, -0x4bdb

    aput v18, v1, v17

    const/16 v17, -0x4c

    aput v17, v1, v16

    const/16 v16, -0x57

    aput v16, v1, v15

    const/16 v15, -0x7f

    aput v15, v1, v14

    const/16 v14, -0x3191

    aput v14, v1, v13

    const/16 v13, -0x32

    aput v13, v1, v12

    const/16 v12, -0x15

    aput v12, v1, v11

    const/16 v11, 0x6b71

    aput v11, v1, v10

    const/16 v10, 0x3c6b

    aput v10, v1, v9

    const/16 v9, -0x28c4

    aput v9, v1, v8

    const/16 v8, -0x29

    aput v8, v1, v7

    const/16 v7, -0x7d

    aput v7, v1, v6

    const/16 v6, -0x53

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_a
    array-length v6, v1

    if-lt v3, v6, :cond_11

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_b
    array-length v6, v1

    if-lt v3, v6, :cond_12

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v5, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    :goto_c
    const/16 v1, 0x15

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, -0x22b9

    aput v25, v2, v24

    const/16 v24, -0x46

    aput v24, v2, v23

    const/16 v23, -0x58c

    aput v23, v2, v22

    const/16 v22, -0x6f

    aput v22, v2, v21

    const/16 v21, -0x2b

    aput v21, v2, v20

    const/16 v20, -0x7589

    aput v20, v2, v19

    const/16 v19, -0x26

    aput v19, v2, v18

    const/16 v18, -0x78

    aput v18, v2, v17

    const/16 v17, -0x73dd

    aput v17, v2, v16

    const/16 v16, -0x1b

    aput v16, v2, v15

    const/16 v15, 0x111e

    aput v15, v2, v14

    const/16 v14, -0x3a88

    aput v14, v2, v13

    const/16 v13, -0x4f

    aput v13, v2, v12

    const/16 v12, -0x39

    aput v12, v2, v11

    const/16 v11, -0x4bbe

    aput v11, v2, v10

    const/16 v10, -0x40

    aput v10, v2, v9

    const/16 v9, 0x396f

    aput v9, v2, v8

    const/16 v8, -0x14a2

    aput v8, v2, v7

    const/16 v7, -0x71

    aput v7, v2, v6

    const/16 v6, 0x4657

    aput v6, v2, v3

    const/16 v3, -0x7bcf

    aput v3, v2, v1

    const/16 v1, 0x15

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, -0x22de

    aput v26, v1, v25

    const/16 v25, -0x23

    aput v25, v1, v24

    const/16 v24, -0x5eb

    aput v24, v1, v23

    const/16 v23, -0x6

    aput v23, v1, v22

    const/16 v22, -0x4a

    aput v22, v1, v21

    const/16 v21, -0x75ea

    aput v21, v1, v20

    const/16 v20, -0x76

    aput v20, v1, v19

    const/16 v19, -0xf

    aput v19, v1, v18

    const/16 v18, -0x73a9

    aput v18, v1, v17

    const/16 v17, -0x74

    aput v17, v1, v16

    const/16 v16, 0x1168

    aput v16, v1, v15

    const/16 v15, -0x3aef

    aput v15, v1, v14

    const/16 v14, -0x3b

    aput v14, v1, v13

    const/16 v13, -0x5c

    aput v13, v1, v12

    const/16 v12, -0x4bfd

    aput v12, v1, v11

    const/16 v11, -0x4c

    aput v11, v1, v10

    const/16 v10, 0x390a

    aput v10, v1, v9

    const/16 v9, -0x14c7

    aput v9, v1, v8

    const/16 v8, -0x15

    aput v8, v1, v7

    const/16 v7, 0x463e

    aput v7, v1, v6

    const/16 v6, -0x7bba

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_d
    array-length v6, v1

    if-lt v3, v6, :cond_15

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_e
    array-length v6, v1

    if-lt v3, v6, :cond_16

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const/16 v1, 0x18

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, -0x44fb

    aput v28, v2, v27

    const/16 v27, -0x38

    aput v27, v2, v26

    const/16 v26, -0x4e3

    aput v26, v2, v25

    const/16 v25, -0x68

    aput v25, v2, v24

    const/16 v24, -0x11

    aput v24, v2, v23

    const/16 v23, -0x7e

    aput v23, v2, v22

    const/16 v22, 0x467a

    aput v22, v2, v21

    const/16 v21, -0x39cc

    aput v21, v2, v20

    const/16 v20, -0x5c

    aput v20, v2, v19

    const/16 v19, 0x5752

    aput v19, v2, v18

    const/16 v18, 0x4323

    aput v18, v2, v17

    const/16 v17, -0x16da

    aput v17, v2, v16

    const/16 v16, -0x72

    aput v16, v2, v15

    const/16 v15, -0x37f4

    aput v15, v2, v14

    const/16 v14, -0x5f

    aput v14, v2, v13

    const/16 v13, -0x30da

    aput v13, v2, v12

    const/16 v12, -0x70

    aput v12, v2, v11

    const/16 v11, 0x2214

    aput v11, v2, v10

    const/16 v10, -0x3bb5

    aput v10, v2, v9

    const/16 v9, -0x65

    aput v9, v2, v8

    const/16 v8, -0x69

    aput v8, v2, v7

    const/4 v7, -0x3

    aput v7, v2, v6

    const/16 v6, -0x19

    aput v6, v2, v3

    const/16 v3, -0x1de8

    aput v3, v2, v1

    const/16 v1, 0x18

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, -0x448f

    aput v29, v1, v28

    const/16 v28, -0x45

    aput v28, v1, v27

    const/16 v27, -0x484

    aput v27, v1, v26

    const/16 v26, -0x5

    aput v26, v1, v25

    const/16 v25, -0x75

    aput v25, v1, v24

    const/16 v24, -0x1d

    aput v24, v1, v23

    const/16 v23, 0x4615

    aput v23, v1, v22

    const/16 v22, -0x39ba

    aput v22, v1, v21

    const/16 v21, -0x3a

    aput v21, v1, v20

    const/16 v20, 0x570d

    aput v20, v1, v19

    const/16 v19, 0x4357

    aput v19, v1, v18

    const/16 v18, -0x16bd

    aput v18, v1, v17

    const/16 v17, -0x17

    aput v17, v1, v16

    const/16 v16, -0x3798

    aput v16, v1, v15

    const/16 v15, -0x38

    aput v15, v1, v14

    const/16 v14, -0x30af

    aput v14, v1, v13

    const/16 v13, -0x31

    aput v13, v1, v12

    const/16 v12, 0x227a

    aput v12, v1, v11

    const/16 v11, -0x3bde

    aput v11, v1, v10

    const/16 v10, -0x3c

    aput v10, v1, v9

    const/16 v9, -0x10

    aput v9, v1, v8

    const/16 v8, -0x78

    aput v8, v1, v7

    const/16 v7, -0x75

    aput v7, v1, v6

    const/16 v6, -0x1d98

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_f
    array-length v6, v1

    if-lt v3, v6, :cond_17

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_10
    array-length v6, v1

    if-lt v3, v6, :cond_18

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v5, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v1, 0x6

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/16 v10, -0x1d

    aput v10, v2, v9

    const/16 v9, -0x28

    aput v9, v2, v8

    const/16 v8, -0x30df

    aput v8, v2, v7

    const/16 v7, -0x55

    aput v7, v2, v6

    const/16 v6, 0x496e

    aput v6, v2, v3

    const/16 v3, -0x7ac2

    aput v3, v2, v1

    const/4 v1, 0x6

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/16 v11, -0x69

    aput v11, v1, v10

    const/16 v10, -0x43

    aput v10, v1, v9

    const/16 v9, -0x30ba

    aput v9, v1, v8

    const/16 v8, -0x31

    aput v8, v1, v7

    const/16 v7, 0x4907

    aput v7, v1, v6

    const/16 v6, -0x7ab7

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_11
    array-length v6, v1

    if-lt v3, v6, :cond_19

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_12
    array-length v6, v1

    if-lt v3, v6, :cond_1a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v5, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/high16 v1, 0x4000000

    invoke-virtual {v5, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const/16 v1, 0x14

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, -0x70

    aput v24, v2, v23

    const/16 v23, -0x59a

    aput v23, v2, v22

    const/16 v22, -0x6d

    aput v22, v2, v21

    const/16 v21, -0x50

    aput v21, v2, v20

    const/16 v20, 0x446b

    aput v20, v2, v19

    const/16 v19, -0x16d9

    aput v19, v2, v18

    const/16 v18, -0x80

    aput v18, v2, v17

    const/16 v17, -0x7be9

    aput v17, v2, v16

    const/16 v16, -0x13

    aput v16, v2, v15

    const/16 v15, -0x18ee

    aput v15, v2, v14

    const/16 v14, -0x78

    aput v14, v2, v13

    const/16 v13, -0x2e

    aput v13, v2, v12

    const/16 v12, -0x4c

    aput v12, v2, v11

    const/16 v11, -0x13

    aput v11, v2, v10

    const/16 v10, -0x1d

    aput v10, v2, v9

    const/16 v9, -0x40

    aput v9, v2, v8

    const/16 v8, -0x1b

    aput v8, v2, v7

    const/16 v7, -0x67

    aput v7, v2, v6

    const/16 v6, 0x7010

    aput v6, v2, v3

    const/16 v3, -0x13d9

    aput v3, v2, v1

    const/16 v1, 0x14

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, -0x2

    aput v25, v1, v24

    const/16 v24, -0x5f7

    aput v24, v1, v23

    const/16 v23, -0x6

    aput v23, v1, v22

    const/16 v22, -0x3c

    aput v22, v1, v21

    const/16 v21, 0x440a

    aput v21, v1, v20

    const/16 v20, -0x16bc

    aput v20, v1, v19

    const/16 v19, -0x17

    aput v19, v1, v18

    const/16 v18, -0x7b8f

    aput v18, v1, v17

    const/16 v17, -0x7c

    aput v17, v1, v16

    const/16 v16, -0x189a

    aput v16, v1, v15

    const/16 v15, -0x19

    aput v15, v1, v14

    const/16 v14, -0x64

    aput v14, v1, v13

    const/16 v13, -0x3a

    aput v13, v1, v12

    const/16 v12, -0x5e

    aput v12, v1, v11

    const/16 v11, -0x69

    aput v11, v1, v10

    const/16 v10, -0x5b

    aput v10, v1, v9

    const/16 v9, -0x7e

    aput v9, v1, v8

    const/4 v8, -0x3

    aput v8, v1, v7

    const/16 v7, 0x7079

    aput v7, v1, v6

    const/16 v6, -0x1390

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_13
    array-length v6, v1

    if-lt v3, v6, :cond_1b

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_14
    array-length v6, v1

    if-lt v3, v6, :cond_1c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x1f

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x11

    const/16 v24, 0x12

    const/16 v25, 0x13

    const/16 v26, 0x14

    const/16 v27, 0x15

    const/16 v28, 0x16

    const/16 v29, 0x17

    const/16 v30, 0x18

    const/16 v31, 0x19

    const/16 v32, 0x1a

    const/16 v33, 0x1b

    const/16 v34, 0x1c

    const/16 v35, 0x1d

    const/16 v36, 0x1e

    const/16 v37, 0x3065

    aput v37, v2, v36

    const/16 v36, 0x2a10

    aput v36, v2, v35

    const/16 v35, -0x71b2

    aput v35, v2, v34

    const/16 v34, -0x15

    aput v34, v2, v33

    const/16 v33, -0x579d

    aput v33, v2, v32

    const/16 v32, -0x3f

    aput v32, v2, v31

    const/16 v31, -0x80

    aput v31, v2, v30

    const/16 v30, -0x93

    aput v30, v2, v29

    const/16 v29, -0x66

    aput v29, v2, v28

    const/16 v28, -0xf

    aput v28, v2, v27

    const/16 v27, -0x5b

    aput v27, v2, v26

    const/16 v26, -0x56

    aput v26, v2, v25

    const/16 v25, 0x3b71

    aput v25, v2, v24

    const/16 v24, 0x7452

    aput v24, v2, v23

    const/16 v23, -0x800

    aput v23, v2, v22

    const/16 v22, -0x67

    aput v22, v2, v21

    const/16 v21, -0x13

    aput v21, v2, v20

    const/16 v20, -0x2

    aput v20, v2, v19

    const/16 v19, -0x14e6

    aput v19, v2, v18

    const/16 v18, -0x7e

    aput v18, v2, v17

    const/16 v17, 0x4d7e

    aput v17, v2, v16

    const/16 v16, 0x3e22

    aput v16, v2, v15

    const/16 v15, -0x5790

    aput v15, v2, v14

    const/16 v14, -0x26

    aput v14, v2, v13

    const/16 v13, -0x1f

    aput v13, v2, v12

    const/16 v12, -0x4095

    aput v12, v2, v11

    const/16 v11, -0x26

    aput v11, v2, v10

    const/16 v10, -0x5a

    aput v10, v2, v9

    const/16 v9, -0x17

    aput v9, v2, v8

    const/16 v8, -0x56c0

    aput v8, v2, v3

    const/4 v3, -0x2

    aput v3, v2, v1

    const/16 v1, 0x1f

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v11, 0x4

    const/4 v12, 0x5

    const/4 v13, 0x6

    const/4 v14, 0x7

    const/16 v15, 0x8

    const/16 v16, 0x9

    const/16 v17, 0xa

    const/16 v18, 0xb

    const/16 v19, 0xc

    const/16 v20, 0xd

    const/16 v21, 0xe

    const/16 v22, 0xf

    const/16 v23, 0x10

    const/16 v24, 0x11

    const/16 v25, 0x12

    const/16 v26, 0x13

    const/16 v27, 0x14

    const/16 v28, 0x15

    const/16 v29, 0x16

    const/16 v30, 0x17

    const/16 v31, 0x18

    const/16 v32, 0x19

    const/16 v33, 0x1a

    const/16 v34, 0x1b

    const/16 v35, 0x1c

    const/16 v36, 0x1d

    const/16 v37, 0x1e

    const/16 v38, 0x3045

    aput v38, v1, v37

    const/16 v37, 0x2a30

    aput v37, v1, v36

    const/16 v36, -0x71d6

    aput v36, v1, v35

    const/16 v35, -0x72

    aput v35, v1, v34

    const/16 v34, -0x57eb

    aput v34, v1, v33

    const/16 v33, -0x58

    aput v33, v1, v32

    const/16 v32, -0x1b

    aput v32, v1, v31

    const/16 v31, -0xf2

    aput v31, v1, v30

    const/16 v30, -0x1

    aput v30, v1, v29

    const/16 v29, -0x7d

    aput v29, v1, v28

    const/16 v28, -0x7b

    aput v28, v1, v27

    const/16 v27, -0x3c

    aput v27, v1, v26

    const/16 v26, 0x3b1e

    aput v26, v1, v25

    const/16 v25, 0x743b

    aput v25, v1, v24

    const/16 v24, -0x78c

    aput v24, v1, v23

    const/16 v23, -0x8

    aput v23, v1, v22

    const/16 v22, -0x72

    aput v22, v1, v21

    const/16 v21, -0x69

    aput v21, v1, v20

    const/16 v20, -0x1484

    aput v20, v1, v19

    const/16 v19, -0x15

    aput v19, v1, v18

    const/16 v18, 0x4d0a

    aput v18, v1, v17

    const/16 v17, 0x3e4d

    aput v17, v1, v16

    const/16 v16, -0x57c2

    aput v16, v1, v15

    const/16 v15, -0x58

    aput v15, v1, v14

    const/16 v14, -0x52

    aput v14, v1, v13

    const/16 v13, -0x40e1

    aput v13, v1, v12

    const/16 v12, -0x41

    aput v12, v1, v11

    const/16 v11, -0x3f

    aput v11, v1, v10

    const/16 v10, -0x73

    aput v10, v1, v9

    const/16 v9, -0x56d7

    aput v9, v1, v8

    const/16 v8, -0x57

    aput v8, v1, v3

    const/4 v3, 0x0

    :goto_15
    array-length v8, v1

    if-lt v3, v8, :cond_1d

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_16
    array-length v8, v1

    if-lt v3, v8, :cond_1e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v5}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, -0x2883

    aput v12, v2, v11

    const/16 v11, -0x5d

    aput v11, v2, v10

    const/16 v10, 0x5574

    aput v10, v2, v9

    const/16 v9, 0x6234

    aput v9, v2, v8

    const/16 v8, -0x67f9

    aput v8, v2, v7

    const/16 v7, -0x30

    aput v7, v2, v6

    const/16 v6, -0x2c

    aput v6, v2, v3

    const/16 v3, -0x42fd

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, -0x28eb

    aput v13, v1, v12

    const/16 v12, -0x29

    aput v12, v1, v11

    const/16 v11, 0x5518

    aput v11, v1, v10

    const/16 v10, 0x6255

    aput v10, v1, v9

    const/16 v9, -0x679e

    aput v9, v1, v8

    const/16 v8, -0x68

    aput v8, v1, v7

    const/16 v7, -0xc

    aput v7, v1, v6

    const/16 v6, -0x42b0

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_17
    array-length v6, v1

    if-lt v3, v6, :cond_1f

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_18
    array-length v6, v1

    if-lt v3, v6, :cond_20

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v6

    const/16 v1, 0x1a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, -0x5188

    aput v31, v2, v30

    const/16 v30, -0x39

    aput v30, v2, v29

    const/16 v29, -0x1e

    aput v29, v2, v28

    const/16 v28, -0x2f

    aput v28, v2, v27

    const/16 v27, -0x41

    aput v27, v2, v26

    const/16 v26, 0xb

    aput v26, v2, v25

    const/16 v25, 0x5920

    aput v25, v2, v24

    const/16 v24, -0x40c4

    aput v24, v2, v23

    const/16 v23, -0x37

    aput v23, v2, v22

    const/16 v22, -0x6fe1

    aput v22, v2, v21

    const/16 v21, -0x1c

    aput v21, v2, v20

    const/16 v20, -0xdfa

    aput v20, v2, v19

    const/16 v19, -0x69

    aput v19, v2, v18

    const/16 v18, -0x47ee

    aput v18, v2, v17

    const/16 v17, -0x35

    aput v17, v2, v16

    const/16 v16, -0x31

    aput v16, v2, v15

    const/16 v15, -0x78e7

    aput v15, v2, v14

    const/16 v14, -0x59

    aput v14, v2, v13

    const/16 v13, 0x5858

    aput v13, v2, v12

    const/16 v12, 0x1c3b

    aput v12, v2, v11

    const/16 v11, 0x1672

    aput v11, v2, v10

    const/16 v10, 0x7763

    aput v10, v2, v9

    const/16 v9, 0x416

    aput v9, v2, v8

    const/16 v8, -0x398

    aput v8, v2, v7

    const/16 v7, -0x24

    aput v7, v2, v3

    const/16 v3, -0x7bfb

    aput v3, v2, v1

    const/16 v1, 0x1a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x11

    const/16 v24, 0x12

    const/16 v25, 0x13

    const/16 v26, 0x14

    const/16 v27, 0x15

    const/16 v28, 0x16

    const/16 v29, 0x17

    const/16 v30, 0x18

    const/16 v31, 0x19

    const/16 v32, -0x51ea

    aput v32, v1, v31

    const/16 v31, -0x52

    aput v31, v1, v30

    const/16 v30, -0x7b

    aput v30, v1, v29

    const/16 v29, -0x5c

    aput v29, v1, v28

    const/16 v28, -0x2d

    aput v28, v1, v27

    const/16 v27, 0x7b

    aput v27, v1, v26

    const/16 v26, 0x5900

    aput v26, v1, v25

    const/16 v25, -0x40a7

    aput v25, v1, v24

    const/16 v24, -0x41

    aput v24, v1, v23

    const/16 v23, -0x6f8a

    aput v23, v1, v22

    const/16 v22, -0x70

    aput v22, v1, v21

    const/16 v21, -0xd9b

    aput v21, v1, v20

    const/16 v20, -0xe

    aput v20, v1, v19

    const/16 v19, -0x479e

    aput v19, v1, v18

    const/16 v18, -0x48

    aput v18, v1, v17

    const/16 v17, -0x56

    aput v17, v1, v16

    const/16 v16, -0x7895

    aput v16, v1, v15

    const/16 v15, -0x79

    aput v15, v1, v14

    const/16 v14, 0x5830

    aput v14, v1, v13

    const/16 v13, 0x1c58

    aput v13, v1, v12

    const/16 v12, 0x161c

    aput v12, v1, v11

    const/16 v11, 0x7716

    aput v11, v1, v10

    const/16 v10, 0x477

    aput v10, v1, v9

    const/16 v9, -0x3fc

    aput v9, v1, v8

    const/4 v8, -0x4

    aput v8, v1, v7

    const/16 v7, -0x7bd8

    aput v7, v1, v3

    const/4 v3, 0x0

    :goto_19
    array-length v7, v1

    if-lt v3, v7, :cond_21

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1a
    array-length v7, v1

    if-lt v3, v7, :cond_22

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->startActivity(Landroid/content/Intent;)V

    const/16 v1, 0x14

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x959

    aput v23, v2, v22

    const/16 v22, 0x6866

    aput v22, v2, v21

    const/16 v21, 0x1401

    aput v21, v2, v20

    const/16 v20, -0x60a0

    aput v20, v2, v19

    const/16 v19, -0x4

    aput v19, v2, v18

    const/16 v18, -0x11a8

    aput v18, v2, v17

    const/16 v17, -0x69

    aput v17, v2, v16

    const/16 v16, -0x61d3

    aput v16, v2, v15

    const/16 v15, -0x9

    aput v15, v2, v14

    const/16 v14, 0x1368

    aput v14, v2, v13

    const/16 v13, 0x27a

    aput v13, v2, v12

    const/16 v12, 0xf76

    aput v12, v2, v11

    const/16 v11, 0x316c

    aput v11, v2, v10

    const/16 v10, -0x7a90

    aput v10, v2, v9

    const/16 v9, -0xf

    aput v9, v2, v8

    const/16 v8, 0x2c21

    aput v8, v2, v7

    const/16 v7, 0x604b

    aput v7, v2, v6

    const/16 v6, 0x1504

    aput v6, v2, v5

    const/16 v5, -0x5884

    aput v5, v2, v3

    const/16 v3, -0x30

    aput v3, v2, v1

    const/16 v1, 0x14

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x937

    aput v24, v1, v23

    const/16 v23, 0x6809

    aput v23, v1, v22

    const/16 v22, 0x1468

    aput v22, v1, v21

    const/16 v21, -0x60ec

    aput v21, v1, v20

    const/16 v20, -0x61

    aput v20, v1, v19

    const/16 v19, -0x11e7

    aput v19, v1, v18

    const/16 v18, -0x12

    aput v18, v1, v17

    const/16 v17, -0x61a7

    aput v17, v1, v16

    const/16 v16, -0x62

    aput v16, v1, v15

    const/16 v15, 0x131e

    aput v15, v1, v14

    const/16 v14, 0x213

    aput v14, v1, v13

    const/16 v13, 0xf02

    aput v13, v1, v12

    const/16 v12, 0x310f

    aput v12, v1, v11

    const/16 v11, -0x7acf

    aput v11, v1, v10

    const/16 v10, -0x7b

    aput v10, v1, v9

    const/16 v9, 0x2c44

    aput v9, v1, v8

    const/16 v8, 0x602c

    aput v8, v1, v7

    const/16 v7, 0x1560

    aput v7, v1, v6

    const/16 v6, -0x58eb

    aput v6, v1, v5

    const/16 v5, -0x59

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_1b
    array-length v5, v1

    if-lt v3, v5, :cond_23

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1c
    array-length v5, v1

    if-lt v3, v5, :cond_24

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    :cond_8
    const/4 v1, 0x1

    :goto_1d
    return v1

    :cond_9
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_a
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_b
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_c
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_d
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_e
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :cond_f
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_8

    :cond_10
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_9

    :cond_11
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_a

    :cond_12
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_b

    :catch_0
    move-exception v1

    const/16 v1, 0x23

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0xf4a

    aput v39, v2, v38

    const/16 v38, 0x7e4b

    aput v38, v2, v37

    const/16 v37, 0xf31

    aput v37, v2, v36

    const/16 v36, 0x7042

    aput v36, v2, v35

    const/16 v35, -0x21d1

    aput v35, v2, v34

    const/16 v34, -0x6a

    aput v34, v2, v33

    const/16 v33, -0x3a

    aput v33, v2, v32

    const/16 v32, -0x56

    aput v32, v2, v31

    const/16 v31, 0x3164

    aput v31, v2, v30

    const/16 v30, -0x3b8c

    aput v30, v2, v29

    const/16 v29, -0x70

    aput v29, v2, v28

    const/16 v28, -0x7d

    aput v28, v2, v27

    const/16 v27, -0x1fda

    aput v27, v2, v26

    const/16 v26, -0x72

    aput v26, v2, v25

    const/16 v25, -0x1f

    aput v25, v2, v24

    const/16 v24, -0x15fb

    aput v24, v2, v23

    const/16 v23, -0x62

    aput v23, v2, v22

    const/16 v22, -0x2f

    aput v22, v2, v21

    const/16 v21, 0x3521

    aput v21, v2, v20

    const/16 v20, 0x81b

    aput v20, v2, v19

    const/16 v19, -0x77a0

    aput v19, v2, v18

    const/16 v18, -0x4

    aput v18, v2, v17

    const/16 v17, 0x6f7e    # 3.9996E-41f

    aput v17, v2, v16

    const/16 v16, -0x46f2

    aput v16, v2, v15

    const/16 v15, -0x24

    aput v15, v2, v14

    const/16 v14, -0x19

    aput v14, v2, v13

    const/16 v13, -0x45

    aput v13, v2, v12

    const/16 v12, -0x15

    aput v12, v2, v11

    const/16 v11, -0x45

    aput v11, v2, v10

    const/16 v10, -0x44

    aput v10, v2, v9

    const/16 v9, -0x3f

    aput v9, v2, v8

    const/16 v8, -0x58

    aput v8, v2, v7

    const/16 v7, -0x2a

    aput v7, v2, v6

    const/4 v6, -0x8

    aput v6, v2, v3

    const/16 v3, -0xc

    aput v3, v2, v1

    const/16 v1, 0x23

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, 0x1a

    const/16 v32, 0x1b

    const/16 v33, 0x1c

    const/16 v34, 0x1d

    const/16 v35, 0x1e

    const/16 v36, 0x1f

    const/16 v37, 0x20

    const/16 v38, 0x21

    const/16 v39, 0x22

    const/16 v40, 0xf0f

    aput v40, v1, v39

    const/16 v39, 0x7e0f

    aput v39, v1, v38

    const/16 v38, 0xf7e

    aput v38, v1, v37

    const/16 v37, 0x700f

    aput v37, v1, v36

    const/16 v36, -0x2190

    aput v36, v1, v35

    const/16 v35, -0x22

    aput v35, v1, v34

    const/16 v34, -0x6e

    aput v34, v1, v33

    const/16 v33, -0x1a

    aput v33, v1, v32

    const/16 v32, 0x3125

    aput v32, v1, v31

    const/16 v31, -0x3bcf

    aput v31, v1, v30

    const/16 v30, -0x3c

    aput v30, v1, v29

    const/16 v29, -0x30

    aput v29, v1, v28

    const/16 v28, -0x1ff8

    aput v28, v1, v27

    const/16 v27, -0x20

    aput v27, v1, v26

    const/16 v26, -0x72

    aput v26, v1, v25

    const/16 v25, -0x1594

    aput v25, v1, v24

    const/16 v24, -0x16

    aput v24, v1, v23

    const/16 v23, -0x4e

    aput v23, v1, v22

    const/16 v22, 0x3540

    aput v22, v1, v21

    const/16 v21, 0x835

    aput v21, v1, v20

    const/16 v20, -0x77f8

    aput v20, v1, v19

    const/16 v19, -0x78

    aput v19, v1, v18

    const/16 v18, 0x6f12

    aput v18, v1, v17

    const/16 v17, -0x4691

    aput v17, v1, v16

    const/16 v16, -0x47

    aput v16, v1, v15

    const/16 v15, -0x71

    aput v15, v1, v14

    const/16 v14, -0x38

    aput v14, v1, v13

    const/16 v13, -0x3b

    aput v13, v1, v12

    const/16 v12, -0x28

    aput v12, v1, v11

    const/16 v11, -0x27

    aput v11, v1, v10

    const/16 v10, -0x4e

    aput v10, v1, v9

    const/16 v9, -0x7a

    aput v9, v1, v8

    const/16 v8, -0x45

    aput v8, v1, v7

    const/16 v7, -0x69

    aput v7, v1, v6

    const/16 v6, -0x69

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_1e
    array-length v6, v1

    if-lt v3, v6, :cond_13

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1f
    array-length v6, v1

    if-lt v3, v6, :cond_14

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_c

    :cond_13
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1e

    :cond_14
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1f

    :cond_15
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_d

    :cond_16
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_e

    :cond_17
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_f

    :cond_18
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_10

    :cond_19
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_11

    :cond_1a
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_12

    :cond_1b
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_13

    :cond_1c
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_14

    :cond_1d
    aget v8, v1, v3

    aget v9, v2, v3

    xor-int/2addr v8, v9

    aput v8, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_15

    :cond_1e
    aget v8, v2, v3

    int-to-char v8, v8

    aput-char v8, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_16

    :cond_1f
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_17

    :cond_20
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_18

    :cond_21
    aget v7, v1, v3

    aget v8, v2, v3

    xor-int/2addr v7, v8

    aput v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_19

    :cond_22
    aget v7, v2, v3

    int-to-char v7, v7

    aput-char v7, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1a

    :cond_23
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1b

    :cond_24
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1c

    :cond_25
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->isCorrecPassword()Z

    move-result v1

    if-eqz v1, :cond_31

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->isSetPasswordActivity:Z

    if-eqz v1, :cond_31

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, -0x6681

    aput v10, v2, v9

    const/16 v9, -0x13

    aput v9, v2, v8

    const/4 v8, -0x8

    aput v8, v2, v7

    const/16 v7, -0x5ba3

    aput v7, v2, v6

    const/16 v6, -0x3f

    aput v6, v2, v5

    const/16 v5, -0x5e

    aput v5, v2, v4

    const/16 v4, -0x23

    aput v4, v2, v3

    const/16 v3, -0x38

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, -0x66e9

    aput v11, v1, v10

    const/16 v10, -0x67

    aput v10, v1, v9

    const/16 v9, -0x6c

    aput v9, v1, v8

    const/16 v8, -0x5bc4

    aput v8, v1, v7

    const/16 v7, -0x5c

    aput v7, v1, v6

    const/16 v6, -0x16

    aput v6, v1, v5

    const/4 v5, -0x3

    aput v5, v1, v4

    const/16 v4, -0x65

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_20
    array-length v4, v1

    if-lt v3, v4, :cond_27

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_21
    array-length v4, v1

    if-lt v3, v4, :cond_28

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->u6iG()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/sec/android/service/health/keyManager/KeyManager;->setting_password:Z

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mEditTextPinCode:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->setPasswordByUser(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mEditTextPinCode:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->setPincode(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mEditTextPinCode:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/service/health/keyManager/KeyManager;->getDBKeyFromKeyStore(Ljava/lang/String;I)Ljava/lang/String;

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->setApplicationPassword(Z)V

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->isToHomeActivity:Z

    if-eqz v2, :cond_26

    new-instance v4, Landroid/content/Intent;

    const/16 v1, 0x1c

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, -0x5

    aput v31, v2, v30

    const/16 v30, -0x5b

    aput v30, v2, v29

    const/16 v29, -0x14

    aput v29, v2, v28

    const/16 v28, -0x6ba6

    aput v28, v2, v27

    const/16 v27, -0x3

    aput v27, v2, v26

    const/16 v26, -0x8

    aput v26, v2, v25

    const/16 v25, -0x5c

    aput v25, v2, v24

    const/16 v24, 0x5706

    aput v24, v2, v23

    const/16 v23, -0x4fce

    aput v23, v2, v22

    const/16 v22, -0x23

    aput v22, v2, v21

    const/16 v21, -0x14

    aput v21, v2, v20

    const/16 v20, -0x12

    aput v20, v2, v19

    const/16 v19, -0x31ad

    aput v19, v2, v18

    const/16 v18, -0x5a

    aput v18, v2, v17

    const/16 v17, -0x3c

    aput v17, v2, v16

    const/16 v16, -0x1d

    aput v16, v2, v15

    const/16 v15, -0x6ff4

    aput v15, v2, v14

    const/16 v14, -0xb

    aput v14, v2, v13

    const/16 v13, 0x6b62

    aput v13, v2, v12

    const/16 v12, -0x6ae8

    aput v12, v2, v11

    const/16 v11, -0x45

    aput v11, v2, v10

    const/16 v10, -0x28b4

    aput v10, v2, v9

    const/16 v9, -0x4e

    aput v9, v2, v8

    const/16 v8, -0x14de

    aput v8, v2, v7

    const/16 v7, -0x3b

    aput v7, v2, v6

    const/16 v6, -0x60db

    aput v6, v2, v5

    const/16 v5, -0x10

    aput v5, v2, v3

    const/16 v3, -0x3696

    aput v3, v2, v1

    const/16 v1, 0x1c

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, -0x7e

    aput v32, v1, v31

    const/16 v31, -0x2f

    aput v31, v1, v30

    const/16 v30, -0x7b

    aput v30, v1, v29

    const/16 v29, -0x6bd4

    aput v29, v1, v28

    const/16 v28, -0x6c

    aput v28, v1, v27

    const/16 v27, -0x74

    aput v27, v1, v26

    const/16 v26, -0x39

    aput v26, v1, v25

    const/16 v25, 0x5747

    aput v25, v1, v24

    const/16 v24, -0x4fa9

    aput v24, v1, v23

    const/16 v23, -0x50

    aput v23, v1, v22

    const/16 v22, -0x7d

    aput v22, v1, v21

    const/16 v21, -0x5a

    aput v21, v1, v20

    const/16 v20, -0x3183

    aput v20, v1, v19

    const/16 v19, -0x32

    aput v19, v1, v18

    const/16 v18, -0x50

    aput v18, v1, v17

    const/16 v17, -0x71

    aput v17, v1, v16

    const/16 v16, -0x6f93

    aput v16, v1, v15

    const/16 v15, -0x70

    aput v15, v1, v14

    const/16 v14, 0x6b0a

    aput v14, v1, v13

    const/16 v13, -0x6a95

    aput v13, v1, v12

    const/16 v12, -0x6b

    aput v12, v1, v11

    const/16 v11, -0x28d1

    aput v11, v1, v10

    const/16 v10, -0x29

    aput v10, v1, v9

    const/16 v9, -0x14af

    aput v9, v1, v8

    const/16 v8, -0x15

    aput v8, v1, v7

    const/16 v7, -0x60b8

    aput v7, v1, v6

    const/16 v6, -0x61

    aput v6, v1, v5

    const/16 v5, -0x36f7

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_22
    array-length v5, v1

    if-lt v3, v5, :cond_29

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_23
    array-length v5, v1

    if-lt v3, v5, :cond_2a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x1b

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, -0x55c6

    aput v30, v2, v29

    const/16 v29, -0x22

    aput v29, v2, v28

    const/16 v28, -0x26a3

    aput v28, v2, v27

    const/16 v27, -0x48

    aput v27, v2, v26

    const/16 v26, -0x73

    aput v26, v2, v25

    const/16 v25, -0x87

    aput v25, v2, v24

    const/16 v24, -0x74

    aput v24, v2, v23

    const/16 v23, -0x36f6

    aput v23, v2, v22

    const/16 v22, -0x47

    aput v22, v2, v21

    const/16 v21, -0x7cbb

    aput v21, v2, v20

    const/16 v20, -0x1e

    aput v20, v2, v19

    const/16 v19, -0x53b9

    aput v19, v2, v18

    const/16 v18, -0x38

    aput v18, v2, v17

    const/16 v17, 0x2a5c

    aput v17, v2, v16

    const/16 v16, -0x55bb

    aput v16, v2, v15

    const/16 v15, -0x28

    aput v15, v2, v14

    const/16 v14, 0x3d39

    aput v14, v2, v13

    const/16 v13, -0x3cad

    aput v13, v2, v12

    const/16 v12, -0x5e

    aput v12, v2, v11

    const/16 v11, -0x1a

    aput v11, v2, v10

    const/16 v10, -0x78

    aput v10, v2, v9

    const/16 v9, -0x27

    aput v9, v2, v8

    const/16 v8, -0x14c3

    aput v8, v2, v7

    const/16 v7, -0x3b

    aput v7, v2, v6

    const/16 v6, -0x18

    aput v6, v2, v5

    const/16 v5, -0x43c9

    aput v5, v2, v3

    const/16 v3, -0x21

    aput v3, v2, v1

    const/16 v1, 0x1b

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, -0x55ae

    aput v31, v1, v30

    const/16 v30, -0x56

    aput v30, v1, v29

    const/16 v29, -0x26cf

    aput v29, v1, v28

    const/16 v28, -0x27

    aput v28, v1, v27

    const/16 v27, -0x18

    aput v27, v1, v26

    const/16 v26, -0xef

    aput v26, v1, v25

    const/16 v25, -0x1

    aput v25, v1, v24

    const/16 v24, -0x36dc

    aput v24, v1, v23

    const/16 v23, -0x37

    aput v23, v1, v22

    const/16 v22, -0x7ccb

    aput v22, v1, v21

    const/16 v21, -0x7d

    aput v21, v1, v20

    const/16 v20, -0x5397

    aput v20, v1, v19

    const/16 v19, -0x54

    aput v19, v1, v18

    const/16 v18, 0x2a35

    aput v18, v1, v17

    const/16 v17, -0x55d6

    aput v17, v1, v16

    const/16 v16, -0x56

    aput v16, v1, v15

    const/16 v15, 0x3d5d

    aput v15, v1, v14

    const/16 v14, -0x3cc3

    aput v14, v1, v13

    const/16 v13, -0x3d

    aput v13, v1, v12

    const/16 v12, -0x38

    aput v12, v1, v11

    const/16 v11, -0x15

    aput v11, v1, v10

    const/16 v10, -0x44

    aput v10, v1, v9

    const/16 v9, -0x14b2

    aput v9, v1, v8

    const/16 v8, -0x15

    aput v8, v1, v7

    const/16 v7, -0x7b

    aput v7, v1, v6

    const/16 v6, -0x43a8

    aput v6, v1, v5

    const/16 v5, -0x44

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_24
    array-length v5, v1

    if-lt v3, v5, :cond_2b

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_25
    array-length v5, v1

    if-lt v3, v5, :cond_2c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v4, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-object v1, v4

    :cond_26
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/service/health/keyManager/KeyManager;->addCallBackActivityIntent(Landroid/content/Intent;)V

    :goto_26
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/keyManager/KeyManager;->launchKeyManagerCallbackReceiver()V

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, -0x2ace

    aput v10, v2, v9

    const/16 v9, -0x5f

    aput v9, v2, v8

    const/16 v8, -0x4d

    aput v8, v2, v7

    const/16 v7, -0x1cbe

    aput v7, v2, v6

    const/16 v6, -0x7a

    aput v6, v2, v5

    const/16 v5, 0x7d72

    aput v5, v2, v4

    const/16 v4, -0x3ba3

    aput v4, v2, v3

    const/16 v3, -0x69

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, -0x2aa6

    aput v11, v1, v10

    const/16 v10, -0x2b

    aput v10, v1, v9

    const/16 v9, -0x21

    aput v9, v1, v8

    const/16 v8, -0x1cdd

    aput v8, v1, v7

    const/16 v7, -0x1d

    aput v7, v1, v6

    const/16 v6, 0x7d3a

    aput v6, v1, v5

    const/16 v5, -0x3b83

    aput v5, v1, v4

    const/16 v4, -0x3c

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_27
    array-length v4, v1

    if-lt v3, v4, :cond_2d

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_28
    array-length v4, v1

    if-lt v3, v4, :cond_2e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->rQYC()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->setRequestPassword(Z)V

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    move-result-object v4

    const/16 v1, 0x10

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x2265

    aput v19, v2, v18

    const/16 v18, -0x7bb0

    aput v18, v2, v17

    const/16 v17, -0x15

    aput v17, v2, v16

    const/16 v16, -0x5eff

    aput v16, v2, v15

    const/16 v15, -0x2e

    aput v15, v2, v14

    const/16 v14, 0x7850

    aput v14, v2, v13

    const/16 v13, 0x3919

    aput v13, v2, v12

    const/16 v12, 0x649

    aput v12, v2, v11

    const/16 v11, 0x2259

    aput v11, v2, v10

    const/16 v10, -0x4daa

    aput v10, v2, v9

    const/16 v9, -0x3f

    aput v9, v2, v8

    const/16 v8, -0x4e

    aput v8, v2, v7

    const/16 v7, 0xd1a

    aput v7, v2, v6

    const/16 v6, 0x2a7c

    aput v6, v2, v5

    const/16 v5, -0x3fb1

    aput v5, v2, v3

    const/16 v3, -0x4e

    aput v3, v2, v1

    const/16 v1, 0x10

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x2201

    aput v20, v1, v19

    const/16 v19, -0x7bde

    aput v19, v1, v18

    const/16 v18, -0x7c

    aput v18, v1, v17

    const/16 v17, -0x5e8a

    aput v17, v1, v16

    const/16 v16, -0x5f

    aput v16, v1, v15

    const/16 v15, 0x7823

    aput v15, v1, v14

    const/16 v14, 0x3978

    aput v14, v1, v13

    const/16 v13, 0x639

    aput v13, v1, v12

    const/16 v12, 0x2206

    aput v12, v1, v11

    const/16 v11, -0x4dde

    aput v11, v1, v10

    const/16 v10, -0x4e

    aput v10, v1, v9

    const/16 v9, -0x29

    aput v9, v1, v8

    const/16 v8, 0xd6f

    aput v8, v1, v7

    const/16 v7, 0x2a0d

    aput v7, v1, v6

    const/16 v6, -0x3fd6

    aput v6, v1, v5

    const/16 v5, -0x40

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_29
    array-length v5, v1

    if-lt v3, v5, :cond_2f

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2a
    array-length v5, v1

    if-lt v3, v5, :cond_30

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v4, v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->saveBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;Z)V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->finish()V

    const/4 v1, 0x1

    goto/16 :goto_1d

    :cond_27
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_20

    :cond_28
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_21

    :cond_29
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_22

    :cond_2a
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_23

    :cond_2b
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_24

    :cond_2c
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_25

    :cond_2d
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_27

    :cond_2e
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_28

    :cond_2f
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_29

    :cond_30
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2a

    :cond_31
    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x4645

    aput v10, v2, v9

    const/16 v9, 0x7f32

    aput v9, v2, v8

    const/16 v8, -0x3bed

    aput v8, v2, v7

    const/16 v7, -0x5b

    aput v7, v2, v6

    const/16 v6, -0x80

    aput v6, v2, v5

    const/16 v5, -0x6cc2

    aput v5, v2, v4

    const/16 v4, -0x4d

    aput v4, v2, v3

    const/16 v3, -0x10a2

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x462d

    aput v11, v1, v10

    const/16 v10, 0x7f46

    aput v10, v1, v9

    const/16 v9, -0x3b81

    aput v9, v1, v8

    const/16 v8, -0x3c

    aput v8, v1, v7

    const/16 v7, -0x1b

    aput v7, v1, v6

    const/16 v6, -0x6c8a

    aput v6, v1, v5

    const/16 v5, -0x6d

    aput v5, v1, v4

    const/16 v4, -0x10f3

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_2b
    array-length v4, v1

    if-lt v3, v4, :cond_33

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2c
    array-length v4, v1

    if-lt v3, v4, :cond_34

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0xc

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, -0x26

    aput v15, v2, v14

    const/16 v14, 0x3473

    aput v14, v2, v13

    const/16 v13, 0x4358

    aput v13, v2, v12

    const/16 v12, 0x6d26

    aput v12, v2, v11

    const/16 v11, 0x5a4d

    aput v11, v2, v10

    const/16 v10, 0x7d35

    aput v10, v2, v9

    const/16 v9, 0x3f09

    aput v9, v2, v8

    const/16 v8, 0x701f

    aput v8, v2, v7

    const/16 v7, -0x54eb

    aput v7, v2, v6

    const/16 v6, -0x3a

    aput v6, v2, v5

    const/16 v5, -0x61

    aput v5, v2, v3

    const/16 v3, -0x7e

    aput v3, v2, v1

    const/16 v1, 0xc

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, -0x41

    aput v16, v1, v15

    const/16 v15, 0x3400

    aput v15, v1, v14

    const/16 v14, 0x4334

    aput v14, v1, v13

    const/16 v13, 0x6d43

    aput v13, v1, v12

    const/16 v12, 0x5a6d

    aput v12, v1, v11

    const/16 v11, 0x7d5a

    aput v11, v1, v10

    const/16 v10, 0x3f7d

    aput v10, v1, v9

    const/16 v9, 0x703f

    aput v9, v1, v8

    const/16 v8, -0x5490

    aput v8, v1, v7

    const/16 v7, -0x55

    aput v7, v1, v6

    const/16 v6, -0x10

    aput v6, v1, v5

    const/16 v5, -0x1f

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2d
    array-length v5, v1

    if-lt v3, v5, :cond_35

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2e
    array-length v5, v1

    if-lt v3, v5, :cond_36

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->wrongPasswordcount:I

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->wrongPasswordcount:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mStatusMessageText:Landroid/widget/TextView;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->wrong_password_message:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mStatusMessageText:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x1060018

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->setRequestPassword(Z)V

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->wrongPasswordcount:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_32

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->isDeviceSignInSamsungAccount(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_32

    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mIsDialogShown:Z

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->showSamsungAccountPopup()V

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->wrongPasswordcount:I

    :cond_32
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mEditTextPinCode:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x1

    goto/16 :goto_1d

    :cond_33
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2b

    :cond_34
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2c

    :cond_35
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2d

    :cond_36
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2e

    :cond_37
    const/4 v1, 0x0

    goto/16 :goto_1d

    :catch_1
    move-exception v1

    goto/16 :goto_26
.end method

.method protected onPause()V
    .locals 26

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mOutStateBundle:Landroid/os/Bundle;

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mOutStateBundle:Landroid/os/Bundle;

    invoke-virtual {v1}, Landroid/os/Bundle;->clear()V

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mOutStateBundle:Landroid/os/Bundle;

    :goto_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mOutStateBundle:Landroid/os/Bundle;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mOutStateBundle:Landroid/os/Bundle;

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, -0x5ba6

    aput v11, v2, v10

    const/16 v10, -0x2a

    aput v10, v2, v9

    const/16 v9, -0x4af2

    aput v9, v2, v8

    const/16 v8, -0x26

    aput v8, v2, v7

    const/16 v7, -0x16

    aput v7, v2, v6

    const/16 v6, 0x6e56

    aput v6, v2, v5

    const/16 v5, 0x140b

    aput v5, v2, v3

    const/16 v3, -0x5081

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, -0x5bc2

    aput v12, v1, v11

    const/16 v11, -0x5c

    aput v11, v1, v10

    const/16 v10, -0x4a91

    aput v10, v1, v9

    const/16 v9, -0x4b

    aput v9, v1, v8

    const/16 v8, -0x78

    aput v8, v1, v7

    const/16 v7, 0x6e2f

    aput v7, v1, v6

    const/16 v6, 0x146e

    aput v6, v1, v5

    const/16 v5, -0x50ec

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_1

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mIsKeyBoardShown:Z

    invoke-virtual {v4, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, -0x52

    aput v10, v2, v9

    const/16 v9, -0x50e9

    aput v9, v2, v8

    const/16 v8, -0x3d

    aput v8, v2, v7

    const/16 v7, 0xe6d

    aput v7, v2, v6

    const/16 v6, 0x1e6b

    aput v6, v2, v5

    const/16 v5, -0x20aa

    aput v5, v2, v4

    const/4 v4, -0x1

    aput v4, v2, v3

    const/16 v3, -0x33

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, -0x3a

    aput v11, v1, v10

    const/16 v10, -0x509d

    aput v10, v1, v9

    const/16 v9, -0x51

    aput v9, v1, v8

    const/16 v8, 0xe0c

    aput v8, v1, v7

    const/16 v7, 0x1e0e

    aput v7, v1, v6

    const/16 v6, -0x20e2

    aput v6, v1, v5

    const/16 v5, -0x21

    aput v5, v1, v4

    const/16 v4, -0x62

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_3
    array-length v4, v1

    if-lt v3, v4, :cond_3

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_4
    array-length v4, v1

    if-lt v3, v4, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x14

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x12

    aput v24, v2, v23

    const/16 v23, 0x3673

    aput v23, v2, v22

    const/16 v22, -0x32bd

    aput v22, v2, v21

    const/16 v21, -0x54

    aput v21, v2, v20

    const/16 v20, -0x55d7

    aput v20, v2, v19

    const/16 v19, -0x3c

    aput v19, v2, v18

    const/16 v18, -0x1d

    aput v18, v2, v17

    const/16 v17, -0x1a

    aput v17, v2, v16

    const/16 v16, -0x62

    aput v16, v2, v15

    const/16 v15, 0x794e

    aput v15, v2, v14

    const/16 v14, -0x2dea

    aput v14, v2, v13

    const/16 v13, -0x4f

    aput v13, v2, v12

    const/16 v12, -0x5b

    aput v12, v2, v11

    const/16 v11, 0x5237

    aput v11, v2, v10

    const/16 v10, -0x8fe

    aput v10, v2, v9

    const/16 v9, -0x7d

    aput v9, v2, v8

    const/16 v8, -0x5a2

    aput v8, v2, v7

    const/16 v7, -0x76

    aput v7, v2, v6

    const/16 v6, 0x7d57

    aput v6, v2, v3

    const/16 v3, 0x7a34

    aput v3, v2, v1

    const/16 v1, 0x14

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x77

    aput v25, v1, v24

    const/16 v24, 0x3600

    aput v24, v1, v23

    const/16 v23, -0x32ca

    aput v23, v1, v22

    const/16 v22, -0x33

    aput v22, v1, v21

    const/16 v21, -0x5587

    aput v21, v1, v20

    const/16 v20, -0x56

    aput v20, v1, v19

    const/16 v19, -0x74

    aput v19, v1, v18

    const/16 v18, -0x3a

    aput v18, v1, v17

    const/16 v17, -0x5

    aput v17, v1, v16

    const/16 v16, 0x792a

    aput v16, v1, v15

    const/16 v15, -0x2d87

    aput v15, v1, v14

    const/16 v14, -0x2e

    aput v14, v1, v13

    const/16 v13, -0x35

    aput v13, v1, v12

    const/16 v12, 0x525e

    aput v12, v1, v11

    const/16 v11, -0x8ae

    aput v11, v1, v10

    const/16 v10, -0x9

    aput v10, v1, v9

    const/16 v9, -0x5d5

    aput v9, v1, v8

    const/4 v8, -0x6

    aput v8, v1, v7

    const/16 v7, 0x7d39

    aput v7, v1, v6

    const/16 v6, 0x7a7d

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_5
    array-length v6, v1

    if-lt v3, v6, :cond_5

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_6
    array-length v6, v1

    if-lt v3, v6, :cond_6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->isEnteredWithCorrectPasscode:Z

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    move-result-object v4

    const/16 v1, 0x10

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, -0xd

    aput v19, v2, v18

    const/16 v18, -0x2f

    aput v18, v2, v17

    const/16 v17, -0x4c

    aput v17, v2, v16

    const/16 v16, -0x46

    aput v16, v2, v15

    const/16 v15, -0x49

    aput v15, v2, v14

    const/4 v14, -0x4

    aput v14, v2, v13

    const/16 v13, -0x1b

    aput v13, v2, v12

    const/16 v12, -0x16

    aput v12, v2, v11

    const/16 v11, -0x6ec3

    aput v11, v2, v10

    const/16 v10, -0x1b

    aput v10, v2, v9

    const/16 v9, -0x22

    aput v9, v2, v8

    const/16 v8, -0x1cb4

    aput v8, v2, v7

    const/16 v7, -0x6a

    aput v7, v2, v6

    const/16 v6, 0x7e07

    aput v6, v2, v5

    const/16 v5, -0x3e5

    aput v5, v2, v3

    const/16 v3, -0x72

    aput v3, v2, v1

    const/16 v1, 0x10

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, -0x69

    aput v20, v1, v19

    const/16 v19, -0x5d

    aput v19, v1, v18

    const/16 v18, -0x25

    aput v18, v1, v17

    const/16 v17, -0x33

    aput v17, v1, v16

    const/16 v16, -0x3c

    aput v16, v1, v15

    const/16 v15, -0x71

    aput v15, v1, v14

    const/16 v14, -0x7c

    aput v14, v1, v13

    const/16 v13, -0x66

    aput v13, v1, v12

    const/16 v12, -0x6e9e

    aput v12, v1, v11

    const/16 v11, -0x6f

    aput v11, v1, v10

    const/16 v10, -0x53

    aput v10, v1, v9

    const/16 v9, -0x1cd7

    aput v9, v1, v8

    const/16 v8, -0x1d

    aput v8, v1, v7

    const/16 v7, 0x7e76

    aput v7, v1, v6

    const/16 v6, -0x382

    aput v6, v1, v5

    const/4 v5, -0x4

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_7

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_8
    array-length v5, v1

    if-lt v3, v5, :cond_8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v4, v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->saveBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;Z)V

    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->unregisterSipAndRecentKeyReceiver()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->hideHeightKeyboard()V

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mIsRecentKeyPressed:Z

    invoke-super/range {p0 .. p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V

    return-void

    :cond_1
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_3
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_4
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_5
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_6
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_7
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_8
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :catch_0
    move-exception v1

    goto/16 :goto_0
.end method

.method protected onResume()V
    .locals 14

    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x4

    const/4 v10, 0x1

    const/4 v3, 0x0

    const/16 v0, 0xb

    new-array v1, v0, [I

    const/4 v0, 0x5

    const/4 v2, 0x6

    const/4 v4, 0x7

    const/16 v5, 0x8

    const/16 v6, 0x9

    const/16 v7, 0xa

    const/16 v8, -0x79

    aput v8, v1, v7

    const/16 v7, 0x1b5a

    aput v7, v1, v6

    const/16 v6, 0x5c7f

    aput v6, v1, v5

    const/16 v5, 0x762e

    aput v5, v1, v4

    const/16 v4, 0x1d17

    aput v4, v1, v2

    const/16 v2, 0x2c72

    aput v2, v1, v0

    const/16 v0, 0x2a4e

    aput v0, v1, v11

    const/16 v0, -0x7da6

    aput v0, v1, v13

    const/16 v0, -0x15

    aput v0, v1, v12

    const/16 v0, -0x3db2

    aput v0, v1, v10

    const/16 v0, -0x5f

    aput v0, v1, v3

    const/16 v0, 0xb

    new-array v0, v0, [I

    const/4 v2, 0x5

    const/4 v4, 0x6

    const/4 v5, 0x7

    const/16 v6, 0x8

    const/16 v7, 0x9

    const/16 v8, 0xa

    const/4 v9, -0x1

    aput v9, v0, v8

    const/16 v8, 0x1b1f

    aput v8, v0, v7

    const/16 v7, 0x5c1b

    aput v7, v0, v6

    const/16 v6, 0x765c

    aput v6, v0, v5

    const/16 v5, 0x1d76

    aput v5, v0, v4

    const/16 v4, 0x2c1d

    aput v4, v0, v2

    const/16 v2, 0x2a2c

    aput v2, v0, v11

    const/16 v2, -0x7dd6

    aput v2, v0, v13

    const/16 v2, -0x7e

    aput v2, v0, v12

    const/16 v2, -0x3dde

    aput v2, v0, v10

    const/16 v2, -0x3e

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v4, v0

    if-lt v2, v4, :cond_1

    array-length v0, v1

    new-array v0, v0, [C

    move v2, v3

    :goto_1
    array-length v4, v0

    if-lt v2, v4, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/sec/clipboard/ClipboardExManager;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    invoke-virtual {v0}, Landroid/sec/clipboard/ClipboardExManager;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/sec/clipboard/ClipboardExManager;->dismissUIDataDialog()V

    :cond_0
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mOutStateBundle:Landroid/os/Bundle;

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mOutStateBundle:Landroid/os/Bundle;

    const/16 v0, 0x8

    new-array v1, v0, [I

    const/4 v0, 0x5

    const/4 v2, 0x6

    const/4 v5, 0x7

    const/16 v6, -0x3493

    aput v6, v1, v5

    const/16 v5, -0x47

    aput v5, v1, v2

    const/16 v2, -0x3d81

    aput v2, v1, v0

    const/16 v0, -0x53

    aput v0, v1, v11

    const/16 v0, 0x4f0f

    aput v0, v1, v13

    const/16 v0, -0x5cca

    aput v0, v1, v12

    const/16 v0, -0x3a

    aput v0, v1, v10

    const/16 v0, -0x53

    aput v0, v1, v3

    const/16 v0, 0x8

    new-array v0, v0, [I

    const/4 v2, 0x5

    const/4 v5, 0x6

    const/4 v6, 0x7

    const/16 v7, -0x34f7

    aput v7, v0, v6

    const/16 v6, -0x35

    aput v6, v0, v5

    const/16 v5, -0x3de2

    aput v5, v0, v2

    const/16 v2, -0x3e

    aput v2, v0, v11

    const/16 v2, 0x4f6d

    aput v2, v0, v13

    const/16 v2, -0x5cb1

    aput v2, v0, v12

    const/16 v2, -0x5d

    aput v2, v0, v10

    const/16 v2, -0x3a

    aput v2, v0, v3

    move v2, v3

    :goto_3
    array-length v5, v0

    if-lt v2, v5, :cond_3

    array-length v0, v1

    new-array v0, v0, [C

    move v2, v3

    :goto_4
    array-length v5, v0

    if-lt v2, v5, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mKeyBoardStatus:Z

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mKeyBoardStatus:Z

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v12}, Landroid/view/Window;->setSoftInputMode(I)V

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mIsKeyBoardShown:Z

    :goto_5
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mIsDialogShown:Z

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->toggleKeyBoard()V

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->registerSipAndRecentKeyReceiver()V

    return-void

    :cond_1
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_2
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    :cond_3
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_4
    aget v5, v1, v2

    int-to-char v5, v5

    aput-char v5, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mEditTextPinCode:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/view/Window;->setSoftInputMode(I)V

    iput-boolean v10, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mIsKeyBoardShown:Z

    goto :goto_5

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mEditTextPinCode:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/view/Window;->setSoftInputMode(I)V

    iput-boolean v10, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mIsKeyBoardShown:Z

    goto :goto_5

    :catch_1
    move-exception v0

    goto/16 :goto_2
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 12

    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v3, 0x0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mOutStateBundle:Landroid/os/Bundle;

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mOutStateBundle:Landroid/os/Bundle;

    const/16 v0, 0x8

    new-array v1, v0, [I

    const/4 v0, 0x5

    const/4 v2, 0x6

    const/4 v5, 0x7

    const/16 v6, -0x28a

    aput v6, v1, v5

    const/16 v5, -0x71

    aput v5, v1, v2

    const/16 v2, 0x1a0d

    aput v2, v1, v0

    const/16 v0, 0x4775

    aput v0, v1, v11

    const/16 v0, 0x5f25

    aput v0, v1, v10

    const/16 v0, -0x59da

    aput v0, v1, v9

    const/16 v0, -0x3d

    aput v0, v1, v8

    const/16 v0, -0x639e

    aput v0, v1, v3

    const/16 v0, 0x8

    new-array v0, v0, [I

    const/4 v2, 0x5

    const/4 v5, 0x6

    const/4 v6, 0x7

    const/16 v7, -0x2ee

    aput v7, v0, v6

    const/4 v6, -0x3

    aput v6, v0, v5

    const/16 v5, 0x1a6c

    aput v5, v0, v2

    const/16 v2, 0x471a

    aput v2, v0, v11

    const/16 v2, 0x5f47

    aput v2, v0, v10

    const/16 v2, -0x59a1

    aput v2, v0, v9

    const/16 v2, -0x5a

    aput v2, v0, v8

    const/16 v2, -0x63f7

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v5, v0

    if-lt v2, v5, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    move v2, v3

    :goto_1
    array-length v5, v0

    if-lt v2, v5, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_2
    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x5

    const/4 v4, 0x6

    const/4 v5, 0x7

    const/16 v6, -0x31d1

    aput v6, v2, v5

    const/16 v5, -0x44

    aput v5, v2, v4

    const/16 v4, -0x3be6

    aput v4, v2, v1

    const/16 v1, -0x55

    aput v1, v2, v11

    const/16 v1, -0x2e

    aput v1, v2, v10

    const/16 v1, -0x5f

    aput v1, v2, v9

    const/16 v1, -0x61

    aput v1, v2, v8

    const/16 v1, 0x2262

    aput v1, v2, v3

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v4, 0x5

    const/4 v5, 0x6

    const/4 v6, 0x7

    const/16 v7, -0x31b5

    aput v7, v1, v6

    const/16 v6, -0x32

    aput v6, v1, v5

    const/16 v5, -0x3b85

    aput v5, v1, v4

    const/16 v4, -0x3c

    aput v4, v1, v11

    const/16 v4, -0x50

    aput v4, v1, v10

    const/16 v4, -0x28

    aput v4, v1, v9

    const/4 v4, -0x6

    aput v4, v1, v8

    const/16 v4, 0x2209

    aput v4, v1, v3

    move v4, v3

    :goto_3
    array-length v5, v1

    if-lt v4, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    :goto_4
    array-length v4, v1

    if-lt v3, v4, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void

    :cond_0
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_1
    aget v5, v1, v2

    int-to-char v5, v5

    aput-char v5, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    :cond_2
    aget v5, v1, v4

    aget v6, v2, v4

    xor-int/2addr v5, v6

    aput v5, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_3
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :catch_0
    move-exception v1

    goto/16 :goto_2
.end method

.method protected onStart()V
    .locals 0

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStop()V

    return-void
.end method
