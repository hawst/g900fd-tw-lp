.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$12;
.super Ljava/lang/Object;
.source "ExerciseProMapActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V
    .locals 0

    .prologue
    .line 806
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUpdatePhotoList(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$PhotoMarkerOptions;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 809
    .local p1, "MarkOptionList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$PhotoMarkerOptions;>;"
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 810
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapMarkerList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$2700(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 811
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/google/android/gms/maps/GoogleMap;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/maps/GoogleMap;->clear()V

    .line 812
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    const-wide/16 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mLastQueryTime:J
    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$2802(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;J)J

    .line 813
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->playMapFromCacheDB(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$2200(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Z)V

    .line 814
    const/4 v0, 0x0

    .line 815
    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapPhotoMarker:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$2900(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;->getSize()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 816
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapMarkerList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$2700(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/google/android/gms/maps/GoogleMap;

    move-result-object v3

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$PhotoMarkerOptions;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$PhotoMarkerOptions;->getMarkerOption()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v3, v1}, Lcom/google/android/gms/maps/GoogleMap;->addMarker(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/Marker;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 817
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 819
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getHeartRateData()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$3000(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V

    .line 820
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->addHRMMarkerToMap()V

    .line 822
    .end local v0    # "i":I
    :cond_1
    return-void
.end method
