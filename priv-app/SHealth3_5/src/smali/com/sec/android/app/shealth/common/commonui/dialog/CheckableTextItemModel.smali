.class public Lcom/sec/android/app/shealth/common/commonui/dialog/CheckableTextItemModel;
.super Lcom/sec/android/app/shealth/common/commonui/dialog/TextItemModel;
.source "CheckableTextItemModel.java"


# instance fields
.field private isChecked:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "itemText"    # Ljava/lang/String;
    .param p2, "itemDescription"    # Ljava/lang/String;
    .param p3, "isChecked"    # Z

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/dialog/TextItemModel;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    iput-boolean p3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/CheckableTextItemModel;->isChecked:Z

    .line 27
    return-void
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/CheckableTextItemModel;->isChecked:Z

    return v0
.end method
