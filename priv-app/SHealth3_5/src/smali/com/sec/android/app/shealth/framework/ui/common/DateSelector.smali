.class public Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
.super Landroid/widget/LinearLayout;

# interfaces
.implements Lcom/sec/android/app/shealth/framework/ui/calendar/OnPeriodSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/ui/common/DateSelector$4;
    }
.end annotation


# static fields
.field private static REQUEST_CODE_CALENDAR:I

.field private static final tempCal:Ljava/util/Calendar;


# instance fields
.field private mCallback:Lcom/sec/android/app/shealth/framework/ui/common/IDateChangeCallback;

.field private mContext:Landroid/content/Context;

.field private mDateSelectorView:Landroid/view/View;

.field private mDateSwitcher:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;

.field private mDateformat:Ljava/text/SimpleDateFormat;

.field private mLeftArrow:Landroid/widget/LinearLayout;

.field private mLeftArrowBtn:Landroid/widget/ImageButton;

.field private mNextDate:Ljava/util/Date;

.field private mPrevDate:Ljava/util/Date;

.field private mPrevSelectedDate:Ljava/util/Date;

.field private mRightArrow:Landroid/widget/LinearLayout;

.field private mRightArrowBtn:Landroid/widget/ImageButton;

.field private mSelecteddate:Ljava/util/Date;

.field private mSystemDate:Ljava/util/Date;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->tempCal:Ljava/util/Calendar;

    const/16 v0, 0x64

    sput v0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->REQUEST_CODE_CALENDAR:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mDateformat:Ljava/text/SimpleDateFormat;

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mDateformat:Ljava/text/SimpleDateFormat;

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mDateformat:Ljava/text/SimpleDateFormat;

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mContext:Landroid/content/Context;

    return-void
.end method

.method private InitializeDate()V
    .locals 4

    new-instance v0, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mSystemDate:Ljava/util/Date;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mPrevSelectedDate:Ljava/util/Date;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mSelecteddate:Ljava/util/Date;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setSelecteddate(Ljava/util/Date;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mDateSwitcher:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mDateSwitcher:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mDateSwitcher:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_button:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mLeftArrow:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mRightArrow:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private getDefaultDateText(Ljava/util/Date;)Ljava/lang/String;
    .locals 7

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    invoke-virtual {v0}, Ljava/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    const-string v4, ""

    const-string v5, ""

    const-string v5, "ko"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string/jumbo v5, "zh"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "ja"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->day:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/sec/android/app/shealth/framework/ui/R$string;->year:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/sec/android/app/shealth/framework/ui/R$string;->week_of_day:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "<font color=#414141>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "yyyy"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " MMM "

    invoke-static {v4, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "</font><b>"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</b> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "<font color=#414141>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "E"

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</font>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    const-string/jumbo v5, "nb"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<font color=#414141>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "E"

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    invoke-static {v2, v5, v6}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</font><b>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "d. "

    invoke-static {v2, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</b><font color=#414141>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " MMM"

    invoke-static {v2, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " yyyy"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</font>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector$4;->$SwitchMap$com$sec$android$app$shealth$common$utils$calendar$DateFormatType:[I

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getCurrentSystemDateFormatType()Lcom/sec/android/app/shealth/common/utils/calendar/DateFormatType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/calendar/DateFormatType;->ordinal()I

    move-result v5

    aget v2, v2, v5

    packed-switch v2, :pswitch_data_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "<font color=#414141>"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "yyyy"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " MMM "

    invoke-static {v4, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "</font><b>"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</b><font color=#414141>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " - d"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</font>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<font color=#414141>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "E"

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    invoke-static {v2, v5, v6}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</font><b>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "d"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</b><font color=#414141>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " MMM"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " yyyy"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</font>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<font color=#414141>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "E"

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    invoke-static {v2, v5, v6}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "MMM "

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    invoke-static {v2, v5, v6}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</font><b>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "d"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</b><font color=#414141>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " yyyy"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</font>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<font color=#414141>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "yyyy"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " MMM "

    invoke-static {v2, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</font><b>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "d"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</b><font color=#414141>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "E"

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</font>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<font color=#414141>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "yyyy"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</font><b>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " d"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</b><font color=#414141>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " MMM "

    invoke-static {v2, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "E"

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</font>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getEndOfDay(J)J
    .locals 4

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->tempCal:Ljava/util/Calendar;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xb

    const/16 v3, 0x17

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xc

    const/16 v3, 0x3b

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xd

    const/16 v3, 0x3b

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xe

    const/16 v3, 0x3e7

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getStartOfDay(J)J
    .locals 4

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->tempCal:Ljava/util/Calendar;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xb

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xc

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xd

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xe

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private initialize()V
    .locals 2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$layout;->datebar:I

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->date_layout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mDateSelectorView:Landroid/view/View;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setupDateBar()V

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->InitializeDate()V

    return-void
.end method

.method private setupDateBar()V
    .locals 4

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->date_switcher:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mDateSwitcher:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->layout_left_arrow:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mLeftArrow:Landroid/widget/LinearLayout;

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->layout_right_arrow:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mRightArrow:Landroid/widget/LinearLayout;

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->left_arrow:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mLeftArrowBtn:Landroid/widget/ImageButton;

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->right_arrow:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mRightArrowBtn:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mLeftArrow:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->previous:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_button:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mRightArrow:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->next:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_button:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mDateSwitcher:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->setAnimeListener(Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$AnimeListener;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mLeftArrow:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector$2;-><init>(Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mRightArrow:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector$3;-><init>(Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public getDateSelectorView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mDateSelectorView:Landroid/view/View;

    return-object v0
.end method

.method public getNextDate()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mNextDate:Ljava/util/Date;

    return-object v0
.end method

.method public getPrevDate()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mPrevDate:Ljava/util/Date;

    return-object v0
.end method

.method public getPrevSelectedDate()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mPrevSelectedDate:Ljava/util/Date;

    return-object v0
.end method

.method public getPrevSelecteddate()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mPrevSelectedDate:Ljava/util/Date;

    return-object v0
.end method

.method public getSelecteddate()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mSelecteddate:Ljava/util/Date;

    return-object v0
.end method

.method public getSystemDate()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mSystemDate:Ljava/util/Date;

    return-object v0
.end method

.method public isLeftArrowEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mLeftArrow:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isEnabled()Z

    move-result v0

    return v0
.end method

.method public isRightArrowEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mRightArrow:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isEnabled()Z

    move-result v0

    return v0
.end method

.method public moveLeft()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mSelecteddate:Ljava/util/Date;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mPrevSelectedDate:Ljava/util/Date;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getPrevDate()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setSelecteddate(Ljava/util/Date;)V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mCallback:Lcom/sec/android/app/shealth/framework/ui/common/IDateChangeCallback;

    invoke-interface {v1, v0}, Lcom/sec/android/app/shealth/framework/ui/common/IDateChangeCallback;->onDateChanged(Ljava/util/Date;)V

    :cond_0
    return-void
.end method

.method public moveRight()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mSelecteddate:Ljava/util/Date;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mPrevSelectedDate:Ljava/util/Date;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getNextDate()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setSelecteddate(Ljava/util/Date;)V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mCallback:Lcom/sec/android/app/shealth/framework/ui/common/IDateChangeCallback;

    invoke-interface {v1, v0}, Lcom/sec/android/app/shealth/framework/ui/common/IDateChangeCallback;->onDateChanged(Ljava/util/Date;)V

    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->initialize()V

    return-void
.end method

.method public onPeriodSelected(JIZ)V
    .locals 2

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getDateInstance(J)Ljava/sql/Date;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setSelecteddate(Ljava/util/Date;)V

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getDateInstance(J)Ljava/sql/Date;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setText(Ljava/util/Date;)V

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mCallback:Lcom/sec/android/app/shealth/framework/ui/common/IDateChangeCallback;

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getDateInstance(J)Ljava/sql/Date;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/IDateChangeCallback;->onDateChanged(Ljava/util/Date;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public registerDateChangeListener(Lcom/sec/android/app/shealth/framework/ui/common/IDateChangeCallback;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mCallback:Lcom/sec/android/app/shealth/framework/ui/common/IDateChangeCallback;

    return-void
.end method

.method public setArrowVisible(Z)V
    .locals 4

    const/4 v3, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mLeftArrow:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mRightArrow:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mLeftArrow:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mRightArrow:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mLeftArrow:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mRightArrow:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mLeftArrow:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mRightArrow:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    goto :goto_0
.end method

.method public setBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mDateSelectorView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setDateFormat(Ljava/text/SimpleDateFormat;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mDateformat:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public setLeftArrowEnabled(Z)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mLeftArrow:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mLeftArrowBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mLeftArrow:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mLeftArrowBtn:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    return-void
.end method

.method public setLeftOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mLeftArrow:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setNextDate(Ljava/util/Date;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mNextDate:Ljava/util/Date;

    return-void
.end method

.method public setPrevDate(Ljava/util/Date;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mPrevDate:Ljava/util/Date;

    return-void
.end method

.method public setPrevSelectedDate(Ljava/util/Date;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mPrevSelectedDate:Ljava/util/Date;

    return-void
.end method

.method public setRightArrowEnabled(Z)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mRightArrow:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mRightArrowBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mRightArrow:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mRightArrowBtn:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    return-void
.end method

.method public setRightOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mRightArrow:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setSelecteddate(Ljava/util/Date;)V
    .locals 4

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mSelecteddate:Ljava/util/Date;

    invoke-virtual {p1, v0}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mDateSwitcher:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$Mode;->NULL:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$Mode;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->setAnimationMode(Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$Mode;)V

    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mSelecteddate:Ljava/util/Date;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mDateformat:Ljava/text/SimpleDateFormat;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mDateSwitcher:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mDateformat:Ljava/text/SimpleDateFormat;

    invoke-virtual {v1, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mDateSwitcher:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mDateSwitcher:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mDateSwitcher:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_button:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mSelecteddate:Ljava/util/Date;

    invoke-virtual {p1, v0}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mDateSwitcher:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$Mode;->NULL:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$Mode;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->setAnimationMode(Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$Mode;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mDateSwitcher:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getDefaultDateText(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public setSystemDate(Ljava/util/Date;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mSystemDate:Ljava/util/Date;

    return-void
.end method

.method public setText(Ljava/util/Date;)V
    .locals 4

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mDateformat:Ljava/text/SimpleDateFormat;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mDateSwitcher:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mDateformat:Ljava/text/SimpleDateFormat;

    invoke-virtual {v1, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mDateSwitcher:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mDateSwitcher:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mDateSwitcher:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_button:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->mDateSwitcher:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getDefaultDateText(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
