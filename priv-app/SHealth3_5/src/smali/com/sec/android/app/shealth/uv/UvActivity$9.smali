.class Lcom/sec/android/app/shealth/uv/UvActivity$9;
.super Ljava/lang/Object;
.source "UvActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/uv/UvActivity;->showDeleteDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/UvActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/UvActivity;)V
    .locals 0

    .prologue
    .line 451
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvActivity$9;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    const/4 v2, 0x0

    .line 455
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$9;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$400(Lcom/sec/android/app/shealth/uv/UvActivity;)Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->deleteDailyData()V

    .line 456
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$9;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$400(Lcom/sec/android/app/shealth/uv/UvActivity;)Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mState:Lcom/sec/android/app/shealth/uv/state/UvState;

    const/16 v1, -0x3e9

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/uv/state/UvState;->updateNextState(II)V

    .line 457
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$9;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$400(Lcom/sec/android/app/shealth/uv/UvActivity;)Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->updateUvDataView(Z)V

    .line 458
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$9;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # setter for: Lcom/sec/android/app/shealth/uv/UvActivity;->isShownPopup:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$702(Lcom/sec/android/app/shealth/uv/UvActivity;Z)Z

    .line 459
    return-void
.end method
