.class public Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;
.super Landroid/widget/LinearLayout;
.source "StateSummaryBarType2.java"


# instance fields
.field private ivPolygon:Landroid/widget/ImageView;

.field private llBar:Landroid/widget/LinearLayout;

.field private mStateBar:Lcom/sec/android/app/shealth/heartrate/barview/StateBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    const v0, 0x7f03020d

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 23
    const v0, 0x7f080935

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;->ivPolygon:Landroid/widget/ImageView;

    .line 24
    const v0, 0x7f0804fa

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;->llBar:Landroid/widget/LinearLayout;

    .line 25
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/barview/StateBar;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/heartrate/barview/StateBar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;->mStateBar:Lcom/sec/android/app/shealth/heartrate/barview/StateBar;

    .line 26
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;->llBar:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;->mStateBar:Lcom/sec/android/app/shealth/heartrate/barview/StateBar;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 27
    return-void
.end method

.method private convertDptoPx(F)I
    .locals 4
    .param p1, "dp"    # F

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 70
    .local v2, "res":Landroid/content/res/Resources;
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 71
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    iget v3, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float v1, p1, v3

    .line 72
    .local v1, "px":F
    float-to-int v3, v1

    return v3
.end method


# virtual methods
.method public getStateColor()I
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;->mStateBar:Lcom/sec/android/app/shealth/heartrate/barview/StateBar;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/barview/StateBar;->getStateBarColor()I

    move-result v0

    return v0
.end method

.method public moveToPolygon(I[I[I)V
    .locals 9
    .param p1, "bpm"    # I
    .param p2, "avgRange"    # [I
    .param p3, "pecentile"    # [I

    .prologue
    const/high16 v3, 0x40e00000    # 7.0f

    const-wide v4, 0x4046800000000000L    # 45.0

    const v8, 0x7f020635

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 30
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;->mStateBar:Lcom/sec/android/app/shealth/heartrate/barview/StateBar;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/barview/StateBar;->invalidate()V

    .line 31
    const/4 v0, 0x0

    .line 33
    .local v0, "calculatePosition":F
    aget v2, p2, v6

    if-ge p1, v2, :cond_3

    .line 34
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;->convertDptoPx(F)I

    move-result v2

    int-to-float v0, v2

    .line 35
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;->ivPolygon:Landroid/widget/ImageView;

    const v3, 0x7f02052d

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 53
    :cond_0
    :goto_0
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 54
    .local v1, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 55
    const/4 v2, 0x3

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 56
    aget v2, p2, v6

    if-lt p1, v2, :cond_1

    aget v2, p2, v7

    if-le p1, v2, :cond_2

    .line 57
    :cond_1
    const/high16 v2, 0x40000000    # 2.0f

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;->convertDptoPx(F)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 59
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;->ivPolygon:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 61
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;->mStateBar:Lcom/sec/android/app/shealth/heartrate/barview/StateBar;

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/heartrate/barview/StateBar;->setStateBarColor(I)V

    .line 62
    return-void

    .line 36
    .end local v1    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :cond_3
    aget v2, p2, v7

    if-le p1, v2, :cond_4

    .line 37
    const/high16 v2, 0x43260000    # 166.0f

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;->convertDptoPx(F)I

    move-result v2

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;->convertDptoPx(F)I

    move-result v3

    add-int/2addr v2, v3

    int-to-float v0, v2

    .line 38
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;->ivPolygon:Landroid/widget/ImageView;

    const v3, 0x7f02052f

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 39
    :cond_4
    aget v2, p3, v6

    if-ge p1, v2, :cond_5

    .line 40
    aget v2, p3, v6

    aget v3, p2, v6

    sub-int/2addr v2, v3

    int-to-double v2, v2

    div-double v2, v4, v2

    double-to-float v2, v2

    aget v3, p2, v6

    sub-int v3, p1, v3

    int-to-float v3, v3

    mul-float v0, v2, v3

    .line 41
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;->convertDptoPx(F)I

    move-result v2

    int-to-float v0, v2

    .line 42
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;->ivPolygon:Landroid/widget/ImageView;

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 43
    :cond_5
    aget v2, p3, v7

    if-gt p1, v2, :cond_6

    aget v2, p3, v6

    if-lt p1, v2, :cond_6

    .line 44
    const-wide v2, 0x4056800000000000L    # 90.0

    aget v4, p3, v7

    aget v5, p3, v6

    sub-int/2addr v4, v5

    int-to-double v4, v4

    div-double/2addr v2, v4

    double-to-float v2, v2

    aget v3, p3, v6

    sub-int v3, p1, v3

    int-to-float v3, v3

    mul-float v0, v2, v3

    .line 45
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;->convertDptoPx(F)I

    move-result v2

    const/high16 v3, 0x42340000    # 45.0f

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;->convertDptoPx(F)I

    move-result v3

    add-int/2addr v2, v3

    int-to-float v0, v2

    .line 46
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;->ivPolygon:Landroid/widget/ImageView;

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 47
    :cond_6
    aget v2, p3, v7

    if-le p1, v2, :cond_0

    .line 48
    aget v2, p2, v7

    aget v3, p3, v7

    sub-int/2addr v2, v3

    int-to-double v2, v2

    div-double v2, v4, v2

    double-to-float v2, v2

    aget v3, p3, v7

    sub-int v3, p1, v3

    int-to-float v3, v3

    mul-float v0, v2, v3

    .line 49
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;->convertDptoPx(F)I

    move-result v2

    const/high16 v3, 0x43070000    # 135.0f

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;->convertDptoPx(F)I

    move-result v3

    add-int/2addr v2, v3

    int-to-float v0, v2

    .line 50
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;->ivPolygon:Landroid/widget/ImageView;

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0
.end method
