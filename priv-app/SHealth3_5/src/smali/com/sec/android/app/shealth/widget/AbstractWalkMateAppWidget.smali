.class public abstract Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;
.super Landroid/appwidget/AppWidgetProvider;
.source "AbstractWalkMateAppWidget.java"


# static fields
.field protected static final EASY_MODE_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.easylauncher"

.field protected static final MODE_GEAR:Ljava/lang/String; = "gear"

.field protected static final MODE_HEALTHY:Ljava/lang/String; = "healthy"

.field protected static final MODE_INACTIVE:Ljava/lang/String; = "inactive"

.field protected static final MODE_NORMAL:Ljava/lang/String; = "normal"

.field protected static final TAG:Ljava/lang/String; = "AbstractWalkMateAppWidget"

.field protected static final TOUCH_WIZ_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.launcher"

.field protected static mWalkingMateDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

.field private static sBitmapMap:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field protected static sWidgets:[Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;


# instance fields
.field protected mGoalStep:I

.field protected mTotalStep:J

.field protected mWearableConnected:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->mWalkingMateDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .line 54
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;

    sput-object v0, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->sWidgets:[Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;

    .line 56
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->sBitmapMap:Ljava/util/WeakHashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->mWearableConnected:Z

    .line 59
    const-string v0, "AbstractWalkMateAppWidget"

    const-string v1, "Constructor called and array initialized"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;Landroid/content/Context;J)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # J

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->loadLatestData(Landroid/content/Context;J)V

    return-void
.end method

.method private declared-synchronized loadLatestData(Landroid/content/Context;J)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "time"    # J

    .prologue
    const/4 v3, 0x0

    .line 92
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v2

    .line 93
    .local v2, "isContentProviderAccessible":Z
    const-string v4, "AbstractWalkMateAppWidget"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isContentProviderAccessible : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    if-eqz v2, :cond_3

    .line 95
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v4

    const/16 v5, 0x2719

    if-eq v4, v5, :cond_0

    .line 96
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->updateFromDB(Z)V

    .line 97
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getTotalStep()I

    move-result v4

    int-to-long v4, v4

    iput-wide v4, p0, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->mTotalStep:J

    .line 98
    invoke-static {p1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getGoal()I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->mGoalStep:I

    .line 99
    invoke-static {p1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateSummaryUtils;->isWearableConnected(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {p1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateSummaryUtils;->isActivityTrackerConnected(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    const/4 v3, 0x1

    :cond_2
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->mWearableConnected:Z

    .line 102
    :cond_3
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    .line 103
    .local v1, "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    new-instance v3, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-direct {v3, p1, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v3}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    .line 105
    .local v0, "appWidgetIds":[I
    invoke-virtual {p0, p1, v1, v0}, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->updateWidget(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    monitor-exit p0

    return-void

    .line 92
    .end local v0    # "appWidgetIds":[I
    .end local v1    # "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    .end local v2    # "isContentProviderAccessible":Z
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public static declared-synchronized updateHomeWidgets(Landroid/content/Context;Z)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mSyncfailed"    # Z

    .prologue
    .line 294
    const-class v3, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;

    monitor-enter v3

    :try_start_0
    const-string v2, "AbstractWalkMateAppWidget"

    const-string/jumbo v4, "updateHomeWidgets"

    invoke-static {v2, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    sget-object v2, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->sWidgets:[Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;

    if-nez v2, :cond_1

    .line 297
    const/4 v2, 0x3

    new-array v2, v2, [Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;

    sput-object v2, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->sWidgets:[Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 314
    :cond_0
    :goto_0
    monitor-exit v3

    return-void

    .line 301
    :cond_1
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 303
    .local v0, "currentTime":J
    sget-object v2, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->sWidgets:[Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    if-eqz v2, :cond_2

    .line 304
    sget-object v2, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->sWidgets:[Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    invoke-direct {v2, p0, v0, v1}, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->loadLatestData(Landroid/content/Context;J)V

    .line 307
    :cond_2
    sget-object v2, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->sWidgets:[Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;

    const/4 v4, 0x1

    aget-object v2, v2, v4

    if-eqz v2, :cond_3

    .line 308
    sget-object v2, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->sWidgets:[Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;

    const/4 v4, 0x1

    aget-object v2, v2, v4

    invoke-direct {v2, p0, v0, v1}, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->loadLatestData(Landroid/content/Context;J)V

    .line 311
    :cond_3
    sget-object v2, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->sWidgets:[Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;

    const/4 v4, 0x2

    aget-object v2, v2, v4

    if-eqz v2, :cond_0

    .line 312
    sget-object v2, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->sWidgets:[Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;

    const/4 v4, 0x2

    aget-object v2, v2, v4

    invoke-direct {v2, p0, v0, v1}, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->loadLatestData(Landroid/content/Context;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 294
    .end local v0    # "currentTime":J
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static updateHomeWidgets(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;Z)V
    .locals 1
    .param p0, "walkingMateDayStepService"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;
    .param p1, "mSyncfailed"    # Z

    .prologue
    .line 317
    sput-object p0, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->mWalkingMateDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .line 318
    sget-object v0, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->mWalkingMateDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->updateHomeWidgets(Landroid/content/Context;Z)V

    .line 319
    return-void
.end method


# virtual methods
.method protected getCurrentMode(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 244
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v0

    .line 246
    .local v0, "activeTimeMonitor":Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v1

    const/16 v2, 0x2719

    if-eq v1, v2, :cond_0

    .line 247
    const-string v1, "gear"

    .line 255
    :goto_0
    return-object v1

    .line 249
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->isHealthyStep()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 250
    const-string v1, "healthy"

    goto :goto_0

    .line 252
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->isInactivetime()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 253
    const-string v1, "inactive"

    goto :goto_0

    .line 255
    :cond_2
    const-string/jumbo v1, "normal"

    goto :goto_0
.end method

.method protected getHomePackageName(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 259
    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.MAIN"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 260
    .local v1, "intent":Landroid/content/Intent;
    const-string v4, "android.intent.category.HOME"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 261
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 262
    .local v2, "packageManager":Landroid/content/pm/PackageManager;
    const-string v0, "com.sec.android.app.launcher"

    .line 263
    .local v0, "homePackageName":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 264
    const/high16 v4, 0x10000

    invoke-virtual {v2, v1, v4}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v3

    .line 265
    .local v3, "resolveInfo":Landroid/content/pm/ResolveInfo;
    if-eqz v3, :cond_0

    .line 266
    iget-object v4, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 270
    .end local v3    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    :cond_0
    const-string v4, "AbstractWalkMateAppWidget"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "PackageName: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    return-object v0
.end method

.method protected getIconBitmap(Landroid/content/Context;IZ)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # I
    .param p3, "isStartWalking"    # Z

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x0

    .line 109
    if-nez p1, :cond_0

    .line 110
    const-string v4, "icon_bitmap"

    const-string v5, "context is null"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    :goto_0
    return-object v1

    .line 114
    :cond_0
    sget-object v4, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->sBitmapMap:Ljava/util/WeakHashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Bitmap;

    .line 115
    .local v3, "source":Landroid/graphics/Bitmap;
    if-nez v3, :cond_1

    .line 116
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 117
    if-eqz v3, :cond_1

    .line 118
    sget-object v4, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->sBitmapMap:Ljava/util/WeakHashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    :cond_1
    if-nez v3, :cond_2

    .line 123
    const-string v4, "icon_bitmap"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "there is not bitmap for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 127
    :cond_2
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 128
    .local v1, "out":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 129
    .local v0, "canvas":Landroid/graphics/Canvas;
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 130
    .local v2, "p":Landroid/graphics/Paint;
    const/4 v4, -0x1

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 131
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 132
    if-eqz p3, :cond_3

    .line 133
    const/16 v4, 0xff

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 137
    :goto_1
    invoke-virtual {v0, v3, v7, v7, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 135
    :cond_3
    const/16 v4, 0x7f

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    goto :goto_1
.end method

.method protected getIconBitmap(Landroid/content/Context;IZI)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # I
    .param p3, "isStartWalking"    # Z
    .param p4, "connectAccessory"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v9, -0x1

    const/4 v8, 0x0

    .line 142
    if-nez p1, :cond_0

    .line 143
    const-string v4, "icon_bitmap"

    const-string v5, "context is null"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    :goto_0
    return-object v1

    .line 147
    :cond_0
    sget-object v4, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->sBitmapMap:Ljava/util/WeakHashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Bitmap;

    .line 148
    .local v3, "source":Landroid/graphics/Bitmap;
    if-nez v3, :cond_1

    .line 149
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 150
    if-eqz v3, :cond_1

    .line 151
    sget-object v4, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->sBitmapMap:Ljava/util/WeakHashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    :cond_1
    if-nez v3, :cond_2

    .line 156
    const-string v4, "icon_bitmap"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "there is not bitmap for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 160
    :cond_2
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 161
    .local v1, "out":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 162
    .local v0, "canvas":Landroid/graphics/Canvas;
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 163
    .local v2, "p":Landroid/graphics/Paint;
    invoke-virtual {v2, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 164
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 165
    if-nez p3, :cond_3

    iget-wide v4, p0, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->mTotalStep:J

    iget v6, p0, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->mGoalStep:I

    int-to-long v6, v6

    cmp-long v4, v4, v6

    if-gez v4, :cond_3

    const/16 v4, 0x2719

    if-eq p4, v4, :cond_4

    .line 166
    :cond_3
    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v9, v4}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 167
    const/16 v4, 0xff

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 172
    :goto_1
    invoke-virtual {v0, v3, v8, v8, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 169
    :cond_4
    const v4, 0x33ffffff

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 170
    const/16 v4, 0x7f

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    goto :goto_1
.end method

.method protected getLastUpdatedTime()Ljava/lang/String;
    .locals 11

    .prologue
    .line 178
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 179
    .local v0, "context":Landroid/content/Context;
    const-string v5, ""

    .line 180
    .local v5, "lastUpdatedTime":Ljava/lang/String;
    const-wide/16 v6, 0x0

    .line 181
    .local v6, "time":J
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    .line 221
    const-string v5, ""

    .line 225
    :goto_0
    const-wide/16 v8, 0x0

    cmp-long v8, v6, v8

    if-lez v8, :cond_6

    .line 226
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    const-string v10, "/"

    invoke-static {v0, v9, v10}, Lcom/sec/android/app/shealth/common/utils/DateFormatHelper;->getSystemDateFormatMMDD(Landroid/content/Context;Ljava/lang/Long;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 227
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 228
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/DateFormatHelper;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v9

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 233
    :goto_1
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 234
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 235
    .local v1, "curTime":J
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    const-string v10, "/"

    invoke-static {v0, v9, v10}, Lcom/sec/android/app/shealth/common/utils/DateFormatHelper;->getSystemDateFormatMMDD(Landroid/content/Context;Ljava/lang/Long;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 236
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 237
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/DateFormatHelper;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v9

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 240
    .end local v1    # "curTime":J
    :cond_0
    return-object v5

    .line 183
    :sswitch_0
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getUpdateTimeOfGearFit()J

    move-result-wide v3

    .line 184
    .local v3, "lastTime":J
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getUpdateTimeOfGear()J

    move-result-wide v8

    cmp-long v8, v3, v8

    if-gez v8, :cond_1

    .line 185
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getUpdateTimeOfGear()J

    move-result-wide v3

    .line 186
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getUpdateTimeOfTizenGear()J

    move-result-wide v8

    cmp-long v8, v3, v8

    if-gez v8, :cond_2

    .line 187
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getUpdateTimeOfTizenGear()J

    move-result-wide v3

    .line 188
    :cond_2
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getUpdateTimeOfGear2()J

    move-result-wide v8

    cmp-long v8, v3, v8

    if-gez v8, :cond_3

    .line 189
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getUpdateTimeOfGear2()J

    move-result-wide v3

    .line 190
    :cond_3
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getUpdateTimeOfGear3()J

    move-result-wide v8

    cmp-long v8, v3, v8

    if-gez v8, :cond_4

    .line 191
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getUpdateTimeOfGear3()J

    move-result-wide v3

    .line 192
    :cond_4
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getUpdateTimeOfGearO()J

    move-result-wide v8

    cmp-long v8, v3, v8

    if-gez v8, :cond_5

    .line 193
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getUpdateTimeOfGearO()J

    move-result-wide v3

    .line 194
    :cond_5
    move-wide v6, v3

    .line 195
    goto/16 :goto_0

    .line 197
    .end local v3    # "lastTime":J
    :sswitch_1
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getUpdateTimeOfGearFit()J

    move-result-wide v6

    .line 198
    goto/16 :goto_0

    .line 200
    :sswitch_2
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getUpdateTimeOfGear()J

    move-result-wide v6

    .line 201
    goto/16 :goto_0

    .line 203
    :sswitch_3
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getUpdateTimeOfTizenGear()J

    move-result-wide v6

    .line 204
    goto/16 :goto_0

    .line 206
    :sswitch_4
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getUpdateTimeOfGear2()J

    move-result-wide v6

    .line 207
    goto/16 :goto_0

    .line 209
    :sswitch_5
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getUpdateTimeOfGear3()J

    move-result-wide v6

    .line 210
    goto/16 :goto_0

    .line 212
    :sswitch_6
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getUpdateTimeOfGearO()J

    move-result-wide v6

    .line 213
    goto/16 :goto_0

    .line 215
    :sswitch_7
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getUpdateTimeOfActivityTracker()J

    move-result-wide v6

    .line 216
    goto/16 :goto_0

    .line 218
    :sswitch_8
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getUpdateTimeOfGear3()J

    move-result-wide v6

    .line 219
    goto/16 :goto_0

    .line 230
    :cond_6
    const-string v5, ""

    goto/16 :goto_1

    .line 181
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_8
        0x2723 -> :sswitch_1
        0x2724 -> :sswitch_2
        0x2726 -> :sswitch_4
        0x2727 -> :sswitch_7
        0x2728 -> :sswitch_3
        0x272e -> :sswitch_5
        0x272f -> :sswitch_0
        0x2730 -> :sswitch_6
    .end sparse-switch
.end method

.method protected getLaunchPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 275
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/shealth/receiver/LaunchWidgetReceiver;

    invoke-direct {v0, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 276
    .local v0, "intentSyncStart":Landroid/content/Intent;
    const-string v2, "com.sec.shealth.broadcast.LAUNCH_APP_WIDGET_ACTIVITY"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 277
    const-string/jumbo v2, "widgetActivityAction"

    const-string v3, "com.sec.shealth.action.PEDOMETER"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 278
    const-string/jumbo v2, "widgetActivityPackage"

    const-string v3, "com.sec.android.app.shealth"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 279
    const-string v2, "isFromWidget"

    const-string v3, "fromAbstractWidget"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 280
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/16 v3, 0x1ca8

    const/high16 v4, 0x8000000

    invoke-static {v2, v3, v0, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 281
    .local v1, "pendingIntent":Landroid/app/PendingIntent;
    return-object v1
.end method

.method protected getSyncPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 285
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;

    invoke-direct {v0, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 286
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "android.intent.action.WEARABLE_DEVICE_SYNC_CHECK"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 287
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/16 v3, 0x1ca8

    const/high16 v4, 0x8000000

    invoke-static {v2, v3, v0, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 288
    .local v1, "pendingIntentSync":Landroid/app/PendingIntent;
    return-object v1
.end method

.method public onDisabled(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 83
    const-string v0, "AbstractWalkMateAppWidget"

    const-string/jumbo v1, "onDisabled"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onDisabled(Landroid/content/Context;)V

    .line 85
    sget-object v0, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->sWidgets:[Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->sWidgets:[Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->sWidgets:[Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    if-nez v0, :cond_0

    .line 86
    const-string v0, "AbstractWalkMateAppWidget"

    const-string v1, "OnDisabled Walk, calling clear Images ..."

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->clearImages()V

    .line 89
    :cond_0
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 64
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 65
    const-string v0, "AbstractWalkMateAppWidget"

    const-string/jumbo v1, "onReceive"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget$1;-><init>(Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 73
    return-void
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .prologue
    .line 77
    invoke-super {p0, p1, p2, p3}, Landroid/appwidget/AppWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 78
    const-string v0, "AbstractWalkMateAppWidget"

    const-string/jumbo v1, "onUpdate"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    return-void
.end method

.method protected abstract updateWidget(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
.end method
