.class public abstract Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;
.super Landroid/os/Handler;
.source "PauseHandler.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field final messageQueueBuffer:Ljava/util/concurrent/LinkedBlockingDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingDeque",
            "<",
            "Landroid/os/Message;",
            ">;"
        }
    .end annotation
.end field

.field private volatile paused:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 23
    new-instance v0, Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->messageQueueBuffer:Ljava/util/concurrent/LinkedBlockingDeque;

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 65
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->paused:Z

    if-eqz v1, :cond_1

    .line 66
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->storeMessage(Landroid/os/Message;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 67
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 68
    .local v0, "msgCopy":Landroid/os/Message;
    invoke-virtual {v0, p1}, Landroid/os/Message;->copyFrom(Landroid/os/Message;)V

    .line 69
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->messageQueueBuffer:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/LinkedBlockingDeque;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 70
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->messageQueueBuffer:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/LinkedBlockingDeque;->addLast(Ljava/lang/Object;)V

    .line 76
    .end local v0    # "msgCopy":Landroid/os/Message;
    :cond_0
    :goto_0
    return-void

    .line 74
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->processMessage(Landroid/os/Message;)V

    goto :goto_0
.end method

.method public final pause()V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->paused:Z

    .line 43
    return-void
.end method

.method protected abstract processMessage(Landroid/os/Message;)V
.end method

.method public final resume()V
    .locals 3

    .prologue
    .line 29
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->TAG:Ljava/lang/String;

    const-string v2, "handler resumed"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 30
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->paused:Z

    .line 31
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->messageQueueBuffer:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v1}, Ljava/util/concurrent/LinkedBlockingDeque;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 33
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->messageQueueBuffer:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v1}, Ljava/util/concurrent/LinkedBlockingDeque;->takeFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Message;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 34
    :catch_0
    move-exception v0

    .line 35
    .local v0, "e":Ljava/lang/InterruptedException;
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 38
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_0
    return-void
.end method

.method protected abstract storeMessage(Landroid/os/Message;)Z
.end method
