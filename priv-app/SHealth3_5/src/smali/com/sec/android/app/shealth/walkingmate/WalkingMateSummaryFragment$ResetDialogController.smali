.class public Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$ResetDialogController;
.super Ljava/lang/Object;
.source "WalkingMateSummaryFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ResetDialogController"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1832
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 6
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    const/4 v5, 0x1

    .line 1837
    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4400()Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1880
    :goto_0
    :pswitch_0
    return-void

    .line 1841
    :cond_0
    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4400()Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v1

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4700(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Ljava/util/Date;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1842
    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4400()Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    # setter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4802(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;Ljava/util/Date;)Ljava/util/Date;

    .line 1845
    :cond_1
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$15;->$SwitchMap$com$sec$android$app$shealth$common$commonui$dialog$DialogButtonType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1847
    :pswitch_1
    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4400()Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v1

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4900(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_2

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1849
    invoke-static {}, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->updateWidgets()V

    .line 1853
    :cond_2
    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4400()Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v1

    # invokes: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$5000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getSelecteddate()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4400()Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->deleteWalkDataInExercise(JLandroid/content/Context;)V

    .line 1855
    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4400()Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v1

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$5100(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_3

    .line 1857
    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4400()Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v1

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$2800(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1858
    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4400()Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v1

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$2800(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->resetData(Z)V

    .line 1862
    :cond_3
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4400()Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09094a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v5}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1865
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.shealth.command.deletetotalstep"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1866
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "selectedDate"

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4400()Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v2

    # invokes: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$5200(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getSelecteddate()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1867
    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4400()Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1868
    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4400()Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v1

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4400()Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v2

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$5300(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->loadLatestData(Ljava/util/Date;)V

    .line 1869
    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4400()Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v1

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4400()Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v2

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->alreadyInitDelay:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$2500(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->updateAnimation(I)V

    .line 1870
    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4400()Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v1

    # invokes: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setPercentValue()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$1900(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V

    .line 1871
    invoke-static {}, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->updateWidgets()V

    .line 1872
    const-string/jumbo v1, "reset summaryfragment"

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1845
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
