.class Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$2;
.super Ljava/lang/Object;
.source "InitialProfileSamsungAccount.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$2;->this$0:Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    .line 173
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->getInstance()Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$2;->this$0:Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;

    sget-object v6, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->SAMSUNG_ACCOUNT_NOTIFICATION:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    invoke-virtual {v4, v5, v3, v6}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->scheduleReminderNotification(Landroid/content/Context;ILcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)V

    .line 174
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$2;->this$0:Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->getMigrationState(Landroid/content/Context;)I

    move-result v2

    .line 175
    .local v2, "migrationState":I
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/service/health/keyManager/KeyManager;->isSecureStorageSupported()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$2;->this$0:Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;

    # getter for: Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->access$200(Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;)Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$2;->this$0:Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-string/jumbo v6, "security_pin_enabled"

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 179
    .local v3, "requestPasswordCondition":Z
    :goto_0
    if-eqz v3, :cond_1

    .line 180
    new-instance v1, Landroid/content/Intent;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$2;->this$0:Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 181
    .local v1, "intent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$2;->this$0:Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;

    invoke-virtual {v4, v1}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->startActivity(Landroid/content/Intent;)V

    .line 193
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_1
    return-void

    .line 175
    .end local v3    # "requestPasswordCondition":Z
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 183
    .restart local v3    # "requestPasswordCondition":Z
    :cond_1
    if-nez v2, :cond_2

    .line 185
    new-instance v1, Landroid/content/Intent;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$2;->this$0:Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 186
    .restart local v1    # "intent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$2;->this$0:Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;

    invoke-virtual {v4, v1}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 188
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_2
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$2;->this$0:Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;

    invoke-direct {v0, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;-><init>(Landroid/content/Context;)V

    .line 189
    .local v0, "dataMigrationHelper":Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$2;->this$0:Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;

    # getter for: Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->access$300(Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;)Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$2;->this$0:Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;

    # getter for: Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mAppID:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->access$400(Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "MI01"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->startLocalMigration()V

    goto :goto_1
.end method
