.class Lcom/sec/android/app/shealth/common/commonui/ListPopup$InternalOnItemClickedListener;
.super Ljava/lang/Object;
.source "ListPopup.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commonui/ListPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InternalOnItemClickedListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/ListPopup;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/common/commonui/ListPopup;)V
    .locals 0

    .prologue
    .line 282
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup$InternalOnItemClickedListener;->this$0:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/common/commonui/ListPopup;Lcom/sec/android/app/shealth/common/commonui/ListPopup$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/common/commonui/ListPopup$1;

    .prologue
    .line 282
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/ListPopup$InternalOnItemClickedListener;-><init>(Lcom/sec/android/app/shealth/common/commonui/ListPopup;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 285
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 286
    .local v0, "index":I
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup$InternalOnItemClickedListener;->this$0:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/ListPopup;->onItemClickedListener:Lcom/sec/android/app/shealth/common/commonui/ListPopup$OnItemClickedListener;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->access$500(Lcom/sec/android/app/shealth/common/commonui/ListPopup;)Lcom/sec/android/app/shealth/common/commonui/ListPopup$OnItemClickedListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 287
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup$InternalOnItemClickedListener;->this$0:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/ListPopup;->onItemClickedListener:Lcom/sec/android/app/shealth/common/commonui/ListPopup$OnItemClickedListener;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->access$500(Lcom/sec/android/app/shealth/common/commonui/ListPopup;)Lcom/sec/android/app/shealth/common/commonui/ListPopup$OnItemClickedListener;

    move-result-object v2

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup$InternalOnItemClickedListener;->this$0:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/ListPopup;->mItems:Ljava/util/List;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->access$600(Lcom/sec/android/app/shealth/common/commonui/ListPopup;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/ListPopup$InternalOnItemClickedListener;->this$0:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-interface {v2, v0, v1, v3}, Lcom/sec/android/app/shealth/common/commonui/ListPopup$OnItemClickedListener;->onItemClicked(ILjava/lang/String;Landroid/widget/PopupWindow;)V

    .line 289
    :cond_0
    return-void
.end method
