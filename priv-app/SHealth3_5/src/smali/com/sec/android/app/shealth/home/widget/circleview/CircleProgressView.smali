.class public Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;
.super Landroid/widget/FrameLayout;
.source "CircleProgressView.java"


# static fields
.field public static final COUNT:I = 0x64

.field public static final DELAY:I = 0xa

.field private static TAG:Ljava/lang/String;


# instance fields
.field private R1:F

.field private color1:I

.field private currentValue:F

.field private goalValue:F

.field private r1:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 49
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    sget-object v1, Lcom/sec/android/app/shealth/R$styleable;->SHealthCircleProgressView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 51
    .local v0, "typedArray":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->r1:F

    .line 52
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->R1:F

    .line 53
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->color1:I

    .line 54
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 55
    return-void
.end method

.method private addCircleView(FFI)Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;
    .locals 4
    .param p1, "innerRadius"    # F
    .param p2, "externalRadius"    # F
    .param p3, "color"    # I

    .prologue
    .line 105
    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v3, p2

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 106
    .local v2, "dim":I
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 107
    .local v1, "circlesLayoutParams":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v3, 0x11

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 108
    new-instance v0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;-><init>(Landroid/content/Context;)V

    .line 109
    .local v0, "circleView":Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;
    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->setInnerRadius(F)V

    .line 110
    invoke-virtual {v0, p2}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->setExternalRadius(F)V

    .line 111
    invoke-virtual {v0, p3}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->setColor(I)V

    .line 112
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 113
    return-object v0
.end method

.method private checkValue(F)V
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 62
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 63
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "value should be > 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :cond_0
    return-void
.end method


# virtual methods
.method public invalidateAndStartAnimation()V
    .locals 6

    .prologue
    const/high16 v5, 0x43b40000    # 360.0f

    const/4 v4, 0x0

    .line 94
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->removeAllViews()V

    .line 95
    iget v1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->r1:F

    iget v2, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->R1:F

    iget v3, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->color1:I

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->addCircleView(FFI)Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;

    move-result-object v0

    .line 96
    .local v0, "firstCircle":Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;
    iget v1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->currentValue:F

    iget v2, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->goalValue:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    .line 97
    new-instance v1, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;

    iget v2, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->currentValue:F

    mul-float/2addr v2, v5

    iget v3, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->goalValue:F

    div-float/2addr v2, v3

    invoke-direct {v1, v0, v4, v2}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;-><init>(Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;FF)V

    const/16 v2, 0xa

    const/16 v3, 0x64

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->start(II)V

    .line 102
    :goto_0
    return-void

    .line 99
    :cond_0
    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->setStartAngle(F)V

    .line 100
    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->setEndAngle(F)V

    goto :goto_0
.end method

.method public setCurrentValue(F)V
    .locals 0
    .param p1, "currentValue"    # F

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->checkValue(F)V

    .line 69
    iput p1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->currentValue:F

    .line 70
    return-void
.end method

.method public setGoalValue(F)V
    .locals 3
    .param p1, "goalValue"    # F

    .prologue
    .line 74
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->checkValue(F)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    :goto_0
    iput p1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->goalValue:F

    .line 80
    return-void

    .line 75
    :catch_0
    move-exception v0

    .line 76
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public updateProgressColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 58
    iput p1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->color1:I

    .line 59
    return-void
.end method

.method public updateWithoutAnimation()V
    .locals 5

    .prologue
    const/high16 v4, 0x43b40000    # 360.0f

    .line 83
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->removeAllViews()V

    .line 84
    iget v1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->r1:F

    iget v2, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->R1:F

    iget v3, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->color1:I

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->addCircleView(FFI)Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;

    move-result-object v0

    .line 85
    .local v0, "firstCircle":Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->setStartAngle(F)V

    .line 86
    iget v1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->currentValue:F

    iget v2, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->goalValue:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    .line 87
    iget v1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->currentValue:F

    mul-float/2addr v1, v4

    iget v2, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->goalValue:F

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->setEndAngle(F)V

    .line 91
    :goto_0
    return-void

    .line 89
    :cond_0
    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->setEndAngle(F)V

    goto :goto_0
.end method
