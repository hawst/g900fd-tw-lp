.class public Lcom/sec/android/app/shealth/settings/SHealthUpdation;
.super Ljava/lang/Object;
.source "SHealthUpdation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/SHealthUpdation$1;,
        Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;,
        Lcom/sec/android/app/shealth/settings/SHealthUpdation$OnAppUpdateListener;
    }
.end annotation


# static fields
.field private static final RESULT_FAIL:I = 0x0

.field private static final RESULT_SUCCESS:I = 0x1

.field private static final TAG:Ljava/lang/String; = "SHealthUpdation"


# instance fields
.field private final END_NUMBER:I

.field private final START_NUMBER:I

.field private appId:Ljava/lang/String;

.field private mAppContext:Landroid/content/Context;

.field private mCheckTask:Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;

.field private mCurVersion:Ljava/lang/String;

.field private mCurVersionCode:Ljava/lang/String;

.field private mIsForceupdate:Z

.field private mUiListener:Lcom/sec/android/app/shealth/settings/SHealthUpdation$OnAppUpdateListener;

.field private mUpdateNeeded:Z

.field private resultCode:Ljava/lang/String;

.field private resultMsg:Ljava/lang/String;

.field private version:Ljava/lang/String;

.field private versionCode:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mCheckTask:Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;

    .line 50
    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mAppContext:Landroid/content/Context;

    .line 51
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mUpdateNeeded:Z

    .line 52
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mIsForceupdate:Z

    .line 56
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mCurVersion:Ljava/lang/String;

    .line 57
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mCurVersionCode:Ljava/lang/String;

    .line 59
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->appId:Ljava/lang/String;

    .line 60
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->resultCode:Ljava/lang/String;

    .line 61
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->resultMsg:Ljava/lang/String;

    .line 62
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->version:Ljava/lang/String;

    .line 63
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->versionCode:Ljava/lang/String;

    .line 65
    iput v2, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->START_NUMBER:I

    .line 66
    const/4 v1, 0x2

    iput v1, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->END_NUMBER:I

    .line 84
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mAppContext:Landroid/content/Context;

    .line 85
    new-instance v1, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;-><init>(Lcom/sec/android/app/shealth/settings/SHealthUpdation;Lcom/sec/android/app/shealth/settings/SHealthUpdation$1;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mCheckTask:Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;

    .line 86
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mCheckTask:Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 87
    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    invoke-static {}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->readModelCMCC()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mIsForceupdate:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/shealth/settings/SHealthUpdation;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/SHealthUpdation;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mIsForceupdate:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mCheckTask:Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/shealth/settings/SHealthUpdation;Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;)Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/SHealthUpdation;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mCheckTask:Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)Lcom/sec/android/app/shealth/settings/SHealthUpdation$OnAppUpdateListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mUiListener:Lcom/sec/android/app/shealth/settings/SHealthUpdation$OnAppUpdateListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mAppContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mCurVersionCode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/settings/SHealthUpdation;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/SHealthUpdation;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mCurVersionCode:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->isPD()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->getMCC()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->getMNC()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/settings/SHealthUpdation;Ljava/net/URL;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/SHealthUpdation;
    .param p1, "x1"    # Ljava/net/URL;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->checkUpdate(Ljava/net/URL;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mUpdateNeeded:Z

    return v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/shealth/settings/SHealthUpdation;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/SHealthUpdation;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mUpdateNeeded:Z

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->versionCode:Ljava/lang/String;

    return-object v0
.end method

.method private checkUpdate(Ljava/net/URL;)Z
    .locals 13
    .param p1, "url"    # Ljava/net/URL;

    .prologue
    .line 185
    const/4 v6, 0x0

    .line 191
    .local v6, "rtn":Z
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v3

    .line 192
    .local v3, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v3}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    .line 193
    .local v4, "parser":Lorg/xmlpull/v1/XmlPullParser;
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v1

    .line 194
    .local v1, "con":Ljava/net/URLConnection;
    const/16 v9, 0x7530

    invoke-virtual {v1, v9}, Ljava/net/URLConnection;->setReadTimeout(I)V

    .line 195
    invoke-virtual {v1}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v9

    const/4 v10, 0x0

    invoke-interface {v4, v9, v10}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 197
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v5

    .line 200
    .local v5, "parserEvent":I
    :goto_0
    const/4 v9, 0x1

    if-eq v5, v9, :cond_6

    .line 202
    const/4 v9, 0x2

    if-ne v5, v9, :cond_0

    .line 204
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v7

    .line 206
    .local v7, "tag":Ljava/lang/String;
    const-string v9, "appId"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 207
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v8

    .line 209
    .local v8, "type":I
    const/4 v9, 0x4

    if-ne v8, v9, :cond_0

    .line 210
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->appId:Ljava/lang/String;

    .line 211
    const-string v9, "SHealthUpdation"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "[Update RSP] appId : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->appId:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    .end local v7    # "tag":Ljava/lang/String;
    .end local v8    # "type":I
    :cond_0
    :goto_1
    const/4 v9, 0x3

    if-ne v5, v9, :cond_1

    .line 259
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v7

    .line 261
    .restart local v7    # "tag":Ljava/lang/String;
    const-string v9, "appInfo"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 262
    iget-object v9, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->appId:Ljava/lang/String;

    iget-object v10, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->resultCode:Ljava/lang/String;

    invoke-direct {p0, v9, v10}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->getResultUpdateCheck(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    .line 265
    .end local v7    # "tag":Ljava/lang/String;
    :cond_1
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v5

    goto :goto_0

    .line 214
    .restart local v7    # "tag":Ljava/lang/String;
    :cond_2
    const-string/jumbo v9, "resultCode"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 216
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v8

    .line 218
    .restart local v8    # "type":I
    const/4 v9, 0x4

    if-ne v8, v9, :cond_0

    .line 219
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->resultCode:Ljava/lang/String;

    .line 220
    const-string v9, "SHealthUpdation"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "[Update RSP] resultCode : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->resultCode:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_1

    .line 268
    .end local v1    # "con":Ljava/net/URLConnection;
    .end local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v4    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v5    # "parserEvent":I
    .end local v7    # "tag":Ljava/lang/String;
    .end local v8    # "type":I
    :catch_0
    move-exception v2

    .line 269
    .local v2, "e":Lorg/xmlpull/v1/XmlPullParserException;
    const-string v9, "SHealthUpdation"

    const-string/jumbo v10, "xml parsing error"

    invoke-static {v9, v10, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 270
    const/4 v9, 0x0

    .line 281
    .end local v2    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :goto_2
    return v9

    .line 223
    .restart local v1    # "con":Ljava/net/URLConnection;
    .restart local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v4    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v5    # "parserEvent":I
    .restart local v7    # "tag":Ljava/lang/String;
    :cond_3
    :try_start_1
    const-string/jumbo v9, "resultMsg"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 225
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v8

    .line 227
    .restart local v8    # "type":I
    const/4 v9, 0x4

    if-ne v8, v9, :cond_0

    .line 228
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->resultMsg:Ljava/lang/String;

    .line 229
    const-string v9, "SHealthUpdation"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "[Update RSP] resultMsg : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->resultMsg:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto/16 :goto_1

    .line 272
    .end local v1    # "con":Ljava/net/URLConnection;
    .end local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v4    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v5    # "parserEvent":I
    .end local v7    # "tag":Ljava/lang/String;
    .end local v8    # "type":I
    :catch_1
    move-exception v2

    .line 273
    .local v2, "e":Ljava/net/SocketException;
    const-string v9, "SHealthUpdation"

    const-string v10, "Update check, network is unavailable"

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    const/4 v9, 0x0

    goto :goto_2

    .line 232
    .end local v2    # "e":Ljava/net/SocketException;
    .restart local v1    # "con":Ljava/net/URLConnection;
    .restart local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v4    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v5    # "parserEvent":I
    .restart local v7    # "tag":Ljava/lang/String;
    :cond_4
    :try_start_2
    const-string/jumbo v9, "version"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 234
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v8

    .line 236
    .restart local v8    # "type":I
    const/4 v9, 0x4

    if-ne v8, v9, :cond_0

    .line 237
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->version:Ljava/lang/String;

    .line 238
    const-string v9, "SHealthUpdation"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "[Update RSP] version : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->version:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_1

    .line 276
    .end local v1    # "con":Ljava/net/URLConnection;
    .end local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v4    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v5    # "parserEvent":I
    .end local v7    # "tag":Ljava/lang/String;
    .end local v8    # "type":I
    :catch_2
    move-exception v2

    .line 277
    .local v2, "e":Ljava/io/IOException;
    const-string v9, "SHealthUpdation"

    const-string v10, "Update check, but network fail"

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    const/4 v9, 0x0

    goto :goto_2

    .line 241
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "con":Ljava/net/URLConnection;
    .restart local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v4    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v5    # "parserEvent":I
    .restart local v7    # "tag":Ljava/lang/String;
    :cond_5
    :try_start_3
    const-string/jumbo v9, "versionCode"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 243
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v8

    .line 245
    .restart local v8    # "type":I
    const/4 v9, 0x4

    if-ne v8, v9, :cond_0

    .line 246
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->versionCode:Ljava/lang/String;

    .line 248
    iget-object v9, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->versionCode:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 249
    .local v0, "code":I
    const-string v9, "%010d"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->versionCode:Ljava/lang/String;

    .line 251
    const-string v9, "SHealthUpdation"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "[Update RSP] versionCode : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->versionCode:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_1

    .end local v0    # "code":I
    .end local v7    # "tag":Ljava/lang/String;
    .end local v8    # "type":I
    :cond_6
    move v9, v6

    .line 281
    goto/16 :goto_2
.end method

.method private getCSCVersion()Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 391
    const/4 v5, 0x0

    .line 393
    .local v5, "tmpString":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    const-string v7, "/system/csc/sales_code.dat"

    invoke-direct {v4, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 395
    .local v4, "mFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 397
    const/16 v7, 0x14

    new-array v0, v7, [B

    .line 399
    .local v0, "buffer":[B
    const/4 v2, 0x0

    .line 402
    .local v2, "in":Ljava/io/InputStream;
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 404
    .end local v2    # "in":Ljava/io/InputStream;
    .local v3, "in":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    move-result v7

    if-eqz v7, :cond_1

    .line 405
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .end local v5    # "tmpString":Ljava/lang/String;
    .local v6, "tmpString":Ljava/lang/String;
    move-object v5, v6

    .line 413
    .end local v6    # "tmpString":Ljava/lang/String;
    .restart local v5    # "tmpString":Ljava/lang/String;
    :goto_0
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 416
    .end local v0    # "buffer":[B
    .end local v3    # "in":Ljava/io/InputStream;
    :cond_0
    :goto_1
    return-object v5

    .line 407
    .restart local v0    # "buffer":[B
    .restart local v3    # "in":Ljava/io/InputStream;
    :cond_1
    :try_start_2
    new-instance v6, Ljava/lang/String;

    const-string v7, "FAIL"

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .end local v5    # "tmpString":Ljava/lang/String;
    .restart local v6    # "tmpString":Ljava/lang/String;
    move-object v5, v6

    .end local v6    # "tmpString":Ljava/lang/String;
    .restart local v5    # "tmpString":Ljava/lang/String;
    goto :goto_0

    .line 410
    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    :catch_0
    move-exception v1

    .line 411
    .local v1, "e":Ljava/io/IOException;
    :goto_2
    :try_start_3
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 413
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    goto :goto_1

    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    :goto_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    throw v7

    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    :catchall_1
    move-exception v7

    move-object v2, v3

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    goto :goto_3

    .line 410
    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    :catch_1
    move-exception v1

    move-object v2, v3

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    goto :goto_2
.end method

.method private getMCC()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x3

    .line 287
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mAppContext:Landroid/content/Context;

    const-string/jumbo v5, "phone"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    .line 290
    .local v3, "telMgr":Landroid/telephony/TelephonyManager;
    const-string v0, ""

    .line 292
    .local v0, "mcc":Ljava/lang/String;
    if-nez v3, :cond_0

    move-object v1, v0

    .line 327
    .end local v0    # "mcc":Ljava/lang/String;
    .local v1, "mcc":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 298
    .end local v1    # "mcc":Ljava/lang/String;
    .restart local v0    # "mcc":Ljava/lang/String;
    :cond_0
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v2

    .line 300
    .local v2, "networkOperator":Ljava/lang/String;
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 315
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v2

    .line 317
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-lt v4, v6, :cond_2

    .line 319
    invoke-virtual {v2, v7, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :goto_1
    move-object v1, v0

    .line 327
    .end local v0    # "mcc":Ljava/lang/String;
    .restart local v1    # "mcc":Ljava/lang/String;
    goto :goto_0

    .line 304
    .end local v1    # "mcc":Ljava/lang/String;
    .restart local v0    # "mcc":Ljava/lang/String;
    :pswitch_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-lt v4, v6, :cond_1

    .line 305
    invoke-virtual {v2, v7, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 306
    goto :goto_1

    .line 309
    :cond_1
    const-string v0, ""

    .line 311
    goto :goto_1

    .line 321
    :cond_2
    const-string v0, ""

    goto :goto_1

    .line 300
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private getMNC()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 333
    const-string v0, ""

    .line 335
    .local v0, "mnc":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mAppContext:Landroid/content/Context;

    const-string/jumbo v4, "phone"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 338
    .local v2, "telMgr":Landroid/telephony/TelephonyManager;
    if-nez v2, :cond_1

    .line 352
    .end local v0    # "mnc":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 344
    .restart local v0    # "mnc":Ljava/lang/String;
    :cond_1
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    .line 346
    .local v1, "networkOperator":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v5, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x6

    if-gt v3, v4, :cond_0

    .line 348
    invoke-virtual {v1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getResultUpdateCheck(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "code"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 438
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mAppContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 440
    .local v1, "stubPackageName":Ljava/lang/String;
    const-string v3, "SHealthUpdation"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getResultUpdateCheck - packageName : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", code : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 442
    const-string v3, "SHealthUpdation"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getResultUpdateCheck - stubPackageName : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    const-string v3, "0"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 445
    const-string v3, "SHealthUpdation"

    const-string v4, "There is no application of the Application ID."

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 452
    :cond_0
    :goto_0
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "2"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 453
    const-string v3, "SHealthUpdation"

    const-string v4, "Found Press Reader Update"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mAppContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.sec.android.app.samsungapps"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 461
    const-string v3, "SHealthUpdation"

    const-string/jumbo v4, "samsungapps installed"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 462
    const/4 v2, 0x1

    .line 474
    :goto_1
    return v2

    .line 446
    :cond_1
    const-string v3, "1"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 447
    const-string v3, "SHealthUpdation"

    const-string v4, "There is the application but it is not updatable."

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 448
    :cond_2
    const-string v3, "2"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 449
    const-string v3, "SHealthUpdation"

    const-string v4, "There is updates of the application. You can update it!"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 464
    :catch_0
    move-exception v0

    .line 466
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 467
    const-string v3, "SHealthUpdation"

    const-string v4, "com.sec.android.app.samsungapps is not present in device"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 471
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_3
    const-string v3, "SHealthUpdation"

    const-string v4, "Not found Press Reader Update"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private isCSCExistFile()Z
    .locals 6

    .prologue
    .line 421
    const/4 v2, 0x0

    .line 423
    .local v2, "result":Z
    new-instance v1, Ljava/io/File;

    const-string v3, "/system/csc/sales_code.dat"

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 426
    .local v1, "file":Ljava/io/File;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    .line 427
    if-nez v2, :cond_0

    .line 428
    const-string v3, "SHealthUpdation"

    const-string v4, "CSC is not exist"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 433
    :cond_0
    :goto_0
    return v2

    .line 430
    :catch_0
    move-exception v0

    .line 431
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "SHealthUpdation"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isCSCExistFile::"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isPD()Z
    .locals 2

    .prologue
    .line 479
    new-instance v0, Ljava/io/File;

    const-string/jumbo v1, "mnt/sdcard/pd.test"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 481
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 482
    const/4 v1, 0x0

    .line 485
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static readModelCMCC()Ljava/lang/String;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 491
    const-string v7, ""

    .line 492
    .local v7, "name":Ljava/lang/String;
    const-string v6, "/system/version"

    .line 493
    .local v6, "modelFile":Ljava/lang/String;
    const/4 v5, -0x1

    .line 494
    .local v5, "len":I
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 496
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 497
    const/16 v9, 0x80

    new-array v0, v9, [B

    .line 498
    .local v0, "buffer":[B
    const/4 v3, 0x0

    .line 500
    .local v3, "in":Ljava/io/InputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v3    # "in":Ljava/io/InputStream;
    .local v4, "in":Ljava/io/InputStream;
    move-object v3, v4

    .line 506
    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    :goto_0
    :try_start_1
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    move-result v5

    .line 507
    if-lez v5, :cond_0

    .line 508
    new-instance v8, Ljava/lang/String;

    const/4 v9, 0x0

    invoke-direct {v8, v0, v9, v5}, Ljava/lang/String;-><init>([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v7    # "name":Ljava/lang/String;
    .local v8, "name":Ljava/lang/String;
    move-object v7, v8

    .line 513
    .end local v8    # "name":Ljava/lang/String;
    .restart local v7    # "name":Ljava/lang/String;
    :cond_0
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 516
    .end local v0    # "buffer":[B
    .end local v3    # "in":Ljava/io/InputStream;
    :cond_1
    :goto_1
    return-object v7

    .line 501
    .restart local v0    # "buffer":[B
    .restart local v3    # "in":Ljava/io/InputStream;
    :catch_0
    move-exception v1

    .line 502
    .local v1, "e":Ljava/io/FileNotFoundException;
    const-string v9, "SHealthUpdation"

    const-string v10, "Util::readModelCMCC::File not found"

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 510
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v1

    .line 511
    .local v1, "e":Ljava/io/IOException;
    :try_start_2
    const-string v9, "SHealthUpdation"

    const-string v10, "Util::readModelCMCC::"

    invoke-static {v9, v10, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 513
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    goto :goto_1

    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    throw v9
.end method


# virtual methods
.method public cancelCheckTask()V
    .locals 2

    .prologue
    .line 521
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mCheckTask:Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;

    if-eqz v0, :cond_0

    .line 522
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mCheckTask:Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->cancel(Z)Z

    .line 524
    :cond_0
    return-void
.end method

.method public getCSC()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 360
    const-string v0, ""

    .line 362
    .local v0, "cscVersion":Ljava/lang/String;
    const/4 v1, 0x0

    .line 365
    .local v1, "value":Ljava/lang/String;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->isCSCExistFile()Z

    move-result v2

    if-nez v2, :cond_0

    .line 385
    :goto_0
    return-object v0

    .line 369
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->getCSCVersion()Ljava/lang/String;

    move-result-object v1

    .line 371
    if-nez v1, :cond_1

    .line 372
    const-string v2, "SHealthUpdation"

    const-string v3, "getCSC::getCSCVersion::value is null"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 376
    :cond_1
    const-string v2, "FAIL"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 377
    const-string v2, "SHealthUpdation"

    const-string v3, "getCSC::getCSCVersion::Fail to read CSC Version"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 381
    :cond_2
    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public registerAppUpdateListener(Lcom/sec/android/app/shealth/settings/SHealthUpdation$OnAppUpdateListener;)V
    .locals 1
    .param p1, "l"    # Lcom/sec/android/app/shealth/settings/SHealthUpdation$OnAppUpdateListener;

    .prologue
    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mUiListener:Lcom/sec/android/app/shealth/settings/SHealthUpdation$OnAppUpdateListener;

    .line 71
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mUiListener:Lcom/sec/android/app/shealth/settings/SHealthUpdation$OnAppUpdateListener;

    .line 72
    return-void
.end method

.method public unregisterAppUpdateListener()V
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mUiListener:Lcom/sec/android/app/shealth/settings/SHealthUpdation$OnAppUpdateListener;

    .line 76
    return-void
.end method
