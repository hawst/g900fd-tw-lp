.class public Lcom/sec/android/app/shealth/framework/repository/database/old/SharedPreferencesHelper;
.super Ljava/lang/Object;
.source "SharedPreferencesHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/repository/database/old/SharedPreferencesHelper$SharedPreferencesKeys;
    }
.end annotation


# static fields
.field private static final SHAREDPREF_FILE_BACKUP_RESTORE:Ljava/lang/String; = "com.sec.android.app.shealth_preferences"

.field private static final TAG:Ljava/lang/String;

.field private static mContext:Landroid/content/Context;

.field public static prefs:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/sec/android/app/shealth/framework/repository/database/old/SharedPreferencesHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    .line 15
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    return-void
.end method

.method public static getProfileImageStatus()Ljava/lang/String;
    .locals 4

    .prologue
    .line 27
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/old/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "profile_image_status"

    const-string/jumbo v3, "not_modified"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 28
    .local v0, "profileImageStatus":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/old/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getProfileImageStatus : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 29
    return-object v0
.end method

.method public static initialize(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    sput-object p0, Lcom/sec/android/app/shealth/framework/repository/database/old/SharedPreferencesHelper;->mContext:Landroid/content/Context;

    .line 22
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/SharedPreferencesHelper;->mContext:Landroid/content/Context;

    const-string v1, "com.sec.android.app.shealth_preferences"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    .line 23
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "PREF_VERSION"

    const/4 v2, 0x5

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 24
    return-void
.end method
