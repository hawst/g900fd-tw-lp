.class Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$1;
.super Ljava/lang/Object;
.source "DynamicGridView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$1;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 10
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .line 67
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$1;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 68
    const/4 v0, 0x0

    .line 105
    :goto_0
    return v0

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$1;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mTotalOffsetY:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->access$002(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;I)I

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$1;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mTotalOffsetX:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->access$102(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;I)I

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$1;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/home/data/IconData;

    .line 72
    .local v6, "icondata":Lcom/sec/android/app/shealth/home/data/IconData;
    if-eqz v6, :cond_1

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/home/data/IconData;->getIconType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 74
    const/4 v0, 0x0

    goto :goto_0

    .line 77
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$1;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$1;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    # getter for: Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mDownX:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->access$200(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$1;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    # getter for: Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mDownY:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->access$300(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->pointToPosition(II)I

    move-result v8

    .line 78
    .local v8, "position":I
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$1;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getFirstVisiblePosition()I

    move-result v0

    sub-int v7, v8, v0

    .line 80
    .local v7, "itemNum":I
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$1;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    invoke-virtual {v0, v7}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 81
    .local v9, "selectedView":Landroid/view/View;
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$1;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$1;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1, v8}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v1

    # setter for: Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mMobileItemId:J
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->access$402(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;J)J

    .line 84
    if-eqz v9, :cond_2

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$1;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$1;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    # invokes: Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->getAndAddHoverView(Landroid/view/View;)Landroid/graphics/drawable/BitmapDrawable;
    invoke-static {v1, v9}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->access$600(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;Landroid/view/View;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mHoverCell:Landroid/graphics/drawable/BitmapDrawable;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->access$502(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;Landroid/graphics/drawable/BitmapDrawable;)Landroid/graphics/drawable/BitmapDrawable;

    .line 88
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$1;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    # invokes: Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->isPostHoneycomb()Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->access$700(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz v9, :cond_3

    .line 90
    const/4 v0, 0x4

    invoke-virtual {v9, v0}, Landroid/view/View;->setVisibility(I)V

    .line 92
    const v0, 0x7f0804f2

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 94
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$1;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mCellIsMobile:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->access$802(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;Z)Z

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$1;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$1;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    # getter for: Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mMobileItemId:J
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->access$400(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;)J

    move-result-wide v1

    # invokes: Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->updateNeighborViewsForId(J)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->access$900(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;J)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$1;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    # invokes: Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->playTalkBack(I)V
    invoke-static {v0, p3}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->access$1000(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;I)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$1;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    # getter for: Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mUserLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->access$1100(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;)Landroid/widget/AdapterView$OnItemLongClickListener;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$1;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    # getter for: Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mUserLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->access$1100(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;)Landroid/widget/AdapterView$OnItemLongClickListener;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemLongClickListener;->onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z

    .line 105
    :cond_4
    const/4 v0, 0x1

    goto/16 :goto_0
.end method
