.class Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$2;
.super Ljava/lang/Object;
.source "PortionSizeSliderView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "motionEvent"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    .line 135
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 136
    .local v0, "x":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 158
    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 138
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mScaleView:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->access$200(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 139
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mHandlerView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->access$400(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)Landroid/view/View;

    move-result-object v1

    float-to-int v2, v0

    invoke-virtual {v1, v2, v4, v4, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 140
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->calculatePortionValue(F)F
    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->access$700(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;F)F

    move-result v2

    # setter for: Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mCurrentPortionValue:F
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->access$602(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;F)F

    goto :goto_0

    .line 144
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mScaleView:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->access$200(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v1, v1, v0

    if-gtz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mScaleViewWidth:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->access$100(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mScaleView:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->access$200(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v1, v1, v0

    if-ltz v1, :cond_0

    .line 146
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mScaleView:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->access$200(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v3

    int-to-float v3, v3

    sub-float v3, v0, v3

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->calculatePortionValue(F)F
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->access$700(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;F)F

    move-result v2

    # setter for: Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mCurrentPortionValue:F
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->access$602(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;F)F

    .line 147
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mStartHandlerWidth:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->access$300(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    .line 148
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mHandlerView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->access$400(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)Landroid/view/View;

    move-result-object v1

    float-to-int v2, v0

    invoke-virtual {v1, v2, v4, v4, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 150
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mTextViewPortionValue:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->access$800(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mCurrentPortionValue:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->access$600(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 154
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->moveHandlerToCurrentValue()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->access$500(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)V

    goto/16 :goto_0

    .line 136
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
