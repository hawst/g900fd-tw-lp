.class Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$1;
.super Ljava/lang/Object;
.source "TemperatureDeviceConnector.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;)V
    .locals 0

    .prologue
    .line 280
    iput-object p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onJoined(I)V
    .locals 3
    .param p1, "arg0"    # I

    .prologue
    .line 284
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "onJoined"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$100(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$200(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 287
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$100(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    iget-object v2, v2, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->startReceivingData(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;)V

    .line 288
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    iget-boolean v1, v1, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->isRecord:Z

    if-eqz v1, :cond_0

    .line 289
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$100(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->record()V

    .line 292
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$202(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;Z)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_0 .. :try_end_0} :catch_7

    .line 314
    :goto_0
    return-void

    .line 293
    :catch_0
    move-exception v0

    .line 294
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 295
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 296
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 297
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_2
    move-exception v0

    .line 298
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 299
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_3
    move-exception v0

    .line 300
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 301
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_4
    move-exception v0

    .line 303
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0

    .line 304
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :catch_5
    move-exception v0

    .line 306
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 307
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_6
    move-exception v0

    .line 309
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;->printStackTrace()V

    goto :goto_0

    .line 310
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    :catch_7
    move-exception v0

    .line 312
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;->printStackTrace()V

    goto :goto_0
.end method

.method public onLeft(I)V
    .locals 5
    .param p1, "error"    # I

    .prologue
    const/4 v4, 0x0

    .line 318
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onLeft : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$100(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$200(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 321
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$100(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->stopReceivingData()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 335
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    # setter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$202(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;Z)Z

    .line 337
    :goto_0
    return-void

    .line 323
    :catch_0
    move-exception v0

    .line 324
    .local v0, "e":Ljava/lang/IllegalStateException;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 335
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    # setter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$202(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;Z)Z

    goto :goto_0

    .line 325
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 326
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_2
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 335
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    # setter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$202(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;Z)Z

    goto :goto_0

    .line 327
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_2
    move-exception v0

    .line 328
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :try_start_3
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 335
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    # setter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$202(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;Z)Z

    goto :goto_0

    .line 329
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_3
    move-exception v0

    .line 330
    .local v0, "e":Ljava/lang/NullPointerException;
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 335
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    # setter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$202(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;Z)Z

    goto :goto_0

    .line 331
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_4
    move-exception v0

    .line 333
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :try_start_5
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 335
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    # setter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$202(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;Z)Z

    goto :goto_0

    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector$1;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    # setter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->IsDeviceStarted:Z
    invoke-static {v2, v4}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$202(Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;Z)Z

    throw v1
.end method

.method public onResponseReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;)V
    .locals 3
    .param p1, "command"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;
    .param p2, "response"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;

    .prologue
    .line 341
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Response ---------"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Response commandId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;->getCommandId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Response errorCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;->getErrorCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Response errorDescription : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;->getErrorDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    return-void
.end method

.method public onStateChanged(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 350
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Response onStateChanged : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    return-void
.end method
