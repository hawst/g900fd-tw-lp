.class public Lcom/sec/android/app/shealth/common/commonui/dialog/TextItemModel;
.super Ljava/lang/Object;
.source "TextItemModel.java"


# instance fields
.field private description:Ljava/lang/String;

.field private text:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "description"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/TextItemModel;->text:Ljava/lang/String;

    .line 35
    iput-object p2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/TextItemModel;->description:Ljava/lang/String;

    .line 36
    return-void
.end method


# virtual methods
.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/TextItemModel;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/TextItemModel;->text:Ljava/lang/String;

    return-object v0
.end method
