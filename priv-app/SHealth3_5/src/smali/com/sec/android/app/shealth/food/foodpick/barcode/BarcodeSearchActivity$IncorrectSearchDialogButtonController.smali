.class Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$IncorrectSearchDialogButtonController;
.super Ljava/lang/Object;
.source "BarcodeSearchActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IncorrectSearchDialogButtonController"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)V
    .locals 0

    .prologue
    .line 409
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$IncorrectSearchDialogButtonController;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$1;

    .prologue
    .line 409
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$IncorrectSearchDialogButtonController;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 2
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 412
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$8;->$SwitchMap$com$sec$android$app$shealth$common$commonui$dialog$DialogButtonType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 425
    :goto_0
    return-void

    .line 414
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$IncorrectSearchDialogButtonController;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->enterManually()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$1900(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)V

    goto :goto_0

    .line 418
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$IncorrectSearchDialogButtonController;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->finish()V

    goto :goto_0

    .line 422
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$IncorrectSearchDialogButtonController;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->tryNewSearch()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$1800(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)V

    goto :goto_0

    .line 412
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
