.class public final enum Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;
.super Ljava/lang/Enum;
.source "ConfigUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/utils/ConfigUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Languages"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;

.field public static final enum Arabic:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;

.field public static final enum Hebrew:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;

.field public static final enum Other:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 228
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;

    const-string v1, "Arabic"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;->Arabic:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;

    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;

    const-string v1, "Hebrew"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;->Hebrew:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;

    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;

    const-string v1, "Other"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;->Other:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;

    .line 226
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;->Arabic:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;->Hebrew:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;->Other:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;->$VALUES:[Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 226
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 226
    const-class v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;
    .locals 1

    .prologue
    .line 226
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;->$VALUES:[Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 238
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$1;->$SwitchMap$com$sec$android$app$shealth$common$utils$ConfigUtils$Languages:[I

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Languages;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 247
    const-string/jumbo v0, "other"

    :goto_0
    return-object v0

    .line 241
    :pswitch_0
    const-string v0, "ar"

    goto :goto_0

    .line 243
    :pswitch_1
    const-string v0, "iw"

    goto :goto_0

    .line 238
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
