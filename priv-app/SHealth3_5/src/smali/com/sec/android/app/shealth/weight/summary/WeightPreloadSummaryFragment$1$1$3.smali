.class Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1$3;
.super Ljava/lang/Object;
.source "WeightPreloadSummaryFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;->showErrorPopUp()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1$3;->this$2:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/app/Activity;)V
    .locals 2
    .param p1, "parentActivity"    # Landroid/app/Activity;

    .prologue
    .line 165
    sget-object v0, Lcom/sec/android/app/shealth/weight/common/WeightConstants;->APP_TYPE:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->isAccessorySupported:Z

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1$3;->this$2:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;

    iget-object v0, v0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;->this$1:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;

    iget-object v0, v0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightBinder:Lcom/sec/android/app/shealth/weight/service/WeightService$WeightBinder;
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->access$000(Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;)Lcom/sec/android/app/shealth/weight/service/WeightService$WeightBinder;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 167
    # getter for: Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onResume(), we call searchpairedDeviceList"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1$3;->this$2:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;

    iget-object v0, v0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1$1;->this$1:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;

    iget-object v0, v0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightBinder:Lcom/sec/android/app/shealth/weight/service/WeightService$WeightBinder;
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->access$000(Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;)Lcom/sec/android/app/shealth/weight/service/WeightService$WeightBinder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/weight/service/WeightService$WeightBinder;->startWeightService()V

    .line 173
    :cond_0
    return-void
.end method
