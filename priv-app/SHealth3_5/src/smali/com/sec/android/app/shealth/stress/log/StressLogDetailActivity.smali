.class public Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "StressLogDetailActivity.java"


# static fields
.field public static final ACTIONBAR_EDIT:I = 0x0

.field public static final EDIT:Ljava/lang/String; = "Edit"


# instance fields
.field private actionbarClickListener:Landroid/view/View$OnClickListener;

.field private mComment:Landroid/widget/TextView;

.field private mCommentTitle:Landroid/widget/TextView;

.field private mStateBar:Lcom/sec/android/app/shealth/stress/custom/StressStateDetailBar;

.field private mStressData:Lcom/sec/android/app/shealth/stress/data/StressData;

.field private mStressDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

.field private mTimeDate:Landroid/widget/TextView;

.field private memoIcon:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 126
    new-instance v0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity$1;-><init>(Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->actionbarClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;)Lcom/sec/android/app/shealth/stress/data/StressData;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->mStressData:Lcom/sec/android/app/shealth/stress/data/StressData;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->mTimeDate:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;)Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->mStressDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    return-object v0
.end method

.method private initView(Ljava/lang/String;)V
    .locals 7
    .param p1, "stressId"    # Ljava/lang/String;

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 53
    invoke-static {p0}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->mStressDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    .line 54
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->mStressDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->getDataById(Ljava/lang/String;)Lcom/sec/android/app/shealth/stress/data/StressData;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->mStressData:Lcom/sec/android/app/shealth/stress/data/StressData;

    .line 57
    const v2, 0x7f08052f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->mTimeDate:Landroid/widget/TextView;

    .line 58
    const v2, 0x7f080762

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/stress/custom/StressStateDetailBar;

    iput-object v2, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->mStateBar:Lcom/sec/android/app/shealth/stress/custom/StressStateDetailBar;

    .line 59
    const v2, 0x7f080539

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->mComment:Landroid/widget/TextView;

    .line 60
    const v2, 0x7f0803e6

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->mCommentTitle:Landroid/widget/TextView;

    .line 61
    const v2, 0x7f0803e4

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->memoIcon:Landroid/widget/ImageView;

    .line 65
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->mTimeDate:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->mStressData:Lcom/sec/android/app/shealth/stress/data/StressData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/stress/data/StressData;->getSampleTime()J

    move-result-wide v3

    invoke-static {p0, v3, v4}, Lcom/sec/android/app/shealth/stress/utils/DateFormatUtil;->getDateTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->mStateBar:Lcom/sec/android/app/shealth/stress/custom/StressStateDetailBar;

    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->mStressData:Lcom/sec/android/app/shealth/stress/data/StressData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/stress/data/StressData;->getScore()D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/stress/custom/StressStateDetailBar;->moveToPolygon(D)V

    .line 71
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->mStressData:Lcom/sec/android/app/shealth/stress/data/StressData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/stress/data/StressData;->getComment()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->mStressData:Lcom/sec/android/app/shealth/stress/data/StressData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/stress/data/StressData;->getComment()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    .line 73
    new-instance v1, Landroid/text/SpannableString;

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->mStressData:Lcom/sec/android/app/shealth/stress/data/StressData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/stress/data/StressData;->getComment()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 74
    .local v1, "mSpannableString":Landroid/text/SpannableString;
    new-instance v2, Landroid/text/style/LeadingMarginSpan$Standard;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a06a7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    invoke-direct {v2, v3, v5}, Landroid/text/style/LeadingMarginSpan$Standard;-><init>(II)V

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v5, v3, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 75
    const v2, 0x7f080538

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 76
    .local v0, "MemoLayout":Landroid/widget/RelativeLayout;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f09007c

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f09020b

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->mStressData:Lcom/sec/android/app/shealth/stress/data/StressData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/stress/data/StressData;->getComment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->mComment:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->memoIcon:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 87
    .end local v0    # "MemoLayout":Landroid/widget/RelativeLayout;
    .end local v1    # "mSpannableString":Landroid/text/SpannableString;
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->mCommentTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 89
    return-void

    .line 84
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->memoIcon:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private showDeletePopup()V
    .locals 3

    .prologue
    .line 148
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09078e

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090035

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity$2;-><init>(Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 158
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 119
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 120
    const/4 v0, 0x0

    .line 121
    .local v0, "emptyActionItemTextId":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    const v2, 0x7f090026

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 122
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    new-array v2, v8, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v3, 0x0

    new-instance v4, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v5, 0x7f020299

    const v6, 0x7f090040

    iget-object v7, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->actionbarClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {v4, v5, v0, v6, v7}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 123
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 124
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 47
    const v0, 0x7f030230

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->setContentView(I)V

    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "HEART_RATE_ID_KEY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->initView(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f09005b

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 50
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f100025

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 100
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 105
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 114
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 107
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.stress"

    const-string v2, "ST09"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->prepareShareView()V

    goto :goto_0

    .line 111
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->showDeletePopup()V

    goto :goto_0

    .line 105
    :pswitch_data_0
    .packed-switch 0x7f080c8d
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 93
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->mTimeDate:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;->mStressData:Lcom/sec/android/app/shealth/stress/data/StressData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/data/StressData;->getSampleTime()J

    move-result-wide v1

    invoke-static {p0, v1, v2}, Lcom/sec/android/app/shealth/stress/utils/DateFormatUtil;->getDateTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    return-void
.end method
