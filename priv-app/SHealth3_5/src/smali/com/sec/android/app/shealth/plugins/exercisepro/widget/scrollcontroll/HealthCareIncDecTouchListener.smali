.class public Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;
.super Ljava/lang/Object;
.source "HealthCareIncDecTouchListener.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener$1;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener$TimerTaskOne;
    }
.end annotation


# static fields
.field public static final INCREASE_DELAY:I = 0x32

.field private static volatile sIsPressed:Z


# instance fields
.field public LONG_CLICK_DELAY:I

.field private mActivity:Landroid/app/Activity;

.field private mDecreaseButton:Landroid/view/View;

.field private mIncreaseButton:Landroid/view/View;

.field private volatile mIsQuickIncDecBegins:Z

.field private mQuickDecrease:Z

.field private mQuickIncrease:Z

.field private mTimer:Ljava/util/Timer;

.field private mTouchedView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->sIsPressed:Z

    return-void
.end method

.method public constructor <init>(Landroid/view/View;Landroid/view/View;Landroid/app/Activity;)V
    .locals 2
    .param p1, "increaseButton"    # Landroid/view/View;
    .param p2, "decreaseButton"    # Landroid/view/View;
    .param p3, "context"    # Landroid/app/Activity;

    .prologue
    const/4 v1, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const/16 v0, 0xc8

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->LONG_CLICK_DELAY:I

    .line 20
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mQuickIncrease:Z

    .line 21
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mQuickDecrease:Z

    .line 26
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mIncreaseButton:Landroid/view/View;

    .line 27
    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mDecreaseButton:Landroid/view/View;

    .line 28
    iput-object p3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mActivity:Landroid/app/Activity;

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/view/View;Landroid/view/View;Landroid/app/Activity;I)V
    .locals 2
    .param p1, "increaseButton"    # Landroid/view/View;
    .param p2, "decreaseButton"    # Landroid/view/View;
    .param p3, "context"    # Landroid/app/Activity;
    .param p4, "delay"    # I

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const/16 v0, 0xc8

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->LONG_CLICK_DELAY:I

    .line 20
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mQuickIncrease:Z

    .line 21
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mQuickDecrease:Z

    .line 32
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mIncreaseButton:Landroid/view/View;

    .line 33
    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mDecreaseButton:Landroid/view/View;

    .line 34
    iput-object p3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mActivity:Landroid/app/Activity;

    .line 35
    iput p4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->LONG_CLICK_DELAY:I

    .line 36
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mTouchedView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;
    .param p1, "x1"    # Z

    .prologue
    .line 13
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mIsQuickIncDecBegins:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->performOnClick()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method private performOnClick()V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mTouchedView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->playSoundEffect(I)V

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mTouchedView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mIncreaseButton:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mIncreaseButton:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->callOnClick()Z

    .line 91
    :goto_0
    return-void

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mDecreaseButton:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->callOnClick()Z

    goto :goto_0
.end method


# virtual methods
.method public beforeTouchDownEvent()V
    .locals 0

    .prologue
    .line 121
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "motionEvent"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 40
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 76
    sget-boolean v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->sIsPressed:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mTouchedView:Landroid/view/View;

    if-eq p1, v1, :cond_0

    move v0, v7

    .line 82
    :cond_0
    :goto_0
    return v0

    .line 42
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->beforeTouchDownEvent()V

    .line 43
    sget-boolean v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->sIsPressed:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mTouchedView:Landroid/view/View;

    if-eq p1, v1, :cond_1

    move v0, v7

    .line 44
    goto :goto_0

    .line 45
    :cond_1
    invoke-virtual {p1, v7}, Landroid/view/View;->setPressed(Z)V

    .line 46
    sput-boolean v7, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->sIsPressed:Z

    .line 47
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mTouchedView:Landroid/view/View;

    .line 48
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mIsQuickIncDecBegins:Z

    .line 49
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener$TimerTaskOne;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener$TimerTaskOne;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener$1;)V

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->LONG_CLICK_DELAY:I

    int-to-long v2, v2

    const-wide/16 v4, 0x32

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    move v0, v7

    .line 50
    goto :goto_0

    .line 53
    :pswitch_1
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 54
    .local v6, "rect":Landroid/graphics/Rect;
    invoke-virtual {p1, v6}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    .line 55
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v6, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-nez v1, :cond_0

    .line 78
    .end local v6    # "rect":Landroid/graphics/Rect;
    :goto_1
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mQuickIncrease:Z

    if-eqz v1, :cond_4

    .line 79
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mQuickIncrease:Z

    goto :goto_0

    .line 62
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mTimer:Ljava/util/Timer;

    if-eqz v1, :cond_2

    .line 63
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mTimer:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 64
    :cond_2
    sget-boolean v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->sIsPressed:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mTouchedView:Landroid/view/View;

    if-ne p1, v1, :cond_0

    .line 65
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mTouchedView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setPressed(Z)V

    .line 66
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mIsQuickIncDecBegins:Z

    if-nez v1, :cond_3

    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->performOnClick()V

    .line 68
    :cond_3
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mIsQuickIncDecBegins:Z

    .line 69
    sput-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->sIsPressed:Z

    .line 70
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mTouchedView:Landroid/view/View;

    goto :goto_1

    .line 80
    :cond_4
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mQuickDecrease:Z

    if-eqz v1, :cond_0

    .line 81
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mQuickDecrease:Z

    goto :goto_0

    .line 40
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public stopQuickIncDec()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mTouchedView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mTouchedView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 114
    sput-boolean v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->sIsPressed:Z

    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecTouchListener;->mTouchedView:Landroid/view/View;

    .line 117
    :cond_1
    return-void
.end method
