.class Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$3;
.super Ljava/lang/Object;
.source "WalkingMateInactiveTimeSetTimeFragment.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2
    .param p1, "arg0"    # Landroid/widget/CompoundButton;
    .param p2, "checked"    # Z

    .prologue
    .line 145
    invoke-static {p2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setInactiveTimeTrackingAvailableTimeAlways(Z)V

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->configButtonStatus()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->access$1100(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)V

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->setInactiveMonitor(I)V

    .line 148
    return-void
.end method
