.class public Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;
.super Landroid/view/View;
.source "HoloCircleSeekBar.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private arc_finish_radians:I

.field private conversion:I

.field private end_wheel:I

.field private init_position:I

.field private isTimerTaskRunning:Z

.field private mAngle:F

.field private mAnimationIntervalTime:I

.field private mArcColor:Landroid/graphics/Paint;

.field private mBackupValue:I

.field private mColorWheelPaint:Landroid/graphics/Paint;

.field private mColorWheelRadius:F

.field private mColorWheelRectangle:Landroid/graphics/RectF;

.field private mContext:Landroid/content/Context;

.field private mCurrentAniValue:I

.field private mCurrentValue:I

.field private mMax:I

.field private mPointerColor:Landroid/graphics/Paint;

.field private mPressHaloPaint:I

.field private mRadiusSize:F

.field private mScore:I

.field private mShader:Landroid/graphics/SweepGradient;

.field private mTimerTask:Ljava/util/TimerTask;

.field private mTranslationOffset:F

.field private pointer_color:I

.field private start_arc:I

.field private text:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 36
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mColorWheelRectangle:Landroid/graphics/RectF;

    .line 41
    const/16 v0, 0xf

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mAnimationIntervalTime:I

    .line 47
    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->conversion:I

    .line 48
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mMax:I

    .line 53
    const/16 v0, 0x168

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->arc_finish_radians:I

    .line 54
    const/16 v0, 0x10e

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->start_arc:I

    .line 62
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mScore:I

    .line 69
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mContext:Landroid/content/Context;

    .line 70
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->init(Landroid/util/AttributeSet;I)V

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 74
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mColorWheelRectangle:Landroid/graphics/RectF;

    .line 41
    const/16 v0, 0xf

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mAnimationIntervalTime:I

    .line 47
    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->conversion:I

    .line 48
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mMax:I

    .line 53
    const/16 v0, 0x168

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->arc_finish_radians:I

    .line 54
    const/16 v0, 0x10e

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->start_arc:I

    .line 62
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mScore:I

    .line 75
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mContext:Landroid/content/Context;

    .line 76
    invoke-direct {p0, p2, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->init(Landroid/util/AttributeSet;I)V

    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 80
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mColorWheelRectangle:Landroid/graphics/RectF;

    .line 41
    const/16 v0, 0xf

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mAnimationIntervalTime:I

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->conversion:I

    .line 48
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mMax:I

    .line 53
    const/16 v0, 0x168

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->arc_finish_radians:I

    .line 54
    const/16 v0, 0x10e

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->start_arc:I

    .line 62
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mScore:I

    .line 81
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mContext:Landroid/content/Context;

    .line 82
    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->init(Landroid/util/AttributeSet;I)V

    .line 83
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->isTimerTaskRunning:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mCurrentAniValue:I

    return v0
.end method

.method static synthetic access$112(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;
    .param p1, "x1"    # I

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mCurrentAniValue:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mCurrentAniValue:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mCurrentValue:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mBackupValue:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->arc_finish_radians:I

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;
    .param p1, "x1"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->arc_finish_radians:I

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mMax:I

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;
    .param p1, "x1"    # F

    .prologue
    .line 29
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mAngle:F

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;F)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;
    .param p1, "x1"    # F

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->calculateAngleFromRadians(F)F

    move-result v0

    return v0
.end method

.method private calculateAngleFromRadians(F)F
    .locals 4
    .param p1, "radians"    # F

    .prologue
    .line 229
    const/high16 v0, 0x43870000    # 270.0f

    add-float/2addr v0, p1

    float-to-double v0, v0

    const-wide v2, 0x401921fb54442d18L    # 6.283185307179586

    mul-double/2addr v0, v2

    const-wide v2, 0x4076800000000000L    # 360.0

    div-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method private calculateAngleFromText(I)D
    .locals 10
    .param p1, "position"    # I

    .prologue
    const-wide v0, 0x4056800000000000L    # 90.0

    .line 207
    if-eqz p1, :cond_0

    iget v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mMax:I

    if-lt p1, v6, :cond_1

    .line 214
    :cond_0
    :goto_0
    return-wide v0

    .line 210
    :cond_1
    iget v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mMax:I

    int-to-double v6, v6

    int-to-double v8, p1

    div-double v2, v6, v8

    .line 211
    .local v2, "f":D
    const-wide v6, 0x4076800000000000L    # 360.0

    div-double v4, v6, v2

    .line 212
    .local v4, "f_r":D
    add-double/2addr v0, v4

    .line 214
    .local v0, "ang":D
    goto :goto_0
.end method

.method private calculateTextFromAngle(F)I
    .locals 4
    .param p1, "angle"    # F

    .prologue
    .line 195
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->start_arc:I

    int-to-float v2, v2

    sub-float v1, p1, v2

    .line 196
    .local v1, "m":F
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->end_wheel:I

    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->start_arc:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    div-float v0, v2, v1

    .line 197
    .local v0, "f":F
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mMax:I

    int-to-float v2, v2

    div-float/2addr v2, v0

    float-to-int v2, v2

    return v2
.end method

.method private calculateTextFromStartAngle(F)I
    .locals 4
    .param p1, "angle"    # F

    .prologue
    .line 201
    move v1, p1

    .line 202
    .local v1, "m":F
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->end_wheel:I

    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->start_arc:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    div-float v0, v2, v1

    .line 203
    .local v0, "f":F
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mMax:I

    int-to-float v2, v2

    div-float/2addr v2, v0

    float-to-int v2, v2

    return v2
.end method

.method private init(Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "attrs"    # Landroid/util/AttributeSet;
    .param p2, "defStyle"    # I

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/R$styleable;->cigna_workout_HoloCircleSeekBar:[I

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 110
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->initAttributes(Landroid/content/res/TypedArray;)V

    .line 112
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 114
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    .line 115
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mShader:Landroid/graphics/SweepGradient;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 116
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->pointer_color:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 117
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 118
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    const/high16 v3, 0x40e00000    # 7.0f

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->convertDpToPx(Landroid/content/Context;F)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 121
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->init_position:I

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->calculateAngleFromText(I)D

    move-result-wide v1

    double-to-int v1, v1

    add-int/lit8 v1, v1, -0x5a

    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->arc_finish_radians:I

    .line 123
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->arc_finish_radians:I

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->end_wheel:I

    if-le v1, v2, :cond_0

    .line 124
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->end_wheel:I

    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->arc_finish_radians:I

    .line 125
    :cond_0
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->arc_finish_radians:I

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->end_wheel:I

    if-le v1, v2, :cond_1

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->end_wheel:I

    int-to-float v1, v1

    :goto_0
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->calculateAngleFromRadians(F)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mAngle:F

    .line 127
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->arc_finish_radians:I

    int-to-float v1, v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->calculateTextFromAngle(F)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->text:Ljava/lang/String;

    .line 128
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->invalidate()V

    .line 130
    return-void

    .line 125
    :cond_1
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->arc_finish_radians:I

    int-to-float v1, v1

    goto :goto_0
.end method

.method private initAttributes(Landroid/content/res/TypedArray;)V
    .locals 3
    .param p1, "a"    # Landroid/content/res/TypedArray;

    .prologue
    const/4 v2, 0x0

    .line 133
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x41600000    # 14.0f

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->convertDpToPx(Landroid/content/Context;F)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mPressHaloPaint:I

    .line 134
    const/4 v0, 0x2

    const/16 v1, 0x64

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mMax:I

    .line 137
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->init_position:I

    .line 139
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->start_arc:I

    .line 140
    const/4 v0, 0x5

    const/16 v1, 0x168

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->end_wheel:I

    .line 146
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->init_position:I

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->start_arc:I

    if-ge v0, v1, :cond_0

    .line 147
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->start_arc:I

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->calculateTextFromStartAngle(F)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->init_position:I

    .line 150
    :cond_0
    const v0, -0xad4df2

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->pointer_color:I

    .line 151
    return-void
.end method


# virtual methods
.method public doTimerTask()V
    .locals 6

    .prologue
    .line 321
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 322
    .local v0, "t":Ljava/util/Timer;
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mCurrentAniValue:I

    .line 323
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mBackupValue:I

    .line 324
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->isTimerTaskRunning:Z

    .line 325
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    if-eqz v1, :cond_0

    .line 327
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    invoke-virtual {v1}, Ljava/util/TimerTask;->cancel()Z

    .line 328
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    .line 330
    :cond_0
    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    .line 346
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    const-wide/16 v2, 0x64

    iget v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mAnimationIntervalTime:I

    int-to-long v4, v4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 348
    return-void
.end method

.method public doUiChanged()V
    .locals 2

    .prologue
    .line 351
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 375
    return-void
.end method

.method public getValue()I
    .locals 1

    .prologue
    .line 238
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->conversion:I

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 301
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 306
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 311
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 312
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 169
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mTranslationOffset:F

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mTranslationOffset:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 171
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mColorWheelRectangle:Landroid/graphics/RectF;

    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->start_arc:I

    add-int/lit16 v0, v0, 0x10e

    int-to-float v2, v0

    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->arc_finish_radians:I

    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->end_wheel:I

    if-le v0, v3, :cond_0

    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->end_wheel:I

    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->start_arc:I

    sub-int/2addr v0, v3

    int-to-float v3, v0

    :goto_0
    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 174
    return-void

    .line 171
    :cond_0
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->arc_finish_radians:I

    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->start_arc:I

    sub-int/2addr v0, v3

    int-to-float v3, v0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 8
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 178
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->getSuggestedMinimumHeight()I

    move-result v3

    invoke-static {v3, p2}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->getDefaultSize(II)I

    move-result v0

    .line 180
    .local v0, "height":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->getSuggestedMinimumWidth()I

    move-result v3

    invoke-static {v3, p1}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->getDefaultSize(II)I

    move-result v2

    .line 181
    .local v2, "width":I
    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 183
    .local v1, "min":I
    int-to-float v3, v1

    float-to-double v3, v3

    const-wide/high16 v5, 0x4000000000000000L    # 2.0

    div-double/2addr v3, v5

    double-to-float v3, v3

    iput v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mRadiusSize:F

    .line 184
    invoke-virtual {p0, v1, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->setMeasuredDimension(II)V

    .line 186
    int-to-float v3, v1

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mTranslationOffset:F

    .line 187
    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mTranslationOffset:F

    iget v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mPressHaloPaint:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x411b3333    # 9.7f

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->convertDpToPx(Landroid/content/Context;F)F

    move-result v4

    add-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mColorWheelRadius:F

    .line 188
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mColorWheelRectangle:Landroid/graphics/RectF;

    iget v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mColorWheelRadius:F

    neg-float v4, v4

    iget v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mColorWheelRadius:F

    neg-float v5, v5

    iget v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mColorWheelRadius:F

    iget v7, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mColorWheelRadius:F

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 192
    return-void
.end method

.method public setMax(I)V
    .locals 1
    .param p1, "max"    # I

    .prologue
    .line 242
    add-int/lit8 v0, p1, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mMax:I

    .line 243
    return-void
.end method

.method public setProgressBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mArcColor:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 157
    return-void
.end method

.method public setProgressColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 161
    return-void
.end method

.method public setProgressWithAnimation(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 257
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->isTimerTaskRunning:Z

    if-eqz v0, :cond_0

    .line 258
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->stopTask()V

    .line 260
    :cond_0
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mCurrentValue:I

    .line 261
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->doTimerTask()V

    .line 262
    return-void
.end method

.method public setScore(I)V
    .locals 2
    .param p1, "score"    # I

    .prologue
    .line 86
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mScore:I

    .line 87
    const v0, -0xad4df2

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->pointer_color:I

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->pointer_color:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 90
    return-void
.end method

.method public setScoreForColor(I)V
    .locals 2
    .param p1, "score"    # I

    .prologue
    .line 93
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mScore:I

    .line 95
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mScore:I

    if-lez v0, :cond_1

    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mScore:I

    const/16 v1, 0x1f

    if-ge v0, v1, :cond_1

    .line 96
    const v0, -0x2cb3ea

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->pointer_color:I

    .line 103
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->pointer_color:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 104
    return-void

    .line 97
    :cond_1
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mScore:I

    const/16 v1, 0x1e

    if-le v0, v1, :cond_2

    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mScore:I

    const/16 v1, 0x47

    if-ge v0, v1, :cond_2

    .line 98
    const v0, -0x116ee8

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->pointer_color:I

    goto :goto_0

    .line 99
    :cond_2
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mScore:I

    const/16 v1, 0x46

    if-le v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mScore:I

    const/16 v1, 0x64

    if-gt v0, v1, :cond_0

    .line 100
    const v0, -0xad4df2

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->pointer_color:I

    goto :goto_0
.end method

.method public stopTask()V
    .locals 1

    .prologue
    .line 378
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->isTimerTaskRunning:Z

    .line 379
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    if-eqz v0, :cond_0

    .line 380
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 381
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    .line 383
    :cond_0
    return-void
.end method
