.class Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;
.super Ljava/lang/Object;
.source "BackupActivity.java"

# interfaces
.implements Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->cignaBackup()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFinished(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;ZLcom/cigna/coach/interfaces/IBackupAndRestore$BRError;Ljava/lang/String;)V
    .locals 8
    .param p1, "requestType"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;
    .param p2, "isSuccess"    # Z
    .param p3, "taskError"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;
    .param p4, "errorMessage"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    const v6, 0x7f0907fc

    const/4 v5, 0x0

    .line 178
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "backup on Finished"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$400(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Landroid/widget/ProgressBar;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Landroid/widget/TextView;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 181
    const/16 v0, 0x64

    .line 182
    .local v0, "percentage":I
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$400(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Landroid/widget/ProgressBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 183
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    const v4, 0x7f090d15

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    .end local v0    # "percentage":I
    :cond_0
    sget-object v1, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NONE:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    if-ne p3, v1, :cond_2

    .line 187
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "backup finished successfully"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 190
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 191
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1, v7}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$602(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 194
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v5}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 206
    :goto_0
    return-void

    .line 198
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "backup failed with error code : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 201
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 202
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1, v7}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$602(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 204
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v5}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V
    .locals 5
    .param p1, "requestType"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;
    .param p2, "fractionCompleted"    # D

    .prologue
    .line 164
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "backup on progress"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$400(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Landroid/widget/ProgressBar;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Landroid/widget/TextView;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 167
    const-wide/high16 v1, 0x4014000000000000L    # 5.0

    mul-double/2addr v1, p2

    double-to-int v1, v1

    add-int/lit8 v0, v1, 0x5f

    .line 168
    .local v0, "percentage":I
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$400(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Landroid/widget/ProgressBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 169
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    const v4, 0x7f090d15

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 172
    .end local v0    # "percentage":I
    :cond_0
    return-void
.end method

.method public onStarted(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;)V
    .locals 2
    .param p1, "requestType"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "backup started"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    return-void
.end method
