.class public Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;
.super Landroid/app/ProgressDialog;
.source "ProgressDlg.java"


# static fields
.field private static final MIN_DURATION:I = 0x258


# instance fields
.field private date:Ljava/util/Date;

.field private handler:Landroid/os/Handler;

.field private r:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0, p1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 17
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->handler:Landroid/os/Handler;

    .line 18
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg$1;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->r:Ljava/lang/Runnable;

    .line 41
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->setProgressStyle(I)V

    .line 42
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->setCancelable(Z)V

    .line 43
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->setCanceledOnTouchOutside(Z)V

    .line 44
    return-void
.end method

.method static synthetic access$001(Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    .prologue
    .line 13
    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x258

    .line 71
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->date:Ljava/util/Date;

    if-eqz v2, :cond_0

    .line 73
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    iget-object v4, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->date:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long v0, v2, v4

    .line 74
    .local v0, "duration":J
    cmp-long v2, v0, v6

    if-lez v2, :cond_1

    .line 76
    invoke-super {p0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 83
    .end local v0    # "duration":J
    :cond_0
    :goto_0
    return-void

    .line 80
    .restart local v0    # "duration":J
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->r:Ljava/lang/Runnable;

    sub-long v4, v6, v0

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public show()V
    .locals 2

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$string;->loading:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->show(Ljava/lang/String;)V

    .line 52
    return-void
.end method

.method public show(Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->r:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 61
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->date:Ljava/util/Date;

    .line 62
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->setMessage(Ljava/lang/CharSequence;)V

    .line 63
    invoke-super {p0}, Landroid/app/ProgressDialog;->show()V

    .line 64
    return-void
.end method
