.class public Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout$SavedState;
.super Landroid/view/View$BaseSavedState;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field lockModeLeft:I

.field lockModeRight:I

.field openDrawerGravity:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout$SavedState$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout$SavedState$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout$SavedState;->openDrawerGravity:I

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout$SavedState;->lockModeLeft:I

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout$SavedState;->lockModeRight:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout$SavedState;->openDrawerGravity:I

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout$SavedState;->openDrawerGravity:I

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout$SavedState;->lockModeLeft:I

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout$SavedState;->lockModeRight:I

    return-void
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout$SavedState;->openDrawerGravity:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
