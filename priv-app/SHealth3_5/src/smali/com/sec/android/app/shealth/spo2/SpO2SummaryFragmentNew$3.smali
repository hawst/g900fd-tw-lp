.class Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$3;
.super Ljava/lang/Object;
.source "SpO2SummaryFragmentNew.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->startSensorforSCover(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V
    .locals 0

    .prologue
    .line 484
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$3;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 486
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$3;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$900(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    move-result-object v0

    if-nez v0, :cond_0

    .line 487
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$3;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-static {}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->getInstance()Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$902(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;)Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    .line 488
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$3;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$900(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$3;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$600(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getAppContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$3;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->startMeasuring(Landroid/content/Context;Lcom/sec/android/app/shealth/spo2/SpO2SensorListener;Z)V

    .line 492
    :goto_0
    return-void

    .line 490
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$3;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$900(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$3;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$600(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getAppContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$3;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->startMeasuring(Landroid/content/Context;Lcom/sec/android/app/shealth/spo2/SpO2SensorListener;Z)V

    goto :goto_0
.end method
