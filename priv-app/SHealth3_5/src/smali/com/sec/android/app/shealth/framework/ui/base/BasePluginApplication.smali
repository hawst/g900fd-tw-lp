.class public Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication;
.super Landroid/app/Application;


# static fields
.field public static GENERATE_SAMPLE_DATA:Z

.field private static LOG_TAG:Ljava/lang/String;

.field public static RELEASE:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const-class v0, Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication;->LOG_TAG:Ljava/lang/String;

    sput-boolean v1, Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication;->RELEASE:Z

    sput-boolean v1, Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication;->GENERATE_SAMPLE_DATA:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public onCreate()V
    .locals 33

    invoke-super/range {p0 .. p0}, Landroid/app/Application;->onCreate()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->setContext(Landroid/content/Context;)V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->getInstance()Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->setInitialized(Z)V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeHelper;->isSHealthUpgradeNeeded(Landroid/content/Context;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication;->LOG_TAG:Ljava/lang/String;

    const/16 v1, 0x17

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x744a

    aput v26, v2, v25

    const/16 v25, 0x4411

    aput v25, v2, v24

    const/16 v24, 0x5120

    aput v24, v2, v23

    const/16 v23, -0x32d0

    aput v23, v2, v22

    const/16 v22, -0x41

    aput v22, v2, v21

    const/16 v21, 0x706

    aput v21, v2, v20

    const/16 v20, 0x1f77

    aput v20, v2, v19

    const/16 v19, -0x42b6

    aput v19, v2, v18

    const/16 v18, -0x63

    aput v18, v2, v17

    const/16 v17, -0x35

    aput v17, v2, v16

    const/16 v16, -0x34

    aput v16, v2, v15

    const/16 v15, -0x5f

    aput v15, v2, v14

    const/16 v14, -0xb

    aput v14, v2, v13

    const/16 v13, -0x648e

    aput v13, v2, v12

    const/16 v12, -0x45

    aput v12, v2, v11

    const/4 v11, -0x8

    aput v11, v2, v10

    const/16 v10, 0x1f41

    aput v10, v2, v9

    const/16 v9, 0x6f73

    aput v9, v2, v8

    const/16 v8, 0x240e

    aput v8, v2, v7

    const/16 v7, -0x64bf

    aput v7, v2, v6

    const/16 v6, -0x2d

    aput v6, v2, v5

    const/16 v5, 0x603b

    aput v5, v2, v3

    const/16 v3, -0x73cd

    aput v3, v2, v1

    const/16 v1, 0x17

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x7464

    aput v27, v1, v26

    const/16 v26, 0x4474

    aput v26, v1, v25

    const/16 v25, 0x5144

    aput v25, v1, v24

    const/16 v24, -0x32af

    aput v24, v1, v23

    const/16 v23, -0x33

    aput v23, v1, v22

    const/16 v22, 0x761

    aput v22, v1, v21

    const/16 v21, 0x1f07

    aput v21, v1, v20

    const/16 v20, -0x42e1

    aput v20, v1, v19

    const/16 v19, -0x43

    aput v19, v1, v18

    const/16 v18, -0x48

    aput v18, v1, v17

    const/16 v17, -0x58

    aput v17, v1, v16

    const/16 v16, -0x3c

    aput v16, v1, v15

    const/16 v15, -0x70

    aput v15, v1, v14

    const/16 v14, -0x64e4

    aput v14, v1, v13

    const/16 v13, -0x65

    aput v13, v1, v12

    const/16 v12, -0x70

    aput v12, v1, v11

    const/16 v11, 0x1f35

    aput v11, v1, v10

    const/16 v10, 0x6f1f

    aput v10, v1, v9

    const/16 v9, 0x246f

    aput v9, v1, v8

    const/16 v8, -0x64dc

    aput v8, v1, v7

    const/16 v7, -0x65

    aput v7, v1, v6

    const/16 v6, 0x601b

    aput v6, v1, v5

    const/16 v5, -0x73a0

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_1

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_2
    return-void

    :cond_1
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeHelper;->isHealthServiceUpgradeNeeded(Landroid/content/Context;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_6

    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication;->LOG_TAG:Ljava/lang/String;

    const/16 v1, 0x1c

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x5e2e

    aput v31, v2, v30

    const/16 v30, 0x653b

    aput v30, v2, v29

    const/16 v29, -0x3ff

    aput v29, v2, v28

    const/16 v28, -0x63

    aput v28, v2, v27

    const/16 v27, 0x36f

    aput v27, v2, v26

    const/16 v26, 0x4064

    aput v26, v2, v25

    const/16 v25, 0x30

    aput v25, v2, v24

    const/16 v24, -0x37ab

    aput v24, v2, v23

    const/16 v23, -0x18

    aput v23, v2, v22

    const/16 v22, -0x5f

    aput v22, v2, v21

    const/16 v21, -0x2da5

    aput v21, v2, v20

    const/16 v20, -0x49

    aput v20, v2, v19

    const/16 v19, 0x7265

    aput v19, v2, v18

    const/16 v18, 0x401c

    aput v18, v2, v17

    const/16 v17, -0x1ba0

    aput v17, v2, v16

    const/16 v16, -0x7f

    aput v16, v2, v15

    const/16 v15, -0x67

    aput v15, v2, v14

    const/16 v14, -0x1b

    aput v14, v2, v13

    const/16 v13, -0x1adc

    aput v13, v2, v12

    const/16 v12, -0x69

    aput v12, v2, v11

    const/16 v11, -0x46fd

    aput v11, v2, v10

    const/16 v10, -0x16

    aput v10, v2, v9

    const/16 v9, -0x68

    aput v9, v2, v8

    const/16 v8, -0x49

    aput v8, v2, v7

    const/16 v7, -0x3fe9

    aput v7, v2, v6

    const/16 v6, -0x5f

    aput v6, v2, v5

    const/16 v5, -0x61

    aput v5, v2, v3

    const/16 v3, -0x7e

    aput v3, v2, v1

    const/16 v1, 0x1c

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x5e00

    aput v32, v1, v31

    const/16 v31, 0x655e

    aput v31, v1, v30

    const/16 v30, -0x39b

    aput v30, v1, v29

    const/16 v29, -0x4

    aput v29, v1, v28

    const/16 v28, 0x31d

    aput v28, v1, v27

    const/16 v27, 0x4003

    aput v27, v1, v26

    const/16 v26, 0x40

    aput v26, v1, v25

    const/16 v25, -0x3800

    aput v25, v1, v24

    const/16 v24, -0x38

    aput v24, v1, v23

    const/16 v23, -0x2e

    aput v23, v1, v22

    const/16 v22, -0x2dc1

    aput v22, v1, v21

    const/16 v21, -0x2e

    aput v21, v1, v20

    const/16 v20, 0x7200

    aput v20, v1, v19

    const/16 v19, 0x4072

    aput v19, v1, v18

    const/16 v18, -0x1bc0

    aput v18, v1, v17

    const/16 v17, -0x1c

    aput v17, v1, v16

    const/16 v16, -0x6

    aput v16, v1, v15

    const/16 v15, -0x74

    aput v15, v1, v14

    const/16 v14, -0x1aae

    aput v14, v1, v13

    const/16 v13, -0x1b

    aput v13, v1, v12

    const/16 v12, -0x469a

    aput v12, v1, v11

    const/16 v11, -0x47

    aput v11, v1, v10

    const/16 v10, -0x10

    aput v10, v1, v9

    const/16 v9, -0x3d

    aput v9, v1, v8

    const/16 v8, -0x3f85

    aput v8, v1, v7

    const/16 v7, -0x40

    aput v7, v1, v6

    const/4 v6, -0x6

    aput v6, v1, v5

    const/16 v5, -0x36

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_4
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_5
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_6
    sget-boolean v1, Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication;->RELEASE:Z

    if-eqz v1, :cond_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication$1;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication;)V

    invoke-virtual {v1, v2}, Ljava/lang/Thread;->setUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    goto/16 :goto_2
.end method
