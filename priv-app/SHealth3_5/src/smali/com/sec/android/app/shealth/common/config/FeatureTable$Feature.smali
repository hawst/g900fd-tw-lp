.class Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;
.super Ljava/lang/Object;
.source "FeatureTable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/config/FeatureTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Feature"
.end annotation


# instance fields
.field private bloodGlucose:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodGlucose;

.field private bloodPressure:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;

.field private eur:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

.field private heartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

.field private sleep:Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;

.field private spo2:Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;

.field private stress:Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;

.field private uv:Lcom/sec/android/app/shealth/common/config/FeatureTable$UV;

.field private weight:Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;


# direct methods
.method private constructor <init>()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;->SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->bloodPressure:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;

    .line 95
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodGlucose;->SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodGlucose;

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->bloodGlucose:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodGlucose;

    .line 96
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;->SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->stress:Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;

    .line 97
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;->SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->sleep:Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;

    .line 98
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;->FULL:Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->spo2:Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;

    .line 99
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;->FULL:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->heartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    .line 100
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;->SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->weight:Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    .line 101
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$UV;->SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$UV;

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->uv:Lcom/sec/android/app/shealth/common/config/FeatureTable$UV;

    .line 105
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Do not use this constructor"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private constructor <init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;)V
    .locals 1
    .param p1, "eur"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;->SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->bloodPressure:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;

    .line 95
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodGlucose;->SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodGlucose;

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->bloodGlucose:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodGlucose;

    .line 96
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;->SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->stress:Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;

    .line 97
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;->SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->sleep:Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;

    .line 98
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;->FULL:Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->spo2:Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;

    .line 99
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;->FULL:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->heartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    .line 100
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;->SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->weight:Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    .line 101
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$UV;->SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$UV;

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->uv:Lcom/sec/android/app/shealth/common/config/FeatureTable$UV;

    .line 110
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->eur:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    .line 111
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;Lcom/sec/android/app/shealth/common/config/FeatureTable$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$1;

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;-><init>(Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->stress:Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->stress:Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->sleep:Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->sleep:Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;)Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->spo2:Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;)Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->spo2:Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;)Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->heartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;)Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->heartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;)Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->bloodPressure:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;)Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->bloodPressure:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;)Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodGlucose;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->bloodGlucose:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodGlucose;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodGlucose;)Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodGlucose;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodGlucose;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->bloodGlucose:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodGlucose;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->weight:Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->weight:Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;)Lcom/sec/android/app/shealth/common/config/FeatureTable$UV;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->uv:Lcom/sec/android/app/shealth/common/config/FeatureTable$UV;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;Lcom/sec/android/app/shealth/common/config/FeatureTable$UV;)Lcom/sec/android/app/shealth/common/config/FeatureTable$UV;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$UV;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->uv:Lcom/sec/android/app/shealth/common/config/FeatureTable$UV;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;)Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Feature;->eur:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    return-object v0
.end method
