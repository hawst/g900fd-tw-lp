.class public final enum Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;
.super Ljava/lang/Enum;
.source "WeightUnitHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "HeightType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

.field public static final enum CM_UNIT:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

.field public static final enum INCH_UNIT:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

.field public static final enum NOT_DEFINED:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;


# instance fields
.field private mUnitNameId:I

.field private mUntranslatableName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 97
    new-instance v0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

    const-string v1, "CM_UNIT"

    const-string v2, "cm"

    const v3, 0x7f0900bc

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;->CM_UNIT:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

    .line 98
    new-instance v0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

    const-string v1, "INCH_UNIT"

    const-string v2, "ft, inch"

    const v3, 0x7f0900bd

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;->INCH_UNIT:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

    .line 99
    new-instance v0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

    const-string v1, "NOT_DEFINED"

    const-string/jumbo v2, "not defined"

    const v3, 0x7f090d0f

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;->NOT_DEFINED:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

    .line 96
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

    sget-object v1, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;->CM_UNIT:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;->INCH_UNIT:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;->NOT_DEFINED:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;->$VALUES:[Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "unitId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 115
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 116
    iput p4, p0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;->mUnitNameId:I

    .line 117
    iput-object p3, p0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;->mUntranslatableName:Ljava/lang/String;

    .line 118
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;->mUntranslatableName:Ljava/lang/String;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 96
    const-class v0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;->$VALUES:[Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

    return-object v0
.end method


# virtual methods
.method public getUnitName(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 112
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;->mUnitNameId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
