.class public Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;
.super Ljava/lang/Object;
.source "HtoKMigrationUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil$MigrationStatus;
    }
.end annotation


# static fields
.field private static final APPLICATION_ID_SHEALTH_OLD:Ljava/lang/String; = "shealth2.5"

.field private static BULKINSERT_SIZE:I = 0x0

.field private static final EXERCISE_INFO_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final GEAR_DEVICE_NAME_FOR_H:Ljava/lang/String; = "2.5"

.field private static final H_EXINFO_CYCLING:I = 0xe

.field private static final H_EXINFO_RUNNING:I = 0x3

.field private static final H_EXINFO_WALKING:I = 0x1

.field private static final H_PREFIX:Ljava/lang/String; = ""

.field private static final MIGRATE_URI:Landroid/net/Uri;

.field private static final MIGRATE_URI_KIES:Landroid/net/Uri;

.field private static MIGRATION_START_TIME:J = 0x0L

.field private static final MIN_TO_MILLI_FACTOR:I = 0xea60

.field private static final PEDOMETER_DEVICE_MODEL_NAME:Ljava/lang/String; = "Shealth2.5"

.field private static final PEDOMETER_DEVICE_NAME_FOR_H:Ljava/lang/String; = "2.5"

.field private static final PEDOMETER_DEVICE_NAME_FOR_J:Ljava/lang/String; = "1.5"

.field private static final PEDOMETER_DEVICE_NAME_FOR_JMR:Ljava/lang/String; = "2.0"

.field public static final PROGRESS_AS_PERCENTAGE:Ljava/lang/String; = "progress_percent"

.field private static SHARED_PREF_TIMEZONE_ID:Ljava/lang/String; = null

.field public static final SHEALTH_RESTORE_START_BROADCAST_BR:Ljava/lang/String; = "com.sec.android.app.shealth.RESTORE_PROGRESS_PERCENT"

.field private static final TAG:Ljava/lang/String;

.field private static deviceId:J

.field private static exerciseId:J

.field private static exerciseInfoId:J

.field private static exercise_last_id:J

.field private static isMigratingFromH:Z

.field private static last_createTime:J

.field private static last_exerciseId:J

.field private static mIsKiesMode:Z

.field private static record_number:J

.field private static status:Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil$MigrationStatus;

.field private static total_calorie:F

.field private static total_distance:F

.field private static total_stepcount:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 74
    const-class v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    .line 75
    sput-wide v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->exerciseId:J

    .line 76
    sput-wide v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->exerciseInfoId:J

    .line 77
    sput-wide v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->deviceId:J

    .line 86
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->isMigratingFromH:Z

    .line 88
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string/jumbo v1, "migrate"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->MIGRATE_URI:Landroid/net/Uri;

    .line 89
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string/jumbo v1, "migrate_kies"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->MIGRATE_URI_KIES:Landroid/net/Uri;

    .line 101
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->SHARED_PREF_TIMEZONE_ID:Ljava/lang/String;

    .line 103
    const/16 v0, 0xc8

    sput v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->BULKINSERT_SIZE:I

    .line 105
    const-wide v0, 0x7fffffffffffffffL

    sput-wide v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->exercise_last_id:J

    .line 108
    const-wide v0, 0x1349562b040L

    sput-wide v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->MIGRATION_START_TIME:J

    .line 110
    sput-wide v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->record_number:J

    .line 128
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil$MigrationStatus;->IDLE:Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil$MigrationStatus;

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->status:Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil$MigrationStatus;

    .line 129
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->mIsKiesMode:Z

    .line 212
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/16 v3, 0x2713

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 214
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2712

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2af9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2714

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2715

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2ee1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2ee3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2ee5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2ee9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0xb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2eea

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2eed

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x32c9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0xe

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x36b2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0xf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2eef

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x10

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x32cb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x11

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x32d1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x12

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x3e81

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x13

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x3e83

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x14

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x3e82

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x15

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2ef1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x16

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2ef2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x17

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2ef4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x18

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2efc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x19

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2efd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x1a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2eff

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x1b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2f01

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x1c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x3a9a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x1d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2f02

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x1e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2f03

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x1f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2f05

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x20

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2f08

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x21

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2f1b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x22

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2f0a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x23

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2f0b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x24

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2f0e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x25

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2f10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x26

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2f11

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x27

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2f12

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x28

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2f13

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x29

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2f16

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x2a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2f17

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x2b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x32d4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x2c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x3a9e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x2d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2f19

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x2e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x36b5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    const/16 v1, 0x2f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2f1a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    return-void
.end method

.method private static addExerciseDeviceInfo(JLjava/lang/String;Landroid/content/Context;)V
    .locals 10
    .param p0, "cur_exercise_id"    # J
    .param p2, "connectedDeviceId"    # Ljava/lang/String;
    .param p3, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 732
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " addExerciseDeviceInfo cur_exercise_id "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " connectedDeviceId "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 733
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    const-string v1, "exercise_device_info"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "exercise__id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "user_device__id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 736
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_1

    .line 738
    :cond_0
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 740
    .local v9, "values":Landroid/content/ContentValues;
    const-string v0, "create_time"

    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 741
    const-string/jumbo v0, "update_time"

    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 742
    const-string/jumbo v0, "time_zone"

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDeviceTimeZone()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 743
    const-string/jumbo v0, "sync_status"

    const v1, 0x29812

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 744
    const-string v0, "exercise__id"

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 745
    const-string/jumbo v0, "user_device__id"

    invoke-virtual {v9, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 746
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " addExerciseDeviceInfo Values "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 747
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getMigrationUri()Landroid/net/Uri;

    move-result-object v0

    const-string v1, "exercise_device_info"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p3, v0, v9}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->cpInsert(Landroid/content/Context;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 752
    .end local v9    # "values":Landroid/content/ContentValues;
    :cond_1
    if-eqz v8, :cond_2

    .line 754
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 759
    :cond_2
    return-void

    .line 752
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_3

    .line 754
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private static addMaxMinEleveationFromMap(Landroid/content/ContentValues;JLsamsung/database/sqlite/SecSQLiteDatabase;)V
    .locals 6
    .param p0, "contentValues"    # Landroid/content/ContentValues;
    .param p1, "exerciseId"    # J
    .param p3, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;

    .prologue
    .line 451
    const/4 v2, 0x1

    .line 452
    .local v2, "max_elevation":F
    const v3, 0x7f7fffff    # Float.MAX_VALUE

    .line 453
    .local v3, "min_elevation":F
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SELECT * FROM map_path where db_index = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {p3, v4, v5}, Lsamsung/database/sqlite/SecSQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 456
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-gtz v4, :cond_2

    .line 469
    :cond_0
    if-eqz v0, :cond_1

    .line 471
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 478
    :cond_1
    :goto_0
    return-void

    .line 458
    :cond_2
    :goto_1
    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 460
    const-string v4, "altitude"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getFloat(I)F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    .line 461
    .local v1, "elevation":F
    cmpg-float v4, v1, v3

    if-gez v4, :cond_3

    .line 462
    move v3, v1

    .line 463
    :cond_3
    cmpl-float v4, v1, v2

    if-lez v4, :cond_2

    .line 464
    move v2, v1

    goto :goto_1

    .line 469
    .end local v1    # "elevation":F
    :cond_4
    if-eqz v0, :cond_5

    .line 471
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 475
    :cond_5
    const-string/jumbo v4, "max_altitude"

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 476
    const-string/jumbo v4, "min_altitude"

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    goto :goto_0

    .line 469
    :catchall_0
    move-exception v4

    if-eqz v0, :cond_6

    .line 471
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v4
.end method

.method private static addPedometerUserDevice(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V
    .locals 8
    .param p0, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1846
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1847
    .local v0, "contentValues":Landroid/content/ContentValues;
    const/16 v1, 0x2719

    .line 1848
    .local v1, "deviceId":I
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    .line 1849
    .local v2, "deviceName":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1850
    .local v4, "scontextDeviceId":Ljava/lang/String;
    const-string v5, "application__id"

    const-string/jumbo v6, "shealth2.5"

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1851
    const-string v5, "_id"

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1852
    const-string v5, "device_type"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1853
    const-string v5, "device_id"

    invoke-virtual {v0, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1854
    const-string v5, "device_group_type"

    const v6, 0x57e41

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1855
    const-string/jumbo v5, "model"

    const-string v6, "Shealth2.5"

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1856
    const-string v5, "connectivity_type"

    const/4 v6, 0x4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1857
    const-string/jumbo v5, "time_zone"

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDeviceTimeZone()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1858
    const-string v5, "create_time"

    sget-wide v6, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->MIGRATION_START_TIME:J

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1859
    const-string/jumbo v5, "update_time"

    sget-wide v6, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->MIGRATION_START_TIME:J

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1860
    const-string/jumbo v5, "sync_status"

    const v6, 0x29812

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1861
    sget-object v5, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "addPedometerUserDevice "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1862
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getMigrationUri()Landroid/net/Uri;

    move-result-object v5

    const-string/jumbo v6, "user_device"

    invoke-static {v5, v6}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {p1, v5, v0}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->cpInsert(Landroid/content/Context;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1869
    .end local v0    # "contentValues":Landroid/content/ContentValues;
    .end local v1    # "deviceId":I
    .end local v2    # "deviceName":Ljava/lang/String;
    .end local v4    # "scontextDeviceId":Ljava/lang/String;
    :goto_0
    return-void

    .line 1864
    :catch_0
    move-exception v3

    .line 1866
    .local v3, "e":Ljava/lang/Exception;
    sget-object v5, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " Failed to insert UserDevice data "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1867
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static cancelMigration()V
    .locals 2

    .prologue
    .line 193
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->status:Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil$MigrationStatus;

    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil$MigrationStatus;->ONGOING:Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil$MigrationStatus;

    if-ne v0, v1, :cond_0

    .line 195
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil$MigrationStatus;->STOP_REQUESTED:Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil$MigrationStatus;

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->status:Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil$MigrationStatus;

    .line 196
    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->deleteBackupDatabase()V

    .line 198
    :cond_0
    return-void
.end method

.method private static cpInsert(Landroid/content/Context;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 1954
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->status:Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil$MigrationStatus;

    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil$MigrationStatus;->STOP_REQUESTED:Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil$MigrationStatus;

    if-ne v0, v1, :cond_0

    .line 1956
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    .line 1958
    :cond_0
    const-string/jumbo v0, "validation_policy"

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getMigrationPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->asMask()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1959
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static createExerciseAcitvity(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/database/Cursor;Landroid/content/Context;)V
    .locals 7
    .param p0, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const v6, 0xea60

    .line 379
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v3, " createExerciseAcitvity"

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    const-string/jumbo v2, "sensor_id"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2, p2}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getKInputSrouceType(ILandroid/content/Context;)I

    move-result v2

    const v3, 0x3f7a1

    if-ne v2, v3, :cond_0

    .line 382
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v3, " Input type is manual : No need to create ExerciseActivity"

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 446
    :goto_0
    return-void

    .line 387
    :cond_0
    const-string/jumbo v2, "sensor_id"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_1

    .line 389
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v3, " No need to create ExerciseActivity for walk for life exercises"

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 412
    :cond_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 413
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v2, "exercise__id"

    const-string v3, "_id"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 414
    const-string v2, "application__id"

    const-string/jumbo v3, "shealth2.5"

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    const-string v2, "distance"

    const-string v3, "distance"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 416
    const-string v2, "cadence"

    const-string/jumbo v3, "step_counts"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 417
    const-string v2, "calorie"

    const-string v3, "calories"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 418
    const-string/jumbo v2, "mean_heart_rate_per_min"

    const-string v3, "average_heart_beat"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 419
    const-string v2, "duration_min"

    const-string/jumbo v3, "min"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 420
    const-string v2, "duration_millisecond"

    const-string/jumbo v3, "min"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    mul-int/2addr v3, v6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 421
    const-string v2, "comment"

    const-string v3, "comment"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 422
    const-string/jumbo v2, "max_speed_per_hour"

    const-string/jumbo v3, "max_speed"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 423
    const-string/jumbo v2, "start_time"

    const-string v3, "create_time"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 424
    const-string v2, "end_time"

    const-string v3, "create_time"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const-string/jumbo v5, "min"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    mul-int/2addr v5, v6

    int-to-long v5, v5

    add-long/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 425
    const-string v2, "create_time"

    const-string v3, "_id"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 426
    const-string/jumbo v2, "update_time"

    const-string v3, "_id"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 427
    const-string/jumbo v2, "sync_status"

    const v3, 0x29812

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 428
    const-string/jumbo v2, "time_zone"

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDeviceTimeZone()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 429
    const-string/jumbo v2, "mean_speed_per_hour"

    const-string/jumbo v3, "walk_speed"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 430
    const-string/jumbo v2, "max_heart_rate_per_min"

    const-string/jumbo v3, "pulse"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 433
    :try_start_0
    sget-wide v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->exerciseId:J

    invoke-static {v0, v2, v3, p0}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->addMaxMinEleveationFromMap(Landroid/content/ContentValues;JLsamsung/database/sqlite/SecSQLiteDatabase;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 444
    :goto_1
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " ExerciseActivity content Values "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getMigrationUri()Landroid/net/Uri;

    move-result-object v2

    const-string v3, "exercise_activity"

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {p2, v2, v0}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->cpInsert(Landroid/content/Context;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto/16 :goto_0

    .line 435
    :catch_0
    move-exception v1

    .line 438
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " Exception Occured "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private static createGearPedometerDeviceIfNotExist(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V
    .locals 13
    .param p0, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 1875
    const/16 v10, 0x2724

    .line 1876
    .local v10, "deviceId":I
    const-string v11, "2.5"

    .line 1877
    .local v11, "deviceName":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 1878
    .local v12, "gearDeviceId":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    const-string/jumbo v1, "user_device"

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 1881
    .local v9, "cursor":Landroid/database/Cursor;
    if-eqz v9, :cond_1

    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 1883
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v1, " Gear user device elaredy exist no need to restore "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1889
    if-eqz v9, :cond_0

    .line 1891
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 1909
    :cond_0
    :goto_0
    return-void

    .line 1889
    :cond_1
    if-eqz v9, :cond_2

    .line 1891
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 1894
    :cond_2
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 1895
    .local v8, "contentValues":Landroid/content/ContentValues;
    const-string v0, "application__id"

    const-string/jumbo v1, "shealth2.5"

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1896
    const-string v0, "_id"

    invoke-virtual {v8, v0, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1897
    const-string v0, "device_type"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1898
    const-string v0, "device_group_type"

    const v1, 0x57e43

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1899
    const-string v0, "device_id"

    invoke-virtual {v8, v0, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1900
    const-string/jumbo v0, "time_zone"

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDeviceTimeZone()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1902
    const-string v0, "create_time"

    sget-wide v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->MIGRATION_START_TIME:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1903
    const-string/jumbo v0, "update_time"

    sget-wide v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->MIGRATION_START_TIME:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1904
    const-string/jumbo v0, "sync_status"

    const v1, 0x29812

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1905
    const-string v0, "connectivity_type"

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1907
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addGearUserDevice "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1908
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getMigrationUri()Landroid/net/Uri;

    move-result-object v0

    const-string/jumbo v1, "user_device"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p1, v0, v8}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->cpInsert(Landroid/content/Context;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto/16 :goto_0

    .line 1889
    .end local v8    # "contentValues":Landroid/content/ContentValues;
    :catchall_0
    move-exception v0

    if-eqz v9, :cond_3

    .line 1891
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private static createNewExerciseForWalk(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/ContentValues;Landroid/content/Context;)J
    .locals 9
    .param p0, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p1, "contentValues"    # Landroid/content/ContentValues;
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const-wide v7, 0x7fffffffffffffffL

    const-wide/16 v5, 0x1

    .line 857
    sget-wide v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->exercise_last_id:J

    cmp-long v2, v2, v7

    if-nez v2, :cond_0

    .line 859
    const-wide/32 v2, 0xf423f

    sput-wide v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->exercise_last_id:J

    .line 861
    :cond_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 862
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "application__id"

    const-string/jumbo v3, "shealth2.5"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 863
    const-string v2, "exercise_info__id"

    const/16 v3, 0x4651

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 864
    const-string/jumbo v2, "start_time"

    const-string/jumbo v3, "start_time"

    invoke-virtual {p1, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 865
    const-string v2, "end_time"

    const-string/jumbo v3, "start_time"

    invoke-virtual {p1, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 866
    const-string v2, "create_time"

    sget-wide v3, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->exercise_last_id:J

    add-long/2addr v3, v5

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 867
    const-string/jumbo v2, "update_time"

    sget-wide v3, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->exercise_last_id:J

    add-long/2addr v3, v5

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 868
    const-string/jumbo v2, "time_zone"

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDeviceTimeZone()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 869
    const-string v2, "input_source_type"

    const v3, 0x3f7a2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 870
    const-string/jumbo v2, "sync_status"

    const v3, 0x29812

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 871
    const-string v2, "distance"

    const-string v3, "distance"

    invoke-virtual {p1, v3}, Landroid/content/ContentValues;->getAsFloat(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 872
    const-string/jumbo v2, "total_calorie"

    const-string v3, "calorie"

    invoke-virtual {p1, v3}, Landroid/content/ContentValues;->getAsFloat(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 875
    const-string v2, "exercise_type"

    const/16 v3, 0x4e23

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 876
    const-string v2, "duration_min"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 877
    const-string v2, "comment"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 878
    const-string v2, "cadence"

    const-string/jumbo v3, "total_step"

    invoke-virtual {p1, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 879
    sget-wide v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->exercise_last_id:J

    cmp-long v2, v2, v7

    if-eqz v2, :cond_1

    .line 881
    const-string v2, "_id"

    sget-wide v3, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->exercise_last_id:J

    add-long/2addr v3, v5

    sput-wide v3, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->exercise_last_id:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 882
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " creating exercise with _id "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-wide v4, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->exercise_last_id:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 884
    :cond_1
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " createNewExerciseForWalk Values "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 885
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getMigrationUri()Landroid/net/Uri;

    move-result-object v2

    const-string v3, "exercise"

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {p2, v2, v1}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->cpInsert(Landroid/content/Context;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 886
    .local v0, "insertUri":Landroid/net/Uri;
    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    return-wide v2
.end method

.method private static getBWalkInfoContentValuesFromCursor(Landroid/database/Cursor;Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)Landroid/content/ContentValues;
    .locals 9
    .param p0, "cursor"    # Landroid/database/Cursor;
    .param p1, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 891
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 892
    .local v1, "contentValues":Landroid/content/ContentValues;
    const-string v4, "application__id"

    const-string/jumbo v5, "shealth2.5"

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 893
    const-string v4, "distance"

    const-string v5, "distance"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getFloat(I)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 894
    const-string v4, "db_index"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 895
    .local v2, "exerciseId":I
    const-string v4, "exercise__id"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 896
    const-string v4, "calorie"

    const-string v5, "kcal"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getFloat(I)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 897
    const-string/jumbo v4, "run_step"

    const-string/jumbo v5, "run_steps"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 898
    const-string/jumbo v4, "speed"

    const-string/jumbo v5, "walk_speed"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getFloat(I)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 899
    const-string/jumbo v4, "total_step"

    const-string/jumbo v5, "total_steps"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 900
    const-string/jumbo v4, "updown_step"

    const-string/jumbo v5, "updown_steps"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 901
    const-string/jumbo v4, "walk_step"

    const-string/jumbo v5, "walk_steps"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 902
    const-string v4, "create_time"

    const-string v5, "_id"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 903
    const-string/jumbo v4, "update_time"

    const-string v5, "_id"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 904
    const-string/jumbo v4, "time_zone"

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDeviceTimeZone()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 905
    const-string/jumbo v4, "start_time"

    const-string v5, "create_time"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 908
    const-string v4, "end_time"

    const-string v5, "create_time"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    const-wide/32 v7, 0x36ee80

    add-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 909
    const-string/jumbo v4, "sync_status"

    const v5, 0x29812

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 910
    const-string/jumbo v4, "sample_position"

    const v5, 0x38271

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 911
    const-string v0, "10020_2.5"

    .line 912
    .local v0, "connectedDeviceId":Ljava/lang/String;
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->createGearPedometerDeviceIfNotExist(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V

    .line 913
    const-string/jumbo v4, "user_device__id"

    invoke-virtual {v1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 915
    int-to-long v4, v2

    invoke-static {v4, v5, v0, p2}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->addExerciseDeviceInfo(JLjava/lang/String;Landroid/content/Context;)V

    .line 917
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 918
    .local v3, "update":Landroid/content/ContentValues;
    const-string v4, "exercise_info__id"

    const/16 v5, 0x4651

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 919
    const-string v4, "exercise_type"

    const/16 v5, 0x4e23

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 920
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getMigrationUri()Landroid/net/Uri;

    move-result-object v5

    const-string v6, "exercise"

    invoke-static {v5, v6}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "_id = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 922
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " getBWalkInfoContentValuesFromCursor Values "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 923
    return-object v1
.end method

.method private static getBloodGlucoseContentValuesFromCursor(Landroid/database/Cursor;Landroid/content/Context;)Landroid/content/ContentValues;
    .locals 4
    .param p0, "cursor"    # Landroid/database/Cursor;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1602
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1603
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v1, "application__id"

    const-string/jumbo v2, "shealth2.5"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1604
    const-string v1, "comment"

    const-string v2, "comment"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1605
    const-string v1, "glucose"

    const-string/jumbo v2, "value"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    const-string/jumbo v3, "unit"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getGlucoseValue(FLjava/lang/String;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1606
    const-string v1, "input_source_type"

    const-string/jumbo v2, "sensor_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2, p1}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getKInputSrouceType(ILandroid/content/Context;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1607
    const-string/jumbo v1, "sample_time"

    const-string v2, "create_time"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1608
    const-string v1, "create_time"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1609
    const-string/jumbo v1, "update_time"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1610
    const-string/jumbo v1, "time_zone"

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDeviceTimeZone()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1611
    const-string/jumbo v1, "sync_status"

    const v2, 0x29812

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1612
    const-string v1, "_id"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1613
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " BloodGlucose Values "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1614
    return-object v0
.end method

.method private static getBloodGlucoseMeasurementContextContentValuesFromCursor(Landroid/database/Cursor;Landroid/content/Context;)Landroid/content/ContentValues;
    .locals 4
    .param p0, "cursor"    # Landroid/database/Cursor;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1619
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1620
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v1, "application__id"

    const-string/jumbo v2, "shealth2.5"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1621
    const-string v2, "context_type"

    const-string/jumbo v1, "type"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_0

    const v1, 0x13881

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1622
    const-string v1, "create_time"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1623
    const-string/jumbo v1, "update_time"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1624
    const-string/jumbo v1, "time_zone"

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDeviceTimeZone()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1625
    const-string/jumbo v1, "sync_status"

    const v2, 0x29812

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1626
    const-string v1, "_id"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1627
    const-string v1, "blood_glucose__id"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1628
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " BloodGlucoseMeasurementContext Values "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1629
    return-object v0

    .line 1621
    :cond_0
    const v1, 0x13882

    goto/16 :goto_0
.end method

.method private static getBloodPressureContentValuesFromCursor(Landroid/database/Cursor;Landroid/content/Context;)Landroid/content/ContentValues;
    .locals 5
    .param p0, "cursor"    # Landroid/database/Cursor;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1548
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1549
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v2, "application__id"

    const-string/jumbo v3, "shealth2.5"

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1550
    const-string v2, "comment"

    const-string v3, "comment"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1551
    const-string v2, "diastolic"

    const-string v3, "diastolic"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1552
    const-string/jumbo v2, "mean"

    const-string/jumbo v3, "mean"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1553
    const-string/jumbo v2, "pulse"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 1554
    .local v1, "pulseValue":I
    if-gez v1, :cond_0

    .line 1555
    const/4 v1, 0x0

    .line 1556
    :cond_0
    const-string/jumbo v2, "pulse"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1557
    const-string/jumbo v2, "systolic"

    const-string/jumbo v3, "systolic"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1558
    const-string v2, "input_source_type"

    const-string/jumbo v3, "sensor_id"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3, p1}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getKInputSrouceType(ILandroid/content/Context;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1559
    const-string/jumbo v2, "sample_time"

    const-string v3, "create_time"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1560
    const-string v2, "create_time"

    const-string v3, "_id"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1561
    const-string/jumbo v2, "update_time"

    const-string v3, "_id"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1562
    const-string/jumbo v2, "time_zone"

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDeviceTimeZone()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1563
    const-string/jumbo v2, "sync_status"

    const v3, 0x29812

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1564
    const-string v2, "_id"

    const-string v3, "_id"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1565
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " BloodPressure Values "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1566
    return-object v0
.end method

.method private static getConnectedDeviceIdPedo()Ljava/lang/String;
    .locals 2

    .prologue
    .line 852
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "10009_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getCreateTime(J)J
    .locals 2
    .param p0, "id"    # J

    .prologue
    .line 119
    sget-wide v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->MIGRATION_START_TIME:J

    add-long/2addr v0, p0

    return-wide v0
.end method

.method private static getDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;
    .locals 1

    .prologue
    .line 1964
    sget-boolean v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->mIsKiesMode:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/service/health/cp/database/DBManager;->getInstance()Lcom/sec/android/service/health/cp/database/DBManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/database/DBManager;->getBackupWritableDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v0

    goto :goto_0
.end method

.method private static getDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1831
    sget-object v0, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    return-object v0
.end method

.method public static getDeviceTimeZone()I
    .locals 4

    .prologue
    .line 1939
    const/4 v0, 0x0

    .line 1940
    .local v0, "timeZone":Ljava/util/TimeZone;
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->SHARED_PREF_TIMEZONE_ID:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1942
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " Shared Pref TimeZone "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->SHARED_PREF_TIMEZONE_ID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1943
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->SHARED_PREF_TIMEZONE_ID:Ljava/lang/String;

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    .line 1949
    :goto_0
    invoke-virtual {v0}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v1

    div-int/lit16 v1, v1, 0x3e8

    return v1

    .line 1947
    :cond_0
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    goto :goto_0
.end method

.method private static getExerciseContentValuesFromCursor(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/database/Cursor;Landroid/content/Context;)Landroid/content/ContentValues;
    .locals 9
    .param p0, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const v8, 0x29812

    const v7, 0xea60

    .line 351
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 352
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v3, "application__id"

    const-string/jumbo v4, "shealth2.5"

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    const-string v3, "comment"

    const-string v4, "comment"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    const-string v3, "create_time"

    const-string v4, "_id"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 355
    const-string/jumbo v3, "update_time"

    const-string v4, "_id"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 356
    const-string/jumbo v3, "time_zone"

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDeviceTimeZone()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 357
    const-string v3, "duration_min"

    const-string/jumbo v4, "min"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 358
    const-string v3, "duration_millisecond"

    const-string/jumbo v4, "min"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    mul-int/2addr v4, v7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 359
    const-string/jumbo v3, "total_calorie"

    const-string v4, "calories"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getFloat(I)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 360
    const-string v3, "exercise_type"

    const/16 v4, 0x4e21

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 361
    const-string v3, "input_source_type"

    const-string/jumbo v4, "sensor_id"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4, p2}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getKInputSrouceType(ILandroid/content/Context;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 362
    const-string/jumbo v3, "start_time"

    const-string v4, "create_time"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 363
    const-string v3, "end_time"

    const-string v4, "create_time"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-string/jumbo v6, "min"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    mul-int/2addr v6, v7

    int-to-long v6, v6

    add-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 364
    const-string/jumbo v3, "sync_status"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 365
    const-string v3, "id_exercise_db"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const-string/jumbo v4, "sensor_id"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {p0, v3, v4, p2}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->migrateExerciseInfo(Lsamsung/database/sqlite/SecSQLiteDatabase;IILandroid/content/Context;)J

    move-result-wide v1

    .line 367
    .local v1, "exercise_info_id":J
    const-string v3, "exercise_info__id"

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 368
    const-string v3, "_id"

    const-string v4, "_id"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 369
    const-string/jumbo v3, "sync_status"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 370
    const-string v3, "cadence"

    const-string/jumbo v4, "step_counts"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 371
    const-string v3, "distance"

    const-string v4, "distance"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getFloat(I)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 372
    const-string v3, "heart_rate"

    const-string v4, "average_heart_beat"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getFloat(I)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 373
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " Exercise Values "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    return-object v0
.end method

.method private static getExerciseForWalk(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/ContentValues;Landroid/content/Context;)J
    .locals 7
    .param p0, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p1, "contentValues"    # Landroid/content/ContentValues;
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 790
    const-wide/16 v1, 0x0

    .line 791
    .local v1, "exerciseID":J
    const-string/jumbo v4, "start_time"

    invoke-virtual {p1, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    .line 792
    .local v3, "startTime":Ljava/lang/Long;
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " getExerciseForWalk startTime "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 793
    const/4 v0, 0x0

    .line 812
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SELECT * FROM exercise where start_time = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 813
    if-eqz v0, :cond_0

    .line 815
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 817
    const-string v4, "_id"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    .line 827
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 829
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v5, " unable to get exercise id: creating a exercise id "

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 830
    const/4 v4, 0x0

    sput-boolean v4, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->isMigratingFromH:Z

    .line 831
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->createNewExerciseForWalk(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/ContentValues;Landroid/content/Context;)J

    move-result-wide v1

    .line 832
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getConnectedDeviceIdPedo()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v4, p2}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->addExerciseDeviceInfo(JLjava/lang/String;Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 842
    :goto_1
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_1

    .line 844
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 848
    :cond_1
    return-wide v1

    .line 822
    :cond_2
    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 823
    const/4 v0, 0x0

    goto :goto_0

    .line 836
    :cond_3
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " Exercise is already available "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 837
    const/4 v4, 0x1

    sput-boolean v4, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->isMigratingFromH:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 842
    :catchall_0
    move-exception v4

    if-eqz v0, :cond_4

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_4

    .line 844
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v4
.end method

.method private static getExerciseInfoContentValuesFromCursor(Landroid/database/Cursor;)Landroid/content/ContentValues;
    .locals 6
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 284
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 286
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v3, "application__id"

    const-string/jumbo v4, "shealth2.5"

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    const-string/jumbo v3, "met"

    const-string v4, "kcal"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getFloat(I)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 288
    const-string/jumbo v3, "name"

    const-string/jumbo v4, "name"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    const-string v3, "favorite"

    const-string v4, "favorite"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getKExerciseFavourite(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 290
    const-string/jumbo v3, "pinyin"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 291
    .local v1, "sort1":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 292
    const-string/jumbo v3, "sorting1"

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    :cond_0
    const-string/jumbo v3, "pinyin_sort"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 295
    .local v2, "sort2":Ljava/lang/String;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 296
    const-string/jumbo v3, "sorting2"

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    :cond_1
    const-string v3, "create_time"

    const-string v4, "_id"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 299
    const-string/jumbo v3, "update_time"

    const-string v4, "_id"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 300
    const-string/jumbo v3, "sync_status"

    const v4, 0x29812

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 301
    const-string/jumbo v3, "time_zone"

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDeviceTimeZone()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 302
    const-string/jumbo v3, "source_type"

    const v4, 0x1adb1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 303
    const-string v3, "_id"

    const-string v4, "_id"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 304
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " ExerciseInfo Values "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    return-object v0
.end method

.method private static getExtendedFoodInfoContentValuesFromCursor(Landroid/database/Cursor;)Landroid/content/ContentValues;
    .locals 4
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 1231
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1233
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v1, "calcium"

    const-string v2, "calcium"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1234
    const-string v1, "carbohydrate"

    const-string v2, "carbohydrate"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1235
    const-string v1, "cholesterol"

    const-string v2, "cholesterol"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1237
    const-string v1, "dietary_fiber"

    const-string v2, "dietary"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1238
    const-string/jumbo v1, "total_fat"

    const-string v2, "fat"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1239
    const-string v1, "food_info__id"

    const-string v2, "food_info_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1240
    const-string v1, "kcal_in_unit"

    const-string v2, "gramm_in_kcal"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1241
    const-string v1, "iron"

    const-string v2, "iron"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1242
    const-string/jumbo v1, "monosaturated_fat"

    const-string/jumbo v2, "monosaturated"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1244
    const-string/jumbo v1, "polysaturated_fat"

    const-string/jumbo v2, "polysaturated"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1245
    const-string/jumbo v1, "potassium"

    const-string/jumbo v2, "potassium"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1246
    const-string/jumbo v1, "protein"

    const-string/jumbo v2, "protein"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1247
    const-string/jumbo v1, "saturated_fat"

    const-string/jumbo v2, "saturated"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1248
    const-string/jumbo v1, "sodium"

    const-string/jumbo v2, "sodium"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1249
    const-string/jumbo v1, "sugar"

    const-string/jumbo v2, "sugary"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1251
    const-string/jumbo v1, "metric_serving_amount"

    const-string/jumbo v2, "unit"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1252
    const-string/jumbo v1, "serving_description"

    const-string/jumbo v2, "unit_name"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1253
    const-string/jumbo v1, "vitamin_a"

    const-string/jumbo v2, "vitamin_a"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1254
    const-string/jumbo v1, "vitamin_c"

    const-string/jumbo v2, "vitamin_c"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1256
    const-string v1, "create_time"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1257
    const-string/jumbo v1, "update_time"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1258
    const-string/jumbo v1, "time_zone"

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDeviceTimeZone()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1259
    const-string/jumbo v1, "sync_status"

    const v2, 0x29812

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1260
    const-string v1, "default_number_of_serving_unit"

    const-string v2, "default_number"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1261
    const-string v1, "_id"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1263
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " Food Nutrient Values "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1265
    return-object v0
.end method

.method private static getFoodInfoContentValuesFromCursor(Landroid/database/Cursor;)Landroid/content/ContentValues;
    .locals 7
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 1164
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1166
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v4, "application__id"

    const-string/jumbo v5, "shealth2.5"

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1167
    const-string v4, "kcal"

    const-string v5, "kcal"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getFloat(I)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1168
    const-string/jumbo v4, "name"

    const-string/jumbo v5, "name"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1170
    const-string v4, "deleted"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v5, 0x2

    if-eq v4, v5, :cond_0

    .line 1172
    const-string/jumbo v4, "my"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getKServerSourceType(I)Ljava/lang/Integer;

    move-result-object v1

    .line 1173
    .local v1, "keyMy":Ljava/lang/Integer;
    if-eqz v1, :cond_0

    .line 1174
    const-string/jumbo v4, "server_source_type"

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1177
    .end local v1    # "keyMy":Ljava/lang/Integer;
    :cond_0
    const-string v4, "favorite"

    const-string v5, "favorite"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getKFavourite(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1178
    const-string/jumbo v4, "pinyin"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1179
    .local v2, "sort1":Ljava/lang/String;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1180
    const-string/jumbo v4, "sorting1"

    invoke-virtual {v0, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1182
    :cond_1
    const-string/jumbo v4, "pinyin_sort"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1183
    .local v3, "sort2":Ljava/lang/String;
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 1184
    const-string/jumbo v4, "sorting2"

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1186
    :cond_2
    const-string/jumbo v4, "server_food_id"

    const-string v5, "api_id"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1187
    const-string/jumbo v4, "server_locale"

    const-string v5, "lang"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1188
    const-string v4, "create_time"

    const-string v5, "_id"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1189
    const-string/jumbo v4, "update_time"

    const-string v5, "_id"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1190
    const-string/jumbo v4, "time_zone"

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDeviceTimeZone()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1191
    const-string v4, "_id"

    const-string v5, "_id"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1192
    const-string/jumbo v4, "sync_status"

    const v5, 0x29812

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1193
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " Food Info Values "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1194
    return-object v0
.end method

.method private static getGlucoseValue(FLjava/lang/String;)F
    .locals 1
    .param p0, "value"    # F
    .param p1, "unit"    # Ljava/lang/String;

    .prologue
    .line 1634
    const-string/jumbo v0, "mmol/L"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1637
    .end local p0    # "value":F
    :goto_0
    return p0

    .restart local p0    # "value":F
    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertMgIntoMmol(F)F

    move-result p0

    goto :goto_0
.end method

.method private static getGoalContentValuesFromCursor(Landroid/database/Cursor;Lsamsung/database/sqlite/SecSQLiteDatabase;)Landroid/content/ContentValues;
    .locals 7
    .param p0, "cursor"    # Landroid/database/Cursor;
    .param p1, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;

    .prologue
    .line 1722
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1724
    .local v1, "contentValues":Landroid/content/ContentValues;
    const-string v4, "application__id"

    const-string/jumbo v5, "shealth2.5"

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1725
    const-string/jumbo v4, "period"

    const-string/jumbo v5, "period"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1726
    const-string/jumbo v4, "type"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getKGoal(I)I

    move-result v2

    .line 1727
    .local v2, "goal_type":I
    const-string v4, "goal_type"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1728
    const v4, 0x9c41

    if-ne v2, v4, :cond_0

    .line 1730
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "10009_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDeviceId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1731
    .local v0, "connectedDeviceId":Ljava/lang/String;
    const-string/jumbo v4, "user_device__id"

    invoke-virtual {v1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1733
    .end local v0    # "connectedDeviceId":Ljava/lang/String;
    :cond_0
    const-string/jumbo v4, "subtype"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getKSubGoal(I)I

    move-result v3

    .line 1734
    .local v3, "subtype":I
    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    .line 1735
    const-string v4, "goal_subtype"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1738
    :cond_1
    const-string v4, "create_time"

    const-string v5, "create_time"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1741
    const-string/jumbo v4, "update_time"

    const-string v5, "create_time"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1742
    const-string/jumbo v4, "set_time"

    const-string v5, "create_time"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1743
    const-string/jumbo v4, "value"

    const-string/jumbo v5, "value"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getFloat(I)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1744
    const-string/jumbo v4, "sync_status"

    const v5, 0x29812

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1745
    const-string/jumbo v4, "time_zone"

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDeviceTimeZone()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1746
    const-string v4, "_id"

    const-string v5, "_id"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1747
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " Goal Values "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1749
    return-object v1
.end method

.method private static getHeightFromProfile(Landroid/content/Context;)F
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1510
    new-instance v1, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 1511
    .local v1, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v0

    .line 1512
    .local v0, "height":F
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " getHeightFromProfile height "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1513
    return v0
.end method

.method private static getImageContentValuesFromCursor(Landroid/database/Cursor;)Landroid/content/ContentValues;
    .locals 4
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 1432
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1434
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v1, "application__id"

    const-string/jumbo v2, "shealth2.5"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1435
    const-string v1, "file_path"

    const-string v2, "file_path"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1436
    const-string/jumbo v1, "meal__id"

    const-string/jumbo v2, "measure_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1437
    const-string v1, "create_time"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1438
    const-string/jumbo v1, "update_time"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1439
    const-string/jumbo v1, "time_zone"

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDeviceTimeZone()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1440
    const-string/jumbo v1, "sync_status"

    const v2, 0x29812

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1441
    const-string v1, "_id"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1443
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " MealImage Values "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1444
    return-object v0
.end method

.method private static getKExerciseFavourite(I)I
    .locals 4
    .param p0, "h_favourite"    # I

    .prologue
    .line 266
    packed-switch p0, :pswitch_data_0

    .line 275
    const/4 v0, 0x0

    .line 278
    .local v0, "k_favourite":I
    :goto_0
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " getKExerciseFavourite k_favourite "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " h_favourite "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    return v0

    .line 269
    .end local v0    # "k_favourite":I
    :pswitch_0
    const/4 v0, 0x1

    .line 270
    .restart local v0    # "k_favourite":I
    goto :goto_0

    .line 272
    .end local v0    # "k_favourite":I
    :pswitch_1
    const/4 v0, 0x0

    .line 273
    .restart local v0    # "k_favourite":I
    goto :goto_0

    .line 266
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static getKFavourite(I)I
    .locals 4
    .param p0, "h_favourite"    # I

    .prologue
    .line 1146
    packed-switch p0, :pswitch_data_0

    .line 1155
    const/4 v0, 0x0

    .line 1158
    .local v0, "k_favourite":I
    :goto_0
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " getKFavourite k_favourite "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " k_favourite "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1159
    return v0

    .line 1149
    .end local v0    # "k_favourite":I
    :pswitch_0
    const/4 v0, 0x1

    .line 1150
    .restart local v0    # "k_favourite":I
    goto :goto_0

    .line 1152
    .end local v0    # "k_favourite":I
    :pswitch_1
    const/4 v0, 0x0

    .line 1153
    .restart local v0    # "k_favourite":I
    goto :goto_0

    .line 1146
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static getKGoal(I)I
    .locals 4
    .param p0, "hgoal"    # I

    .prologue
    .line 1673
    sparse-switch p0, :sswitch_data_0

    .line 1688
    const/4 v0, -0x1

    .line 1691
    .local v0, "value":I
    :goto_0
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " Goal HGoal "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " kGoal "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1692
    return v0

    .line 1676
    .end local v0    # "value":I
    :sswitch_0
    const v0, 0x9c42

    .line 1677
    .restart local v0    # "value":I
    goto :goto_0

    .line 1679
    .end local v0    # "value":I
    :sswitch_1
    const v0, 0x9c43

    .line 1680
    .restart local v0    # "value":I
    goto :goto_0

    .line 1682
    .end local v0    # "value":I
    :sswitch_2
    const v0, 0x9c41

    .line 1683
    .restart local v0    # "value":I
    goto :goto_0

    .line 1685
    .end local v0    # "value":I
    :sswitch_3
    const v0, 0x9c44

    .line 1686
    .restart local v0    # "value":I
    goto :goto_0

    .line 1673
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_3
        0xc -> :sswitch_2
        0xf -> :sswitch_1
    .end sparse-switch
.end method

.method private static getKInputSrouceType(ILandroid/content/Context;)I
    .locals 4
    .param p0, "hInputType"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1800
    packed-switch p0, :pswitch_data_0

    .line 1820
    :pswitch_0
    const v0, 0x3f7a1

    .line 1823
    .local v0, "value":I
    :goto_0
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " getKInputSrouceType hInputType "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " kvalue "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1824
    return v0

    .line 1804
    .end local v0    # "value":I
    :pswitch_1
    const v0, 0x3f7a1

    .line 1805
    .restart local v0    # "value":I
    goto :goto_0

    .line 1807
    .end local v0    # "value":I
    :pswitch_2
    const v0, 0x3f7a3

    .line 1808
    .restart local v0    # "value":I
    goto :goto_0

    .line 1817
    .end local v0    # "value":I
    :pswitch_3
    const v0, 0x3f7a2

    .line 1818
    .restart local v0    # "value":I
    goto :goto_0

    .line 1800
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private static getKMealType(I)I
    .locals 1
    .param p0, "hMealType"    # I

    .prologue
    .line 1304
    sparse-switch p0, :sswitch_data_0

    .line 1313
    const v0, 0x186a4

    :goto_0
    return v0

    .line 1307
    :sswitch_0
    const v0, 0x186a2

    goto :goto_0

    .line 1309
    :sswitch_1
    const v0, 0x186a1

    goto :goto_0

    .line 1311
    :sswitch_2
    const v0, 0x186a3

    goto :goto_0

    .line 1304
    nop

    :sswitch_data_0
    .sparse-switch
        0x38 -> :sswitch_0
        0x43 -> :sswitch_1
        0x54 -> :sswitch_2
    .end sparse-switch
.end method

.method private static getKMealUnit(I)I
    .locals 1
    .param p0, "hMealType"    # I

    .prologue
    .line 1370
    packed-switch p0, :pswitch_data_0

    .line 1377
    const v0, 0x1d4c1

    :goto_0
    return v0

    .line 1373
    :pswitch_0
    const v0, 0x1d4c2

    goto :goto_0

    .line 1375
    :pswitch_1
    const v0, 0x1d4c3

    goto :goto_0

    .line 1370
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static getKServerSourceType(I)Ljava/lang/Integer;
    .locals 4
    .param p0, "h_sourcetype"    # I

    .prologue
    .line 1120
    packed-switch p0, :pswitch_data_0

    .line 1135
    const/4 v0, 0x0

    .line 1139
    .local v0, "k_source_type":Ljava/lang/Integer;
    :goto_0
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " getKServerSourceType h_sourcetype "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " k_source_type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1140
    return-object v0

    .line 1123
    .end local v0    # "k_source_type":Ljava/lang/Integer;
    :pswitch_0
    const v1, 0x46cd1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1124
    .restart local v0    # "k_source_type":Ljava/lang/Integer;
    goto :goto_0

    .line 1126
    .end local v0    # "k_source_type":Ljava/lang/Integer;
    :pswitch_1
    const v1, 0x46cd2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1127
    .restart local v0    # "k_source_type":Ljava/lang/Integer;
    goto :goto_0

    .line 1129
    .end local v0    # "k_source_type":Ljava/lang/Integer;
    :pswitch_2
    const v1, 0x46cd5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1130
    .restart local v0    # "k_source_type":Ljava/lang/Integer;
    goto :goto_0

    .line 1132
    .end local v0    # "k_source_type":Ljava/lang/Integer;
    :pswitch_3
    const v1, 0x46cd4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1133
    .restart local v0    # "k_source_type":Ljava/lang/Integer;
    goto :goto_0

    .line 1120
    nop

    :pswitch_data_0
    .packed-switch -0x4
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private static getKSubGoal(I)I
    .locals 4
    .param p0, "hgoal"    # I

    .prologue
    .line 1698
    packed-switch p0, :pswitch_data_0

    .line 1713
    const/4 v0, -0x1

    .line 1716
    .local v0, "value":I
    :goto_0
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " getKSubGoal HGoal "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " kGoal "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1717
    return v0

    .line 1701
    .end local v0    # "value":I
    :pswitch_0
    const v0, 0xc351

    .line 1702
    .restart local v0    # "value":I
    goto :goto_0

    .line 1704
    .end local v0    # "value":I
    :pswitch_1
    const v0, 0xc352

    .line 1705
    .restart local v0    # "value":I
    goto :goto_0

    .line 1707
    .end local v0    # "value":I
    :pswitch_2
    const v0, 0xc353

    .line 1708
    .restart local v0    # "value":I
    goto :goto_0

    .line 1710
    .end local v0    # "value":I
    :pswitch_3
    const v0, 0xc354

    .line 1711
    .restart local v0    # "value":I
    goto :goto_0

    .line 1698
    nop

    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static getMapPathContentValuesFromCursor(Landroid/database/Cursor;Lsamsung/database/sqlite/SecSQLiteDatabase;)Landroid/content/ContentValues;
    .locals 4
    .param p0, "cursor"    # Landroid/database/Cursor;
    .param p1, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;

    .prologue
    .line 1069
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1071
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v1, "application__id"

    const-string/jumbo v2, "shealth2.5"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1072
    const-string v1, "accuracy"

    const-string v2, "accuracy"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1073
    const-string v1, "altitude"

    const-string v2, "altitude"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1074
    const-string v1, "exercise__id"

    const-string v2, "db_index"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1075
    const-string v1, "latitude"

    const-string v2, "latitude"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1076
    const-string v1, "longitude"

    const-string v2, "longitude"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1077
    const-string/jumbo v1, "sample_time"

    const-string/jumbo v2, "time"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1078
    const-string v1, "create_time"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1079
    const-string/jumbo v1, "update_time"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1080
    const-string/jumbo v1, "time_zone"

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDeviceTimeZone()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1081
    const-string/jumbo v1, "sync_status"

    const v2, 0x29812

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1082
    const-string v1, "_id"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1083
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " MapPath Values "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1084
    return-object v0
.end method

.method private static getMealContentValuesFromCursor(Landroid/database/Cursor;)Landroid/content/ContentValues;
    .locals 4
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 1319
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1321
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v1, "application__id"

    const-string/jumbo v2, "shealth2.5"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1322
    const-string v1, "comment"

    const-string v2, "comment"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1323
    const-string/jumbo v1, "total_kilo_calorie"

    const-string v2, "kcal"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1324
    const-string/jumbo v1, "type"

    const-string v2, "kind"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getKMealType(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1325
    const-string v1, "category"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1327
    const-string/jumbo v1, "sample_time"

    const-string v2, "create_time"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1328
    const-string v1, "create_time"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1329
    const-string/jumbo v1, "update_time"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1330
    const-string/jumbo v1, "time_zone"

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDeviceTimeZone()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1331
    const-string v1, "_id"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1332
    const-string/jumbo v1, "sync_status"

    const v2, 0x29812

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1334
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " Meal Values "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1335
    return-object v0
.end method

.method private static getMealItemContentValuesFromCursor(Landroid/database/Cursor;)Landroid/content/ContentValues;
    .locals 4
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 1383
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1385
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v1, "application__id"

    const-string/jumbo v2, "shealth2.5"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1386
    const-string v1, "food_info__id"

    const-string v2, "id_food_info"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1387
    const-string/jumbo v1, "meal__id"

    const-string v2, "id_meal_table"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1388
    const-string v1, "amount"

    const-string/jumbo v2, "percent"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1389
    const-string/jumbo v1, "unit"

    const-string/jumbo v2, "unit"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getKMealUnit(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1391
    const-string v1, "create_time"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1392
    const-string/jumbo v1, "update_time"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1393
    const-string/jumbo v1, "time_zone"

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDeviceTimeZone()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1394
    const-string/jumbo v1, "sync_status"

    const v2, 0x29812

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1395
    const-string v1, "_id"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1397
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " MealItem Values "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1398
    return-object v0
.end method

.method private static getMigrationUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1969
    sget-boolean v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->mIsKiesMode:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->MIGRATE_URI_KIES:Landroid/net/Uri;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->MIGRATE_URI:Landroid/net/Uri;

    goto :goto_0
.end method

.method private static getRealtimeHeartbeatContentValuesFromCursor(Landroid/database/Cursor;Lsamsung/database/sqlite/SecSQLiteDatabase;)Landroid/content/ContentValues;
    .locals 4
    .param p0, "cursor"    # Landroid/database/Cursor;
    .param p1, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;

    .prologue
    .line 1020
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1022
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v1, "application__id"

    const-string/jumbo v2, "shealth2.5"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1023
    const-string v1, "exercise__id"

    const-string v2, "db_index"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1024
    const-string v1, "data_type"

    const v2, 0x493e2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1025
    const-string/jumbo v1, "sample_time"

    const-string/jumbo v2, "time"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1026
    const-string v1, "create_time"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1027
    const-string/jumbo v1, "update_time"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1028
    const-string/jumbo v1, "time_zone"

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDeviceTimeZone()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1029
    const-string v1, "heart_rate_per_min"

    const-string v2, "bpm"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1030
    const-string/jumbo v1, "sync_status"

    const v2, 0x29812

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1032
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " Realtime Heartbeat "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1033
    return-object v0
.end method

.method private static getRealtimeSpeedContentValuesFromCursor(Landroid/database/Cursor;Lsamsung/database/sqlite/SecSQLiteDatabase;)Landroid/content/ContentValues;
    .locals 4
    .param p0, "cursor"    # Landroid/database/Cursor;
    .param p1, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;

    .prologue
    .line 964
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 966
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v1, "application__id"

    const-string/jumbo v2, "shealth2.5"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 967
    const-string v1, "exercise__id"

    const-string v2, "db_index"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 968
    const-string v1, "data_type"

    const v2, 0x493e1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 969
    const-string/jumbo v1, "sample_time"

    const-string/jumbo v2, "time"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 970
    const-string v1, "create_time"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 971
    const-string/jumbo v1, "update_time"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 972
    const-string/jumbo v1, "time_zone"

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDeviceTimeZone()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 973
    const-string/jumbo v1, "speed_per_hour"

    const-string/jumbo v2, "speed"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 974
    const-string/jumbo v1, "sync_status"

    const v2, 0x29812

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 976
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " Realtime Speed "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 978
    return-object v0
.end method

.method private static getTrainingLoadPeakContentValuesFromCursor(Landroid/database/Cursor;)Landroid/content/ContentValues;
    .locals 4
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 1783
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1784
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v1, "application__id"

    const-string/jumbo v2, "shealth2.5"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1785
    const-string v1, "exercise__id"

    const-string v2, "db_index"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1786
    const-string/jumbo v1, "training_load_peak"

    const-string/jumbo v2, "training_load_peak"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1787
    const-string v1, "end_time"

    const-string/jumbo v2, "time"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1788
    const-string/jumbo v1, "time_zone"

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDeviceTimeZone()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1789
    const-string v1, "create_time"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1790
    const-string/jumbo v1, "update_time"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1791
    const-string/jumbo v1, "sync_status"

    const v2, 0x29812

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1792
    const-string v1, "_id"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1793
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " TrainingLoadPeak Values "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1794
    return-object v0
.end method

.method private static getWalkInfoContentValuesFromCursor(Landroid/database/Cursor;Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)Landroid/content/ContentValues;
    .locals 24
    .param p0, "cursor"    # Landroid/database/Cursor;
    .param p1, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 636
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 637
    .local v6, "contentValues":Landroid/content/ContentValues;
    const-string v19, "application__id"

    const-string/jumbo v20, "shealth2.5"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 638
    const-string v19, "_id"

    const-string v20, "_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 639
    const-string v19, "distance"

    const-string v20, "distance"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 640
    const-string v19, "calorie"

    const-string v20, "kcal"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 641
    const-string/jumbo v19, "run_step"

    const-string/jumbo v20, "run_steps"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 642
    const-string/jumbo v19, "speed"

    const-string/jumbo v20, "walk_speed"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 643
    const-string/jumbo v19, "total_step"

    const-string/jumbo v20, "total_steps"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 644
    const-string/jumbo v19, "updown_step"

    const-string/jumbo v20, "updown_steps"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 645
    const-string/jumbo v19, "walk_step"

    const-string/jumbo v20, "walk_steps"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 646
    const-string v19, "create_time"

    const-string v20, "_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 647
    const-string/jumbo v19, "update_time"

    const-string v20, "_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 648
    const-string/jumbo v19, "time_zone"

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDeviceTimeZone()I

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 649
    const-string/jumbo v19, "start_time"

    const-string v20, "create_time"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 652
    const-string v19, "end_time"

    const-string v20, "create_time"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    const-wide/32 v22, 0x36ee80

    add-long v20, v20, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 653
    const-string/jumbo v19, "sync_status"

    const v20, 0x29812

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 654
    const-string/jumbo v19, "sample_position"

    const v20, 0x38271

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 655
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "10009_"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDeviceId()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 656
    .local v5, "connectedDeviceId":Ljava/lang/String;
    const-string/jumbo v19, "user_device__id"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 657
    const-wide/16 v10, -0x1

    .line 658
    .local v10, "cur_exercise_id":J
    const-string v19, "create_time"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 659
    .local v8, "cur_create_time":J
    sget-wide v19, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->last_createTime:J

    const-wide/16 v21, 0x0

    cmp-long v19, v19, v21

    if-eqz v19, :cond_2

    sget-wide v19, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->last_createTime:J

    move-wide/from16 v0, v19

    invoke-static {v0, v1, v8, v9}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->isSameDay(JJ)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 661
    sget-object v19, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, " setting the last exercise_id as current exerciseid  : exercise Id "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    sget-wide v21, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->last_exerciseId:J

    invoke-virtual/range {v20 .. v22}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 662
    sget-wide v10, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->last_exerciseId:J

    .line 663
    sget-boolean v19, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->isMigratingFromH:Z

    if-nez v19, :cond_2

    .line 665
    const-string v19, "calorie"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/content/ContentValues;->getAsFloat(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v7

    .line 666
    .local v7, "curCalorie":Ljava/lang/Float;
    const-string v19, "distance"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/content/ContentValues;->getAsFloat(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v12

    .line 667
    .local v12, "curdistance":Ljava/lang/Float;
    const-string/jumbo v19, "total_step"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v14

    .line 668
    .local v14, "curstepcount":Ljava/lang/Integer;
    if-eqz v7, :cond_0

    .line 669
    sget v19, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->total_calorie:F

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v20

    add-float v19, v19, v20

    sput v19, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->total_calorie:F

    .line 670
    :cond_0
    if-eqz v12, :cond_1

    .line 671
    sget v19, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->total_distance:F

    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    move-result v20

    add-float v19, v19, v20

    sput v19, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->total_distance:F

    .line 672
    :cond_1
    if-eqz v14, :cond_2

    .line 673
    sget v19, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->total_stepcount:I

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v20

    add-int v19, v19, v20

    sput v19, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->total_stepcount:I

    .line 676
    .end local v7    # "curCalorie":Ljava/lang/Float;
    .end local v12    # "curdistance":Ljava/lang/Float;
    .end local v14    # "curstepcount":Ljava/lang/Integer;
    :cond_2
    const-wide/16 v19, -0x1

    cmp-long v19, v10, v19

    if-nez v19, :cond_6

    .line 678
    sget-boolean v19, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->isMigratingFromH:Z

    if-nez v19, :cond_5

    sget-wide v19, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->last_exerciseId:J

    const-wide/16 v21, -0x1

    cmp-long v19, v19, v21

    if-eqz v19, :cond_5

    .line 680
    sget-object v19, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, " updading the last exercise  "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    sget-wide v21, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->last_exerciseId:J

    invoke-virtual/range {v20 .. v22}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 681
    const/4 v13, 0x0

    .line 684
    .local v13, "cursorlocal":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v19

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "SELECT * FROM exercise where _id = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    sget-wide v21, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->last_exerciseId:J

    invoke-virtual/range {v20 .. v22}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x0

    invoke-virtual/range {v19 .. v21}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 685
    if-eqz v13, :cond_3

    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v19

    if-eqz v19, :cond_3

    .line 687
    const-string v19, "distance"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/content/ContentValues;->getAsFloat(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v12

    .line 688
    .restart local v12    # "curdistance":Ljava/lang/Float;
    if-eqz v12, :cond_7

    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    move-result v19

    :goto_0
    sget v20, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->total_distance:F

    add-float v17, v19, v20

    .line 689
    .local v17, "totaldistance":F
    const-string v19, "calorie"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/content/ContentValues;->getAsFloat(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v7

    .line 690
    .restart local v7    # "curCalorie":Ljava/lang/Float;
    if-eqz v7, :cond_8

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v19

    :goto_1
    sget v20, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->total_calorie:F

    add-float v15, v19, v20

    .line 691
    .local v15, "totalCalorie":F
    const-string/jumbo v19, "total_step"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v14

    .line 692
    .restart local v14    # "curstepcount":Ljava/lang/Integer;
    if-eqz v14, :cond_9

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v19

    :goto_2
    sget v20, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->total_stepcount:I

    add-int v16, v19, v20

    .line 693
    .local v16, "totalStepCount":I
    sget-object v19, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, " totaldistance = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " totalCalorie "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " totalStepCount "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 694
    new-instance v18, Landroid/content/ContentValues;

    invoke-direct/range {v18 .. v18}, Landroid/content/ContentValues;-><init>()V

    .line 695
    .local v18, "update":Landroid/content/ContentValues;
    const-string/jumbo v19, "total_calorie"

    invoke-static {v15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 696
    const-string v19, "distance"

    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 697
    const-string v19, "cadence"

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 698
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v19

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getMigrationUri()Landroid/net/Uri;

    move-result-object v20

    const-string v21, "exercise"

    invoke-static/range {v20 .. v21}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v20

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "_id = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    sget-wide v22, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->last_exerciseId:J

    invoke-virtual/range {v21 .. v23}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v18

    move-object/from16 v3, v21

    move-object/from16 v4, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 703
    .end local v7    # "curCalorie":Ljava/lang/Float;
    .end local v12    # "curdistance":Ljava/lang/Float;
    .end local v14    # "curstepcount":Ljava/lang/Integer;
    .end local v15    # "totalCalorie":F
    .end local v16    # "totalStepCount":I
    .end local v17    # "totaldistance":F
    .end local v18    # "update":Landroid/content/ContentValues;
    :cond_3
    if-eqz v13, :cond_4

    invoke-interface {v13}, Landroid/database/Cursor;->isClosed()Z

    move-result v19

    if-nez v19, :cond_4

    .line 705
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 707
    :cond_4
    const/16 v19, 0x0

    sput v19, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->total_calorie:F

    .line 708
    const/16 v19, 0x0

    sput v19, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->total_distance:F

    .line 709
    const/16 v19, 0x0

    sput v19, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->total_stepcount:I

    .line 712
    .end local v13    # "cursorlocal":Landroid/database/Cursor;
    :cond_5
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v6, v1}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getExerciseForWalk(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/ContentValues;Landroid/content/Context;)J

    move-result-wide v10

    .line 714
    sget-object v19, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, " Getting exercise id from the exercise table : exercise Id "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 715
    new-instance v18, Landroid/content/ContentValues;

    invoke-direct/range {v18 .. v18}, Landroid/content/ContentValues;-><init>()V

    .line 716
    .restart local v18    # "update":Landroid/content/ContentValues;
    const-string v19, "exercise_info__id"

    const/16 v20, 0x4651

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 717
    const-string v19, "exercise_type"

    const/16 v20, 0x4e23

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 718
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v19

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getMigrationUri()Landroid/net/Uri;

    move-result-object v20

    const-string v21, "exercise"

    invoke-static/range {v20 .. v21}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v20

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "_id = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v18

    move-object/from16 v3, v21

    move-object/from16 v4, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 721
    .end local v18    # "update":Landroid/content/ContentValues;
    :cond_6
    sput-wide v8, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->last_createTime:J

    .line 722
    sput-wide v10, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->last_exerciseId:J

    .line 723
    const-string v19, "exercise__id"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 724
    sget-object v19, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, " getWalkInfoContentValuesFromCursor Values "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 725
    return-object v6

    .line 688
    .restart local v12    # "curdistance":Ljava/lang/Float;
    .restart local v13    # "cursorlocal":Landroid/database/Cursor;
    :cond_7
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 690
    .restart local v7    # "curCalorie":Ljava/lang/Float;
    .restart local v17    # "totaldistance":F
    :cond_8
    const/16 v19, 0x0

    goto/16 :goto_1

    .line 692
    .restart local v14    # "curstepcount":Ljava/lang/Integer;
    .restart local v15    # "totalCalorie":F
    :cond_9
    const/16 v19, 0x0

    goto/16 :goto_2

    .line 703
    .end local v7    # "curCalorie":Ljava/lang/Float;
    .end local v12    # "curdistance":Ljava/lang/Float;
    .end local v14    # "curstepcount":Ljava/lang/Integer;
    .end local v15    # "totalCalorie":F
    .end local v17    # "totaldistance":F
    :catchall_0
    move-exception v19

    if-eqz v13, :cond_a

    invoke-interface {v13}, Landroid/database/Cursor;->isClosed()Z

    move-result v20

    if-nez v20, :cond_a

    .line 705
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 707
    :cond_a
    const/16 v20, 0x0

    sput v20, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->total_calorie:F

    .line 708
    const/16 v20, 0x0

    sput v20, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->total_distance:F

    .line 709
    const/16 v20, 0x0

    sput v20, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->total_stepcount:I

    throw v19
.end method

.method private static getWeightContentValuesFromCursor(Landroid/database/Cursor;FLandroid/content/Context;)Landroid/content/ContentValues;
    .locals 5
    .param p0, "cursor"    # Landroid/database/Cursor;
    .param p1, "profile_height"    # F
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1482
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1484
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v2, "application__id"

    const-string/jumbo v3, "shealth2.5"

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1485
    const-string v2, "body_mass_index"

    const-string v3, "bmi"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1486
    const-string v2, "basal_metabolic_rate"

    const-string v3, "bmr"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1487
    const-string v2, "body_fat"

    const-string v3, "body_fat"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1488
    const-string v2, "body_age"

    const-string v3, "body_year"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1489
    const-string v2, "comment"

    const-string v3, "comment"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1490
    const-string v2, "height"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    .line 1491
    .local v1, "height":F
    const/4 v2, 0x0

    cmpg-float v2, v1, v2

    if-gtz v2, :cond_0

    .line 1492
    move v1, p1

    .line 1493
    :cond_0
    const-string v2, "height"

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1494
    const-string/jumbo v2, "skeletal_muscle"

    const-string/jumbo v3, "skeletal_muscle"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1495
    const-string/jumbo v2, "weight"

    const-string/jumbo v3, "value"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1496
    const-string/jumbo v2, "visceral_fat"

    const-string/jumbo v3, "visceral_fat"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1497
    const-string v2, "input_source_type"

    const-string/jumbo v3, "sensor_id"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3, p2}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getKInputSrouceType(ILandroid/content/Context;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1498
    const-string/jumbo v2, "sample_time"

    const-string v3, "create_time"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1499
    const-string v2, "create_time"

    const-string v3, "_id"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1500
    const-string/jumbo v2, "update_time"

    const-string v3, "_id"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getCreateTime(J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1501
    const-string/jumbo v2, "time_zone"

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDeviceTimeZone()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1502
    const-string/jumbo v2, "sync_status"

    const v3, 0x29812

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1503
    const-string v2, "_id"

    const-string v3, "_id"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1504
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " Weight Values "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1505
    return-object v0
.end method

.method private static isSameDay(JJ)Z
    .locals 10
    .param p0, "starttime1"    # J
    .param p2, "starttime2"    # J

    .prologue
    const-wide/16 v8, 0x0

    const/4 v7, 0x5

    const/4 v3, 0x0

    .line 763
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " isSameDay starttime1 "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " starttime2 "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 764
    const/4 v2, 0x0

    .line 765
    .local v2, "timeZone":Ljava/util/TimeZone;
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->SHARED_PREF_TIMEZONE_ID:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 767
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " isSameDay  Shared Pref TimeZone "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->SHARED_PREF_TIMEZONE_ID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 768
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->SHARED_PREF_TIMEZONE_ID:Ljava/lang/String;

    invoke-static {v4}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    .line 774
    :goto_0
    cmp-long v4, p0, v8

    if-eqz v4, :cond_0

    cmp-long v4, p2, v8

    if-nez v4, :cond_2

    .line 785
    :cond_0
    :goto_1
    return v3

    .line 772
    :cond_1
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v2

    goto :goto_0

    .line 776
    :cond_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 777
    .local v0, "firstday":Ljava/util/Calendar;
    invoke-virtual {v0, v2}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 778
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 779
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 780
    .local v1, "secondDay":Ljava/util/Calendar;
    invoke-virtual {v1, v2}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 781
    invoke-virtual {v1, p2, p3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 782
    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {v1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 783
    const/4 v3, 0x1

    goto :goto_1
.end method

.method private static migrateBloodGlucoseData(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V
    .locals 4
    .param p0, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 1571
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v3, " ---------- start migrateBloodGlucoseData-------------- "

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1572
    const/4 v0, 0x0

    .line 1575
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v2, "SELECT * FROM blood_glucose"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lsamsung/database/sqlite/SecSQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1576
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1580
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getMigrationUri()Landroid/net/Uri;

    move-result-object v2

    const-string v3, "blood_glucose"

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getBloodGlucoseContentValuesFromCursor(Landroid/database/Cursor;Landroid/content/Context;)Landroid/content/ContentValues;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->cpInsert(Landroid/content/Context;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1581
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getMigrationUri()Landroid/net/Uri;

    move-result-object v2

    const-string v3, "blood_glucose_measurement_context"

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getBloodGlucoseMeasurementContextContentValuesFromCursor(Landroid/database/Cursor;Landroid/content/Context;)Landroid/content/ContentValues;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->cpInsert(Landroid/content/Context;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1582
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 1591
    :cond_1
    if-eqz v0, :cond_2

    .line 1593
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1597
    :cond_2
    :goto_0
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v3, " ---------- End migrateBloodGlucoseData-------------- "

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1598
    return-void

    .line 1585
    :catch_0
    move-exception v1

    .line 1587
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1591
    if-eqz v0, :cond_2

    .line 1593
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1591
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_3

    .line 1593
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v2
.end method

.method private static migrateBloodPressureData(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V
    .locals 4
    .param p0, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 1518
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v3, " ---------- start migrateBloodPressureData -------------- "

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1519
    const/4 v0, 0x0

    .line 1522
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v2, "SELECT * FROM blood_pressure"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lsamsung/database/sqlite/SecSQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1523
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1527
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getMigrationUri()Landroid/net/Uri;

    move-result-object v2

    const-string v3, "blood_pressure"

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getBloodPressureContentValuesFromCursor(Landroid/database/Cursor;Landroid/content/Context;)Landroid/content/ContentValues;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->cpInsert(Landroid/content/Context;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1528
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 1537
    :cond_1
    if-eqz v0, :cond_2

    .line 1539
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1543
    :cond_2
    :goto_0
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v3, " ---------- End migrateBloodPressureData -------------- "

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1544
    return-void

    .line 1531
    :catch_0
    move-exception v1

    .line 1533
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1537
    if-eqz v0, :cond_2

    .line 1539
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1537
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_3

    .line 1539
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v2
.end method

.method private static migrateExerciseData(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V
    .locals 6
    .param p0, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 310
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v4, " ---------- start migrateExerciseData -------------- "

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    const-string v3, "exercise"

    const-string v4, "create_time not in (select create_time from walk_for_life) and sensor_id = 8"

    invoke-virtual {p0, v3, v4, v5}, Lsamsung/database/sqlite/SecSQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 315
    const/4 v1, 0x0

    .line 318
    .local v1, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v3, "SELECT * FROM exercise"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lsamsung/database/sqlite/SecSQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 319
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 323
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getMigrationUri()Landroid/net/Uri;

    move-result-object v3

    const-string v4, "exercise"

    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {p0, v1, p1}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getExerciseContentValuesFromCursor(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/database/Cursor;Landroid/content/Context;)Landroid/content/ContentValues;

    move-result-object v4

    invoke-static {p1, v3, v4}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->cpInsert(Landroid/content/Context;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 324
    .local v0, "cur_exercise_uri":Landroid/net/Uri;
    invoke-static {p0, v1, p1}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->createExerciseAcitvity(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/database/Cursor;Landroid/content/Context;)V

    .line 326
    const-string/jumbo v3, "sensor_id"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_1

    .line 328
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v4, " creating exercise device info for pedo exercises"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-long v3, v3

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getConnectedDeviceIdPedo()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5, p1}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->addExerciseDeviceInfo(JLjava/lang/String;Landroid/content/Context;)V

    .line 331
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    .line 340
    .end local v0    # "cur_exercise_uri":Landroid/net/Uri;
    :cond_2
    if-eqz v1, :cond_3

    .line 342
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 346
    :cond_3
    :goto_0
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v4, " ---------- End migrateExerciseData -------------- "

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    return-void

    .line 334
    :catch_0
    move-exception v2

    .line 336
    .local v2, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 340
    if-eqz v1, :cond_3

    .line 342
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 340
    .end local v2    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_4

    .line 342
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v3
.end method

.method private static migrateExerciseInfo(Lsamsung/database/sqlite/SecSQLiteDatabase;IILandroid/content/Context;)J
    .locals 11
    .param p0, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p1, "exercise_info"    # I
    .param p2, "sensorId"    # I
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x5

    .line 482
    sget-object v7, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " migrateExerciseInfo  "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    const-wide/16 v4, 0x0

    .line 484
    .local v4, "resultExerciseInfoId":J
    const/4 v0, 0x0

    .line 487
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SELECT * FROM exercise_info where _id = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {p0, v7, v8}, Lsamsung/database/sqlite/SecSQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 488
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 490
    const-string/jumbo v7, "my"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 491
    .local v3, "my":I
    sget-object v7, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " migrateExerciseInfo  "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 492
    if-nez v3, :cond_5

    sget-object v7, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    if-eqz v7, :cond_5

    .line 494
    const/4 v7, 0x1

    if-ne p1, v7, :cond_2

    if-ne p2, v10, :cond_2

    .line 495
    const-wide/16 v4, 0x4652

    .line 522
    .end local v3    # "my":I
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 524
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 527
    :cond_1
    :goto_1
    return-wide v4

    .line 497
    .restart local v3    # "my":I
    :cond_2
    const/4 v7, 0x3

    if-ne p1, v7, :cond_3

    if-ne p2, v10, :cond_3

    .line 498
    const-wide/16 v4, 0x4653

    goto :goto_0

    .line 500
    :cond_3
    const/16 v7, 0xe

    if-ne p1, v7, :cond_4

    if-ne p2, v10, :cond_4

    .line 501
    const-wide/16 v4, 0x4654

    goto :goto_0

    .line 504
    :cond_4
    :try_start_1
    sget-object v7, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->EXERCISE_INFO_MAP:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-long v4, v7

    goto :goto_0

    .line 509
    :cond_5
    sget-object v7, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v8, " creating a new exercise_info  "

    invoke-static {v7, v8}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 510
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getExerciseInfoContentValuesFromCursor(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v6

    .line 511
    .local v6, "values":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getMigrationUri()Landroid/net/Uri;

    move-result-object v7

    const-string v8, "exercise_info"

    invoke-static {v7, v8}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-static {p3, v7, v6}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->cpInsert(Landroid/content/Context;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    .line 512
    .local v2, "inserturi":Landroid/net/Uri;
    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v4

    goto :goto_0

    .line 516
    .end local v2    # "inserturi":Landroid/net/Uri;
    .end local v3    # "my":I
    .end local v6    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v1

    .line 518
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v7, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " Exception Occured migrateExerciseInfo  "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 522
    if-eqz v0, :cond_1

    .line 524
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 522
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    if-eqz v0, :cond_6

    .line 524
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v7
.end method

.method private static migrateExtendedFoodInfoData(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V
    .locals 5
    .param p0, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 1199
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v4, " ---------- start migrateExtendedFoodInfoData -------------- "

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1200
    const/4 v0, 0x0

    .line 1203
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v2, "food_nutrient"

    .line 1204
    .local v2, "tableName":Ljava/lang/String;
    const-string v3, "SELECT * FROM extend_food_info"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lsamsung/database/sqlite/SecSQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1205
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1211
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getMigrationUri()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getExtendedFoodInfoContentValuesFromCursor(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v4

    invoke-static {p1, v3, v4}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->cpInsert(Landroid/content/Context;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1212
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    .line 1221
    :cond_1
    if-eqz v0, :cond_2

    .line 1223
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1226
    .end local v2    # "tableName":Ljava/lang/String;
    :cond_2
    :goto_0
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v4, " ---------- start migrateExtendedFoodInfoData -------------- "

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1227
    return-void

    .line 1215
    :catch_0
    move-exception v1

    .line 1217
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1221
    if-eqz v0, :cond_2

    .line 1223
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1221
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_3

    .line 1223
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v3
.end method

.method private static migrateFoodInfoData(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V
    .locals 4
    .param p0, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 1089
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v3, " ---------- start migrateFoodInfoData -------------- "

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1090
    const/4 v0, 0x0

    .line 1093
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v2, "SELECT * FROM food_info"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lsamsung/database/sqlite/SecSQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1094
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1099
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getMigrationUri()Landroid/net/Uri;

    move-result-object v2

    const-string v3, "food_info"

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getFoodInfoContentValuesFromCursor(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->cpInsert(Landroid/content/Context;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1100
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 1109
    :cond_1
    if-eqz v0, :cond_2

    .line 1111
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1114
    :cond_2
    :goto_0
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v3, " ---------- End migrateFoodInfoData -------------- "

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1115
    return-void

    .line 1103
    :catch_0
    move-exception v1

    .line 1105
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1109
    if-eqz v0, :cond_2

    .line 1111
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1109
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_3

    .line 1111
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v2
.end method

.method private static migrateGoalData(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V
    .locals 4
    .param p0, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 1642
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v3, " ---------- Start migrateGoalData-------------- "

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1643
    const/4 v0, 0x0

    .line 1646
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v2, "SELECT * FROM goal"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lsamsung/database/sqlite/SecSQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1647
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1651
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getMigrationUri()Landroid/net/Uri;

    move-result-object v2

    const-string v3, "goal"

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0, p0}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getGoalContentValuesFromCursor(Landroid/database/Cursor;Lsamsung/database/sqlite/SecSQLiteDatabase;)Landroid/content/ContentValues;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->cpInsert(Landroid/content/Context;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1652
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 1661
    :cond_1
    if-eqz v0, :cond_2

    .line 1663
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1666
    :cond_2
    :goto_0
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v3, " ---------- End migrateGoalData-------------- "

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1667
    return-void

    .line 1655
    :catch_0
    move-exception v1

    .line 1657
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1661
    if-eqz v0, :cond_2

    .line 1663
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1661
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_3

    .line 1663
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v2
.end method

.method private static migrateImagesData(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V
    .locals 4
    .param p0, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 1403
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v3, " ---------- Start migrateImagesData -------------- "

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1404
    const/4 v0, 0x0

    .line 1407
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v2, "SELECT * FROM images_table"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lsamsung/database/sqlite/SecSQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1408
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1412
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getMigrationUri()Landroid/net/Uri;

    move-result-object v2

    const-string/jumbo v3, "meal_image"

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getImageContentValuesFromCursor(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->cpInsert(Landroid/content/Context;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1413
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 1422
    :cond_1
    if-eqz v0, :cond_2

    .line 1424
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1427
    :cond_2
    :goto_0
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v3, " ---------- End migrateImagesData -------------- "

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1428
    return-void

    .line 1416
    :catch_0
    move-exception v1

    .line 1418
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1422
    if-eqz v0, :cond_2

    .line 1424
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1422
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_3

    .line 1424
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v2
.end method

.method private static migrateMapPathData(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V
    .locals 5
    .param p0, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 1038
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v4, " ---------- start migrateMapPathData -------------- "

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1039
    const/4 v0, 0x0

    .line 1042
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v3, "SELECT * FROM map_path"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lsamsung/database/sqlite/SecSQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1043
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1047
    :cond_0
    const-string v2, "location_data"

    .line 1048
    .local v2, "tableName":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getMigrationUri()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v0, p0}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getMapPathContentValuesFromCursor(Landroid/database/Cursor;Lsamsung/database/sqlite/SecSQLiteDatabase;)Landroid/content/ContentValues;

    move-result-object v4

    invoke-static {p1, v3, v4}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->cpInsert(Landroid/content/Context;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1049
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    .line 1058
    .end local v2    # "tableName":Ljava/lang/String;
    :cond_1
    if-eqz v0, :cond_2

    .line 1060
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1064
    :cond_2
    :goto_0
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v4, " ---------- End migrateMapPathData -------------- "

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1065
    return-void

    .line 1052
    :catch_0
    move-exception v1

    .line 1054
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1058
    if-eqz v0, :cond_2

    .line 1060
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1058
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_3

    .line 1060
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v3
.end method

.method private static migrateMealData(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V
    .locals 7
    .param p0, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 1270
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v4, " ---------- Start migrateMealData -------------- "

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1271
    const/4 v1, 0x0

    .line 1274
    .local v1, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v3, "SELECT * FROM meal"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lsamsung/database/sqlite/SecSQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1275
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1277
    const/4 v0, 0x0

    .line 1280
    .local v0, "createTime":Ljava/lang/Long;
    :cond_0
    const-string v3, "create_time"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 1282
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-nez v3, :cond_4

    .line 1285
    :cond_1
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    .line 1294
    .end local v0    # "createTime":Ljava/lang/Long;
    :cond_2
    if-eqz v1, :cond_3

    .line 1296
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1299
    :cond_3
    :goto_1
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v4, " ---------- End migrateMealData -------------- "

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1300
    return-void

    .line 1284
    .restart local v0    # "createTime":Ljava/lang/Long;
    :cond_4
    :try_start_1
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getMigrationUri()Landroid/net/Uri;

    move-result-object v3

    const-string/jumbo v4, "meal"

    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getMealContentValuesFromCursor(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v4

    invoke-static {p1, v3, v4}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->cpInsert(Landroid/content/Context;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1288
    .end local v0    # "createTime":Ljava/lang/Long;
    :catch_0
    move-exception v2

    .line 1290
    .local v2, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1294
    if-eqz v1, :cond_3

    .line 1296
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 1294
    .end local v2    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_5

    .line 1296
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v3
.end method

.method private static migrateMealItemData(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V
    .locals 5
    .param p0, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 1340
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v4, " ---------- Start migrateMealItemData -------------- "

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1341
    const/4 v0, 0x0

    .line 1344
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v3, "SELECT * FROM meal_items"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lsamsung/database/sqlite/SecSQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1345
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1349
    :cond_0
    const-string/jumbo v2, "meal_item"

    .line 1350
    .local v2, "tableName":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getMigrationUri()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getMealItemContentValuesFromCursor(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v4

    invoke-static {p1, v3, v4}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->cpInsert(Landroid/content/Context;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1351
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    .line 1360
    .end local v2    # "tableName":Ljava/lang/String;
    :cond_1
    if-eqz v0, :cond_2

    .line 1362
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1365
    :cond_2
    :goto_0
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v4, " ---------- Start migrateMealItemData -------------- "

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1366
    return-void

    .line 1354
    :catch_0
    move-exception v1

    .line 1356
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1360
    if-eqz v0, :cond_2

    .line 1362
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1360
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_3

    .line 1362
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v3
.end method

.method private static migrateRealtimeHeartbeatData(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V
    .locals 6
    .param p0, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 983
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v5, " ---------- start migrate Realtime Heartbeat -------------- "

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 984
    const/4 v0, 0x0

    .line 987
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 988
    .local v2, "realtimeContentValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    const-string/jumbo v3, "realtime_data"

    .line 989
    .local v3, "tableName":Ljava/lang/String;
    const-string v4, "SELECT * FROM realtime_heartbeat"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Lsamsung/database/sqlite/SecSQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 990
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 994
    :cond_0
    invoke-static {v0, p0}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getRealtimeHeartbeatContentValuesFromCursor(Landroid/database/Cursor;Lsamsung/database/sqlite/SecSQLiteDatabase;)Landroid/content/ContentValues;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 995
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 998
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 1000
    invoke-static {p1, v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->splitInsert(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1009
    :cond_2
    if-eqz v0, :cond_3

    .line 1011
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1015
    .end local v2    # "realtimeContentValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    .end local v3    # "tableName":Ljava/lang/String;
    :cond_3
    :goto_0
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v5, " ---------- end migrate Realtime Heartbeat -------------- "

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1016
    return-void

    .line 1003
    :catch_0
    move-exception v1

    .line 1005
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1009
    if-eqz v0, :cond_3

    .line 1011
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1009
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v4

    if-eqz v0, :cond_4

    .line 1011
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v4
.end method

.method private static migrateRealtimeSpeedData(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V
    .locals 6
    .param p0, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 928
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v5, " ---------- start migrate Realtime Speed -------------- "

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 929
    const/4 v0, 0x0

    .line 932
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 933
    .local v2, "realtimeContentValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    const-string/jumbo v3, "realtime_data"

    .line 934
    .local v3, "tableName":Ljava/lang/String;
    const-string v4, "SELECT * FROM realtime_speed"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Lsamsung/database/sqlite/SecSQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 935
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 939
    :cond_0
    invoke-static {v0, p0}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getRealtimeSpeedContentValuesFromCursor(Landroid/database/Cursor;Lsamsung/database/sqlite/SecSQLiteDatabase;)Landroid/content/ContentValues;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 940
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 943
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 945
    invoke-static {p1, v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->splitInsert(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 954
    :cond_2
    if-eqz v0, :cond_3

    .line 956
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 959
    .end local v2    # "realtimeContentValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    .end local v3    # "tableName":Ljava/lang/String;
    :cond_3
    :goto_0
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v5, " ---------- end migrate Realtime Speed -------------- "

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 960
    return-void

    .line 948
    :catch_0
    move-exception v1

    .line 950
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 954
    if-eqz v0, :cond_3

    .line 956
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 954
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v4

    if-eqz v0, :cond_4

    .line 956
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v4
.end method

.method private static migrateTrainingLoadPeakData(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V
    .locals 4
    .param p0, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1754
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v3, " ---------- Start TrainingLoadData-------------- "

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1755
    const/4 v0, 0x0

    .line 1758
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v2, "SELECT * FROM training_load_peak"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lsamsung/database/sqlite/SecSQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1759
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1763
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getMigrationUri()Landroid/net/Uri;

    move-result-object v2

    const-string v3, "first_beat_coaching_result"

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getTrainingLoadPeakContentValuesFromCursor(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->cpInsert(Landroid/content/Context;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1764
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 1773
    :cond_1
    if-eqz v0, :cond_2

    .line 1775
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1778
    :cond_2
    :goto_0
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v3, " ---------- End TrainingLoadData-------------- "

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1779
    return-void

    .line 1767
    :catch_0
    move-exception v1

    .line 1769
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1773
    if-eqz v0, :cond_2

    .line 1775
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1773
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_3

    .line 1775
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v2
.end method

.method private static migrateWalkInfo(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V
    .locals 8
    .param p0, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 533
    sget-object v5, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v6, " ---------- start migrateWalkInfo -------------- "

    invoke-static {v5, v6}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 534
    const/4 v0, 0x0

    .line 535
    .local v0, "cursor":Landroid/database/Cursor;
    const-wide/16 v5, 0x0

    sput-wide v5, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->last_createTime:J

    .line 536
    const-wide/16 v5, -0x1

    sput-wide v5, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->last_exerciseId:J

    .line 537
    sput v7, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->total_calorie:F

    .line 538
    sput v7, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->total_distance:F

    .line 539
    const/4 v5, 0x0

    sput v5, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->total_stepcount:I

    .line 543
    :try_start_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 544
    .local v4, "walkContentValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    const-string v5, "SELECT * FROM walk_for_life ORDER BY create_time"

    const/4 v6, 0x0

    invoke-virtual {p0, v5, v6}, Lsamsung/database/sqlite/SecSQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 545
    if-eqz v0, :cond_2

    .line 547
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 551
    :cond_0
    invoke-static {v0, p0, p1}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getWalkInfoContentValuesFromCursor(Landroid/database/Cursor;Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)Landroid/content/ContentValues;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 552
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 554
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 557
    :cond_2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_3

    .line 559
    const-string/jumbo v5, "walk_info"

    invoke-static {p1, v4, v5}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->splitInsert(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 562
    :cond_3
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 563
    .local v3, "walkBContentValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    const-string v5, "SELECT * FROM b_pedometer"

    const/4 v6, 0x0

    invoke-virtual {p0, v5, v6}, Lsamsung/database/sqlite/SecSQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 564
    if-eqz v0, :cond_5

    .line 566
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 570
    :cond_4
    invoke-static {v0, p0, p1}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getBWalkInfoContentValuesFromCursor(Landroid/database/Cursor;Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)Landroid/content/ContentValues;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 571
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-nez v5, :cond_4

    .line 575
    :cond_5
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_6

    .line 577
    const-string/jumbo v5, "walk_info"

    invoke-static {p1, v3, v5}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->splitInsert(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 587
    :cond_6
    if-eqz v0, :cond_7

    .line 589
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 596
    .end local v3    # "walkBContentValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    .end local v4    # "walkContentValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    :cond_7
    :goto_0
    :try_start_1
    const-string v2, "delete from exercise where start_time in (select start_time from walk_info) and exercise_type != 20003"

    .line 597
    .local v2, "exercise_deleteQuery":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getDatabase()Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v2, v6}, Lcom/sec/android/service/health/cp/database/SQLiteDatabaseWrapper;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 601
    if-eqz v0, :cond_8

    .line 603
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 608
    :cond_8
    sget-object v5, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v6, " ---------- End migrateWalkInfo -------------- "

    invoke-static {v5, v6}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 609
    return-void

    .line 581
    .end local v2    # "exercise_deleteQuery":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 583
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 587
    if-eqz v0, :cond_7

    .line 589
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 587
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v5

    if-eqz v0, :cond_9

    .line 589
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v5

    .line 601
    :catchall_1
    move-exception v5

    if-eqz v0, :cond_a

    .line 603
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_a
    throw v5
.end method

.method private static migrateWeightData(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V
    .locals 5
    .param p0, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 1450
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v4, " ---------- start migrateWeightData -------------- "

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1452
    const/4 v0, 0x0

    .line 1453
    .local v0, "cursor":Landroid/database/Cursor;
    invoke-static {p1}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getHeightFromProfile(Landroid/content/Context;)F

    move-result v2

    .line 1456
    .local v2, "profile_height":F
    :try_start_0
    const-string v3, "SELECT * FROM weight"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lsamsung/database/sqlite/SecSQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1457
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1461
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getMigrationUri()Landroid/net/Uri;

    move-result-object v3

    const-string/jumbo v4, "weight"

    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v0, v2, p1}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getWeightContentValuesFromCursor(Landroid/database/Cursor;FLandroid/content/Context;)Landroid/content/ContentValues;

    move-result-object v4

    invoke-static {p1, v3, v4}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->cpInsert(Landroid/content/Context;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1462
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    .line 1471
    :cond_1
    if-eqz v0, :cond_2

    .line 1473
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1477
    :cond_2
    :goto_0
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v4, " ---------- End migrateWeightData -------------- "

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1478
    return-void

    .line 1465
    :catch_0
    move-exception v1

    .line 1467
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1471
    if-eqz v0, :cond_2

    .line 1473
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1471
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_3

    .line 1473
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v3
.end method

.method private sendProgressBroadcast(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "i"    # I

    .prologue
    .line 202
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.shealth.RESTORE_PROGRESS_PERCENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 204
    .local v0, "progressIntent":Landroid/content/Intent;
    const-string/jumbo v1, "progress_percent"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 205
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 206
    return-void
.end method

.method public static setSharedPrefTimeZone(Ljava/lang/String;)V
    .locals 0
    .param p0, "timeZoneId"    # Ljava/lang/String;

    .prologue
    .line 114
    sput-object p0, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->SHARED_PREF_TIMEZONE_ID:Ljava/lang/String;

    .line 115
    return-void
.end method

.method private static splitInsert(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p2, "tableName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentValues;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 613
    .local p1, "valueList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    sget-object v5, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " doing bulk insert for  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " size "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 614
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_1

    .line 616
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 618
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/ContentValues;

    const-string/jumbo v6, "validation_policy"

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getMigrationPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->asMask()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 616
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 620
    :cond_0
    const/4 v2, 0x0

    .local v2, "start":I
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v2, v5, :cond_1

    .line 622
    sget v5, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->BULKINSERT_SIZE:I

    add-int/2addr v5, v2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 623
    .local v0, "end":I
    invoke-virtual {p1, v2, v0}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v3

    .line 624
    .local v3, "sublist":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentValues;>;"
    sget-object v5, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " doing sub bulk insert for walkBinfo "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 625
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    new-array v4, v5, [Landroid/content/ContentValues;

    .line 626
    .local v4, "values":[Landroid/content/ContentValues;
    invoke-interface {v3, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 627
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->getMigrationUri()Landroid/net/Uri;

    move-result-object v6

    invoke-static {v6, p2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    .line 628
    invoke-static {}, Ljava/lang/Thread;->yield()V

    .line 620
    sget v5, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->BULKINSERT_SIZE:I

    add-int/2addr v2, v5

    goto :goto_1

    .line 632
    .end local v0    # "end":I
    .end local v1    # "i":I
    .end local v2    # "start":I
    .end local v3    # "sublist":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentValues;>;"
    .end local v4    # "values":[Landroid/content/ContentValues;
    :cond_1
    return-void
.end method


# virtual methods
.method public doMigration(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;Z)V
    .locals 3
    .param p1, "db"    # Lsamsung/database/sqlite/SecSQLiteDatabase;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "isRestoreFromKies"    # Z

    .prologue
    .line 135
    :try_start_0
    sput-boolean p3, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->mIsKiesMode:Z

    .line 136
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil$MigrationStatus;->ONGOING:Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil$MigrationStatus;

    sput-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->status:Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil$MigrationStatus;

    .line 138
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v2, "doMigration"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    const/16 v1, 0xa

    invoke-direct {p0, p2, v1}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->sendProgressBroadcast(Landroid/content/Context;I)V

    .line 142
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->addPedometerUserDevice(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V

    .line 144
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->migrateFoodInfoData(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V

    .line 145
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->migrateExtendedFoodInfoData(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V

    .line 146
    const/16 v1, 0x14

    invoke-direct {p0, p2, v1}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->sendProgressBroadcast(Landroid/content/Context;I)V

    .line 148
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->migrateMealData(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V

    .line 149
    const/16 v1, 0x1e

    invoke-direct {p0, p2, v1}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->sendProgressBroadcast(Landroid/content/Context;I)V

    .line 151
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->migrateMealItemData(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V

    .line 152
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->migrateImagesData(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V

    .line 153
    const/16 v1, 0x28

    invoke-direct {p0, p2, v1}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->sendProgressBroadcast(Landroid/content/Context;I)V

    .line 155
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->migrateWeightData(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V

    .line 156
    const/16 v1, 0x32

    invoke-direct {p0, p2, v1}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->sendProgressBroadcast(Landroid/content/Context;I)V

    .line 158
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->migrateBloodPressureData(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V

    .line 159
    const/16 v1, 0x3c

    invoke-direct {p0, p2, v1}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->sendProgressBroadcast(Landroid/content/Context;I)V

    .line 161
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->migrateBloodGlucoseData(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V

    .line 162
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->migrateGoalData(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V

    .line 163
    const/16 v1, 0x46

    invoke-direct {p0, p2, v1}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->sendProgressBroadcast(Landroid/content/Context;I)V

    .line 166
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->migrateExerciseData(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V

    .line 167
    const/16 v1, 0x50

    invoke-direct {p0, p2, v1}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->sendProgressBroadcast(Landroid/content/Context;I)V

    .line 169
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->migrateWalkInfo(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V

    .line 170
    const/16 v1, 0x5a

    invoke-direct {p0, p2, v1}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->sendProgressBroadcast(Landroid/content/Context;I)V

    .line 172
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->migrateRealtimeHeartbeatData(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V

    .line 173
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->migrateRealtimeSpeedData(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V

    .line 174
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->migrateMapPathData(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V

    .line 176
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->migrateTrainingLoadPeakData(Lsamsung/database/sqlite/SecSQLiteDatabase;Landroid/content/Context;)V

    .line 177
    const/16 v1, 0x64

    invoke-direct {p0, p2, v1}, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->sendProgressBroadcast(Landroid/content/Context;I)V

    .line 179
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v2, "doMigration - completed"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 187
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil$MigrationStatus;->IDLE:Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil$MigrationStatus;

    sput-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->status:Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil$MigrationStatus;

    .line 189
    :goto_0
    return-void

    .line 181
    :catch_0
    move-exception v0

    .line 183
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_1
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->TAG:Ljava/lang/String;

    const-string v2, "doMigration - canceled"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 187
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil$MigrationStatus;->IDLE:Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil$MigrationStatus;

    sput-object v1, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->status:Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil$MigrationStatus;

    goto :goto_0

    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil$MigrationStatus;->IDLE:Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil$MigrationStatus;

    sput-object v2, Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil;->status:Lcom/sec/android/app/shealth/framework/repository/database/utils/HtoKMigrationUtil$MigrationStatus;

    throw v1
.end method
