.class public Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;
.super Ljava/lang/Object;
.source "SuggestedDataFactory.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static UpdateGoalStatus(ILcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;)V
    .locals 5
    .param p0, "goalId"    # I
    .param p1, "status"    # Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    .prologue
    .line 196
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/cigna/coach/factory/CoachFactory;->getGoalsMissions(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IGoalsMissions;

    move-result-object v1

    .line 197
    .local v1, "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v2

    .line 200
    .local v2, "userId":Ljava/lang/String;
    :try_start_0
    invoke-interface {v1, v2, p0, p1}, Lcom/cigna/coach/interfaces/IGoalsMissions;->setUserGoalStatus(Ljava/lang/String;ILcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;)Lcom/cigna/coach/apiobjects/CoachResponse;
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    .line 204
    :goto_0
    return-void

    .line 201
    :catch_0
    move-exception v0

    .line 202
    .local v0, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v0}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    goto :goto_0
.end method

.method public static UpdateMissionStatus(IILcom/cigna/coach/apiobjects/MissionOptions;)V
    .locals 5
    .param p0, "goalId"    # I
    .param p1, "missionId"    # I
    .param p2, "mo"    # Lcom/cigna/coach/apiobjects/MissionOptions;

    .prologue
    .line 207
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/cigna/coach/factory/CoachFactory;->getGoalsMissions(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IGoalsMissions;

    move-result-object v1

    .line 208
    .local v1, "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v2

    .line 212
    .local v2, "userId":Ljava/lang/String;
    :try_start_0
    invoke-interface {v1, v2, p0, p1, p2}, Lcom/cigna/coach/interfaces/IGoalsMissions;->setUserMission(Ljava/lang/String;IILcom/cigna/coach/apiobjects/MissionOptions;)Lcom/cigna/coach/apiobjects/CoachResponse;
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    .line 216
    :goto_0
    return-void

    .line 213
    :catch_0
    move-exception v0

    .line 214
    .local v0, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v0}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    goto :goto_0
.end method

.method public static createGoalListData(Landroid/content/Context;)Ljava/util/List;
    .locals 21
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 42
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 43
    .local v12, "goalListData":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;>;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 44
    .local v7, "coachPickData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    const/16 v18, 0x0

    .line 47
    .local v18, "moreGoalData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    :try_start_0
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/cigna/coach/factory/CoachFactory;->getGoalsMissions(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IGoalsMissions;

    move-result-object v15

    .line 48
    .local v15, "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v20

    .line 50
    .local v20, "userId":Ljava/lang/String;
    move-object/from16 v0, v20

    invoke-interface {v15, v0}, Lcom/cigna/coach/interfaces/IGoalsMissions;->getUserAvailableGoals(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/CoachResponse;

    move-result-object v8

    .line 52
    .local v8, "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;>;"
    invoke-virtual {v8}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/List;

    .line 53
    .local v11, "giList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .line 55
    .local v14, "icm":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x3

    if-le v2, v3, :cond_0

    .line 56
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .end local v18    # "moreGoalData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    .local v19, "moreGoalData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    move-object/from16 v18, v19

    .line 58
    .end local v19    # "moreGoalData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    .restart local v18    # "moreGoalData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    :cond_0
    const/4 v13, 0x0

    .line 60
    .local v13, "i":I
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 102
    :cond_1
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 103
    new-instance v2, Landroid/util/Pair;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f090337

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x3

    if-le v2, v3, :cond_2

    .line 105
    new-instance v2, Landroid/util/Pair;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f090338

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-direct {v2, v3, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    .end local v8    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;>;"
    .end local v11    # "giList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    .end local v13    # "i":I
    .end local v14    # "icm":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    .end local v15    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .end local v20    # "userId":Ljava/lang/String;
    :cond_2
    :goto_0
    return-object v12

    .line 63
    .restart local v8    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;>;"
    .restart local v11    # "giList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    .restart local v13    # "i":I
    .restart local v14    # "icm":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    .restart local v15    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .restart local v20    # "userId":Ljava/lang/String;
    :cond_3
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 64
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/cigna/coach/apiobjects/GoalInfo;

    .line 79
    .local v10, "gi":Lcom/cigna/coach/apiobjects/GoalInfo;
    const/16 v17, 0x0

    .line 80
    .local v17, "missionToComplete":I
    const/16 v16, 0x0

    .line 81
    .local v16, "minMissionFrequencyNum":I
    instance-of v2, v10, Lcom/cigna/coach/dataobjects/GoalData;

    if-eqz v2, :cond_4

    .line 82
    move-object v0, v10

    check-cast v0, Lcom/cigna/coach/dataobjects/GoalData;

    move-object v2, v0

    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/GoalData;->getNumMissionsToComplete()I

    move-result v17

    .line 83
    move-object v0, v10

    check-cast v0, Lcom/cigna/coach/dataobjects/GoalData;

    move-object v2, v0

    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/GoalData;->getMinMissionFrequencyNumber()I

    move-result v16

    .line 86
    :cond_4
    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;

    invoke-virtual {v10}, Lcom/cigna/coach/apiobjects/GoalInfo;->getGoalId()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v10}, Lcom/cigna/coach/apiobjects/GoalInfo;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->getCignaGoalIconIdByCategory(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v10}, Lcom/cigna/coach/apiobjects/GoalInfo;->getGoal()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10}, Lcom/cigna/coach/apiobjects/GoalInfo;->getGoalFrequency()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v10}, Lcom/cigna/coach/apiobjects/GoalInfo;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;-><init>(ILandroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V

    .line 89
    .local v1, "goalData":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    move/from16 v0, v17

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->setMissionToComplete(I)V

    .line 90
    move/from16 v0, v16

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->setMinFrequency(I)V

    .line 92
    const/4 v2, 0x3

    if-ge v13, v2, :cond_5

    .line 93
    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    :goto_2
    add-int/lit8 v13, v13, 0x1

    .line 99
    goto :goto_1

    .line 95
    :cond_5
    move-object/from16 v0, v18

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 109
    .end local v1    # "goalData":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    .end local v8    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;>;"
    .end local v10    # "gi":Lcom/cigna/coach/apiobjects/GoalInfo;
    .end local v11    # "giList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    .end local v13    # "i":I
    .end local v14    # "icm":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    .end local v15    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .end local v16    # "minMissionFrequencyNum":I
    .end local v17    # "missionToComplete":I
    .end local v20    # "userId":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 110
    .local v9, "e":Ljava/lang/Throwable;
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception processing displayUserAvailableGoals, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v9}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public static createGoalListDataByCategory(Landroid/content/Context;I)Ljava/util/List;
    .locals 21
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "category"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 118
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 119
    .local v12, "goalListData":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;>;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 120
    .local v7, "coachPickData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    const/16 v18, 0x0

    .line 123
    .local v18, "moreGoalData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    :try_start_0
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/cigna/coach/factory/CoachFactory;->getGoalsMissions(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IGoalsMissions;

    move-result-object v15

    .line 124
    .local v15, "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v20

    .line 126
    .local v20, "userId":Ljava/lang/String;
    move-object/from16 v0, v20

    invoke-interface {v15, v0}, Lcom/cigna/coach/interfaces/IGoalsMissions;->getUserAvailableGoals(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/CoachResponse;

    move-result-object v8

    .line 128
    .local v8, "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;>;"
    invoke-virtual {v8}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/List;

    .line 129
    .local v11, "giList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .line 131
    .local v14, "icm":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x3

    if-le v2, v3, :cond_0

    .line 132
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .end local v18    # "moreGoalData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    .local v19, "moreGoalData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    move-object/from16 v18, v19

    .line 134
    .end local v19    # "moreGoalData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    .restart local v18    # "moreGoalData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    :cond_0
    const/4 v13, 0x0

    .line 136
    .local v13, "i":I
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 181
    :cond_1
    if-lez v13, :cond_2

    .line 182
    new-instance v2, Landroid/util/Pair;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f090337

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 183
    const/4 v2, 0x3

    if-le v13, v2, :cond_2

    .line 184
    new-instance v2, Landroid/util/Pair;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f090338

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-direct {v2, v3, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 192
    .end local v8    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;>;"
    .end local v11    # "giList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    .end local v13    # "i":I
    .end local v14    # "icm":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    .end local v15    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .end local v20    # "userId":Ljava/lang/String;
    :cond_2
    :goto_0
    return-object v12

    .line 139
    .restart local v8    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;>;"
    .restart local v11    # "giList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    .restart local v13    # "i":I
    .restart local v14    # "icm":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    .restart local v15    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .restart local v20    # "userId":Ljava/lang/String;
    :cond_3
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 140
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/cigna/coach/apiobjects/GoalInfo;

    .line 143
    .local v10, "gi":Lcom/cigna/coach/apiobjects/GoalInfo;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v2

    invoke-virtual {v10}, Lcom/cigna/coach/apiobjects/GoalInfo;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v3

    move/from16 v0, p1

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->isCignaGoalMatchedCategory(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 157
    const/16 v17, 0x0

    .line 158
    .local v17, "missionToComplete":I
    const/16 v16, 0x0

    .line 159
    .local v16, "minMissionFrequencyNum":I
    instance-of v2, v10, Lcom/cigna/coach/dataobjects/GoalData;

    if-eqz v2, :cond_4

    .line 160
    move-object v0, v10

    check-cast v0, Lcom/cigna/coach/dataobjects/GoalData;

    move-object v2, v0

    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/GoalData;->getNumMissionsToComplete()I

    move-result v17

    .line 161
    move-object v0, v10

    check-cast v0, Lcom/cigna/coach/dataobjects/GoalData;

    move-object v2, v0

    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/GoalData;->getMinMissionFrequencyNumber()I

    move-result v16

    .line 164
    :cond_4
    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;

    invoke-virtual {v10}, Lcom/cigna/coach/apiobjects/GoalInfo;->getGoalId()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v10}, Lcom/cigna/coach/apiobjects/GoalInfo;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->getCignaGoalIconIdByCategory(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v10}, Lcom/cigna/coach/apiobjects/GoalInfo;->getGoal()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10}, Lcom/cigna/coach/apiobjects/GoalInfo;->getGoalFrequency()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v10}, Lcom/cigna/coach/apiobjects/GoalInfo;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;-><init>(ILandroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V

    .line 167
    .local v1, "goalData":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    move/from16 v0, v17

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->setMissionToComplete(I)V

    .line 168
    move/from16 v0, v16

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->setMinFrequency(I)V

    .line 170
    const/4 v2, 0x3

    if-ge v13, v2, :cond_5

    .line 171
    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    :goto_2
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 173
    :cond_5
    move-object/from16 v0, v18

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 188
    .end local v1    # "goalData":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    .end local v8    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;>;"
    .end local v10    # "gi":Lcom/cigna/coach/apiobjects/GoalInfo;
    .end local v11    # "giList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    .end local v13    # "i":I
    .end local v14    # "icm":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    .end local v15    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .end local v16    # "minMissionFrequencyNum":I
    .end local v17    # "missionToComplete":I
    .end local v20    # "userId":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 189
    .local v9, "e":Ljava/lang/Throwable;
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception processing displayUserAvailableGoals, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v9}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public static createMissionListData(Landroid/content/Context;II)Ljava/util/List;
    .locals 23
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "goalID"    # I
    .param p2, "fromWhere"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II)",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 305
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 306
    .local v19, "missionListData":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;>;>;"
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 307
    .local v11, "currentMissionData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    const/16 v20, 0x0

    .line 310
    .local v20, "moreMissionData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    :try_start_0
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Lcom/cigna/coach/factory/CoachFactory;->getGoalsMissions(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IGoalsMissions;

    move-result-object v17

    .line 311
    .local v17, "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v22

    .line 314
    .local v22, "userId":Ljava/lang/String;
    move-object/from16 v0, v17

    move-object/from16 v1, v22

    move/from16 v2, p1

    invoke-interface {v0, v1, v2}, Lcom/cigna/coach/interfaces/IGoalsMissions;->getUserAvailableGoalMissions(Ljava/lang/String;I)Lcom/cigna/coach/apiobjects/CoachResponse;

    move-result-object v10

    .line 316
    .local v10, "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;>;"
    invoke-virtual {v10}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/List;

    .line 318
    .local v14, "gmiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;"
    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .line 320
    .local v16, "igmi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;"
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x3

    if-le v3, v4, :cond_0

    .line 321
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .end local v20    # "moreMissionData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    .local v21, "moreMissionData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    move-object/from16 v20, v21

    .line 323
    .end local v21    # "moreMissionData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    .restart local v20    # "moreMissionData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    :cond_0
    const/4 v15, 0x0

    .line 325
    .local v15, "i":I
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 326
    sget-object v3, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->TAG:Ljava/lang/String;

    const-string v4, "Goal Mission List - None Defined"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    :cond_1
    sget-object v3, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Total Mission Size = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Total Mission Count ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    if-lez v15, :cond_2

    .line 367
    new-instance v3, Landroid/util/Pair;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f090337

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v11}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 369
    const/4 v3, 0x3

    if-le v15, v3, :cond_2

    .line 370
    new-instance v3, Landroid/util/Pair;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f09033a

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-direct {v3, v4, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 378
    .end local v10    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;>;"
    .end local v14    # "gmiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;"
    .end local v15    # "i":I
    .end local v16    # "igmi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;"
    .end local v17    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .end local v22    # "userId":Ljava/lang/String;
    :cond_2
    :goto_0
    return-object v19

    .line 328
    .restart local v10    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;>;"
    .restart local v14    # "gmiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;"
    .restart local v15    # "i":I
    .restart local v16    # "igmi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;"
    .restart local v17    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .restart local v22    # "userId":Ljava/lang/String;
    :cond_3
    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 329
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/cigna/coach/apiobjects/GoalMissionInfo;

    .line 331
    .local v13, "gmi":Lcom/cigna/coach/apiobjects/GoalMissionInfo;
    move-object v0, v13

    check-cast v0, Lcom/cigna/coach/dataobjects/GoalMissionData;

    move-object v3, v0

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/GoalMissionData;->getPrimaryCategory()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v18

    .line 343
    .local v18, "missionCategory":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    const/4 v3, 0x3

    if-ge v15, v3, :cond_4

    .line 344
    new-instance v3, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v13}, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->getGoalId()I

    move-result v4

    invoke-virtual {v13}, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->getMissionId()I

    move-result v5

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->getCignaGoalIconIdByCategory(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v13}, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->getMission()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v13}, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->getFrequencyDefault()Ljava/lang/String;

    move-result-object v8

    check-cast v13, Lcom/cigna/coach/dataobjects/GoalMissionData;

    .end local v13    # "gmi":Lcom/cigna/coach/apiobjects/GoalMissionInfo;
    invoke-virtual {v13}, Lcom/cigna/coach/dataobjects/GoalMissionData;->getMissionStage()Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->isRepeatable(Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;)Z

    move-result v9

    invoke-direct/range {v3 .. v9}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;-><init>(IILandroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 353
    :goto_2
    add-int/lit8 v15, v15, 0x1

    .line 354
    goto :goto_1

    .line 348
    .restart local v13    # "gmi":Lcom/cigna/coach/apiobjects/GoalMissionInfo;
    :cond_4
    new-instance v3, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v13}, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->getGoalId()I

    move-result v4

    invoke-virtual {v13}, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->getMissionId()I

    move-result v5

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->getCignaGoalIconIdByCategory(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v13}, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->getMission()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v13}, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->getFrequencyDefault()Ljava/lang/String;

    move-result-object v8

    check-cast v13, Lcom/cigna/coach/dataobjects/GoalMissionData;

    .end local v13    # "gmi":Lcom/cigna/coach/apiobjects/GoalMissionInfo;
    invoke-virtual {v13}, Lcom/cigna/coach/dataobjects/GoalMissionData;->getMissionStage()Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->isRepeatable(Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;)Z

    move-result v9

    invoke-direct/range {v3 .. v9}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;-><init>(IILandroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, v20

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 373
    .end local v10    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;>;"
    .end local v14    # "gmiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;"
    .end local v15    # "i":I
    .end local v16    # "igmi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;"
    .end local v17    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .end local v18    # "missionCategory":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .end local v22    # "userId":Ljava/lang/String;
    :catch_0
    move-exception v12

    .line 374
    .local v12, "e":Ljava/lang/Exception;
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V

    .line 375
    sget-object v3, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception in setGoal, "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v12}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public static createUserMissionDetailData(I)Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    .locals 9
    .param p0, "missionId"    # I

    .prologue
    .line 516
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v6

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/cigna/coach/factory/CoachFactory;->getGoalsMissions(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IGoalsMissions;

    move-result-object v4

    .line 517
    .local v4, "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v5

    .line 521
    .local v5, "userId":Ljava/lang/String;
    :try_start_0
    invoke-interface {v4, v5, p0}, Lcom/cigna/coach/interfaces/IGoalsMissions;->getUserMissionDetails(Ljava/lang/String;I)Lcom/cigna/coach/apiobjects/CoachResponse;

    move-result-object v1

    .line 523
    .local v1, "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v0

    .line 524
    .local v0, "cmList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->displayCoachMessages(Ljava/util/List;)V

    .line 526
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;

    .line 528
    .local v3, "gmdi":Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;
    invoke-static {v3}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->valueOf(Lcom/cigna/coach/apiobjects/GoalMissionInfo;)Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 534
    .end local v0    # "cmList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v1    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .end local v3    # "gmdi":Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;
    :goto_0
    return-object v6

    .line 530
    :catch_0
    move-exception v2

    .line 531
    .local v2, "e":Ljava/lang/Throwable;
    sget-object v6, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception processing displayUserAvailableGoals, "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 534
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private static displayCoachMessages(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 608
    .local p0, "cmList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    :try_start_0
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 609
    .local v1, "icm":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 610
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->TAG:Ljava/lang/String;

    const-string v3, "Coach Message Type - None Defined"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 622
    .end local v1    # "icm":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    :cond_0
    :goto_0
    return-void

    .line 612
    .restart local v1    # "icm":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 613
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/apiobjects/CoachMessage;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 619
    .end local v1    # "icm":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    :catch_0
    move-exception v0

    .line 620
    .local v0, "e":Ljava/lang/Throwable;
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception processing displayCoachMessages, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getAvailableGoalData(I)Lcom/cigna/coach/apiobjects/GoalInfo;
    .locals 10
    .param p0, "goalId"    # I

    .prologue
    .line 409
    :try_start_0
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v7

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/cigna/coach/factory/CoachFactory;->getGoalsMissions(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IGoalsMissions;

    move-result-object v5

    .line 410
    .local v5, "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v6

    .line 412
    .local v6, "userId":Ljava/lang/String;
    invoke-interface {v5, v6}, Lcom/cigna/coach/interfaces/IGoalsMissions;->getUserAvailableGoals(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/CoachResponse;

    move-result-object v0

    .line 414
    .local v0, "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;>;"
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 415
    .local v3, "giList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 416
    .local v4, "icm":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 438
    .end local v0    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;>;"
    .end local v3    # "giList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    .end local v4    # "icm":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    .end local v5    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .end local v6    # "userId":Ljava/lang/String;
    :cond_0
    :goto_0
    const/4 v2, 0x0

    :goto_1
    return-object v2

    .line 419
    .restart local v0    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;>;"
    .restart local v3    # "giList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    .restart local v4    # "icm":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    .restart local v5    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .restart local v6    # "userId":Ljava/lang/String;
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 420
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/apiobjects/GoalInfo;

    .line 429
    .local v2, "gi":Lcom/cigna/coach/apiobjects/GoalInfo;
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/GoalInfo;->getGoalId()I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    if-ne p0, v7, :cond_1

    goto :goto_1

    .line 435
    .end local v0    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;>;"
    .end local v2    # "gi":Lcom/cigna/coach/apiobjects/GoalInfo;
    .end local v3    # "giList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    .end local v4    # "icm":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    .end local v5    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .end local v6    # "userId":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 436
    .local v1, "e":Ljava/lang/Throwable;
    sget-object v7, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Exception processing displayUserAvailableGoals, "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getAvailableMissionData(II)Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    .locals 11
    .param p0, "goalId"    # I
    .param p1, "missionId"    # I

    .prologue
    .line 449
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v8

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/cigna/coach/factory/CoachFactory;->getGoalsMissions(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IGoalsMissions;

    move-result-object v6

    .line 450
    .local v6, "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v7

    .line 454
    .local v7, "userId":Ljava/lang/String;
    :try_start_0
    invoke-interface {v6, v7, p0}, Lcom/cigna/coach/interfaces/IGoalsMissions;->getUserAvailableGoalMissions(Ljava/lang/String;I)Lcom/cigna/coach/apiobjects/CoachResponse;

    move-result-object v1

    .line 456
    .local v1, "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;>;"
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v0

    .line 457
    .local v0, "cmList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->displayCoachMessages(Ljava/util/List;)V

    .line 459
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 461
    .local v4, "gmiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 463
    .local v5, "igmi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;"
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_1

    .line 464
    sget-object v8, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->TAG:Ljava/lang/String;

    const-string v9, "Goal Mission List - None Defined"

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    .end local v0    # "cmList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v1    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;>;"
    .end local v4    # "gmiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;"
    .end local v5    # "igmi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;"
    :cond_0
    :goto_0
    const/4 v8, 0x0

    :goto_1
    return-object v8

    .line 466
    .restart local v0    # "cmList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .restart local v1    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;>;"
    .restart local v4    # "gmiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;"
    .restart local v5    # "igmi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;"
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 467
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cigna/coach/apiobjects/GoalMissionInfo;

    .line 469
    .local v3, "gmi":Lcom/cigna/coach/apiobjects/GoalMissionInfo;
    invoke-virtual {v3}, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->getMissionId()I

    move-result v8

    if-ne p1, v8, :cond_1

    .line 471
    invoke-static {v3}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->valueOf(Lcom/cigna/coach/apiobjects/GoalMissionInfo;)Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    goto :goto_1

    .line 476
    .end local v0    # "cmList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v1    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;>;"
    .end local v3    # "gmi":Lcom/cigna/coach/apiobjects/GoalMissionInfo;
    .end local v4    # "gmiList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;"
    .end local v5    # "igmi":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;"
    :catch_0
    move-exception v2

    .line 477
    .local v2, "e":Ljava/lang/Throwable;
    sget-object v8, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Exception processing displayUserAvailableGoals, "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getGoalCoachMessage()Ljava/lang/String;
    .locals 10

    .prologue
    .line 241
    :try_start_0
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v7

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/cigna/coach/factory/CoachFactory;->getGoalsMissions(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IGoalsMissions;

    move-result-object v5

    .line 242
    .local v5, "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v6

    .line 245
    .local v6, "userId":Ljava/lang/String;
    invoke-interface {v5, v6}, Lcom/cigna/coach/interfaces/IGoalsMissions;->getUserAvailableGoals(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/CoachResponse;

    move-result-object v2

    .line 248
    .local v2, "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;>;"
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v1

    .line 250
    .local v1, "cmList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 251
    .local v4, "icm":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 252
    sget-object v7, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->TAG:Ljava/lang/String;

    const-string v8, "Coach Message Type - None Defined"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    .end local v1    # "cmList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v2    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;>;"
    .end local v4    # "icm":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v6    # "userId":Ljava/lang/String;
    :cond_0
    :goto_0
    const/4 v7, 0x0

    :goto_1
    return-object v7

    .line 254
    .restart local v1    # "cmList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .restart local v2    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;>;"
    .restart local v4    # "icm":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .restart local v6    # "userId":Ljava/lang/String;
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 255
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/CoachMessage;

    .line 261
    .local v0, "cm":Lcom/cigna/coach/apiobjects/CoachMessage;
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageDescription()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    goto :goto_1

    .line 265
    .end local v0    # "cm":Lcom/cigna/coach/apiobjects/CoachMessage;
    .end local v1    # "cmList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v2    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;>;"
    .end local v4    # "icm":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v6    # "userId":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 266
    .local v3, "e":Ljava/lang/Throwable;
    sget-object v7, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Exception processing displayUserAvailableGoals, "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getGoalInfoForMission(I)Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
    .locals 10
    .param p0, "goalID"    # I

    .prologue
    .line 626
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v7

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/cigna/coach/factory/CoachFactory;->getGoalsMissions(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IGoalsMissions;

    move-result-object v4

    .line 627
    .local v4, "igm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v6

    .line 630
    .local v6, "userId":Ljava/lang/String;
    const/4 v7, 0x0

    :try_start_0
    invoke-interface {v4, v6, v7}, Lcom/cigna/coach/interfaces/IGoalsMissions;->getUserSelectedGoalMissions(Ljava/lang/String;Z)Lcom/cigna/coach/apiobjects/CoachResponse;

    move-result-object v5

    .line 631
    .local v5, "response":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    invoke-virtual {v5}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 633
    .local v0, "availableMissions":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;

    .line 638
    .local v2, "goalMisssion":Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->getGoalId()I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    if-ne p0, v7, :cond_0

    .line 653
    .end local v0    # "availableMissions":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .end local v2    # "goalMisssion":Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v5    # "response":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    :goto_0
    return-object v2

    .line 650
    :catch_0
    move-exception v1

    .line 651
    .local v1, "e":Ljava/lang/Throwable;
    sget-object v7, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Exception processing displayUserAvailableGoals, "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 653
    .end local v1    # "e":Ljava/lang/Throwable;
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getMissionCoachMessage(I)Ljava/lang/String;
    .locals 10
    .param p0, "goalId"    # I

    .prologue
    .line 275
    :try_start_0
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v7

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/cigna/coach/factory/CoachFactory;->getGoalsMissions(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IGoalsMissions;

    move-result-object v5

    .line 276
    .local v5, "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v6

    .line 278
    .local v6, "userId":Ljava/lang/String;
    invoke-interface {v5, v6, p0}, Lcom/cigna/coach/interfaces/IGoalsMissions;->getUserAvailableGoalMissions(Ljava/lang/String;I)Lcom/cigna/coach/apiobjects/CoachResponse;

    move-result-object v2

    .line 280
    .local v2, "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;>;"
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v1

    .line 282
    .local v1, "cmList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 283
    .local v4, "icm":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 284
    sget-object v7, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->TAG:Ljava/lang/String;

    const-string v8, "Coach Message Type - None Defined"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 300
    .end local v1    # "cmList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v2    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;>;"
    .end local v4    # "icm":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v5    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .end local v6    # "userId":Ljava/lang/String;
    :cond_0
    :goto_0
    sget-object v7, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->TAG:Ljava/lang/String;

    const-string v8, "MissionCoachMessage is empty ~~"

    invoke-static {v7, v8}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    const-string v7, ""

    :goto_1
    return-object v7

    .line 286
    .restart local v1    # "cmList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .restart local v2    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;>;"
    .restart local v4    # "icm":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .restart local v5    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .restart local v6    # "userId":Ljava/lang/String;
    :cond_1
    :try_start_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 287
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/CoachMessage;

    .line 293
    .local v0, "cm":Lcom/cigna/coach/apiobjects/CoachMessage;
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageDescription()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v7

    goto :goto_1

    .line 297
    .end local v0    # "cm":Lcom/cigna/coach/apiobjects/CoachMessage;
    .end local v1    # "cmList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v2    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;>;"
    .end local v4    # "icm":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v5    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .end local v6    # "userId":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 298
    .local v3, "e":Ljava/lang/Throwable;
    sget-object v7, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Exception processing displayUserAvailableGoals, "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static setUserMission(IIILcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;J)V
    .locals 8
    .param p0, "goalId"    # I
    .param p1, "missionId"    # I
    .param p2, "frequenceIndex"    # I
    .param p3, "status"    # Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;
    .param p4, "startDateTimeMillis"    # J

    .prologue
    .line 221
    :try_start_0
    new-instance v3, Lcom/cigna/coach/apiobjects/MissionOptions;

    invoke-direct {v3}, Lcom/cigna/coach/apiobjects/MissionOptions;-><init>()V

    .line 222
    .local v3, "mo":Lcom/cigna/coach/apiobjects/MissionOptions;
    invoke-static {p4, p5}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance(J)Ljava/util/GregorianCalendar;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/cigna/coach/apiobjects/MissionOptions;->setMissionStartDate(Ljava/util/Calendar;)V

    .line 223
    invoke-virtual {v3, p3}, Lcom/cigna/coach/apiobjects/MissionOptions;->setMissionStatus(Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;)V

    .line 224
    invoke-virtual {v3, p2}, Lcom/cigna/coach/apiobjects/MissionOptions;->setSelectedFrequency(I)V

    .line 226
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v0

    .line 227
    .local v0, "cf":Lcom/cigna/coach/factory/CoachFactory;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/cigna/coach/factory/CoachFactory;->getGoalsMissions(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IGoalsMissions;

    move-result-object v2

    .line 228
    .local v2, "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v4

    .line 230
    .local v4, "userId":Ljava/lang/String;
    invoke-interface {v2, v4, p0, p1, v3}, Lcom/cigna/coach/interfaces/IGoalsMissions;->setUserMission(Ljava/lang/String;IILcom/cigna/coach/apiobjects/MissionOptions;)Lcom/cigna/coach/apiobjects/CoachResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 236
    .end local v0    # "cf":Lcom/cigna/coach/factory/CoachFactory;
    .end local v2    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    .end local v3    # "mo":Lcom/cigna/coach/apiobjects/MissionOptions;
    .end local v4    # "userId":Ljava/lang/String;
    :goto_0
    return-void

    .line 233
    :catch_0
    move-exception v1

    .line 234
    .local v1, "e":Ljava/lang/Exception;
    sget-object v5, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception in setGoal, "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
