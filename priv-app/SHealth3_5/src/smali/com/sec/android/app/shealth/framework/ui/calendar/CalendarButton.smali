.class public Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;
.super Ljava/lang/Object;


# static fields
.field public static final DAY_TYPE_DATA:I = 0x6

.field public static final DAY_TYPE_DIM:I = 0x1

.field public static final DAY_TYPE_REGULAR:I = 0x0

.field public static final DAY_TYPE_SELECTED:I = 0x4

.field public static final DAY_TYPE_SUNDAY:I = 0x2

.field public static final DAY_TYPE_SUNDAY_DIM:I = 0x3

.field public static final DAY_TYPE_TODAY:I = 0x5


# instance fields
.field protected mContent:Landroid/view/View;

.field protected mDayBackground:Landroid/widget/RelativeLayout;

.field protected mMedal:Landroid/widget/ImageView;

.field protected mText:Landroid/widget/TextView;

.field resources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/content/res/Resources;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mContent:Landroid/view/View;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mContent:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->calendar_button_button1:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mDayBackground:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mContent:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->calendar_button_text1:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mText:Landroid/widget/TextView;

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->resources:Landroid/content/res/Resources;

    return-void
.end method


# virtual methods
.method public enableMedalView()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mContent:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->calendar_button_medal:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mMedal:Landroid/widget/ImageView;

    return-void
.end method

.method public getView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mContent:Landroid/view/View;

    return-object v0
.end method

.method public setDayType(I)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->resources:Landroid/content/res/Resources;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$color;->calendar_day_color:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mDayBackground:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->resources:Landroid/content/res/Resources;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$color;->calendar_text_day_dim:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mDayBackground:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mDayBackground:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mDayBackground:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->resources:Landroid/content/res/Resources;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$color;->default_background_color:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mDayBackground:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setPressed(Z)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->resources:Landroid/content/res/Resources;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$color;->calendar_text_sunday:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mDayBackground:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->resources:Landroid/content/res/Resources;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$color;->calendar_text_sunday_dim:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mDayBackground:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mDayBackground:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mDayBackground:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->resources:Landroid/content/res/Resources;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$color;->default_background_color:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mDayBackground:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setPressed(Z)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mDayBackground:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mDayBackground:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->resources:Landroid/content/res/Resources;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$color;->calendar_day_selected:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mDayBackground:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mDayBackground:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    goto/16 :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mText:Landroid/widget/TextView;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->cal_day_select:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->resources:Landroid/content/res/Resources;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$color;->calendar_day_today:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mDayBackground:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method setMedalImage(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mMedal:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mMedal:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method public setMedalState(Z)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mMedal:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mMedal:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setSelected(Z)V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mMedal:Landroid/widget/ImageView;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mDayBackground:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setSelected(Z)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isSelected()Z

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mContent:Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->setSelected(Z)V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mDayBackground:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, p1}, Landroid/widget/RelativeLayout;->setSelected(Z)V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setSelected(Z)V

    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mDayBackground:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method
