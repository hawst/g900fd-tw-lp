.class Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncEventListener;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WearableSyncEventListener"
.end annotation


# instance fields
.field private mSensorDevice:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

.field private mSyncEventListener:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$SyncEventListener;

.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$SyncEventListener;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncEventListener;->mSensorDevice:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    iput-object p3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncEventListener;->mSyncEventListener:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$SyncEventListener;

    return-void
.end method


# virtual methods
.method public onJoined(I)V
    .locals 52

    const/4 v1, 0x2

    new-array v4, v1, [J

    const/4 v1, 0x1

    const-wide/16 v2, 0x1

    aput-wide v2, v4, v1

    const/4 v1, 0x0

    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v4, v2

    long-to-int v2, v2

    if-gtz v2, :cond_0

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v3, 0x0

    move/from16 v0, p1

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    ushr-long v5, v1, v5

    aget-wide v1, v4, v3

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_1

    const-wide v7, 0xc2231fd0fee3890L    # 3.1766697016344E-250

    xor-long/2addr v1, v7

    :cond_1
    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    const/16 v7, 0x20

    shl-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, 0xc2231fd0fee3890L    # 3.1766697016344E-250

    xor-long/2addr v1, v5

    aput-wide v1, v4, v3

    const/16 v1, 0x1a

    :try_start_0
    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, -0x29

    aput v29, v2, v28

    const/16 v28, -0x19c7

    aput v28, v2, v27

    const/16 v27, -0x76

    aput v27, v2, v26

    const/16 v26, -0x658e

    aput v26, v2, v25

    const/16 v25, -0xb

    aput v25, v2, v24

    const/16 v24, -0x4d

    aput v24, v2, v23

    const/16 v23, -0x26

    aput v23, v2, v22

    const/16 v22, -0x7

    aput v22, v2, v21

    const/16 v21, 0x173d

    aput v21, v2, v20

    const/16 v20, 0x1d54

    aput v20, v2, v19

    const/16 v19, 0x2769

    aput v19, v2, v18

    const/16 v18, -0x1eb7

    aput v18, v2, v17

    const/16 v17, -0x7c

    aput v17, v2, v16

    const/16 v16, 0x3c02

    aput v16, v2, v15

    const/16 v15, -0x17a5

    aput v15, v2, v14

    const/16 v14, -0x77

    aput v14, v2, v13

    const/16 v13, 0x5a5a

    aput v13, v2, v12

    const/16 v12, 0x611c

    aput v12, v2, v11

    const/16 v11, 0x5006

    aput v11, v2, v10

    const/16 v10, 0x183e

    aput v10, v2, v9

    const/16 v9, -0xf8f

    aput v9, v2, v8

    const/16 v8, -0x62

    aput v8, v2, v7

    const/16 v7, 0x3b29

    aput v7, v2, v6

    const/16 v6, -0x51a6

    aput v6, v2, v5

    const/16 v5, -0x33

    aput v5, v2, v3

    const/16 v3, -0x69

    aput v3, v2, v1

    const/16 v1, 0x1a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, -0x5b

    aput v30, v1, v29

    const/16 v29, -0x19a4

    aput v29, v1, v28

    const/16 v28, -0x1a

    aput v28, v1, v27

    const/16 v27, -0x65e2

    aput v27, v1, v26

    const/16 v26, -0x66

    aput v26, v1, v25

    const/16 v25, -0x3f

    aput v25, v1, v24

    const/16 v24, -0x52

    aput v24, v1, v23

    const/16 v23, -0x69

    aput v23, v1, v22

    const/16 v22, 0x1752

    aput v22, v1, v21

    const/16 v21, 0x1d17

    aput v21, v1, v20

    const/16 v20, 0x271d

    aput v20, v1, v19

    const/16 v19, -0x1ed9

    aput v19, v1, v18

    const/16 v18, -0x1f

    aput v18, v1, v17

    const/16 v17, 0x3c6f

    aput v17, v1, v16

    const/16 v16, -0x17c4

    aput v16, v1, v15

    const/16 v15, -0x18

    aput v15, v1, v14

    const/16 v14, 0x5a28

    aput v14, v1, v13

    const/16 v13, 0x615a

    aput v13, v1, v12

    const/16 v12, 0x5061

    aput v12, v1, v11

    const/16 v11, 0x1850

    aput v11, v1, v10

    const/16 v10, -0xfe8

    aput v10, v1, v9

    const/16 v9, -0x10

    aput v9, v1, v8

    const/16 v8, 0x3b47

    aput v8, v1, v7

    const/16 v7, -0x51c5

    aput v7, v1, v6

    const/16 v6, -0x52

    aput v6, v1, v5

    const/16 v5, -0x3c

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x2d

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, 0x1a

    const/16 v32, 0x1b

    const/16 v33, 0x1c

    const/16 v34, 0x1d

    const/16 v35, 0x1e

    const/16 v36, 0x1f

    const/16 v37, 0x20

    const/16 v38, 0x21

    const/16 v39, 0x22

    const/16 v40, 0x23

    const/16 v41, 0x24

    const/16 v42, 0x25

    const/16 v43, 0x26

    const/16 v44, 0x27

    const/16 v45, 0x28

    const/16 v46, 0x29

    const/16 v47, 0x2a

    const/16 v48, 0x2b

    const/16 v49, 0x2c

    const/16 v50, 0x2105

    aput v50, v2, v49

    const/16 v49, 0x31c

    aput v49, v2, v48

    const/16 v48, -0x69dd

    aput v48, v2, v47

    const/16 v47, -0x1c

    aput v47, v2, v46

    const/16 v46, -0x7a

    aput v46, v2, v45

    const/16 v45, 0x4b23

    aput v45, v2, v44

    const/16 v44, -0x13c7

    aput v44, v2, v43

    const/16 v43, -0x77

    aput v43, v2, v42

    const/16 v42, -0x6c

    aput v42, v2, v41

    const/16 v41, -0x74

    aput v41, v2, v40

    const/16 v40, -0x55

    aput v40, v2, v39

    const/16 v39, -0x75

    aput v39, v2, v38

    const/16 v38, -0x50a8

    aput v38, v2, v37

    const/16 v37, -0x3f

    aput v37, v2, v36

    const/16 v36, -0x7f9

    aput v36, v2, v35

    const/16 v35, -0x69

    aput v35, v2, v34

    const/16 v34, 0x36a

    aput v34, v2, v33

    const/16 v33, 0x796d

    aput v33, v2, v32

    const/16 v32, 0x716

    aput v32, v2, v31

    const/16 v31, 0x2327

    aput v31, v2, v30

    const/16 v30, -0x17af

    aput v30, v2, v29

    const/16 v29, -0x73

    aput v29, v2, v28

    const/16 v28, -0xd

    aput v28, v2, v27

    const/16 v27, 0x1639

    aput v27, v2, v26

    const/16 v26, 0x7262

    aput v26, v2, v25

    const/16 v25, -0x55ff

    aput v25, v2, v24

    const/16 v24, -0x3d

    aput v24, v2, v23

    const/16 v23, -0x1b

    aput v23, v2, v22

    const/16 v22, -0x5c

    aput v22, v2, v21

    const/16 v21, -0x69

    aput v21, v2, v20

    const/16 v20, -0x68f3

    aput v20, v2, v19

    const/16 v19, -0x1f

    aput v19, v2, v18

    const/16 v18, -0x2e

    aput v18, v2, v17

    const/16 v17, 0xa53

    aput v17, v2, v16

    const/16 v16, -0x769c

    aput v16, v2, v15

    const/16 v15, -0x10

    aput v15, v2, v14

    const/16 v14, 0x1549

    aput v14, v2, v13

    const/16 v13, 0x4e70

    aput v13, v2, v12

    const/16 v12, 0x6722

    aput v12, v2, v11

    const/16 v11, -0x2ffb

    aput v11, v2, v10

    const/16 v10, -0x4f

    aput v10, v2, v9

    const/16 v9, 0x48

    aput v9, v2, v8

    const/16 v8, -0x1f9f

    aput v8, v2, v7

    const/16 v7, -0x7b

    aput v7, v2, v3

    const/4 v3, -0x2

    aput v3, v2, v1

    const/16 v1, 0x2d

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x11

    const/16 v24, 0x12

    const/16 v25, 0x13

    const/16 v26, 0x14

    const/16 v27, 0x15

    const/16 v28, 0x16

    const/16 v29, 0x17

    const/16 v30, 0x18

    const/16 v31, 0x19

    const/16 v32, 0x1a

    const/16 v33, 0x1b

    const/16 v34, 0x1c

    const/16 v35, 0x1d

    const/16 v36, 0x1e

    const/16 v37, 0x1f

    const/16 v38, 0x20

    const/16 v39, 0x21

    const/16 v40, 0x22

    const/16 v41, 0x23

    const/16 v42, 0x24

    const/16 v43, 0x25

    const/16 v44, 0x26

    const/16 v45, 0x27

    const/16 v46, 0x28

    const/16 v47, 0x29

    const/16 v48, 0x2a

    const/16 v49, 0x2b

    const/16 v50, 0x2c

    const/16 v51, 0x2125

    aput v51, v1, v50

    const/16 v50, 0x321

    aput v50, v1, v49

    const/16 v49, -0x69fd

    aput v49, v1, v48

    const/16 v48, -0x6a

    aput v48, v1, v47

    const/16 v47, -0x17

    aput v47, v1, v46

    const/16 v46, 0x4b51

    aput v46, v1, v45

    const/16 v45, -0x13b5

    aput v45, v1, v44

    const/16 v44, -0x14

    aput v44, v1, v43

    const/16 v43, -0x4c

    aput v43, v1, v42

    const/16 v42, -0x5b

    aput v42, v1, v41

    const/16 v41, -0x7d

    aput v41, v1, v40

    const/16 v40, -0x11

    aput v40, v1, v39

    const/16 v39, -0x50c3

    aput v39, v1, v38

    const/16 v38, -0x51

    aput v38, v1, v37

    const/16 v37, -0x792

    aput v37, v1, v36

    const/16 v36, -0x8

    aput v36, v1, v35

    const/16 v35, 0x320

    aput v35, v1, v34

    const/16 v34, 0x7903

    aput v34, v1, v33

    const/16 v33, 0x779

    aput v33, v1, v32

    const/16 v32, 0x2307

    aput v32, v1, v31

    const/16 v31, -0x17dd

    aput v31, v1, v30

    const/16 v30, -0x18

    aput v30, v1, v29

    const/16 v29, -0x63

    aput v29, v1, v28

    const/16 v28, 0x165c

    aput v28, v1, v27

    const/16 v27, 0x7216

    aput v27, v1, v26

    const/16 v26, -0x558e

    aput v26, v1, v25

    const/16 v25, -0x56

    aput v25, v1, v24

    const/16 v24, -0x57

    aput v24, v1, v23

    const/16 v23, -0x30

    aput v23, v1, v22

    const/16 v22, -0x7

    aput v22, v1, v21

    const/16 v21, -0x6898

    aput v21, v1, v20

    const/16 v20, -0x69

    aput v20, v1, v19

    const/16 v19, -0x69

    aput v19, v1, v18

    const/16 v18, 0xa30

    aput v18, v1, v17

    const/16 v17, -0x76f6

    aput v17, v1, v16

    const/16 v16, -0x77

    aput v16, v1, v15

    const/16 v15, 0x151a

    aput v15, v1, v14

    const/16 v14, 0x4e15

    aput v14, v1, v13

    const/16 v13, 0x674e

    aput v13, v1, v12

    const/16 v12, -0x2f99

    aput v12, v1, v11

    const/16 v11, -0x30

    aput v11, v1, v10

    const/16 v10, 0x3a

    aput v10, v1, v9

    const/16 v9, -0x2000

    aput v9, v1, v8

    const/16 v8, -0x20

    aput v8, v1, v7

    const/16 v7, -0x57

    aput v7, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v7, v1

    if-lt v3, v7, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v7, v1

    if-lt v3, v7, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-gtz v1, :cond_8

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x5b

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x6b

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_5

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    :goto_6
    return-void

    :cond_2
    :try_start_1
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_4
    aget v7, v1, v3

    aget v8, v2, v3

    xor-int/2addr v7, v8

    aput v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_5
    aget v7, v2, v3

    int-to-char v7, v7

    aput-char v7, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_6
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_7
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_8
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v4, v1, v6

    if-eqz v4, :cond_9

    const-wide v6, 0xc2231fd0fee3890L    # 3.1766697016344E-250

    xor-long/2addr v1, v6

    :cond_9
    const/16 v4, 0x20

    shl-long/2addr v1, v4

    const/16 v4, 0x20

    shr-long/2addr v1, v4

    long-to-int v1, v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncEventListener;->mSensorDevice:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_1 .. :try_end_1} :catch_5

    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_2 .. :try_end_2} :catch_5

    :try_start_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncEventListener;->mSensorDevice:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_3 .. :try_end_3} :catch_5

    move-result-object v1

    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_7
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_4 .. :try_end_4} :catch_5

    :try_start_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncEventListener;->mSensorDevice:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncDataListener;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncEventListener;->mSensorDevice:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncEventListener;->mSyncEventListener:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$SyncEventListener;

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncDataListener;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$SyncEventListener;)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->startReceivingData(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;)V

    :goto_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncEventListener;->mSyncEventListener:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$SyncEventListener;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_5 .. :try_end_5} :catch_5

    :try_start_6
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_6
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_8
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_6 .. :try_end_6} :catch_5

    :try_start_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncEventListener;->mSyncEventListener:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$SyncEventListener;

    invoke-interface {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$SyncEventListener;->onSyncDeviceJoined()V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_7 .. :try_end_7} :catch_5

    goto/16 :goto_6

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto/16 :goto_6

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto/16 :goto_6

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto/16 :goto_6

    :catch_4
    move-exception v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;->printStackTrace()V

    goto/16 :goto_6

    :catch_5
    move-exception v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto/16 :goto_6

    :catch_6
    move-exception v1

    goto :goto_7

    :catch_7
    move-exception v1

    goto :goto_7

    :catch_8
    move-exception v1

    goto/16 :goto_6
.end method

.method public onLeft(I)V
    .locals 11

    const-wide v9, 0x2125dd004ed12b5fL    # 5.343275781417412E-149

    const/4 v8, 0x0

    const/16 v7, 0x20

    const/4 v0, 0x2

    new-array v2, v0, [J

    const/4 v0, 0x1

    const-wide/16 v3, 0x1

    aput-wide v3, v2, v0

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v8}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p1

    shl-long/2addr v0, v7

    ushr-long v3, v0, v7

    aget-wide v0, v2, v8

    const-wide/16 v5, 0x0

    cmp-long v5, v0, v5

    if-eqz v5, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v3

    xor-long/2addr v0, v9

    aput-wide v0, v2, v8

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncEventListener;->mSyncEventListener:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$SyncEventListener;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncEventListener;->mSyncEventListener:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$SyncEventListener;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$SyncEventListener;->onSyncDeviceLeft()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onResponseReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;)V
    .locals 0

    return-void
.end method

.method public onStateChanged(I)V
    .locals 11

    const-wide v9, 0x1cdcbab51cf95a72L

    const/4 v8, 0x0

    const/16 v7, 0x20

    const/4 v0, 0x2

    new-array v2, v0, [J

    const/4 v0, 0x1

    const-wide/16 v3, 0x1

    aput-wide v3, v2, v0

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v8}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p1

    shl-long/2addr v0, v7

    ushr-long v3, v0, v7

    aget-wide v0, v2, v8

    const-wide/16 v5, 0x0

    cmp-long v5, v0, v5

    if-eqz v5, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v3

    xor-long/2addr v0, v9

    aput-wide v0, v2, v8

    return-void
.end method
