.class Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$11;
.super Ljava/lang/Object;
.source "RestoreActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

.field final synthetic val$dialogTag:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 864
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$11;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    iput-object p2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$11;->val$dialogTag:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 6
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 868
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$11;->val$dialogTag:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$11;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->RESTART_POPUP:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$1600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 869
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$12;->$SwitchMap$com$sec$android$app$shealth$common$commonui$dialog$DialogButtonType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 940
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 871
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$11;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Restoration has been done, SHealth will restart"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 873
    sput-boolean v3, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mIsProgressCompleted:Z

    .line 875
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$11;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestartAppDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$1700(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$11;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestartAppDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$1700(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 877
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$11;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestartAppDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$1700(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 878
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$11;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestartAppDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1, v5}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$1702(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 882
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 883
    .local v0, "extras":Landroid/os/Bundle;
    const-string v1, "key"

    const-string v2, "Is_Restore_Completed"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 884
    const-string/jumbo v1, "value"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 885
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$11;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v3, "CONFIG_OPTION_PUT"

    invoke-virtual {v1, v2, v3, v5, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 888
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$11;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->restartApp()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$1800(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)V

    goto :goto_0

    .line 893
    .end local v0    # "extras":Landroid/os/Bundle;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$11;->val$dialogTag:Ljava/lang/String;

    const-string/jumbo v2, "progress_popup"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 894
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$12;->$SwitchMap$com$sec$android$app$shealth$common$commonui$dialog$DialogButtonType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 896
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$11;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Cancel is clicked for all work"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 898
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$11;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->stopBackupOrRestore(Z)V

    .line 899
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$11;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->stopBackupOrRestore(Z)V

    .line 901
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$11;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    const v2, 0x7f0808b1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setClickable(Z)V

    .line 902
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$11;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->isOngoing:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$1202(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Z)Z

    goto/16 :goto_0

    .line 908
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$11;->val$dialogTag:Ljava/lang/String;

    const-string/jumbo v2, "restore_failed_network"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 910
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$11;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mIsNetworkErrorOccured:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$1102(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Z)Z

    .line 912
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$12;->$SwitchMap$com$sec$android$app$shealth$common$commonui$dialog$DialogButtonType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_2

    goto/16 :goto_0

    .line 914
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$11;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->START:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->showRestorePopup(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;)V

    .line 915
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$11$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$11$1;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$11;)V

    const-wide/16 v3, 0x1f4

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 932
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$11;->val$dialogTag:Ljava/lang/String;

    const-string/jumbo v2, "restore_failed_nodata"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 933
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$12;->$SwitchMap$com$sec$android$app$shealth$common$commonui$dialog$DialogButtonType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_3

    goto/16 :goto_0

    .line 869
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch

    .line 894
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch

    .line 912
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
    .end packed-switch

    .line 933
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
