.class public Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;
.super Landroid/widget/LinearLayout;
.source "CustomFoodNutrientItemView.java"


# instance fields
.field private customView:Landroid/view/View;

.field private mInvalidInputToast:Landroid/widget/Toast;

.field private mNutrientTitle:Landroid/widget/TextView;

.field private mNutrientUnit:Landroid/widget/TextView;

.field private mNutrientValue:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

.field private mOutOfRangeToast:Landroid/widget/Toast;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->init()V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 60
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->init()V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 71
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 72
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->init()V

    .line 73
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->mOutOfRangeToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->mOutOfRangeToast:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->mInvalidInputToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->mInvalidInputToast:Landroid/widget/Toast;

    return-object p1
.end method

.method private init()V
    .locals 5

    .prologue
    const/high16 v4, 0x43fa0000    # 500.0f

    const/4 v3, 0x0

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300a1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->customView:Landroid/view/View;

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->customView:Landroid/view/View;

    const v1, 0x7f080315

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->mNutrientTitle:Landroid/widget/TextView;

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->customView:Landroid/view/View;

    const v1, 0x7f080317

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->mNutrientValue:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->customView:Landroid/view/View;

    const v1, 0x7f080318

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->mNutrientUnit:Landroid/widget/TextView;

    .line 87
    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->setNutrientFilter(FF)V

    .line 88
    const v0, 0x3dcccccd    # 0.1f

    invoke-virtual {p0, v0, v3, v4}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->setRangeAndIntervalOfNutrient(FFF)V

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->mNutrientValue:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    new-instance v1, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView$1;-><init>(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->mNutrientValue:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->setId(I)V

    .line 100
    return-void
.end method


# virtual methods
.method public getNutrientTitleView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->mNutrientTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method public getNutrientUnitView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->mNutrientUnit:Landroid/widget/TextView;

    return-object v0
.end method

.method public getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->mNutrientValue:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    return-object v0
.end method

.method public isFieldEmpty()Z
    .locals 1

    .prologue
    .line 204
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->getNutrientValueView()Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public setNutrientFilter(FF)V
    .locals 10
    .param p1, "minValue"    # F
    .param p2, "maxValue"    # F

    .prologue
    const/4 v4, 0x1

    .line 135
    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->mNutrientValue:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    new-array v8, v4, [Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;

    const/4 v9, 0x0

    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView$2;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v5, p1

    move v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView$2;-><init>(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;FFIFF)V

    aput-object v0, v8, v9

    invoke-virtual {v7, v8}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 168
    return-void
.end method

.method public setRangeAndIntervalOfNutrient(FFF)V
    .locals 1
    .param p1, "interval"    # F
    .param p2, "minValue"    # F
    .param p3, "maxValue"    # F

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/CustomFoodNutrientItemView;->mNutrientValue:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->setIntervalValues(FFF)V

    .line 132
    return-void
.end method
