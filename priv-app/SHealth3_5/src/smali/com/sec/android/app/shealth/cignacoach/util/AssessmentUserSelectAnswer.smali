.class public Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;
.super Ljava/lang/Object;
.source "AssessmentUserSelectAnswer.java"


# static fields
.field private static mInstance:Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;


# instance fields
.field private mAnswerData:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;->mAnswerData:Landroid/util/SparseArray;

    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;->mInstance:Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;

    if-nez v0, :cond_0

    .line 16
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;->mInstance:Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;

    .line 18
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;->mInstance:Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;

    return-object v0
.end method


# virtual methods
.method public addAnswer(II)V
    .locals 2
    .param p1, "questionId"    # I
    .param p2, "userSelectIndex"    # I

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;->mAnswerData:Landroid/util/SparseArray;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 23
    return-void
.end method

.method public clearSelectedAnswer()V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;->mAnswerData:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;->mAnswerData:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 44
    :cond_0
    return-void
.end method

.method public getAnswer(I)I
    .locals 1
    .param p1, "questionId"    # I

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;->mAnswerData:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;->mAnswerData:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 33
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getAnswerCount()I
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;->mAnswerData:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    return v0
.end method
