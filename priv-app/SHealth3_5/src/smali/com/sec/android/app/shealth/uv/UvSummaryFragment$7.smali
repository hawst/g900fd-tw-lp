.class Lcom/sec/android/app/shealth/uv/UvSummaryFragment$7;
.super Ljava/lang/Object;
.source "UvSummaryFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V
    .locals 0

    .prologue
    .line 1006
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$7;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 1010
    sget-object v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "uvOnClickListener"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1011
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 1060
    :goto_0
    return-void

    .line 1013
    :sswitch_0
    sget-object v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "uvOnClickListener event : ib_summary_third_graph"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1015
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$7;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mStopSensor:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$702(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;Z)Z

    .line 1016
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$7;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->stopSensor()V

    .line 1017
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$7;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mMeasuringTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 1018
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$7;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->clearDataCollection()V

    .line 1019
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$7;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # invokes: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->showGraphFragment()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$800(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    goto :goto_0

    .line 1022
    :sswitch_1
    sget-object v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "uvOnClickListener event : ll_summary_third_previous"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1024
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$7;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$7;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$900(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1027
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$7;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iput-boolean v3, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->ismFinishedState:Z

    .line 1028
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$7;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    sget-object v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->effectAudio:[I

    aget v1, v1, v3

    # invokes: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->playSound(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$1000(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;I)V

    .line 1029
    sget-object v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "uvOnClickListener event : bt_summary_second_retry"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1031
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$7;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->checkforLocation(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1032
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$7;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->createLocationOnPopup(Landroid/content/Context;)V

    goto :goto_0

    .line 1034
    :cond_0
    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1035
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$7;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # invokes: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->startMeasuringUv()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$1100(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    goto :goto_0

    .line 1040
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$7;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iput-boolean v3, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->ismFinishedState:Z

    .line 1041
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$7;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    sget-object v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->effectAudio:[I

    const/4 v2, 0x2

    aget v1, v1, v2

    # invokes: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->playSound(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$1000(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;I)V

    .line 1042
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$7;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$900(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$7;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$900(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090df5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1047
    sget-object v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "uvOnClickListener event : bt_summary_second_retry"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1049
    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1050
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$7;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # invokes: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->initToReadyState()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$1200(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    goto/16 :goto_0

    .line 1054
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$7;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # invokes: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->showSpfPickerDialog()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$1300(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    goto/16 :goto_0

    .line 1011
    :sswitch_data_0
    .sparse-switch
        0x7f0805e5 -> :sswitch_1
        0x7f0805ef -> :sswitch_0
        0x7f080a03 -> :sswitch_3
        0x7f080aff -> :sswitch_2
        0x7f080b1a -> :sswitch_4
    .end sparse-switch
.end method
