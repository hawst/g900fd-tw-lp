.class Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$12;
.super Ljava/lang/Object;
.source "ExerciseProHRMFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->playSound(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

.field final synthetic val$effectAudio:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;I)V
    .locals 0

    .prologue
    .line 870
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    iput p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$12;->val$effectAudio:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v4, 0x1

    const/high16 v2, 0x42c80000    # 100.0f

    .line 874
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "sth  playSound  runOnUiThread"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 875
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$12;->val$effectAudio:I

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->effectAudio:[I
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$3100()[I

    move-result-object v1

    aget v1, v1, v4

    if-ne v0, v1, :cond_0

    .line 876
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->pool:Landroid/media/SoundPool;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$3200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Landroid/media/SoundPool;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$12;->val$effectAudio:I

    const/4 v5, -0x1

    move v3, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    .line 880
    :goto_0
    return-void

    .line 878
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->pool:Landroid/media/SoundPool;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$3200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Landroid/media/SoundPool;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$12;->val$effectAudio:I

    const/4 v5, 0x0

    move v3, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    goto :goto_0
.end method
