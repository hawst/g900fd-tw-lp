.class public Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "SetGoalActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;
.implements Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;


# static fields
.field private static final DEFAULT_PERIOD:I = 0x0

.field private static final sDiscardChangesDialog:Ljava/lang/String; = "sDiscardChangesDialog"

.field private static final sMealGoalDataKey:Ljava/lang/String; = "MEAL_GOAL_DATA_KEY"

.field private static final sOutOfRangeDialog:Ljava/lang/String; = "sOutOfRangeDialog"

.field private static final sUnifiedConsumedCalorieGoalDataKey:Ljava/lang/String; = "UNIFIED_CONSUMED_CALOIRE_GOAL_DATA_KEY"


# instance fields
.field private mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

.field mDialogBackPressListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;

.field mEditTouchListener:Landroid/view/View$OnTouchListener;

.field mFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

.field private mGoalDao:Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;

.field private mIntakeInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

.field private mIsInitializedState:Z

.field private mMealGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

.field private mParentLayout1:Landroid/widget/LinearLayout;

.field private mParentLayout2:Landroid/widget/LinearLayout;

.field private mUnifiedComsumedGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

.field private mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 473
    new-instance v0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$9;-><init>(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mDialogBackPressListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;

    .line 484
    new-instance v0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$10;-><init>(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mEditTouchListener:Landroid/view/View$OnTouchListener;

    .line 535
    new-instance v0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$11;-><init>(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->showPopupOrClose()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->saveData()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mParentLayout2:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mParentLayout1:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->setMinValToInputField()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;)Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mIntakeInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;)Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    return-object v0
.end method

.method private initGoalData(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, -0x4

    const v7, 0x9c4b

    const v4, 0x9c43

    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 181
    if-eqz p1, :cond_4

    const-string v2, "MEAL_GOAL_DATA_KEY"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 183
    const-string v2, "MEAL_GOAL_DATA_KEY"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mMealGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    .line 195
    :goto_0
    if-eqz p1, :cond_5

    const-string v2, "UNIFIED_CONSUMED_CALOIRE_GOAL_DATA_KEY"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 197
    const-string v2, "UNIFIED_CONSUMED_CALOIRE_GOAL_DATA_KEY"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mUnifiedComsumedGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    .line 204
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mMealGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mUnifiedComsumedGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    if-eqz v2, :cond_6

    .line 206
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mMealGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v0

    .line 207
    .local v0, "before_MealGoalDataValue":F
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mUnifiedComsumedGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v1

    .line 209
    .local v1, "before_UnifiedComsumedGoalDataValue":F
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mMealGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getGoalSubType()I

    move-result v2

    if-eq v2, v8, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mMealGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getGoalSubType()I

    move-result v2

    if-eqz v2, :cond_0

    .line 211
    new-instance v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/NetCalorieUtils;->getRecommendedCalorieIntake()I

    move-result v3

    int-to-float v3, v3

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;-><init>(FIII)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mMealGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    .line 214
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mUnifiedComsumedGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getGoalSubType()I

    move-result v2

    if-eq v2, v8, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mUnifiedComsumedGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getGoalSubType()I

    move-result v2

    if-eqz v2, :cond_1

    .line 216
    new-instance v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/NetCalorieUtils;->getRecommendedCalorieBurn()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Math;->floor(D)D

    move-result-wide v3

    double-to-float v3, v3

    invoke-direct {v2, v3, v7, v5, v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;-><init>(FIII)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mUnifiedComsumedGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    .line 219
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mMealGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v2

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->isEqual(FF)Z

    move-result v2

    if-nez v2, :cond_2

    .line 221
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mMealGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->setSetTime(J)V

    .line 222
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mMealGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->insertGoal(Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;)V

    .line 225
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mUnifiedComsumedGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v2

    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->isEqual(FF)Z

    move-result v2

    if-nez v2, :cond_3

    .line 227
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mUnifiedComsumedGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->setSetTime(J)V

    .line 228
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mUnifiedComsumedGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->insertGoal(Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;)V

    .line 242
    .end local v0    # "before_MealGoalDataValue":F
    .end local v1    # "before_UnifiedComsumedGoalDataValue":F
    :cond_3
    :goto_2
    return-void

    .line 187
    :cond_4
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/GoalUtils;->getLatestGoalData(Landroid/content/Context;Ljava/lang/Integer;)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mMealGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    goto/16 :goto_0

    .line 201
    :cond_5
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/GoalUtils;->getLatestGoalData(Landroid/content/Context;Ljava/lang/Integer;)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mUnifiedComsumedGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    goto/16 :goto_1

    .line 233
    :cond_6
    new-instance v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/NetCalorieUtils;->getRecommendedCalorieIntake()I

    move-result v3

    int-to-float v3, v3

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;-><init>(FIII)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mMealGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    .line 234
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mMealGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->setSetTime(J)V

    .line 235
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mMealGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->insertGoal(Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;)V

    .line 237
    new-instance v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/NetCalorieUtils;->getRecommendedCalorieBurn()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Math;->floor(D)D

    move-result-wide v3

    double-to-float v3, v3

    invoke-direct {v2, v3, v7, v5, v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;-><init>(FIII)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mUnifiedComsumedGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    .line 238
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mUnifiedComsumedGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->setSetTime(J)V

    .line 239
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mUnifiedComsumedGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->insertGoal(Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;)V

    goto :goto_2
.end method

.method private initViews()V
    .locals 11

    .prologue
    const v10, 0x7f08049c

    const v9, 0x7f080473

    const/high16 v8, 0x41200000    # 10.0f

    const v7, 0x47c34f80    # 99999.0f

    const/high16 v6, 0x3f800000    # 1.0f

    .line 122
    const v4, 0x7f08049a

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/food/goal/SetGoalInputModule;

    iput-object v4, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mIntakeInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    .line 123
    const v4, 0x7f08049b

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/food/goal/SetGoalInputModule;

    iput-object v4, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    .line 124
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mIntakeInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mMealGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->setValue(F)V

    .line 128
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v4, v8, v7}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->setInputRange(FF)V

    .line 129
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v4

    invoke-virtual {v4, v6, v8, v7}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setIntervalValues(FFF)V

    .line 130
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mUnifiedComsumedGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->setValue(F)V

    .line 132
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mIntakeInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v4

    invoke-virtual {v4, v6, v6, v7}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setIntervalValues(FFF)V

    .line 134
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mIntakeInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v4, v9}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 135
    .local v1, "calUnitIntake":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v4, v9}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 136
    .local v0, "calUnitBurn":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mIntakeInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v4, p0}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->setTextEmptyListener(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;)V

    .line 137
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v4, p0}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->setTextEmptyListener(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;)V

    .line 139
    const v4, 0x7f080495

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 140
    .local v2, "recommendedKCal":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090950

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getDefaultIntakeCalorieGoal()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 142
    .local v3, "recommendedKCalMessage":Ljava/lang/String;
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mIntakeInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v4, v10}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mParentLayout1:Landroid/widget/LinearLayout;

    .line 144
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mParentLayout1:Landroid/widget/LinearLayout;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f090953

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mIntakeInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getValue()Ljava/lang/Float;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Float;->intValue()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 145
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v4, v10}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mParentLayout2:Landroid/widget/LinearLayout;

    .line 146
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mParentLayout2:Landroid/widget/LinearLayout;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f090954

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getValue()Ljava/lang/Float;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Float;->intValue()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 147
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mIntakeInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mEditTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 148
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mEditTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 149
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    new-instance v5, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$3;

    invoke-direct {v5, p0, v0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$3;-><init>(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;Landroid/widget/TextView;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->setOnFloatInputChangedListener(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnFloatInputChangedListener;)V

    .line 162
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mIntakeInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    new-instance v5, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$4;

    invoke-direct {v5, p0, v1}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$4;-><init>(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;Landroid/widget/TextView;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->setOnFloatInputChangedListener(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnFloatInputChangedListener;)V

    .line 173
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 174
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mIntakeInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 177
    return-void
.end method

.method private insertGoal(Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;)V
    .locals 1
    .param p1, "data"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    .prologue
    .line 245
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mGoalDao:Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;

    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;->insertData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J

    .line 246
    return-void
.end method

.method private saveData()V
    .locals 3

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->validateChangerParameters()Z

    move-result v1

    if-nez v1, :cond_0

    .line 258
    :goto_0
    return-void

    .line 253
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 254
    .local v0, "result":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->setOutputParameters(Landroid/content/Intent;)V

    .line 255
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->setResult(ILandroid/content/Intent;)V

    .line 256
    const v1, 0x7f090835

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 257
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->closeScreen()V

    goto :goto_0
.end method

.method private setMinValToInputField()V
    .locals 4

    .prologue
    const-wide/16 v2, 0xc8

    .line 435
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mIntakeInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->isOutOfRange()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 436
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mIntakeInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setText(Ljava/lang/CharSequence;)V

    .line 437
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mIntakeInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->selectAll()V

    .line 438
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mIntakeInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->requestFocus()Z

    .line 440
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$7;-><init>(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 453
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->isOutOfRange()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 454
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v0

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setText(Ljava/lang/CharSequence;)V

    .line 455
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->selectAll()V

    .line 456
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->requestFocus()Z

    .line 458
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$8;-><init>(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 471
    :cond_1
    return-void
.end method

.method private showConfirmationPopup()V
    .locals 3

    .prologue
    .line 269
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f090081

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090083

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "sDiscardChangesDialog"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 274
    return-void
.end method

.method private showPopupOrClose()V
    .locals 1

    .prologue
    .line 261
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->isDataChanged()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 262
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->showConfirmationPopup()V

    .line 266
    :goto_0
    return-void

    .line 264
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->finish()V

    goto :goto_0
.end method

.method private stopInputModuleAnimations()V
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->stopInertia()V

    .line 369
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mIntakeInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->stopInertia()V

    .line 370
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 98
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 99
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setHomeButtonVisible(Z)V

    .line 100
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    new-array v1, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v3, 0x7f090048

    new-instance v4, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$1;-><init>(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;)V

    invoke-direct {v2, v5, v3, v4, v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;Z)V

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 109
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    new-array v1, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v3, 0x7f090044

    new-instance v4, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$2;-><init>(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;)V

    invoke-direct {v2, v5, v3, v4, v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;Z)V

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 118
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 119
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 354
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 355
    .local v0, "current":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 357
    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 358
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 360
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    .line 363
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    return v1
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 401
    const-string/jumbo v0, "sDiscardChangesDialog"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 403
    new-instance v0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$5;-><init>(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;)V

    .line 429
    :goto_0
    return-object v0

    .line 417
    :cond_0
    const-string/jumbo v0, "sOutOfRangeDialog"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 418
    new-instance v0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$6;-><init>(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;)V

    goto :goto_0

    .line 429
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isDataChanged()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const v5, 0x3dcccccd    # 0.1f

    .line 294
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getValue()Ljava/lang/Float;

    move-result-object v0

    .line 295
    .local v0, "burnGoalInputValue":Ljava/lang/Float;
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mIntakeInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getValue()Ljava/lang/Float;

    move-result-object v1

    .line 296
    .local v1, "intakeGoalInputValue":Ljava/lang/Float;
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 299
    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mUnifiedComsumedGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v3

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->isEqual(FFF)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mMealGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v3

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->isEqual(FFF)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected onCancelClick()V
    .locals 2

    .prologue
    .line 328
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->DONE_SET_GOAL_BY_ACTION_BAR_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 329
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->stopInputModuleAnimations()V

    .line 330
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 81
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 83
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->setRequestedOrientation(I)V

    .line 85
    const v0, 0x7f030115

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->setContentView(I)V

    .line 86
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;->getUserActionLogger()Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    .line 87
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mGoalDao:Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;

    .line 88
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->initGoalData(Landroid/os/Bundle;)V

    .line 89
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->initViews()V

    .line 94
    return-void
.end method

.method protected onDoneClick()V
    .locals 2

    .prologue
    .line 323
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->CANCEL_SET_GOAL_BY_ACTION_BAR_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 324
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 334
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->stopInputModuleAnimations()V

    .line 335
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPause()V

    .line 336
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 341
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 343
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 344
    .local v0, "currentView":Landroid/view/View;
    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->showSoftInputKeypad(Landroid/content/Context;Landroid/view/View;)V

    .line 345
    return-void
.end method

.method public processEmptyField()V
    .locals 9

    .prologue
    const v8, 0x7f090b60

    const v7, 0x1869f

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 377
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f0909bc

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0909bd

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x2

    const/16 v5, 0xa

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 379
    .local v0, "outOfRangeText":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mIntakeInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->isOutOfRange()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 381
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mIntakeInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setText(Ljava/lang/CharSequence;)V

    .line 382
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mIntakeInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2, v6}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 383
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setType(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mDialogBackPressListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string/jumbo v3, "sOutOfRangeDialog"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 395
    :cond_0
    :goto_0
    return-void

    .line 386
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->isOutOfRange()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 388
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setText(Ljava/lang/CharSequence;)V

    .line 389
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2, v6}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 390
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setType(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mDialogBackPressListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string/jumbo v3, "sOutOfRangeDialog"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected setOutputParameters(Landroid/content/Intent;)V
    .locals 4
    .param p1, "result"    # Landroid/content/Intent;

    .prologue
    const/4 v3, -0x4

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mUnifiedComsumedGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getValue()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->setValue(F)V

    .line 282
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mUnifiedComsumedGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->setGoalSubType(I)V

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mUnifiedComsumedGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->setSetTime(J)V

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mUnifiedComsumedGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->insertGoal(Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;)V

    .line 286
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mMealGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mIntakeInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getValue()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->setValue(F)V

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mMealGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->setGoalSubType(I)V

    .line 288
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mMealGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->setSetTime(J)V

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mMealGoalData:Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->insertGoal(Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;)V

    .line 290
    return-void
.end method

.method protected validateChangerParameters()Z
    .locals 8

    .prologue
    const v7, 0x1869f

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 306
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mIntakeInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->isOutOfRange()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->isOutOfRange()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 307
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f0909bc

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f0909bd

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    const/4 v1, 0x2

    const/16 v6, 0xa

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    const/4 v1, 0x3

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 312
    .local v0, "outOfRangeText":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setType(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const v3, 0x7f090b60

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mDialogBackPressListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string/jumbo v4, "sOutOfRangeDialog"

    invoke-virtual {v1, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    move v1, v2

    .line 318
    .end local v0    # "outOfRangeText":Ljava/lang/String;
    :cond_1
    return v1
.end method
