.class public Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachMoreUtils;
.super Ljava/lang/Object;
.source "CignaCoachMoreUtils.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static sInstance:Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachMoreUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachMoreUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachMoreUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachMoreUtils;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachMoreUtils;->sInstance:Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachMoreUtils;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachMoreUtils;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachMoreUtils;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachMoreUtils;->sInstance:Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachMoreUtils;

    .line 32
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachMoreUtils;->sInstance:Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachMoreUtils;

    return-object v0
.end method

.method private showMoreForContextMenu(Landroid/content/Context;Landroid/support/v4/app/Fragment;Landroid/view/MenuItem;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fragment"    # Landroid/support/v4/app/Fragment;
    .param p3, "items"    # Landroid/view/MenuItem;

    .prologue
    .line 44
    invoke-interface {p3}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f080c8d

    if-ne v1, v2, :cond_1

    .line 46
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachMoreUtils;->startDeleteGoalMissionActivity(Landroid/content/Context;)V

    .line 82
    :cond_0
    :goto_0
    return-void

    .line 48
    :cond_1
    invoke-interface {p3}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f080c8e

    if-ne v1, v2, :cond_2

    .line 50
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachMoreUtils;->startViewHistoryActivity(Landroid/content/Context;)V

    goto :goto_0

    .line 52
    :cond_2
    invoke-interface {p3}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f080c8f

    if-ne v1, v2, :cond_3

    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachMoreUtils;->startBadgeListActivity(Landroid/content/Context;)V

    goto :goto_0

    .line 56
    :cond_3
    invoke-interface {p3}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f080c95

    if-ne v1, v2, :cond_4

    .line 58
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachMoreUtils;->startLifestyleReassessment(Landroid/content/Context;)V

    goto :goto_0

    .line 60
    :cond_4
    invoke-interface {p3}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f080c92

    if-ne v1, v2, :cond_5

    .line 62
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachMoreUtils;->startLearnMoreActivity(Landroid/content/Context;)V

    goto :goto_0

    .line 64
    :cond_5
    invoke-interface {p3}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f080c93

    if-ne v1, v2, :cond_6

    .line 66
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachMoreUtils;->startAboutCignaActivity(Landroid/content/Context;)V

    goto :goto_0

    .line 68
    :cond_6
    invoke-interface {p3}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f080c94

    if-ne v1, v2, :cond_7

    .line 70
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachMoreUtils;->startSettingCignaActivity(Landroid/content/Context;)V

    goto :goto_0

    .line 72
    :cond_7
    invoke-interface {p3}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f080c91

    if-ne v1, v2, :cond_8

    .line 74
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachMoreUtils;->startLibraryCignaActivity(Landroid/content/Context;)V

    goto :goto_0

    .line 76
    :cond_8
    invoke-interface {p3}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f080c8a

    if-ne v1, v2, :cond_0

    .line 77
    const-string v1, "com.sec.android.app.shealth.cignacoach"

    const-string v2, "0026"

    invoke-static {p1, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.shealth.help.action.COACH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 80
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private startAboutCignaActivity(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 114
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/cignacoach/AboutCignaActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 115
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 116
    return-void
.end method

.method private startBadgeListActivity(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 103
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 104
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 105
    return-void
.end method

.method private startDeleteGoalMissionActivity(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 85
    const/4 v0, 0x0

    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    move-object v2, p1

    .line 86
    check-cast v2, Landroid/app/Activity;

    instance-of v2, v2, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    if-eqz v2, :cond_0

    .line 87
    check-cast p1, Landroid/app/Activity;

    .end local p1    # "context":Landroid/content/Context;
    check-cast p1, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getCurrentGoalFragment()Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    move-result-object v0

    .line 90
    :cond_0
    if-eqz v0, :cond_1

    .line 91
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 92
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "EXTRA_NAME_DELETE_TYPE"

    sget-object v3, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;->DELETE:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 93
    const/16 v2, 0x2bc

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 95
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_1
    return-void
.end method

.method private startLearnMoreActivity(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 119
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/cignacoach/HelpCignaActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 120
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 121
    return-void
.end method

.method private startLibraryCignaActivity(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 131
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 132
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.shealth.cignacoach.CIGNA_COACH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 133
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 135
    const-string v1, "com.sec.android.app.shealth.cignacoach"

    const-string v2, "0025"

    invoke-static {p1, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    return-void
.end method

.method private startLifestyleReassessment(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 108
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 109
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.shealth.cignacoach.CIGNA_COACH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 110
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 111
    return-void
.end method

.method private startManualActivity(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 139
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/settings/ManualActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 140
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "user_manual_url"

    const-string v2, "http://www.samsung.com/m-manual/common/S_HEALTH_K_COACH"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 142
    return-void
.end method

.method private startSettingCignaActivity(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 124
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 125
    .local v0, "intentSetting":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.shealth"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 126
    const-string v1, "android.shealth.action.LAUNCH_SETTINGS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 127
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 128
    return-void
.end method

.method private startViewHistoryActivity(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 98
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 99
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 100
    return-void
.end method


# virtual methods
.method public showMoreForContextMenu(Landroid/content/Context;Landroid/view/MenuItem;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "items"    # Landroid/view/MenuItem;

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachMoreUtils;->showMoreForContextMenu(Landroid/content/Context;Landroid/support/v4/app/Fragment;Landroid/view/MenuItem;)V

    .line 40
    return-void
.end method
