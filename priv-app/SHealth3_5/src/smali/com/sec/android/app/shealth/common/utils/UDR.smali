.class public Lcom/sec/android/app/shealth/common/utils/UDR;
.super Ljava/lang/Object;
.source "UDR.java"


# static fields
.field public static final ACTION_ACCOUNTSYNC:Ljava/lang/String; = "com.sec.android.app.shealth.accountsync.action"

.field public static final ACTION_BLOODGLUCOSE:Ljava/lang/String; = "com.sec.shealth.action.BLOOD_GLUCOSE"

.field public static final ACTION_BLOODPRESSURE:Ljava/lang/String; = "com.sec.shealth.action.bloodpressure.BLOODPRESSURE_MAIN_ACTIVITY"

.field public static final ACTION_BODYTEMP:Ljava/lang/String; = "com.sec.shealth.action.BODYTEMP"

.field public static final ACTION_BURNT_CALORIES:Ljava/lang/String; = "com.sec.shealth.action.burnt_calories"

.field public static final ACTION_CAMERA_CROP:Ljava/lang/String; = "com.android.camera.action.CROP"

.field public static final ACTION_COACH:Ljava/lang/String; = "com.sec.shealth.action.COACH"

.field public static final ACTION_DELETE_DOWNLOAD_APPS:Ljava/lang/String; = "com.sec.shealth.action.DELETE_DOWNLOAD_APPS"

.field public static final ACTION_ECG:Ljava/lang/String; = "com.sec.shealth.action.ECG"

.field public static final ACTION_EXERCISE:Ljava/lang/String; = "com.sec.shealth.action.EXERCISE"

.field public static final ACTION_FINISHED_PROFILE_SETUP:Ljava/lang/String; = "com.sec.shealth.action.FINISHED_PROFILE_SETUP"

.field public static final ACTION_FOOD:Ljava/lang/String; = "com.sec.shealth.action.FOOD"

.field public static final ACTION_HEART_RATE:Ljava/lang/String; = "com.sec.shealth.action.HEART_RATE"

.field public static final ACTION_HELP:Ljava/lang/String; = "com.sec.shealth.action.HELP"

.field public static final ACTION_HELP_COACH:Ljava/lang/String; = "com.sec.shealth.help.action.COACH"

.field public static final ACTION_HELP_FAVORITES:Ljava/lang/String; = "com.sec.shealth.help.action.FAVORITES"

.field public static final ACTION_HELP_FOOD:Ljava/lang/String; = "com.sec.shealth.help.action.FOOD"

.field public static final ACTION_HELP_HEARTRATE:Ljava/lang/String; = "com.sec.shealth.help.action.HEART_RATE"

.field public static final ACTION_HELP_MORE_APPS:Ljava/lang/String; = "com.sec.shealth.help.action.MORE_APPS"

.field public static final ACTION_HELP_PEDOMETER:Ljava/lang/String; = "com.sec.shealth.help.action.PEDOMETER"

.field public static final ACTION_HELP_SLEEP:Ljava/lang/String; = "com.sec.shealth.help.action.SLEEP"

.field public static final ACTION_HELP_SPO2:Ljava/lang/String; = "com.sec.shealth.help.action.SPO2"

.field public static final ACTION_HELP_STRESS:Ljava/lang/String; = "com.sec.shealth.help.action.STRESS"

.field public static final ACTION_HELP_TGH:Ljava/lang/String; = "com.sec.shealth.help.action.TGH"

.field public static final ACTION_HELP_UV:Ljava/lang/String; = "com.sec.shealth.help.action.UV"

.field public static final ACTION_HELP_WEIGHT:Ljava/lang/String; = "com.sec.shealth.help.action.WEIGHT"

.field public static final ACTION_HELP_WORKOUT:Ljava/lang/String; = "com.sec.shealth.help.action.WORK_OUT"

.field public static final ACTION_LAUCH_HOMEACTIVITY:Ljava/lang/String; = "com.sec.shealth.HomeActivity"

.field public static final ACTION_LAUNCHACTIVITY:Ljava/lang/String; = "com.sec.shealth.LaunchActivity"

.field public static final ACTION_LAUNCH_SETTING:Ljava/lang/String; = "android.shealth.action.LAUNCH_SETTINGS"

.field public static final ACTION_MOREAPPS:Ljava/lang/String; = "android.shealth.action.MOREAPPS"

.field public static final ACTION_MOREAPPS_MOREAPPMAIN:Ljava/lang/String; = "com.sec.android.app.shealth.moreapps.MoreAppsMainActivity"

.field public static final ACTION_PACKAGE_REMOVED:Ljava/lang/String; = "android.intent.action.PACKAGE_REMOVED"

.field public static final ACTION_PEDOMETER:Ljava/lang/String; = "com.sec.shealth.action.PEDOMETER"

.field public static final ACTION_PINCODE_ACTIVITY:Ljava/lang/String; = "com.sec.shealth.PINCODE_ACTIVITY"

.field public static final ACTION_RESTORATION_COMPLETED:Ljava/lang/String; = "action_restoration_completed"

.field public static final ACTION_SLEEP:Ljava/lang/String; = "com.sec.shealth.action.SLEEP"

.field public static final ACTION_SPO2:Ljava/lang/String; = "com.sec.shealth.action.SPO2"

.field public static final ACTION_START_PEDOMETER:Ljava/lang/String; = "com.sec.android.app.shealth.START_PEDOMETER"

.field public static final ACTION_STEALTH_MODE:Ljava/lang/String; = "com.sec.shealth.action.STEALTH_MODE"

.field public static final ACTION_STEALTH_TRY_IT:Ljava/lang/String; = "com.sec.shealth.action.STEALTH_TRY_IT"

.field public static final ACTION_STRESS:Ljava/lang/String; = "com.sec.shealth.action.STRESS"

.field public static final ACTION_THERMOHYGROMETER:Ljava/lang/String; = "com.sec.shealth.action.thermohygrometer.THERMOHYGROMETER_MAIN_ACTIVITY"

.field public static final ACTION_UPDATE_PROFILE:Ljava/lang/String; = "com.sec.shealth.UPDATE_PROFILE"

.field public static final ACTION_USERNAME:Ljava/lang/String; = "com.sec.shealth.action.Username"

.field public static final ACTION_UV:Ljava/lang/String; = "com.sec.shealth.action.UV"

.field public static final ACTION_WEIGHT:Ljava/lang/String; = "com.sec.shealth.action.WEIGHT"

.field public static final ACTIVITY_NAME:Ljava/lang/String; = "activity_name"

.field public static final AUTO_BACKUP_ACTIVITY:Ljava/lang/String; = "AutoBackupActivity"

.field public static final CALORIES_GOAL_ACTIVITY:Ljava/lang/String; = "CalorieGoalActivity"

.field public static final CSC_PATH:Ljava/lang/String; = "/system/csc/sales_code.dat"

.field public static final EXTRA_BURNT_CALORIES:Ljava/lang/String; = "burnt_calories"

.field public static final EXTRA_SHOW_GRAPH_FRAGMENT:Ljava/lang/String; = "show_graph_fragment"

.field public static final GET_IMAGE:I = 0x2

.field public static final HEALTHSERVICE_AWARDBADGEMONITOR:Ljava/lang/String; = "com.sec.android.service.health.award.test.AwardBadgeMonitor"

.field public static final HELPHUB_HELP:Ljava/lang/String; = "com.sec.android.app.shealth.help.HelpActivity"

.field public static final HELPPLUGGIN:Ljava/lang/String; = "com.samsung.helpplugin"

.field public static final INITIALIZATION_STATUSRECEIVER_INTENT:Ljava/lang/String; = "com.sec.android.service.health.cp.PLATFORM_INITIALIZATION_STATUSRECEIVER_INTENT"

.field public static final INITILIZATION_GETSTATUS_INTENT:Ljava/lang/String; = "com.sec.android.service.health.cp.PLATFORM_INITILIZATION_GETSTATUS_INTENT"

.field public static final INSERT_MWC_DATA:Z = false

.field public static final K_COACH_MANUAL_URL:Ljava/lang/String; = "http://www.samsung.com/m-manual/common/S_HEALTH_K_COACH"

.field public static final K_EXERCIS_MANUAL_URL:Ljava/lang/String; = "http://www.samsung.com/m-manual/common/S_HEALTH_K_EXERCIS"

.field public static final K_FOOD_MANUAL_URL:Ljava/lang/String; = "http://www.samsung.com/m-manual/common/S_HEALTH_K_FOOD"

.field public static final K_HEARTRA_MANUAL_URL:Ljava/lang/String; = "http://www.samsung.com/m-manual/common/S_HEALTH_K_HEARTRA"

.field public static final K_PEDOMET_MANUAL_URL:Ljava/lang/String; = "http://www.samsung.com/m-manual/common/S_HEALTH_K_PEDOMET"

.field public static final K_SLEEP_MANUAL_URL:Ljava/lang/String; = "http://www.samsung.com/m-manual/common/S_HEALTH_K_SLEEP"

.field public static final K_STRESS_MANUAL_URL:Ljava/lang/String; = "http://www.samsung.com/m-manual/common/S_HEALTH_K_STRESS"

.field public static final K_UV_MANUAL_URL:Ljava/lang/String; = "http://www.samsung.com/m-manual/common/S_HEALTH_K_UV"

.field public static final K_WEIGHT_MANUAL_URL:Ljava/lang/String; = "http://www.samsung.com/m-manual/common/S_HEALTH_K_WEIGHT"

.field public static final LANDSCAPE_PHOTO:Ljava/lang/String; = "landscape_photo_image"

.field public static final LANDSCAPE_PHOTO_PATH:Ljava/lang/String;

.field public static final LOCALE_ARABIC:Ljava/lang/String; = "ar"

.field public static final LOCALE_BRAZILIAN_PORTUGUESE:Ljava/lang/String; = "pt_BR"

.field public static final LOCALE_BULGARIAN:Ljava/lang/String; = "bg"

.field public static final LOCALE_CANADIAN_FRENCH:Ljava/lang/String; = "fr_CA"

.field public static final LOCALE_CHINESE:Ljava/lang/String; = "zh"

.field public static final LOCALE_CROATIAN:Ljava/lang/String; = "hr"

.field public static final LOCALE_CZECH:Ljava/lang/String; = "cs"

.field public static final LOCALE_DANISH:Ljava/lang/String; = "da"

.field public static final LOCALE_DUTCH:Ljava/lang/String; = "nl"

.field public static final LOCALE_DUTCH_BELGIUM:Ljava/lang/String; = "nl_BE"

.field public static final LOCALE_ENGLISH_AUSTRAILIA:Ljava/lang/String; = "en_AU"

.field public static final LOCALE_ENGLISH_CANADA:Ljava/lang/String; = "en_CA"

.field public static final LOCALE_ENGLISH_IRELAND:Ljava/lang/String; = "en_IE"

.field public static final LOCALE_ESTONIAN:Ljava/lang/String; = "et"

.field public static final LOCALE_FARSI:Ljava/lang/String; = "fa"

.field public static final LOCALE_FINNISH:Ljava/lang/String; = "fi"

.field public static final LOCALE_FRENCH:Ljava/lang/String; = "fr"

.field public static final LOCALE_FRENCH_BELGIUM:Ljava/lang/String; = "fr_BE"

.field public static final LOCALE_FRENCH_SWITZERLAND:Ljava/lang/String; = "fr_CH"

.field public static final LOCALE_GERMAN:Ljava/lang/String; = "de"

.field public static final LOCALE_GERMAN_SWITZERLAND:Ljava/lang/String; = "de_CH"

.field public static final LOCALE_GREEK:Ljava/lang/String; = "el_GR"

.field public static final LOCALE_HEBREW:Ljava/lang/String; = "iw"

.field public static final LOCALE_HUNGARIAN:Ljava/lang/String; = "hu"

.field public static final LOCALE_ICELANDIC:Ljava/lang/String; = "is"

.field public static final LOCALE_ITALIAN:Ljava/lang/String; = "it"

.field public static final LOCALE_ITALIAN_SWITZERLAND:Ljava/lang/String; = "it_CH"

.field public static final LOCALE_JAPANESE:Ljava/lang/String; = "ja"

.field public static final LOCALE_KAZAKHSTAN:Ljava/lang/String; = "kk"

.field public static final LOCALE_KOREAN:Ljava/lang/String; = "ko"

.field public static final LOCALE_LATVIAN:Ljava/lang/String; = "lv"

.field public static final LOCALE_LITHUANIAN:Ljava/lang/String; = "lt"

.field public static final LOCALE_MACEDONIAN_FYROM:Ljava/lang/String; = "mk"

.field public static final LOCALE_NORWEGIAN:Ljava/lang/String; = "no"

.field public static final LOCALE_POLISH:Ljava/lang/String; = "pl"

.field public static final LOCALE_PORTUGUESE:Ljava/lang/String; = "pt"

.field public static final LOCALE_ROMANIAN:Ljava/lang/String; = "ro"

.field public static final LOCALE_RUSSIAN:Ljava/lang/String; = "ru"

.field public static final LOCALE_RUSSIAN_ISRAEL:Ljava/lang/String; = "ru_IL"

.field public static final LOCALE_SERBIAN:Ljava/lang/String; = "sr"

.field public static final LOCALE_SLOVAK:Ljava/lang/String; = "sk"

.field public static final LOCALE_SLOVENIAN:Ljava/lang/String; = "sl"

.field public static final LOCALE_SPANISH:Ljava/lang/String; = "es"

.field public static final LOCALE_SPANISH_MEXICO:Ljava/lang/String; = "es_MX"

.field public static final LOCALE_SWEDISH:Ljava/lang/String; = "sv"

.field public static final LOCALE_S_CHINESE:Ljava/lang/String; = "zh_CN"

.field public static final LOCALE_TURKISH:Ljava/lang/String; = "tr"

.field public static final LOCALE_T_CHINESE_HK:Ljava/lang/String; = "zh_HK"

.field public static final LOCALE_T_CHINESE_TW:Ljava/lang/String; = "zh_TW"

.field public static final LOCALE_UK_ENGLISH:Ljava/lang/String; = "en"

.field public static final LOCALE_US_ENGLISH:Ljava/lang/String; = "en_US"

.field public static final LOCALE_VIETNAMESE:Ljava/lang/String; = "vi"

.field public static final NORMAL_PHOTO:Ljava/lang/String; = "photo_image"

.field public static final NORMAL_PHOTO_PATH:Ljava/lang/String;

.field public static final PACKAGE_SAMSUNGAPPS:Ljava/lang/String; = "com.sec.android.app.samsungapps"

.field public static final PACKAGE_SHEALTH:Ljava/lang/String; = "com.sec.android.app.shealth"

.field public static final PACKAGE_SLEEP:Ljava/lang/String; = "com.sec.android.app.shealth.plugins.sleepmonitor"

.field public static final PATH_PD:Ljava/lang/String; = "mnt/sdcard/pd.test"

.field public static final PIN_CODE_ACTIVITY:Ljava/lang/String; = "PinCodeActivity"

.field public static final PORTRAIT_PHOTO:Ljava/lang/String; = "portrait_photo_image"

.field public static final PORTRAIT_PHOTO_PATH:Ljava/lang/String;

.field public static final PRIVATE_ID_DOWNLOAD_IMAGE:I = 0x2

.field public static final PRIVATE_ID_DOWNLOAD_LIST:I = 0x1

.field public static final PTAG_APPID:Ljava/lang/String; = "appId"

.field public static final PTAG_APPINFO:Ljava/lang/String; = "appInfo"

.field public static final PTAG_RESULT:Ljava/lang/String; = "result"

.field public static final PTAG_RESULTCODE:Ljava/lang/String; = "resultCode"

.field public static final PTAG_RESULT_MSG:Ljava/lang/String; = "resultMsg"

.field public static final PTAG_VERSION:Ljava/lang/String; = "version"

.field public static final PTAG_VERSION_CODE:Ljava/lang/String; = "versionCode"

.field public static final REQUEST_DELETE:I = 0x3

.field public static final REQUEST_HOME_EDIT:I = 0x3ff

.field public static final REQUEST_HOME_EDIT_SUCCESS:Ljava/lang/String; = "HOME_EDIT_SUCCESS"

.field public static final REQUEST_TAKE_PICTURE:I = 0x1

.field public static final REQUEST_USERPROFILE:I = 0x4

.field public static final RESTORE_SHAREDPREF_FILE_PREFIX:Ljava/lang/String; = "restore_"

.field public static final SAMSUNGAPPS_MAIN:Ljava/lang/String; = "com.sec.android.app.samsungapps.Main"

.field public static final SERVER_URL:Ljava/lang/String; = "http://hub.samsungapps.com/product/appCheck.as"

.field public static final SERVER_URL_ANDROID:Ljava/lang/String; = "http://vas.samsungapps.com/stub/stubUpdateCheck.as"

.field public static final SERVICE_HEALTHSERVICE:Ljava/lang/String; = "com.sec.android.service.health"

.field public static final SERVICE_SYNCSERVICE:Ljava/lang/String; = "com.sec.android.app.shealth.syncservice"

.field public static final SETPICTURE:Ljava/lang/String; = "SetPicture"

.field public static final SET_WALLPAPER_POPUP:Ljava/lang/String; = "set_wallpaper_popup"

.field public static final SHEALTH_AWARD_ACTIVITY:Ljava/lang/String; = "com.sec.android.app.shealth.award.AwardActivity"

.field public static final SHEALTH_HOMEACTIVITY:Ljava/lang/String; = "com.sec.android.app.shealth.home.HomeActivity"

.field public static final SHEALTH_MOREAPPS:Ljava/lang/String; = "com.sec.android.app.shealth.moreapps"

.field public static final SHEALTH_MOREAPPS_ACTIVITY:Ljava/lang/String; = "com.sec.android.app.shealth.moreapps.MoreAppsMainActivity"

.field public static final TIMELINE_FILTER_POPUP:Ljava/lang/String; = "timeline_filter_popup"

.field public static final USER_MANUAL_URL:Ljava/lang/String; = "user_manual_url"

.field public static final WALK_FOR_LIFE_ACTIVITY:Ljava/lang/String; = "walk_for_life_activity"

.field public static final WALLPAPER_RESOURCE_ID:Ljava/lang/String; = "wallpaper_resource_id"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 152
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->CACHED_IMAGES_FULL_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "photo_image"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/UDR;->NORMAL_PHOTO_PATH:Ljava/lang/String;

    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->CACHED_IMAGES_FULL_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "portrait_photo_image"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/UDR;->PORTRAIT_PHOTO_PATH:Ljava/lang/String;

    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->CACHED_IMAGES_FULL_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "landscape_photo_image"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/UDR;->LANDSCAPE_PHOTO_PATH:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
