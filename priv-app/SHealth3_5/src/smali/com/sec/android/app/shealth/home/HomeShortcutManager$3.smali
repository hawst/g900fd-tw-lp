.class Lcom/sec/android/app/shealth/home/HomeShortcutManager$3;
.super Ljava/lang/Object;
.source "HomeShortcutManager.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/home/HomeShortcutManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/HomeShortcutManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/HomeShortcutManager;)V
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager$3;->this$0:Lcom/sec/android/app/shealth/home/HomeShortcutManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 187
    const v2, 0x7f080515

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 189
    .local v0, "iconImage":Landroid/widget/ImageView;
    const/4 v1, 0x0

    .line 190
    .local v1, "originIcon":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    .line 191
    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 193
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 209
    :cond_1
    :goto_0
    :pswitch_0
    const/4 v2, 0x0

    return v2

    .line 196
    :pswitch_1
    if-eqz v1, :cond_1

    .line 197
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager$3;->this$0:Lcom/sec/android/app/shealth/home/HomeShortcutManager;

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->convertDrawableIntoBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object v3

    # invokes: Lcom/sec/android/app/shealth/home/HomeShortcutManager;->creatIconWithOutLine(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->access$200(Lcom/sec/android/app/shealth/home/HomeShortcutManager;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 201
    :pswitch_2
    if-eqz v1, :cond_1

    .line 203
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 204
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 193
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
