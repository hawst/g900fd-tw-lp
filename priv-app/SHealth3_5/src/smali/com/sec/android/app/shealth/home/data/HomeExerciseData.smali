.class public Lcom/sec/android/app/shealth/home/data/HomeExerciseData;
.super Ljava/lang/Object;
.source "HomeExerciseData.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "HomeExerciseData"


# instance fields
.field private imageGetter:Landroid/text/Html$ImageGetter;

.field private isNeedShowWearableIcon:Z

.field private mContext:Landroid/content/Context;

.field mName_exercise:Ljava/lang/String;

.field private mTime:J

.field private titleText:Ljava/lang/String;

.field private valueImage1:I

.field private valueImage2:I

.field private valueText:Landroid/text/Spanned;

.field private valueText1:Ljava/lang/String;

.field private valueText2:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->titleText:Ljava/lang/String;

    .line 42
    iput-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->valueText:Landroid/text/Spanned;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->mName_exercise:Ljava/lang/String;

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->isNeedShowWearableIcon:Z

    .line 45
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->mTime:J

    .line 236
    new-instance v0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData$1;-><init>(Lcom/sec/android/app/shealth/home/data/HomeExerciseData;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->imageGetter:Landroid/text/Html$ImageGetter;

    .line 56
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->mContext:Landroid/content/Context;

    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->initialSetData()V

    .line 58
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/home/data/HomeExerciseData;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/data/HomeExerciseData;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private checkExerciseValue(IF)Ljava/lang/String;
    .locals 12
    .param p1, "type"    # I
    .param p2, "val"    # F

    .prologue
    const/4 v11, 0x6

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v10, 0x2

    .line 209
    float-to-double v6, p2

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    .line 210
    .local v3, "value":Ljava/lang/Double;
    const-string v0, ""

    .line 211
    .local v0, "check":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->loadUnitsFromSharedPreferences()V

    .line 212
    sget-object v6, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoDistanceUnit:Ljava/lang/String;

    const-string v7, "km"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    move v2, v4

    .line 213
    .local v2, "unitMile":Z
    :goto_0
    if-eqz v2, :cond_4

    .line 214
    if-ne p1, v10, :cond_3

    .line 215
    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    const-wide v8, 0x3f445c6dea1359a4L    # 6.2137E-4

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    .line 226
    :cond_0
    :goto_1
    if-ne p1, v10, :cond_5

    .line 227
    new-instance v1, Ljava/text/DecimalFormat;

    const-string v4, "0.00"

    new-instance v5, Ljava/text/DecimalFormatSymbols;

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v5, v6}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-direct {v1, v4, v5}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 228
    .local v1, "df":Ljava/text/DecimalFormat;
    sget-object v4, Ljava/math/RoundingMode;->DOWN:Ljava/math/RoundingMode;

    invoke-virtual {v1, v4}, Ljava/text/DecimalFormat;->setRoundingMode(Ljava/math/RoundingMode;)V

    .line 229
    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    .line 233
    .end local v1    # "df":Ljava/text/DecimalFormat;
    :cond_1
    :goto_2
    return-object v0

    .end local v2    # "unitMile":Z
    :cond_2
    move v2, v5

    .line 212
    goto :goto_0

    .line 216
    .restart local v2    # "unitMile":Z
    :cond_3
    if-ne p1, v11, :cond_0

    .line 217
    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    const-wide v8, 0x400a3f290abb44e5L    # 3.28084

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    goto :goto_1

    .line 220
    :cond_4
    if-ne p1, v10, :cond_0

    .line 222
    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    const-wide v8, 0x408f400000000000L    # 1000.0

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    goto :goto_1

    .line 230
    :cond_5
    if-ne p1, v11, :cond_1

    .line 231
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "%d"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Double;->intValue()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v4, v5

    invoke-static {v6, v7, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method private initialSetData()V
    .locals 30

    .prologue
    .line 89
    const/4 v4, 0x0

    .line 90
    .local v4, "projection":[Ljava/lang/String;
    const/4 v2, 0x6

    new-array v4, v2, [Ljava/lang/String;

    .end local v4    # "projection":[Ljava/lang/String;
    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "total_calorie"

    aput-object v3, v4, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "start_time"

    aput-object v3, v4, v2

    const/4 v2, 0x3

    const-string v3, "exercise_info__id"

    aput-object v3, v4, v2

    const/4 v2, 0x4

    const-string v3, "duration_millisecond"

    aput-object v3, v4, v2

    const/4 v2, 0x5

    const-string v3, "distance"

    aput-object v3, v4, v2

    .line 92
    .restart local v4    # "projection":[Ljava/lang/String;
    const/16 v19, 0x0

    .line 94
    .local v19, "cursor":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "exercise_info__id!=18001"

    const/4 v6, 0x0

    const-string/jumbo v7, "start_time DESC LIMIT 1"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 96
    if-eqz v19, :cond_3

    .line 97
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    .line 98
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_9

    .line 99
    const-string v2, "_id"

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v24

    .line 100
    .local v24, "id":J
    const-string/jumbo v2, "total_calorie"

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 101
    .local v18, "kcal":I
    const-string v2, "duration_millisecond"

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v21

    .line 102
    .local v21, "duration":J
    const-string v2, "distance"

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v14

    .line 103
    .local v14, "distance":F
    const-wide/16 v16, 0x0

    .line 105
    .local v16, "elevation":D
    const/4 v13, 0x0

    .line 106
    .local v13, "name_category":Ljava/lang/String;
    const/16 v29, 0x0

    .line 107
    .local v29, "name_exercise":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->mContext:Landroid/content/Context;

    move-wide/from16 v0, v21

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SecToString(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    .line 108
    .local v15, "durationText":Ljava/lang/String;
    const-string/jumbo v2, "start_time"

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->mTime:J

    .line 109
    const-string v2, "exercise_info__id"

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v26

    .line 110
    .local v26, "info_id":J
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 112
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "select ca._id, ca.name ca_Name, ex._id, ex.name ex_Name from exercise_info ex left outer join exercise_info ca on ca._id = ex.exercise_info__id  where ex._id ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, v26

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 113
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 114
    const-string v2, "ca_Name"

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 115
    const-string v2, "ex_Name"

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v29

    .line 116
    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->mName_exercise:Ljava/lang/String;

    .line 117
    const-string v2, "HomeExerciseData"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "name_categoty = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ","

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, "name_exercise ="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v29

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move-object/from16 v28, v13

    .line 119
    .end local v13    # "name_category":Ljava/lang/String;
    .local v28, "name_category":Ljava/lang/String;
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 121
    const/4 v2, 0x1

    new-array v7, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "max_altitude"

    aput-object v3, v7, v2

    .line 122
    .local v7, "projection2":[Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseActivity;->CONTENT_URI:Landroid/net/Uri;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exercise__id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, v24

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 123
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 124
    const-string/jumbo v2, "max_altitude"

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v16

    .line 126
    :cond_1
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 128
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exercise__id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, v24

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 129
    .local v11, "selectionClauseDEVICE":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v10, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "user_device__id"

    aput-object v3, v10, v2

    .line 130
    .local v10, "mSelectionArgsDEVICE":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    sget-object v9, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseDeviceInfo;->CONTENT_URI:Landroid/net/Uri;

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual/range {v8 .. v13}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 131
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 132
    const-string/jumbo v2, "user_device__id"

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 133
    .local v20, "deviceType":Ljava/lang/String;
    if-eqz v20, :cond_2

    const-string v2, "_"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_2

    .line 134
    const-string v2, "_"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/16 v3, 0x2719

    if-eq v2, v3, :cond_2

    .line 135
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->isNeedShowWearableIcon:Z

    .line 139
    .end local v20    # "deviceType":Ljava/lang/String;
    :cond_2
    if-nez v28, :cond_a

    if-eqz v29, :cond_a

    .line 140
    move-object/from16 v13, v29

    .line 143
    .end local v28    # "name_category":Ljava/lang/String;
    .restart local v13    # "name_category":Ljava/lang/String;
    :goto_0
    if-eqz v29, :cond_5

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->upgradeSpecificCode(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 145
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->upgradeSpecificCode_getNameId(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->titleText:Ljava/lang/String;

    .line 146
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->titleText:Ljava/lang/String;

    .end local v13    # "name_category":Ljava/lang/String;
    move-object/from16 v12, p0

    invoke-direct/range {v12 .. v18}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->setValueText(Ljava/lang/String;FLjava/lang/String;DI)V

    .line 147
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->titleText:Ljava/lang/String;

    move-object/from16 v12, p0

    invoke-direct/range {v12 .. v18}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->setValue(Ljava/lang/String;FLjava/lang/String;DI)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 171
    .end local v7    # "projection2":[Ljava/lang/String;
    .end local v10    # "mSelectionArgsDEVICE":[Ljava/lang/String;
    .end local v11    # "selectionClauseDEVICE":Ljava/lang/String;
    .end local v14    # "distance":F
    .end local v15    # "durationText":Ljava/lang/String;
    .end local v16    # "elevation":D
    .end local v18    # "kcal":I
    .end local v21    # "duration":J
    .end local v24    # "id":J
    .end local v26    # "info_id":J
    .end local v29    # "name_exercise":Ljava/lang/String;
    :cond_3
    :goto_1
    if-eqz v19, :cond_4

    .line 172
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 175
    :cond_4
    :goto_2
    return-void

    .line 152
    .restart local v7    # "projection2":[Ljava/lang/String;
    .restart local v10    # "mSelectionArgsDEVICE":[Ljava/lang/String;
    .restart local v11    # "selectionClauseDEVICE":Ljava/lang/String;
    .restart local v13    # "name_category":Ljava/lang/String;
    .restart local v14    # "distance":F
    .restart local v15    # "durationText":Ljava/lang/String;
    .restart local v16    # "elevation":D
    .restart local v18    # "kcal":I
    .restart local v21    # "duration":J
    .restart local v24    # "id":J
    .restart local v26    # "info_id":J
    .restart local v29    # "name_exercise":Ljava/lang/String;
    :cond_5
    if-eqz v13, :cond_6

    :try_start_1
    const-string v2, "RealtimeWorkout"

    invoke-virtual {v13, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 153
    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->titleText:Ljava/lang/String;

    move-object/from16 v12, p0

    move-object/from16 v13, v29

    .line 154
    invoke-direct/range {v12 .. v18}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->setValueText(Ljava/lang/String;FLjava/lang/String;DI)V

    .end local v13    # "name_category":Ljava/lang/String;
    move-object/from16 v12, p0

    move-object/from16 v13, v29

    .line 155
    invoke-direct/range {v12 .. v18}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->setValue(Ljava/lang/String;FLjava/lang/String;DI)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 168
    .end local v7    # "projection2":[Ljava/lang/String;
    .end local v10    # "mSelectionArgsDEVICE":[Ljava/lang/String;
    .end local v11    # "selectionClauseDEVICE":Ljava/lang/String;
    .end local v14    # "distance":F
    .end local v15    # "durationText":Ljava/lang/String;
    .end local v16    # "elevation":D
    .end local v18    # "kcal":I
    .end local v21    # "duration":J
    .end local v24    # "id":J
    .end local v26    # "info_id":J
    .end local v29    # "name_exercise":Ljava/lang/String;
    :catch_0
    move-exception v23

    .line 169
    .local v23, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 171
    if-eqz v19, :cond_4

    .line 172
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 156
    .end local v23    # "e":Ljava/lang/Exception;
    .restart local v7    # "projection2":[Ljava/lang/String;
    .restart local v10    # "mSelectionArgsDEVICE":[Ljava/lang/String;
    .restart local v11    # "selectionClauseDEVICE":Ljava/lang/String;
    .restart local v13    # "name_category":Ljava/lang/String;
    .restart local v14    # "distance":F
    .restart local v15    # "durationText":Ljava/lang/String;
    .restart local v16    # "elevation":D
    .restart local v18    # "kcal":I
    .restart local v21    # "duration":J
    .restart local v24    # "id":J
    .restart local v26    # "info_id":J
    .restart local v29    # "name_exercise":Ljava/lang/String;
    :cond_6
    if-eqz v13, :cond_8

    .line 157
    :try_start_3
    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->titleText:Ljava/lang/String;

    move-object/from16 v12, p0

    .line 158
    invoke-direct/range {v12 .. v18}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->setValueText(Ljava/lang/String;FLjava/lang/String;DI)V

    move-object/from16 v12, p0

    .line 159
    invoke-direct/range {v12 .. v18}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->setValue(Ljava/lang/String;FLjava/lang/String;DI)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 171
    .end local v7    # "projection2":[Ljava/lang/String;
    .end local v10    # "mSelectionArgsDEVICE":[Ljava/lang/String;
    .end local v11    # "selectionClauseDEVICE":Ljava/lang/String;
    .end local v13    # "name_category":Ljava/lang/String;
    .end local v14    # "distance":F
    .end local v15    # "durationText":Ljava/lang/String;
    .end local v16    # "elevation":D
    .end local v18    # "kcal":I
    .end local v21    # "duration":J
    .end local v24    # "id":J
    .end local v26    # "info_id":J
    .end local v29    # "name_exercise":Ljava/lang/String;
    :catchall_0
    move-exception v2

    if-eqz v19, :cond_7

    .line 172
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v2

    .line 161
    .restart local v7    # "projection2":[Ljava/lang/String;
    .restart local v10    # "mSelectionArgsDEVICE":[Ljava/lang/String;
    .restart local v11    # "selectionClauseDEVICE":Ljava/lang/String;
    .restart local v13    # "name_category":Ljava/lang/String;
    .restart local v14    # "distance":F
    .restart local v15    # "durationText":Ljava/lang/String;
    .restart local v16    # "elevation":D
    .restart local v18    # "kcal":I
    .restart local v21    # "duration":J
    .restart local v24    # "id":J
    .restart local v26    # "info_id":J
    .restart local v29    # "name_exercise":Ljava/lang/String;
    :cond_8
    const-wide/16 v2, -0x1

    :try_start_4
    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->mTime:J

    goto :goto_1

    .line 165
    .end local v7    # "projection2":[Ljava/lang/String;
    .end local v10    # "mSelectionArgsDEVICE":[Ljava/lang/String;
    .end local v11    # "selectionClauseDEVICE":Ljava/lang/String;
    .end local v13    # "name_category":Ljava/lang/String;
    .end local v14    # "distance":F
    .end local v15    # "durationText":Ljava/lang/String;
    .end local v16    # "elevation":D
    .end local v18    # "kcal":I
    .end local v21    # "duration":J
    .end local v24    # "id":J
    .end local v26    # "info_id":J
    .end local v29    # "name_exercise":Ljava/lang/String;
    :cond_9
    const-wide/16 v2, -0x1

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->mTime:J
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .restart local v7    # "projection2":[Ljava/lang/String;
    .restart local v10    # "mSelectionArgsDEVICE":[Ljava/lang/String;
    .restart local v11    # "selectionClauseDEVICE":Ljava/lang/String;
    .restart local v14    # "distance":F
    .restart local v15    # "durationText":Ljava/lang/String;
    .restart local v16    # "elevation":D
    .restart local v18    # "kcal":I
    .restart local v21    # "duration":J
    .restart local v24    # "id":J
    .restart local v26    # "info_id":J
    .restart local v28    # "name_category":Ljava/lang/String;
    .restart local v29    # "name_exercise":Ljava/lang/String;
    :cond_a
    move-object/from16 v13, v28

    .end local v28    # "name_category":Ljava/lang/String;
    .restart local v13    # "name_category":Ljava/lang/String;
    goto/16 :goto_0
.end method

.method private setValue(Ljava/lang/String;FLjava/lang/String;DI)V
    .locals 6
    .param p1, "index"    # Ljava/lang/String;
    .param p2, "distance"    # F
    .param p3, "duration"    # Ljava/lang/String;
    .param p4, "elevation"    # D
    .param p6, "kcal"    # I

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x2

    const v3, 0x7f0204c4

    .line 178
    const-string v0, "Running"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Walking"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Cycling"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 179
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, v4, p2}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->checkExerciseValue(IF)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->mContext:Landroid/content/Context;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->valueText1:Ljava/lang/String;

    .line 180
    iput-object p3, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->valueText2:Ljava/lang/String;

    .line 181
    const v0, 0x7f0204c3

    iput v0, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->valueImage1:I

    .line 182
    iput v3, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->valueImage2:I

    .line 194
    :goto_0
    return-void

    .line 183
    :cond_1
    const-string v0, "Hiking"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 184
    iput-object p3, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->valueText1:Ljava/lang/String;

    .line 185
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    double-to-float v1, p4

    invoke-direct {p0, v5, v1}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->checkExerciseValue(IF)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->mContext:Landroid/content/Context;

    double-to-float v2, p4

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->valueText2:Ljava/lang/String;

    .line 186
    iput v3, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->valueImage1:I

    .line 187
    const v0, 0x7f0204c5

    iput v0, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->valueImage2:I

    goto :goto_0

    .line 189
    :cond_2
    iput-object p3, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->valueText1:Ljava/lang/String;

    .line 190
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900b9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->valueText2:Ljava/lang/String;

    .line 191
    iput v3, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->valueImage1:I

    .line 192
    const v0, 0x7f0204c2

    iput v0, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->valueImage2:I

    goto :goto_0
.end method

.method private setValueText(Ljava/lang/String;FLjava/lang/String;DI)V
    .locals 6
    .param p1, "index"    # Ljava/lang/String;
    .param p2, "distance"    # F
    .param p3, "duration"    # Ljava/lang/String;
    .param p4, "elevation"    # D
    .param p6, "kcal"    # I

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 196
    if-nez p1, :cond_0

    .line 206
    :goto_0
    return-void

    .line 199
    :cond_0
    const-string v0, "Running"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "Walking"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "Cycling"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 200
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<img src=\"distanceIcon\"> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, v4, p2}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->checkExerciseValue(IF)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->mContext:Landroid/content/Context;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  <img src=\"durationIcon\"> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->imageGetter:Landroid/text/Html$ImageGetter;

    invoke-static {v0, v1, v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->valueText:Landroid/text/Spanned;

    goto :goto_0

    .line 201
    :cond_2
    const-string v0, "Hiking"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 202
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<img src=\"durationIcon\"> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  <img src=\"elevation\"> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    double-to-float v1, p4

    invoke-direct {p0, v5, v1}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->checkExerciseValue(IF)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->mContext:Landroid/content/Context;

    double-to-float v2, p4

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->imageGetter:Landroid/text/Html$ImageGetter;

    invoke-static {v0, v1, v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->valueText:Landroid/text/Spanned;

    goto/16 :goto_0

    .line 204
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<img src=\"durationIcon\"> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  <img src=\"calory\"> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900b9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->imageGetter:Landroid/text/Html$ImageGetter;

    invoke-static {v0, v1, v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->valueText:Landroid/text/Spanned;

    goto/16 :goto_0
.end method


# virtual methods
.method public getNameExercise()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->mName_exercise:Ljava/lang/String;

    return-object v0
.end method

.method public getTime()J
    .locals 2

    .prologue
    .line 81
    iget-wide v0, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->mTime:J

    return-wide v0
.end method

.method public getTitleText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->titleText:Ljava/lang/String;

    return-object v0
.end method

.method public getValueImage1()I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->valueImage1:I

    return v0
.end method

.method public getValueImage2()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->valueImage2:I

    return v0
.end method

.method public getValueText()Landroid/text/Spanned;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->valueText:Landroid/text/Spanned;

    return-object v0
.end method

.method public getValueText1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->valueText1:Ljava/lang/String;

    return-object v0
.end method

.method public getValueText2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->valueText2:Ljava/lang/String;

    return-object v0
.end method

.method public isNeedShowWearableIcon()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->isNeedShowWearableIcon:Z

    return v0
.end method

.method public upgradeSpecificCode(Ljava/lang/String;)Z
    .locals 1
    .param p1, "l_name_exercise"    # Ljava/lang/String;

    .prologue
    .line 264
    const-string v0, "Ballroom Dance, Slow"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Pull-up(Chin-ups)"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Pull-up ( Chin-ups )"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Martial arts, Moderate pace"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Sit-up ( Sit-ups )"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Push-up ( Press-ups )"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Fast Walking"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Walking Downstairs"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Martial arts, Slower pace"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Walking Upstairs"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Tai chi, General"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Running"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 267
    :cond_0
    const/4 v0, 0x1

    .line 270
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public upgradeSpecificCode_getImageId(Ljava/lang/String;)I
    .locals 2
    .param p1, "l_name_exercise"    # Ljava/lang/String;

    .prologue
    .line 291
    const-string v0, "Fast Walking"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 293
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "Power walking"

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 304
    :goto_0
    return v0

    .line 295
    :cond_0
    const-string v0, "Tai chi, General"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 297
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/utils/ExerciseImageResources;->RESOURCES:Ljava/util/TreeMap;

    const-string v1, "T\'ai chi"

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 301
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    invoke-virtual {v0, p1}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 302
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;->EXERCISE_RESOURCES:Ljava/util/TreeMap;

    invoke-virtual {v0, p1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 304
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public upgradeSpecificCode_getNameId(Ljava/lang/String;)I
    .locals 2
    .param p1, "l_name_exercise"    # Ljava/lang/String;

    .prologue
    .line 275
    const-string v0, "Fast Walking"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 277
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "Power walking"

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 285
    :goto_0
    return v0

    .line 279
    :cond_0
    const-string v0, "Tai chi, General"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 281
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    const-string v1, "T\'ai chi"

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 285
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    invoke-virtual {v0, p1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method
