.class public Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;
.super Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;
.source "WeightSummaryFragment.java"


# instance fields
.field protected mBMITextView:Landroid/widget/TextView;

.field protected mRangeStateTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;-><init>()V

    return-void
.end method

.method private setBMI(FFLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;)V
    .locals 11
    .param p1, "weight"    # F
    .param p2, "entryHeight"    # F
    .param p3, "heightUnit"    # Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

    .prologue
    .line 86
    const/4 v4, 0x0

    .line 88
    .local v4, "newText":Ljava/lang/String;
    move v2, p2

    .line 90
    .local v2, "height":F
    const/high16 v5, -0x40800000    # -1.0f

    cmpl-float v5, v2, v5

    if-nez v5, :cond_0

    .line 91
    new-instance v1, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-direct {v1, v5}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 92
    .local v1, "healthProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v2

    .line 107
    .end local v1    # "healthProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    :cond_0
    const/high16 v5, -0x40800000    # -1.0f

    cmpl-float v5, v2, v5

    if-eqz v5, :cond_1

    const/4 v5, 0x0

    cmpl-float v5, p1, v5

    if-nez v5, :cond_2

    .line 108
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090ca2

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 121
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;->mBMITextView:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    return-void

    .line 110
    :cond_2
    float-to-double v5, p1

    const/high16 v7, 0x42c80000    # 100.0f

    div-float v7, v2, v7

    float-to-double v7, v7

    const-wide/high16 v9, 0x4000000000000000L    # 2.0

    invoke-static {v7, v8, v9, v10}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v7

    div-double/2addr v5, v7

    double-to-float v0, v5

    .line 113
    .local v0, "bmi":F
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget-object v5, v5, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    .line 114
    .local v3, "language":Ljava/lang/String;
    if-eqz v3, :cond_3

    const-string v5, "ar"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 115
    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    const v7, 0x7f090c94

    invoke-virtual {v6, v7}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 117
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const v6, 0x7f090c94

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/support/v4/app/FragmentActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private setWeightIndicatorNormalRange(F)V
    .locals 4
    .param p1, "heightInCm"    # F

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/sec/android/app/shealth/weight/utils/WeightNormalRangeHelper;->getNormalRangeValues(Landroid/content/Context;F)Landroid/util/Pair;

    move-result-object v0

    .line 137
    .local v0, "normalRange":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;->mWeightIndicator:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v3

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v2, v3, v1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->setNormalWeightRange(FF)V

    .line 138
    return-void
.end method

.method private updateBMIValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;)V
    .locals 3
    .param p1, "weightData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    .prologue
    .line 80
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getValue()F

    move-result v1

    .line 81
    .local v1, "weight":F
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getHeight()F

    move-result v0

    .line 82
    .local v0, "height":F
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getHeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;->setBMI(FFLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;)V

    .line 83
    return-void
.end method

.method private updateNormalRange(Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;)V
    .locals 5
    .param p1, "weightData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    .prologue
    .line 125
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getValue()F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v1

    .line 126
    .local v1, "weight":F
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getHeight()F

    move-result v0

    .line 127
    .local v0, "height":F
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;->setWeightIndicatorNormalRange(F)V

    .line 128
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2, v1, v0}, Lcom/sec/android/app/shealth/weight/utils/WeightNormalRangeHelper;->isInNormalRange(Landroid/content/Context;FF)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 129
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;->mRangeStateTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v4, 0x7f090d10

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    :goto_0
    return-void

    .line 131
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;->mRangeStateTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v4, 0x7f090c9f

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method protected getContentViewId()I
    .locals 1

    .prologue
    .line 46
    const v0, 0x7f0302c7

    return v0
.end method

.method protected initializeValueTextViews(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 51
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->initializeValueTextViews(Landroid/view/View;)V

    .line 52
    const v0, 0x7f080c47

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;->mRangeStateTextView:Landroid/widget/TextView;

    .line 53
    sget-object v0, Lcom/sec/android/app/shealth/weight/common/WeightConstants;->APP_TYPE:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    sget-object v1, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->DOWNLOAD_MR:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;->mRangeStateTextView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 59
    :goto_0
    const v0, 0x7f080c44

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;->mBMITextView:Landroid/widget/TextView;

    .line 60
    return-void

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;->mRangeStateTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 73
    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;->mBMITextView:Landroid/widget/TextView;

    .line 74
    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;->mRangeStateTextView:Landroid/widget/TextView;

    .line 76
    invoke-super {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->onDestroy()V

    .line 77
    return-void
.end method

.method protected updateWeightData(Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;Z)V
    .locals 0
    .param p1, "weightData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    .param p2, "needAnimation"    # Z

    .prologue
    .line 64
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->updateWeightData(Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;Z)V

    .line 65
    if-eqz p1, :cond_0

    .line 66
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;->updateBMIValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;)V

    .line 67
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;->updateNormalRange(Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;)V

    .line 69
    :cond_0
    return-void
.end method
