.class public Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;
.super Ljava/lang/Object;
.source "MapPhotoList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList$UpdateHandler;
    }
.end annotation


# instance fields
.field private gHandler:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList$UpdateHandler;

.field mContext:Landroid/content/Context;

.field mPhotoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;>;"
        }
    .end annotation
.end field

.field private mPhotoListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;

.field res:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;->mPhotoList:Ljava/util/ArrayList;

    .line 24
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;->mContext:Landroid/content/Context;

    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;->init()V

    .line 26
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;->mPhotoListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;

    return-object v0
.end method

.method private init()V
    .locals 4

    .prologue
    .line 35
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList$UpdateHandler;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList$UpdateHandler;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;->gHandler:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList$UpdateHandler;

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;->res:Landroid/content/res/Resources;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;->gHandler:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList$UpdateHandler;

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->init(Landroid/content/Context;Landroid/content/res/Resources;Ljava/lang/Object;Z)V

    .line 37
    return-void
.end method


# virtual methods
.method public SetPhotoList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 44
    .local p1, "photodatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;"
    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->SetPhotoList(Ljava/util/List;)V

    .line 45
    return-void
.end method

.method public getScreenPhotoList(I)Ljava/util/List;
    .locals 1
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->getScreenPhotoList(I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 52
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->getSize()I

    move-result v0

    return v0
.end method

.method public setOnUpdatePhotoListListener(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;

    .prologue
    .line 40
    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->setOnUpdatePhotoListListener(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;)V

    .line 41
    return-void
.end method
