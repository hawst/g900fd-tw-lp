.class public Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;
.super Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;
.source "WalkMatePlainAppWidget.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;-><init>()V

    .line 44
    const-class v0, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Constructor called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    sget-object v0, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->sWidgets:[Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    .line 46
    return-void
.end method


# virtual methods
.method public onDisabled(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    sget-object v0, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->sWidgets:[Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;

    const/4 v1, 0x0

    const/4 v2, 0x0

    aput-object v2, v0, v1

    .line 51
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->onDisabled(Landroid/content/Context;)V

    .line 52
    return-void
.end method

.method protected updateWidget(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 27
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .prologue
    .line 56
    sget-object v21, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v23, "updateWidget() - appWidgetIds size = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p3

    array-length v0, v0

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v5

    .line 59
    .local v5, "connectedDevice":I
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v12

    .line 60
    .local v12, "isActiveDisplay":Z
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getHRAvailabliltyFalseIfUnknown(Landroid/content/Context;)Z

    move-result v21

    if-eqz v21, :cond_8

    sget-object v21, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->HeartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v21

    sget-object v22, Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_8

    const/4 v10, 0x1

    .line 62
    .local v10, "hrAvailability":Z
    :goto_0
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v13

    .line 63
    .local v13, "isContentProviderAccessible":Z
    sget-object v21, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "isContentProviderAccessible : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    move-wide/from16 v0, v21

    long-to-float v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->mGoalStep:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    div-float v17, v21, v22

    .line 68
    .local v17, "percent":F
    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v21, v0

    const-wide v23, 0x3fb999999999999aL    # 0.1

    cmpl-double v21, v21, v23

    if-ltz v21, :cond_9

    move/from16 v21, v17

    :goto_1
    const/16 v22, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-static {v0, v1, v12, v2}, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->getSmallProgressBitmapBurned(Landroid/content/Context;FZZ)Landroid/graphics/Bitmap;

    move-result-object v18

    .line 69
    .local v18, "progressImage":Landroid/graphics/Bitmap;
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->getCurrentMode(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 73
    .local v6, "currentMode":Ljava/lang/String;
    const/4 v11, 0x0

    .local v11, "i":I
    move-object/from16 v0, p3

    array-length v14, v0

    .local v14, "n":I
    :goto_2
    if-ge v11, v14, :cond_21

    .line 74
    aget v4, p3, v11

    .line 75
    .local v4, "appWidgetId":I
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->isUseCigna()Z

    move-result v21

    if-eqz v21, :cond_b

    .line 76
    new-instance v20, Landroid/widget/RemoteViews;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v21

    const v22, 0x7f03027c

    invoke-direct/range {v20 .. v22}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 77
    .local v20, "views":Landroid/widget/RemoteViews;
    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v13, v0, :cond_a

    .line 78
    const v21, 0x7f080b42

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getCignaText()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 79
    const v21, 0x7f080b42

    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v22

    const-string v23, "SHEALTH_WIDGET"

    const/16 v24, 0x98e

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    move-object/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getAppropriatePendingIntent(Landroid/content/Context;Ljava/lang/String;I)Landroid/app/PendingIntent;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 84
    :goto_3
    const v21, 0x7f080b43

    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getCignaWebLinkPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 85
    const v21, 0x7f080b43

    const v22, 0x7f090faf

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 91
    :goto_4
    const v21, 0x7f080b33

    const/16 v22, 0x4

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 94
    const-string v21, "healthy"

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_d

    .line 96
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->mGoalStep:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v23, v0

    cmp-long v21, v21, v23

    if-gez v21, :cond_c

    .line 97
    const v21, 0x7f080b31

    const v22, 0x7f020712

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v12, v5}, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->getIconBitmap(Landroid/content/Context;IZI)Landroid/graphics/Bitmap;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 98
    const v21, 0x7f080b33

    const v22, 0x7f02071b

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v12, v5}, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->getIconBitmap(Landroid/content/Context;IZI)Landroid/graphics/Bitmap;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 99
    const v21, 0x7f080b33

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 130
    :goto_5
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->mGoalStep:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v23, v0

    cmp-long v21, v21, v23

    if-gez v21, :cond_15

    .line 131
    const v21, 0x7f080b32

    move-object/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 132
    const v21, 0x7f080b32

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 138
    :goto_6
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    const-wide/32 v23, 0x186a0

    cmp-long v21, v21, v23

    if-ltz v21, :cond_16

    .line 140
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    move-wide/from16 v0, v21

    long-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    div-int/lit16 v0, v0, 0x3e8

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v19

    .line 141
    .local v19, "stepValue":Ljava/lang/String;
    const v21, 0x7f080b34

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const v23, 0x7f090bb4

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 148
    .end local v19    # "stepValue":Ljava/lang/String;
    :goto_7
    const-string v21, "gear"

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_0

    .line 149
    const v21, 0x7f080b3b

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->getLastUpdatedTime()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 152
    :cond_0
    const v21, 0x7f080b35

    const v22, 0x7f0907dd

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 153
    const v21, 0x7f080b36

    const v22, 0x7f0907dd

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 156
    const-string/jumbo v21, "normal"

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_17

    if-nez v12, :cond_17

    .line 157
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->mGoalStep:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v23, v0

    cmp-long v21, v21, v23

    if-gez v21, :cond_1

    .line 158
    const v21, 0x7f080b31

    const v22, 0x7f02072c

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v12, v5}, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->getIconBitmap(Landroid/content/Context;IZI)Landroid/graphics/Bitmap;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 160
    :cond_1
    const v21, 0x7f080b37

    const v22, 0x7f090a17

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 161
    const v21, 0x7f080b34

    const/16 v22, 0x80

    const/16 v23, 0xff

    const/16 v24, 0xff

    const/16 v25, 0xff

    invoke-static/range {v22 .. v25}, Landroid/graphics/Color;->argb(IIII)I

    move-result v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 162
    const v21, 0x7f080b35

    const/16 v22, 0x80

    const/16 v23, 0xff

    const/16 v24, 0xff

    const/16 v25, 0xff

    invoke-static/range {v22 .. v25}, Landroid/graphics/Color;->argb(IIII)I

    move-result v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 163
    const v21, 0x7f080b36

    const/16 v22, 0x80

    const/16 v23, 0xff

    const/16 v24, 0xff

    const/16 v25, 0xff

    invoke-static/range {v22 .. v25}, Landroid/graphics/Color;->argb(IIII)I

    move-result v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 172
    :goto_8
    const-string v21, "gear"

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_1c

    .line 173
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    const-wide/16 v23, 0x0

    cmp-long v21, v21, v23

    if-nez v21, :cond_18

    .line 174
    const v21, 0x7f080b34

    const/16 v22, 0x8

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 175
    const v21, 0x7f080b35

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 180
    :goto_9
    const v21, 0x7f080b37

    const/16 v22, 0x8

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 181
    const v21, 0x7f080b36

    const/16 v22, 0x8

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 183
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->mWearableConnected:Z

    move/from16 v21, v0

    if-eqz v21, :cond_19

    .line 184
    const v21, 0x7f080521

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 185
    const v21, 0x7f080b3d

    const/16 v22, 0x8

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 187
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isSyncing()Z

    move-result v21

    if-eqz v21, :cond_2

    .line 188
    const v21, 0x7f080521

    const/16 v22, 0x8

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 189
    const v21, 0x7f080b3d

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 196
    :cond_2
    :goto_a
    const/4 v8, -0x1

    .line 197
    .local v8, "deviceType":I
    const/4 v7, 0x0

    .line 198
    .local v7, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz v13, :cond_3

    .line 199
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v21

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getSynchronizedWearableList(J)Ljava/util/ArrayList;

    move-result-object v7

    .line 200
    if-eqz v7, :cond_3

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_3

    .line 201
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 205
    :cond_3
    if-eqz v7, :cond_4

    .line 206
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_1a

    .line 207
    const v21, 0x7f080b3a

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 208
    const v21, 0x7f080b3c

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 209
    const v21, 0x7f080b3b

    const/16 v22, 0x8

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 212
    sparse-switch v8, :sswitch_data_0

    .line 227
    const-string v9, ""

    .line 230
    .local v9, "firstInfo":Ljava/lang/String;
    :goto_b
    const v21, 0x7f080b3c

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v0, v1, v9}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 256
    .end local v7    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v8    # "deviceType":I
    .end local v9    # "firstInfo":Ljava/lang/String;
    :cond_4
    :goto_c
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isSyncing()Z

    move-result v21

    if-nez v21, :cond_5

    .line 257
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->getSyncPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v16

    .line 258
    .local v16, "pendingIntentSync":Landroid/app/PendingIntent;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->mWearableConnected:Z

    move/from16 v21, v0

    if-eqz v21, :cond_1e

    .line 259
    const v21, 0x7f080b3a

    move-object/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 265
    .end local v16    # "pendingIntentSync":Landroid/app/PendingIntent;
    :cond_5
    :goto_d
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->getLaunchPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v15

    .line 267
    .local v15, "pendingIntent":Landroid/app/PendingIntent;
    if-eqz v15, :cond_7

    .line 269
    if-eqz v13, :cond_6

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_1f

    .line 270
    :cond_6
    const v21, 0x7f080b34

    const/16 v22, 0x8

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 271
    const v21, 0x7f080b37

    const/16 v22, 0x8

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 272
    const v21, 0x7f080b36

    const/16 v22, 0x8

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 273
    const v21, 0x7f080b3a

    const/16 v22, 0x8

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 274
    const v21, 0x7f080b35

    const/16 v22, 0x8

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 276
    const v21, 0x7f080b31

    const v22, 0x7f02072b

    const/16 v23, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v0, v1, v2, v3, v5}, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->getIconBitmap(Landroid/content/Context;IZI)Landroid/graphics/Bitmap;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 277
    const v21, 0x7f080525

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 278
    const v21, 0x7f080b36

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 279
    const v21, 0x7f080b36

    const/16 v22, 0xff

    const/16 v23, 0xff

    const/16 v24, 0xff

    const/16 v25, 0xff

    invoke-static/range {v22 .. v25}, Landroid/graphics/Color;->argb(IIII)I

    move-result v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 280
    const v21, 0x7f080525

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v0, v1, v15}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 281
    const v21, 0x7f080b36

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v0, v1, v15}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 293
    :goto_e
    if-eqz v10, :cond_20

    .line 294
    const v21, 0x7f080b3f

    const v22, 0x7f020714

    const/16 v23, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->getIconBitmap(Landroid/content/Context;IZ)Landroid/graphics/Bitmap;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 295
    const v21, 0x7f080b3f

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 296
    const v21, 0x7f080b3e

    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->getNotificationHRIntent()Landroid/app/PendingIntent;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 301
    :goto_f
    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v4, v1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 73
    :cond_7
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_2

    .line 60
    .end local v4    # "appWidgetId":I
    .end local v6    # "currentMode":Ljava/lang/String;
    .end local v10    # "hrAvailability":Z
    .end local v11    # "i":I
    .end local v13    # "isContentProviderAccessible":Z
    .end local v14    # "n":I
    .end local v15    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v17    # "percent":F
    .end local v18    # "progressImage":Landroid/graphics/Bitmap;
    .end local v20    # "views":Landroid/widget/RemoteViews;
    :cond_8
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 68
    .restart local v10    # "hrAvailability":Z
    .restart local v13    # "isContentProviderAccessible":Z
    .restart local v17    # "percent":F
    :cond_9
    const/16 v21, 0x0

    goto/16 :goto_1

    .line 81
    .restart local v4    # "appWidgetId":I
    .restart local v6    # "currentMode":Ljava/lang/String;
    .restart local v11    # "i":I
    .restart local v14    # "n":I
    .restart local v18    # "progressImage":Landroid/graphics/Bitmap;
    .restart local v20    # "views":Landroid/widget/RemoteViews;
    :cond_a
    const v21, 0x7f080b42

    const v22, 0x7f090247

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 82
    const v21, 0x7f080b42

    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getLaunchPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto/16 :goto_3

    .line 88
    .end local v20    # "views":Landroid/widget/RemoteViews;
    :cond_b
    new-instance v20, Landroid/widget/RemoteViews;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v21

    const v22, 0x7f03027f

    invoke-direct/range {v20 .. v22}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .restart local v20    # "views":Landroid/widget/RemoteViews;
    goto/16 :goto_4

    .line 101
    :cond_c
    const v21, 0x7f080b31

    const v22, 0x7f02071a

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v12, v5}, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->getIconBitmap(Landroid/content/Context;IZI)Landroid/graphics/Bitmap;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto/16 :goto_5

    .line 103
    :cond_d
    const-string v21, "inactive"

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_f

    .line 104
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    const-wide/16 v23, 0x0

    cmp-long v21, v21, v23

    if-nez v21, :cond_e

    .line 105
    const v21, 0x7f080b31

    const v22, 0x7f020716

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v12, v5}, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->getIconBitmap(Landroid/content/Context;IZI)Landroid/graphics/Bitmap;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto/16 :goto_5

    .line 107
    :cond_e
    const v21, 0x7f080b31

    const v22, 0x7f020715

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v12, v5}, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->getIconBitmap(Landroid/content/Context;IZI)Landroid/graphics/Bitmap;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto/16 :goto_5

    .line 108
    :cond_f
    const-string v21, "gear"

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_12

    .line 109
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    const-wide/16 v23, 0x0

    cmp-long v21, v21, v23

    if-nez v21, :cond_10

    .line 110
    const v21, 0x7f080b31

    const v22, 0x7f02071d

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v12, v5}, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->getIconBitmap(Landroid/content/Context;IZI)Landroid/graphics/Bitmap;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto/16 :goto_5

    .line 112
    :cond_10
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->mGoalStep:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v23, v0

    cmp-long v21, v21, v23

    if-gez v21, :cond_11

    .line 113
    const v21, 0x7f080b31

    const v22, 0x7f02071c

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v12, v5}, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->getIconBitmap(Landroid/content/Context;IZI)Landroid/graphics/Bitmap;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto/16 :goto_5

    .line 115
    :cond_11
    const v21, 0x7f080b31

    const v22, 0x7f02071e

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v12, v5}, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->getIconBitmap(Landroid/content/Context;IZI)Landroid/graphics/Bitmap;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto/16 :goto_5

    .line 119
    :cond_12
    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v21, v0

    const-wide v23, 0x3fb999999999999aL    # 0.1

    cmpg-double v21, v21, v23

    if-gtz v21, :cond_13

    .line 120
    const v21, 0x7f080b31

    const v22, 0x7f020718

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v12, v5}, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->getIconBitmap(Landroid/content/Context;IZI)Landroid/graphics/Bitmap;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto/16 :goto_5

    .line 122
    :cond_13
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->mGoalStep:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v23, v0

    cmp-long v21, v21, v23

    if-gez v21, :cond_14

    .line 123
    const v21, 0x7f080b31

    const v22, 0x7f020717

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v12, v5}, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->getIconBitmap(Landroid/content/Context;IZI)Landroid/graphics/Bitmap;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto/16 :goto_5

    .line 125
    :cond_14
    const v21, 0x7f080b31

    const v22, 0x7f02071a

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v12, v5}, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->getIconBitmap(Landroid/content/Context;IZI)Landroid/graphics/Bitmap;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto/16 :goto_5

    .line 134
    :cond_15
    const v21, 0x7f080b32

    const/16 v22, 0x8

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_6

    .line 145
    :cond_16
    const v21, 0x7f080b34

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->mTotalStep:J

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_7

    .line 165
    :cond_17
    const v21, 0x7f080b37

    const v22, 0x7f0907e5

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 166
    const v21, 0x7f080b34

    const/16 v22, 0xff

    const/16 v23, 0xff

    const/16 v24, 0xff

    const/16 v25, 0xff

    invoke-static/range {v22 .. v25}, Landroid/graphics/Color;->argb(IIII)I

    move-result v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 167
    const v21, 0x7f080b35

    const/16 v22, 0xff

    const/16 v23, 0xff

    const/16 v24, 0xff

    const/16 v25, 0xff

    invoke-static/range {v22 .. v25}, Landroid/graphics/Color;->argb(IIII)I

    move-result v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 168
    const v21, 0x7f080b36

    const/16 v22, 0xff

    const/16 v23, 0xff

    const/16 v24, 0xff

    const/16 v25, 0xff

    invoke-static/range {v22 .. v25}, Landroid/graphics/Color;->argb(IIII)I

    move-result v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setTextColor(II)V

    goto/16 :goto_8

    .line 177
    :cond_18
    const v21, 0x7f080b34

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 178
    const v21, 0x7f080b35

    const/16 v22, 0x8

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_9

    .line 192
    :cond_19
    const v21, 0x7f080521

    const/16 v22, 0x8

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 193
    const v21, 0x7f080b3d

    const/16 v22, 0x8

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_a

    .line 215
    .restart local v7    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v8    # "deviceType":I
    :sswitch_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f090bcd

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v21

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v25

    move/from16 v0, v21

    move-wide/from16 v1, v25

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getTotalStepsFromDeviceType(IJ)I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v23, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 216
    .restart local v9    # "firstInfo":Ljava/lang/String;
    goto/16 :goto_b

    .line 218
    .end local v9    # "firstInfo":Ljava/lang/String;
    :sswitch_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f090bcc

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v21

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v25

    move/from16 v0, v21

    move-wide/from16 v1, v25

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getTotalStepsFromDeviceType(IJ)I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v23, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 219
    .restart local v9    # "firstInfo":Ljava/lang/String;
    goto/16 :goto_b

    .line 221
    .end local v9    # "firstInfo":Ljava/lang/String;
    :sswitch_2
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f090bce

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v21

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v25

    move/from16 v0, v21

    move-wide/from16 v1, v25

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getTotalStepsFromDeviceType(IJ)I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v23, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 222
    .restart local v9    # "firstInfo":Ljava/lang/String;
    goto/16 :goto_b

    .line 224
    .end local v9    # "firstInfo":Ljava/lang/String;
    :sswitch_3
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f090bca

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v21

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v25

    move/from16 v0, v21

    move-wide/from16 v1, v25

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getTotalStepsFromDeviceType(IJ)I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v23, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 225
    .restart local v9    # "firstInfo":Ljava/lang/String;
    goto/16 :goto_b

    .line 232
    .end local v9    # "firstInfo":Ljava/lang/String;
    :cond_1a
    const v21, 0x7f080b3b

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 233
    const v21, 0x7f080b3c

    const/16 v22, 0x8

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 234
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->getLastUpdatedTime()Ljava/lang/String;

    move-result-object v21

    const-string v22, ""

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_1b

    .line 235
    const v21, 0x7f080b3a

    const/16 v22, 0x8

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_c

    .line 237
    :cond_1b
    const v21, 0x7f080b3a

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_c

    .line 242
    .end local v7    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v8    # "deviceType":I
    :cond_1c
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    const-wide/16 v23, 0x0

    cmp-long v21, v21, v23

    if-nez v21, :cond_1d

    .line 243
    const v21, 0x7f080b36

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 244
    const v21, 0x7f080b34

    const/16 v22, 0x8

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 245
    const v21, 0x7f080b37

    const/16 v22, 0x8

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 251
    :goto_10
    const v21, 0x7f080b35

    const/16 v22, 0x8

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 252
    const v21, 0x7f080b3a

    const/16 v22, 0x8

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_c

    .line 247
    :cond_1d
    const v21, 0x7f080b36

    const/16 v22, 0x8

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 248
    const v21, 0x7f080b34

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 249
    const v21, 0x7f080b37

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_10

    .line 261
    .restart local v16    # "pendingIntentSync":Landroid/app/PendingIntent;
    :cond_1e
    const v21, 0x7f080b3a

    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;->getLaunchPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto/16 :goto_d

    .line 286
    .end local v16    # "pendingIntentSync":Landroid/app/PendingIntent;
    .restart local v15    # "pendingIntent":Landroid/app/PendingIntent;
    :cond_1f
    const v21, 0x7f080525

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v0, v1, v15}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 287
    const v21, 0x7f080b34

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v0, v1, v15}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 288
    const v21, 0x7f080b37

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v0, v1, v15}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 289
    const v21, 0x7f080b36

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v0, v1, v15}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 290
    const v21, 0x7f080b35

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v0, v1, v15}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto/16 :goto_e

    .line 298
    :cond_20
    const v21, 0x7f080b3e

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v0, v1, v15}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto/16 :goto_f

    .line 304
    .end local v4    # "appWidgetId":I
    .end local v15    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v20    # "views":Landroid/widget/RemoteViews;
    :cond_21
    return-void

    .line 212
    :sswitch_data_0
    .sparse-switch
        0x2723 -> :sswitch_2
        0x2724 -> :sswitch_0
        0x2726 -> :sswitch_1
        0x2728 -> :sswitch_0
        0x272e -> :sswitch_3
    .end sparse-switch
.end method
