.class public interface abstract Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperationsWithFilter;
.super Ljava/lang/Object;
.source "IMeasureSpecificDbOperationsWithFilter.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Contract::",
        "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations",
        "<TContract;>;"
    }
.end annotation


# virtual methods
.method public abstract getPeriodCursorWithGoalFilter(JJLjava/util/Set;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;ZIILcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;)Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/util/Set",
            "<TContract;>;",
            "Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;",
            "TContract;",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;",
            "ZII",
            "Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;",
            ")",
            "Landroid/database/Cursor;"
        }
    .end annotation
.end method
