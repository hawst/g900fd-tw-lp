.class public Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;
.super Lcom/sec/android/app/shealth/common/utils/SharedPreferencesHelper;


# static fields
.field private static final ACCESSORY_ASSET_LOADING:Ljava/lang/String; = "accessories_asset_loading"

.field private static final ACCOUNT_INFO:Ljava/lang/String; = "user_account_info"

.field private static final INITIAL_SETTING_ONGOING:Ljava/lang/String; = "intitial_setting_ongoing"

.field private static final LEGAL_CHECK:Ljava/lang/String; = "user_legal_check"

.field private static final LOCK_SCREEN_OPTION_VISIBLE:Ljava/lang/String; = "lock_screen_option_visible"

.field public static final LOCK_SCREEN_SHOWN:Ljava/lang/String; = "lock_screen_shown"

.field private static final PIN_WALLPAPER:Ljava/lang/String; = "picture_background_pin_code"

.field private static final RESTORATION_POPUP_SHOWN:Ljava/lang/String; = "restore_popup_shwon"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/utils/SharedPreferencesHelper;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public getPinWallPaper()Z
    .locals 2

    const-string/jumbo v0, "picture_background_pin_code"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->readBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getUserAccountInfo()Ljava/lang/String;
    .locals 2

    const-string/jumbo v0, "user_account_info"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->readString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUserCheckPrivacyPolicy()Z
    .locals 2

    const-string/jumbo v0, "user_legal_check"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->readBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isAccessoryAssetsLoadingRequired()Z
    .locals 2

    const-string v0, "accessories_asset_loading"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->readBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isInitialSettingOngoing()Z
    .locals 2

    const-string v0, "intitial_setting_ongoing"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->readBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public putPinWallPaper(Z)V
    .locals 1

    const-string/jumbo v0, "picture_background_pin_code"

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->writeBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public setInitialSettingOngoing(Z)V
    .locals 1

    const-string v0, "intitial_setting_ongoing"

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->writeBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public setUserAccountInfo(Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "user_account_info"

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->writeString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setUserCheckPrivacyPolicy(Z)V
    .locals 1

    const-string/jumbo v0, "user_legal_check"

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->writeBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public updateAccessoryLoadedStatus(Z)V
    .locals 1

    const-string v0, "accessories_asset_loading"

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->writeBoolean(Ljava/lang/String;Z)V

    return-void
.end method
