.class Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;
.super Ljava/lang/Thread;
.source "UvSummaryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "measuringHapticThread"
.end annotation


# instance fields
.field private isPlay:Z

.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;Z)V
    .locals 1
    .param p2, "isPlay"    # Z

    .prologue
    .line 2124
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 2122
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;->isPlay:Z

    .line 2125
    iput-boolean p2, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;->isPlay:Z

    .line 2126
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 2135
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 2136
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;->isPlay:Z

    if-eqz v0, :cond_0

    .line 2137
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSystemVibrator:Landroid/os/SystemVibrator;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$3100(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/os/SystemVibrator;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->ivt_measuring:[B

    invoke-virtual {v0, v1}, Landroid/os/SystemVibrator;->vibrateImmVibe([B)V

    .line 2138
    const-wide/16 v0, 0x7d0

    invoke-static {v0, v1}, Landroid/os/SystemClock;->sleep(J)V

    goto :goto_0

    .line 2140
    :cond_0
    return-void
.end method

.method public stopThread()V
    .locals 1

    .prologue
    .line 2129
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;->isPlay:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$measuringHapticThread;->isPlay:Z

    .line 2130
    return-void

    .line 2129
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
