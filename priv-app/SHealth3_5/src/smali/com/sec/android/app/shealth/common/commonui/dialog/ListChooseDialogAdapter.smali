.class public Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;
.super Landroid/widget/BaseAdapter;
.source "ListChooseDialogAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter$ItemHolder;
    }
.end annotation


# instance fields
.field protected descriptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mdisabledIndex:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected onDialogListItemClick:Lcom/sec/android/app/shealth/common/commonui/dialog/OnDialogListItemClick;

.field protected onSaveListener:Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 54
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "descriptions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;->mdisabledIndex:Ljava/util/ArrayList;

    .line 55
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;->items:Ljava/util/List;

    .line 56
    if-nez p2, :cond_0

    if-eqz p1, :cond_0

    .line 57
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;->descriptions:Ljava/util/List;

    .line 62
    :goto_0
    return-void

    .line 60
    :cond_0
    iput-object p2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;->descriptions:Ljava/util/List;

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;->items:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 75
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 81
    new-instance v2, Lcom/sec/android/app/shealth/common/commonui/dialog/TextItemModel;

    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;->descriptions:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v2, v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/TextItemModel;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 87
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    .line 116
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/common/commonui/dialog/TextItemModel;

    .line 117
    .local v3, "itemModel":Lcom/sec/android/app/shealth/common/commonui/dialog/TextItemModel;
    if-nez p2, :cond_1

    .line 119
    if-nez p3, :cond_0

    .line 120
    const/4 v4, 0x0

    .line 163
    :goto_0
    return-object v4

    .line 121
    :cond_0
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v6, "layout_inflater"

    invoke-virtual {v4, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 122
    .local v1, "inflater":Landroid/view/LayoutInflater;
    sget v4, Lcom/sec/android/app/shealth/common/commonui/R$layout;->textview_item:I

    invoke-virtual {v1, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 124
    new-instance v2, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter$ItemHolder;

    invoke-direct {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter$ItemHolder;-><init>()V

    .line 125
    .local v2, "itemHolder":Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter$ItemHolder;
    sget v4, Lcom/sec/android/app/shealth/common/commonui/R$id;->text:I

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v2, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter$ItemHolder;->textView:Landroid/widget/TextView;

    .line 126
    sget v4, Lcom/sec/android/app/shealth/common/commonui/R$id;->textParent:I

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    new-instance v6, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter$1;

    invoke-direct {v6, p0, p3}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter$1;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;Landroid/view/ViewGroup;)V

    invoke-virtual {v4, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 148
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    :goto_1
    sget v4, Lcom/sec/android/app/shealth/common/commonui/R$id;->btns_margin:I

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 149
    .local v0, "divider":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne p1, v4, :cond_2

    .line 151
    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 157
    :goto_2
    iget-object v4, v2, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter$ItemHolder;->textView:Landroid/widget/TextView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/TextItemModel;->getText()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/TextItemModel;->getDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 159
    invoke-virtual {p2, p1}, Landroid/view/View;->setId(I)V

    .line 161
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;->mdisabledIndex:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    const/4 v4, 0x1

    :goto_3
    invoke-virtual {p2, v4}, Landroid/view/View;->setEnabled(Z)V

    move-object v4, p2

    .line 163
    goto :goto_0

    .line 146
    .end local v0    # "divider":Landroid/view/View;
    .end local v2    # "itemHolder":Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter$ItemHolder;
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter$ItemHolder;

    .restart local v2    # "itemHolder":Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter$ItemHolder;
    goto :goto_1

    .line 155
    .restart local v0    # "divider":Landroid/view/View;
    :cond_2
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_3
    move v4, v5

    .line 161
    goto :goto_3
.end method

.method public setDisabled([I)V
    .locals 3
    .param p1, "disabledIndex"    # [I

    .prologue
    .line 96
    if-nez p1, :cond_0

    .line 109
    :goto_0
    return-void

    .line 101
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;->mdisabledIndex:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 103
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_1
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 105
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;->mdisabledIndex:Ljava/util/ArrayList;

    aget v2, p1, v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 108
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public setOnDialogListItemClick(Lcom/sec/android/app/shealth/common/commonui/dialog/OnDialogListItemClick;)V
    .locals 0
    .param p1, "onDialogListItemClick"    # Lcom/sec/android/app/shealth/common/commonui/dialog/OnDialogListItemClick;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;->onDialogListItemClick:Lcom/sec/android/app/shealth/common/commonui/dialog/OnDialogListItemClick;

    .line 51
    return-void
.end method
