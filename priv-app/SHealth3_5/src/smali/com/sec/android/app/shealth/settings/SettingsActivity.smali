.class public Lcom/sec/android/app/shealth/settings/SettingsActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "SettingsActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissControllerProvider;


# static fields
.field private static final SETTINGS_FRAGMENT:Ljava/lang/String; = "settings_fragment"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 52
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 54
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 55
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f090037

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 56
    return-void
.end method

.method public getDismissController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;
    .locals 3
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "settings_fragment"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/settings/SettingsFragment;

    .line 62
    .local v0, "settingsFragment":Lcom/sec/android/app/shealth/settings/SettingsFragment;
    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getDismissController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;

    move-result-object v1

    .line 65
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 41
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 42
    const v1, 0x7f030196

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/SettingsActivity;->setContentView(I)V

    .line 44
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SettingsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 45
    .local v0, "fragmentTransaction":Landroid/support/v4/app/FragmentTransaction;
    const v1, 0x7f0806f4

    new-instance v2, Lcom/sec/android/app/shealth/settings/SettingsFragment;

    invoke-direct {v2}, Lcom/sec/android/app/shealth/settings/SettingsFragment;-><init>()V

    const-string/jumbo v3, "settings_fragment"

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 46
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 47
    return-void
.end method
