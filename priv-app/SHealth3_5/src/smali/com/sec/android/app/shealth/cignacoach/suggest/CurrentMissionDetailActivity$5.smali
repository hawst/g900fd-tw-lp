.class Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$5;
.super Ljava/lang/Object;
.source "CurrentMissionDetailActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;)V
    .locals 0

    .prologue
    .line 597
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDateChanged()V
    .locals 2

    .prologue
    .line 613
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$5$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$5$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$5;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 619
    return-void
.end method

.method public onGoalMissionStatusChange(II)V
    .locals 2
    .param p1, "goalId"    # I
    .param p2, "missionId"    # I

    .prologue
    .line 600
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->access$000(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;)Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getId()I

    move-result v0

    if-ne v0, p2, :cond_0

    .line 601
    # getter for: Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->access$700()Ljava/lang/String;

    move-result-object v0

    const-string v1, "This mission was expired or completed~"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 602
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$5$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$5$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$5;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 610
    :cond_0
    return-void
.end method
