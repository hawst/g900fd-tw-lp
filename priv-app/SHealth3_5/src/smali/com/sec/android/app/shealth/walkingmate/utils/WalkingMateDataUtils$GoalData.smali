.class public Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$GoalData;
.super Ljava/lang/Object;
.source "WalkingMateDataUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GoalData"
.end annotation


# instance fields
.field private set_time:J

.field private value:F


# direct methods
.method public constructor <init>(FJ)V
    .locals 0
    .param p1, "value"    # F
    .param p2, "set_time"    # J

    .prologue
    .line 1157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1158
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$GoalData;->value:F

    .line 1159
    iput-wide p2, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$GoalData;->set_time:J

    .line 1160
    return-void
.end method


# virtual methods
.method public getSetTime()J
    .locals 2

    .prologue
    .line 1171
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$GoalData;->set_time:J

    return-wide v0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 1163
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$GoalData;->value:F

    return v0
.end method

.method public setSetTime(I)V
    .locals 2
    .param p1, "set_time"    # I

    .prologue
    .line 1175
    int-to-long v0, p1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$GoalData;->set_time:J

    .line 1176
    return-void
.end method

.method public setValue(F)V
    .locals 0
    .param p1, "value"    # F

    .prologue
    .line 1167
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$GoalData;->value:F

    .line 1168
    return-void
.end method
