.class public Lcom/sec/android/app/shealth/common/utils/SoftInputUtils$SoftInputAutoHideTouchListener;
.super Ljava/lang/Object;
.source "SoftInputUtils.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SoftInputAutoHideTouchListener"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentFocus:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "currentFocus"    # Landroid/view/View;

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils$SoftInputAutoHideTouchListener;->mContext:Landroid/content/Context;

    .line 125
    iput-object p2, p0, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils$SoftInputAutoHideTouchListener;->mCurrentFocus:Landroid/view/View;

    .line 126
    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils$SoftInputAutoHideTouchListener;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils$SoftInputAutoHideTouchListener;->mCurrentFocus:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->hideSoftInput(Landroid/content/Context;Landroid/view/View;)V

    .line 131
    const/4 v0, 0x0

    return v0
.end method
