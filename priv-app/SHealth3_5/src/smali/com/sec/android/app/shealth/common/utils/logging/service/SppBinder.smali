.class public Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;
.super Ljava/lang/Object;
.source "SppBinder.java"


# static fields
.field private static final LOG_COLLECTOR_PACKAGE:Ljava/lang/String; = "com.sec.spp.push"

.field private static final LOG_COLLECTOR_SERVICE:Ljava/lang/String; = "com.sec.spp.push.dlc.writer.WriterService"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mConnection:Landroid/content/ServiceConnection;

.field private final mHandler:Landroid/os/Handler;

.field private mIDlcService:Lcom/sec/spp/push/dlc/api/IDlcService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;)V
    .locals 2
    .param p1, "handler"    # Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->mIDlcService:Lcom/sec/spp/push/dlc/api/IDlcService;

    .line 79
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder$1;-><init>(Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->mConnection:Landroid/content/ServiceConnection;

    .line 48
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->TAG:Ljava/lang/String;

    const-string v1, "Construct SPPBinder"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->mHandler:Landroid/os/Handler;

    .line 50
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;Lcom/sec/spp/push/dlc/api/IDlcService;)Lcom/sec/spp/push/dlc/api/IDlcService;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;
    .param p1, "x1"    # Lcom/sec/spp/push/dlc/api/IDlcService;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->mIDlcService:Lcom/sec/spp/push/dlc/api/IDlcService;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private isServiceConnected()Z
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->mIDlcService:Lcom/sec/spp/push/dlc/api/IDlcService;

    if-nez v0, :cond_0

    .line 105
    const/4 v0, 0x0

    .line 107
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public bindService(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    sget-object v2, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->TAG:Ljava/lang/String;

    const-string v3, "Bind Service"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 55
    .local v1, "service":Landroid/content/Intent;
    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.sec.spp.push"

    const-string v4, "com.sec.spp.push.dlc.writer.WriterService"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 56
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->mConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {p1, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    .line 58
    .local v0, "result":Z
    if-nez v0, :cond_0

    .line 59
    sget-object v2, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->TAG:Ljava/lang/String;

    const-string v3, "Bind to DLC is failed."

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 62
    :cond_0
    return-void
.end method

.method public onDestroy(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->unbindService(Landroid/content/Context;)V

    .line 45
    return-void
.end method

.method public sendLog(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 11
    .param p1, "category"    # Ljava/lang/String;
    .param p2, "svcCode"    # Ljava/lang/String;
    .param p3, "timeStamp"    # J
    .param p5, "activity"    # Ljava/lang/String;
    .param p6, "deviceId"    # Ljava/lang/String;
    .param p7, "userId"    # Ljava/lang/String;
    .param p8, "appVersion"    # Ljava/lang/String;
    .param p9, "body"    # Ljava/lang/String;

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->isServiceConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 116
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->mIDlcService:Lcom/sec/spp/push/dlc/api/IDlcService;

    if-nez v0, :cond_0

    .line 117
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->TAG:Ljava/lang/String;

    const-string v1, "IDlcService is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    const/4 v0, 0x0

    .line 128
    :goto_0
    return v0

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->mIDlcService:Lcom/sec/spp/push/dlc/api/IDlcService;

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-interface/range {v0 .. v9}, Lcom/sec/spp/push/dlc/api/IDlcService;->requestSend(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 121
    :catch_0
    move-exception v10

    .line 122
    .local v10, "e":Ljava/lang/Exception;
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    .line 123
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    .end local v10    # "e":Ljava/lang/Exception;
    :goto_1
    const/16 v0, -0x3e8

    goto :goto_0

    .line 126
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->TAG:Ljava/lang/String;

    const-string v1, "IDlc service is not connected"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public unbindService(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->TAG:Ljava/lang/String;

    const-string v1, "Unbind Service"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->mIDlcService:Lcom/sec/spp/push/dlc/api/IDlcService;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->mIDlcService:Lcom/sec/spp/push/dlc/api/IDlcService;

    .line 69
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->TAG:Ljava/lang/String;

    const-string v1, "SPPBinder is disconnected"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 71
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->TAG:Ljava/lang/String;

    const-string v1, "ConMsgHandler is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    :cond_0
    :goto_0
    return-void

    .line 74
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
