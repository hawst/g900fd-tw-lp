.class public Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;
.super Lcom/sec/android/app/shealth/stress/state/StressState;
.source "StressStateMeasuring.java"


# static fields
.field private static final CURRENT_STATE_TIMEOUT:J = 0x4fb0L

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isInCurrentState:Z

.field private mCurrentStateTimer:Landroid/os/CountDownTimer;

.field private sendedStartLogging:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V
    .locals 6
    .param p1, "summaryFragment"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/stress/state/StressState;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V

    .line 16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->sendedStartLogging:Z

    .line 83
    new-instance v0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring$1;

    const-wide/16 v2, 0x4fb0

    const-wide/16 v4, 0xc8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring$1;-><init>(Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;JJ)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->mCurrentStateTimer:Landroid/os/CountDownTimer;

    .line 23
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;

    .prologue
    .line 11
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->isInCurrentState:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;
    .param p1, "x1"    # Z

    .prologue
    .line 11
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->isInCurrentState:Z

    return p1
.end method


# virtual methods
.method public clearForNextState()V
    .locals 2

    .prologue
    .line 78
    sget-object v0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->TAG:Ljava/lang/String;

    const-string v1, "clearForNextState()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->isInCurrentState:Z

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->mCurrentStateTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 81
    return-void
.end method

.method public getSendedStartLogging()Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->sendedStartLogging:Z

    return v0
.end method

.method public reStartStates()V
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getStateReady()Lcom/sec/android/app/shealth/stress/state/StressState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->setState(Lcom/sec/android/app/shealth/stress/state/StressState;)V

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getStateReady()Lcom/sec/android/app/shealth/stress/state/StressState;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/state/StressState;->updateViewWithAnimation(Z)V

    .line 37
    return-void
.end method

.method public setSendedStartLogging(Z)V
    .locals 0
    .param p1, "sendedStartLogging"    # Z

    .prologue
    .line 26
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->sendedStartLogging:Z

    .line 27
    return-void
.end method

.method public terminateState()V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->stopSensor()V

    .line 107
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->clearForNextState()V

    .line 108
    return-void
.end method

.method public updateNextState(II)V
    .locals 5
    .param p1, "rate"    # I
    .param p2, "interval"    # I

    .prologue
    const/4 v4, 0x0

    .line 41
    sget-object v1, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateNextState() rate: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " interval : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget v1, v1, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->sumRRI:I

    const/16 v2, 0x7530

    if-lt v1, v2, :cond_1

    .line 43
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->dataCollection:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->dataCollectionSet(Ljava/util/ArrayList;)V

    .line 44
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->terminateState()V

    .line 45
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getStateFinished()Lcom/sec/android/app/shealth/stress/state/StressState;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->setState(Lcom/sec/android/app/shealth/stress/state/StressState;)V

    .line 46
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getStateFinished()Lcom/sec/android/app/shealth/stress/state/StressState;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/stress/state/StressState;->updateViewWithAnimation(Z)V

    .line 47
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->measureFinishAnimation()V

    .line 65
    :cond_0
    :goto_0
    return-void

    .line 48
    :cond_1
    if-lez p1, :cond_2

    if-lez p2, :cond_2

    .line 49
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->dataCollection:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 50
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 51
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x5

    iput v1, v0, Landroid/os/Message;->what:I

    .line 52
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 53
    iput p2, v0, Landroid/os/Message;->arg2:I

    .line 54
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 55
    .end local v0    # "msg":Landroid/os/Message;
    :cond_2
    const/4 v1, -0x1

    if-ge p1, v1, :cond_0

    .line 56
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->clearForNextState()V

    .line 57
    const/4 v1, -0x6

    if-ne p1, v1, :cond_3

    .line 58
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getStateMeasureFailed()Lcom/sec/android/app/shealth/stress/state/StressState;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->setState(Lcom/sec/android/app/shealth/stress/state/StressState;)V

    .line 59
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getStateMeasureFailed()Lcom/sec/android/app/shealth/stress/state/StressState;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/stress/state/StressState;->updateViewWithAnimation(Z)V

    goto :goto_0

    .line 60
    :cond_3
    const/16 v1, -0x9

    if-lt p1, v1, :cond_0

    .line 61
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getStateMeasureWarning()Lcom/sec/android/app/shealth/stress/state/StressState;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->setState(Lcom/sec/android/app/shealth/stress/state/StressState;)V

    .line 62
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getStateMeasureWarning()Lcom/sec/android/app/shealth/stress/state/StressState;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/stress/state/StressState;->updateViewWithAnimation(Z)V

    goto :goto_0
.end method

.method public updateViewWithAnimation(Z)V
    .locals 2
    .param p1, "hasAnimation"    # Z

    .prologue
    const/4 v1, 0x1

    .line 69
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->isInCurrentState:Z

    .line 70
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 71
    .local v0, "msg":Landroid/os/Message;
    iput v1, v0, Landroid/os/Message;->what:I

    .line 72
    if-eqz p1, :cond_0

    const/4 v1, 0x0

    :cond_0
    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 73
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 74
    return-void
.end method
