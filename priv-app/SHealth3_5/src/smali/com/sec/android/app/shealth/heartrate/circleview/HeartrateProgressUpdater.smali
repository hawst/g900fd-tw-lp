.class public abstract Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;
.super Ljava/lang/Object;
.source "HeartrateProgressUpdater.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$TimerHandler;
    }
.end annotation


# instance fields
.field private counter:I

.field private delay:I

.field private handler:Landroid/os/Handler;

.field private isRun:Z

.field private messagesCount:I


# direct methods
.method protected constructor <init>(II)V
    .locals 1
    .param p1, "delay"    # I
    .param p2, "messagesCount"    # I

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->counter:I

    .line 29
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->isRun:Z

    .line 32
    iput p1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->delay:I

    .line 33
    iput p2, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->messagesCount:I

    .line 34
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->isRun:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    .prologue
    .line 24
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->counter:I

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;
    .param p1, "x1"    # I

    .prologue
    .line 24
    iput p1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->counter:I

    return p1
.end method

.method static synthetic access$208(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    .prologue
    .line 24
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->counter:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->counter:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    .prologue
    .line 24
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->messagesCount:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    .prologue
    .line 24
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->delay:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->handler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public isRun()Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->isRun:Z

    return v0
.end method

.method protected abstract onProgressUpdate(ZF)V
.end method

.method public start()V
    .locals 2

    .prologue
    .line 41
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$TimerHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$TimerHandler;-><init>(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->handler:Landroid/os/Handler;

    .line 42
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;-><init>(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 78
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 81
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->isRun:Z

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 83
    return-void
.end method
