.class public abstract Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;
.source "CustomGraphFragment.java"


# static fields
.field private static final CHART_CACHE_IMAGE_PART_DENOMINATOR:I = 0x8

.field private static final LEGEND_LAYOUT_VIEW_NUMBER:I = 0x4

.field public static final SUMMARY_VIEW_DATE:Ljava/lang/String; = "SUMMARY_VIEW_DATE"


# instance fields
.field private mChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

.field private mContentObserver:Landroid/database/ContentObserver;

.field protected mDataWasChanged:Z

.field private mHandlerListener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

.field private mHandlerUpdateDataManager:Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;

.field private mIsGraphTapped:Z

.field private mMemoryCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mXAxisCount:I

.field private mYAxisCount:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 59
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;-><init>()V

    .line 68
    new-instance v0, Lcom/sec/android/app/shealth/logutils/graph/data/DefaultHandlerUpdateDataManager;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/DefaultHandlerUpdateDataManager;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mHandlerUpdateDataManager:Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;

    .line 82
    iput v1, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mXAxisCount:I

    .line 83
    iput v1, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mYAxisCount:I

    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mReadyToShown:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;)Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mHandlerUpdateDataManager:Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;

    return-object v0
.end method

.method private addLegendMark(Lcom/sec/android/app/shealth/logutils/graph/LegendMark;Landroid/widget/LinearLayout;Landroid/widget/LinearLayout;)V
    .locals 4
    .param p1, "legendMark"    # Lcom/sec/android/app/shealth/logutils/graph/LegendMark;
    .param p2, "bottomLegendLayout"    # Landroid/widget/LinearLayout;
    .param p3, "topLegendLayout"    # Landroid/widget/LinearLayout;

    .prologue
    .line 178
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/sec/android/app/shealth/logutils/graph/LegendMark;->createLegendMarkTextView(Landroid/content/Context;)Landroid/widget/TextView;

    move-result-object v1

    .line 179
    .local v1, "textMarkView":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/sec/android/app/shealth/logutils/graph/LegendMark;->createLegendMarkImageView(Landroid/content/Context;)Landroid/widget/ImageView;

    move-result-object v0

    .line 180
    .local v0, "imageMarkView":Landroid/widget/ImageView;
    invoke-virtual {p3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    .line 181
    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 182
    invoke-virtual {p2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 187
    :goto_0
    return-void

    .line 184
    :cond_0
    invoke-virtual {p3, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 185
    invoke-virtual {p3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method private getContentObserver()Landroid/database/ContentObserver;
    .locals 2

    .prologue
    .line 624
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mContentObserver:Landroid/database/ContentObserver;

    if-nez v0, :cond_0

    .line 625
    new-instance v0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment$4;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment$4;-><init>(Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mContentObserver:Landroid/database/ContentObserver;

    .line 635
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mContentObserver:Landroid/database/ContentObserver;

    return-object v0
.end method

.method private resetHandlerListener()V
    .locals 2

    .prologue
    .line 597
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    if-eqz v1, :cond_0

    .line 598
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v0

    .line 599
    .local v0, "chartStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerListener(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;)V

    .line 600
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 602
    .end local v0    # "chartStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract createDataSet(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;
.end method

.method protected createDefaultChartStyle(IILcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .locals 8
    .param p1, "xAxisMarkCount"    # I
    .param p2, "yAxisMarkCount"    # I
    .param p3, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 509
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/logutils/R$drawable;->s_health_graph_handler:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getCachedBitmapFromResource(I)Landroid/graphics/Bitmap;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/logutils/R$drawable;->s_health_graph_default_bg:I

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getCachedBitmapFromResource(I)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getHandlerListener()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getYAxisLabelTitle()Ljava/lang/String;

    move-result-object v7

    move v4, p1

    move v5, p2

    move-object v6, p3

    invoke-static/range {v0 .. v7}, Lcom/sec/android/app/shealth/logutils/graph/GraphFragmentUtils;->getDefaultChartStyle(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;IILcom/sec/android/app/shealth/framework/ui/graph/PeriodH;Ljava/lang/String;)Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v0

    return-object v0
.end method

.method protected customizeChartInteraction(Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;)V
    .locals 0
    .param p1, "chartInteraction"    # Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;

    .prologue
    .line 149
    return-void
.end method

.method protected customizeChartStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V
    .locals 0
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    .prologue
    .line 140
    return-void
.end method

.method protected customizeHandlerItemWhenVisible(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V
    .locals 0
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    .prologue
    .line 591
    return-void
.end method

.method protected getAvgDaoContract(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    .locals 1
    .param p1, "baseContract"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    .param p2, "avgContract"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    .param p3, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 391
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-eq p3, v0, :cond_0

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p3, v0, :cond_1

    :cond_0
    move-object p1, p2

    .line 394
    .end local p1    # "baseContract":Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    :cond_1
    return-object p1
.end method

.method protected getCachedBitmapFromResource(I)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "drawableId"    # I

    .prologue
    .line 405
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mMemoryCache:Landroid/util/LruCache;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 406
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 407
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 408
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mMemoryCache:Landroid/util/LruCache;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410
    :cond_0
    return-object v0
.end method

.method protected getCandleTimeSeries(Ljava/util/Set;Ljava/util/Set;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;I)Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartCandleTimeSeries;
    .locals 9
    .param p3, "timeContract"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    .param p4, "countContract"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    .param p5, "dao"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;
    .param p6, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .param p7, "seriesId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
            ">;",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;",
            "Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;",
            "I)",
            "Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartCandleTimeSeries;"
        }
    .end annotation

    .prologue
    .line 318
    .local p1, "contract":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;>;"
    .local p2, "extractContract":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mHandlerUpdateDataManager:Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v8

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move/from16 v7, p7

    invoke-static/range {v0 .. v8}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->getCandleTimeSeries(Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;Ljava/util/Set;Ljava/util/Set;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;ILandroid/os/Bundle;)Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartCandleTimeSeries;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getContentUriList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end method

.method protected getDataSeriesList(Ljava/util/Set;Ljava/util/Set;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;Ljava/lang/String;Ljava/lang/Object;Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IValueConversionProvider;I)Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;
    .locals 12
    .param p3, "timeContract"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    .param p4, "countContract"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    .param p5, "dao"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;
    .param p6, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .param p7, "additionalColumn"    # Ljava/lang/String;
    .param p8, "additionalValue"    # Ljava/lang/Object;
    .param p9, "valueConversionProvider"    # Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IValueConversionProvider;
    .param p10, "seriesId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
            ">;",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;",
            "Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IValueConversionProvider;",
            "I)",
            "Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;"
        }
    .end annotation

    .prologue
    .line 290
    .local p1, "contract":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;>;"
    .local p2, "extractContract":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mHandlerUpdateDataManager:Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v11

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move/from16 v10, p10

    invoke-static/range {v0 .. v11}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->getDataSeriesList(Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;Ljava/util/Set;Ljava/util/Set;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;Ljava/lang/String;Ljava/lang/Object;Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IValueConversionProvider;ILandroid/os/Bundle;)Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    move-result-object v0

    return-object v0
.end method

.method public getGraphSwitchButton()Landroid/view/View;
    .locals 3

    .prologue
    .line 375
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/logutils/R$id;->graph_triangular_switch_fragment_button:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 377
    .local v0, "legendButton":Landroid/widget/ImageButton;
    return-object v0
.end method

.method protected getHandlerListener()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;
    .locals 1

    .prologue
    .line 534
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mHandlerListener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    if-nez v0, :cond_0

    .line 535
    new-instance v0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment$3;-><init>(Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mHandlerListener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    .line 575
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mHandlerListener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    return-object v0
.end method

.method public getHandlerUpdateDataManager()Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mHandlerUpdateDataManager:Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;

    return-object v0
.end method

.method protected abstract getInfoView()Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;
.end method

.method protected abstract getLegendButtonBackgroundResourceId()I
.end method

.method protected abstract getLegendMarks()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/logutils/graph/LegendMark;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract getNoDataIcon()I
.end method

.method protected getTimeSeriesList(Ljava/util/Set;Ljava/util/Set;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;Ljava/lang/String;Ljava/lang/Object;Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IValueConversionProvider;I)Ljava/util/List;
    .locals 12
    .param p3, "timeContract"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    .param p4, "countContract"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    .param p5, "dao"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;
    .param p6, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .param p7, "additionalColumn"    # Ljava/lang/String;
    .param p8, "additionalValue"    # Ljava/lang/Object;
    .param p9, "valueConversionProvider"    # Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IValueConversionProvider;
    .param p10, "seriesId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
            ">;",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;",
            "Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IValueConversionProvider;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;",
            ">;"
        }
    .end annotation

    .prologue
    .line 274
    .local p1, "contract":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;>;"
    .local p2, "extractContract":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mHandlerUpdateDataManager:Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v11

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move/from16 v10, p10

    invoke-static/range {v0 .. v11}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->getTimeSeriesList(Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;Ljava/util/Set;Ljava/util/Set;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;Ljava/lang/String;Ljava/lang/Object;Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IValueConversionProvider;ILandroid/os/Bundle;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getYAxisLabelTitle()Ljava/lang/String;
.end method

.method protected initGeneralView()V
    .locals 0

    .prologue
    .line 524
    invoke-direct {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->resetHandlerListener()V

    .line 525
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->initGeneralView()V

    .line 526
    return-void
.end method

.method protected initGraphLegendArea()Landroid/view/View;
    .locals 9

    .prologue
    .line 334
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    sget v7, Lcom/sec/android/app/shealth/logutils/R$layout;->legend_area_with_triangular_button_decoration:I

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 337
    .local v3, "legendLayout":Landroid/widget/LinearLayout;
    sget v6, Lcom/sec/android/app/shealth/logutils/R$id;->first_legend_mark_layout:I

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    .line 339
    .local v5, "topLegendMarkLayout":Landroid/widget/LinearLayout;
    sget v6, Lcom/sec/android/app/shealth/logutils/R$id;->second_legend_mark_layout:I

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 344
    .local v0, "bottomLegendMarkLayout":Landroid/widget/LinearLayout;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->isNoDataGraph()Z

    move-result v6

    if-nez v6, :cond_0

    .line 346
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getLegendMarks()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/logutils/graph/LegendMark;

    .line 348
    .local v4, "legendMark":Lcom/sec/android/app/shealth/logutils/graph/LegendMark;
    invoke-direct {p0, v4, v0, v5}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->addLegendMark(Lcom/sec/android/app/shealth/logutils/graph/LegendMark;Landroid/widget/LinearLayout;Landroid/widget/LinearLayout;)V

    goto :goto_0

    .line 352
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v4    # "legendMark":Lcom/sec/android/app/shealth/logutils/graph/LegendMark;
    :cond_0
    sget v6, Lcom/sec/android/app/shealth/logutils/R$id;->graph_triangular_switch_fragment_button:I

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    .line 354
    .local v2, "legendButton":Landroid/widget/ImageButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    iget v6, v6, Landroid/content/res/Configuration;->orientation:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_2

    .line 355
    const/16 v6, 0x8

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 360
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getLegendButtonBackgroundResourceId()I

    move-result v6

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 362
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->isNoDataGraph()Z

    move-result v6

    if-nez v6, :cond_1

    .line 365
    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 367
    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 370
    :cond_1
    return-object v3

    .line 357
    :cond_2
    new-instance v7, Lcom/sec/android/app/shealth/logutils/graph/GraphLogUtils$GraphToSummaryFragmentSwitchController;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/logutils/graph/FragmentSwitchable;

    invoke-direct {v7, v6}, Lcom/sec/android/app/shealth/logutils/graph/GraphLogUtils$GraphToSummaryFragmentSwitchController;-><init>(Lcom/sec/android/app/shealth/logutils/graph/FragmentSwitchable;)V

    invoke-virtual {v2, v7}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method protected initGraphView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
    .locals 9
    .param p1, "periodStart"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 442
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mIsGraphTapped:Z

    .line 443
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getInfoView()Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 444
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getInfoView()Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;->setDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 446
    :cond_0
    iget v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mXAxisCount:I

    iget v1, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mYAxisCount:I

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->setXYAxisMarkCount(II)V

    .line 447
    iget v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mXAxisCount:I

    iget v1, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mYAxisCount:I

    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->createDefaultChartStyle(IILcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v7

    .line 450
    .local v7, "chartStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->customizeChartStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V

    .line 451
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mHandlerUpdateDataManager:Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;

    const-wide/16 v1, 0x0

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;->setHandlerTime(J)V

    .line 452
    invoke-virtual {p0, p3}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->createDataSet(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;

    move-result-object v8

    .line 453
    .local v8, "dataSet":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;
    invoke-virtual {p0, v7, v8}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->initSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;)V

    .line 455
    invoke-virtual {p0, p3}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->updateLatestMeasureInHourTimeMap(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 456
    new-instance v0, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1, v7, v8}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/samsung/android/sdk/chart/series/SchartDataSet;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    .line 458
    invoke-static {p3}, Lcom/sec/android/app/shealth/logutils/graph/GraphFragmentUtils;->getChartInteraction(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;

    move-result-object v6

    .line 459
    .local v6, "chartInteraction":Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->customizeChartInteraction(Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;)V

    .line 460
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setChartInteraction(Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;)V

    .line 462
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    new-instance v1, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment$2;-><init>(Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setReadyToShowListener(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartReadyToShowListener;)V

    .line 477
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 478
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getNoDataIcon()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->showNoData(I)V

    .line 479
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 488
    :goto_0
    return-object v0

    .line 481
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mHandlerUpdateDataManager:Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;

    invoke-interface {v1}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;->getHandlerTime()J

    move-result-wide v1

    long-to-double v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setHandlerStartDate(D)V

    .line 482
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    invoke-static {p3}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getGraphParams(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    move-result-object v1

    iget v1, v1, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->timeDepthLevel:I

    iget-object v2, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mHandlerUpdateDataManager:Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;

    invoke-interface {v2}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;->getHandlerTime()J

    move-result-wide v2

    invoke-static {v2, v3, p3}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->getGraphStartTime(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)J

    move-result-wide v2

    long-to-double v2, v2

    invoke-static {p3}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getGraphParams(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getHorizontalMarksInterval()I

    move-result v4

    invoke-static {p3}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getGraphParams(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getHorizontalMarksCount()I

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setStartVisual(IDII)V

    .line 486
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->setAdditionalChartViewArguments(Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;)V

    .line 488
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    goto :goto_0
.end method

.method protected abstract initSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;)V
.end method

.method public isGraphTapped()Z
    .locals 1

    .prologue
    .line 427
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mIsGraphTapped:Z

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 236
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 238
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 158
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->onAttach(Landroid/app/Activity;)V

    .line 160
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getContentUriList()Ljava/util/List;

    move-result-object v2

    .line 161
    .local v2, "uriList":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    if-eqz v2, :cond_0

    .line 162
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 163
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->registerContentObserver(Landroid/net/Uri;)V

    goto :goto_0

    .line 166
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 191
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->onCreate(Landroid/os/Bundle;)V

    .line 192
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->setHasOptionsMenu(Z)V

    .line 193
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 197
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 198
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 202
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v1

    long-to-int v0, v1

    .line 203
    .local v0, "maxMemory":I
    new-instance v1, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment$1;

    div-int/lit8 v2, v0, 0x8

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment$1;-><init>(Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;I)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mMemoryCache:Landroid/util/LruCache;

    .line 210
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    .line 415
    iget-object v3, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mMemoryCache:Landroid/util/LruCache;

    invoke-virtual {v3}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v2

    .line 416
    .local v2, "snapshot":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Landroid/graphics/Bitmap;>;"
    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 417
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 419
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mMemoryCache:Landroid/util/LruCache;

    invoke-virtual {v3}, Landroid/util/LruCache;->evictAll()V

    .line 420
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->onDestroy()V

    .line 421
    return-void
.end method

.method public onDetach()V
    .locals 0

    .prologue
    .line 517
    invoke-direct {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->resetHandlerListener()V

    .line 518
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->unregisterContentObserver()V

    .line 519
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->onDetach()V

    .line 520
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 242
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->onResume()V

    .line 243
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mIsGraphTapped:Z

    .line 245
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mDataWasChanged:Z

    if-eqz v0, :cond_0

    .line 246
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->updateGraphFragment()V

    .line 247
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mDataWasChanged:Z

    .line 249
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 494
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->onStop()V

    .line 495
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mIsGraphTapped:Z

    .line 496
    return-void
.end method

.method protected registerContentObserver(Landroid/net/Uri;)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 611
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getContentObserver()Landroid/database/ContentObserver;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 612
    return-void
.end method

.method protected abstract setAdditionalChartViewArguments(Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;)V
.end method

.method protected setXYAxisMarkCount(II)V
    .locals 0
    .param p1, "xCount"    # I
    .param p2, "yCount"    # I

    .prologue
    .line 152
    iput p1, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mXAxisCount:I

    .line 153
    iput p2, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mYAxisCount:I

    .line 154
    return-void
.end method

.method protected unregisterContentObserver()V
    .locals 2

    .prologue
    .line 618
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mContentObserver:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 619
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 621
    :cond_0
    return-void
.end method

.method public updateGraphFragment()V
    .locals 0

    .prologue
    .line 328
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->initGeneralView()V

    .line 329
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->initInformationArea()V

    .line 330
    return-void
.end method

.method public updateLatestMeasureInHourTimeMap(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;Ljava/lang/String;Ljava/util/List;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;)V
    .locals 1
    .param p1, "dao"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;
    .param p2, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .param p3, "additionalColumn"    # Ljava/lang/String;
    .param p5, "maxTimeContract"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;",
            "Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
            ")V"
        }
    .end annotation

    .prologue
    .line 228
    .local p4, "additionalValueList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p2, v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mHandlerUpdateDataManager:Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;

    invoke-static {p1, p3, p4, v0, p5}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->updateLatestMeasureTimeInHour(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;Ljava/lang/String;Ljava/util/List;Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;)V

    .line 232
    :cond_0
    return-void
.end method

.method protected abstract updateLatestMeasureInHourTimeMap(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
.end method
