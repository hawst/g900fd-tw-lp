.class public Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;
.super Ljava/lang/Object;
.source "AssessmentClickUtils.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static sInstance:Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;


# instance fields
.field private mClickCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;->sInstance:Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;->sInstance:Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;

    .line 15
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;->sInstance:Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;

    return-object v0
.end method


# virtual methods
.method public addClickCount()V
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;->mClickCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;->mClickCount:I

    .line 23
    return-void
.end method

.method public clearClickCount()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;->mClickCount:I

    .line 31
    return-void
.end method

.method public getClickCount()I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;->mClickCount:I

    return v0
.end method
