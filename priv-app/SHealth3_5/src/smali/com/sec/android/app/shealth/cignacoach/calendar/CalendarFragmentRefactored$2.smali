.class Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$2;
.super Ljava/lang/Object;
.source "CalendarFragmentRefactored.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->checkIfDataExists(Ljava/util/Map;Ljava/util/TreeMap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;

.field final synthetic val$c:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;

.field final synthetic val$d:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;)V
    .locals 0

    .prologue
    .line 883
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;

    iput-object p2, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$2;->val$d:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;

    iput-object p3, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$2;->val$c:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 887
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 889
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$2;->val$d:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;->getMedalResourceId()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 891
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$2;->val$d:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;->getMedalResourceId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 892
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    .line 894
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$2;->val$c:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$2;->val$d:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;->getMedalResourceId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;->setMedalImage(Landroid/graphics/drawable/Drawable;)V

    .line 895
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$2;->val$c:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;->setMedalState(Z)V

    .line 899
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    return-void
.end method
