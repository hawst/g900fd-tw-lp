.class Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;
.super Lcom/sec/android/app/shealth/common/utils/TextWatcherStub;
.source "FloatEditText.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FloatEditTextWatcher"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)V
    .locals 0

    .prologue
    .line 309
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/TextWatcherStub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$1;

    .prologue
    .line 309
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;-><init>(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 8
    .param p1, "editable"    # Landroid/text/Editable;

    .prologue
    const/4 v7, -0x1

    .line 313
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v5

    if-nez v5, :cond_1

    .line 314
    iget-object v5, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    const/4 v6, 0x0

    # setter for: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mInputValue:Ljava/lang/Float;
    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$502(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;Ljava/lang/Float;)Ljava/lang/Float;

    .line 315
    iget-object v5, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->informClientAboutValue()V
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$600(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)V

    .line 369
    :cond_0
    :goto_0
    return-void

    .line 319
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mShouldMissTextWatcher:Z
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$700(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 320
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v5

    invoke-static {p1, v5}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 321
    iget-object v5, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    const/4 v6, 0x0

    # setter for: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mShouldMissTextWatcher:Z
    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$702(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;Z)Z

    goto :goto_0

    .line 325
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 327
    .local v0, "changeString":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mSystemNumberSeparator:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$200(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 328
    const-string v5, "."

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 329
    iget-object v5, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    iget-object v6, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mMinValue:F
    invoke-static {v6}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$800(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)F

    move-result v6

    # invokes: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->changeToValue(F)V
    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$900(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;F)V

    goto :goto_0

    .line 334
    :cond_3
    move-object v4, v0

    .line 335
    .local v4, "value":Ljava/lang/String;
    const/4 v3, 0x0

    .line 336
    .local v3, "start":I
    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-ne v5, v7, :cond_5

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    .line 339
    .local v1, "end":I
    :goto_1
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v1, v5, :cond_4

    .line 340
    invoke-virtual {v4, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 342
    :cond_4
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    .line 343
    .local v2, "inputValue":F
    iget-object v5, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->isFirstZeroStringWrong(Ljava/lang/String;)Z
    invoke-static {v5, v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$1000(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 344
    iget-object v5, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v5, v2}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setFloatValue(F)V

    .line 345
    iget-object v5, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    iget-object v6, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-interface {v6}, Landroid/text/Editable;->length()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setSelection(I)V

    goto :goto_0

    .line 336
    .end local v1    # "end":I
    .end local v2    # "inputValue":F
    :cond_5
    const-string v5, "."

    const-string v6, "."

    invoke-virtual {v4, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v5

    if-ne v5, v7, :cond_6

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_1

    :cond_6
    const-string v5, "."

    const-string v6, "."

    invoke-virtual {v4, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    goto :goto_1

    .line 349
    .restart local v1    # "end":I
    .restart local v2    # "inputValue":F
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mMaxValue:F
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$1100(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)F

    move-result v5

    invoke-static {v2, v5}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->compare(FF)I

    move-result v5

    if-lez v5, :cond_8

    .line 350
    iget-object v5, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    iget-object v6, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mMaxValue:F
    invoke-static {v6}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$1100(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)F

    move-result v6

    # invokes: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->changeToValue(F)V
    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$900(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;F)V

    goto/16 :goto_0

    .line 354
    :cond_8
    iget-object v5, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mMinValue:F
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$800(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)F

    move-result v5

    invoke-static {v2, v5}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->compare(FF)I

    move-result v5

    if-gez v5, :cond_a

    .line 355
    iget-object v5, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->isNeedToReplaceWithMinRangeValue(FLjava/lang/String;)Z
    invoke-static {v5, v2, v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$1200(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;FLjava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 356
    iget-object v5, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    iget-object v6, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mMinValue:F
    invoke-static {v6}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$800(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)F

    move-result v6

    # invokes: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->changeToValue(F)V
    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$900(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;F)V

    goto/16 :goto_0

    .line 359
    :cond_9
    iget-object v5, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setValueIfChanged(F)Z
    invoke-static {v5, v2}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$1300(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;F)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 360
    iget-object v5, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->informClientAboutValue()V
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$600(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)V

    goto/16 :goto_0

    .line 364
    :cond_a
    iget-object v5, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setValueIfChanged(F)Z
    invoke-static {v5, v2}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$1300(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;F)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 365
    iget-object v5, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->informClientAboutUpdate()V
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$1400(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)V

    .line 366
    iget-object v5, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->informClientAboutValue()V
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$600(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)V

    goto/16 :goto_0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 374
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mSystemNumberSeparator:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$200(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)Ljava/lang/String;

    move-result-object v0

    .line 375
    .local v0, "systemNumberSeparator":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mLanguage:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$1500(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "ar"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 376
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    const-string v2, "."

    # setter for: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mSystemNumberSeparator:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$202(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;Ljava/lang/String;)Ljava/lang/String;

    .line 381
    :goto_0
    return-void

    .line 379
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextWatcher;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    # setter for: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mSystemNumberSeparator:Ljava/lang/String;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$202(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method
