.class Lcom/sec/android/app/shealth/stress/StressActivity$16;
.super Ljava/lang/Object;
.source "StressActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/stress/StressActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/stress/StressActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/stress/StressActivity;)V
    .locals 0

    .prologue
    .line 790
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressActivity$16;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/app/Activity;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;

    .prologue
    const/4 v2, 0x0

    .line 793
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity$16;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    # setter for: Lcom/sec/android/app/shealth/stress/StressActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/stress/StressActivity;->access$1302(Lcom/sec/android/app/shealth/stress/StressActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 794
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity$16;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity$16;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    # getter for: Lcom/sec/android/app/shealth/stress/StressActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/StressActivity;->access$700(Lcom/sec/android/app/shealth/stress/StressActivity;)Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/StressActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 795
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity$16;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    # getter for: Lcom/sec/android/app/shealth/stress/StressActivity;->callFromHome:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressActivity;->access$1500(Lcom/sec/android/app/shealth/stress/StressActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 796
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity$16;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    # getter for: Lcom/sec/android/app/shealth/stress/StressActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressActivity;->access$700(Lcom/sec/android/app/shealth/stress/StressActivity;)Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getState()Lcom/sec/android/app/shealth/stress/state/StressState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/state/StressState;->reStartStates()V

    .line 797
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity$16;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    # getter for: Lcom/sec/android/app/shealth/stress/StressActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressActivity;->access$1600(Lcom/sec/android/app/shealth/stress/StressActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 798
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity$16;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    # getter for: Lcom/sec/android/app/shealth/stress/StressActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressActivity;->access$1700(Lcom/sec/android/app/shealth/stress/StressActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 799
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity$16;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    # setter for: Lcom/sec/android/app/shealth/stress/StressActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/stress/StressActivity;->access$1602(Lcom/sec/android/app/shealth/stress/StressActivity;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 800
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity$16;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    # setter for: Lcom/sec/android/app/shealth/stress/StressActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/stress/StressActivity;->access$1702(Lcom/sec/android/app/shealth/stress/StressActivity;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 801
    return-void
.end method
