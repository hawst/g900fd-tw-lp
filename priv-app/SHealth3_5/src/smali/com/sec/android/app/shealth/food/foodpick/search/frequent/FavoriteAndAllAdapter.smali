.class public Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;
.super Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;
.source "FavoriteAndAllAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;",
        ":",
        "Ljava/lang/Comparable;",
        ">",
        "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private favoriteComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p4, "header"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<TE;>;",
            "Ljava/util/List",
            "<TE;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 35
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter<TE;>;"
    .local p2, "favoritesItems":Ljava/util/List;, "Ljava/util/List<TE;>;"
    .local p3, "allItemsList":Ljava/util/List;, "Ljava/util/List<TE;>;"
    const v0, 0x7f090088

    invoke-static {p1, v0}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getCaptionForCategoryInListView(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter$1;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;->favoriteComparator:Ljava/util/Comparator;

    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;->sortFavorites()V

    .line 37
    return-void
.end method

.method private sortFavorites()V
    .locals 2

    .prologue
    .line 40
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter<TE;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;->firstCategory:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;->favoriteComparator:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 41
    return-void
.end method


# virtual methods
.method protected isFirstHeaderVisible()Z
    .locals 1

    .prologue
    .line 66
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter<TE;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;->firstCategory:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isMainHeaderVisible()Z
    .locals 1

    .prologue
    .line 70
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter<TE;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;->itemsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic performClickOnFavoriteButton(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;

    .prologue
    .line 27
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter<TE;>;"
    check-cast p1, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;

    .end local p1    # "x0":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;->performClickOnFavoriteButton(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;)V

    return-void
.end method

.method protected performClickOnFavoriteButton(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 56
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter<TE;>;"
    .local p1, "listItem":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;, "TE;"
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;->isFavorite()Z

    move-result v0

    if-nez v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;->firstCategory:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;->sortFavorites()V

    .line 62
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->performClickOnFavoriteButton(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;)V

    .line 63
    return-void

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;->firstCategory:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public updateFavoritesItems(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 49
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter<TE;>;"
    .local p1, "favoritesItems":Ljava/util/List;, "Ljava/util/List<TE;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;->firstCategory:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;->firstCategory:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 51
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAndAllAdapter;->sortFavorites()V

    .line 52
    return-void
.end method
