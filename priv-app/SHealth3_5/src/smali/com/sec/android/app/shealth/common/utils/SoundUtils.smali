.class public Lcom/sec/android/app/shealth/common/utils/SoundUtils;
.super Ljava/lang/Object;
.source "SoundUtils.java"


# static fields
.field public static final BT_CONNECTED:I = 0x5

.field public static final BUTTON:I = 0x0

.field public static final GRADUATION:I = 0x2

.field private static final MAX_SOUND_POOL:I = 0xa

.field public static final TOUCH:I = 0x1

.field public static final WALKMATE_PAUSE:I = 0x3

.field public static final WALKMATE_START:I = 0x4

.field private static mSoundUtils:Lcom/sec/android/app/shealth/common/utils/SoundUtils;


# instance fields
.field private mCurSoundRes:[I

.field private mSoundPool:Landroid/media/SoundPool;

.field private mSoundPoolId:[I

.field private mSoundResId:[I

.field private volume:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/SoundUtils;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/utils/SoundUtils;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/SoundUtils;->mSoundUtils:Lcom/sec/android/app/shealth/common/utils/SoundUtils;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object v3, p0, Lcom/sec/android/app/shealth/common/utils/SoundUtils;->mSoundPool:Landroid/media/SoundPool;

    .line 24
    const/16 v0, 0xa

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/SoundUtils;->mSoundPoolId:[I

    .line 25
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/sec/android/app/shealth/common/utils/R$raw;->s_health_button:I

    aput v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/SoundUtils;->mSoundResId:[I

    .line 26
    iput-object v3, p0, Lcom/sec/android/app/shealth/common/utils/SoundUtils;->mCurSoundRes:[I

    .line 34
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/common/utils/SoundUtils;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    sget-object v2, Lcom/sec/android/app/shealth/common/utils/SoundUtils;->mSoundUtils:Lcom/sec/android/app/shealth/common/utils/SoundUtils;

    monitor-enter v2

    .line 45
    if-eqz p0, :cond_0

    .line 47
    :try_start_0
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 48
    .local v0, "audioManager":Landroid/media/AudioManager;
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/SoundUtils;->mSoundUtils:Lcom/sec/android/app/shealth/common/utils/SoundUtils;

    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v3

    int-to-float v3, v3

    iput v3, v1, Lcom/sec/android/app/shealth/common/utils/SoundUtils;->volume:F

    .line 49
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/SoundUtils;->mSoundUtils:Lcom/sec/android/app/shealth/common/utils/SoundUtils;

    monitor-exit v2

    .line 53
    .end local v0    # "audioManager":Landroid/media/AudioManager;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    monitor-exit v2

    goto :goto_0

    .line 55
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private soundSet(Landroid/content/Context;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "set"    # I

    .prologue
    const/4 v4, 0x0

    .line 60
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/SoundUtils;->releaseSoundSet()Z

    .line 61
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/utils/SoundUtils;->mSoundResId:[I

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/utils/SoundUtils;->mCurSoundRes:[I

    .line 62
    new-instance v1, Landroid/media/SoundPool;

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/utils/SoundUtils;->mCurSoundRes:[I

    array-length v2, v2

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3, v4}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/utils/SoundUtils;->mSoundPool:Landroid/media/SoundPool;

    .line 65
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/utils/SoundUtils;->mCurSoundRes:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 67
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/utils/SoundUtils;->mSoundPoolId:[I

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/utils/SoundUtils;->mSoundPool:Landroid/media/SoundPool;

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/utils/SoundUtils;->mCurSoundRes:[I

    aget v3, v3, v0

    invoke-virtual {v2, p1, v3, v4}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    aput v2, v1, v0

    .line 65
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 69
    :cond_0
    return-void
.end method


# virtual methods
.method public playSoundResource(Landroid/content/Context;I)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "i"    # I

    .prologue
    .line 80
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "sound_effects_enabled"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 90
    :goto_0
    return-void

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/SoundUtils;->mSoundPool:Landroid/media/SoundPool;

    if-nez v0, :cond_1

    .line 83
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/common/utils/SoundUtils;->soundSet(Landroid/content/Context;I)V

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/SoundUtils;->mSoundPool:Landroid/media/SoundPool;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/utils/SoundUtils;->mSoundPoolId:[I

    aget v1, v1, p2

    iget v2, p0, Lcom/sec/android/app/shealth/common/utils/SoundUtils;->volume:F

    iget v3, p0, Lcom/sec/android/app/shealth/common/utils/SoundUtils;->volume:F

    const/16 v4, 0xa

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 86
    :catch_0
    move-exception v7

    .line 88
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public releaseSoundSet()Z
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/SoundUtils;->mSoundPool:Landroid/media/SoundPool;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/SoundUtils;->mSoundPool:Landroid/media/SoundPool;

    invoke-virtual {v0}, Landroid/media/SoundPool;->release()V

    .line 104
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/SoundUtils;->mSoundPool:Landroid/media/SoundPool;

    .line 105
    const/4 v0, 0x1

    .line 109
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
