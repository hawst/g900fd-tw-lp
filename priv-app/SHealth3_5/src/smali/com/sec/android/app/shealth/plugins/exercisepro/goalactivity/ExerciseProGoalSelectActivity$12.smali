.class Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$12;
.super Ljava/lang/Object;
.source "ExerciseProGoalSelectActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->showInputMaxHRPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;)V
    .locals 0

    .prologue
    .line 347
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 5
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    const/4 v4, 0x0

    .line 350
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->checkMaxHrManuallyValue()Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->access$1400(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 351
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->showMaxHrWarningPopup()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->access$1200(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;)V

    .line 366
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 367
    .local v0, "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->mMaxHrEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 368
    return-void

    .line 354
    .end local v0    # "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    invoke-static {v4}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setIsRealTimeMaxHRAutoUpdate(Z)V

    .line 355
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->mMaxHrEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;)Landroid/widget/EditText;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->mMaxHrEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 356
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->mMaxHrEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 357
    .local v1, "value":I
    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setRealTimeManualyMaxHR(I)V

    .line 359
    .end local v1    # "value":I
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isNeedMaxHeartrateConfirm()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 360
    invoke-static {v4}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setNeedMaxHeartrateConfirm(Z)V

    .line 362
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    const-class v3, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGoalActivity;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->startNextActivity(Ljava/lang/Class;)V
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;Ljava/lang/Class;)V

    .line 363
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->setAllButtonsWereClicked(Z)V
    invoke-static {v2, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;Z)V

    goto :goto_0
.end method
