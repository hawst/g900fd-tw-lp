.class Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$10;
.super Ljava/lang/Object;
.source "SetGoalActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;)V
    .locals 0

    .prologue
    .line 485
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$10;->this$0:Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "arg1"    # Landroid/view/MotionEvent;

    .prologue
    .line 490
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$10;->this$0:Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->access$600(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;)Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v0

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$10;->this$0:Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mIntakeInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->access$500(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;)Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->isOutOfRange()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 493
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$10;->this$0:Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$10;->this$0:Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->access$600(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;)Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->hideSoftInput(Landroid/content/Context;Landroid/view/View;)V

    .line 495
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$10;->this$0:Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->access$600(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;)Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->clearFocus()V

    .line 496
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$10;->this$0:Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mIntakeInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->access$500(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;)Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setText(Ljava/lang/CharSequence;)V

    .line 497
    new-instance v0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$10$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$10$1;-><init>(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$10;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 530
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 512
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$10;->this$0:Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mIntakeInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->access$500(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;)Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$10;->this$0:Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->access$600(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;)Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->isOutOfRange()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 515
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$10;->this$0:Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$10;->this$0:Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->access$600(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;)Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->hideSoftInput(Landroid/content/Context;Landroid/view/View;)V

    .line 516
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$10;->this$0:Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mIntakeInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->access$500(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;)Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->clearFocus()V

    .line 517
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$10;->this$0:Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mBurnInputModule:Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->access$600(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;)Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setText(Ljava/lang/CharSequence;)V

    .line 518
    new-instance v0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$10$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$10$2;-><init>(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$10;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
