.class Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$8;
.super Ljava/lang/Object;
.source "HeartrateTagListActivity.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)V
    .locals 0

    .prologue
    .line 674
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$8;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    const/4 v1, 0x0

    .line 677
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$8;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->setViewForDelete(Z)V

    .line 678
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$8;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHrTagListAdapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$400(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->setCheckAll(Z)V

    .line 679
    if-nez p2, :cond_0

    .line 680
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$8;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # invokes: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->enableActionBarButtons(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$800(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;Z)V

    .line 681
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$8;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # invokes: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->updateSelectedCount(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$500(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;I)V

    .line 682
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$8;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagsToDelete:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 686
    :goto_0
    return-void

    .line 685
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$8;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->enableActionBarButtons(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$800(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;Z)V

    goto :goto_0
.end method
