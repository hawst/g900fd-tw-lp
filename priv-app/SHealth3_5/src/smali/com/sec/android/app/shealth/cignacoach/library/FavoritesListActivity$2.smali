.class Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$2;
.super Ljava/lang/Object;
.source "FavoritesListActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;)V
    .locals 0

    .prologue
    .line 243
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSave(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 248
    if-nez p1, :cond_0

    const/4 v0, 0x1

    .line 250
    .local v0, "isCategory":Z
    :goto_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;->setFilterCategory(Z)V

    .line 252
    if-eqz v0, :cond_1

    .line 253
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->sortByCategory()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->access$000(Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;)V

    .line 259
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoriteListView:Landroid/widget/ExpandableListView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;)Landroid/widget/ExpandableListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ExpandableListView;->setSelectionAfterHeaderView()V

    .line 260
    return-void

    .line 248
    .end local v0    # "isCategory":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 256
    .restart local v0    # "isCategory":Z
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->sortByTitle()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->access$100(Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;)V

    goto :goto_1
.end method
