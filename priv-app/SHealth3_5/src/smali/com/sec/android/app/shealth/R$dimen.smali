.class public final Lcom/sec/android/app/shealth/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final _1dp:I = 0x7f0a00ca

.field public static final about_shealth_button_marginBottom:I = 0x7f0a04e3

.field public static final about_shealth_imageView_margin_top:I = 0x7f0a04e0

.field public static final about_shealth_linear_layout_height:I = 0x7f0a04e1

.field public static final about_shealth_linear_layout_marginTop:I = 0x7f0a04df

.field public static final about_shealth_scrollview_height:I = 0x7f0a04e2

.field public static final accessory_connection_icon_size:I = 0x7f0a00ac

.field public static final accessory_connection_view_status_text_left_margin:I = 0x7f0a00ab

.field public static final accessory_connection_view_status_text_size:I = 0x7f0a00aa

.field public static final action_bar_additional_content_container_width_default:I = 0x7f0a0161

.field public static final action_bar_back_button_container_width:I = 0x7f0a0173

.field public static final action_bar_back_button_height:I = 0x7f0a0170

.field public static final action_bar_back_button_margin_left:I = 0x7f0a0171

.field public static final action_bar_back_button_margin_right:I = 0x7f0a0172

.field public static final action_bar_back_button_move_animation_range:I = 0x7f0a0175

.field public static final action_bar_back_button_width:I = 0x7f0a016f

.field public static final action_bar_btn_height:I = 0x7f0a0152

.field public static final action_bar_btn_width:I = 0x7f0a0153

.field public static final action_bar_button_left_margin_icon_and_text:I = 0x7f0a0168

.field public static final action_bar_button_left_margin_icon_only:I = 0x7f0a0165

.field public static final action_bar_button_left_margin_text_only:I = 0x7f0a0162

.field public static final action_bar_button_middle_margin_icon_and_text:I = 0x7f0a0169

.field public static final action_bar_button_middle_margin_icon_only:I = 0x7f0a0166

.field public static final action_bar_button_middle_margin_text_only:I = 0x7f0a0163

.field public static final action_bar_button_ok_cancel_text_size:I = 0x7f0a0154

.field public static final action_bar_button_right_margin_icon_and_text:I = 0x7f0a016a

.field public static final action_bar_button_right_margin_icon_only:I = 0x7f0a0167

.field public static final action_bar_button_right_margin_text_only:I = 0x7f0a0164

.field public static final action_bar_button_text_size:I = 0x7f0a0155

.field public static final action_bar_buttons_divider_height:I = 0x7f0a0174

.field public static final action_bar_height:I = 0x7f0a015f

.field public static final action_bar_height2:I = 0x7f0a0160

.field public static final action_bar_spinner_header_left_padding:I = 0x7f0a015e

.field public static final action_bar_spinner_horizontal_offset:I = 0x7f0a015b

.field public static final action_bar_spinner_left_margin:I = 0x7f0a015d

.field public static final action_bar_spinner_left_padding:I = 0x7f0a015c

.field public static final action_bar_spinner_vertical_offset:I = 0x7f0a015a

.field public static final action_bar_title_icon_margin_left:I = 0x7f0a0177

.field public static final action_bar_title_icon_margin_right:I = 0x7f0a0178

.field public static final action_bar_title_icon_size:I = 0x7f0a0176

.field public static final action_bar_up_button_margin_bottom:I = 0x7f0a016e

.field public static final action_bar_up_button_margin_left:I = 0x7f0a016c

.field public static final action_bar_up_button_margin_top:I = 0x7f0a016d

.field public static final action_bar_vertical_offset:I = 0x7f0a0159

.field public static final action_up_button_height:I = 0x7f0a0157

.field public static final action_up_button_margin:I = 0x7f0a0158

.field public static final action_up_button_width:I = 0x7f0a0156

.field public static final actionbar_button_margin:I = 0x7f0a017b

.field public static final actionbar_button_padding:I = 0x7f0a017c

.field public static final actionbar_button_width:I = 0x7f0a017d

.field public static final actionbar_divider_height:I = 0x7f0a017a

.field public static final actionbar_divider_width:I = 0x7f0a0179

.field public static final actionbar_summary_height:I = 0x7f0a0180

.field public static final actionbar_summary_height_data:I = 0x7f0a0181

.field public static final actionbar_summary_height_div:I = 0x7f0a0182

.field public static final actionbar_summary_margin:I = 0x7f0a017e

.field public static final actionbar_summary_nav_size:I = 0x7f0a017f

.field public static final actionbar_switch_width:I = 0x7f0a0183

.field public static final activity_horizontal_margin:I = 0x7f0a0000

.field public static final activity_vertical_margin:I = 0x7f0a0001

.field public static final alert_text_size:I = 0x7f0a0a16

.field public static final alert_title_text_size:I = 0x7f0a0a17

.field public static final app_icon_dimen:I = 0x7f0a05d0

.field public static final appwidget_kcal_unit_text_size:I = 0x7f0a05d3

.field public static final auto_backup_account_layout_backup_layout_header_padding_down:I = 0x7f0a04f6

.field public static final auto_backup_account_layout_backup_layout_header_padding_top:I = 0x7f0a04f5

.field public static final auto_backup_account_layout_checkBox_marginLeft:I = 0x7f0a06af

.field public static final auto_backup_account_layout_linearLayout_height:I = 0x7f0a04f7

.field public static final auto_backup_item_textview_height:I = 0x7f0a04f8

.field public static final award_center_view_height:I = 0x7f0a05a7

.field public static final award_da_road_stroke_width:I = 0x7f0a05b9

.field public static final award_da_status_width:I = 0x7f0a05ba

.field public static final award_list_item_layout_item_icon_layout_height:I = 0x7f0a05b2

.field public static final award_list_item_layout_item_icon_layout_marginLeft:I = 0x7f0a05b3

.field public static final award_list_item_layout_item_icon_layout_marginRight:I = 0x7f0a05b4

.field public static final award_list_item_layout_item_icon_layout_width:I = 0x7f0a05b1

.field public static final award_list_item_layout_text_date_layout_marginRight:I = 0x7f0a05b6

.field public static final award_list_item_layout_text_date_textSize:I = 0x7f0a05b7

.field public static final award_list_item_layout_text_value_textSize:I = 0x7f0a05b8

.field public static final award_list_item_layout_title__textSize:I = 0x7f0a05b5

.field public static final award_more_option_popup_width:I = 0x7f0a05a8

.field public static final award_pedo_road_stroke_width:I = 0x7f0a05bb

.field public static final award_pedo_status_width:I = 0x7f0a05bc

.field public static final award_top_view_height:I = 0x7f0a05a6

.field public static final axes_vertical_text_space:I = 0x7f0a0144

.field public static final bg_input_field_image_button_height:I = 0x7f0a01c9

.field public static final bg_input_field_image_button_margin_left:I = 0x7f0a01cb

.field public static final bg_input_field_image_button_margin_right:I = 0x7f0a01ca

.field public static final bg_input_field_image_button_width:I = 0x7f0a01c8

.field public static final blood_glucose_width_after_meal_line:I = 0x7f0a0146

.field public static final blood_glucose_width_fasting_line:I = 0x7f0a0145

.field public static final bmi_max_width:I = 0x7f0a0595

.field public static final bmi_value_width:I = 0x7f0a0596

.field public static final bmi_width:I = 0x7f0a0594

.field public static final bt_item_device_image_height:I = 0x7f0a0187

.field public static final bt_item_device_image_width:I = 0x7f0a0186

.field public static final bt_item_device_name_text_size:I = 0x7f0a0188

.field public static final bt_item_height:I = 0x7f0a0184

.field public static final bt_item_paired_image_height:I = 0x7f0a0189

.field public static final bt_item_paired_image_width:I = 0x7f0a018a

.field public static final bt_item_paried_image_height:I = 0x7f0a05a0

.field public static final bt_item_paried_image_width:I = 0x7f0a05a1

.field public static final bt_item_width:I = 0x7f0a0185

.field public static final calendar_adjacent_period_btn_side_margin:I = 0x7f0a01af

.field public static final calendar_adjacent_period_button_height:I = 0x7f0a01ac

.field public static final calendar_adjacent_period_button_text_size:I = 0x7f0a01ae

.field public static final calendar_adjacent_period_button_width:I = 0x7f0a01ad

.field public static final calendar_bottom_panel_height_include_shadow:I = 0x7f0a01a5

.field public static final calendar_content_area_height:I = 0x7f0a019e

.field public static final calendar_day_button_height:I = 0x7f0a01b7

.field public static final calendar_day_button_textsize:I = 0x7f0a01b9

.field public static final calendar_day_button_width:I = 0x7f0a01b8

.field public static final calendar_day_number_text_size:I = 0x7f0a01b1

.field public static final calendar_days_of_week_container_height:I = 0x7f0a01b4

.field public static final calendar_for_six_weeks_height:I = 0x7f0a01b2

.field public static final calendar_for_six_weeks_width:I = 0x7f0a01b3

.field public static final calendar_horizontal_divider_height:I = 0x7f0a01b6

.field public static final calendar_period_panel_height:I = 0x7f0a01ab

.field public static final calendar_popup_layout_width:I = 0x7f0a01a0

.field public static final calendar_popup_month_textview_width:I = 0x7f0a019f

.field public static final calendar_popup_width:I = 0x7f0a01a1

.field public static final calendar_popup_width_include_shadow:I = 0x7f0a01a2

.field public static final calendar_selected_period_label_text_size:I = 0x7f0a01b0

.field public static final calendar_text_day_size:I = 0x7f0a01a3

.field public static final calendar_this_period_btn_height:I = 0x7f0a01a9

.field public static final calendar_this_period_btn_text_size:I = 0x7f0a01aa

.field public static final calendar_this_period_btn_width:I = 0x7f0a01a8

.field public static final calendar_this_period_button_margin_right:I = 0x7f0a01a7

.field public static final calendar_today_monthdate_textsize:I = 0x7f0a01bb

.field public static final calendar_top_panel_height_include_shadow:I = 0x7f0a01a4

.field public static final calendar_top_panel_text_size:I = 0x7f0a01a6

.field public static final calendar_toppanel_height:I = 0x7f0a01ba

.field public static final calendar_vertical_divider_width:I = 0x7f0a01b5

.field public static final chartview_goal_level_text_size:I = 0x7f0a04fd

.field public static final chartview_goal_level_xy_lebel_size:I = 0x7f0a04fe

.field public static final chartview_handler01_height:I = 0x7f0a04ff

.field public static final chartview_handler01_text_size:I = 0x7f0a0501

.field public static final chartview_handler01_width:I = 0x7f0a0500

.field public static final chartview_handler_date_text_size:I = 0x7f0a04fc

.field public static final chartview_handler_height:I = 0x7f0a04f9

.field public static final chartview_handler_text_size:I = 0x7f0a04fb

.field public static final chartview_handler_width:I = 0x7f0a04fa

.field public static final chartview_tab_height:I = 0x7f0a01f1

.field public static final cigna_about_cigna_image_view_height:I = 0x7f0a0455

.field public static final cigna_about_cigna_image_view_width:I = 0x7f0a0454

.field public static final cigna_about_cigna_text_padding_bottom:I = 0x7f0a0457

.field public static final cigna_about_cigna_text_padding_left:I = 0x7f0a0456

.field public static final cigna_about_cigna_text_size:I = 0x7f0a0458

.field public static final cigna_assessment_4button_layout_height:I = 0x7f0a02f4

.field public static final cigna_assessment_4button_layout_margin:I = 0x7f0a02f5

.field public static final cigna_assessment_a_height:I = 0x7f0a020a

.field public static final cigna_assessment_a_margin_right:I = 0x7f0a0207

.field public static final cigna_assessment_a_margin_top:I = 0x7f0a0208

.field public static final cigna_assessment_a_width:I = 0x7f0a0209

.field public static final cigna_assessment_header_scroll_height:I = 0x7f0a02f3

.field public static final cigna_assessment_header_text_size:I = 0x7f0a02fc

.field public static final cigna_assessment_header_weight_text_margin:I = 0x7f0a02fd

.field public static final cigna_assessment_indicator_area_height:I = 0x7f0a0205

.field public static final cigna_assessment_num_text_size:I = 0x7f0a0288

.field public static final cigna_assessment_page_controller_btn_height:I = 0x7f0a02fe

.field public static final cigna_assessment_page_controller_height:I = 0x7f0a02f7

.field public static final cigna_assessment_reference_header_height:I = 0x7f0a02eb

.field public static final cigna_assessment_reference_header_image_height:I = 0x7f0a02f1

.field public static final cigna_assessment_reference_header_image_margin:I = 0x7f0a02f2

.field public static final cigna_assessment_reference_header_image_width:I = 0x7f0a02f0

.field public static final cigna_assessment_reference_header_iv_width:I = 0x7f0a02ee

.field public static final cigna_assessment_reference_header_margin:I = 0x7f0a02ef

.field public static final cigna_assessment_str_text_size:I = 0x7f0a0289

.field public static final cigna_assessment_sub_title_margin_top:I = 0x7f0a020c

.field public static final cigna_assessment_text_margin_top:I = 0x7f0a020b

.field public static final cigna_assessment_top_content_height:I = 0x7f0a02ed

.field public static final cigna_assessment_top_content_width:I = 0x7f0a02ec

.field public static final cigna_assessment_tracker_padding:I = 0x7f0a02f6

.field public static final cigna_assessment_tracker_says_margin_top:I = 0x7f0a0206

.field public static final cigna_assessment_tracker_text_size:I = 0x7f0a0287

.field public static final cigna_assessment_weight_edit_ft_min_width:I = 0x7f0a0276

.field public static final cigna_assessment_weight_edit_text_min_width:I = 0x7f0a0275

.field public static final cigna_assessment_weight_layout_padding:I = 0x7f0a02fb

.field public static final cigna_assessment_weight_meter_handle_meter_stroke:I = 0x7f0a0273

.field public static final cigna_assessment_weight_meter_handle_small_meter_start_offset:I = 0x7f0a0274

.field public static final cigna_assessment_weight_meter_input_module_height:I = 0x7f0a0270

.field public static final cigna_assessment_weight_meter_offset_bottom:I = 0x7f0a0272

.field public static final cigna_assessment_weight_meter_offset_top:I = 0x7f0a0271

.field public static final cigna_badge_achieve_button_area_height:I = 0x7f0a0303

.field public static final cigna_badge_achieve_button_height:I = 0x7f0a0305

.field public static final cigna_badge_achieve_button_margin:I = 0x7f0a0307

.field public static final cigna_badge_achieve_button_text_size:I = 0x7f0a0306

.field public static final cigna_badge_achieve_button_width:I = 0x7f0a0304

.field public static final cigna_badge_achieve_image_margin:I = 0x7f0a0302

.field public static final cigna_badge_list_item_badge_description_text_size:I = 0x7f0a030e

.field public static final cigna_badge_list_item_badge_name_layout_padding:I = 0x7f0a030a

.field public static final cigna_badge_list_item_badge_name_layout_padding_right:I = 0x7f0a030b

.field public static final cigna_badge_list_item_badge_name_text_size:I = 0x7f0a030d

.field public static final cigna_badge_list_item_badge_name_tv_padding:I = 0x7f0a030c

.field public static final cigna_badge_list_item_icon_layout_margin:I = 0x7f0a0309

.field public static final cigna_badge_list_item_icon_size:I = 0x7f0a0308

.field public static final cigna_calendar_bottom_btn_height:I = 0x7f0a031c

.field public static final cigna_calendar_btn_height:I = 0x7f0a0318

.field public static final cigna_calendar_btn_layout_height:I = 0x7f0a0315

.field public static final cigna_calendar_btn_layout_padding:I = 0x7f0a0316

.field public static final cigna_calendar_btn_padding:I = 0x7f0a0319

.field public static final cigna_calendar_btn_width:I = 0x7f0a0317

.field public static final cigna_calendar_currentmonth_text_size:I = 0x7f0a031b

.field public static final cigna_calendar_currentmonth_width:I = 0x7f0a031a

.field public static final cigna_calendar_date_height:I = 0x7f0a0310

.field public static final cigna_calendar_date_margin:I = 0x7f0a0311

.field public static final cigna_calendar_date_padding:I = 0x7f0a0312

.field public static final cigna_calendar_date_text_size:I = 0x7f0a0313

.field public static final cigna_calendar_popup_height:I = 0x7f0a0314

.field public static final cigna_calendar_root_layout_width:I = 0x7f0a030f

.field public static final cigna_cancel_unaligned_goal_description_margin_top:I = 0x7f0a043e

.field public static final cigna_cancel_unaligned_goal_description_text_size:I = 0x7f0a043f

.field public static final cigna_cancel_unaligned_goal_message_height:I = 0x7f0a043c

.field public static final cigna_cancel_unaligned_goal_message_text_size:I = 0x7f0a043d

.field public static final cigna_category_header_height:I = 0x7f0a031d

.field public static final cigna_category_txt_height:I = 0x7f0a031e

.field public static final cigna_category_txt_margin:I = 0x7f0a031f

.field public static final cigna_category_txt_padding_bottom:I = 0x7f0a0321

.field public static final cigna_category_txt_padding_top:I = 0x7f0a0320

.field public static final cigna_check_box_item_margin:I = 0x7f0a0379

.field public static final cigna_check_box_layout:I = 0x7f0a04dd

.field public static final cigna_child_header_height:I = 0x7f0a0373

.field public static final cigna_child_header_margin:I = 0x7f0a0374

.field public static final cigna_circle_progress_image_layout_width:I = 0x7f0a0490

.field public static final cigna_circle_progress_image_size:I = 0x7f0a048f

.field public static final cigna_circle_progress_indicator_height:I = 0x7f0a026e

.field public static final cigna_circle_progress_indicator_left_margin:I = 0x7f0a026f

.field public static final cigna_circle_progress_indicator_width:I = 0x7f0a026d

.field public static final cigna_coach_corner_greenting_text:I = 0x7f0a028b

.field public static final cigna_coach_corner_greeting_padding:I = 0x7f0a020d

.field public static final cigna_coach_corner_hello_text:I = 0x7f0a028a

.field public static final cigna_coach_first_time_msg_extra_value:I = 0x7f0a037d

.field public static final cigna_coach_first_time_msg_margin:I = 0x7f0a037c

.field public static final cigna_coach_intro_button_area_height:I = 0x7f0a020f

.field public static final cigna_coach_intro_button_height:I = 0x7f0a0211

.field public static final cigna_coach_intro_button_width:I = 0x7f0a0210

.field public static final cigna_coach_life_style_score_bottom_button_height:I = 0x7f0a0213

.field public static final cigna_coach_life_style_score_bottom_button_width:I = 0x7f0a0212

.field public static final cigna_coach_message_activity_height:I = 0x7f0a0322

.field public static final cigna_coach_message_gauge_correct_value:I = 0x7f0a02de

.field public static final cigna_coach_message_gauge_marker_height:I = 0x7f0a02dd

.field public static final cigna_coach_message_gauge_marker_width:I = 0x7f0a02dc

.field public static final cigna_coach_message_gauge_step_gap:I = 0x7f0a02d9

.field public static final cigna_coach_message_gauge_step_width:I = 0x7f0a02d8

.field public static final cigna_coach_message_gauge_view_height:I = 0x7f0a0324

.field public static final cigna_coach_message_gauge_width:I = 0x7f0a02da

.field public static final cigna_coach_message_marker_area:I = 0x7f0a02db

.field public static final cigna_coach_welcom_button_area_height:I = 0x7f0a020e

.field public static final cigna_complete_badge_divider_height:I = 0x7f0a0283

.field public static final cigna_complete_badge_divider_margin_right:I = 0x7f0a0284

.field public static final cigna_complete_badge_divider_margin_top:I = 0x7f0a0285

.field public static final cigna_complete_badge_divider_width:I = 0x7f0a0282

.field public static final cigna_complete_badge_height:I = 0x7f0a027d

.field public static final cigna_complete_badge_iv_icon_margin_left:I = 0x7f0a027f

.field public static final cigna_complete_badge_iv_icon_size:I = 0x7f0a027e

.field public static final cigna_complete_badge_txt_sub_title_height:I = 0x7f0a0280

.field public static final cigna_complete_badge_txt_sub_title_text_size:I = 0x7f0a02a2

.field public static final cigna_complete_badge_txt_title_height:I = 0x7f0a0281

.field public static final cigna_complete_badge_txt_title_text_size:I = 0x7f0a02a3

.field public static final cigna_complete_header_txt_exclamations_height:I = 0x7f0a027b

.field public static final cigna_complete_header_txt_exclamations_text_size:I = 0x7f0a02a1

.field public static final cigna_complete_header_txt_goal_exclamations_margin_top:I = 0x7f0a027c

.field public static final cigna_complete_header_txt_header_title_height:I = 0x7f0a0278

.field public static final cigna_complete_header_txt_header_title_padding_top:I = 0x7f0a0277

.field public static final cigna_complete_header_txt_header_title_text_size:I = 0x7f0a029e

.field public static final cigna_complete_header_txt_msg_margin_top:I = 0x7f0a0279

.field public static final cigna_complete_header_txt_msg_text_size:I = 0x7f0a029f

.field public static final cigna_complete_header_txt_progress_msg_height:I = 0x7f0a027a

.field public static final cigna_complete_header_txt_progress_msg_text_size:I = 0x7f0a02a0

.field public static final cigna_complete_header_view_padding:I = 0x7f0a0337

.field public static final cigna_complete_header_view_padding_top:I = 0x7f0a0338

.field public static final cigna_current_detail_header_extra_text_size:I = 0x7f0a02a6

.field public static final cigna_current_detail_header_title_text_size:I = 0x7f0a02a4

.field public static final cigna_current_detail_header_xdayleft_text_size:I = 0x7f0a02a5

.field public static final cigna_current_detail_tracking_day_check_text_size:I = 0x7f0a02a8

.field public static final cigna_current_detail_tracking_day_text_size:I = 0x7f0a02a7

.field public static final cigna_current_goal_arrow_margin_left:I = 0x7f0a0248

.field public static final cigna_current_goal_arrow_margin_right:I = 0x7f0a0249

.field public static final cigna_current_goal_child_item_margin_left:I = 0x7f0a0242

.field public static final cigna_current_goal_coach_msg_margin_left:I = 0x7f0a023f

.field public static final cigna_current_goal_coach_msg_margin_right:I = 0x7f0a0240

.field public static final cigna_current_goal_coach_msg_text_size:I = 0x7f0a029c

.field public static final cigna_current_goal_divider_margin_left:I = 0x7f0a0246

.field public static final cigna_current_goal_divider_margin_vertical:I = 0x7f0a0247

.field public static final cigna_current_goal_icon_margin_left:I = 0x7f0a0244

.field public static final cigna_current_goal_icon_size:I = 0x7f0a0243

.field public static final cigna_current_goal_list_group_height:I = 0x7f0a024a

.field public static final cigna_current_goal_list_header_height:I = 0x7f0a0343

.field public static final cigna_current_goal_no_data_image_size:I = 0x7f0a0241

.field public static final cigna_current_goal_text_margin_left:I = 0x7f0a0245

.field public static final cigna_current_goal_text_margin_right:I = 0x7f0a0364

.field public static final cigna_current_goal_text_padding:I = 0x7f0a0341

.field public static final cigna_current_mission_detail_detail_expandable_child_text_size:I = 0x7f0a0291

.field public static final cigna_current_mission_detail_detail_expandable_group_text_size:I = 0x7f0a0290

.field public static final cigna_current_mission_detail_detail_text_size:I = 0x7f0a028f

.field public static final cigna_current_mission_detail_details_description_margin:I = 0x7f0a0347

.field public static final cigna_current_mission_detail_details_padding:I = 0x7f0a0344

.field public static final cigna_current_mission_detail_details_paddingBottom:I = 0x7f0a0346

.field public static final cigna_current_mission_detail_details_paddingTop:I = 0x7f0a0345

.field public static final cigna_current_mission_detail_details_title_margin:I = 0x7f0a0348

.field public static final cigna_current_mission_detail_green_header_text_size:I = 0x7f0a028d

.field public static final cigna_current_mission_detail_header_icon_margin_bottom:I = 0x7f0a0235

.field public static final cigna_current_mission_detail_header_icon_margin_left:I = 0x7f0a0232

.field public static final cigna_current_mission_detail_header_icon_margin_right:I = 0x7f0a0233

.field public static final cigna_current_mission_detail_header_icon_margin_top:I = 0x7f0a0234

.field public static final cigna_current_mission_detail_list_group_height:I = 0x7f0a023d

.field public static final cigna_current_mission_detail_list_item_padding:I = 0x7f0a023e

.field public static final cigna_current_mission_detail_tip_text_detail_margin:I = 0x7f0a035a

.field public static final cigna_current_mission_detail_tips_title_item_view_padding:I = 0x7f0a0359

.field public static final cigna_current_mission_detail_title_text_size:I = 0x7f0a028e

.field public static final cigna_current_mission_detail_tracking_check_margin_top:I = 0x7f0a023b

.field public static final cigna_current_mission_detail_tracking_check_width:I = 0x7f0a023a

.field public static final cigna_current_mission_detail_tracking_day_height:I = 0x7f0a0238

.field public static final cigna_current_mission_detail_tracking_day_margin_top:I = 0x7f0a0239

.field public static final cigna_current_mission_detail_tracking_day_of_week_margin_left:I = 0x7f0a023c

.field public static final cigna_current_mission_detail_tracking_margin_bottom:I = 0x7f0a0237

.field public static final cigna_current_mission_detail_tracking_margin_top:I = 0x7f0a0236

.field public static final cigna_current_mission_detail_viewpager_height:I = 0x7f0a0358

.field public static final cigna_current_mission_header_again_btn_height:I = 0x7f0a0351

.field public static final cigna_current_mission_header_again_btn_layout_height:I = 0x7f0a034f

.field public static final cigna_current_mission_header_again_btn_width:I = 0x7f0a0350

.field public static final cigna_current_mission_header_complete_date_txt_height:I = 0x7f0a034d

.field public static final cigna_current_mission_header_complete_date_txt_size:I = 0x7f0a034e

.field public static final cigna_current_mission_header_tracker_info_margin_bottom:I = 0x7f0a0353

.field public static final cigna_current_mission_header_tracker_info_margin_top:I = 0x7f0a0352

.field public static final cigna_current_mission_header_tracker_info_txt_size:I = 0x7f0a0354

.field public static final cigna_current_mission_header_txt_height:I = 0x7f0a034a

.field public static final cigna_current_mission_header_txt_margin:I = 0x7f0a034b

.field public static final cigna_current_mission_header_xdayleft_txt_margin:I = 0x7f0a034c

.field public static final cigna_custom_header_height:I = 0x7f0a0204

.field public static final cigna_custom_main_score_header_message_height:I = 0x7f0a0203

.field public static final cigna_custom_viewpager_indicator_number_height:I = 0x7f0a022f

.field public static final cigna_custom_viewpager_indicator_number_left_margin:I = 0x7f0a022b

.field public static final cigna_custom_viewpager_indicator_number_line_height:I = 0x7f0a0231

.field public static final cigna_custom_viewpager_indicator_number_line_left_margin:I = 0x7f0a022c

.field public static final cigna_custom_viewpager_indicator_number_line_right_margin:I = 0x7f0a022d

.field public static final cigna_custom_viewpager_indicator_number_line_width:I = 0x7f0a0230

.field public static final cigna_custom_viewpager_indicator_number_top_margin:I = 0x7f0a022a

.field public static final cigna_custom_viewpager_indicator_number_width:I = 0x7f0a022e

.field public static final cigna_custom_viewpager_indicator_rectangle_height:I = 0x7f0a0229

.field public static final cigna_custom_viewpager_indicator_rectangle_top_margin:I = 0x7f0a0227

.field public static final cigna_custom_viewpager_indicator_rectangle_width:I = 0x7f0a0228

.field public static final cigna_engagin_select_button_area_height:I = 0x7f0a0431

.field public static final cigna_engagin_select_button_area_padding_top:I = 0x7f0a0432

.field public static final cigna_engaging_fifth_height:I = 0x7f0a0438

.field public static final cigna_engaging_fifth_margin_left:I = 0x7f0a0439

.field public static final cigna_engaging_fifth_width:I = 0x7f0a0437

.field public static final cigna_engaging_first_height:I = 0x7f0a0434

.field public static final cigna_engaging_first_width:I = 0x7f0a0433

.field public static final cigna_engaging_header_icon_margin:I = 0x7f0a042f

.field public static final cigna_engaging_header_icon_size:I = 0x7f0a042e

.field public static final cigna_engaging_second_margin_top:I = 0x7f0a0435

.field public static final cigna_engaging_third_margin_top:I = 0x7f0a0436

.field public static final cigna_expandable_arrow_size:I = 0x7f0a024d

.field public static final cigna_expandable_btn_size:I = 0x7f0a024e

.field public static final cigna_expandable_button_margin:I = 0x7f0a033d

.field public static final cigna_expandable_child_item_view_padding_left:I = 0x7f0a0365

.field public static final cigna_expandable_child_item_view_padding_right:I = 0x7f0a0366

.field public static final cigna_expandable_child_layout_margin_left:I = 0x7f0a035f

.field public static final cigna_expandable_child_layout_margin_right:I = 0x7f0a0360

.field public static final cigna_expandable_child_layout_min_height:I = 0x7f0a0339

.field public static final cigna_expandable_child_layout_padding:I = 0x7f0a033a

.field public static final cigna_expandable_cpi_progress_height:I = 0x7f0a033c

.field public static final cigna_expandable_date_text_size:I = 0x7f0a0294

.field public static final cigna_expandable_divider_height:I = 0x7f0a033e

.field public static final cigna_expandable_expandable_image_arrow_margin_right:I = 0x7f0a036d

.field public static final cigna_expandable_expandable_image_arrow_margin_top:I = 0x7f0a036c

.field public static final cigna_expandable_group_image_icon_margin:I = 0x7f0a036b

.field public static final cigna_expandable_group_image_icon_size:I = 0x7f0a036a

.field public static final cigna_expandable_group_padding:I = 0x7f0a024b

.field public static final cigna_expandable_icon_size:I = 0x7f0a024c

.field public static final cigna_expandable_image_icon_margin:I = 0x7f0a0367

.field public static final cigna_expandable_info_margin:I = 0x7f0a0342

.field public static final cigna_expandable_info_text_size:I = 0x7f0a0293

.field public static final cigna_expandable_title_text_size:I = 0x7f0a0292

.field public static final cigna_expandable_txt_complete_height:I = 0x7f0a0369

.field public static final cigna_expandable_txt_margin_left:I = 0x7f0a024f

.field public static final cigna_expandable_txt_title_height:I = 0x7f0a0368

.field public static final cigna_expandable_txt_title_margin:I = 0x7f0a033b

.field public static final cigna_expandable_txt_title_margin_bottom:I = 0x7f0a0362

.field public static final cigna_expandable_txt_title_margin_left:I = 0x7f0a0361

.field public static final cigna_expandable_txt_title_padding:I = 0x7f0a0363

.field public static final cigna_expandablelist_group_height:I = 0x7f0a036e

.field public static final cigna_first_time_btn_start_height:I = 0x7f0a037e

.field public static final cigna_first_time_btn_start_text_size:I = 0x7f0a037f

.field public static final cigna_first_time_divider_height:I = 0x7f0a0380

.field public static final cigna_first_time_message_image_margin:I = 0x7f0a037b

.field public static final cigna_first_time_message_layout_height:I = 0x7f0a037a

.field public static final cigna_first_time_no_score_msg_margin:I = 0x7f0a0389

.field public static final cigna_first_time_no_score_msg_margin_top:I = 0x7f0a038a

.field public static final cigna_first_time_no_score_msg_text_size:I = 0x7f0a038b

.field public static final cigna_five_button_header_scroll_margin:I = 0x7f0a0430

.field public static final cigna_full_width_popup_btn_text_size:I = 0x7f0a038e

.field public static final cigna_full_width_popup_layout_padding:I = 0x7f0a038c

.field public static final cigna_full_width_popup_text_size:I = 0x7f0a038d

.field public static final cigna_gender_image_height:I = 0x7f0a02f9

.field public static final cigna_gender_image_margin:I = 0x7f0a02fa

.field public static final cigna_gender_image_width:I = 0x7f0a02f8

.field public static final cigna_goal_complete_1button_area_height:I = 0x7f0a0393

.field public static final cigna_goal_complete_badge_layout_margin:I = 0x7f0a0392

.field public static final cigna_goal_complete_button_ok_height:I = 0x7f0a0394

.field public static final cigna_goal_complete_button_ok_margin:I = 0x7f0a0396

.field public static final cigna_goal_complete_button_ok_padding:I = 0x7f0a0395

.field public static final cigna_goal_complete_button_text_size:I = 0x7f0a0397

.field public static final cigna_goal_complete_button_width:I = 0x7f0a0398

.field public static final cigna_goal_complete_image_height:I = 0x7f0a0390

.field public static final cigna_goal_complete_image_margin:I = 0x7f0a0391

.field public static final cigna_goal_complete_image_width:I = 0x7f0a038f

.field public static final cigna_goal_history_header_custom_height:I = 0x7f0a039c

.field public static final cigna_goal_history_header_custom_margin:I = 0x7f0a039d

.field public static final cigna_goal_history_header_height:I = 0x7f0a0399

.field public static final cigna_goal_history_header_image_margin:I = 0x7f0a039b

.field public static final cigna_goal_history_header_image_size:I = 0x7f0a039a

.field public static final cigna_goal_view_expandable_list_margin:I = 0x7f0a0340

.field public static final cigna_goal_view_no_data_image_view_height:I = 0x7f0a041d

.field public static final cigna_goal_view_no_data_margin_top:I = 0x7f0a041c

.field public static final cigna_goal_view_no_data_text_size:I = 0x7f0a029d

.field public static final cigna_goal_view_no_data_text_view_margin_bottom_2:I = 0x7f0a0420

.field public static final cigna_goal_view_no_data_text_view_margin_bottom_combined:I = 0x7f0a0423

.field public static final cigna_goal_view_no_data_text_view_margin_left:I = 0x7f0a0424

.field public static final cigna_goal_view_no_data_text_view_margin_top:I = 0x7f0a0421

.field public static final cigna_goal_view_no_data_text_view_margin_top_2:I = 0x7f0a041f

.field public static final cigna_goal_view_no_data_text_view_margin_top_combined:I = 0x7f0a0422

.field public static final cigna_goal_view_no_data_text_view_size:I = 0x7f0a0425

.field public static final cigna_goal_view_no_data_text_view_width:I = 0x7f0a041e

.field public static final cigna_graph_comparison_bottom_padding:I = 0x7f0a01f3

.field public static final cigna_graph_comparison_height:I = 0x7f0a0215

.field public static final cigna_graph_handler_height:I = 0x7f0a0218

.field public static final cigna_graph_handler_item_offset:I = 0x7f0a021a

.field public static final cigna_graph_handler_item_text_offset:I = 0x7f0a021b

.field public static final cigna_graph_height:I = 0x7f0a01f2

.field public static final cigna_graph_separator_spacing_top_default:I = 0x7f0a0217

.field public static final cigna_graph_x_axis_text_space:I = 0x7f0a0216

.field public static final cigna_group_expand_button_layout_margin:I = 0x7f0a0370

.field public static final cigna_group_expand_button_layout_size:I = 0x7f0a036f

.field public static final cigna_height_editable_text_double_prime_size:I = 0x7f0a03a5

.field public static final cigna_height_editable_text_margin_left:I = 0x7f0a03a3

.field public static final cigna_height_editable_text_margin_right:I = 0x7f0a03a4

.field public static final cigna_height_editable_text_size:I = 0x7f0a03a2

.field public static final cigna_height_editable_text_view_height:I = 0x7f0a03a1

.field public static final cigna_height_inputmodule_dropdownlist_height:I = 0x7f0a03a7

.field public static final cigna_height_inputmodule_dropdownlist_width:I = 0x7f0a03a6

.field public static final cigna_height_root_center_height:I = 0x7f0a039f

.field public static final cigna_height_root_center_view_width:I = 0x7f0a03a0

.field public static final cigna_height_root_center_width:I = 0x7f0a039e

.field public static final cigna_help_coach_guidance_height:I = 0x7f0a03a9

.field public static final cigna_help_coach_guidance_layout_height:I = 0x7f0a03ad

.field public static final cigna_help_coach_guidance_layout_margin_left:I = 0x7f0a03ae

.field public static final cigna_help_coach_guidance_layout_margin_top:I = 0x7f0a03af

.field public static final cigna_help_coach_guidance_layout_padding_bottom:I = 0x7f0a03b0

.field public static final cigna_help_coach_guidance_margin:I = 0x7f0a03aa

.field public static final cigna_help_coach_guidance_padding_bottom:I = 0x7f0a03ab

.field public static final cigna_help_coach_guidance_padding_right:I = 0x7f0a03ac

.field public static final cigna_help_coach_icon_margin:I = 0x7f0a03b2

.field public static final cigna_help_coach_icon_size:I = 0x7f0a03b1

.field public static final cigna_help_coach_step1_description_txt_margin:I = 0x7f0a03c3

.field public static final cigna_help_coach_step1_height:I = 0x7f0a03be

.field public static final cigna_help_coach_step1_layout_height:I = 0x7f0a03b9

.field public static final cigna_help_coach_step1_layout_margin_left:I = 0x7f0a03bb

.field public static final cigna_help_coach_step1_layout_margin_right:I = 0x7f0a03bc

.field public static final cigna_help_coach_step1_margin_left:I = 0x7f0a03c0

.field public static final cigna_help_coach_step1_margin_top:I = 0x7f0a03c1

.field public static final cigna_help_coach_step1_name_txt_margin:I = 0x7f0a03c2

.field public static final cigna_help_coach_step1_width:I = 0x7f0a03bd

.field public static final cigna_help_coach_step2_height:I = 0x7f0a03bf

.field public static final cigna_help_coach_step2_layout_height:I = 0x7f0a03ba

.field public static final cigna_help_coach_step_padding_bottom:I = 0x7f0a03c4

.field public static final cigna_help_guide_description_txt_margin_left:I = 0x7f0a03b5

.field public static final cigna_help_guide_description_txt_margin_right:I = 0x7f0a03b6

.field public static final cigna_help_guide_description_txt_margin_top:I = 0x7f0a03b7

.field public static final cigna_help_guide_description_txt_size:I = 0x7f0a03b8

.field public static final cigna_help_guide_txt_margin_left:I = 0x7f0a03b3

.field public static final cigna_help_guide_txt_margin_top:I = 0x7f0a03b4

.field public static final cigna_help_infographic_layout_padding:I = 0x7f0a03a8

.field public static final cigna_icons_visibility_layout_margin:I = 0x7f0a0378

.field public static final cigna_info_left_count_txt_height:I = 0x7f0a035b

.field public static final cigna_info_left_count_txt_size:I = 0x7f0a035c

.field public static final cigna_info_left_title_txt_height:I = 0x7f0a035d

.field public static final cigna_info_left_title_txt_size:I = 0x7f0a035e

.field public static final cigna_item_composer_header_height:I = 0x7f0a03c5

.field public static final cigna_item_composer_section_header_text_height:I = 0x7f0a03c6

.field public static final cigna_item_composer_section_header_text_margin:I = 0x7f0a03c9

.field public static final cigna_item_composer_section_header_text_margin_bottom:I = 0x7f0a03c8

.field public static final cigna_item_composer_section_header_text_margin_top:I = 0x7f0a03c7

.field public static final cigna_library_base_list_empty_iv_iconSize:I = 0x7f0a02cd

.field public static final cigna_library_base_list_empty_txt_textSize:I = 0x7f0a02ce

.field public static final cigna_library_base_list_group_container_height:I = 0x7f0a02cf

.field public static final cigna_library_base_list_group_txt_height:I = 0x7f0a02d0

.field public static final cigna_library_base_list_group_txt_marginLeft:I = 0x7f0a02d1

.field public static final cigna_library_base_list_group_txt_paddingBottom:I = 0x7f0a02d3

.field public static final cigna_library_base_list_group_txt_paddingTop:I = 0x7f0a02d2

.field public static final cigna_library_base_list_group_txt_textSize:I = 0x7f0a02d4

.field public static final cigna_library_base_list_item_iconSize:I = 0x7f0a02c7

.field public static final cigna_library_base_list_item_icon_marginLeft:I = 0x7f0a02c8

.field public static final cigna_library_base_list_item_icon_marginRight:I = 0x7f0a02c9

.field public static final cigna_library_base_list_item_minHeight:I = 0x7f0a02c4

.field public static final cigna_library_base_list_item_paddingLeft:I = 0x7f0a02c5

.field public static final cigna_library_base_list_item_paddingRight:I = 0x7f0a02c6

.field public static final cigna_library_base_list_item_txt_subInfomation_height:I = 0x7f0a02cb

.field public static final cigna_library_base_list_item_txt_subInfomation_textSize:I = 0x7f0a02cc

.field public static final cigna_library_base_list_item_txt_title_textSize:I = 0x7f0a02ca

.field public static final cigna_library_child_checkbox_margin_left:I = 0x7f0a04b1

.field public static final cigna_library_child_checkbox_margin_right:I = 0x7f0a04b2

.field public static final cigna_library_custom_viewpager_indicator_rectangle_height:I = 0x7f0a02b9

.field public static final cigna_library_custom_viewpager_indicator_rectangle_top_margin:I = 0x7f0a02b7

.field public static final cigna_library_custom_viewpager_indicator_rectangle_width:I = 0x7f0a02b8

.field public static final cigna_library_divider_height:I = 0x7f0a04b0

.field public static final cigna_library_home_article_tip_icon_height:I = 0x7f0a02c0

.field public static final cigna_library_home_article_tip_icon_margin:I = 0x7f0a04b4

.field public static final cigna_library_home_article_tip_icon_width:I = 0x7f0a02bf

.field public static final cigna_library_home_article_tip_iv_background_height:I = 0x7f0a02bb

.field public static final cigna_library_home_article_tip_iv_height:I = 0x7f0a04b3

.field public static final cigna_library_home_article_tip_txt_title_height:I = 0x7f0a02bc

.field public static final cigna_library_home_article_tip_txt_title_paddingLeft:I = 0x7f0a02bd

.field public static final cigna_library_home_article_tip_txt_title_textSize:I = 0x7f0a02be

.field public static final cigna_library_home_indicator_viewpager_height:I = 0x7f0a02ba

.field public static final cigna_library_home_search_field_et_search_height:I = 0x7f0a04b5

.field public static final cigna_library_home_search_field_et_search_margin:I = 0x7f0a04b6

.field public static final cigna_library_home_search_field_et_search_width:I = 0x7f0a02c1

.field public static final cigna_library_tip_article_detail_webview_padding_left:I = 0x7f0a02c2

.field public static final cigna_library_tip_article_detail_webview_padding_right:I = 0x7f0a02c3

.field public static final cigna_life_style_2bottom_layout_margin:I = 0x7f0a032d

.field public static final cigna_life_style_description_height:I = 0x7f0a0325

.field public static final cigna_life_style_description_margin:I = 0x7f0a0326

.field public static final cigna_life_style_description_margin_top:I = 0x7f0a0327

.field public static final cigna_life_style_description_text_size:I = 0x7f0a0328

.field public static final cigna_life_style_divider_height:I = 0x7f0a0323

.field public static final cigna_life_style_graph_legend_heght:I = 0x7f0a0214

.field public static final cigna_life_style_progressbar_color_height:I = 0x7f0a0330

.field public static final cigna_life_style_progressbar_color_margin:I = 0x7f0a0331

.field public static final cigna_life_style_progressbar_height:I = 0x7f0a032e

.field public static final cigna_life_style_progressbar_margin:I = 0x7f0a032f

.field public static final cigna_life_style_progressbar_marking_img_margin:I = 0x7f0a0332

.field public static final cigna_life_style_progressbar_score_layout_height:I = 0x7f0a0333

.field public static final cigna_life_style_progressbar_score_margin:I = 0x7f0a0335

.field public static final cigna_life_style_progressbar_score_text_size:I = 0x7f0a0336

.field public static final cigna_life_style_progressbar_score_width:I = 0x7f0a0334

.field public static final cigna_life_style_reassess_description_margin_left:I = 0x7f0a03ef

.field public static final cigna_life_style_reassess_description_margin_top:I = 0x7f0a03ee

.field public static final cigna_life_style_reassess_description_text_size:I = 0x7f0a03f0

.field public static final cigna_life_style_text_description1_margin:I = 0x7f0a032b

.field public static final cigna_life_style_text_description2_margin:I = 0x7f0a032c

.field public static final cigna_life_style_text_description3_text_size:I = 0x7f0a0329

.field public static final cigna_life_style_text_layout_width:I = 0x7f0a0286

.field public static final cigna_life_style_text_title1_margin:I = 0x7f0a032a

.field public static final cigna_lifestyle_fragment_bottom_image_view_size:I = 0x7f0a03ce

.field public static final cigna_lifestyle_get_your_score_layout_height:I = 0x7f0a03d1

.field public static final cigna_lifestyle_get_your_score_layout_margin_right:I = 0x7f0a03d4

.field public static final cigna_lifestyle_get_your_score_layout_width:I = 0x7f0a03d2

.field public static final cigna_lifestyle_get_your_score_text_width:I = 0x7f0a03d3

.field public static final cigna_lifestyle_multi_score_chart_current_date_txt_margin:I = 0x7f0a03dd

.field public static final cigna_lifestyle_multi_score_chart_current_date_txt_size:I = 0x7f0a03de

.field public static final cigna_lifestyle_multi_score_chart_current_score_txt_height:I = 0x7f0a03e2

.field public static final cigna_lifestyle_multi_score_chart_current_score_txt_size:I = 0x7f0a03e3

.field public static final cigna_lifestyle_multi_score_info_area_layout_height:I = 0x7f0a03d8

.field public static final cigna_lifestyle_multi_score_iv_lifestyle_cap_height:I = 0x7f0a03da

.field public static final cigna_lifestyle_multi_score_iv_lifestyle_cap_width:I = 0x7f0a03d9

.field public static final cigna_lifestyle_multi_score_tv_comparison_score_txt_size:I = 0x7f0a03e4

.field public static final cigna_lifestyle_multi_score_tv_lifestyle_sub_msg_height:I = 0x7f0a03db

.field public static final cigna_lifestyle_multi_score_tv_lifestyle_sub_msg_text_size:I = 0x7f0a03dc

.field public static final cigna_lifestyle_multi_score_view_title_height:I = 0x7f0a03df

.field public static final cigna_lifestyle_multi_score_view_title_margin:I = 0x7f0a03e0

.field public static final cigna_lifestyle_multi_score_view_title_text_size:I = 0x7f0a03e1

.field public static final cigna_lifestyle_no_score_image_view_margin:I = 0x7f0a03e7

.field public static final cigna_lifestyle_no_score_image_view_size:I = 0x7f0a03e6

.field public static final cigna_lifestyle_no_score_layout_height:I = 0x7f0a03e5

.field public static final cigna_lifestyle_no_score_message_height:I = 0x7f0a03e8

.field public static final cigna_lifestyle_no_score_message_margin:I = 0x7f0a03e9

.field public static final cigna_lifestyle_no_score_message_text_size:I = 0x7f0a03ea

.field public static final cigna_lifestyle_no_score_remain_category1_margin:I = 0x7f0a03eb

.field public static final cigna_lifestyle_no_score_remain_category2_margin:I = 0x7f0a03ec

.field public static final cigna_lifestyle_one_score_bottom_layout_margin:I = 0x7f0a03f1

.field public static final cigna_lifestyle_one_score_layout_height:I = 0x7f0a03ed

.field public static final cigna_lifestyle_one_score_reassess_now_margin:I = 0x7f0a03cf

.field public static final cigna_lifestyle_one_score_reassess_now_text_size:I = 0x7f0a03d0

.field public static final cigna_lifestyle_score_bottom_left_btn_layout_width:I = 0x7f0a03ca

.field public static final cigna_lifestyle_score_bottom_left_text_size:I = 0x7f0a03cb

.field public static final cigna_lifestyle_score_bottom_padding:I = 0x7f0a03cd

.field public static final cigna_lifestyle_score_bottom_right_btn_layout_margin_left:I = 0x7f0a03cc

.field public static final cigna_lifestyle_score_coach_message_margin_left:I = 0x7f0a03f6

.field public static final cigna_lifestyle_score_coach_message_margin_right:I = 0x7f0a03f7

.field public static final cigna_lifestyle_score_coach_message_max_height:I = 0x7f0a03f4

.field public static final cigna_lifestyle_score_coach_message_min_height:I = 0x7f0a03f5

.field public static final cigna_lifestyle_score_coach_message_text_size:I = 0x7f0a03f8

.field public static final cigna_lifestyle_score_message_layout_height:I = 0x7f0a03f3

.field public static final cigna_lifestyle_score_message_layout_margin_left:I = 0x7f0a03fb

.field public static final cigna_lifestyle_score_message_layout_margin_right:I = 0x7f0a03fa

.field public static final cigna_lifestyle_score_message_layout_width:I = 0x7f0a03f2

.field public static final cigna_lifestyle_score_txt_margin_top:I = 0x7f0a03fd

.field public static final cigna_lifestyle_score_view_margin_right:I = 0x7f0a03f9

.field public static final cigna_lifestyle_scoreview_circle_margin_left:I = 0x7f0a01f8

.field public static final cigna_lifestyle_scoreview_circle_progress_size:I = 0x7f0a01f7

.field public static final cigna_lifestyle_scoreview_height:I = 0x7f0a01f6

.field public static final cigna_lifestyle_scoreview_text_margin_left:I = 0x7f0a01f9

.field public static final cigna_lifestyle_scoreview_text_margin_right:I = 0x7f0a01fa

.field public static final cigna_lifestyle_scoreview_text_size_middle:I = 0x7f0a01fc

.field public static final cigna_lifestyle_scoreview_text_size_top:I = 0x7f0a01fb

.field public static final cigna_list_popup_text_padding_left:I = 0x7f0a01f4

.field public static final cigna_list_popup_text_padding_right:I = 0x7f0a01f5

.field public static final cigna_list_popup_text_size:I = 0x7f0a03fe

.field public static final cigna_logo_height:I = 0x7f0a0300

.field public static final cigna_logo_margin:I = 0x7f0a0301

.field public static final cigna_logo_width:I = 0x7f0a02ff

.field public static final cigna_main_bottom_layout_height:I = 0x7f0a033f

.field public static final cigna_main_change_fragment_btn_size:I = 0x7f0a0405

.field public static final cigna_main_set_goal_btn_image_size:I = 0x7f0a0402

.field public static final cigna_main_set_goal_btn_layout_height:I = 0x7f0a0400

.field public static final cigna_main_set_goal_btn_layout_margin:I = 0x7f0a0401

.field public static final cigna_main_set_goal_btn_layout_width:I = 0x7f0a03ff

.field public static final cigna_memo_image_size:I = 0x7f0a0375

.field public static final cigna_mission_complete_1button_area_height:I = 0x7f0a0417

.field public static final cigna_mission_complete_badge_layout_margin_top:I = 0x7f0a0416

.field public static final cigna_mission_complete_button_ok_height:I = 0x7f0a0419

.field public static final cigna_mission_complete_button_ok_text_size:I = 0x7f0a041a

.field public static final cigna_mission_complete_button_ok_width:I = 0x7f0a0418

.field public static final cigna_mission_complete_button_right_margin_left:I = 0x7f0a041b

.field public static final cigna_mission_complete_logo_icon_height:I = 0x7f0a0414

.field public static final cigna_mission_complete_logo_icon_margin:I = 0x7f0a0415

.field public static final cigna_mission_complete_logo_icon_width:I = 0x7f0a0413

.field public static final cigna_no_data_image_view_height:I = 0x7f0a0427

.field public static final cigna_no_data_margin_top:I = 0x7f0a0426

.field public static final cigna_no_score_category_txt_margin_left:I = 0x7f0a0409

.field public static final cigna_no_score_category_txt_size:I = 0x7f0a040a

.field public static final cigna_no_score_exercise_txt_margin:I = 0x7f0a0386

.field public static final cigna_no_score_exercise_txt_size:I = 0x7f0a0387

.field public static final cigna_no_score_food_margin:I = 0x7f0a0388

.field public static final cigna_no_score_icon_size:I = 0x7f0a0408

.field public static final cigna_no_score_layout_height:I = 0x7f0a0406

.field public static final cigna_no_score_layout_margin_left:I = 0x7f0a0407

.field public static final cigna_notification_desc_message_margin_top:I = 0x7f0a0429

.field public static final cigna_notification_desc_message_text_size:I = 0x7f0a042a

.field public static final cigna_notification_head_message_margin:I = 0x7f0a042c

.field public static final cigna_notification_head_message_text_size:I = 0x7f0a042d

.field public static final cigna_notification_image_margin_top:I = 0x7f0a042b

.field public static final cigna_notification_two_btn_layout_height:I = 0x7f0a0428

.field public static final cigna_old_circle_progress_image_margin_left:I = 0x7f0a04b8

.field public static final cigna_old_circle_progress_image_margin_top:I = 0x7f0a04b9

.field public static final cigna_old_circle_progress_image_size:I = 0x7f0a04b7

.field public static final cigna_old_widget_cigna_logo_height:I = 0x7f0a04c2

.field public static final cigna_old_widget_cigna_logo_margin_right:I = 0x7f0a04c4

.field public static final cigna_old_widget_cigna_logo_margin_top:I = 0x7f0a04c3

.field public static final cigna_old_widget_cigna_logo_width:I = 0x7f0a04c1

.field public static final cigna_old_widget_header_score_area_margin_bottom:I = 0x7f0a04bb

.field public static final cigna_old_widget_header_score_area_margin_left:I = 0x7f0a04ba

.field public static final cigna_old_widget_header_score_view_height:I = 0x7f0a04bd

.field public static final cigna_old_widget_header_score_view_height_px:I = 0x7f0a04be

.field public static final cigna_old_widget_header_score_view_width:I = 0x7f0a04bc

.field public static final cigna_old_widget_initial_txt_height:I = 0x7f0a04cf

.field public static final cigna_old_widget_initial_txt_margin:I = 0x7f0a04d0

.field public static final cigna_old_widget_initial_txt_margin_top:I = 0x7f0a04d1

.field public static final cigna_old_widget_initial_txt_size:I = 0x7f0a04d2

.field public static final cigna_old_widget_lifestyle_score_txt_margin_right:I = 0x7f0a04da

.field public static final cigna_old_widget_mission_info_layout_padding_bottom:I = 0x7f0a04c6

.field public static final cigna_old_widget_mission_info_text_size:I = 0x7f0a04d6

.field public static final cigna_old_widget_mission_info_txt_margin:I = 0x7f0a04c5

.field public static final cigna_old_widget_no_score_text_size:I = 0x7f0a04d7

.field public static final cigna_old_widget_out_of_txt_margin_left:I = 0x7f0a04d4

.field public static final cigna_old_widget_plus_btn_margin:I = 0x7f0a04c0

.field public static final cigna_old_widget_plus_btn_margin_right:I = 0x7f0a04d5

.field public static final cigna_old_widget_plus_btn_size:I = 0x7f0a04bf

.field public static final cigna_old_widget_root_view_margin_top:I = 0x7f0a04c8

.field public static final cigna_old_widget_score_out_of_text_size:I = 0x7f0a04d8

.field public static final cigna_old_widget_score_padding_top:I = 0x7f0a04c7

.field public static final cigna_old_widget_score_text_size:I = 0x7f0a04d9

.field public static final cigna_old_widget_score_txt_margin_bottom:I = 0x7f0a04d3

.field public static final cigna_old_widget_title_sub_txt_height:I = 0x7f0a04cc

.field public static final cigna_old_widget_title_sub_txt_margin:I = 0x7f0a04cd

.field public static final cigna_old_widget_title_sub_txt_size:I = 0x7f0a04ce

.field public static final cigna_old_widget_title_txt_height:I = 0x7f0a04c9

.field public static final cigna_old_widget_title_txt_margin:I = 0x7f0a04ca

.field public static final cigna_old_widget_title_txt_size:I = 0x7f0a04cb

.field public static final cigna_plain_widget_1stline_margin_right:I = 0x7f0a04a0

.field public static final cigna_plain_widget_2ndline_margin_right:I = 0x7f0a04a1

.field public static final cigna_plain_widget_header_score_area_margin_bottom:I = 0x7f0a0493

.field public static final cigna_plain_widget_header_score_area_margin_right:I = 0x7f0a0491

.field public static final cigna_plain_widget_mission_info_txt_margin_right:I = 0x7f0a049e

.field public static final cigna_plain_widget_mission_info_txt_padding_left:I = 0x7f0a049d

.field public static final cigna_plain_widget_out_of_padding_bottom:I = 0x7f0a04ae

.field public static final cigna_plain_widget_out_of_txt_margin_left:I = 0x7f0a04ad

.field public static final cigna_plain_widget_plus_btn_margin_bottom:I = 0x7f0a0496

.field public static final cigna_plain_widget_plus_btn_margin_right:I = 0x7f0a0495

.field public static final cigna_plain_widget_score_out_of_text_size:I = 0x7f0a02b0

.field public static final cigna_plain_widget_score_text_size:I = 0x7f0a02b1

.field public static final cigna_plain_widget_score_txt_height:I = 0x7f0a04ab

.field public static final cigna_popup_content_padding:I = 0x7f0a02ac

.field public static final cigna_reassessment_completed_logo_icon_margin_bottom:I = 0x7f0a043b

.field public static final cigna_reassessment_completed_logo_icon_margin_top:I = 0x7f0a043a

.field public static final cigna_reassessment_less_than6_btn_divider_width:I = 0x7f0a0449

.field public static final cigna_reassessment_less_than6_btn_height:I = 0x7f0a0447

.field public static final cigna_reassessment_less_than6_btn_text_size:I = 0x7f0a0448

.field public static final cigna_reassessment_less_than_btn_height:I = 0x7f0a0442

.field public static final cigna_row_left_text_size:I = 0x7f0a0376

.field public static final cigna_row_right_text_size:I = 0x7f0a0377

.field public static final cigna_score_animation_icon_size:I = 0x7f0a040c

.field public static final cigna_score_animation_layout_height:I = 0x7f0a040b

.field public static final cigna_score_category_txt_margin_left:I = 0x7f0a040d

.field public static final cigna_score_category_txt_margin_right:I = 0x7f0a040e

.field public static final cigna_score_category_txt_size:I = 0x7f0a040f

.field public static final cigna_score_icon_bg_size:I = 0x7f0a01fd

.field public static final cigna_score_icon_margin_left:I = 0x7f0a0201

.field public static final cigna_score_icon_size:I = 0x7f0a0385

.field public static final cigna_score_layout_height:I = 0x7f0a0381

.field public static final cigna_score_layout_margin_left:I = 0x7f0a0382

.field public static final cigna_score_layout_margin_right:I = 0x7f0a0383

.field public static final cigna_score_layout_margin_top:I = 0x7f0a0384

.field public static final cigna_score_layout_width:I = 0x7f0a0202

.field public static final cigna_score_no_score_exercise_top_margin:I = 0x7f0a01fe

.field public static final cigna_score_no_score_top_margin:I = 0x7f0a01ff

.field public static final cigna_score_score_top_margin:I = 0x7f0a0200

.field public static final cigna_score_score_txt_margin:I = 0x7f0a0411

.field public static final cigna_score_score_txt_size:I = 0x7f0a0412

.field public static final cigna_score_score_txt_width:I = 0x7f0a0410

.field public static final cigna_search_cancel_image_margin:I = 0x7f0a02ea

.field public static final cigna_search_edit_height:I = 0x7f0a02e0

.field public static final cigna_search_edit_margin_left:I = 0x7f0a02e2

.field public static final cigna_search_edit_margin_right:I = 0x7f0a02e3

.field public static final cigna_search_edit_padding_left:I = 0x7f0a02e4

.field public static final cigna_search_edit_padding_right:I = 0x7f0a02e5

.field public static final cigna_search_icon_margin_left:I = 0x7f0a02e7

.field public static final cigna_search_icon_size:I = 0x7f0a02e6

.field public static final cigna_search_panel_buttons_size:I = 0x7f0a02df

.field public static final cigna_search_text_margin:I = 0x7f0a02e9

.field public static final cigna_search_text_side:I = 0x7f0a02e1

.field public static final cigna_search_text_width:I = 0x7f0a02e8

.field public static final cigna_select_all_checkbox_margin_left:I = 0x7f0a044c

.field public static final cigna_select_all_height:I = 0x7f0a02d5

.field public static final cigna_select_all_text_size:I = 0x7f0a044d

.field public static final cigna_spinner_child_height:I = 0x7f0a02b5

.field public static final cigna_spinner_group_height:I = 0x7f0a02b4

.field public static final cigna_spinner_group_text_margin_top:I = 0x7f0a02b6

.field public static final cigna_spinner_item_txt_padding_left:I = 0x7f0a044e

.field public static final cigna_spinner_item_txt_size:I = 0x7f0a044f

.field public static final cigna_suggest_detail_goal_view_divider_height:I = 0x7f0a045c

.field public static final cigna_suggest_detail_goal_view_divider_margin:I = 0x7f0a045e

.field public static final cigna_suggest_detail_goal_view_divider_margin_top:I = 0x7f0a045d

.field public static final cigna_suggest_reassess_btn_height:I = 0x7f0a0451

.field public static final cigna_suggest_reassess_btn_margin_bottom:I = 0x7f0a0452

.field public static final cigna_suggest_reassess_btn_margin_top:I = 0x7f0a0453

.field public static final cigna_suggest_reassess_btn_width:I = 0x7f0a0450

.field public static final cigna_suggested_bottom_image_bottom_margin:I = 0x7f0a0254

.field public static final cigna_suggested_bottom_image_right_margin:I = 0x7f0a0253

.field public static final cigna_suggested_btn_height:I = 0x7f0a045a

.field public static final cigna_suggested_detail_btn_height:I = 0x7f0a0266

.field public static final cigna_suggested_detail_contents_layout_height:I = 0x7f0a045b

.field public static final cigna_suggested_detail_contents_mission_margin:I = 0x7f0a045f

.field public static final cigna_suggested_detail_frequence_text_size:I = 0x7f0a029b

.field public static final cigna_suggested_detail_goal_frequency_margin_top:I = 0x7f0a026b

.field public static final cigna_suggested_detail_goal_question_response_top_margin:I = 0x7f0a026c

.field public static final cigna_suggested_detail_header_icon_margin:I = 0x7f0a025b

.field public static final cigna_suggested_detail_header_icon_margin_left:I = 0x7f0a0349

.field public static final cigna_suggested_detail_header_icon_margin_top:I = 0x7f0a047a

.field public static final cigna_suggested_detail_header_icon_size:I = 0x7f0a025a

.field public static final cigna_suggested_detail_header_starting_layout_height:I = 0x7f0a0482

.field public static final cigna_suggested_detail_header_starting_layout_margin:I = 0x7f0a0483

.field public static final cigna_suggested_detail_header_starting_txt_margin:I = 0x7f0a0484

.field public static final cigna_suggested_detail_header_starting_txt_size:I = 0x7f0a0485

.field public static final cigna_suggested_detail_header_title_margin_right:I = 0x7f0a025c

.field public static final cigna_suggested_detail_header_title_margin_top:I = 0x7f0a047b

.field public static final cigna_suggested_detail_header_title_text_size:I = 0x7f0a029a

.field public static final cigna_suggested_detail_sub_text_size:I = 0x7f0a0298

.field public static final cigna_suggested_detail_title_text_size:I = 0x7f0a0297

.field public static final cigna_suggested_detail_top_padding_bottom:I = 0x7f0a0268

.field public static final cigna_suggested_detail_top_padding_left:I = 0x7f0a0269

.field public static final cigna_suggested_detail_top_padding_right:I = 0x7f0a026a

.field public static final cigna_suggested_detail_top_padding_top:I = 0x7f0a0267

.field public static final cigna_suggested_detail_tracking_margin_top:I = 0x7f0a0265

.field public static final cigna_suggested_detail_tracking_text_size:I = 0x7f0a0299

.field public static final cigna_suggested_detail_txt_ll_margin_left:I = 0x7f0a0260

.field public static final cigna_suggested_detail_txt_ll_margin_right:I = 0x7f0a025d

.field public static final cigna_suggested_detail_txt_margin_bottom:I = 0x7f0a0264

.field public static final cigna_suggested_detail_txt_margin_top:I = 0x7f0a0263

.field public static final cigna_suggested_frequency_layout_margin_top:I = 0x7f0a047d

.field public static final cigna_suggested_frequency_layout_width:I = 0x7f0a047c

.field public static final cigna_suggested_goal_detail_frequency_text_size:I = 0x7f0a02aa

.field public static final cigna_suggested_goal_detail_question_response_text_size:I = 0x7f0a02ab

.field public static final cigna_suggested_goal_detail_top_text_size:I = 0x7f0a02a9

.field public static final cigna_suggested_goal_detail_txt_margin_bottom:I = 0x7f0a0262

.field public static final cigna_suggested_goal_detail_txt_margin_top:I = 0x7f0a0261

.field public static final cigna_suggested_goal_min_frequency_txt_margin:I = 0x7f0a046b

.field public static final cigna_suggested_header_view_height:I = 0x7f0a0459

.field public static final cigna_suggested_list_item_height:I = 0x7f0a0259

.field public static final cigna_suggested_list_item_icon_area_width:I = 0x7f0a0460

.field public static final cigna_suggested_list_item_icon_margin_left:I = 0x7f0a0462

.field public static final cigna_suggested_list_item_icon_margin_right:I = 0x7f0a0463

.field public static final cigna_suggested_list_item_icon_size:I = 0x7f0a0461

.field public static final cigna_suggested_list_item_padding:I = 0x7f0a0465

.field public static final cigna_suggested_list_item_padding_right:I = 0x7f0a0466

.field public static final cigna_suggested_list_item_txt_extraString_height:I = 0x7f0a0468

.field public static final cigna_suggested_list_item_txt_extraString_margin:I = 0x7f0a0469

.field public static final cigna_suggested_list_item_txt_extraString_size:I = 0x7f0a046a

.field public static final cigna_suggested_list_item_txt_title_size:I = 0x7f0a0467

.field public static final cigna_suggested_list_item_width:I = 0x7f0a0464

.field public static final cigna_suggested_more_btn_height:I = 0x7f0a0257

.field public static final cigna_suggested_more_btn_top_margin:I = 0x7f0a0258

.field public static final cigna_suggested_more_btn_width:I = 0x7f0a0256

.field public static final cigna_suggested_more_layout_height:I = 0x7f0a0255

.field public static final cigna_suggested_spinner_frequency_height:I = 0x7f0a0480

.field public static final cigna_suggested_spinner_frequency_layout_height:I = 0x7f0a047e

.field public static final cigna_suggested_spinner_frequency_layout_padding_top:I = 0x7f0a047f

.field public static final cigna_suggested_spinner_frequency_offset:I = 0x7f0a0481

.field public static final cigna_suggested_top_coach_message_height:I = 0x7f0a0474

.field public static final cigna_suggested_top_coach_message_margin_left:I = 0x7f0a0476

.field public static final cigna_suggested_top_coach_message_margin_top:I = 0x7f0a0475

.field public static final cigna_suggested_top_coach_message_max_height:I = 0x7f0a0478

.field public static final cigna_suggested_top_coach_message_min_height:I = 0x7f0a0477

.field public static final cigna_suggested_top_coach_message_text_size:I = 0x7f0a0479

.field public static final cigna_suggested_top_coach_message_width:I = 0x7f0a0473

.field public static final cigna_suggested_top_comment_left_margin:I = 0x7f0a0251

.field public static final cigna_suggested_top_comment_right_margin:I = 0x7f0a0252

.field public static final cigna_suggested_top_comment_top_margin:I = 0x7f0a0250

.field public static final cigna_suggested_top_content_height:I = 0x7f0a0470

.field public static final cigna_suggested_top_content_image_view_margin:I = 0x7f0a0472

.field public static final cigna_suggested_top_content_image_view_size:I = 0x7f0a0471

.field public static final cigna_suggested_top_title_margin:I = 0x7f0a046e

.field public static final cigna_suggested_top_title_margin_bottom:I = 0x7f0a046d

.field public static final cigna_suggested_top_title_margin_top:I = 0x7f0a046c

.field public static final cigna_suggested_top_title_text_size:I = 0x7f0a046f

.field public static final cigna_summary_bottom_txt_margin:I = 0x7f0a0403

.field public static final cigna_summary_bottom_txt_size:I = 0x7f0a0404

.field public static final cigna_summary_header_assess_btn_height:I = 0x7f0a03d5

.field public static final cigna_summary_header_assess_btn_margin_top:I = 0x7f0a03d6

.field public static final cigna_summary_header_assess_btn_text_size:I = 0x7f0a03d7

.field public static final cigna_summary_header_score_area_height:I = 0x7f0a03fc

.field public static final cigna_summary_header_view_layout_height:I = 0x7f0a0486

.field public static final cigna_terms_of_use:I = 0x7f0a04dc

.field public static final cigna_tips_expandable_group_header_divider:I = 0x7f0a0357

.field public static final cigna_tips_expandable_group_header_tv_height:I = 0x7f0a0355

.field public static final cigna_tips_expandable_group_header_tv_padding:I = 0x7f0a0356

.field public static final cigna_toast_text_size:I = 0x7f0a02b3

.field public static final cigna_top_sub_text_size:I = 0x7f0a0296

.field public static final cigna_top_text_size:I = 0x7f0a0295

.field public static final cigna_top_txt_margin_left:I = 0x7f0a025e

.field public static final cigna_top_txt_margin_right:I = 0x7f0a025f

.field public static final cigna_tracker_data_loading_image_height:I = 0x7f0a0220

.field public static final cigna_tracker_data_loading_image_top_margin:I = 0x7f0a0221

.field public static final cigna_tracker_data_loading_image_width:I = 0x7f0a021f

.field public static final cigna_tracker_data_loading_popup_bottom_padding:I = 0x7f0a021e

.field public static final cigna_tracker_data_loading_popup_left_padding:I = 0x7f0a021c

.field public static final cigna_tracker_data_loading_popup_right_padding:I = 0x7f0a021d

.field public static final cigna_tracker_data_loading_progress_height:I = 0x7f0a0223

.field public static final cigna_tracker_data_loading_progress_margin:I = 0x7f0a0224

.field public static final cigna_tracker_data_loading_progress_width:I = 0x7f0a0222

.field public static final cigna_tracker_data_loading_text_area_top_margin:I = 0x7f0a0226

.field public static final cigna_tracker_data_loading_text_area_width:I = 0x7f0a0225

.field public static final cigna_tracker_data_loading_tracking_text_size:I = 0x7f0a028c

.field public static final cigna_tv_reassessment_coach_description_first_margin:I = 0x7f0a0445

.field public static final cigna_tv_reassessment_coach_description_first_max_height:I = 0x7f0a0444

.field public static final cigna_tv_reassessment_coach_description_height:I = 0x7f0a0443

.field public static final cigna_tv_reassessment_coach_description_text_size:I = 0x7f0a0446

.field public static final cigna_tv_reassessment_goal_warning_message_margin:I = 0x7f0a0440

.field public static final cigna_tv_reassessment_goal_warning_message_text_size:I = 0x7f0a0441

.field public static final cigna_tv_reengaging_description_margin_top:I = 0x7f0a044a

.field public static final cigna_tv_reengaging_description_text_size:I = 0x7f0a044b

.field public static final cigna_txt_team_header_height:I = 0x7f0a0371

.field public static final cigna_txt_team_header_margin:I = 0x7f0a0372

.field public static final cigna_weight_editable_text_size:I = 0x7f0a048b

.field public static final cigna_weight_editable_text_view_height:I = 0x7f0a048a

.field public static final cigna_weight_inputmodule_dropdownlist_height:I = 0x7f0a048d

.field public static final cigna_weight_inputmodule_dropdownlist_marginRight:I = 0x7f0a048e

.field public static final cigna_weight_inputmodule_dropdownlist_width:I = 0x7f0a048c

.field public static final cigna_weight_root_center_height:I = 0x7f0a0488

.field public static final cigna_weight_root_center_view_width:I = 0x7f0a0489

.field public static final cigna_weight_root_center_width:I = 0x7f0a0487

.field public static final cigna_widget_cigna_logo_height:I = 0x7f0a0498

.field public static final cigna_widget_cigna_logo_margin_right:I = 0x7f0a049b

.field public static final cigna_widget_cigna_logo_margin_top:I = 0x7f0a0499

.field public static final cigna_widget_cigna_logo_margin_top_no_score:I = 0x7f0a049a

.field public static final cigna_widget_cigna_logo_score_mission_margin_right:I = 0x7f0a049c

.field public static final cigna_widget_cigna_logo_width:I = 0x7f0a0497

.field public static final cigna_widget_header_score_area_margin_bottom:I = 0x7f0a0492

.field public static final cigna_widget_height:I = 0x7f0a02ad

.field public static final cigna_widget_initial_txt_margin:I = 0x7f0a04a8

.field public static final cigna_widget_initial_txt_margin_top:I = 0x7f0a04a9

.field public static final cigna_widget_initial_txt_size:I = 0x7f0a04aa

.field public static final cigna_widget_lifestyle_score_txt_margin_right:I = 0x7f0a04af

.field public static final cigna_widget_mission_info_layout_margin_top:I = 0x7f0a049f

.field public static final cigna_widget_mission_info_text_size:I = 0x7f0a02af

.field public static final cigna_widget_no_score_text_size:I = 0x7f0a02b2

.field public static final cigna_widget_out_of_txt_margin_left:I = 0x7f0a04ac

.field public static final cigna_widget_plus_btn_size:I = 0x7f0a0494

.field public static final cigna_widget_title_sub_txt_height:I = 0x7f0a04a5

.field public static final cigna_widget_title_sub_txt_margin:I = 0x7f0a04a6

.field public static final cigna_widget_title_sub_txt_size:I = 0x7f0a04a7

.field public static final cigna_widget_title_txt_height:I = 0x7f0a04a2

.field public static final cigna_widget_title_txt_margin:I = 0x7f0a04a3

.field public static final cigna_widget_title_txt_size:I = 0x7f0a04a4

.field public static final cigna_widget_width:I = 0x7f0a02ae

.field public static final comfort_level_area_height:I = 0x7f0a0a0e

.field public static final comfort_level_area_hum_height:I = 0x7f0a0a13

.field public static final comfort_level_area_margin_all_left:I = 0x7f0a0a11

.field public static final comfort_level_area_margin_all_top:I = 0x7f0a0a12

.field public static final comfort_level_area_temp_width:I = 0x7f0a0a14

.field public static final comfort_level_area_width:I = 0x7f0a0a10

.field public static final common_cbx_general_layout_dimen:I = 0x7f0a01c1

.field public static final common_rb_general_layout_dimen:I = 0x7f0a01c2

.field public static final common_spn_shadow_radius:I = 0x7f0a01c0

.field public static final common_spn_shadow_x:I = 0x7f0a01be

.field public static final common_spn_shadow_y:I = 0x7f0a01bf

.field public static final common_spn_txt_general_textSize:I = 0x7f0a01c3

.field public static final common_wgt_general_layout_margin_lr:I = 0x7f0a01bd

.field public static final common_wgt_general_layout_margin_tb:I = 0x7f0a01bc

.field public static final compatible_device_connect_connect_layout_height:I = 0x7f0a0197

.field public static final compatible_device_connect_description_text_size:I = 0x7f0a019d

.field public static final compatible_device_connect_device_image_size:I = 0x7f0a0198

.field public static final compatible_device_connect_image_accessory_pair_height:I = 0x7f0a019b

.field public static final compatible_device_connect_image_accessory_pair_width:I = 0x7f0a019c

.field public static final compatible_device_connect_image_point_size:I = 0x7f0a0199

.field public static final compatible_device_connect_image_shealth_icon_size:I = 0x7f0a019a

.field public static final compatible_device_detail_bottom_notice_text_size:I = 0x7f0a0196

.field public static final compatible_device_detail_company_image_height:I = 0x7f0a0193

.field public static final compatible_device_detail_connectivity_image_height:I = 0x7f0a018f

.field public static final compatible_device_detail_connectivity_image_width:I = 0x7f0a0190

.field public static final compatible_device_detail_description_text_size:I = 0x7f0a0195

.field public static final compatible_device_detail_device_image_height:I = 0x7f0a018d

.field public static final compatible_device_detail_device_image_layout_height:I = 0x7f0a018b

.field public static final compatible_device_detail_device_image_layout_width:I = 0x7f0a018c

.field public static final compatible_device_detail_device_image_margin_left:I = 0x7f0a0191

.field public static final compatible_device_detail_device_image_width:I = 0x7f0a018e

.field public static final compatible_device_detail_device_name_text_size:I = 0x7f0a0192

.field public static final compatible_device_detail_web_site_text_size:I = 0x7f0a0194

.field public static final constrollerview_gradationbar_text_size:I = 0x7f0a0106

.field public static final controllerview_big_gradationbar_height_horizontal:I = 0x7f0a0100

.field public static final controllerview_big_gradationbar_height_vertical:I = 0x7f0a0103

.field public static final controllerview_gradationbar_horizontal_between_distance:I = 0x7f0a00ff

.field public static final controllerview_gradationbar_vertical_between_distance:I = 0x7f0a00fe

.field public static final controllerview_gradationbar_width:I = 0x7f0a0108

.field public static final controllerview_horizontal_bottomline_height:I = 0x7f0a0107

.field public static final controllerview_horizontal_height:I = 0x7f0a00f0

.field public static final controllerview_horizontal_width:I = 0x7f0a00ef

.field public static final controllerview_margin:I = 0x7f0a00f1

.field public static final controllerview_middle_gradationbar_height_horizontal:I = 0x7f0a0101

.field public static final controllerview_middle_gradationbar_height_vertical:I = 0x7f0a0104

.field public static final controllerview_small_gradationbar_height_horizontal:I = 0x7f0a0102

.field public static final controllerview_small_gradationbar_height_vertical:I = 0x7f0a0105

.field public static final controllerview_vertical_height:I = 0x7f0a00ee

.field public static final controllerview_vertical_width:I = 0x7f0a00ed

.field public static final date_bar_font_size:I = 0x7f0a0131

.field public static final date_bar_margin:I = 0x7f0a0132

.field public static final date_bar_padding:I = 0x7f0a0133

.field public static final dateselector_navibtn_height:I = 0x7f0a01e4

.field public static final dateselector_navibtn_width:I = 0x7f0a01e5

.field public static final dateselector_touch_of_arrow_width:I = 0x7f0a01e6

.field public static final dialog_bottom_padding:I = 0x7f0a008a

.field public static final dialog_side_padding:I = 0x7f0a008b

.field public static final dialog_title_header_height:I = 0x7f0a008c

.field public static final drawer_icon_padding_left:I = 0x7f0a01e8

.field public static final drawermenu_list_profile_height:I = 0x7f0a01e7

.field public static final drop_down_list_gap:I = 0x7f0a0523

.field public static final drop_down_list_item_height:I = 0x7f0a0525

.field public static final drop_down_list_item_width:I = 0x7f0a0524

.field public static final ex_inputactivity_memo_max:I = 0x7f0a069a

.field public static final exerdise_log_list_expand_button_margin_right:I = 0x7f0a06a6

.field public static final food_add_custom_food_calories_edit_text_width:I = 0x7f0a07ec

.field public static final food_add_custom_food_checkbox_text_size:I = 0x7f0a07e4

.field public static final food_add_custom_food_edit_text_calories_text_size:I = 0x7f0a07e1

.field public static final food_add_custom_food_edit_text_calories_width:I = 0x7f0a07e2

.field public static final food_add_custom_food_edit_text_padding_left:I = 0x7f0a07e3

.field public static final food_add_custom_food_height_of_calories_container:I = 0x7f0a07e9

.field public static final food_add_custom_food_height_of_container:I = 0x7f0a07e7

.field public static final food_add_custom_food_height_of_container_save_in_my_food:I = 0x7f0a07ed

.field public static final food_add_custom_food_height_of_view_another_field_btn:I = 0x7f0a07ef

.field public static final food_add_custom_food_name_of_food_edit_text_height:I = 0x7f0a07e8

.field public static final food_add_custom_food_padding_left:I = 0x7f0a07e5

.field public static final food_add_custom_food_padding_right:I = 0x7f0a07e6

.field public static final food_add_custom_food_text_size:I = 0x7f0a07e0

.field public static final food_add_custom_food_text_view_kcal_margin_left:I = 0x7f0a07ea

.field public static final food_add_custom_food_text_view_margin_bottom:I = 0x7f0a07eb

.field public static final food_add_custom_food_width_of_view_another_field_btn:I = 0x7f0a07ee

.field public static final food_add_to_my_food_popup_edit_text_margin_top:I = 0x7f0a0856

.field public static final food_add_to_my_food_popup_padding_bottom:I = 0x7f0a0853

.field public static final food_add_to_my_food_popup_padding_left:I = 0x7f0a0852

.field public static final food_add_to_my_food_popup_padding_right:I = 0x7f0a0854

.field public static final food_add_to_my_food_popup_text_size:I = 0x7f0a0855

.field public static final food_barcode_search_activity_bottom_view_height:I = 0x7f0a0801

.field public static final food_barcode_search_activity_bottom_view_text_size:I = 0x7f0a0806

.field public static final food_barcode_search_activity_center_view_height:I = 0x7f0a0803

.field public static final food_barcode_search_activity_center_view_merging_left_right:I = 0x7f0a0802

.field public static final food_barcode_search_activity_center_view_merging_top_bottom:I = 0x7f0a0804

.field public static final food_barcode_search_activity_frame_corner_image_view_size:I = 0x7f0a0805

.field public static final food_barcode_search_activity_top_view_height:I = 0x7f0a0800

.field public static final food_base_divider_size:I = 0x7f0a06bb

.field public static final food_bottom_button_height:I = 0x7f0a06b7

.field public static final food_bottom_button_split_line_height:I = 0x7f0a06b8

.field public static final food_date_time_buttons_btn_date_height:I = 0x7f0a06fb

.field public static final food_date_time_buttons_btn_date_margin_left:I = 0x7f0a06fc

.field public static final food_date_time_buttons_btn_date_margin_right:I = 0x7f0a06fd

.field public static final food_date_time_buttons_btn_date_textsize:I = 0x7f0a06fe

.field public static final food_date_time_buttons_btn_time_height:I = 0x7f0a06ff

.field public static final food_date_time_buttons_btn_time_margin_left:I = 0x7f0a0700

.field public static final food_date_time_buttons_btn_time_margin_right:I = 0x7f0a0701

.field public static final food_date_time_buttons_btn_time_textsize:I = 0x7f0a0702

.field public static final food_default_list_divider_height:I = 0x7f0a06b0

.field public static final food_gallery_view_item_size:I = 0x7f0a0787

.field public static final food_graph_axis_text_size:I = 0x7f0a084b

.field public static final food_graph_goal_line_thickness:I = 0x7f0a0850

.field public static final food_graph_goal_line_unit_text_size:I = 0x7f0a084d

.field public static final food_graph_goal_line_value_text_size:I = 0x7f0a084c

.field public static final food_graph_goal_line_x_offset:I = 0x7f0a084f

.field public static final food_graph_goal_line_y_offset:I = 0x7f0a084e

.field public static final food_graph_information_area_amount_view_text_size:I = 0x7f0a050d

.field public static final food_graph_information_area_amount_view_text_size_for_hour:I = 0x7f0a0842

.field public static final food_graph_information_area_avr_unit_text_size:I = 0x7f0a0843

.field public static final food_graph_information_area_for_hour_second_container_margin_left:I = 0x7f0a0849

.field public static final food_graph_information_area_padding_top_multi_item:I = 0x7f0a050c

.field public static final food_graph_information_area_pie_chart_hour_margin_right:I = 0x7f0a0847

.field public static final food_graph_information_area_pie_chart_margin_right:I = 0x7f0a0848

.field public static final food_graph_information_area_pie_chart_margin_top:I = 0x7f0a0846

.field public static final food_graph_information_area_subtitle_margin_top:I = 0x7f0a050f

.field public static final food_graph_information_area_subtitle_text_size:I = 0x7f0a050b

.field public static final food_graph_information_area_unit_view_margin_bottom:I = 0x7f0a0844

.field public static final food_graph_information_area_unit_view_text_size:I = 0x7f0a050e

.field public static final food_graph_information_area_unit_view_text_size_for_hour:I = 0x7f0a0845

.field public static final food_graph_separator_spacing_top_default:I = 0x7f0a0851

.field public static final food_graph_x_axis_text_space:I = 0x7f0a084a

.field public static final food_grid_image_holder_height:I = 0x7f0a050a

.field public static final food_grid_image_holder_width:I = 0x7f0a0509

.field public static final food_help_text_size:I = 0x7f0a085f

.field public static final food_image_grid_height:I = 0x7f0a0808

.field public static final food_image_grid_horizontal_spacing:I = 0x7f0a0809

.field public static final food_image_grid_right_most_view_margin_right:I = 0x7f0a080f

.field public static final food_image_grid_vertical_spacing:I = 0x7f0a080a

.field public static final food_image_grid_view_item_delete_button_height:I = 0x7f0a080b

.field public static final food_image_grid_view_item_delete_button_margin:I = 0x7f0a080d

.field public static final food_image_grid_view_item_delete_button_width:I = 0x7f0a080c

.field public static final food_image_grid_view_margin_of_items:I = 0x7f0a080e

.field public static final food_image_grid_width:I = 0x7f0a0807

.field public static final food_input_date_time_area_height:I = 0x7f0a070b

.field public static final food_input_memo_edit_text_single_line_layout_height:I = 0x7f0a0724

.field public static final food_input_memo_edit_text_single_line_padding_left:I = 0x7f0a0723

.field public static final food_input_memo_edit_text_single_line_padding_right:I = 0x7f0a0722

.field public static final food_list_header_height:I = 0x7f0a06b3

.field public static final food_list_header_min_height:I = 0x7f0a06b5

.field public static final food_list_header_padding_bottom:I = 0x7f0a06b4

.field public static final food_list_header_padding_left:I = 0x7f0a06d3

.field public static final food_list_header_text_size:I = 0x7f0a06b6

.field public static final food_list_popup_divider_height:I = 0x7f0a071b

.field public static final food_list_popup_item_height:I = 0x7f0a071a

.field public static final food_list_popup_item_text_size:I = 0x7f0a071e

.field public static final food_list_popup_text_padding_left:I = 0x7f0a071c

.field public static final food_list_popup_text_padding_right:I = 0x7f0a071d

.field public static final food_margin:I = 0x7f0a06b1

.field public static final food_meal_activity_add_button_image_size:I = 0x7f0a06e6

.field public static final food_meal_activity_add_button_text_view_padding_right:I = 0x7f0a06e7

.field public static final food_meal_activity_add_image_divider_height:I = 0x7f0a0714

.field public static final food_meal_activity_add_image_part_image_view_height:I = 0x7f0a06e3

.field public static final food_meal_activity_add_image_part_image_view_width:I = 0x7f0a06e4

.field public static final food_meal_activity_btn_image_margin_right:I = 0x7f0a06de

.field public static final food_meal_activity_btn_view_nutrition_info_height:I = 0x7f0a06e1

.field public static final food_meal_activity_btn_view_nutrition_info_width:I = 0x7f0a06e2

.field public static final food_meal_activity_comment_icon_padding_right:I = 0x7f0a06e9

.field public static final food_meal_activity_divider_line_height:I = 0x7f0a0721

.field public static final food_meal_activity_image_active_size:I = 0x7f0a06e5

.field public static final food_meal_activity_image_pager_delete_button_margin_right:I = 0x7f0a0710

.field public static final food_meal_activity_image_pager_delete_button_margin_top:I = 0x7f0a070f

.field public static final food_meal_activity_image_pager_delete_button_size:I = 0x7f0a070e

.field public static final food_meal_activity_image_pager_height:I = 0x7f0a070c

.field public static final food_meal_activity_image_pager_width:I = 0x7f0a070d

.field public static final food_meal_activity_margin_between_indicators:I = 0x7f0a0713

.field public static final food_meal_activity_margin_left:I = 0x7f0a06dc

.field public static final food_meal_activity_margin_right:I = 0x7f0a06dd

.field public static final food_meal_activity_meal_input_memo_header_height:I = 0x7f0a0720

.field public static final food_meal_activity_meal_input_memo_header_padding_right:I = 0x7f0a06fa

.field public static final food_meal_activity_meal_input_memo_header_text_size:I = 0x7f0a071f

.field public static final food_meal_activity_meal_item_view_container_calories_text_views_margin_bottom:I = 0x7f0a0719

.field public static final food_meal_activity_meal_item_view_container_calories_text_views_margin_right:I = 0x7f0a0717

.field public static final food_meal_activity_meal_item_view_container_calories_text_views_margin_top:I = 0x7f0a0718

.field public static final food_meal_activity_meal_item_view_image_divider_height:I = 0x7f0a0716

.field public static final food_meal_activity_meal_item_view_image_margin_right:I = 0x7f0a0715

.field public static final food_meal_activity_one_item_height:I = 0x7f0a06df

.field public static final food_meal_activity_one_item_text_size:I = 0x7f0a06e0

.field public static final food_meal_activity_page_indicator_height:I = 0x7f0a0712

.field public static final food_meal_activity_page_indicator_width:I = 0x7f0a0711

.field public static final food_meal_activity_text_view_total_calories_height:I = 0x7f0a06e8

.field public static final food_meal_date_left_margin:I = 0x7f0a0858

.field public static final food_meal_date_right_margin:I = 0x7f0a0859

.field public static final food_meal_date_time_image_dimens:I = 0x7f0a085e

.field public static final food_meal_date_time_title_text_height:I = 0x7f0a085b

.field public static final food_meal_date_time_title_text_size:I = 0x7f0a085a

.field public static final food_meal_date_time_value_text_height:I = 0x7f0a085d

.field public static final food_meal_date_time_value_text_size:I = 0x7f0a085c

.field public static final food_meal_input_top_meal_type_height:I = 0x7f0a0704

.field public static final food_meal_input_top_meal_type_margin_bottom:I = 0x7f0a070a

.field public static final food_meal_input_top_meal_type_margin_left:I = 0x7f0a0707

.field public static final food_meal_input_top_meal_type_margin_right:I = 0x7f0a0708

.field public static final food_meal_input_top_meal_type_margin_top:I = 0x7f0a0709

.field public static final food_meal_input_top_meal_type_min_height:I = 0x7f0a0703

.field public static final food_meal_input_top_meal_type_text_size:I = 0x7f0a0706

.field public static final food_meal_input_top_meal_type_width:I = 0x7f0a0705

.field public static final food_meal_plan_btn_date_height:I = 0x7f0a06ce

.field public static final food_meal_plan_btn_date_margin_left:I = 0x7f0a06cf

.field public static final food_meal_plan_btn_date_margin_minus:I = 0x7f0a06d2

.field public static final food_meal_plan_btn_date_margin_right:I = 0x7f0a06d0

.field public static final food_meal_plan_btn_date_text_size:I = 0x7f0a06d1

.field public static final food_meal_plan_btn_period_date_margin_left:I = 0x7f0a06d4

.field public static final food_meal_plan_divider_height:I = 0x7f0a06d7

.field public static final food_meal_plan_divider_width:I = 0x7f0a06d8

.field public static final food_meal_plan_eat_btn_item_height:I = 0x7f0a06d9

.field public static final food_meal_plan_eat_btn_view_plan_height:I = 0x7f0a06da

.field public static final food_meal_plan_eat_btn_view_plan_width:I = 0x7f0a06db

.field public static final food_meal_plan_list_preview_meal_type_text_size:I = 0x7f0a06c8

.field public static final food_meal_plan_list_preview_period_range_text_size:I = 0x7f0a06c9

.field public static final food_meal_plan_preview_list_item_height:I = 0x7f0a06cb

.field public static final food_meal_plan_preview_list_item_margin_left:I = 0x7f0a06cc

.field public static final food_meal_plan_preview_margin_left:I = 0x7f0a06cd

.field public static final food_meal_plan_preview_period_container_height:I = 0x7f0a06ca

.field public static final food_meal_plan_previous_and_next_button_drawable_padding:I = 0x7f0a06d6

.field public static final food_meal_plan_previous_and_next_button_text_size:I = 0x7f0a06d5

.field public static final food_meal_plan_select_meal_type_radio_button_height:I = 0x7f0a06bd

.field public static final food_meal_plan_select_meal_type_radio_button_margin_left:I = 0x7f0a06bf

.field public static final food_meal_plan_select_meal_type_radio_button_margin_right:I = 0x7f0a06c0

.field public static final food_meal_plan_select_meal_type_radio_button_text_size:I = 0x7f0a06be

.field public static final food_meal_plan_set_day_of_week_check_box_height:I = 0x7f0a06c5

.field public static final food_meal_plan_set_day_of_week_check_box_margin_left:I = 0x7f0a06c3

.field public static final food_meal_plan_set_day_of_week_check_box_margin_right:I = 0x7f0a06c4

.field public static final food_meal_plan_set_day_of_week_check_box_text_size:I = 0x7f0a06c7

.field public static final food_meal_plan_set_day_of_week_check_box_width:I = 0x7f0a06c6

.field public static final food_meal_plan_set_period_data_button_container_height:I = 0x7f0a06c1

.field public static final food_meal_plan_set_period_day_of_week_container_height:I = 0x7f0a06c2

.field public static final food_my_food_add_button_height:I = 0x7f0a074d

.field public static final food_my_food_caption_divider:I = 0x7f0a0857

.field public static final food_my_food_select_all_button_container_height:I = 0x7f0a074e

.field public static final food_no_connection_dialog_alert_padding_left_right:I = 0x7f0a083f

.field public static final food_no_connection_dialog_alert_padding_top_bottom:I = 0x7f0a083e

.field public static final food_no_connection_dialog_check_box_padding:I = 0x7f0a0841

.field public static final food_no_connection_dialog_text_size:I = 0x7f0a0840

.field public static final food_no_data_label_text_size:I = 0x7f0a06bc

.field public static final food_notes_as_text_view_margin_bottom:I = 0x7f0a06eb

.field public static final food_notes_as_text_view_margin_left:I = 0x7f0a06ec

.field public static final food_notes_as_text_view_margin_right:I = 0x7f0a06ed

.field public static final food_notes_as_text_view_margin_top:I = 0x7f0a06ea

.field public static final food_notes_as_text_view_text_size:I = 0x7f0a06ee

.field public static final food_notes_dialog_content_height:I = 0x7f0a06f9

.field public static final food_notes_dialog_height:I = 0x7f0a06f8

.field public static final food_notes_input_view_margin_bottom:I = 0x7f0a06f0

.field public static final food_notes_input_view_margin_left:I = 0x7f0a06f1

.field public static final food_notes_input_view_margin_right:I = 0x7f0a06f2

.field public static final food_notes_input_view_margin_top:I = 0x7f0a06ef

.field public static final food_notes_input_view_padding_bottom:I = 0x7f0a06f4

.field public static final food_notes_input_view_padding_left:I = 0x7f0a06f5

.field public static final food_notes_input_view_padding_right:I = 0x7f0a06f6

.field public static final food_notes_input_view_padding_top:I = 0x7f0a06f3

.field public static final food_notes_input_view_text_size:I = 0x7f0a06f7

.field public static final food_nutrition_info_item_height:I = 0x7f0a07c5

.field public static final food_nutrition_info_item_margin_top:I = 0x7f0a07c6

.field public static final food_nutrition_info_list_item_min_height:I = 0x7f0a07c7

.field public static final food_nutrition_item_button_link_height:I = 0x7f0a07c1

.field public static final food_nutrition_item_button_link_margin_top:I = 0x7f0a07c0

.field public static final food_nutrition_item_container_title_margin_bottom:I = 0x7f0a07b6

.field public static final food_nutrition_item_daily_values_info_margin_top:I = 0x7f0a07c2

.field public static final food_nutrition_item_dot_width:I = 0x7f0a07b5

.field public static final food_nutrition_item_main_padding_bottom:I = 0x7f0a07be

.field public static final food_nutrition_item_main_padding_top:I = 0x7f0a07bf

.field public static final food_nutrition_item_text_height:I = 0x7f0a07b2

.field public static final food_nutrition_item_text_margin_left:I = 0x7f0a07b3

.field public static final food_nutrition_item_text_max_width:I = 0x7f0a07b4

.field public static final food_nutrition_nutrition_item_default_text_size:I = 0x7f0a07ab

.field public static final food_nutrition_nutrition_item_drop_arrow_view_height:I = 0x7f0a07ae

.field public static final food_nutrition_nutrition_item_drop_arrow_view_width:I = 0x7f0a07af

.field public static final food_nutrition_nutrition_item_for_split_line_margin_left:I = 0x7f0a07b8

.field public static final food_nutrition_nutrition_item_for_split_line_margin_right:I = 0x7f0a07b7

.field public static final food_nutrition_nutrition_item_head_margin_left:I = 0x7f0a07b1

.field public static final food_nutrition_nutrition_item_head_margin_right:I = 0x7f0a07b0

.field public static final food_nutrition_nutrition_item_title_name_height:I = 0x7f0a07bc

.field public static final food_nutrition_nutrition_item_title_name_text_size:I = 0x7f0a07bb

.field public static final food_nutrition_nutrition_item_tv_meal_tile_name_margin_bottom:I = 0x7f0a07ba

.field public static final food_nutrition_nutrition_item_tv_meal_title_name_margin_top:I = 0x7f0a07b9

.field public static final food_nutrition_nutrition_list_footer_margin_right:I = 0x7f0a07c4

.field public static final food_nutrition_nutrition_list_footer_margin_top:I = 0x7f0a07c3

.field public static final food_nutrition_nutrition_split_line_13_margin_top:I = 0x7f0a07bd

.field public static final food_nutrition_value_info_provided_view_text_size:I = 0x7f0a07aa

.field public static final food_nutrition_value_info_small_text_size:I = 0x7f0a07ac

.field public static final food_nutrition_value_info_sub_view_text_size:I = 0x7f0a07ad

.field public static final food_pick_add_custom_food_button_container_height:I = 0x7f0a077b

.field public static final food_pick_add_custom_food_horizontal_margin:I = 0x7f0a077c

.field public static final food_pick_auto_complete_popup_height:I = 0x7f0a077a

.field public static final food_pick_category_item_left_margin:I = 0x7f0a0758

.field public static final food_pick_category_item_margin:I = 0x7f0a0778

.field public static final food_pick_category_item_text_size:I = 0x7f0a0777

.field public static final food_pick_category_title_bottom_line_height:I = 0x7f0a0779

.field public static final food_pick_category_title_height:I = 0x7f0a077d

.field public static final food_pick_frequent_title_text_size:I = 0x7f0a0774

.field public static final food_pick_frequent_title_text_view_padding_left:I = 0x7f0a0775

.field public static final food_pick_frequent_title_text_view_padding_top:I = 0x7f0a0776

.field public static final food_pick_list_check_left_margin:I = 0x7f0a075c

.field public static final food_pick_list_check_right_margin:I = 0x7f0a075d

.field public static final food_pick_list_item_calories_text_view_margin_top:I = 0x7f0a0753

.field public static final food_pick_list_item_calories_text_view_size:I = 0x7f0a0754

.field public static final food_pick_list_item_category_height:I = 0x7f0a074f

.field public static final food_pick_list_item_category_text_size:I = 0x7f0a0752

.field public static final food_pick_list_item_checkbox_size:I = 0x7f0a075e

.field public static final food_pick_list_item_divider:I = 0x7f0a0756

.field public static final food_pick_list_item_divider_height:I = 0x7f0a0755

.field public static final food_pick_list_item_height:I = 0x7f0a074c

.field public static final food_pick_list_item_product_text_view_size:I = 0x7f0a0751

.field public static final food_pick_list_item_right_margin:I = 0x7f0a0757

.field public static final food_pick_list_item_star_image_view_margin_right:I = 0x7f0a075f

.field public static final food_pick_list_item_star_image_view_size:I = 0x7f0a0750

.field public static final food_pick_list_text_holder_left_margin:I = 0x7f0a075b

.field public static final food_pick_list_text_holder_right_margin:I = 0x7f0a075a

.field public static final food_pick_list_text_holder_vert_margin:I = 0x7f0a0759

.field public static final food_pick_loading_footer_height:I = 0x7f0a0760

.field public static final food_pick_loading_footer_padding_left:I = 0x7f0a0761

.field public static final food_pick_loading_footer_text_margin_left:I = 0x7f0a0763

.field public static final food_pick_loading_footer_text_size:I = 0x7f0a0764

.field public static final food_pick_loading_progress_bar_size:I = 0x7f0a0762

.field public static final food_pick_my_food_fragment_child_list_item_margin_left:I = 0x7f0a077f

.field public static final food_pick_my_food_fragment_list_item_category_caption_padding_bottom:I = 0x7f0a0785

.field public static final food_pick_my_food_fragment_list_item_category_caption_padding_top:I = 0x7f0a0786

.field public static final food_pick_my_food_fragment_list_item_category_caption_text_size:I = 0x7f0a0783

.field public static final food_pick_my_food_fragment_list_item_expand_indicator_margin_right:I = 0x7f0a0781

.field public static final food_pick_my_food_fragment_list_item_expand_indicator_size:I = 0x7f0a0782

.field public static final food_pick_my_food_fragment_list_item_height:I = 0x7f0a0784

.field public static final food_pick_my_food_fragment_list_item_margin_left:I = 0x7f0a0780

.field public static final food_pick_my_food_fragment_no_data_padding_top:I = 0x7f0a077e

.field public static final food_pick_no_food_bottom_background_height:I = 0x7f0a076c

.field public static final food_pick_no_food_desc_margin_top:I = 0x7f0a076a

.field public static final food_pick_no_food_desc_size:I = 0x7f0a0769

.field public static final food_pick_no_food_image_size:I = 0x7f0a0766

.field public static final food_pick_no_food_layout_marginTop:I = 0x7f0a0765

.field public static final food_pick_no_food_margin_horizontal:I = 0x7f0a076b

.field public static final food_pick_no_food_text_size:I = 0x7f0a0768

.field public static final food_pick_no_search_food_image_margin_top:I = 0x7f0a0767

.field public static final food_pick_no_search_result_bottom_container_height:I = 0x7f0a076e

.field public static final food_pick_no_search_result_bottom_container_padding_bottom:I = 0x7f0a076d

.field public static final food_pick_search_cancel_image_margin:I = 0x7f0a0742

.field public static final food_pick_search_cancel_padding_right:I = 0x7f0a073f

.field public static final food_pick_search_edit_drawable_padding:I = 0x7f0a073c

.field public static final food_pick_search_edit_height:I = 0x7f0a0743

.field public static final food_pick_search_edit_margin_left:I = 0x7f0a073a

.field public static final food_pick_search_edit_margin_right:I = 0x7f0a073b

.field public static final food_pick_search_edit_padding_left:I = 0x7f0a0744

.field public static final food_pick_search_edit_padding_left_text:I = 0x7f0a0745

.field public static final food_pick_search_edit_padding_right:I = 0x7f0a0746

.field public static final food_pick_search_fragment_button_google_margin_top:I = 0x7f0a0773

.field public static final food_pick_search_fragment_button_height:I = 0x7f0a0771

.field public static final food_pick_search_fragment_button_manual_margin_top:I = 0x7f0a0772

.field public static final food_pick_search_fragment_button_width:I = 0x7f0a0770

.field public static final food_pick_search_icon_margin_left:I = 0x7f0a0748

.field public static final food_pick_search_icon_margin_left_default:I = 0x7f0a0749

.field public static final food_pick_search_icon_padding:I = 0x7f0a074a

.field public static final food_pick_search_icon_size:I = 0x7f0a0747

.field public static final food_pick_search_panel_buttons_side_margin:I = 0x7f0a0740

.field public static final food_pick_search_panel_buttons_size:I = 0x7f0a073e

.field public static final food_pick_search_panel_divider_width:I = 0x7f0a0741

.field public static final food_pick_search_panel_height:I = 0x7f0a0736

.field public static final food_pick_search_text_margin:I = 0x7f0a073d

.field public static final food_pick_search_text_side:I = 0x7f0a0739

.field public static final food_pick_search_text_width:I = 0x7f0a074b

.field public static final food_pick_selected_panel_divider_height:I = 0x7f0a0738

.field public static final food_pick_selected_panel_divider_layout_height:I = 0x7f0a07d4

.field public static final food_pick_selected_panel_divider_layout_marginLeft:I = 0x7f0a07d5

.field public static final food_pick_selected_panel_divider_layout_width:I = 0x7f0a07d3

.field public static final food_pick_selected_panel_height:I = 0x7f0a0737

.field public static final food_pick_selected_panel_item_image_view_size:I = 0x7f0a07d1

.field public static final food_pick_selected_panel_item_margin_left:I = 0x7f0a07d2

.field public static final food_pick_selected_panel_item_padding:I = 0x7f0a07cd

.field public static final food_pick_selected_panel_item_text_margin:I = 0x7f0a07ce

.field public static final food_pick_selected_panel_item_text_max_width:I = 0x7f0a07d0

.field public static final food_pick_selected_panel_item_text_size:I = 0x7f0a07cf

.field public static final food_pick_selected_panel_layout_height:I = 0x7f0a07cc

.field public static final food_pick_selected_panel_layout_margin_right:I = 0x7f0a07cb

.field public static final food_pick_selected_panel_layout_padding_left:I = 0x7f0a07ca

.field public static final food_pick_selected_panel_padding_left:I = 0x7f0a07c8

.field public static final food_pick_selected_panel_padding_right:I = 0x7f0a07c9

.field public static final food_pick_tab_container_height:I = 0x7f0a0733

.field public static final food_pick_tab_separator_width:I = 0x7f0a0735

.field public static final food_pick_tab_text_size:I = 0x7f0a0734

.field public static final food_pick_unable_to_find_text_size:I = 0x7f0a076f

.field public static final food_plugin_horizontal_black_divider_height:I = 0x7f0a06b9

.field public static final food_plugin_horizontal_green_divider_height:I = 0x7f0a06ba

.field public static final food_portion_size_controller_view_width:I = 0x7f0a0820

.field public static final food_portion_size_input_screen_edit_text_height:I = 0x7f0a0822

.field public static final food_portion_size_input_screen_edit_text_margin_bottom:I = 0x7f0a0824

.field public static final food_portion_size_input_screen_edit_text_margin_top:I = 0x7f0a0823

.field public static final food_portion_size_input_screen_edit_text_text_size:I = 0x7f0a0825

.field public static final food_portion_size_input_screen_edit_text_width:I = 0x7f0a0821

.field public static final food_portion_size_input_screen_unit_height:I = 0x7f0a0826

.field public static final food_portion_size_input_screen_unit_margin_left:I = 0x7f0a0828

.field public static final food_portion_size_input_screen_unit_margin_top:I = 0x7f0a0827

.field public static final food_portion_size_input_screen_weight_text_text_size:I = 0x7f0a081f

.field public static final food_portion_size_popup_button_container_size:I = 0x7f0a0729

.field public static final food_portion_size_popup_button_text_size:I = 0x7f0a0728

.field public static final food_portion_size_popup_carb_prot_fat_text_size:I = 0x7f0a072f

.field public static final food_portion_size_popup_checked_button_height:I = 0x7f0a072a

.field public static final food_portion_size_popup_content_edit_text_margin_bottom:I = 0x7f0a082a

.field public static final food_portion_size_popup_content_padding_left_right:I = 0x7f0a0829

.field public static final food_portion_size_popup_content_unit_text_margin_bottom:I = 0x7f0a082b

.field public static final food_portion_size_popup_header_padding_top:I = 0x7f0a0727

.field public static final food_portion_size_popup_header_text_size:I = 0x7f0a0726

.field public static final food_portion_size_popup_slider_bubble_text_size:I = 0x7f0a072e

.field public static final food_portion_size_popup_total_calories_margin_bottom:I = 0x7f0a072c

.field public static final food_portion_size_popup_total_calories_margin_top:I = 0x7f0a072d

.field public static final food_portion_size_popup_total_calories_text_size:I = 0x7f0a072b

.field public static final food_portion_size_slider_food_name_text_size:I = 0x7f0a0810

.field public static final food_portion_size_slider_handler_container_height:I = 0x7f0a0815

.field public static final food_portion_size_slider_handler_height:I = 0x7f0a0817

.field public static final food_portion_size_slider_handler_up_view_height:I = 0x7f0a0818

.field public static final food_portion_size_slider_handler_up_view_width:I = 0x7f0a0819

.field public static final food_portion_size_slider_handler_width:I = 0x7f0a0816

.field public static final food_portion_size_slider_portion_value_text_size:I = 0x7f0a0811

.field public static final food_portion_size_slider_scale_view_height:I = 0x7f0a0812

.field public static final food_portion_size_slider_scale_view_padding_left:I = 0x7f0a0813

.field public static final food_portion_size_slider_scale_view_padding_right:I = 0x7f0a0814

.field public static final food_precise_portion_size_popup_dropdown_button_margin_top:I = 0x7f0a081b

.field public static final food_precise_portion_size_popup_dropdown_button_padding_left:I = 0x7f0a081c

.field public static final food_precise_portion_size_popup_dropdown_button_width:I = 0x7f0a081d

.field public static final food_precise_portion_size_popup_padding_top:I = 0x7f0a081a

.field public static final food_precise_portion_size_popup_total_kcal_margin_top:I = 0x7f0a081e

.field public static final food_precise_unit_fragment_dropdown_button_padding_right:I = 0x7f0a0731

.field public static final food_precise_unit_fragment_dropdown_button_text_size:I = 0x7f0a0732

.field public static final food_precise_unit_fragment_inputs_height:I = 0x7f0a0730

.field public static final food_set_goal_input_module_include_margin_top:I = 0x7f0a07df

.field public static final food_set_goal_input_module_layout_margin_bottom:I = 0x7f0a07d8

.field public static final food_set_goal_input_module_margin_left:I = 0x7f0a0512

.field public static final food_set_goal_input_module_text_height:I = 0x7f0a0513

.field public static final food_set_goal_input_module_text_margin_top:I = 0x7f0a07db

.field public static final food_set_goal_input_module_text_size:I = 0x7f0a0514

.field public static final food_set_goal_input_module_text_width:I = 0x7f0a0511

.field public static final food_set_goal_input_module_title_height:I = 0x7f0a07d9

.field public static final food_set_goal_input_module_title_margin_top:I = 0x7f0a07da

.field public static final food_set_goal_input_module_title_text_size:I = 0x7f0a0510

.field public static final food_set_goal_input_module_unit_height:I = 0x7f0a07dc

.field public static final food_set_goal_input_module_unit_margin_bottom:I = 0x7f0a07de

.field public static final food_set_goal_input_module_unit_margin_top:I = 0x7f0a07dd

.field public static final food_set_goal_input_module_unit_text_size:I = 0x7f0a0515

.field public static final food_set_goal_title_container_recommended_text_height:I = 0x7f0a07d6

.field public static final food_set_goal_title_container_recommended_text_padding_left_right:I = 0x7f0a07d7

.field public static final food_set_goal_title_container_recommended_text_size:I = 0x7f0a0516

.field public static final food_summary_view_bottom_container_camera_button_margin_left:I = 0x7f0a078e

.field public static final food_summary_view_bottom_container_icon_dimen:I = 0x7f0a078b

.field public static final food_summary_view_bottom_container_my_food_button_margin_left:I = 0x7f0a078c

.field public static final food_summary_view_bottom_container_my_food_button_width:I = 0x7f0a078d

.field public static final food_summary_view_bottom_container_text_drawable_padding:I = 0x7f0a078f

.field public static final food_title_height:I = 0x7f0a06b2

.field public static final food_tracker_base_fragment_bottom_container_height:I = 0x7f0a0789

.field public static final food_tracker_base_fragment_main_container_height:I = 0x7f0a0788

.field public static final food_tracker_summary_view_bottom_container_height:I = 0x7f0a0504

.field public static final food_tracker_summary_view_bottom_container_text_size:I = 0x7f0a078a

.field public static final food_tracker_summary_view_main_container_item_add_button_height:I = 0x7f0a0508

.field public static final food_tracker_summary_view_main_container_item_add_button_width:I = 0x7f0a0795

.field public static final food_tracker_summary_view_main_container_item_double_line_text_size:I = 0x7f0a0506

.field public static final food_tracker_summary_view_main_container_item_eat_plan_button_height:I = 0x7f0a0797

.field public static final food_tracker_summary_view_main_container_item_eat_plan_button_width:I = 0x7f0a0796

.field public static final food_tracker_summary_view_main_container_item_eat_plan_image_margin_top:I = 0x7f0a0799

.field public static final food_tracker_summary_view_main_container_item_eat_plan_image_size:I = 0x7f0a0798

.field public static final food_tracker_summary_view_main_container_item_eat_plan_text_height:I = 0x7f0a079a

.field public static final food_tracker_summary_view_main_container_item_eat_plan_text_margin_top:I = 0x7f0a079b

.field public static final food_tracker_summary_view_main_container_item_eat_plan_text_size:I = 0x7f0a079c

.field public static final food_tracker_summary_view_main_container_item_height:I = 0x7f0a0503

.field public static final food_tracker_summary_view_main_container_item_image_margin_left:I = 0x7f0a0792

.field public static final food_tracker_summary_view_main_container_item_image_margin_right:I = 0x7f0a0793

.field public static final food_tracker_summary_view_main_container_item_image_size:I = 0x7f0a0794

.field public static final food_tracker_summary_view_main_container_item_plus_view_size:I = 0x7f0a0791

.field public static final food_tracker_summary_view_main_container_item_single_line_text_padding_left:I = 0x7f0a0790

.field public static final food_tracker_summary_view_main_container_item_single_line_text_size:I = 0x7f0a0505

.field public static final food_tracker_summary_view_main_container_item_text_size:I = 0x7f0a0507

.field public static final food_tracker_summary_view_top_container_achievement_medal_margin_left:I = 0x7f0a07a7

.field public static final food_tracker_summary_view_top_container_achievement_medal_size:I = 0x7f0a07a6

.field public static final food_tracker_summary_view_top_container_achievement_percents_view_text_size:I = 0x7f0a079d

.field public static final food_tracker_summary_view_top_container_circle_progress_margin_left:I = 0x7f0a07a5

.field public static final food_tracker_summary_view_top_container_circle_progress_size:I = 0x7f0a07a4

.field public static final food_tracker_summary_view_top_container_height:I = 0x7f0a0502

.field public static final food_tracker_summary_view_top_container_intake_goal_text_view_height:I = 0x7f0a07a2

.field public static final food_tracker_summary_view_top_container_intake_goal_text_view_text_size:I = 0x7f0a07a3

.field public static final food_tracker_summary_view_top_container_intake_text_height:I = 0x7f0a07a1

.field public static final food_tracker_summary_view_top_container_intake_text_view_height:I = 0x7f0a079f

.field public static final food_tracker_summary_view_top_container_intake_text_view_text_size:I = 0x7f0a07a0

.field public static final food_tracker_summary_view_top_container_intake_text_view_width:I = 0x7f0a079e

.field public static final food_tracker_summary_view_top_container_text_opacity:I = 0x7f0a07a9

.field public static final food_tracker_top_container_text_switcher_text_size:I = 0x7f0a07a8

.field public static final food_voice_search_error_dialog_bottom_height:I = 0x7f0a07fa

.field public static final food_voice_search_error_dialog_button_divider_width:I = 0x7f0a07f9

.field public static final food_voice_search_error_dialog_button_height:I = 0x7f0a07f7

.field public static final food_voice_search_error_dialog_button_text_size:I = 0x7f0a07fb

.field public static final food_voice_search_error_dialog_button_width:I = 0x7f0a07f8

.field public static final food_voice_search_error_dialog_message_padding_bottom:I = 0x7f0a07f6

.field public static final food_voice_search_error_dialog_message_padding_left:I = 0x7f0a07f4

.field public static final food_voice_search_error_dialog_message_padding_top:I = 0x7f0a07f5

.field public static final food_voice_search_error_dialog_message_text_size:I = 0x7f0a07f3

.field public static final food_voice_search_error_dialog_title_height:I = 0x7f0a07f0

.field public static final food_voice_search_error_dialog_title_padding_left:I = 0x7f0a07f2

.field public static final food_voice_search_error_dialog_title_text_size:I = 0x7f0a07f1

.field public static final food_voice_search_image_size:I = 0x7f0a07fe

.field public static final food_voice_search_language_text_size:I = 0x7f0a07ff

.field public static final food_voice_search_scale_mic_down:I = 0x7f0a0860

.field public static final food_voice_search_scale_mic_up:I = 0x7f0a0861

.field public static final food_voice_search_text_margin_bottom:I = 0x7f0a07fc

.field public static final food_voice_search_text_size:I = 0x7f0a07fd

.field public static final gradationview_gradationbar_text_size:I = 0x7f0a00f8

.field public static final gradationview_gradationbar_width:I = 0x7f0a00fb

.field public static final gradationview_horizontal_between_gradationbars_distance:I = 0x7f0a00fd

.field public static final gradationview_horizontal_big_gradationbar_height:I = 0x7f0a00f2

.field public static final gradationview_horizontal_bottomline_height:I = 0x7f0a00f9

.field public static final gradationview_horizontal_middle_gradationbar_height:I = 0x7f0a00f3

.field public static final gradationview_horizontal_small_gradationbar_height:I = 0x7f0a00f4

.field public static final gradationview_vertical_between_gradationbars_distance:I = 0x7f0a00fc

.field public static final gradationview_vertical_big_gradationbar_height:I = 0x7f0a00f5

.field public static final gradationview_vertical_bottomline_height:I = 0x7f0a00fa

.field public static final gradationview_vertical_middle_gradationbar_height:I = 0x7f0a00f6

.field public static final gradationview_vertical_small_gradationbar_height:I = 0x7f0a00f7

.field public static final graph_anime_bubble_body_margin:I = 0x7f0a013a

.field public static final graph_anime_bubble_d_height:I = 0x7f0a0140

.field public static final graph_anime_bubble_d_width:I = 0x7f0a013f

.field public static final graph_anime_bubble_height:I = 0x7f0a014a

.field public static final graph_anime_bubble_left_margin:I = 0x7f0a0138

.field public static final graph_anime_bubble_memo_height:I = 0x7f0a014b

.field public static final graph_anime_bubble_picker_height:I = 0x7f0a013b

.field public static final graph_anime_bubble_picker_width:I = 0x7f0a013c

.field public static final graph_anime_bubble_right_margin:I = 0x7f0a0139

.field public static final graph_anime_bubble_width:I = 0x7f0a0137

.field public static final graph_anime_column_width_day:I = 0x7f0a0141

.field public static final graph_anime_dot_size:I = 0x7f0a0136

.field public static final graph_anime_handler_text_size_day:I = 0x7f0a0150

.field public static final graph_anime_handler_width:I = 0x7f0a014f

.field public static final graph_anime_legend_height:I = 0x7f0a0149

.field public static final graph_anime_offset_axis_values_horizontal:I = 0x7f0a013e

.field public static final graph_anime_vertical_axis_text_size_wfl:I = 0x7f0a013d

.field public static final graph_anime_vertical_axis_values_padding:I = 0x7f0a0135

.field public static final graph_anime_weigh_values_line_height:I = 0x7f0a0143

.field public static final graph_anime_weigh_values_line_width:I = 0x7f0a0142

.field public static final graph_chart_abandoned_handler_item_height:I = 0x7f0a0055

.field public static final graph_chart_abandoned_handler_item_width:I = 0x7f0a0054

.field public static final graph_chart_bottom_chart_padding:I = 0x7f0a0060

.field public static final graph_chart_goal_label_text_size:I = 0x7f0a0063

.field public static final graph_chart_goal_line_thickness:I = 0x7f0a0061

.field public static final graph_chart_goal_text_margin_right:I = 0x7f0a0064

.field public static final graph_chart_goal_value_text_size:I = 0x7f0a0062

.field public static final graph_chart_handler_text_size:I = 0x7f0a0053

.field public static final graph_chart_holding_handler_for_year_item_width:I = 0x7f0a0219

.field public static final graph_chart_holding_handler_item_height:I = 0x7f0a0057

.field public static final graph_chart_holding_handler_item_space:I = 0x7f0a0058

.field public static final graph_chart_holding_handler_item_width:I = 0x7f0a0056

.field public static final graph_chart_left_chart_padding:I = 0x7f0a005d

.field public static final graph_chart_line_thickness:I = 0x7f0a004c

.field public static final graph_chart_right_chart_padding:I = 0x7f0a005e

.field public static final graph_chart_separator_stroke_width:I = 0x7f0a005c

.field public static final graph_chart_separator_text_size:I = 0x7f0a0059

.field public static final graph_chart_separator_text_spacing_top:I = 0x7f0a005a

.field public static final graph_chart_separator_width:I = 0x7f0a005b

.field public static final graph_chart_top_chart_padding:I = 0x7f0a005f

.field public static final graph_chart_x_axis_stroke_width:I = 0x7f0a004d

.field public static final graph_chart_x_axis_text_size:I = 0x7f0a004e

.field public static final graph_chart_x_axis_text_space:I = 0x7f0a004f

.field public static final graph_chart_y_axis_label_text_size:I = 0x7f0a0052

.field public static final graph_chart_y_axis_text_size:I = 0x7f0a0050

.field public static final graph_chart_y_axis_text_space:I = 0x7f0a0051

.field public static final graph_general_view_height:I = 0x7f0a01ef

.field public static final graph_information_area_amount_layout_margin_top:I = 0x7f0a0044

.field public static final graph_information_area_amount_view_height:I = 0x7f0a0041

.field public static final graph_information_area_amount_view_text_size:I = 0x7f0a0043

.field public static final graph_information_area_amount_view_without_label_text_size:I = 0x7f0a0045

.field public static final graph_information_area_date_value_height:I = 0x7f0a0048

.field public static final graph_information_area_item_title_area_height:I = 0x7f0a0039

.field public static final graph_information_area_padding_top_multi_item:I = 0x7f0a0047

.field public static final graph_information_area_padding_top_single_item:I = 0x7f0a0046

.field public static final graph_information_area_pie_chart_size:I = 0x7f0a004b

.field public static final graph_information_area_subtitle_margin_top:I = 0x7f0a003d

.field public static final graph_information_area_subtitle_text_size:I = 0x7f0a003c

.field public static final graph_information_area_unit_margin_left:I = 0x7f0a0042

.field public static final graph_information_area_unit_view_average_label_text_size:I = 0x7f0a003f

.field public static final graph_information_area_unit_view_height:I = 0x7f0a003a

.field public static final graph_information_area_unit_view_margin_left:I = 0x7f0a0040

.field public static final graph_information_area_unit_view_padding_bottom:I = 0x7f0a004a

.field public static final graph_information_area_unit_view_text_size:I = 0x7f0a003e

.field public static final graph_information_container_height:I = 0x7f0a01f0

.field public static final graph_information_date_text_size:I = 0x7f0a003b

.field public static final graph_information_item_container_height:I = 0x7f0a0049

.field public static final graph_legend_area_button_size:I = 0x7f0a006b

.field public static final graph_legend_area_layout_margin_top:I = 0x7f0a006a

.field public static final graph_legend_area_legend_mark_height:I = 0x7f0a0066

.field public static final graph_legend_area_legend_mark_margin_left:I = 0x7f0a0067

.field public static final graph_legend_area_legend_mark_text_size:I = 0x7f0a006e

.field public static final graph_legend_area_legend_mark_width:I = 0x7f0a0065

.field public static final graph_legend_area_mark_margin_left:I = 0x7f0a0069

.field public static final graph_legend_area_text_mark_width:I = 0x7f0a0068

.field public static final graph_no_data_view_height:I = 0x7f0a01ee

.field public static final graph_separator_spacing_top_default:I = 0x7f0a006c

.field public static final graph_text_mark_view_left_offset:I = 0x7f0a006d

.field public static final grid_height:I = 0x7f0a05ce

.field public static final grid_padding:I = 0x7f0a05cf

.field public static final health_board_drag_n_drop_text_padding:I = 0x7f0a016b

.field public static final health_easywidget_single_screen_height:I = 0x7f0a05dd

.field public static final health_easywidget_single_screen_width:I = 0x7f0a05dc

.field public static final health_measuring_background_height:I = 0x7f0a05d8

.field public static final health_measuring_background_widget_height:I = 0x7f0a05da

.field public static final health_measuring_background_widget_width:I = 0x7f0a05db

.field public static final health_measuring_background_width:I = 0x7f0a05d9

.field public static final health_measuring_height:I = 0x7f0a05d4

.field public static final health_measuring_widget_height:I = 0x7f0a05d6

.field public static final health_measuring_widget_width:I = 0x7f0a05d7

.field public static final health_measuring_width:I = 0x7f0a05d5

.field public static final height_graph:I = 0x7f0a0a0c

.field public static final helphub_help_list_item_extra_line_spacing:I = 0x7f0a06a9

.field public static final helphub_help_list_item_text_size:I = 0x7f0a06aa

.field public static final holo_circle_width:I = 0x7f0a051c

.field public static final home_dash_board_margin_top:I = 0x7f0a05c0

.field public static final home_dash_container_height:I = 0x7f0a05bf

.field public static final home_fragment_layout_dashboard_height:I = 0x7f0a04f3

.field public static final home_fragment_layout_scrollview_margin_top:I = 0x7f0a04f4

.field public static final home_grid_default_height:I = 0x7f0a05bd

.field public static final home_icon_side_margin:I = 0x7f0a05c1

.field public static final home_icon_side_margin_end:I = 0x7f0a05c2

.field public static final home_lateset_data_list_item_layout_icon_layout_height:I = 0x7f0a05c6

.field public static final home_lateset_data_list_item_layout_icon_layout_marginLeft:I = 0x7f0a05c7

.field public static final home_lateset_data_list_item_layout_icon_layout_marginRight:I = 0x7f0a05c8

.field public static final home_lateset_data_list_item_layout_icon_layout_width:I = 0x7f0a05c5

.field public static final home_lateset_data_list_item_layout_marginLeft:I = 0x7f0a05c3

.field public static final home_lateset_data_list_item_layout_marginRight:I = 0x7f0a05c4

.field public static final home_lateset_data_list_item_layout_value_layout_marginBottom:I = 0x7f0a05cd

.field public static final home_lateset_data_list_item_layout_value_layout_marginRight:I = 0x7f0a05cc

.field public static final home_lateset_data_list_item_layout_wearable_icon_layout_height:I = 0x7f0a05ca

.field public static final home_lateset_data_list_item_layout_wearable_icon_layout_marginBottom:I = 0x7f0a05cb

.field public static final home_lateset_data_list_item_layout_wearable_icon_layout_width:I = 0x7f0a05c9

.field public static final home_main_bg_height:I = 0x7f0a05be

.field public static final home_more_option_devider_height:I = 0x7f0a059c

.field public static final home_more_option_popup_width:I = 0x7f0a059b

.field public static final home_title_default_size:I = 0x7f0a051f

.field public static final horizontal_axis_vs_handler_height:I = 0x7f0a0148

.field public static final hr_input_screen_scroll_content_layout_height:I = 0x7f0a05f0

.field public static final hrm_icon_scale_height:I = 0x7f0a05ed

.field public static final hrm_icon_scale_width:I = 0x7f0a05ec

.field public static final hrm_log_child_body_check_left:I = 0x7f0a05e6

.field public static final hrm_log_child_body_check_right:I = 0x7f0a05e7

.field public static final hrm_log_child_body_left:I = 0x7f0a05e4

.field public static final hrm_log_child_body_right:I = 0x7f0a05e5

.field public static final hrm_log_child_header_height:I = 0x7f0a05e1

.field public static final hrm_log_child_header_left:I = 0x7f0a05e2

.field public static final hrm_log_child_header_right:I = 0x7f0a05e3

.field public static final hrm_log_detail_icon_left:I = 0x7f0a05e8

.field public static final hrm_log_detail_icon_right:I = 0x7f0a05e9

.field public static final hrm_log_detail_right:I = 0x7f0a05ea

.field public static final hrm_log_group_expand_left:I = 0x7f0a05df

.field public static final hrm_log_group_expand_right:I = 0x7f0a05e0

.field public static final hrm_log_group_title_left:I = 0x7f0a05de

.field public static final hrm_log_item_layout_height:I = 0x7f0a05eb

.field public static final hrm_scover_icon_scale_height:I = 0x7f0a05ef

.field public static final hrm_scover_icon_scale_width:I = 0x7f0a05ee

.field public static final init_profile_card_top_layout_height:I = 0x7f0a06ad

.field public static final initial_profile_intro_initial_profile_intro_layout_margintop:I = 0x7f0a04e4

.field public static final initial_profile_intro_linear_layout_margintop:I = 0x7f0a04e5

.field public static final initial_profile_start_layout_imageview_resolution:I = 0x7f0a04e8

.field public static final initial_profile_start_layout_textview_marginbottom:I = 0x7f0a04e6

.field public static final initial_profile_start_layout_textview_margitop:I = 0x7f0a04e7

.field public static final input_activity_date_button_height:I = 0x7f0a01ce

.field public static final input_activity_date_button_margin_left:I = 0x7f0a01cf

.field public static final input_activity_date_button_margin_right:I = 0x7f0a01d0

.field public static final input_activity_date_button_text_size:I = 0x7f0a01d1

.field public static final input_activity_date_button_width:I = 0x7f0a01cd

.field public static final input_activity_datetime_layout_height:I = 0x7f0a01cc

.field public static final input_activity_memo_header_height:I = 0x7f0a01d8

.field public static final input_activity_memo_header_margin_top:I = 0x7f0a01d9

.field public static final input_activity_memo_header_strip_height:I = 0x7f0a01e0

.field public static final input_activity_memo_header_text_height:I = 0x7f0a01da

.field public static final input_activity_memo_header_text_margin_bottom:I = 0x7f0a01dc

.field public static final input_activity_memo_header_text_margin_top:I = 0x7f0a01db

.field public static final input_activity_memo_header_text_padding_left:I = 0x7f0a01dd

.field public static final input_activity_memo_header_text_padding_right:I = 0x7f0a01de

.field public static final input_activity_memo_header_text_size:I = 0x7f0a01df

.field public static final input_activity_memo_height:I = 0x7f0a01d7

.field public static final input_activity_time_button_height:I = 0x7f0a01d3

.field public static final input_activity_time_button_margin_left:I = 0x7f0a01d4

.field public static final input_activity_time_button_margin_right:I = 0x7f0a01d5

.field public static final input_activity_time_button_text_size:I = 0x7f0a01d6

.field public static final input_activity_time_button_width:I = 0x7f0a01d2

.field public static final inputmodule_height:I = 0x7f0a012f

.field public static final inputmodule_horizontal_image_button_margin_left:I = 0x7f0a0111

.field public static final inputmodule_horizontal_image_button_margin_right:I = 0x7f0a0110

.field public static final inputmodule_middle_layout_margin_bottom:I = 0x7f0a0112

.field public static final inputmodule_padding_left:I = 0x7f0a012d

.field public static final inputmodule_padding_right:I = 0x7f0a012e

.field public static final inputmodule_pin_margin_left:I = 0x7f0a0130

.field public static final inputmodule_top_layout_edittext_height:I = 0x7f0a010c

.field public static final inputmodule_top_layout_edittext_margin_bottom:I = 0x7f0a010e

.field public static final inputmodule_top_layout_edittext_text_size:I = 0x7f0a010b

.field public static final inputmodule_top_layout_edittext_width:I = 0x7f0a010d

.field public static final inputmodule_top_layout_height:I = 0x7f0a0109

.field public static final inputmodule_top_layout_margin_bottom:I = 0x7f0a010a

.field public static final inputmodule_top_layout_texview_text_size:I = 0x7f0a010f

.field public static final inputmodule_vertical_image_button_margin_bottom:I = 0x7f0a0121

.field public static final inputmodule_vertical_image_button_margin_top:I = 0x7f0a0120

.field public static final inputmodule_vertical_middle_layout_button_margin_top:I = 0x7f0a011e

.field public static final inputmodule_vertical_middle_layout_button_size:I = 0x7f0a011d

.field public static final inputmodule_vertical_middle_layout_controller_view_margin_top:I = 0x7f0a011f

.field public static final inputmodule_vertical_middle_layout_margin_top:I = 0x7f0a011c

.field public static final inputmodule_vertical_top_layout_checkbox_layout_height:I = 0x7f0a05f4

.field public static final inputmodule_vertical_top_layout_checkbox_margin_left:I = 0x7f0a05f5

.field public static final inputmodule_vertical_top_layout_edittable_height:I = 0x7f0a0118

.field public static final inputmodule_vertical_top_layout_edittable_margin_top:I = 0x7f0a0117

.field public static final inputmodule_vertical_top_layout_edittable_text_size:I = 0x7f0a0119

.field public static final inputmodule_vertical_top_layout_layout_width:I = 0x7f0a0114

.field public static final inputmodule_vertical_top_layout_margin_bottom:I = 0x7f0a0113

.field public static final inputmodule_vertical_top_layout_margin_top:I = 0x7f0a05f1

.field public static final inputmodule_vertical_top_layout_textview_margin_left:I = 0x7f0a05f2

.field public static final inputmodule_vertical_top_layout_textview_size:I = 0x7f0a05f3

.field public static final inputmodule_vertical_top_layout_textview_title_height:I = 0x7f0a0115

.field public static final inputmodule_vertical_top_layout_textview_title_text_size:I = 0x7f0a0116

.field public static final inputmodule_vertical_top_layout_textview_unit_margin_top:I = 0x7f0a011a

.field public static final inputmodule_vertical_top_layout_textview_unit_text_size:I = 0x7f0a011b

.field public static final legend_height_graph:I = 0x7f0a0134

.field public static final legend_mark_max_width:I = 0x7f0a006f

.field public static final list_item_header_title_arrow_width:I = 0x7f0a0725

.field public static final list_item_layout_item_icon_layout_height:I = 0x7f0a05aa

.field public static final list_item_layout_item_icon_layout_marginLeft:I = 0x7f0a05ab

.field public static final list_item_layout_item_icon_layout_marginRight:I = 0x7f0a05ac

.field public static final list_item_layout_item_icon_layout_width:I = 0x7f0a05a9

.field public static final list_item_layout_text_date_layout_marginRight:I = 0x7f0a05ae

.field public static final list_item_layout_text_date_textSize:I = 0x7f0a05af

.field public static final list_item_layout_text_value_textSize:I = 0x7f0a05b0

.field public static final list_item_layout_title__textSize:I = 0x7f0a05ad

.field public static final list_popup_divider_height:I = 0x7f0a00e9

.field public static final list_popup_empty_space_height:I = 0x7f0a09d2

.field public static final list_popup_height:I = 0x7f0a00e7

.field public static final list_popup_item_height:I = 0x7f0a00e6

.field public static final list_popup_text_padding_left:I = 0x7f0a00ea

.field public static final list_popup_text_padding_right:I = 0x7f0a00eb

.field public static final list_popup_text_size:I = 0x7f0a00ec

.field public static final list_popup_width:I = 0x7f0a00e8

.field public static final lock_widget_item_height:I = 0x7f0a06ab

.field public static final lock_widget_text_size:I = 0x7f0a06ac

.field public static final log_list_data_row_height:I = 0x7f0a0010

.field public static final log_list_data_row_height_weight:I = 0x7f0a0011

.field public static final log_list_expand_button_height:I = 0x7f0a0017

.field public static final log_list_expand_button_margin_right:I = 0x7f0a0018

.field public static final log_list_expand_button_margin_top:I = 0x7f0a0019

.field public static final log_list_expand_button_width:I = 0x7f0a0016

.field public static final log_list_group_header_height:I = 0x7f0a0012

.field public static final log_list_left_text_margin_left:I = 0x7f0a0015

.field public static final log_list_left_text_margin_top:I = 0x7f0a0014

.field public static final log_list_left_text_size:I = 0x7f0a0013

.field public static final log_list_no_data_label_text_size:I = 0x7f0a0038

.field public static final log_list_right_row_part_margin_right:I = 0x7f0a001f

.field public static final log_list_row_accessory_image_height:I = 0x7f0a0024

.field public static final log_list_row_accessory_image_width:I = 0x7f0a0023

.field public static final log_list_row_bottom_text_size:I = 0x7f0a001e

.field public static final log_list_row_check_box_margin_left:I = 0x7f0a0029

.field public static final log_list_row_check_box_margin_right:I = 0x7f0a0028

.field public static final log_list_row_divider_height:I = 0x7f0a002a

.field public static final log_list_row_divider_left_margin:I = 0x7f0a0027

.field public static final log_list_row_left_image_margin_left:I = 0x7f0a0034

.field public static final log_list_row_left_image_size:I = 0x7f0a0035

.field public static final log_list_row_left_text_height:I = 0x7f0a001a

.field public static final log_list_row_left_text_margin_left:I = 0x7f0a001b

.field public static final log_list_row_left_text_size:I = 0x7f0a001d

.field public static final log_list_row_left_text_with_icon_margin_left:I = 0x7f0a001c

.field public static final log_list_row_medal_image_size:I = 0x7f0a0036

.field public static final log_list_row_medal_margin_left:I = 0x7f0a0037

.field public static final log_list_row_memo_image_height:I = 0x7f0a0021

.field public static final log_list_row_memo_image_width:I = 0x7f0a0020

.field public static final log_list_row_right_text_margin_top:I = 0x7f0a0026

.field public static final log_list_row_right_text_size:I = 0x7f0a0025

.field public static final log_list_row_text_margin:I = 0x7f0a0022

.field public static final log_list_team_header_height:I = 0x7f0a002b

.field public static final log_list_team_header_left_text_left_margin:I = 0x7f0a002d

.field public static final log_list_team_header_medal_bottom_margin:I = 0x7f0a0030

.field public static final log_list_team_header_right_text_height:I = 0x7f0a0033

.field public static final log_list_team_header_right_text_right_margin:I = 0x7f0a0032

.field public static final log_list_team_header_separator_height:I = 0x7f0a002c

.field public static final log_list_team_header_text_bottom_margin:I = 0x7f0a002f

.field public static final log_list_team_header_text_size:I = 0x7f0a0031

.field public static final log_list_team_header_text_top_margin:I = 0x7f0a002e

.field public static final log_no_data_popup_height:I = 0x7f0a01e1

.field public static final log_no_data_popup_right_margin:I = 0x7f0a01e2

.field public static final log_no_data_view_margin_bottom:I = 0x7f0a01e3

.field public static final max_height_listpopup:I = 0x7f0a0094

.field public static final more_option_popup_width:I = 0x7f0a059a

.field public static final my_accessory_manager_layour_accessory_info_padding_bottom:I = 0x7f0a0540

.field public static final my_accessory_manager_layour_accessory_info_padding_left:I = 0x7f0a0541

.field public static final my_accessory_manager_layour_accessory_info_padding_top:I = 0x7f0a0542

.field public static final my_accessory_manager_layour_accessory_info_text_size:I = 0x7f0a0543

.field public static final my_accessory_manager_layour_bluetooth_icon_height:I = 0x7f0a0532

.field public static final my_accessory_manager_layour_bluetooth_icon_margin_left:I = 0x7f0a0533

.field public static final my_accessory_manager_layour_bluetooth_icon_width:I = 0x7f0a0531

.field public static final my_accessory_manager_layour_gear_connected_status_height:I = 0x7f0a0537

.field public static final my_accessory_manager_layour_gear_connected_status_text_size:I = 0x7f0a0538

.field public static final my_accessory_manager_layour_gear_name_layout_margin_left:I = 0x7f0a0534

.field public static final my_accessory_manager_layour_gear_name_layout_margin_right:I = 0x7f0a0535

.field public static final my_accessory_manager_layour_gear_name_text_size:I = 0x7f0a0536

.field public static final my_accessory_manager_layour_gear_split_line_height:I = 0x7f0a053a

.field public static final my_accessory_manager_layour_gear_split_line_height_28:I = 0x7f0a053b

.field public static final my_accessory_manager_layour_gear_split_line_width:I = 0x7f0a0539

.field public static final my_accessory_manager_layour_list_selector_height:I = 0x7f0a0530

.field public static final my_accessory_manager_layour_paried_gear_height:I = 0x7f0a052d

.field public static final my_accessory_manager_layour_paried_gear_margin_left:I = 0x7f0a052e

.field public static final my_accessory_manager_layour_paried_gear_text_size:I = 0x7f0a052f

.field public static final my_accessory_manager_layour_paried_title_text_size:I = 0x7f0a0544

.field public static final my_accessory_manager_layour_scanning_layout_margin_right:I = 0x7f0a0548

.field public static final my_accessory_manager_layour_search_image_height:I = 0x7f0a0550

.field public static final my_accessory_manager_layour_search_image_width:I = 0x7f0a054f

.field public static final my_accessory_manager_layour_search_layout_height:I = 0x7f0a054d

.field public static final my_accessory_manager_layour_search_text_margin_top:I = 0x7f0a054e

.field public static final my_accessory_manager_layour_search_text_size:I = 0x7f0a0551

.field public static final my_accessory_manager_layour_setting_progressbar_spinner_height:I = 0x7f0a054a

.field public static final my_accessory_manager_layour_setting_progressbar_spinner_width:I = 0x7f0a0549

.field public static final my_accessory_manager_layour_setting_textView_Status_margin_left:I = 0x7f0a0546

.field public static final my_accessory_manager_layour_setting_textView_Status_text_size:I = 0x7f0a0547

.field public static final my_accessory_manager_layour_stop_btn_height:I = 0x7f0a0552

.field public static final my_accessory_manager_layour_stop_btn_text_size:I = 0x7f0a0553

.field public static final my_accessory_manager_layour_sub_title_height:I = 0x7f0a0545

.field public static final my_accessory_manager_layour_sync_icon_height:I = 0x7f0a053d

.field public static final my_accessory_manager_layour_sync_icon_margin_left:I = 0x7f0a053e

.field public static final my_accessory_manager_layour_sync_icon_margin_right:I = 0x7f0a053f

.field public static final my_accessory_manager_layour_sync_icon_width:I = 0x7f0a053c

.field public static final my_accessory_manager_layour_watch_scnning_margin_left:I = 0x7f0a054b

.field public static final my_accessory_manager_layour_watch_scnning_text_size:I = 0x7f0a054c

.field public static final offset_bottom_graph:I = 0x7f0a014d

.field public static final ok_cancel_buttons_double_line_height:I = 0x7f0a0098

.field public static final ok_cancel_buttons_height_disclaimer:I = 0x7f0a0a18

.field public static final ok_cancel_buttons_padding_bottom:I = 0x7f0a0097

.field public static final ok_cancel_buttons_padding_side:I = 0x7f0a0096

.field public static final ok_cancel_buttons_padding_top:I = 0x7f0a0095

.field public static final ok_cancel_buttons_single_line_height:I = 0x7f0a007f

.field public static final ok_cancel_buttons_text_size:I = 0x7f0a007a

.field public static final options_menu_item_text_size:I = 0x7f0a007e

.field public static final pading:I = 0x7f0a0a0b

.field public static final pedometer_ranking_update_time:I = 0x7f0a0a59

.field public static final pop_content_text_padding_bottom:I = 0x7f0a0076

.field public static final popup_bloodglucose_switch_padding_bottom:I = 0x7f0a0079

.field public static final popup_comfort_level_height:I = 0x7f0a0a15

.field public static final popup_content_date_time_margin_bottom:I = 0x7f0a0077

.field public static final popup_content_padding_bottom:I = 0x7f0a0091

.field public static final popup_content_padding_left:I = 0x7f0a0074

.field public static final popup_content_padding_right:I = 0x7f0a0075

.field public static final popup_content_padding_top:I = 0x7f0a0090

.field public static final popup_datetime_title_padding:I = 0x7f0a0093

.field public static final popup_image_height:I = 0x7f0a0a1a

.field public static final popup_margin:I = 0x7f0a008d

.field public static final popup_shadow_size:I = 0x7f0a008e

.field public static final popup_text1_height:I = 0x7f0a0a19

.field public static final popup_text2_height:I = 0x7f0a0a1b

.field public static final popup_text_content_padding:I = 0x7f0a0092

.field public static final popup_text_content_text_size:I = 0x7f0a0078

.field public static final popup_text_date_time_size:I = 0x7f0a007d

.field public static final popup_text_value__symbol_size:I = 0x7f0a007c

.field public static final popup_text_value_size:I = 0x7f0a007b

.field public static final popup_title_height:I = 0x7f0a0071

.field public static final popup_title_padding_left:I = 0x7f0a0073

.field public static final popup_title_text_size:I = 0x7f0a0072

.field public static final popup_width:I = 0x7f0a0070

.field public static final popup_width_content:I = 0x7f0a008f

.field public static final pro_exercise_chart_margin_bottom_4:I = 0x7f0a069f

.field public static final pro_exercise_chart_margin_top_17:I = 0x7f0a069d

.field public static final pro_exercise_chart_margin_top_25:I = 0x7f0a069e

.field public static final pro_exercise_hrm_icon_scale_height:I = 0x7f0a069c

.field public static final pro_exercise_hrm_icon_scale_width:I = 0x7f0a069b

.field public static final pro_exercise_pro_activity_type_height:I = 0x7f0a09c6

.field public static final pro_exercise_pro_activity_type_postion_adjust:I = 0x7f0a09c5

.field public static final pro_exercise_pro_activity_type_width:I = 0x7f0a09c4

.field public static final pro_image_grid_view_margin_of_items:I = 0x7f0a09b3

.field public static final profile_attribute_item_edit_text_container_height:I = 0x7f0a00df

.field public static final profile_basic_infomation:I = 0x7f0a0593

.field public static final profile_card_fifth_image_layout_height:I = 0x7f0a04e9

.field public static final profile_card_fifth_imageview_margintop:I = 0x7f0a04ea

.field public static final profile_card_fifth_linear_layout_height1:I = 0x7f0a04ec

.field public static final profile_card_fifth_linear_layout_height2:I = 0x7f0a04ed

.field public static final profile_card_fifth_textview_height:I = 0x7f0a04eb

.field public static final profile_card_image_height:I = 0x7f0a05a2

.field public static final profile_card_text_top_margin:I = 0x7f0a05a3

.field public static final profile_center_height:I = 0x7f0a04db

.field public static final profile_drop_down_list_margin_left:I = 0x7f0a05a5

.field public static final profile_drop_down_list_text_size:I = 0x7f0a05a4

.field public static final profile_edit__weight_min_width:I = 0x7f0a058f

.field public static final profile_edit_ft_min_width:I = 0x7f0a058e

.field public static final profile_edit_text_min_width:I = 0x7f0a0590

.field public static final profile_edit_text_width_1:I = 0x7f0a02d6

.field public static final profile_edit_text_width_2:I = 0x7f0a02d7

.field public static final profile_first_view_extra_height:I = 0x7f0a04de

.field public static final profile_height_handle_left_margin:I = 0x7f0a0586

.field public static final profile_height_handle_margin_bottom:I = 0x7f0a0587

.field public static final profile_height_handle_margin_small_meter_start_offset:I = 0x7f0a0589

.field public static final profile_height_handle_margin_top:I = 0x7f0a0588

.field public static final profile_height_handle_right_margin:I = 0x7f0a0585

.field public static final profile_height_handler_line_stroke_size:I = 0x7f0a0584

.field public static final profile_height_handler_size:I = 0x7f0a0583

.field public static final profile_heigt_meter_input_module_height:I = 0x7f0a058c

.field public static final profile_heigt_meter_line_stroke:I = 0x7f0a058d

.field public static final profile_heigt_meter_offset_bottom:I = 0x7f0a058b

.field public static final profile_heigt_meter_offset_top:I = 0x7f0a058a

.field public static final profile_image_height:I = 0x7f0a059e

.field public static final profile_image_width:I = 0x7f0a059f

.field public static final profile_init_setting_card_step_height:I = 0x7f0a0527

.field public static final profile_init_setting_card_step_line_height:I = 0x7f0a0528

.field public static final profile_init_setting_card_step_line_width:I = 0x7f0a0529

.field public static final profile_input_module_profile_editable_container_height:I = 0x7f0a04f0

.field public static final profile_input_module_profile_editable_container_prime_textsize:I = 0x7f0a04f2

.field public static final profile_input_module_profile_editable_container_textsize:I = 0x7f0a04f1

.field public static final profile_input_module_root_center_height:I = 0x7f0a04ee

.field public static final profile_input_module_root_center_view_height:I = 0x7f0a04ef

.field public static final profile_next_bottom_height:I = 0x7f0a0592

.field public static final profile_second_position:I = 0x7f0a059d

.field public static final profile_spinner_text_margin_top:I = 0x7f0a0591

.field public static final progressbar_label_height:I = 0x7f0a012c

.field public static final quick_input_image_panel_bottom_text_size:I = 0x7f0a0832

.field public static final quick_input_image_panel_calories_unit_height:I = 0x7f0a082d

.field public static final quick_input_image_panel_center_text_size:I = 0x7f0a0831

.field public static final quick_input_image_panel_height:I = 0x7f0a082c

.field public static final quick_input_image_panel_total_cal_text_size:I = 0x7f0a082f

.field public static final quick_input_image_panel_total_text_height:I = 0x7f0a082e

.field public static final quick_input_image_panel_total_text_size:I = 0x7f0a0830

.field public static final s4_screen_height:I = 0x7f0a0147

.field public static final samsung_account_signin_account_image_height:I = 0x7f0a052a

.field public static final samsung_account_signin_account_logo_height:I = 0x7f0a052b

.field public static final samsung_account_signin_text_size:I = 0x7f0a052c

.field public static final scanning_description_height:I = 0x7f0a01ea

.field public static final scanning_noitem_bg_height:I = 0x7f0a01e9

.field public static final scanning_sub_tabs_width:I = 0x7f0a01eb

.field public static final set_quick_input_pop_up_calorie_text_height:I = 0x7f0a083a

.field public static final set_quick_input_pop_up_calorie_text_size:I = 0x7f0a083b

.field public static final set_quick_input_pop_up_edit_text_height:I = 0x7f0a0834

.field public static final set_quick_input_pop_up_edit_text_margin_right:I = 0x7f0a0835

.field public static final set_quick_input_pop_up_edit_text_size:I = 0x7f0a0836

.field public static final set_quick_input_pop_up_edit_text_width:I = 0x7f0a0833

.field public static final set_quick_input_pop_up_header_margin_bottom:I = 0x7f0a0839

.field public static final set_quick_input_pop_up_header_text_height:I = 0x7f0a0837

.field public static final set_quick_input_pop_up_header_text_size:I = 0x7f0a0838

.field public static final set_quick_input_pop_up_layout_height:I = 0x7f0a083c

.field public static final set_quick_input_pop_up_layout_vertical_padding:I = 0x7f0a083d

.field public static final settings_item:I = 0x7f0a0520

.field public static final settings_margin:I = 0x7f0a0521

.field public static final settings_screen_textsize:I = 0x7f0a06ae

.field public static final share_app_icon_size:I = 0x7f0a0080

.field public static final share_app_item_height:I = 0x7f0a0083

.field public static final share_app_item_width:I = 0x7f0a0082

.field public static final share_app_name_text_width:I = 0x7f0a0081

.field public static final share_grid_items_padding:I = 0x7f0a0084

.field public static final share_grid_items_padding_bottom:I = 0x7f0a0088

.field public static final share_grid_items_padding_left:I = 0x7f0a0085

.field public static final share_grid_items_padding_right:I = 0x7f0a0086

.field public static final share_grid_items_padding_top:I = 0x7f0a0087

.field public static final share_grid_items_spacing_vertical:I = 0x7f0a0089

.field public static final sharevia_home_date_text_width:I = 0x7f0a01c5

.field public static final sharevia_home_text_width:I = 0x7f0a01c4

.field public static final sharevia_other_text_width:I = 0x7f0a01c6

.field public static final sharevia_text_height:I = 0x7f0a01c7

.field public static final sleep_mate_bottom_button_height:I = 0x7f0a09cd

.field public static final sleep_mate_first_bottom_button_icon:I = 0x7f0a09d0

.field public static final sleep_mate_first_bottom_button_left_padding:I = 0x7f0a09ce

.field public static final sleep_mate_first_bottom_button_right_padding:I = 0x7f0a09cf

.field public static final sleep_mate_first_bottom_button_width:I = 0x7f0a09d1

.field public static final spo2_actionbar_more_popup_width:I = 0x7f0a09d3

.field public static final spo2_log_child_body_check_left:I = 0x7f0a09dd

.field public static final spo2_log_child_body_check_right:I = 0x7f0a09de

.field public static final spo2_log_child_body_left:I = 0x7f0a09db

.field public static final spo2_log_child_body_right:I = 0x7f0a09dc

.field public static final spo2_log_child_header_height:I = 0x7f0a09d8

.field public static final spo2_log_child_header_left:I = 0x7f0a09d9

.field public static final spo2_log_child_header_right:I = 0x7f0a09da

.field public static final spo2_log_detail_icon_left:I = 0x7f0a09df

.field public static final spo2_log_detail_icon_right:I = 0x7f0a09e0

.field public static final spo2_log_detail_right:I = 0x7f0a09e1

.field public static final spo2_log_group_expand_left:I = 0x7f0a09d6

.field public static final spo2_log_group_expand_right:I = 0x7f0a09d7

.field public static final spo2_log_group_main_layout_button_height:I = 0x7f0a09f9

.field public static final spo2_log_group_main_layout_button_margin_right:I = 0x7f0a09fa

.field public static final spo2_log_group_main_layout_button_width:I = 0x7f0a09f8

.field public static final spo2_log_group_main_layout_height:I = 0x7f0a09f6

.field public static final spo2_log_group_main_layout_textsize:I = 0x7f0a09f7

.field public static final spo2_log_group_subtitle_left:I = 0x7f0a09d5

.field public static final spo2_log_group_title_left:I = 0x7f0a09d4

.field public static final spo2_log_group_txtGroupLeft_margin_left:I = 0x7f0a09f5

.field public static final spo2_log_item_icon_image_height:I = 0x7f0a09ef

.field public static final spo2_log_item_icon_image_margin_left:I = 0x7f0a09f0

.field public static final spo2_log_item_icon_image_width:I = 0x7f0a09ee

.field public static final spo2_log_item_info_image_height:I = 0x7f0a09eb

.field public static final spo2_log_item_info_image_margin_left:I = 0x7f0a09ec

.field public static final spo2_log_item_info_image_margin_right:I = 0x7f0a09ed

.field public static final spo2_log_item_info_image_width:I = 0x7f0a09ea

.field public static final spo2_log_item_layout_height:I = 0x7f0a09e9

.field public static final spo2_log_maingroup_layout_height:I = 0x7f0a09e2

.field public static final spo2_log_subgroup_goal_image_height:I = 0x7f0a09e7

.field public static final spo2_log_subgroup_goal_image_width:I = 0x7f0a09e6

.field public static final spo2_log_subgroup_goal_margin_left:I = 0x7f0a09e8

.field public static final spo2_log_subgroup_layout_margin_left:I = 0x7f0a09e3

.field public static final spo2_log_subgroup_layout_margin_right:I = 0x7f0a09e4

.field public static final spo2_log_subgroup_main_layout_margin_left:I = 0x7f0a09f2

.field public static final spo2_log_subgroup_main_layout_margin_right:I = 0x7f0a09f3

.field public static final spo2_log_subgroup_sub_layout_margin_left:I = 0x7f0a09f4

.field public static final spo2_log_subgroup_text_height:I = 0x7f0a09e5

.field public static final spo2_log_txtgrouprowright_margin_top:I = 0x7f0a09f1

.field public static final stamp_edit_full_height:I = 0x7f0a0556

.field public static final stamp_edit_height:I = 0x7f0a055a

.field public static final stamp_edit_view_page_margin:I = 0x7f0a055b

.field public static final stamp_exercise_pro_graph_margin:I = 0x7f0a055e

.field public static final stamp_exercise_pro_graph_side_margin:I = 0x7f0a055f

.field public static final stamp_full_height:I = 0x7f0a0555

.field public static final stamp_full_width:I = 0x7f0a0554

.field public static final stamp_home_height:I = 0x7f0a055d

.field public static final stamp_home_width:I = 0x7f0a055c

.field public static final stamp_item_height:I = 0x7f0a0557

.field public static final stamp_thermo_hygrometer_full_height:I = 0x7f0a0558

.field public static final stamp_thermo_hygrometer_height:I = 0x7f0a0559

.field public static final stm_icon_scale_height:I = 0x7f0a0a09

.field public static final stm_icon_scale_width:I = 0x7f0a0a08

.field public static final stm_log_child_body_check_left:I = 0x7f0a0a03

.field public static final stm_log_child_body_check_right:I = 0x7f0a0a04

.field public static final stm_log_child_body_left:I = 0x7f0a0a01

.field public static final stm_log_child_body_right:I = 0x7f0a0a02

.field public static final stm_log_child_header_height:I = 0x7f0a09fe

.field public static final stm_log_child_header_left:I = 0x7f0a09ff

.field public static final stm_log_child_header_right:I = 0x7f0a0a00

.field public static final stm_log_detail_icon_left:I = 0x7f0a0a05

.field public static final stm_log_detail_icon_right:I = 0x7f0a0a06

.field public static final stm_log_detail_right:I = 0x7f0a0a07

.field public static final stm_log_group_expand_left:I = 0x7f0a09fc

.field public static final stm_log_group_expand_right:I = 0x7f0a09fd

.field public static final stm_log_group_title_left:I = 0x7f0a09fb

.field public static final summary_fragment_datebar_height:I = 0x7f0a01ec

.field public static final summary_fragment_datebar_height_data:I = 0x7f0a01ed

.field public static final summary_pressure_logo_size:I = 0x7f0a00a5

.field public static final summary_screen_accessory_connection_bottom_margin:I = 0x7f0a00e2

.field public static final summary_screen_accessory_connection_left_margin:I = 0x7f0a00e1

.field public static final summary_screen_accessory_connection_text_left_margin:I = 0x7f0a00e0

.field public static final summary_screen_accessory_connection_text_size:I = 0x7f0a00e3

.field public static final summary_screen_accessory_icon_height:I = 0x7f0a00e5

.field public static final summary_screen_accessory_icon_width:I = 0x7f0a00e4

.field public static final summary_view_button_text_size:I = 0x7f0a00af

.field public static final summary_view_content_balance_statistics_text_size:I = 0x7f0a00ba

.field public static final summary_view_content_balance_text_margin_bottom:I = 0x7f0a00b8

.field public static final summary_view_content_balance_text_margin_top:I = 0x7f0a00b7

.field public static final summary_view_content_balance_text_size:I = 0x7f0a00b9

.field public static final summary_view_content_bar_height:I = 0x7f0a00b2

.field public static final summary_view_content_bar_margin:I = 0x7f0a00b3

.field public static final summary_view_content_divider_height:I = 0x7f0a00b5

.field public static final summary_view_content_divider_margin_top:I = 0x7f0a00b4

.field public static final summary_view_content_divider_width:I = 0x7f0a00b6

.field public static final summary_view_content_icon_height:I = 0x7f0a00a1

.field public static final summary_view_content_icon_width:I = 0x7f0a00a2

.field public static final summary_view_content_layout_height:I = 0x7f0a00b0

.field public static final summary_view_content_layout_margin_top:I = 0x7f0a00b1

.field public static final summary_view_content_text_margin_left:I = 0x7f0a00a4

.field public static final summary_view_content_text_size:I = 0x7f0a00a3

.field public static final summary_view_content_title_margin_bottom:I = 0x7f0a00a0

.field public static final summary_view_content_title_margin_top:I = 0x7f0a009f

.field public static final summary_view_content_title_text_height:I = 0x7f0a009e

.field public static final summary_view_content_title_text_size:I = 0x7f0a009d

.field public static final summary_view_content_value_layout_height:I = 0x7f0a009c

.field public static final summary_view_counter_text_size:I = 0x7f0a00cc

.field public static final summary_view_header_balance_state_one_line_text_size:I = 0x7f0a00a7

.field public static final summary_view_header_balance_state_text_height:I = 0x7f0a00a9

.field public static final summary_view_header_balance_state_two_lines_text_size:I = 0x7f0a00a8

.field public static final summary_view_header_height:I = 0x7f0a009b

.field public static final summary_view_header_icon_margin_bottom:I = 0x7f0a009a

.field public static final summary_view_header_icon_margin_top:I = 0x7f0a0099

.field public static final summary_view_header_view_height:I = 0x7f0a00a6

.field public static final summary_view_health_care_counter_view_text_size:I = 0x7f0a00c2

.field public static final summary_view_health_care_counter_view_units_text_size:I = 0x7f0a00c3

.field public static final summary_view_health_care_progress_bar_height:I = 0x7f0a00bc

.field public static final summary_view_health_care_progress_bar_label_side_margin:I = 0x7f0a00c8

.field public static final summary_view_health_care_progress_bar_label_text_size:I = 0x7f0a00c7

.field public static final summary_view_health_care_progress_bar_margin_top:I = 0x7f0a00c1

.field public static final summary_view_health_care_progress_bar_side_margin:I = 0x7f0a00c0

.field public static final summary_view_health_care_progress_bar_slider_height:I = 0x7f0a00be

.field public static final summary_view_health_care_progress_bar_slider_width:I = 0x7f0a00bd

.field public static final summary_view_health_care_progress_bar_text_container_width:I = 0x7f0a00c9

.field public static final summary_view_health_care_progress_bar_title_margin_top:I = 0x7f0a00c5

.field public static final summary_view_health_care_progress_bar_title_text_size:I = 0x7f0a00c6

.field public static final summary_view_health_care_progress_bar_title_width:I = 0x7f0a00c4

.field public static final summary_view_health_care_progress_bar_width:I = 0x7f0a00bf

.field public static final summary_view_health_care_progress_view_container_height:I = 0x7f0a00bb

.field public static final summary_view_single_counter_view_margin_left:I = 0x7f0a00db

.field public static final summary_view_single_degree_line_height:I = 0x7f0a00dd

.field public static final summary_view_single_degree_line_width:I = 0x7f0a00de

.field public static final summary_view_single_indicator_icon_margin_top:I = 0x7f0a00d6

.field public static final summary_view_single_indicator_icon_size:I = 0x7f0a00da

.field public static final summary_view_single_measure_text_label_height:I = 0x7f0a00cf

.field public static final summary_view_single_measure_text_label_margin_left:I = 0x7f0a00ce

.field public static final summary_view_single_measure_text_label_margin_top:I = 0x7f0a00cd

.field public static final summary_view_single_measure_text_label_size:I = 0x7f0a00d0

.field public static final summary_view_single_outside_normal_icon_text_margin:I = 0x7f0a00d2

.field public static final summary_view_single_outside_normal_range_height:I = 0x7f0a00d1

.field public static final summary_view_single_outside_normal_range_text_size:I = 0x7f0a00d4

.field public static final summary_view_single_outside_normal_text_margin:I = 0x7f0a00d3

.field public static final summary_view_single_statistics_from_text_margin:I = 0x7f0a00d5

.field public static final summary_view_single_units_text_size:I = 0x7f0a00dc

.field public static final summary_view_single_value_counter_view_height:I = 0x7f0a00d7

.field public static final summary_view_single_value_counter_view_margin_bottom:I = 0x7f0a00d9

.field public static final summary_view_single_value_counter_view_margin_left:I = 0x7f0a00d8

.field public static final summary_view_slider_width:I = 0x7f0a00cb

.field public static final summary_view_tgh_bottom_view_height:I = 0x7f0a0a0a

.field public static final summary_view_update_button_height:I = 0x7f0a00ae

.field public static final summary_view_update_button_width:I = 0x7f0a00ad

.field public static final target_date_button_height:I = 0x7f0a06a2

.field public static final target_date_button_margin_left:I = 0x7f0a06a3

.field public static final target_date_button_margin_right:I = 0x7f0a06a4

.field public static final target_date_button_text_size:I = 0x7f0a06a5

.field public static final target_date_button_width:I = 0x7f0a06a1

.field public static final target_date_layout_height:I = 0x7f0a06a0

.field public static final textSize_10:I = 0x7f0a0565

.field public static final textSize_15:I = 0x7f0a0564

.field public static final textSize_20:I = 0x7f0a0563

.field public static final textSize_21:I = 0x7f0a0b7b

.field public static final text_size_settings:I = 0x7f0a0522

.field public static final tgh_content_bar_height:I = 0x7f0a0a24

.field public static final tgh_content_icon_height:I = 0x7f0a0a21

.field public static final tgh_content_icon_width:I = 0x7f0a0a27

.field public static final tgh_content_layout_height:I = 0x7f0a0a23

.field public static final tgh_content_text_size:I = 0x7f0a0a22

.field public static final tgh_content_title_text_height:I = 0x7f0a0a20

.field public static final tgh_content_title_text_size:I = 0x7f0a0a1f

.field public static final tgh_content_value_layout_height:I = 0x7f0a0a26

.field public static final tgh_inputmodule_layout_margin_left:I = 0x7f0a0a2b

.field public static final tgh_inputmodule_layout_margin_top:I = 0x7f0a0a29

.field public static final tgh_inputmodule_layout_width:I = 0x7f0a0a2a

.field public static final tgh_right_content_icon_width:I = 0x7f0a0a25

.field public static final tgh_summary_btn_chart_size:I = 0x7f0a0a28

.field public static final tgh_summary_content_divider_height:I = 0x7f0a0a0f

.field public static final tgh_summary_header_view_height:I = 0x7f0a0a1c

.field public static final tgh_summary_logo_height:I = 0x7f0a0a1d

.field public static final tgh_summary_logo_width:I = 0x7f0a0a1e

.field public static final timeline_11dp:I = 0x7f0a056d

.field public static final timeline_1dp:I = 0x7f0a0566

.field public static final timeline_2dp:I = 0x7f0a0567

.field public static final timeline_3dp:I = 0x7f0a0568

.field public static final timeline_4dp:I = 0x7f0a0569

.field public static final timeline_5dp:I = 0x7f0a056a

.field public static final timeline_6dp:I = 0x7f0a056b

.field public static final timeline_8dp:I = 0x7f0a056c

.field public static final timeline_compare_graph_height:I = 0x7f0a0577

.field public static final timeline_compare_stack_graph_height:I = 0x7f0a0578

.field public static final timeline_compare_two_stack_graph_height:I = 0x7f0a0579

.field public static final timeline_compare_two_stack_margin_middle:I = 0x7f0a057a

.field public static final timeline_graph_height:I = 0x7f0a0562

.field public static final timeline_horizontal_arrow_width:I = 0x7f0a057e

.field public static final timeline_horizontal_bar_height:I = 0x7f0a057c

.field public static final timeline_horizontal_bar_margin:I = 0x7f0a057d

.field public static final timeline_horizontal_bar_width:I = 0x7f0a057b

.field public static final timeline_horizontal_my_walking_width:I = 0x7f0a057f

.field public static final timeline_item_side_padding:I = 0x7f0a056e

.field public static final timeline_line_section_height:I = 0x7f0a056f

.field public static final timeline_line_section_line_height:I = 0x7f0a0571

.field public static final timeline_line_section_line_top_margin:I = 0x7f0a0572

.field public static final timeline_line_section_line_width:I = 0x7f0a0570

.field public static final timeline_line_section_text_size:I = 0x7f0a0575

.field public static final timeline_line_section_text_top_margin:I = 0x7f0a0573

.field public static final timeline_line_section_text_width:I = 0x7f0a0574

.field public static final timeline_progress_bar_height:I = 0x7f0a0581

.field public static final timeline_progress_bar_margin:I = 0x7f0a0582

.field public static final timeline_progress_bar_width:I = 0x7f0a0580

.field public static final timeline_text_bpm:I = 0x7f0a0576

.field public static final type_c_imgSize:I = 0x7f0a0560

.field public static final type_g_imgSize:I = 0x7f0a0561

.field public static final upload_list_item_height:I = 0x7f0a0526

.field public static final user_bmi_max_width:I = 0x7f0a0598

.field public static final user_bmi_value_width:I = 0x7f0a0599

.field public static final user_bmi_width:I = 0x7f0a0597

.field public static final uv_input_screen_scroll_content_layout_height:I = 0x7f0a06a8

.field public static final uv_memo_dimensions:I = 0x7f0a06a7

.field public static final vertical_handler_offset:I = 0x7f0a014e

.field public static final vertical_progress_area_margin:I = 0x7f0a0128

.field public static final vertical_progress_area_width:I = 0x7f0a0127

.field public static final vertical_progress_height:I = 0x7f0a0125

.field public static final vertical_progress_labels_area_margin:I = 0x7f0a0123

.field public static final vertical_progress_labels_area_width:I = 0x7f0a0122

.field public static final vertical_progress_mark_height:I = 0x7f0a0126

.field public static final vertical_progress_value_label_height:I = 0x7f0a012a

.field public static final vertical_progress_value_label_text_size:I = 0x7f0a012b

.field public static final vertical_progress_value_label_width:I = 0x7f0a0129

.field public static final vertical_progress_width:I = 0x7f0a0124

.field public static final walking_summary_health_step_box:I = 0x7f0a0ac3

.field public static final walking_summary_health_step_container_height:I = 0x7f0a0ac2

.field public static final water_graph_bottom_chart_padding:I = 0x7f0a000b

.field public static final water_graph_goal_line_text_size:I = 0x7f0a0002

.field public static final water_graph_handler_text_size:I = 0x7f0a0006

.field public static final water_graph_left_chart_padding:I = 0x7f0a0008

.field public static final water_graph_legend_item_height:I = 0x7f0a000f

.field public static final water_graph_legend_item_width:I = 0x7f0a000e

.field public static final water_graph_line_thickness:I = 0x7f0a0003

.field public static final water_graph_popup_stroke_width:I = 0x7f0a000d

.field public static final water_graph_right_chart_padding:I = 0x7f0a0009

.field public static final water_graph_top_chart_padding:I = 0x7f0a000a

.field public static final water_graph_x_axis_stroke_width:I = 0x7f0a000c

.field public static final water_graph_x_chart_text_size:I = 0x7f0a0004

.field public static final water_graph_x_sub_text_style:I = 0x7f0a0007

.field public static final water_graph_y_chart_text_size:I = 0x7f0a0005

.field public static final weight_goal_height:I = 0x7f0a0b7a

.field public static final wfl_graph_view_circle_diagram_size:I = 0x7f0a014c

.field public static final wgt_accessory_summary_view_units_text_size:I = 0x7f0a0b07

.field public static final wgt_accessory_summary_view_value_description_height:I = 0x7f0a0b09

.field public static final wgt_accessory_summary_view_value_description_top_margin:I = 0x7f0a0b08

.field public static final wgt_accessory_summary_view_values_container_height:I = 0x7f0a0b0b

.field public static final wgt_accessory_summary_view_values_height:I = 0x7f0a0b06

.field public static final wgt_accessory_summary_view_values_label_text_size:I = 0x7f0a0b0a

.field public static final wgt_accessory_summary_view_values_margin_right:I = 0x7f0a0b05

.field public static final wgt_accessory_summary_view_values_text_size:I = 0x7f0a0b04

.field public static final wgt_activity_horizontal_margin:I = 0x7f0a0aed

.field public static final wgt_disclaimer_checkbox_padding:I = 0x7f0a0b20

.field public static final wgt_disclaimer_checkbox_text_size:I = 0x7f0a0b1e

.field public static final wgt_disclaimer_checkbox_top_margin:I = 0x7f0a0b21

.field public static final wgt_disclaimer_side_margin:I = 0x7f0a0b1f

.field public static final wgt_disclaimer_text_bottom_margin:I = 0x7f0a0b22

.field public static final wgt_disclaimer_text_top_margin:I = 0x7f0a0b23

.field public static final wgt_goal_view_bottom_margin:I = 0x7f0a0b24

.field public static final wgt_input_screen_content_layout_height:I = 0x7f0a0b27

.field public static final wgt_input_screen_controller_view_margin_top:I = 0x7f0a0b37

.field public static final wgt_input_screen_edit_text_layout_height:I = 0x7f0a0b2e

.field public static final wgt_input_screen_edit_text_margin_top:I = 0x7f0a0b2d

.field public static final wgt_input_screen_edit_text_text_size:I = 0x7f0a0b2f

.field public static final wgt_input_screen_edit_text_width:I = 0x7f0a0b39

.field public static final wgt_input_screen_info_text_layout_height:I = 0x7f0a0b33

.field public static final wgt_input_screen_info_text_layout_width:I = 0x7f0a0b34

.field public static final wgt_input_screen_info_text_line_space:I = 0x7f0a0b35

.field public static final wgt_input_screen_info_text_side_margin:I = 0x7f0a0b3a

.field public static final wgt_input_screen_info_text_text_size:I = 0x7f0a0b36

.field public static final wgt_input_screen_inputmodule_dropdownlist_width:I = 0x7f0a0b38

.field public static final wgt_input_screen_unit_height:I = 0x7f0a0b30

.field public static final wgt_input_screen_unit_margin_left:I = 0x7f0a0b32

.field public static final wgt_input_screen_unit_margin_top:I = 0x7f0a0b31

.field public static final wgt_input_screen_weight_text_height:I = 0x7f0a0b29

.field public static final wgt_input_screen_weight_text_layout_height:I = 0x7f0a0b28

.field public static final wgt_input_screen_weight_text_margin_top:I = 0x7f0a0b2a

.field public static final wgt_input_screen_weight_text_text_margin:I = 0x7f0a0b2c

.field public static final wgt_input_screen_weight_text_text_size:I = 0x7f0a0b2b

.field public static final wgt_next_prev_icon_size:I = 0x7f0a0b70

.field public static final wgt_profile_attribute_item_edit_text_container_height:I = 0x7f0a0af0

.field public static final wgt_set_goal_calorie_input_module_calorie_burn_margin_left:I = 0x7f0a0b43

.field public static final wgt_set_goal_calorie_layout_margin_bottom:I = 0x7f0a0b45

.field public static final wgt_set_goal_calorie_recommend_description_height:I = 0x7f0a0b41

.field public static final wgt_set_goal_calorie_recommend_description_text_size:I = 0x7f0a0b40

.field public static final wgt_set_goal_calorie_recommend_height:I = 0x7f0a0b42

.field public static final wgt_set_goal_calorie_recommend_text_size:I = 0x7f0a0b3f

.field public static final wgt_set_goal_calorie_screen_top_padding:I = 0x7f0a0b44

.field public static final wgt_set_goal_edit_text_left_padding:I = 0x7f0a0b72

.field public static final wgt_set_goal_goal_weight_bottom_margin:I = 0x7f0a0b3d

.field public static final wgt_set_goal_goal_weight_text_size:I = 0x7f0a0b3e

.field public static final wgt_set_goal_goal_weight_top_margin:I = 0x7f0a0b3c

.field public static final wgt_set_goal_input_module_text_height:I = 0x7f0a0b5f

.field public static final wgt_set_goal_input_module_text_margin_top:I = 0x7f0a0b60

.field public static final wgt_set_goal_input_module_text_size:I = 0x7f0a0b61

.field public static final wgt_set_goal_input_module_text_width:I = 0x7f0a0b5e

.field public static final wgt_set_goal_input_module_title_height:I = 0x7f0a0b5b

.field public static final wgt_set_goal_input_module_title_margin_top:I = 0x7f0a0b5c

.field public static final wgt_set_goal_input_module_title_text_size:I = 0x7f0a0b5d

.field public static final wgt_set_goal_input_module_unit_height:I = 0x7f0a0b62

.field public static final wgt_set_goal_input_module_unit_margin_top:I = 0x7f0a0b63

.field public static final wgt_set_goal_input_module_unit_text_size:I = 0x7f0a0b64

.field public static final wgt_set_goal_layout_bottom_divider:I = 0x7f0a0b71

.field public static final wgt_set_goal_progressbar_height:I = 0x7f0a0b6b

.field public static final wgt_set_goal_progressbar_margin_bottom:I = 0x7f0a0b68

.field public static final wgt_set_goal_progressbar_margin_left:I = 0x7f0a0b69

.field public static final wgt_set_goal_progressbar_margin_top:I = 0x7f0a0b67

.field public static final wgt_set_goal_progressbar_text_size:I = 0x7f0a0b6a

.field public static final wgt_set_goal_set_challenge_button_height:I = 0x7f0a0b6f

.field public static final wgt_set_goal_set_challenge_button_width:I = 0x7f0a0b6e

.field public static final wgt_set_goal_set_challenge_top_height:I = 0x7f0a0b65

.field public static final wgt_set_goal_target_date_btn_width:I = 0x7f0a0b77

.field public static final wgt_set_goal_target_date_height:I = 0x7f0a0b76

.field public static final wgt_set_goal_target_date_left_margin:I = 0x7f0a0b74

.field public static final wgt_set_goal_target_date_right_margin:I = 0x7f0a0b75

.field public static final wgt_set_goal_target_date_text_size:I = 0x7f0a0b73

.field public static final wgt_set_goal_top_margin_10dp:I = 0x7f0a0b79

.field public static final wgt_set_goal_top_margin_12dp:I = 0x7f0a0b78

.field public static final wgt_set_goal_try_challenge_new_bottom_margin:I = 0x7f0a0b6c

.field public static final wgt_set_goal_try_challenge_new_top_margin:I = 0x7f0a0b6d

.field public static final wgt_set_goal_view_add_goal_items_left_margin:I = 0x7f0a0b47

.field public static final wgt_set_goal_view_add_goal_items_right_margin:I = 0x7f0a0b4a

.field public static final wgt_set_goal_view_add_goal_items_text_size:I = 0x7f0a0b48

.field public static final wgt_set_goal_view_add_goal_title_header_text_bottom_margin:I = 0x7f0a0b49

.field public static final wgt_set_goal_view_add_goal_top_label_divider_height:I = 0x7f0a0b4b

.field public static final wgt_set_goal_view_add_goal_top_label_layout_height:I = 0x7f0a0b46

.field public static final wgt_set_goal_view_bottom_layout_height:I = 0x7f0a0b58

.field public static final wgt_set_goal_view_edit_goal_layout_height:I = 0x7f0a0b4e

.field public static final wgt_set_goal_view_edit_goal_layout_top_margin:I = 0x7f0a0b4f

.field public static final wgt_set_goal_view_goal_range_top_text_size:I = 0x7f0a0b57

.field public static final wgt_set_goal_view_goal_range_top_text_top_margin:I = 0x7f0a0b56

.field public static final wgt_set_goal_view_goal_weight_edit_text_height:I = 0x7f0a0b51

.field public static final wgt_set_goal_view_goal_weight_edit_text_text_size:I = 0x7f0a0b55

.field public static final wgt_set_goal_view_goal_weight_edit_text_width:I = 0x7f0a0b50

.field public static final wgt_set_goal_view_goal_weight_text_size:I = 0x7f0a0b4c

.field public static final wgt_set_goal_view_goal_weight_text_top_margin:I = 0x7f0a0b4d

.field public static final wgt_set_goal_view_goal_weight_unit_edit_text_bottom_margin:I = 0x7f0a0b52

.field public static final wgt_set_goal_view_goal_weight_unit_edit_text_left_margin:I = 0x7f0a0b53

.field public static final wgt_set_goal_view_goal_weight_unit_edit_text_size:I = 0x7f0a0b54

.field public static final wgt_set_goal_view_next_button_drawable_padding:I = 0x7f0a0b59

.field public static final wgt_set_goal_view_next_button_text_size:I = 0x7f0a0b5a

.field public static final wgt_set_goal_view_no_data_text_size:I = 0x7f0a0b3b

.field public static final wgt_set_goal_weight_progress_margin_top:I = 0x7f0a0b66

.field public static final wgt_settings_item_small:I = 0x7f0a0aee

.field public static final wgt_settings_margin:I = 0x7f0a0aef

.field public static final wgt_summary_screen_button_margins:I = 0x7f0a0b26

.field public static final wgt_summary_screen_warning_popup_text_size:I = 0x7f0a0b25

.field public static final wgt_summary_view_bottom_view_height:I = 0x7f0a0af4

.field public static final wgt_summary_view_center_view_context_layouts_height:I = 0x7f0a0af7

.field public static final wgt_summary_view_center_view_from_divider_margin:I = 0x7f0a0af8

.field public static final wgt_summary_view_center_view_labels_text_size:I = 0x7f0a0afa

.field public static final wgt_summary_view_center_view_top_margin:I = 0x7f0a0af6

.field public static final wgt_summary_view_center_view_values_text_size:I = 0x7f0a0af9

.field public static final wgt_summary_view_indicator_view_bottom_margin:I = 0x7f0a0b13

.field public static final wgt_summary_view_indicator_view_deal_text_size:I = 0x7f0a0b14

.field public static final wgt_summary_view_indicator_view_dial_height:I = 0x7f0a0b18

.field public static final wgt_summary_view_indicator_view_dial_text_y_position:I = 0x7f0a0b1c

.field public static final wgt_summary_view_indicator_view_dial_width:I = 0x7f0a0b15

.field public static final wgt_summary_view_indicator_view_dial_y_position:I = 0x7f0a0b1a

.field public static final wgt_summary_view_indicator_view_goal_dial_width:I = 0x7f0a0b16

.field public static final wgt_summary_view_indicator_view_height:I = 0x7f0a0b10

.field public static final wgt_summary_view_indicator_view_pivot_y:I = 0x7f0a0b1d

.field public static final wgt_summary_view_indicator_view_range_dial_width:I = 0x7f0a0b17

.field public static final wgt_summary_view_indicator_view_range_height:I = 0x7f0a0b19

.field public static final wgt_summary_view_indicator_view_range_y_position:I = 0x7f0a0b1b

.field public static final wgt_summary_view_indicator_view_windows_bottom_margin:I = 0x7f0a0b12

.field public static final wgt_summary_view_indicator_view_windows_top_padding:I = 0x7f0a0b11

.field public static final wgt_summary_view_middle_view_height:I = 0x7f0a0af3

.field public static final wgt_summary_view_top_view_height:I = 0x7f0a0af1

.field public static final wgt_summary_view_top_view_margin_bottom:I = 0x7f0a0af2

.field public static final wgt_summary_view_top_view_values_view_height:I = 0x7f0a0b03

.field public static final wgt_summary_view_units_text_size:I = 0x7f0a0b0f

.field public static final wgt_summary_view_update_weight_button_bottom_margin:I = 0x7f0a0af5

.field public static final wgt_summary_view_values_label_text_size:I = 0x7f0a0b0e

.field public static final wgt_summary_view_values_margin_right:I = 0x7f0a0b0d

.field public static final wgt_summary_view_values_text_size:I = 0x7f0a0b0c

.field public static final wgt_summary_view_weight_goal_label_bottom_margin:I = 0x7f0a0b02

.field public static final wgt_summary_view_weight_goal_label_text_size:I = 0x7f0a0b00

.field public static final wgt_summary_view_weight_icon_label_bottom_margin:I = 0x7f0a0afc

.field public static final wgt_summary_view_weight_icon_label_left_margit:I = 0x7f0a0afe

.field public static final wgt_summary_view_weight_icon_label_text_size:I = 0x7f0a0afd

.field public static final wgt_summary_view_weight_icon_top_margin:I = 0x7f0a0afb

.field public static final wgt_summary_view_weight_update_time_label_text_size:I = 0x7f0a0b01

.field public static final wgt_summary_view_weight_value_text_size:I = 0x7f0a0aff

.field public static final widget_height:I = 0x7f0a05d1

.field public static final widget_width:I = 0x7f0a05d2

.field public static final width_graph:I = 0x7f0a0a0d

.field public static final workout__1dp:I = 0x7f0a0a2e

.field public static final workout__2dp:I = 0x7f0a0918

.field public static final workout_actionbar_size:I = 0x7f0a09c2

.field public static final workout_activity_horizontal_margin:I = 0x7f0a0a2c

.field public static final workout_activity_vertical_margin:I = 0x7f0a0a2d

.field public static final workout_age_place_ranking_height:I = 0x7f0a0ad1

.field public static final workout_alphabet_letter_height:I = 0x7f0a05fa

.field public static final workout_alphabet_letter_textsize:I = 0x7f0a05fb

.field public static final workout_alphabet_scroll_handle:I = 0x7f0a05f8

.field public static final workout_alphabet_scroll_view_root_layout_width:I = 0x7f0a0650

.field public static final workout_alphabet_scroll_width:I = 0x7f0a05f7

.field public static final workout_alphabet_star_height:I = 0x7f0a05f9

.field public static final workout_appeasywidget_value:I = 0x7f0a0a32

.field public static final workout_appwidget_clearcover_value:I = 0x7f0a0a33

.field public static final workout_appwidget_goal:I = 0x7f0a0a30

.field public static final workout_appwidget_last_synced_text_size:I = 0x7f0a0a38

.field public static final workout_appwidget_percent_unit_text_size:I = 0x7f0a0a3a

.field public static final workout_appwidget_percent_value_text_size:I = 0x7f0a0a39

.field public static final workout_appwidget_title:I = 0x7f0a0a2f

.field public static final workout_appwidget_title_not_inited:I = 0x7f0a0a37

.field public static final workout_appwidget_value:I = 0x7f0a0a31

.field public static final workout_constrollerview_gradationbar_text_size:I = 0x7f0a0a40

.field public static final workout_controllerview_big_gradationbar_height:I = 0x7f0a0a3f

.field public static final workout_controllerview_gradationbar_horizontal_between_distance:I = 0x7f0a0a42

.field public static final workout_controllerview_gradationbar_vertical_between_distance:I = 0x7f0a0a41

.field public static final workout_controllerview_margin:I = 0x7f0a0a3b

.field public static final workout_controllerview_small_gradationbar_height:I = 0x7f0a0a3e

.field public static final workout_controllerview_vertical_height:I = 0x7f0a0a3d

.field public static final workout_controllerview_vertical_width:I = 0x7f0a0a3c

.field public static final workout_details_layout_input_type_pane_layout_height:I = 0x7f0a0940

.field public static final workout_details_layout_input_type_pane_layout_marginTop:I = 0x7f0a0941

.field public static final workout_details_layout_input_type_textSize:I = 0x7f0a0942

.field public static final workout_edit_item_calories_layout_height:I = 0x7f0a0637

.field public static final workout_edit_item_calories_layout_margin_left:I = 0x7f0a0638

.field public static final workout_edit_item_calories_layout_margin_right:I = 0x7f0a0639

.field public static final workout_edit_item_calories_text_view_textsize:I = 0x7f0a063a

.field public static final workout_edit_item_divider_width:I = 0x7f0a0631

.field public static final workout_edit_item_kcal_text_height:I = 0x7f0a063c

.field public static final workout_edit_item_kcal_text_layout_height:I = 0x7f0a063b

.field public static final workout_edit_item_kcal_text_text_size:I = 0x7f0a063d

.field public static final workout_edit_item_name_edit_text_height:I = 0x7f0a0643

.field public static final workout_edit_item_name_edit_text_margin_top:I = 0x7f0a0636

.field public static final workout_edit_item_name_edit_text_padding_left:I = 0x7f0a0644

.field public static final workout_edit_item_name_edit_text_padding_right:I = 0x7f0a0645

.field public static final workout_edit_item_name_layout_height:I = 0x7f0a0632

.field public static final workout_edit_item_name_layout_margin_left:I = 0x7f0a0633

.field public static final workout_edit_item_name_layout_margin_right:I = 0x7f0a0634

.field public static final workout_edit_item_name_text_view_textsize:I = 0x7f0a0635

.field public static final workout_edit_item_save_in_db_drawable_padding:I = 0x7f0a0642

.field public static final workout_edit_item_save_in_db_height:I = 0x7f0a063e

.field public static final workout_edit_item_save_in_db_margin_left:I = 0x7f0a063f

.field public static final workout_edit_item_save_in_db_margin_right:I = 0x7f0a0640

.field public static final workout_edit_item_save_in_db_textsize:I = 0x7f0a0641

.field public static final workout_effective_text_margin_with_box:I = 0x7f0a0ac4

.field public static final workout_everyone_ranking_comparative_dialog_width:I = 0x7f0a0aac

.field public static final workout_everyone_ranking_dialog_content_padding_bottom:I = 0x7f0a0aaf

.field public static final workout_everyone_ranking_dialog_content_padding_top:I = 0x7f0a0aae

.field public static final workout_everyone_ranking_dialog_content_side_margin:I = 0x7f0a0aad

.field public static final workout_everyone_ranking_dialog_everyone_width:I = 0x7f0a051e

.field public static final workout_everyone_ranking_dialog_header_height:I = 0x7f0a0aa8

.field public static final workout_everyone_ranking_dialog_header_padding_side:I = 0x7f0a0aa9

.field public static final workout_everyone_ranking_dialog_header_padding_top:I = 0x7f0a0aaa

.field public static final workout_everyone_ranking_dialog_header_text_size:I = 0x7f0a0aab

.field public static final workout_everyone_ranking_dialog_item_delimiter_width:I = 0x7f0a0ac1

.field public static final workout_everyone_ranking_dialog_item_distance_value_text_size:I = 0x7f0a0abc

.field public static final workout_everyone_ranking_dialog_item_dot_icon_size:I = 0x7f0a0abd

.field public static final workout_everyone_ranking_dialog_item_icon_size:I = 0x7f0a0ab9

.field public static final workout_everyone_ranking_dialog_item_text_size:I = 0x7f0a0abe

.field public static final workout_everyone_ranking_dialog_item_trophy_big_size:I = 0x7f0a0abb

.field public static final workout_everyone_ranking_dialog_item_trophy_size:I = 0x7f0a0aba

.field public static final workout_everyone_ranking_dialog_item_width:I = 0x7f0a0ab4

.field public static final workout_everyone_ranking_dialog_my_info_content_padding:I = 0x7f0a0ac0

.field public static final workout_everyone_ranking_dialog_position_text_size:I = 0x7f0a0ab3

.field public static final workout_everyone_ranking_dialog_suggestion_text_height:I = 0x7f0a0ab0

.field public static final workout_everyone_ranking_dialog_suggestion_text_size:I = 0x7f0a0ab2

.field public static final workout_everyone_ranking_dialog_suggestion_text_top_margin:I = 0x7f0a0ab1

.field public static final workout_everyone_ranking_dialog_user_info_height:I = 0x7f0a0ab5

.field public static final workout_everyone_ranking_dialog_user_info_item_text_margin_left:I = 0x7f0a0abf

.field public static final workout_everyone_ranking_dialog_user_info_width:I = 0x7f0a051d

.field public static final workout_everyone_ranking_galaxy_view_height:I = 0x7f0a0a92

.field public static final workout_everyone_ranking_galaxy_view_margin_top:I = 0x7f0a0a93

.field public static final workout_everyone_ranking_galaxy_view_planet_height:I = 0x7f0a0aa2

.field public static final workout_everyone_ranking_galaxy_view_planet_width:I = 0x7f0a0aa1

.field public static final workout_everyone_ranking_galaxy_view_width:I = 0x7f0a0a91

.field public static final workout_everyone_ranking_header_distance_value_average_text_size:I = 0x7f0a0a8e

.field public static final workout_everyone_ranking_header_distance_value_minimal_text_size:I = 0x7f0a0a8f

.field public static final workout_everyone_ranking_header_distance_value_text_size:I = 0x7f0a0a8d

.field public static final workout_everyone_ranking_header_height:I = 0x7f0a0a8c

.field public static final workout_everyone_ranking_header_text_size:I = 0x7f0a0a90

.field public static final workout_everyone_ranking_item_height:I = 0x7f0a0ab6

.field public static final workout_everyone_ranking_item_icon_margin_bot:I = 0x7f0a0ab8

.field public static final workout_everyone_ranking_item_icon_margin_top:I = 0x7f0a0ab7

.field public static final workout_everyone_ranking_list_item_additional_element_height:I = 0x7f0a0a9a

.field public static final workout_everyone_ranking_list_item_bot_text_size:I = 0x7f0a0aa0

.field public static final workout_everyone_ranking_list_item_height:I = 0x7f0a0a99

.field public static final workout_everyone_ranking_list_item_icon_border_size:I = 0x7f0a0a9e

.field public static final workout_everyone_ranking_list_item_icon_margin_left:I = 0x7f0a0a9c

.field public static final workout_everyone_ranking_list_item_icon_size:I = 0x7f0a0a9b

.field public static final workout_everyone_ranking_list_item_position_margin_right:I = 0x7f0a0a89

.field public static final workout_everyone_ranking_list_item_position_padding:I = 0x7f0a0a8a

.field public static final workout_everyone_ranking_list_item_text_margin_left:I = 0x7f0a0a9d

.field public static final workout_everyone_ranking_list_item_top_text_size:I = 0x7f0a0a9f

.field public static final workout_everyone_ranking_list_item_trophy_margin_right:I = 0x7f0a0a88

.field public static final workout_everyone_ranking_loading_view_size:I = 0x7f0a0aa6

.field public static final workout_everyone_ranking_loading_view_text_size:I = 0x7f0a0aa7

.field public static final workout_everyone_ranking_lsit_item_position_text_size:I = 0x7f0a0a8b

.field public static final workout_everyone_ranking_planet_galaxy_view_planets_layout_margin_top:I = 0x7f0a0aa5

.field public static final workout_everyone_ranking_planet_view_distance_text_size:I = 0x7f0a0aa4

.field public static final workout_everyone_ranking_planet_view_name_text_size:I = 0x7f0a0aa3

.field public static final workout_everyone_ranking_top_walkers_header_line_height:I = 0x7f0a0a95

.field public static final workout_everyone_ranking_top_walkers_header_margin_left:I = 0x7f0a0a96

.field public static final workout_everyone_ranking_top_walkers_header_margin_top:I = 0x7f0a0a94

.field public static final workout_everyone_ranking_top_walkers_header_server_time_margin_left:I = 0x7f0a0a98

.field public static final workout_everyone_ranking_top_walkers_header_text_size:I = 0x7f0a0a97

.field public static final workout_everyoune_view_height:I = 0x7f0a0ae5

.field public static final workout_everyoune_view_width:I = 0x7f0a0ae4

.field public static final workout_exercise_bottom_button_space_height:I = 0x7f0a0621

.field public static final workout_exercise_bottom_button_space_width:I = 0x7f0a0620

.field public static final workout_exercise_details_activity_input_type_text_textsize:I = 0x7f0a066f

.field public static final workout_exercise_details_item_divider:I = 0x7f0a0675

.field public static final workout_exercise_details_item_icon_linear_height:I = 0x7f0a0672

.field public static final workout_exercise_details_item_icon_linear_margin_left:I = 0x7f0a0673

.field public static final workout_exercise_details_item_icon_linear_margin_right:I = 0x7f0a0674

.field public static final workout_exercise_details_item_icon_linear_width:I = 0x7f0a0671

.field public static final workout_exercise_details_item_layout_height:I = 0x7f0a0670

.field public static final workout_exercise_imagepager_imagebutton_height:I = 0x7f0a0959

.field public static final workout_exercise_imagepager_imagebutton_marginRight:I = 0x7f0a095a

.field public static final workout_exercise_imagepager_imagebutton_width:I = 0x7f0a0958

.field public static final workout_exercise_imagepager_main_layout_height:I = 0x7f0a0956

.field public static final workout_exercise_imagepager_main_layout_width:I = 0x7f0a0955

.field public static final workout_exercise_imagepager_pageselectorimpl_marginTop:I = 0x7f0a0957

.field public static final workout_exercise_input_activity_divider:I = 0x7f0a0651

.field public static final workout_exercise_input_activity_exercise_down_button_height:I = 0x7f0a0669

.field public static final workout_exercise_input_activity_exercise_down_button_text_height:I = 0x7f0a066b

.field public static final workout_exercise_input_activity_exercise_down_button_text_margin_left:I = 0x7f0a066d

.field public static final workout_exercise_input_activity_exercise_down_button_text_margin_top:I = 0x7f0a066c

.field public static final workout_exercise_input_activity_exercise_down_button_text_textsize:I = 0x7f0a066e

.field public static final workout_exercise_input_activity_exercise_down_button_text_width:I = 0x7f0a066a

.field public static final workout_exercise_input_activity_exercise_input_button_height:I = 0x7f0a0652

.field public static final workout_exercise_input_activity_exercise_input_button_margin_left:I = 0x7f0a0653

.field public static final workout_exercise_input_activity_exercise_minutes_edittext_margin_bottom:I = 0x7f0a0665

.field public static final workout_exercise_input_activity_exercise_minutes_edittext_margin_top:I = 0x7f0a0666

.field public static final workout_exercise_input_activity_exercise_minutes_edittext_padding:I = 0x7f0a0667

.field public static final workout_exercise_input_activity_exercise_minutes_edittext_textsize:I = 0x7f0a0668

.field public static final workout_exercise_input_activity_exercise_select_button_height:I = 0x7f0a0657

.field public static final workout_exercise_input_activity_exercise_select_button_margin_right:I = 0x7f0a0658

.field public static final workout_exercise_input_activity_exercise_select_button_padding_right:I = 0x7f0a0659

.field public static final workout_exercise_input_activity_exercise_select_button_textsize:I = 0x7f0a065a

.field public static final workout_exercise_input_activity_exercise_select_button_width:I = 0x7f0a0656

.field public static final workout_exercise_input_activity_exercise_up_button_height:I = 0x7f0a0664

.field public static final workout_exercise_input_activity_exercise_up_button_layout_height:I = 0x7f0a0663

.field public static final workout_exercise_input_activity_exercise_up_button_layout_width:I = 0x7f0a0662

.field public static final workout_exercise_input_activity_kcal_text_margin_top:I = 0x7f0a0660

.field public static final workout_exercise_input_activity_kcal_text_textsize:I = 0x7f0a0661

.field public static final workout_exercise_input_activity_kcal_value_textsize:I = 0x7f0a065f

.field public static final workout_exercise_input_activity_kcal_value_width:I = 0x7f0a065e

.field public static final workout_exercise_input_activity_layout_height:I = 0x7f0a065b

.field public static final workout_exercise_input_activity_layout_margin_left:I = 0x7f0a065c

.field public static final workout_exercise_input_activity_layout_margin_right:I = 0x7f0a065d

.field public static final workout_exercise_input_activity_text_calories_value_textsize:I = 0x7f0a0655

.field public static final workout_exercise_input_activity_text_name_exercise_value_textsize:I = 0x7f0a0654

.field public static final workout_exercise_log_type_long_width:I = 0x7f0a0646

.field public static final workout_exercise_log_type_short_width:I = 0x7f0a0647

.field public static final workout_exercise_mate_bottom_button_height:I = 0x7f0a0614

.field public static final workout_exercise_mate_bottom_layout_height:I = 0x7f0a0626

.field public static final workout_exercise_mate_exercie_pick_search_dim_image_marginleft:I = 0x7f0a0697

.field public static final workout_exercise_mate_exercie_pick_search_image:I = 0x7f0a0695

.field public static final workout_exercise_mate_exercie_pick_search_image_marginleft:I = 0x7f0a0696

.field public static final workout_exercise_mate_exercise_pick_no_result_btn_height:I = 0x7f0a0698

.field public static final workout_exercise_mate_exercise_pick_no_result_btn_margin_bottom:I = 0x7f0a0699

.field public static final workout_exercise_mate_first_bottom_button_font_size:I = 0x7f0a061d

.field public static final workout_exercise_mate_first_bottom_button_icon:I = 0x7f0a0618

.field public static final workout_exercise_mate_first_bottom_button_left_padding:I = 0x7f0a0615

.field public static final workout_exercise_mate_first_bottom_button_right_margin:I = 0x7f0a0619

.field public static final workout_exercise_mate_first_bottom_button_right_padding:I = 0x7f0a0616

.field public static final workout_exercise_mate_first_bottom_button_width:I = 0x7f0a0617

.field public static final workout_exercise_mate_grid_view_l:I = 0x7f0a0603

.field public static final workout_exercise_mate_grid_view_received_icon:I = 0x7f0a0605

.field public static final workout_exercise_mate_grid_view_received_icon_margin_right:I = 0x7f0a0606

.field public static final workout_exercise_mate_grid_view_s:I = 0x7f0a0604

.field public static final workout_exercise_mate_grid_view_text_info_margin_bottom:I = 0x7f0a0607

.field public static final workout_exercise_mate_grid_view_xl:I = 0x7f0a0602

.field public static final workout_exercise_mate_initial_bottom_button_layout_82dp:I = 0x7f0a0694

.field public static final workout_exercise_mate_initial_bottom_layout_197dp:I = 0x7f0a0689

.field public static final workout_exercise_mate_initial_circle_progress_image_margin_bottom_36dp:I = 0x7f0a0693

.field public static final workout_exercise_mate_initial_circle_progress_image_margin_top_43dp:I = 0x7f0a0692

.field public static final workout_exercise_mate_initial_kacl_goal_text_size_20dp:I = 0x7f0a068c

.field public static final workout_exercise_mate_initial_kcal_text_size_93dp:I = 0x7f0a068b

.field public static final workout_exercise_mate_initial_layout_height_347dp:I = 0x7f0a0691

.field public static final workout_exercise_mate_initial_progress_circle_size_116dp:I = 0x7f0a068a

.field public static final workout_exercise_mate_initial_time_record_margin_top_34dp:I = 0x7f0a068d

.field public static final workout_exercise_mate_initial_time_record_unit_text_size_17dp:I = 0x7f0a068f

.field public static final workout_exercise_mate_initial_time_record_value_text_size_33dp:I = 0x7f0a068e

.field public static final workout_exercise_mate_initial_top_padding_bottom_35dp:I = 0x7f0a0688

.field public static final workout_exercise_mate_initial_total_active_time_text_size_17dp:I = 0x7f0a0690

.field public static final workout_exercise_mate_middle_item_marginLeft:I = 0x7f0a0625

.field public static final workout_exercise_mate_middle_item_marginTop:I = 0x7f0a0624

.field public static final workout_exercise_mate_second_bottom_button_icon_heidht:I = 0x7f0a061c

.field public static final workout_exercise_mate_second_bottom_button_icon_width:I = 0x7f0a061b

.field public static final workout_exercise_mate_second_bottom_button_width:I = 0x7f0a061a

.field public static final workout_exercise_mate_summary_middle_itme_icon_height:I = 0x7f0a0629

.field public static final workout_exercise_mate_summary_middle_itme_kcal_height:I = 0x7f0a062c

.field public static final workout_exercise_mate_summary_middle_itme_kcal_textSize:I = 0x7f0a062d

.field public static final workout_exercise_mate_summary_middle_itme_main_layout_height:I = 0x7f0a0628

.field public static final workout_exercise_mate_summary_middle_itme_main_layout_width:I = 0x7f0a0627

.field public static final workout_exercise_mate_summary_middle_itme_title_height:I = 0x7f0a062a

.field public static final workout_exercise_mate_summary_middle_itme_title_textSize:I = 0x7f0a062b

.field public static final workout_exercise_mate_time_container_time_record_font_size:I = 0x7f0a062e

.field public static final workout_exercise_mate_time_container_total_activity_time_font_size:I = 0x7f0a062f

.field public static final workout_exercise_mate_top_layout_height:I = 0x7f0a09c7

.field public static final workout_exercise_mate_top_left_img_height:I = 0x7f0a09c9

.field public static final workout_exercise_mate_top_left_img_width:I = 0x7f0a09c8

.field public static final workout_exercise_mate_top_right_goalkcal_textSize:I = 0x7f0a0623

.field public static final workout_exercise_mate_top_right_kcal_textSize:I = 0x7f0a09ca

.field public static final workout_exercise_mate_top_right_sublayout_height:I = 0x7f0a0622

.field public static final workout_exercise_memo_input_view_layout_margin_bottom:I = 0x7f0a0943

.field public static final workout_exercise_memo_input_view_layout_margin_left:I = 0x7f0a0944

.field public static final workout_exercise_memo_input_view_layout_margin_right:I = 0x7f0a0945

.field public static final workout_exercise_memo_input_view_layout_margin_top:I = 0x7f0a0946

.field public static final workout_exercise_memo_input_view_padding_bottom:I = 0x7f0a0947

.field public static final workout_exercise_memo_input_view_padding_left:I = 0x7f0a0948

.field public static final workout_exercise_memo_input_view_padding_right:I = 0x7f0a0949

.field public static final workout_exercise_memo_input_view_padding_top:I = 0x7f0a094a

.field public static final workout_exercise_memo_memo_input_view_textSize:I = 0x7f0a094b

.field public static final workout_exercise_memo_memo_text_view_textSize:I = 0x7f0a0950

.field public static final workout_exercise_memo_text_view_layout_margin_bottom:I = 0x7f0a094c

.field public static final workout_exercise_memo_text_view_layout_margin_left:I = 0x7f0a094d

.field public static final workout_exercise_memo_text_view_layout_margin_right:I = 0x7f0a094e

.field public static final workout_exercise_memo_text_view_layout_margin_top:I = 0x7f0a094f

.field public static final workout_exercise_photo_imagebtn_marginRight:I = 0x7f0a0954

.field public static final workout_exercise_photo_text_view_height:I = 0x7f0a0951

.field public static final workout_exercise_photo_text_view_marginRight:I = 0x7f0a0952

.field public static final workout_exercise_photo_text_view_paddingLeft:I = 0x7f0a0953

.field public static final workout_exercise_photo_text_view_textSize:I = 0x7f0a0686

.field public static final workout_exercise_pick_tabs_divider:I = 0x7f0a064f

.field public static final workout_exercise_pick_tabs_layout_height:I = 0x7f0a064e

.field public static final workout_exercise_pro_time_goal_bottom_buttons_height:I = 0x7f0a08ec

.field public static final workout_exercise_pro_time_goal_hour_edittext_top_padding:I = 0x7f0a08f2

.field public static final workout_exercise_pro_time_goal_hour_up_button_width:I = 0x7f0a08f1

.field public static final workout_exercise_pro_time_goal_min_up_button_width:I = 0x7f0a08f3

.field public static final workout_exercise_pro_time_goal_minutes_edittext_bottom_padding:I = 0x7f0a08f4

.field public static final workout_exercise_pro_time_goal_minutes_edittext_top_padding:I = 0x7f0a08f5

.field public static final workout_exercise_pro_time_goal_recommend_text_layout_height:I = 0x7f0a08f6

.field public static final workout_exercise_pro_time_goal_recommend_text_layout_top_margin:I = 0x7f0a08f7

.field public static final workout_exercise_pro_time_goal_recommend_text_size:I = 0x7f0a08f8

.field public static final workout_exercise_pro_time_goal_skip_time_goal_checkbox_right_margin:I = 0x7f0a08f0

.field public static final workout_exercise_pro_time_goal_skip_time_goal_height:I = 0x7f0a08ed

.field public static final workout_exercise_pro_time_goal_skip_time_goal_left_margin:I = 0x7f0a08ee

.field public static final workout_exercise_pro_time_goal_training_effect_skip_time_goal_text_size:I = 0x7f0a08ef

.field public static final workout_exercise_progressbar_polygon_height:I = 0x7f0a0685

.field public static final workout_exercise_progressbar_polygon_width:I = 0x7f0a0684

.field public static final workout_exercise_second_bottom_button_button_text_margin_left:I = 0x7f0a061e

.field public static final workout_exercise_second_bottom_button_button_textsize:I = 0x7f0a061f

.field public static final workout_exercise_view_height:I = 0x7f0a0687

.field public static final workout_exercisepro_map_count_redius_size:I = 0x7f0a097c

.field public static final workout_exercisepro_map_count_text_size:I = 0x7f0a097b

.field public static final workout_exercisepro_map_photo_mark_height:I = 0x7f0a0974

.field public static final workout_exercisepro_map_photo_mark_src_height:I = 0x7f0a0976

.field public static final workout_exercisepro_map_photo_mark_src_offset_x:I = 0x7f0a0977

.field public static final workout_exercisepro_map_photo_mark_src_offset_y:I = 0x7f0a0978

.field public static final workout_exercisepro_map_photo_mark_src_width:I = 0x7f0a0975

.field public static final workout_exercisepro_map_photo_mark_text_padding:I = 0x7f0a097a

.field public static final workout_exercisepro_map_photo_mark_text_size:I = 0x7f0a0979

.field public static final workout_exercisepro_map_photo_mark_width:I = 0x7f0a0973

.field public static final workout_exericsemate_log_group_main_layout_button_height:I = 0x7f0a0981

.field public static final workout_exericsemate_log_group_main_layout_button_margin_right:I = 0x7f0a0982

.field public static final workout_exericsemate_log_group_main_layout_button_width:I = 0x7f0a0980

.field public static final workout_exericsemate_log_group_main_layout_height:I = 0x7f0a097e

.field public static final workout_exericsemate_log_group_main_layout_textsize:I = 0x7f0a097f

.field public static final workout_exericsemate_log_group_txtGroupLeft_margin_left:I = 0x7f0a097d

.field public static final workout_exericsemate_log_item_icon_image_height:I = 0x7f0a098b

.field public static final workout_exericsemate_log_item_icon_image_margin_left:I = 0x7f0a098c

.field public static final workout_exericsemate_log_item_icon_image_width:I = 0x7f0a098a

.field public static final workout_exericsemate_log_item_info_image_height:I = 0x7f0a0989

.field public static final workout_exericsemate_log_item_info_image_width:I = 0x7f0a0988

.field public static final workout_exericsemate_log_item_layout_height:I = 0x7f0a0987

.field public static final workout_exericsemate_log_subgroup_goal_image_height:I = 0x7f0a0986

.field public static final workout_exericsemate_log_subgroup_goal_image_width:I = 0x7f0a0985

.field public static final workout_exericsemate_log_subgroup_goal_margin_left:I = 0x7f0a09c3

.field public static final workout_exericsemate_log_subgroup_layout_margin_left:I = 0x7f0a0983

.field public static final workout_exericsemate_log_subgroup_layout_margin_right:I = 0x7f0a0984

.field public static final workout_exericsemate_log_subgroup_main_layout_margin_right:I = 0x7f0a098d

.field public static final workout_exericsemate_log_subgroup_sub_layout_margin_left:I = 0x7f0a098e

.field public static final workout_exericsepro_log_group_main_layout_button_height:I = 0x7f0a0971

.field public static final workout_exericsepro_log_group_main_layout_button_margin_right:I = 0x7f0a0972

.field public static final workout_exericsepro_log_group_main_layout_button_width:I = 0x7f0a0970

.field public static final workout_exericsepro_log_group_main_layout_height:I = 0x7f0a096e

.field public static final workout_exericsepro_log_group_main_layout_textsize:I = 0x7f0a096f

.field public static final workout_exericsepro_log_group_txtGroupLeft_margin_left:I = 0x7f0a096d

.field public static final workout_exericsepro_log_item_icon_image_height:I = 0x7f0a0967

.field public static final workout_exericsepro_log_item_icon_image_margin_left:I = 0x7f0a0968

.field public static final workout_exericsepro_log_item_icon_image_width:I = 0x7f0a0966

.field public static final workout_exericsepro_log_item_info_image_height:I = 0x7f0a0965

.field public static final workout_exericsepro_log_item_info_image_width:I = 0x7f0a0964

.field public static final workout_exericsepro_log_item_layout_height:I = 0x7f0a0963

.field public static final workout_exericsepro_log_subgroup_goal_image_height:I = 0x7f0a0961

.field public static final workout_exericsepro_log_subgroup_goal_image_width:I = 0x7f0a0960

.field public static final workout_exericsepro_log_subgroup_goal_margin_left:I = 0x7f0a0962

.field public static final workout_exericsepro_log_subgroup_layout_height:I = 0x7f0a095c

.field public static final workout_exericsepro_log_subgroup_layout_margin_left:I = 0x7f0a095d

.field public static final workout_exericsepro_log_subgroup_layout_margin_right:I = 0x7f0a095e

.field public static final workout_exericsepro_log_subgroup_main_layout_margin_left:I = 0x7f0a096a

.field public static final workout_exericsepro_log_subgroup_main_layout_margin_right:I = 0x7f0a096b

.field public static final workout_exericsepro_log_subgroup_sub_layout_margin_left:I = 0x7f0a096c

.field public static final workout_exericsepro_log_subgroup_text_height:I = 0x7f0a095f

.field public static final workout_exericsepro_log_txtgrouprowright_margin_top:I = 0x7f0a0969

.field public static final workout_goaltext_min_width:I = 0x7f0a0ae8

.field public static final workout_grid_title_font_l_font_size:I = 0x7f0a0608

.field public static final workout_grid_title_font_left_margin:I = 0x7f0a060a

.field public static final workout_grid_title_font_s_font_size:I = 0x7f0a0609

.field public static final workout_grid_title_font_top_margin:I = 0x7f0a060b

.field public static final workout_grid_unit_font_extra_large_font_size:I = 0x7f0a0610

.field public static final workout_grid_unit_font_large_font_size:I = 0x7f0a0611

.field public static final workout_grid_unit_font_medium_font_size:I = 0x7f0a0612

.field public static final workout_grid_unit_font_small_font_size:I = 0x7f0a0613

.field public static final workout_grid_value_font_extra_large_font_size:I = 0x7f0a060c

.field public static final workout_grid_value_font_large_font_size:I = 0x7f0a060d

.field public static final workout_grid_value_font_medium_font_size:I = 0x7f0a060e

.field public static final workout_grid_value_font_small_font_size:I = 0x7f0a060f

.field public static final workout_healthboard_bottom_delimiter_height:I = 0x7f0a0a44

.field public static final workout_healthcare_button_height:I = 0x7f0a0600

.field public static final workout_healthcare_button_min_width:I = 0x7f0a05ff

.field public static final workout_healthcare_button_text_size:I = 0x7f0a0601

.field public static final workout_image_holder_margin_bottom:I = 0x7f0a098f

.field public static final workout_image_holder_margin_right:I = 0x7f0a0990

.field public static final workout_image_size_large_l_height:I = 0x7f0a09ae

.field public static final workout_image_size_large_l_margin_bottom:I = 0x7f0a09b0

.field public static final workout_image_size_large_l_margin_right:I = 0x7f0a09af

.field public static final workout_image_size_large_l_width:I = 0x7f0a09ad

.field public static final workout_image_size_large_m_height:I = 0x7f0a09ab

.field public static final workout_image_size_large_m_margin_bottom:I = 0x7f0a09ac

.field public static final workout_image_size_large_m_width:I = 0x7f0a09aa

.field public static final workout_image_size_large_s_height:I = 0x7f0a09a7

.field public static final workout_image_size_large_s_margin_bottom:I = 0x7f0a09a9

.field public static final workout_image_size_large_s_margin_right:I = 0x7f0a09a8

.field public static final workout_image_size_large_s_width:I = 0x7f0a09a6

.field public static final workout_image_size_large_xl_height:I = 0x7f0a09b2

.field public static final workout_image_size_large_xl_width:I = 0x7f0a09b1

.field public static final workout_image_size_medium_l_height:I = 0x7f0a09a4

.field public static final workout_image_size_medium_l_margin_right:I = 0x7f0a09a5

.field public static final workout_image_size_medium_l_width:I = 0x7f0a09a3

.field public static final workout_image_size_medium_m_height:I = 0x7f0a09a1

.field public static final workout_image_size_medium_m_margin_bottom:I = 0x7f0a09a2

.field public static final workout_image_size_medium_m_width:I = 0x7f0a09a0

.field public static final workout_image_size_medium_s_height:I = 0x7f0a099e

.field public static final workout_image_size_medium_s_margin_right:I = 0x7f0a099f

.field public static final workout_image_size_medium_s_width:I = 0x7f0a099d

.field public static final workout_image_size_small_l_height:I = 0x7f0a099a

.field public static final workout_image_size_small_l_margin_bottom:I = 0x7f0a099c

.field public static final workout_image_size_small_l_margin_right:I = 0x7f0a099b

.field public static final workout_image_size_small_l_width:I = 0x7f0a0999

.field public static final workout_image_size_small_m_height:I = 0x7f0a0996

.field public static final workout_image_size_small_m_margin_bottom:I = 0x7f0a0998

.field public static final workout_image_size_small_m_margin_right:I = 0x7f0a0997

.field public static final workout_image_size_small_m_width:I = 0x7f0a0995

.field public static final workout_image_size_small_s_height:I = 0x7f0a0992

.field public static final workout_image_size_small_s_margin_bottom:I = 0x7f0a0994

.field public static final workout_image_size_small_s_margin_right:I = 0x7f0a0993

.field public static final workout_image_size_small_s_width:I = 0x7f0a0991

.field public static final workout_information_icon:I = 0x7f0a0ae9

.field public static final workout_information_margin:I = 0x7f0a0aea

.field public static final workout_informationtext_padding:I = 0x7f0a0aeb

.field public static final workout_informationtext_size:I = 0x7f0a0aec

.field public static final workout_item_pick_button_mine_height:I = 0x7f0a067b

.field public static final workout_item_pick_button_mine_marginLeft:I = 0x7f0a067c

.field public static final workout_item_pick_button_mine_marginRight:I = 0x7f0a067d

.field public static final workout_item_pick_button_mine_width:I = 0x7f0a067a

.field public static final workout_item_pick_search_items_height:I = 0x7f0a0677

.field public static final workout_item_pick_search_items_linearlayout_height:I = 0x7f0a0676

.field public static final workout_item_pick_search_items_marginLeft:I = 0x7f0a0678

.field public static final workout_item_pick_search_items_marginRight:I = 0x7f0a0679

.field public static final workout_letter_popup_height:I = 0x7f0a05fd

.field public static final workout_letter_popup_width:I = 0x7f0a05fc

.field public static final workout_list_item_layout_item_icon_layout_height:I = 0x7f0a0938

.field public static final workout_list_item_layout_item_icon_layout_marginLeft:I = 0x7f0a0939

.field public static final workout_list_item_layout_item_icon_layout_marginRight:I = 0x7f0a093a

.field public static final workout_list_item_layout_item_icon_layout_width:I = 0x7f0a0937

.field public static final workout_list_item_layout_text_date_layout_marginRight:I = 0x7f0a093c

.field public static final workout_list_item_layout_text_date_textSize:I = 0x7f0a093d

.field public static final workout_list_item_layout_text_value_textSize:I = 0x7f0a093e

.field public static final workout_list_item_layout_title__textSize:I = 0x7f0a093b

.field public static final workout_list_item_layout_view_layout_height:I = 0x7f0a093f

.field public static final workout_list_popup_4_personal_profile_item_height:I = 0x7f0a0a43

.field public static final workout_list_popup_divider_height:I = 0x7f0a08e7

.field public static final workout_list_popup_empty_space_height:I = 0x7f0a08e6

.field public static final workout_list_popup_item_height:I = 0x7f0a08e5

.field public static final workout_list_popup_text_padding_left:I = 0x7f0a08ea

.field public static final workout_list_popup_text_padding_right:I = 0x7f0a08eb

.field public static final workout_lockwidget_small_unit:I = 0x7f0a0ae0

.field public static final workout_lockwidget_small_value:I = 0x7f0a0adf

.field public static final workout_lockwidget_title:I = 0x7f0a0adc

.field public static final workout_lockwidget_unit:I = 0x7f0a0ade

.field public static final workout_lockwidget_value:I = 0x7f0a0add

.field public static final workout_medal_accessoryg_height:I = 0x7f0a0ad7

.field public static final workout_medal_accessoryg_width:I = 0x7f0a0ad6

.field public static final workout_medal_walking_height:I = 0x7f0a0ad5

.field public static final workout_medal_walking_width:I = 0x7f0a0ad4

.field public static final workout_minimum_sub_tab_width:I = 0x7f0a05fe

.field public static final workout_my_accessory_manager_layour_gear_split_line_height:I = 0x7f0a095b

.field public static final workout_my_exercise_items_textsize:I = 0x7f0a0648

.field public static final workout_my_ranking_average:I = 0x7f0a0ad2

.field public static final workout_my_ranking_best_steps:I = 0x7f0a0ad3

.field public static final workout_my_ranking_header_height:I = 0x7f0a0a5b

.field public static final workout_my_ranking_header_text_margin:I = 0x7f0a0a64

.field public static final workout_my_ranking_header_title_text_height:I = 0x7f0a0a5e

.field public static final workout_my_ranking_header_title_text_size:I = 0x7f0a0a5c

.field public static final workout_my_ranking_header_title_text_value_margin:I = 0x7f0a0a5d

.field public static final workout_my_ranking_header_totals_height:I = 0x7f0a0a83

.field public static final workout_my_ranking_header_totals_text_size:I = 0x7f0a0a86

.field public static final workout_my_ranking_header_unit_height:I = 0x7f0a0a82

.field public static final workout_my_ranking_header_unit_text_size:I = 0x7f0a0a85

.field public static final workout_my_ranking_header_value_height:I = 0x7f0a0a81

.field public static final workout_my_ranking_header_value_text_size:I = 0x7f0a0a84

.field public static final workout_my_ranking_item_height:I = 0x7f0a0a65

.field public static final workout_my_ranking_item_icon_height:I = 0x7f0a0a67

.field public static final workout_my_ranking_item_icon_margin_right:I = 0x7f0a0a69

.field public static final workout_my_ranking_item_icon_margin_top:I = 0x7f0a0a68

.field public static final workout_my_ranking_item_icon_width:I = 0x7f0a0a66

.field public static final workout_my_ranking_item_margin:I = 0x7f0a0a5a

.field public static final workout_my_ranking_item_margin_left:I = 0x7f0a0a63

.field public static final workout_my_ranking_item_margin_top:I = 0x7f0a0a62

.field public static final workout_my_ranking_item_place_number_text_size:I = 0x7f0a0a6a

.field public static final workout_my_ranking_item_place_stat_text_size:I = 0x7f0a0a6b

.field public static final workout_my_ranking_progress_slider_marker_width:I = 0x7f0a0a7f

.field public static final workout_my_ranking_runner_mark_height:I = 0x7f0a0a73

.field public static final workout_my_ranking_runner_mark_width:I = 0x7f0a0a74

.field public static final workout_my_ranking_slider_average_mark_margin_bottom:I = 0x7f0a0a7d

.field public static final workout_my_ranking_slider_chei_tuflya_size:I = 0x7f0a0a7c

.field public static final workout_my_ranking_slider_height:I = 0x7f0a0a6c

.field public static final workout_my_ranking_slider_left_margin:I = 0x7f0a0a6e

.field public static final workout_my_ranking_slider_progress_bar_line_height:I = 0x7f0a0a78

.field public static final workout_my_ranking_slider_progress_bar_line_text_margin:I = 0x7f0a0a79

.field public static final workout_my_ranking_slider_progress_bar_line_width:I = 0x7f0a0a77

.field public static final workout_my_ranking_slider_progress_bar_mark_text_size:I = 0x7f0a0a7b

.field public static final workout_my_ranking_slider_progress_bar_padding:I = 0x7f0a0a7e

.field public static final workout_my_ranking_slider_progress_bar_value_text_size:I = 0x7f0a0a7a

.field public static final workout_my_ranking_slider_progress_grade_line_width:I = 0x7f0a0a72

.field public static final workout_my_ranking_slider_progress_height:I = 0x7f0a0a71

.field public static final workout_my_ranking_slider_progress_marker_width:I = 0x7f0a0a76

.field public static final workout_my_ranking_slider_progress_text_size:I = 0x7f0a0a75

.field public static final workout_my_ranking_slider_progress_width:I = 0x7f0a0a70

.field public static final workout_my_ranking_slider_right_margin:I = 0x7f0a0a6f

.field public static final workout_my_ranking_slider_top_margin:I = 0x7f0a0a6d

.field public static final workout_my_ranking_title_item_height:I = 0x7f0a0a5f

.field public static final workout_my_ranking_title_item_padding_left:I = 0x7f0a0a61

.field public static final workout_my_ranking_title_item_text_size:I = 0x7f0a0a60

.field public static final workout_my_ranking_world_info_item_width:I = 0x7f0a0a80

.field public static final workout_no_data_text_no_data_textsize:I = 0x7f0a0649

.field public static final workout_pick_item_calories_text_view_size:I = 0x7f0a0680

.field public static final workout_pick_item_height:I = 0x7f0a067f

.field public static final workout_pick_item_product_text_view_size:I = 0x7f0a0681

.field public static final workout_pick_item_star_image_view_height:I = 0x7f0a0683

.field public static final workout_pick_item_star_image_view_width:I = 0x7f0a0682

.field public static final workout_pro_list_popup_text_padding_left:I = 0x7f0a08e8

.field public static final workout_pro_list_popup_text_padding_right:I = 0x7f0a08e9

.field public static final workout_profile_attribute_item_edit_text_container_height:I = 0x7f0a0928

.field public static final workout_ranking_tabs_divider:I = 0x7f0a0a47

.field public static final workout_ranking_tabs_layout_height:I = 0x7f0a0a46

.field public static final workout_realtime_bottom_button_button_icon_size:I = 0x7f0a08e0

.field public static final workout_realtime_bottom_button_button_text_marginleft:I = 0x7f0a08e1

.field public static final workout_realtime_bottom_button_button_text_textsize:I = 0x7f0a08e2

.field public static final workout_realtime_bottom_button_root_layout_height:I = 0x7f0a08de

.field public static final workout_realtime_bottom_button_root_layout_minwidth:I = 0x7f0a08df

.field public static final workout_realtime_drop_text_divider_height:I = 0x7f0a08b8

.field public static final workout_realtime_drop_text_drop_bubble_height_size:I = 0x7f0a09cc

.field public static final workout_realtime_drop_text_drop_bubble_marginleft:I = 0x7f0a067e

.field public static final workout_realtime_drop_text_drop_bubble_width_size:I = 0x7f0a09cb

.field public static final workout_realtime_drop_text_drop_icon_marginleft:I = 0x7f0a08b3

.field public static final workout_realtime_drop_text_drop_icon_size:I = 0x7f0a08b2

.field public static final workout_realtime_drop_text_drop_text_marginleft:I = 0x7f0a08b4

.field public static final workout_realtime_drop_text_drop_text_textsize:I = 0x7f0a08b5

.field public static final workout_realtime_drop_text_imageview_marginbottom:I = 0x7f0a08b7

.field public static final workout_realtime_drop_text_imageview_size:I = 0x7f0a08b6

.field public static final workout_realtime_exercise_status_big_data_icon_leftmargin:I = 0x7f0a0895

.field public static final workout_realtime_exercise_status_big_data_type_height:I = 0x7f0a0896

.field public static final workout_realtime_exercise_status_big_data_type_leftmargin:I = 0x7f0a0897

.field public static final workout_realtime_exercise_status_big_data_type_textsize:I = 0x7f0a0898

.field public static final workout_realtime_exercise_status_big_unit_height:I = 0x7f0a089d

.field public static final workout_realtime_exercise_status_big_unit_margintop:I = 0x7f0a089e

.field public static final workout_realtime_exercise_status_big_unit_textsize:I = 0x7f0a089f

.field public static final workout_realtime_exercise_status_big_value_linespace:I = 0x7f0a089b

.field public static final workout_realtime_exercise_status_big_value_padding:I = 0x7f0a089c

.field public static final workout_realtime_exercise_status_data_layout_padding:I = 0x7f0a08a0

.field public static final workout_realtime_exercise_status_dropdownlist_height:I = 0x7f0a0894

.field public static final workout_realtime_exercise_status_duration_arrow_height:I = 0x7f0a089a

.field public static final workout_realtime_exercise_status_duration_arrow_width:I = 0x7f0a0899

.field public static final workout_realtime_exercise_status_fragment_type_switch_height:I = 0x7f0a08a7

.field public static final workout_realtime_exercise_status_fragment_type_switch_width:I = 0x7f0a08a6

.field public static final workout_realtime_exercise_status_gone_visual_guidance_height:I = 0x7f0a0893

.field public static final workout_realtime_exercise_status_indoor_layout_height:I = 0x7f0a08a5

.field public static final workout_realtime_exercise_status_operate_layout_padding:I = 0x7f0a0517

.field public static final workout_realtime_exercise_status_operate_text_height:I = 0x7f0a08a1

.field public static final workout_realtime_exercise_status_outdoor_layout_height:I = 0x7f0a08a3

.field public static final workout_realtime_exercise_status_outdoor_map_layout_height:I = 0x7f0a08a4

.field public static final workout_realtime_exercise_status_touch_lock_layout_height:I = 0x7f0a08a8

.field public static final workout_realtime_exercise_status_tryit_textsize:I = 0x7f0a08a2

.field public static final workout_realtime_exercise_status_visual_guide_bottom_margin:I = 0x7f0a0892

.field public static final workout_realtime_exercise_status_visual_guide_height:I = 0x7f0a051b

.field public static final workout_realtime_goal_list_settings_item:I = 0x7f0a0865

.field public static final workout_realtime_goal_list_settings_paddingLeft:I = 0x7f0a0864

.field public static final workout_realtime_goal_list_split_line_height:I = 0x7f0a0866

.field public static final workout_realtime_goal_list_text_size:I = 0x7f0a0862

.field public static final workout_realtime_goal_list_text_summary_size:I = 0x7f0a0863

.field public static final workout_realtime_hold_bottom_lock_button_height:I = 0x7f0a08e4

.field public static final workout_realtime_hold_bottom_lock_button_width:I = 0x7f0a08e3

.field public static final workout_realtime_indoor_bottom_connect_layout_marginbottom:I = 0x7f0a08db

.field public static final workout_realtime_indoor_bottom_connect_layout_marginleft:I = 0x7f0a08dc

.field public static final workout_realtime_indoor_bottom_connect_status_marginleft:I = 0x7f0a08dd

.field public static final workout_realtime_indoor_bottom_indoor_info_btn_size:I = 0x7f0a08d8

.field public static final workout_realtime_indoor_bottom_indoor_status_marginleft:I = 0x7f0a08d9

.field public static final workout_realtime_indoor_bottom_indoor_status_textsize:I = 0x7f0a08da

.field public static final workout_realtime_indoor_bottom_root_layout_height:I = 0x7f0a08d7

.field public static final workout_realtime_map_data_data_layout_height:I = 0x7f0a088e

.field public static final workout_realtime_map_data_data_type_text_size:I = 0x7f0a088d

.field public static final workout_realtime_map_data_data_value_height:I = 0x7f0a088f

.field public static final workout_realtime_map_data_exercise_drop_arrow_height:I = 0x7f0a0891

.field public static final workout_realtime_map_data_exercise_drop_arrow_width:I = 0x7f0a0890

.field public static final workout_realtime_map_gps_margin_bottom:I = 0x7f0a0887

.field public static final workout_realtime_map_gps_margin_left:I = 0x7f0a0888

.field public static final workout_realtime_map_gps_signal_map_margin_left:I = 0x7f0a088b

.field public static final workout_realtime_map_gps_signal_map_text_size:I = 0x7f0a088c

.field public static final workout_realtime_map_map_data_layout_height_land:I = 0x7f0a087e

.field public static final workout_realtime_map_map_data_layout_margin_right:I = 0x7f0a086c

.field public static final workout_realtime_map_map_data_layout_margin_right_land:I = 0x7f0a0880

.field public static final workout_realtime_map_map_data_layout_marigin_top:I = 0x7f0a086b

.field public static final workout_realtime_map_map_data_layout_marigin_top_land:I = 0x7f0a087f

.field public static final workout_realtime_map_map_data_layout_padding_bottom_land:I = 0x7f0a0882

.field public static final workout_realtime_map_map_data_layout_padding_left_land:I = 0x7f0a0883

.field public static final workout_realtime_map_map_data_layout_padding_right_land:I = 0x7f0a0884

.field public static final workout_realtime_map_map_data_layout_padding_top_land:I = 0x7f0a0881

.field public static final workout_realtime_map_map_data_layout_width_land:I = 0x7f0a087d

.field public static final workout_realtime_map_map_viewmode_btn_height:I = 0x7f0a086e

.field public static final workout_realtime_map_map_viewmode_btn_height_land:I = 0x7f0a0886

.field public static final workout_realtime_map_map_viewmode_btn_width:I = 0x7f0a086d

.field public static final workout_realtime_map_map_viewmode_btn_width_land:I = 0x7f0a0885

.field public static final workout_realtime_map_map_viewmode_btnmargin_right:I = 0x7f0a0870

.field public static final workout_realtime_map_map_viewmode_btnmargin_top:I = 0x7f0a086f

.field public static final workout_realtime_map_my_map_location_button_height_land:I = 0x7f0a0872

.field public static final workout_realtime_map_my_map_location_button_margin_bottom_land:I = 0x7f0a0873

.field public static final workout_realtime_map_my_map_location_button_margin_right:I = 0x7f0a0868

.field public static final workout_realtime_map_my_map_location_button_margin_right_land:I = 0x7f0a0874

.field public static final workout_realtime_map_my_map_location_button_width_land:I = 0x7f0a0871

.field public static final workout_realtime_map_right_button_height_land:I = 0x7f0a0876

.field public static final workout_realtime_map_right_button_margin_left:I = 0x7f0a086a

.field public static final workout_realtime_map_right_button_margin_left_land:I = 0x7f0a0878

.field public static final workout_realtime_map_right_button_margin_top:I = 0x7f0a0869

.field public static final workout_realtime_map_right_button_margin_top_land:I = 0x7f0a0877

.field public static final workout_realtime_map_right_button_padding_bottom_land:I = 0x7f0a087a

.field public static final workout_realtime_map_right_button_padding_left_land:I = 0x7f0a087b

.field public static final workout_realtime_map_right_button_padding_right_land:I = 0x7f0a087c

.field public static final workout_realtime_map_right_button_padding_top_land:I = 0x7f0a0879

.field public static final workout_realtime_map_right_button_width_land:I = 0x7f0a0875

.field public static final workout_realtime_map_s_health_h_exercise_gps_height:I = 0x7f0a088a

.field public static final workout_realtime_map_s_health_h_exercise_gps_width:I = 0x7f0a0889

.field public static final workout_realtime_map_visual_guidance_height:I = 0x7f0a0867

.field public static final workout_realtime_outdoor_bottom_button_height:I = 0x7f0a08ce

.field public static final workout_realtime_outdoor_bottom_button_marginleft:I = 0x7f0a08cf

.field public static final workout_realtime_outdoor_bottom_button_width:I = 0x7f0a08cd

.field public static final workout_realtime_outdoor_bottom_connect_image_size:I = 0x7f0a08d4

.field public static final workout_realtime_outdoor_bottom_connect_layout_marginleft:I = 0x7f0a08d3

.field public static final workout_realtime_outdoor_bottom_connect_status_marginleft:I = 0x7f0a08d5

.field public static final workout_realtime_outdoor_bottom_connect_status_textsize:I = 0x7f0a08d6

.field public static final workout_realtime_outdoor_bottom_gps_icon_size:I = 0x7f0a08d0

.field public static final workout_realtime_outdoor_bottom_gps_signal_status_marginleft:I = 0x7f0a08d1

.field public static final workout_realtime_outdoor_bottom_gps_signal_status_textsize:I = 0x7f0a08d2

.field public static final workout_realtime_status_data_arrow_image_margin:I = 0x7f0a08c6

.field public static final workout_realtime_status_data_arrow_image_size:I = 0x7f0a08c5

.field public static final workout_realtime_status_data_data_icon_marginleft:I = 0x7f0a08c4

.field public static final workout_realtime_status_data_data_icon_size:I = 0x7f0a08c3

.field public static final workout_realtime_status_data_dropdownbox_height:I = 0x7f0a08c0

.field public static final workout_realtime_status_data_dropdownbox_paddingbottom:I = 0x7f0a08c2

.field public static final workout_realtime_status_data_dropdownbox_paddingtop:I = 0x7f0a08c1

.field public static final workout_realtime_status_data_root_layout_height:I = 0x7f0a08b9

.field public static final workout_realtime_status_data_scroll_layout_height:I = 0x7f0a08c7

.field public static final workout_realtime_status_data_scroll_layout_marginbottom:I = 0x7f0a08ca

.field public static final workout_realtime_status_data_scroll_layout_marginleft:I = 0x7f0a08c9

.field public static final workout_realtime_status_data_scroll_layout_margintop:I = 0x7f0a08c8

.field public static final workout_realtime_status_data_scroll_layout_padding:I = 0x7f0a08ba

.field public static final workout_realtime_status_data_scroll_layout_paddingright:I = 0x7f0a08cb

.field public static final workout_realtime_status_data_scroll_text_textsize:I = 0x7f0a08cc

.field public static final workout_realtime_status_data_scroll_textview_height:I = 0x7f0a08bb

.field public static final workout_realtime_status_data_scroll_textview_margintop:I = 0x7f0a08bc

.field public static final workout_realtime_status_data_scroll_textview_textsize:I = 0x7f0a08bd

.field public static final workout_realtime_status_data_unit_paddingleft:I = 0x7f0a08be

.field public static final workout_realtime_status_data_unit_textsize:I = 0x7f0a08bf

.field public static final workout_realtime_visual_guidance_status_analysed_by_textsize:I = 0x7f0a08aa

.field public static final workout_realtime_visual_guidance_status_firstbeat_logo_img_height:I = 0x7f0a08ac

.field public static final workout_realtime_visual_guidance_status_firstbeat_logo_img_leftmargin:I = 0x7f0a08ad

.field public static final workout_realtime_visual_guidance_status_firstbeat_logo_img_width:I = 0x7f0a08ab

.field public static final workout_realtime_visual_guidance_status_guidance_arrow_size:I = 0x7f0a08ae

.field public static final workout_realtime_visual_guidance_status_guidance_text_height:I = 0x7f0a08af

.field public static final workout_realtime_visual_guidance_status_guidance_text_leftmargin:I = 0x7f0a08b0

.field public static final workout_realtime_visual_guidance_status_guidance_text_textsize:I = 0x7f0a08b1

.field public static final workout_realtime_visual_guidance_status_layout_height:I = 0x7f0a08a9

.field public static final workout_samsung_account_signin_account_image_height:I = 0x7f0a0ae6

.field public static final workout_samsung_account_signin_text_size:I = 0x7f0a0ae7

.field public static final workout_select_all_layout_check_box_all_layout_height_myactivity:I = 0x7f0a064a

.field public static final workout_select_all_layout_check_box_all_layout_size:I = 0x7f0a064c

.field public static final workout_select_all_layout_check_box_all_margin_left_myactivity:I = 0x7f0a064b

.field public static final workout_select_all_layout_check_box_all_textsize:I = 0x7f0a064d

.field public static final workout_settings_item_small:I = 0x7f0a0929

.field public static final workout_settings_margin:I = 0x7f0a05f6

.field public static final workout_settings_title_list_item_text_size:I = 0x7f0a092a

.field public static final workout_slider_bubble_glide_slider_bubble_text_color_textSize:I = 0x7f0a0919

.field public static final workout_slider_scale_bottom_margin:I = 0x7f0a092c

.field public static final workout_slider_scale_height:I = 0x7f0a092d

.field public static final workout_slider_scale_horizontal_margin:I = 0x7f0a092b

.field public static final workout_summary_walkforlife_container_status_distance_top_height:I = 0x7f0a0aca

.field public static final workout_summary_walkforlife_container_status_height:I = 0x7f0a0ac6

.field public static final workout_summary_walkforlife_container_status_kcal_top_height:I = 0x7f0a0acd

.field public static final workout_summary_walkforlife_container_status_margin_top:I = 0x7f0a0ac5

.field public static final workout_summary_walkforlife_container_status_space_devider_height:I = 0x7f0a0acc

.field public static final workout_summary_walkforlife_container_status_space_devider_width:I = 0x7f0a0acb

.field public static final workout_summary_walkforlife_txt_status_distance_height:I = 0x7f0a0ac8

.field public static final workout_summary_walkforlife_txt_status_distance_margin_top:I = 0x7f0a0ac9

.field public static final workout_summary_walkforlife_txt_status_distance_value_height:I = 0x7f0a0ac7

.field public static final workout_summary_walkforlife_txt_status_kcal_value_height:I = 0x7f0a0ace

.field public static final workout_summary_walkforlife_txt_status_wastedcalories_height:I = 0x7f0a0acf

.field public static final workout_summary_walkforlife_txt_status_wastedcalories_margin_top:I = 0x7f0a0ad0

.field public static final workout_tab_height:I = 0x7f0a0a45

.field public static final workout_text_switcher_font_size:I = 0x7f0a0630

.field public static final workout_training_duration_button_width:I = 0x7f0a09be

.field public static final workout_training_duration_text_size:I = 0x7f0a09bf

.field public static final workout_training_effect_goal_firstbeat_logo_img_layout_height:I = 0x7f0a0912

.field public static final workout_training_effect_goal_firstbeat_logo_img_layout_marginLeft:I = 0x7f0a0913

.field public static final workout_training_effect_goal_firstbeat_logo_img_layout_width:I = 0x7f0a0911

.field public static final workout_training_effect_goal_intensity_of_workout_textSize:I = 0x7f0a0900

.field public static final workout_training_effect_goal_layout_information_layout_height:I = 0x7f0a08fa

.field public static final workout_training_effect_goal_realtime_two_bottom_buttons_layout_height:I = 0x7f0a08f9

.field public static final workout_training_effect_goal_realtime_viusal_guidance_layout_height:I = 0x7f0a090f

.field public static final workout_training_effect_goal_realtime_viusal_guidance_textSize:I = 0x7f0a0910

.field public static final workout_training_effect_goal_training_effect_infomation_hint_layout_height:I = 0x7f0a08fb

.field public static final workout_training_effect_goal_training_effect_infomation_hint_paddingLeft:I = 0x7f0a08fc

.field public static final workout_training_effect_goal_training_effect_infomation_hint_paddingRight:I = 0x7f0a08fd

.field public static final workout_training_effect_goal_training_effect_infomation_hint_textSize:I = 0x7f0a08fe

.field public static final workout_training_effect_goal_training_effect_level_content_minHeight:I = 0x7f0a051a

.field public static final workout_training_effect_goal_training_effect_level_content_paddingBottom:I = 0x7f0a0906

.field public static final workout_training_effect_goal_training_effect_level_content_paddingTop:I = 0x7f0a0905

.field public static final workout_training_effect_goal_training_effect_level_content_textSize:I = 0x7f0a0907

.field public static final workout_training_effect_goal_training_effect_level_content_under_linearlayout_layout_height:I = 0x7f0a0908

.field public static final workout_training_effect_goal_training_effect_level_sub_title_textSize:I = 0x7f0a0904

.field public static final workout_training_effect_goal_training_effect_level_title_textSize:I = 0x7f0a0903

.field public static final workout_training_effect_goal_training_effect_progress_layout_height:I = 0x7f0a0901

.field public static final workout_training_effect_goal_training_effect_progress_top_magrin:I = 0x7f0a0902

.field public static final workout_training_effect_goal_training_effect_progress_under_linearlayout_paddingLeft:I = 0x7f0a0519

.field public static final workout_training_effect_goal_training_effect_select_button_layout_height:I = 0x7f0a090a

.field public static final workout_training_effect_goal_training_effect_select_button_layout_width:I = 0x7f0a0909

.field public static final workout_training_effect_goal_training_effect_select_button_textSize:I = 0x7f0a090b

.field public static final workout_training_effect_goal_training_effect_select_button_under_linearlayout_layout_height:I = 0x7f0a090c

.field public static final workout_training_effect_goal_training_effect_select_button_under_linearlayout_layout_marginBottom:I = 0x7f0a090d

.field public static final workout_training_effect_goal_training_effect_select_button_under_linearlayout_layout_marginRight:I = 0x7f0a090e

.field public static final workout_training_effect_goal_training_tw_sub_action_bar_bg_holo_light_layout_height:I = 0x7f0a0518

.field public static final workout_training_effect_goal_training_tw_sub_action_bar_bg_holo_light_paddingLeft:I = 0x7f0a08ff

.field public static final workout_training_effect_guide_firstbeat_layout_height:I = 0x7f0a0932

.field public static final workout_training_effect_guide_firstbeat_layout_layout_height:I = 0x7f0a092e

.field public static final workout_training_effect_guide_firstbeat_layout_layout_marginBottom:I = 0x7f0a092f

.field public static final workout_training_effect_guide_firstbeat_layout_layout_marginRight:I = 0x7f0a0930

.field public static final workout_training_effect_guide_firstbeat_layout_layout_marginTop:I = 0x7f0a0931

.field public static final workout_training_effect_guide_firstbeat_logo_img_layout_height:I = 0x7f0a0935

.field public static final workout_training_effect_guide_firstbeat_logo_img_layout_marginLeft:I = 0x7f0a0936

.field public static final workout_training_effect_guide_firstbeat_logo_img_layout_width:I = 0x7f0a0934

.field public static final workout_training_effect_guide_firstbeat_textSize:I = 0x7f0a0933

.field public static final workout_training_effect_guide_item_View_1_layout_height:I = 0x7f0a0921

.field public static final workout_training_effect_guide_item_View_2_layout_height:I = 0x7f0a0927

.field public static final workout_training_effect_guide_item_detail_text_paddingBottom:I = 0x7f0a0922

.field public static final workout_training_effect_guide_item_detail_text_paddingLeft:I = 0x7f0a0923

.field public static final workout_training_effect_guide_item_detail_text_paddingRight:I = 0x7f0a0924

.field public static final workout_training_effect_guide_item_detail_text_paddingTop:I = 0x7f0a0925

.field public static final workout_training_effect_guide_item_detail_text_textSize:I = 0x7f0a0926

.field public static final workout_training_effect_guide_item_title_drawablePadding:I = 0x7f0a091d

.field public static final workout_training_effect_guide_item_title_layout_height:I = 0x7f0a091c

.field public static final workout_training_effect_guide_item_title_paddingLeft:I = 0x7f0a091e

.field public static final workout_training_effect_guide_item_title_paddingRight:I = 0x7f0a091f

.field public static final workout_training_effect_guide_item_title_textSize:I = 0x7f0a0920

.field public static final workout_training_effect_progress_max_value_textSize:I = 0x7f0a091b

.field public static final workout_training_effect_progress_min_value_textSize:I = 0x7f0a091a

.field public static final workout_training_list_header_height:I = 0x7f0a09bb

.field public static final workout_training_list_header_text_size:I = 0x7f0a09bd

.field public static final workout_training_margin:I = 0x7f0a09bc

.field public static final workout_training_plan_set_day_of_week_check_box_height:I = 0x7f0a09b8

.field public static final workout_training_plan_set_day_of_week_check_box_margin_left:I = 0x7f0a09b6

.field public static final workout_training_plan_set_day_of_week_check_box_margin_right:I = 0x7f0a09b7

.field public static final workout_training_plan_set_day_of_week_check_box_text_size:I = 0x7f0a09ba

.field public static final workout_training_plan_set_day_of_week_check_box_width:I = 0x7f0a09b9

.field public static final workout_training_plan_set_period_data_button_container_height:I = 0x7f0a09b4

.field public static final workout_training_plan_set_period_data_button_height:I = 0x7f0a09c0

.field public static final workout_training_plan_set_period_data_button_margin:I = 0x7f0a09c1

.field public static final workout_training_plan_set_period_day_of_week_container_height:I = 0x7f0a09b5

.field public static final workout_training_slider_bubble_height:I = 0x7f0a0917

.field public static final workout_training_slider_bubble_width:I = 0x7f0a0916

.field public static final workout_training_slider_handler_height:I = 0x7f0a0915

.field public static final workout_training_slider_handler_width:I = 0x7f0a0914

.field public static final workout_try_popup_height:I = 0x7f0a0ae2

.field public static final workout_try_popup_top_margin:I = 0x7f0a0ae3

.field public static final workout_try_popup_width:I = 0x7f0a0ae1

.field public static final workout_txtGroupRowLeft_text_size:I = 0x7f0a0ad9

.field public static final workout_txtGroupRowRight_height:I = 0x7f0a0ad8

.field public static final workout_txtGroupRowRight_text_size:I = 0x7f0a0adb

.field public static final workout_txtGroupRow_unit_text_size:I = 0x7f0a0ada

.field public static final workout_walking_mate_bottom_button_bottom_margin:I = 0x7f0a0a4b

.field public static final workout_walking_mate_bottom_button_height:I = 0x7f0a0a48

.field public static final workout_walking_mate_bottom_button_left_padding:I = 0x7f0a0a49

.field public static final workout_walking_mate_bottom_button_right_padding:I = 0x7f0a0a4a

.field public static final workout_walking_mate_bottom_button_top_margin:I = 0x7f0a0a4c

.field public static final workout_walking_mate_first_bottom_button_bottom_margin:I = 0x7f0a0a55

.field public static final workout_walking_mate_first_bottom_button_font_size:I = 0x7f0a0a56

.field public static final workout_walking_mate_first_bottom_button_icon:I = 0x7f0a0a50

.field public static final workout_walking_mate_first_bottom_button_left_margin:I = 0x7f0a0a51

.field public static final workout_walking_mate_first_bottom_button_left_padding:I = 0x7f0a0a4d

.field public static final workout_walking_mate_first_bottom_button_right_margin:I = 0x7f0a0a52

.field public static final workout_walking_mate_first_bottom_button_right_padding:I = 0x7f0a0a4e

.field public static final workout_walking_mate_first_bottom_button_width:I = 0x7f0a0a4f

.field public static final workout_walking_mate_second_bottom_button_icon:I = 0x7f0a0a54

.field public static final workout_walking_mate_second_bottom_button_width:I = 0x7f0a0a53

.field public static final workout_walking_second_bottom_button_button_text_margin_left:I = 0x7f0a0a57

.field public static final workout_walking_second_bottom_button_button_textsize:I = 0x7f0a0a58

.field public static final workout_walking_trophy_size:I = 0x7f0a0a87

.field public static final workout_widget_height:I = 0x7f0a0a34

.field public static final workout_widget_text_size_small:I = 0x7f0a0a36

.field public static final workout_widget_width:I = 0x7f0a0a35

.field public static final x_axes_height_graph:I = 0x7f0a0151


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2404
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
