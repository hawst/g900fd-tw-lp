.class public Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;
.super Landroid/view/View;
.source "ExerciseProCircleSeekBar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar$OnCircleSeekBarChangeListener;
    }
.end annotation


# static fields
.field private static final STATE_ANGLE:Ljava/lang/String; = "angle"

.field private static final STATE_PARENT:Ljava/lang/String; = "parent"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private arc_finish_radians:I

.field private block_end:Z

.field private block_start:Z

.field private conversion:I

.field private end_wheel:I

.field private init_position:I

.field private isTouchable:Z

.field private lastX:F

.field private last_radians:I

.field private mAngle:F

.field private mArcColor:Landroid/graphics/Paint;

.field private mCircleTextColor:Landroid/graphics/Paint;

.field private mColorCenterHalo:Landroid/graphics/Paint;

.field private mColorCenterHaloRectangle:Landroid/graphics/RectF;

.field private mColorWheelPaint:Landroid/graphics/Paint;

.field private mColorWheelRadius:F

.field private mColorWheelRectangle:Landroid/graphics/RectF;

.field private mColorWheelStrokeWidth:I

.field private mEnable:Z

.field private mMax:I

.field private mOnCircleSeekBarChangeListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar$OnCircleSeekBarChangeListener;

.field private mPointerBitmap:Landroid/graphics/Bitmap;

.field private mPointerColor:Landroid/graphics/Paint;

.field private mPointerHaloPaint:Landroid/graphics/Paint;

.field private mPointerPressBitmap:Landroid/graphics/Bitmap;

.field private mPointerPressRadius:I

.field private mPointerRadius:I

.field private mRadiusSize:F

.field private mTranslationOffset:F

.field private mUserIsMovingPointer:Z

.field private pointerPosition:[F

.field private pointer_color:I

.field private pointer_color_attr:Ljava/lang/String;

.field private pointer_halo_color:I

.field private pointer_halo_color_attr:Ljava/lang/String;

.field private s:Landroid/graphics/SweepGradient;

.field private start_arc:I

.field private text:Ljava/lang/String;

.field private textPaint:Landroid/graphics/Paint;

.field private text_color:I

.field private text_color_attr:Ljava/lang/String;

.field private text_size:I

.field private unactive_wheel_color:I

.field private wheel_color:I

.field private wheel_color_attr:Ljava/lang/String;

.field private wheel_unactive_color_attr:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 77
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 37
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mEnable:Z

    .line 39
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelRectangle:Landroid/graphics/RectF;

    .line 40
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mUserIsMovingPointer:Z

    .line 51
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->conversion:I

    .line 52
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mMax:I

    .line 59
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->block_end:Z

    .line 61
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->last_radians:I

    .line 62
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->block_start:Z

    .line 64
    const/16 v0, 0x168

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->arc_finish_radians:I

    .line 65
    const/16 v0, 0x10e

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->start_arc:I

    .line 69
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorCenterHaloRectangle:Landroid/graphics/RectF;

    .line 74
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->isTouchable:Z

    .line 78
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->init(Landroid/util/AttributeSet;I)V

    .line 79
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 82
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mEnable:Z

    .line 39
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelRectangle:Landroid/graphics/RectF;

    .line 40
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mUserIsMovingPointer:Z

    .line 51
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->conversion:I

    .line 52
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mMax:I

    .line 59
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->block_end:Z

    .line 61
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->last_radians:I

    .line 62
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->block_start:Z

    .line 64
    const/16 v0, 0x168

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->arc_finish_radians:I

    .line 65
    const/16 v0, 0x10e

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->start_arc:I

    .line 69
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorCenterHaloRectangle:Landroid/graphics/RectF;

    .line 74
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->isTouchable:Z

    .line 83
    invoke-direct {p0, p2, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->init(Landroid/util/AttributeSet;I)V

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 87
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mEnable:Z

    .line 39
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelRectangle:Landroid/graphics/RectF;

    .line 40
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mUserIsMovingPointer:Z

    .line 51
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->conversion:I

    .line 52
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mMax:I

    .line 59
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->block_end:Z

    .line 61
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->last_radians:I

    .line 62
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->block_start:Z

    .line 64
    const/16 v0, 0x168

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->arc_finish_radians:I

    .line 65
    const/16 v0, 0x10e

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->start_arc:I

    .line 69
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorCenterHaloRectangle:Landroid/graphics/RectF;

    .line 74
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->isTouchable:Z

    .line 88
    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->init(Landroid/util/AttributeSet;I)V

    .line 89
    return-void
.end method

.method private calculateAngleFromRadians(I)F
    .locals 4
    .param p1, "radians"    # I

    .prologue
    .line 359
    add-int/lit16 v0, p1, 0x10e

    int-to-double v0, v0

    const-wide v2, 0x401921fb54442d18L    # 6.283185307179586

    mul-double/2addr v0, v2

    const-wide v2, 0x4076800000000000L    # 360.0

    div-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method private calculateAngleFromText(I)D
    .locals 10
    .param p1, "position"    # I

    .prologue
    const-wide v0, 0x4056800000000000L    # 90.0

    .line 337
    if-eqz p1, :cond_0

    iget v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mMax:I

    if-lt p1, v6, :cond_1

    .line 344
    :cond_0
    :goto_0
    return-wide v0

    .line 340
    :cond_1
    iget v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mMax:I

    int-to-double v6, v6

    int-to-double v8, p1

    div-double v2, v6, v8

    .line 341
    .local v2, "f":D
    const-wide v6, 0x4076800000000000L    # 360.0

    div-double v4, v6, v2

    .line 342
    .local v4, "f_r":D
    add-double/2addr v0, v4

    .line 344
    .local v0, "ang":D
    goto :goto_0
.end method

.method private calculatePointerPosition(F)[F
    .locals 6
    .param p1, "angle"    # F

    .prologue
    .line 503
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelRadius:F

    float-to-double v2, v2

    float-to-double v4, p1

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-float v0, v2

    .line 504
    .local v0, "x":F
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelRadius:F

    float-to-double v2, v2

    float-to-double v4, p1

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-float v1, v2

    .line 506
    .local v1, "y":F
    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput v0, v2, v3

    const/4 v3, 0x1

    aput v1, v2, v3

    return-object v2
.end method

.method private calculateRadiansFromAngle(F)I
    .locals 6
    .param p1, "angle"    # F

    .prologue
    .line 348
    float-to-double v2, p1

    const-wide v4, 0x401921fb54442d18L    # 6.283185307179586

    div-double/2addr v2, v4

    double-to-float v1, v2

    .line 349
    .local v1, "unit":F
    const/4 v2, 0x0

    cmpg-float v2, v1, v2

    if-gez v2, :cond_0

    .line 350
    const/high16 v2, 0x3f800000    # 1.0f

    add-float/2addr v1, v2

    .line 352
    :cond_0
    const/high16 v2, 0x43b40000    # 360.0f

    mul-float/2addr v2, v1

    const/high16 v3, 0x43870000    # 270.0f

    sub-float/2addr v2, v3

    float-to-int v0, v2

    .line 353
    .local v0, "radians":I
    if-gez v0, :cond_1

    .line 354
    add-int/lit16 v0, v0, 0x168

    .line 355
    :cond_1
    return v0
.end method

.method private calculateTextFromAngle(F)I
    .locals 4
    .param p1, "angle"    # F

    .prologue
    .line 325
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->start_arc:I

    int-to-float v2, v2

    sub-float v1, p1, v2

    .line 326
    .local v1, "m":F
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->end_wheel:I

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->start_arc:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    div-float v0, v2, v1

    .line 327
    .local v0, "f":F
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mMax:I

    int-to-float v2, v2

    div-float/2addr v2, v0

    float-to-int v2, v2

    return v2
.end method

.method private calculateTextFromStartAngle(F)I
    .locals 4
    .param p1, "angle"    # F

    .prologue
    .line 331
    move v1, p1

    .line 332
    .local v1, "m":F
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->end_wheel:I

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->start_arc:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    div-float v0, v2, v1

    .line 333
    .local v0, "f":F
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mMax:I

    int-to-float v2, v2

    div-float/2addr v2, v0

    float-to-int v2, v2

    return v2
.end method

.method private checkInVisiblePoint(FF)Z
    .locals 5
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 542
    const/4 v0, 0x0

    .line 543
    .local v0, "isVisible":Z
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mRadiusSize:F

    sub-float v2, p1, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mRadiusSize:F

    sub-float v3, p2, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    add-float v1, v2, v3

    .line 544
    .local v1, "s":F
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "x : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", y : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", s : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mRadiusSize:F

    const/high16 v3, 0x42480000    # 50.0f

    sub-float/2addr v2, v3

    cmpl-float v2, v1, v2

    if-ltz v2, :cond_0

    .line 546
    const/4 v0, 0x1

    .line 551
    :goto_0
    return v0

    .line 548
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private init(Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "attrs"    # Landroid/util/AttributeSet;
    .param p2, "defStyle"    # I

    .prologue
    const/4 v4, 0x1

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/R$styleable;->workout_ExerciseProCircleSeekBar:[I

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 95
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->initAttributes(Landroid/content/res/TypedArray;)V

    .line 97
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 99
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0208ee

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mPointerBitmap:Landroid/graphics/Bitmap;

    .line 100
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0208ef

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mPointerPressBitmap:Landroid/graphics/Bitmap;

    .line 102
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    .line 103
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->s:Landroid/graphics/SweepGradient;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 104
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->unactive_wheel_color:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 105
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 106
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelStrokeWidth:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 108
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorCenterHalo:Landroid/graphics/Paint;

    .line 109
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorCenterHalo:Landroid/graphics/Paint;

    const v2, -0xff0001

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 110
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorCenterHalo:Landroid/graphics/Paint;

    const/16 v2, 0xcc

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 112
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mPointerHaloPaint:Landroid/graphics/Paint;

    .line 113
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mPointerHaloPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->pointer_halo_color:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 114
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mPointerHaloPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mPointerPressRadius:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 116
    new-instance v1, Landroid/graphics/Paint;

    const/16 v2, 0x41

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->textPaint:Landroid/graphics/Paint;

    .line 117
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->textPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->text_color:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 118
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->textPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 119
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->textPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 120
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->textPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->text_size:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 122
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mPointerColor:Landroid/graphics/Paint;

    .line 123
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mPointerColor:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mPointerRadius:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 124
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mPointerColor:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->pointer_color:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 126
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mArcColor:Landroid/graphics/Paint;

    .line 127
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mArcColor:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->wheel_color:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 128
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mArcColor:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 129
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mArcColor:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelStrokeWidth:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 131
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mCircleTextColor:Landroid/graphics/Paint;

    .line 132
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mCircleTextColor:Landroid/graphics/Paint;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 133
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mCircleTextColor:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 135
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->init_position:I

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->calculateAngleFromText(I)D

    move-result-wide v1

    double-to-int v1, v1

    add-int/lit8 v1, v1, -0x5a

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->arc_finish_radians:I

    .line 137
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->arc_finish_radians:I

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->end_wheel:I

    if-le v1, v2, :cond_0

    .line 138
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->end_wheel:I

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->arc_finish_radians:I

    .line 139
    :cond_0
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->arc_finish_radians:I

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->end_wheel:I

    if-le v1, v2, :cond_1

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->end_wheel:I

    :goto_0
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->calculateAngleFromRadians(I)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mAngle:F

    .line 141
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->arc_finish_radians:I

    int-to-float v1, v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->calculateTextFromAngle(F)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->text:Ljava/lang/String;

    .line 143
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->invalidate()V

    .line 144
    return-void

    .line 139
    :cond_1
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->arc_finish_radians:I

    goto :goto_0
.end method

.method private initAttributes(Landroid/content/res/TypedArray;)V
    .locals 8
    .param p1, "a"    # Landroid/content/res/TypedArray;

    .prologue
    const v7, -0xff0001

    const v6, 0x77b1b8ad

    const/4 v5, 0x0

    const v4, -0x965400

    const/4 v3, -0x1

    .line 174
    const/16 v1, 0xa

    invoke-virtual {p1, v5, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelStrokeWidth:I

    .line 175
    const/16 v1, 0x33

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mPointerRadius:I

    .line 176
    const/16 v1, 0x3d

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mPointerPressRadius:I

    .line 177
    const/4 v1, 0x2

    const/16 v2, 0x64

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mMax:I

    .line 179
    const/16 v1, 0x9

    invoke-virtual {p1, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->wheel_color_attr:Ljava/lang/String;

    .line 180
    const/16 v1, 0xa

    invoke-virtual {p1, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->wheel_unactive_color_attr:Ljava/lang/String;

    .line 181
    const/16 v1, 0xb

    invoke-virtual {p1, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->pointer_color_attr:Ljava/lang/String;

    .line 182
    const/16 v1, 0xc

    invoke-virtual {p1, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->pointer_halo_color_attr:Ljava/lang/String;

    .line 183
    const/16 v1, 0xd

    invoke-virtual {p1, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->text_color_attr:Ljava/lang/String;

    .line 184
    const/4 v1, 0x6

    const/16 v2, 0x5f

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->text_size:I

    .line 185
    const/4 v1, 0x7

    invoke-virtual {p1, v1, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->init_position:I

    .line 187
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->start_arc:I

    .line 188
    const/4 v1, 0x5

    const/16 v2, 0x168

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->end_wheel:I

    .line 190
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->end_wheel:I

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->last_radians:I

    .line 192
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->init_position:I

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->start_arc:I

    if-ge v1, v2, :cond_0

    .line 193
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->start_arc:I

    int-to-float v1, v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->calculateTextFromStartAngle(F)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->init_position:I

    .line 195
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->wheel_color_attr:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 197
    const v1, 0x77b1b8ad

    :try_start_0
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->wheel_color:I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 205
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->wheel_unactive_color_attr:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 207
    const v1, -0x965400

    :try_start_1
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->unactive_wheel_color:I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 216
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->pointer_color_attr:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 218
    const/4 v1, -0x1

    :try_start_2
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->pointer_color:I
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    .line 227
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->pointer_halo_color_attr:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 229
    const/4 v1, -0x1

    :try_start_3
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->pointer_halo_color:I
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_3

    .line 238
    :goto_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->text_color_attr:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 240
    :try_start_4
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->text_color_attr:Ljava/lang/String;

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->text_color:I
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_4

    .line 248
    :goto_4
    return-void

    .line 198
    :catch_0
    move-exception v0

    .line 199
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    iput v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->wheel_color:I

    goto :goto_0

    .line 203
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_1
    iput v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->wheel_color:I

    goto :goto_0

    .line 208
    :catch_1
    move-exception v0

    .line 209
    .restart local v0    # "e":Ljava/lang/IllegalArgumentException;
    iput v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->unactive_wheel_color:I

    goto :goto_1

    .line 213
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_2
    iput v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->unactive_wheel_color:I

    goto :goto_1

    .line 219
    :catch_2
    move-exception v0

    .line 220
    .restart local v0    # "e":Ljava/lang/IllegalArgumentException;
    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->pointer_color:I

    goto :goto_2

    .line 224
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_3
    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->pointer_color:I

    goto :goto_2

    .line 230
    :catch_3
    move-exception v0

    .line 231
    .restart local v0    # "e":Ljava/lang/IllegalArgumentException;
    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->pointer_halo_color:I

    goto :goto_3

    .line 235
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_4
    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->pointer_halo_color:I

    goto :goto_3

    .line 241
    :catch_4
    move-exception v0

    .line 242
    .restart local v0    # "e":Ljava/lang/IllegalArgumentException;
    iput v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->text_color:I

    goto :goto_4

    .line 245
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_5
    iput v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->text_color:I

    goto :goto_4
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 368
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->conversion:I

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mPointerBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mPointerBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 151
    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mPointerBitmap:Landroid/graphics/Bitmap;

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mPointerPressBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mPointerPressBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 155
    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mPointerPressBitmap:Landroid/graphics/Bitmap;

    .line 157
    :cond_1
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 158
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 281
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mTranslationOffset:F

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mTranslationOffset:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 283
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelRectangle:Landroid/graphics/RectF;

    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->start_arc:I

    add-int/lit16 v0, v0, 0x10e

    int-to-float v2, v0

    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->end_wheel:I

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->start_arc:I

    sub-int/2addr v0, v3

    int-to-float v3, v0

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mArcColor:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 285
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mEnable:Z

    if-nez v0, :cond_1

    .line 300
    :cond_0
    :goto_0
    return-void

    .line 288
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelRectangle:Landroid/graphics/RectF;

    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->start_arc:I

    add-int/lit16 v0, v0, 0x10e

    int-to-float v2, v0

    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->arc_finish_radians:I

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->end_wheel:I

    if-le v0, v3, :cond_3

    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->end_wheel:I

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->start_arc:I

    sub-int/2addr v0, v3

    int-to-float v3, v0

    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 293
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->isTouchable:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mUserIsMovingPointer:Z

    if-eqz v0, :cond_2

    .line 294
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->pointerPosition:[F

    aget v0, v0, v4

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->pointerPosition:[F

    aget v1, v1, v6

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mPointerPressRadius:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mPointerHaloPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 297
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->isTouchable:Z

    if-eqz v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->pointerPosition:[F

    aget v0, v0, v4

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->pointerPosition:[F

    aget v1, v1, v6

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mPointerRadius:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mPointerColor:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 288
    :cond_3
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->arc_finish_radians:I

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->start_arc:I

    sub-int/2addr v0, v3

    int-to-float v3, v0

    goto :goto_1
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 163
    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    .line 164
    return-void
.end method

.method public onFinishTemporaryDetach()V
    .locals 0

    .prologue
    .line 170
    invoke-super {p0}, Landroid/view/View;->onFinishTemporaryDetach()V

    .line 171
    return-void
.end method

.method protected onMeasure(II)V
    .locals 9
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    .line 304
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->getSuggestedMinimumHeight()I

    move-result v3

    invoke-static {v3, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->getDefaultSize(II)I

    move-result v0

    .line 306
    .local v0, "height":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->getSuggestedMinimumWidth()I

    move-result v3

    invoke-static {v3, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->getDefaultSize(II)I

    move-result v2

    .line 307
    .local v2, "width":I
    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 308
    .local v1, "min":I
    int-to-float v3, v1

    div-float/2addr v3, v8

    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mRadiusSize:F

    .line 309
    invoke-virtual {p0, v1, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->setMeasuredDimension(II)V

    .line 311
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->isTouchable:Z

    if-nez v3, :cond_0

    .line 312
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelRectangle:Landroid/graphics/RectF;

    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelStrokeWidth:I

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelStrokeWidth:I

    int-to-float v5, v5

    iget v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelStrokeWidth:I

    sub-int v6, v1, v6

    int-to-float v6, v6

    iget v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelStrokeWidth:I

    sub-int v7, v1, v7

    int-to-float v7, v7

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 313
    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mAngle:F

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->calculatePointerPosition(F)[F

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->pointerPosition:[F

    .line 322
    :goto_0
    return-void

    .line 315
    :cond_0
    int-to-float v3, v1

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mTranslationOffset:F

    .line 316
    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mTranslationOffset:F

    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mPointerPressRadius:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelRadius:F

    .line 317
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelRectangle:Landroid/graphics/RectF;

    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelRadius:F

    neg-float v4, v4

    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelRadius:F

    neg-float v5, v5

    iget v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelRadius:F

    iget v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelRadius:F

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 318
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorCenterHaloRectangle:Landroid/graphics/RectF;

    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelRadius:F

    neg-float v4, v4

    div-float/2addr v4, v8

    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelRadius:F

    neg-float v5, v5

    div-float/2addr v5, v8

    iget v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelRadius:F

    div-float/2addr v6, v8

    iget v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelRadius:F

    div-float/2addr v7, v8

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 319
    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mAngle:F

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->calculatePointerPosition(F)[F

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->pointerPosition:[F

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 522
    move-object v0, p1

    check-cast v0, Landroid/os/Bundle;

    .line 524
    .local v0, "savedState":Landroid/os/Bundle;
    const-string/jumbo v2, "parent"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    .line 525
    .local v1, "superState":Landroid/os/Parcelable;
    invoke-super {p0, v1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 527
    const-string v2, "angle"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mAngle:F

    .line 528
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mAngle:F

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->calculateRadiansFromAngle(F)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->arc_finish_radians:I

    .line 529
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->arc_finish_radians:I

    int-to-float v2, v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->calculateTextFromAngle(F)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->text:Ljava/lang/String;

    .line 530
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mAngle:F

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->calculatePointerPosition(F)[F

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->pointerPosition:[F

    .line 531
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .prologue
    .line 511
    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 513
    .local v1, "superState":Landroid/os/Parcelable;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 514
    .local v0, "state":Landroid/os/Bundle;
    const-string/jumbo v2, "parent"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 515
    const-string v2, "angle"

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mAngle:F

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 517
    return-object v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v9, 0x3c

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 399
    iget-boolean v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mEnable:Z

    if-eqz v5, :cond_0

    iget-boolean v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->isTouchable:Z

    if-nez v5, :cond_1

    .line 488
    :cond_0
    :goto_0
    return v3

    .line 401
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    iget v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mTranslationOffset:F

    sub-float v1, v5, v6

    .line 402
    .local v1, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    iget v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mTranslationOffset:F

    sub-float v2, v5, v6

    .line 404
    .local v2, "y":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 480
    :cond_2
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 481
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    invoke-interface {v5, v4}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 483
    :cond_3
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->lastX:F

    .line 485
    iget-boolean v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mUserIsMovingPointer:Z

    if-eqz v5, :cond_0

    move v3, v4

    .line 486
    goto :goto_0

    .line 406
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    invoke-direct {p0, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->checkInVisiblePoint(FF)Z

    move-result v5

    iput-boolean v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mUserIsMovingPointer:Z

    .line 407
    iget-boolean v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mUserIsMovingPointer:Z

    if-eqz v5, :cond_0

    .line 410
    float-to-double v5, v2

    float-to-double v7, v1

    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v5

    double-to-float v5, v5

    iput v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mAngle:F

    .line 411
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->block_end:Z

    .line 412
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->block_start:Z

    .line 414
    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mAngle:F

    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->calculateRadiansFromAngle(F)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->arc_finish_radians:I

    .line 416
    iget-boolean v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->block_end:Z

    if-nez v5, :cond_2

    iget-boolean v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->block_start:Z

    if-nez v5, :cond_2

    .line 417
    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->arc_finish_radians:I

    int-to-float v5, v5

    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->calculateTextFromAngle(F)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->text:Ljava/lang/String;

    .line 418
    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mAngle:F

    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->calculatePointerPosition(F)[F

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->pointerPosition:[F

    .line 419
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->invalidate()V

    goto :goto_1

    .line 423
    :pswitch_1
    iget-boolean v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mUserIsMovingPointer:Z

    if-eqz v5, :cond_2

    .line 424
    float-to-double v5, v2

    float-to-double v7, v1

    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v5

    double-to-float v5, v5

    iput v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mAngle:F

    .line 426
    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mAngle:F

    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->calculateRadiansFromAngle(F)I

    move-result v0

    .line 428
    .local v0, "radians":I
    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->last_radians:I

    if-le v5, v0, :cond_6

    if-ge v0, v9, :cond_6

    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->lastX:F

    cmpl-float v5, v1, v5

    if-lez v5, :cond_6

    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->last_radians:I

    if-le v5, v9, :cond_6

    .line 431
    iget-boolean v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->block_end:Z

    if-nez v5, :cond_4

    iget-boolean v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->block_start:Z

    if-nez v5, :cond_4

    .line 432
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->block_end:Z

    .line 447
    :cond_4
    :goto_2
    iget-boolean v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->block_end:Z

    if-eqz v5, :cond_b

    .line 449
    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->end_wheel:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->arc_finish_radians:I

    .line 450
    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mMax:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->text:Ljava/lang/String;

    .line 451
    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->arc_finish_radians:I

    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->calculateAngleFromRadians(I)F

    move-result v5

    iput v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mAngle:F

    .line 452
    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mAngle:F

    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->calculatePointerPosition(F)[F

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->pointerPosition:[F

    .line 465
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->invalidate()V

    .line 466
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mOnCircleSeekBarChangeListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar$OnCircleSeekBarChangeListener;

    if-eqz v5, :cond_5

    .line 467
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mOnCircleSeekBarChangeListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar$OnCircleSeekBarChangeListener;

    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->text:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v5, p0, v6, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar$OnCircleSeekBarChangeListener;->onProgressChanged(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;IZ)V

    .line 470
    :cond_5
    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->last_radians:I

    goto/16 :goto_1

    .line 433
    :cond_6
    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->last_radians:I

    iget v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->start_arc:I

    if-lt v5, v6, :cond_7

    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->last_radians:I

    const/16 v6, 0x5a

    if-gt v5, v6, :cond_7

    const/16 v5, 0x167

    if-gt v0, v5, :cond_7

    const/16 v5, 0x10e

    if-lt v0, v5, :cond_7

    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->lastX:F

    cmpg-float v5, v1, v5

    if-gez v5, :cond_7

    .line 435
    iget-boolean v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->block_start:Z

    if-nez v5, :cond_4

    iget-boolean v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->block_end:Z

    if-nez v5, :cond_4

    .line 436
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->block_start:Z

    goto :goto_2

    .line 437
    :cond_7
    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->end_wheel:I

    if-lt v0, v5, :cond_8

    iget-boolean v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->block_start:Z

    if-nez v5, :cond_8

    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->last_radians:I

    if-ge v5, v0, :cond_8

    .line 438
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->block_end:Z

    goto :goto_2

    .line 439
    :cond_8
    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->end_wheel:I

    if-ge v0, v5, :cond_9

    iget-boolean v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->block_end:Z

    if-eqz v5, :cond_9

    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->last_radians:I

    iget v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->end_wheel:I

    if-le v5, v6, :cond_9

    .line 440
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->block_end:Z

    goto :goto_2

    .line 441
    :cond_9
    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->start_arc:I

    if-ge v0, v5, :cond_a

    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->last_radians:I

    if-le v5, v0, :cond_a

    iget-boolean v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->block_end:Z

    if-nez v5, :cond_a

    .line 442
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->block_start:Z

    goto/16 :goto_2

    .line 443
    :cond_a
    iget-boolean v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->block_start:Z

    if-eqz v5, :cond_4

    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->last_radians:I

    if-ge v5, v0, :cond_4

    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->start_arc:I

    if-le v0, v5, :cond_4

    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->end_wheel:I

    if-ge v0, v5, :cond_4

    .line 444
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->block_start:Z

    goto/16 :goto_2

    .line 453
    :cond_b
    iget-boolean v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->block_start:Z

    if-eqz v5, :cond_c

    .line 455
    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->start_arc:I

    iput v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->arc_finish_radians:I

    .line 456
    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->arc_finish_radians:I

    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->calculateAngleFromRadians(I)F

    move-result v5

    iput v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mAngle:F

    .line 457
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->text:Ljava/lang/String;

    .line 458
    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mAngle:F

    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->calculatePointerPosition(F)[F

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->pointerPosition:[F

    goto/16 :goto_3

    .line 460
    :cond_c
    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mAngle:F

    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->calculateRadiansFromAngle(F)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->arc_finish_radians:I

    .line 461
    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->arc_finish_radians:I

    int-to-float v5, v5

    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->calculateTextFromAngle(F)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->text:Ljava/lang/String;

    .line 463
    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mAngle:F

    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->calculatePointerPosition(F)[F

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->pointerPosition:[F

    goto/16 :goto_3

    .line 475
    .end local v0    # "radians":I
    :pswitch_2
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mUserIsMovingPointer:Z

    .line 476
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->invalidate()V

    goto/16 :goto_1

    .line 404
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setEnabled(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 269
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mEnable:Z

    .line 270
    invoke-super {p0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 271
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->invalidate()V

    .line 272
    return-void
.end method

.method public setMax(I)V
    .locals 0
    .param p1, "max"    # I

    .prologue
    .line 372
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mMax:I

    .line 373
    return-void
.end method

.method public setOffset(I)V
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 376
    int-to-float v0, p1

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mTranslationOffset:F

    .line 377
    return-void
.end method

.method public setOnSeekBarChangeListener(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar$OnCircleSeekBarChangeListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar$OnCircleSeekBarChangeListener;

    .prologue
    .line 534
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mOnCircleSeekBarChangeListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar$OnCircleSeekBarChangeListener;

    .line 535
    return-void
.end method

.method public setPointerColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 261
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mPointerColor:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 262
    return-void
.end method

.method public setPressPointerColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 265
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mPointerHaloPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 266
    return-void
.end method

.method public setProgress(F)V
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 380
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mMax:I

    int-to-float v0, v0

    div-float v0, p1, v0

    const/high16 v1, 0x43b40000    # 360.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->arc_finish_radians:I

    .line 381
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->arc_finish_radians:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->calculateAngleFromRadians(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mAngle:F

    .line 382
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mAngle:F

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->calculatePointerPosition(F)[F

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->pointerPosition:[F

    .line 383
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->invalidate()V

    .line 384
    return-void
.end method

.method public setProgressBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 253
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mArcColor:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 254
    return-void
.end method

.method public setProgressColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 257
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 258
    return-void
.end method

.method public setStrokeWidth(I)V
    .locals 2
    .param p1, "width"    # I

    .prologue
    .line 391
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelStrokeWidth:I

    .line 392
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelStrokeWidth:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 393
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mArcColor:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->mColorWheelStrokeWidth:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 394
    return-void
.end method

.method public setTouchAble(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 387
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->isTouchable:Z

    .line 388
    return-void
.end method
