.class Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12$1;
.super Ljava/lang/Object;
.source "SpO2SummaryFragmentNew.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;)V
    .locals 0

    .prologue
    .line 1029
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12$1;->this$1:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "anim"    # Landroid/view/animation/Animation;

    .prologue
    const/16 v2, 0x8

    .line 1038
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12$1;->this$1:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;

    iget-object v0, v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$2600(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->SPO2:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->isMedical(Landroid/content/Context;Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1039
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12$1;->this$1:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;

    iget-object v0, v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$1100(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1043
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12$1;->this$1:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;

    iget-object v0, v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$1000(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1044
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12$1;->this$1:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;

    iget-object v0, v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBig:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$3400(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1045
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12$1;->this$1:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;

    iget-object v0, v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$1200(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1046
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12$1;->this$1:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;

    iget-object v0, v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBigAnimator:Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$3500(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->clearAnimation()V

    .line 1047
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12$1;->this$1:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;

    iget-object v0, v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIconBigAnimator:Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$3500(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterGIFViewUtils;->setVisibility(I)V

    .line 1048
    invoke-virtual {p1}, Landroid/view/animation/Animation;->cancel()V

    .line 1049
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12$1;->this$1:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;

    iget-object v0, v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12$1;->this$1:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;

    iget-object v1, v1, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mHeartRate:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$3600(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12$1;->this$1:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;

    iget-object v2, v2, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mO2Data:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$3700(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->stopSensorAndUpdateUI(IF)V

    .line 1050
    return-void

    .line 1041
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12$1;->this$1:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;

    iget-object v0, v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$12;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$1100(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 1035
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 1032
    return-void
.end method
