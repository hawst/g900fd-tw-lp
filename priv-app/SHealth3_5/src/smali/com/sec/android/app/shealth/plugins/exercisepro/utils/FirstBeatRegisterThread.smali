.class public Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;
.super Ljava/lang/Thread;
.source "FirstBeatRegisterThread.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread$1;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread$ResponseListenerHandler;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread$PostBody;
    }
.end annotation


# static fields
.field private static final PRIVATE_ID_GET_ACCESS_TOKEN:I = 0xc8

.field private static final PRIVATE_ID_POST_ACTIVATiON:I = 0x12c

.field private static final TAG:Ljava/lang/String;

.field private static final TIMEOUT_WAIT_RESPONSE:I = 0x2710

.field private static useHttps:Z


# instance fields
.field private mCondResponse:Landroid/os/ConditionVariable;

.field private mConnectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

.field private mContext:Landroid/content/Context;

.field private mResponseHandler:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread$ResponseListenerHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->TAG:Ljava/lang/String;

    .line 34
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->useHttps:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 42
    new-instance v0, Landroid/os/ConditionVariable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->mCondResponse:Landroid/os/ConditionVariable;

    .line 44
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread$ResponseListenerHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread$ResponseListenerHandler;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->mResponseHandler:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread$ResponseListenerHandler;

    .line 54
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->mContext:Landroid/content/Context;

    .line 55
    return-void
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;)Landroid/os/ConditionVariable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->mCondResponse:Landroid/os/ConditionVariable;

    return-object v0
.end method

.method private initConnMan()V
    .locals 5

    .prologue
    .line 73
    invoke-static {}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->createInstance()Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    .line 74
    const-string v1, "FirstBeat"

    invoke-static {v1}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->createInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    .line 75
    const-string v1, "FirstBeat"

    invoke-static {v1}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->mConnectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    .line 77
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->mConnectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    sget-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->useHttps:Z

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->initConnectionManager(ZLjava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->TAG:Ljava/lang/String;

    const-string v2, "initConnMan()"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    :goto_0
    return-void

    .line 78
    :catch_0
    move-exception v0

    .line 79
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->TAG:Ljava/lang/String;

    const-string v2, "initConnMan()"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->TAG:Ljava/lang/String;

    const-string v3, "initConnMan()"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    throw v1
.end method

.method private postActivation()Z
    .locals 18

    .prologue
    .line 86
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "postActivation()"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    const/4 v7, 0x0

    .line 89
    .local v7, "jsonRepresentation":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->mConnectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    const-string/jumbo v2, "openapi.samsungshealth.com"

    sget v3, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->HTTP_PORT:I

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->setAddress(Ljava/lang/String;I)Z

    .line 90
    const-string/jumbo v5, "platform/common/setAccessoryActivation"

    .line 91
    .local v5, "urlD":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " urlD["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    :try_start_0
    new-instance v14, Lcom/google/gson/GsonBuilder;

    invoke-direct {v14}, Lcom/google/gson/GsonBuilder;-><init>()V

    .line 95
    .local v14, "gsonBuilder":Lcom/google/gson/GsonBuilder;
    invoke-virtual {v14}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v13

    .line 96
    .local v13, "gson":Lcom/google/gson/Gson;
    new-instance v15, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread$PostBody;

    const/4 v1, 0x0

    invoke-direct {v15, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread$PostBody;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread$1;)V

    .line 97
    .local v15, "postBody":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread$PostBody;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "phone"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/telephony/TelephonyManager;

    .line 98
    .local v11, "TelephonyMgr":Landroid/telephony/TelephonyManager;
    invoke-virtual {v11}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v15, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread$PostBody;->deviceUID:Ljava/lang/String;

    .line 99
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v1, v15, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread$PostBody;->deviceModel:Ljava/lang/String;

    .line 100
    const-string v1, "HRM"

    iput-object v1, v15, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread$PostBody;->accType:Ljava/lang/String;

    .line 101
    invoke-virtual {v13, v15}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 106
    .end local v11    # "TelephonyMgr":Landroid/telephony/TelephonyManager;
    .end local v13    # "gson":Lcom/google/gson/Gson;
    .end local v14    # "gsonBuilder":Lcom/google/gson/GsonBuilder;
    .end local v15    # "postBody":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread$PostBody;
    :goto_0
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 107
    .local v10, "headerParams":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    const-string v1, "accept"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "application/json"

    aput-object v4, v2, v3

    invoke-virtual {v10, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->mConnectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    const/16 v3, 0x12c

    sget-object v4, Lcom/sec/android/service/health/connectionmanager2/MethodType;->POST:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->mResponseHandler:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread$ResponseListenerHandler;

    const/4 v9, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v1 .. v10}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->placeRequest(Ljava/lang/Object;ILcom/sec/android/service/health/connectionmanager2/MethodType;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/RequestParam;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;Ljava/lang/Object;Ljava/util/HashMap;)J

    move-result-wide v16

    .line 111
    .local v16, "requestId":J
    const-wide/16 v1, 0x0

    cmp-long v1, v16, v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    return v1

    .line 102
    .end local v10    # "headerParams":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    .end local v16    # "requestId":J
    :catch_0
    move-exception v12

    .line 103
    .local v12, "e":Ljava/lang/Exception;
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 111
    .end local v12    # "e":Ljava/lang/Exception;
    .restart local v10    # "headerParams":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    .restart local v16    # "requestId":J
    :cond_0
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->initConnMan()V

    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->postActivation()Z

    move-result v0

    .line 65
    .local v0, "ret":Z
    if-eqz v0, :cond_1

    .line 66
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->mCondResponse:Landroid/os/ConditionVariable;

    const-wide/16 v2, 0x2710

    invoke-virtual {v1, v2, v3}, Landroid/os/ConditionVariable;->block(J)Z

    move-result v1

    if-nez v1, :cond_0

    .line 68
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/FirstBeatRegisterThread;->mCondResponse:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->close()V

    .line 70
    :cond_1
    return-void
.end method
