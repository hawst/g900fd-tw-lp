.class Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$4;
.super Ljava/lang/Object;
.source "BaseFoodSearchApi.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$ResultRunnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->performCategorySearch()Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$ResultRunnable",
        "<",
        "Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;)V
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$4;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$4;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->performCategorySearchRealization()Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic run()Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$4;->run()Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;

    move-result-object v0

    return-object v0
.end method
