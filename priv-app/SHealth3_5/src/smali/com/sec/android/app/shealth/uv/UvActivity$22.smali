.class Lcom/sec/android/app/shealth/uv/UvActivity$22;
.super Ljava/lang/Object;
.source "UvActivity.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/uv/UvActivity;->animate(Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

.field final synthetic val$imageView1:Landroid/widget/ImageView;

.field final synthetic val$imageView2:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/UvActivity;Landroid/widget/ImageView;Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 1028
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvActivity$22;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    iput-object p2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$22;->val$imageView2:Landroid/widget/ImageView;

    iput-object p3, p0, Lcom/sec/android/app/shealth/uv/UvActivity$22;->val$imageView1:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v1, 0x0

    .line 1030
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$22;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvActivity;->animationFadeIn:Landroid/view/animation/AnimationSet;

    if-ne p1, v0, :cond_1

    .line 1031
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$22;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/uv/UvActivity;->isImage1Found:Z

    if-eqz v0, :cond_0

    .line 1032
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$22;->val$imageView2:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1034
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$22;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/uv/UvActivity;->isImage2Found:Z

    if-eqz v0, :cond_1

    .line 1035
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$22;->val$imageView1:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1038
    :cond_1
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 1041
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 1044
    return-void
.end method
