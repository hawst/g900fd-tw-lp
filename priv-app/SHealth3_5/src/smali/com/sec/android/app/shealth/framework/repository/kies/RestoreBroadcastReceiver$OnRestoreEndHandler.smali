.class Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver$OnRestoreEndHandler;
.super Ljava/lang/Object;
.source "RestoreBroadcastReceiver.java"

# interfaces
.implements Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer$RestoreCompleteListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnRestoreEndHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;)V
    .locals 0

    .prologue
    .line 268
    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver$OnRestoreEndHandler;->this$0:Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver$1;

    .prologue
    .line 268
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver$OnRestoreEndHandler;-><init>(Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;)V

    return-void
.end method


# virtual methods
.method public onRestoreEnd(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 4
    .param p1, "intentName"    # Ljava/lang/String;
    .param p2, "completeIntent"    # Landroid/content/Intent;

    .prologue
    .line 276
    # getter for: Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->access$100()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "KiesBackuper onBackupRestoreEnd : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver$OnRestoreEndHandler;->this$0:Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;

    # invokes: Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->upgradeApplication()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->access$200(Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;)V

    .line 279
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreType;->Kies:Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreType;

    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier;->restoreStopped(Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreType;)V

    .line 280
    const-string v1, "SOURCE"

    # getter for: Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->kies_intent_source:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->access$300()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 281
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, p2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 284
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.RESTART_WIDGET"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 285
    .local v0, "i":Landroid/content/Intent;
    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 286
    return-void
.end method
