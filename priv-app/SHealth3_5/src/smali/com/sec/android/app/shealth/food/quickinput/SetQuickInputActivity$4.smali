.class Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$4;
.super Ljava/lang/Object;
.source "SetQuickInputActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)V
    .locals 0

    .prologue
    .line 374
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$4;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 5
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    const/4 v4, 0x1

    .line 379
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$4;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    const v0, 0x7f0804a2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    # setter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mCalorieValueEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$302(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;)Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    .line 380
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$4;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mCalorieValueEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$300(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$4;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mClickedMealType:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$000(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider;->getCalorieForMealType(Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->setText(Ljava/lang/CharSequence;)V

    .line 381
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$4;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mCalorieValueEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$300(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$4;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mEmptyFieldListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->setEmptyFieldListener(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;)V

    .line 382
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$4;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mCalorieValueEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$300(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->setMinValue(I)V

    .line 383
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$4;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mCalorieValueEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$300(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v2, 0x40000000    # 2.0f

    const v3, 0x459c4000    # 5000.0f

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->setIntervalValues(FFF)V

    .line 384
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$4;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mCalorieValueEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$300(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->setSelectAllOnFocus(Z)V

    .line 385
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$4;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mCalorieValueEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$300(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    move-result-object v0

    new-array v1, v4, [Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$4;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mMinMaxIntegerInputFilter:Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$600(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 387
    return-void
.end method
